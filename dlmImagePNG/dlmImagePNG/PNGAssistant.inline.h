//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmImagePNG library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmImagePNG_PNGAssistant_inline_h_
#define dlmImagePNG_PNGAssistant_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmImagePNG/PNGAssistant.h"

//------------------------------------------------------------------------------
#include "dlm/core/Exception.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <png.h>
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <type_traits>
#include <memory>
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <cstdio>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmImagePNG
{

//==============================================================================
// Implementation namespace
//------------------------------------------------------------------------------
namespace Implementation
{

//==============================================================================
// Local classes and structures
//------------------------------------------------------------------------------
template <typename T>
struct DataExchangeStructure
{
  //============================================================================
  // Type restriction
  //----------------------------------------------------------------------------
  static_assert(std::is_integral<T>::value,
    "This struct is only valid for integral types.");

  //============================================================================
  // Constructors
  //----------------------------------------------------------------------------
  DataExchangeStructure()
    : width   (0)
    , height  (0)
    , channels(0)
  {}

  //----------------------------------------------------------------------------
  DataExchangeStructure(DataExchangeStructure const& other)
    : data    (new T[other.width * other.height * other.channels])
    , width   (other.width                                       )
    , height  (other.height                                      )
    , channels(other.channels                                    )
  {}

  //----------------------------------------------------------------------------
  DataExchangeStructure(DataExchangeStructure&& other)
    : data    (std::move(other.data    ))
    , width   (          other.width    )
    , height  (          other.height   )
    , channels(          other.channels )
  {}

  //============================================================================
  // Operators
  //----------------------------------------------------------------------------
  DataExchangeStructure& operator=(DataExchangeStructure other)
  {
    swap(*this, other);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    return *this;
  }

  //============================================================================
  // Friends
  //----------------------------------------------------------------------------
  friend void swap(DataExchangeStructure& first, DataExchangeStructure& second)
  {
    using std::swap;

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    swap(first.data, second.data);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    swap(first.width,    second.width   );
    swap(first.height,   second.height  );
    swap(first.channels, second.channels);
  }

  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  std::unique_ptr<T[]> data;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  png_uint_32 width;
  png_uint_32 height;
  png_uint_32 channels;
};

//==============================================================================
// Local functions
//------------------------------------------------------------------------------
// Returns true if the runtime architecture is little endian.
// Credit goes to birryree at [http://stackoverflow.com/questions/4239993].
inline bool IsLittleEndian()
{
  dlm::int32 const one = 0x1;

  dlm::int8 const* onePtr = reinterpret_cast<dlm::int8 const*>(&one);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return onePtr[0] == 1;
}

//------------------------------------------------------------------------------
template <typename T>
inline DataExchangeStructure<T> ReadPNG(std::string const& filename)
{
  static_assert(std::is_integral<T>::value,
    "This function is only defined for integral types.");

  static_assert(sizeof(T) == 1 || sizeof(T) == 2,
    "This function is only defined for 1- or 2-byte integral types.");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FILE* file = fopen(filename.c_str(), "rb");

  DLM_ASSERT(file != NULL, DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Ensure that the file actually has a PNG header.
  char header[8] = { '\0' };

  if (fread(header, 1, 8, file) != 8)
  {
    fclose(file);

    throw DLM_RUNTIME_ERROR("Reading header information failed");
  }

  if (png_sig_cmp(reinterpret_cast<png_bytep>(header), 0, 8) != 0)
  {
    fclose(file);

    throw DLM_RUNTIME_ERROR("ReadPNG: file is not a PNG file");
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Create auxiliary data structures.
  png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);

  if (png_ptr == NULL)
  {
    fclose(file);

    throw DLM_RUNTIME_ERROR("ReadPNG: png_create_read_struct failed");
  }

  png_infop info_ptr = png_create_info_struct(png_ptr);

  if (info_ptr == NULL)
  {
    png_destroy_read_struct(&png_ptr, 0, 0);
    fclose(file);

    throw DLM_RUNTIME_ERROR("ReadPNG: png_create_info_struct failed");
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Prepare PNG file for reading.

  // In case of an error, libpng will longjmp back, so setjmp here.
  if (setjmp(png_jmpbuf(png_ptr)))
  {
    png_destroy_read_struct(&png_ptr, &info_ptr, 0);
    fclose(file);

    throw DLM_RUNTIME_ERROR("ReadPNG: libpng initialization error");
  }

  png_init_io(png_ptr, file);
  png_set_sig_bytes(png_ptr, 8);

  png_read_info(png_ptr, info_ptr);

  png_uint_32 const width          = png_get_image_width (png_ptr, info_ptr);
  png_uint_32 const height         = png_get_image_height(png_ptr, info_ptr);
  png_uint_32 const bit_depth      = png_get_bit_depth   (png_ptr, info_ptr);
  png_uint_32 const color_type     = png_get_color_type  (png_ptr, info_ptr);
  png_uint_32 const interlace_type = png_get_interlace_type(png_ptr, info_ptr);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (interlace_type == PNG_INTERLACE_ADAM7)
  {
    png_set_interlace_handling(png_ptr);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (color_type == PNG_COLOR_TYPE_PALETTE)
  {
    png_set_palette_to_rgb(png_ptr);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8)
  {
    png_set_expand_gray_1_2_4_to_8(png_ptr);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS))
  {
    png_set_tRNS_to_alpha(png_ptr);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (bit_depth == 16 && IsLittleEndian())
  {
    png_set_swap(png_ptr);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (bit_depth == 16 && sizeof(T) == 1)
  {
#ifdef PNG_READ_SCALE_16_TO_8_SUPPORTED
    png_set_scale_16(png_ptr);
#else
    png_set_strip_16(png_ptr);
#endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (bit_depth == 8  && sizeof(T) == 2)
  {
#ifdef PNG_READ_EXPAND_16_SUPPORTED
    png_set_expand_16(png_ptr);
#else
    throw std::string("Expanding to 16 bit not supported.");
#endif
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (bit_depth < 8)
  {
    png_set_packing(png_ptr);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  png_read_update_info(png_ptr, info_ptr);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  png_byte const channels = png_get_channels(png_ptr, info_ptr);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Read file data.
  dlm::uint64 const elements = width * height * channels;

  std::unique_ptr<T[]> data(new T[elements]());

  std::unique_ptr<png_bytep[]> row_pointers(new png_bytep[height]);

  for (png_uint_32 i = 0; i < height; ++i)
  {
    png_uint_32 const offset = i * width * channels;

    row_pointers[i] = reinterpret_cast<png_bytep>(data.get() + offset);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (setjmp(png_jmpbuf(png_ptr)))
  {
    png_destroy_read_struct(&png_ptr, &info_ptr, 0);
    fclose(file);

    throw(DLM_RUNTIME_ERROR("ReadPNG: libpng reading error"));
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Actually read the image data.
  png_read_image(png_ptr, row_pointers.get());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Clean up.
  png_read_end(png_ptr, 0); // Not interested in comments and stuff.
  png_destroy_read_struct(&png_ptr, &info_ptr, 0);

  fclose(file);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Construct return struct.
  DataExchangeStructure<T> result;

  result.width    = static_cast<dlm::uint32>(width   );
  result.height   = static_cast<dlm::uint32>(height  );
  result.channels = static_cast<dlm::uint32>(channels);

  result.data = std::move(data);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <typename T>
inline void WritePNG(
  DataExchangeStructure<T> const& data, std::string const& filename)
{
  static_assert(std::is_integral<T>::value,
    "This function is only defined for integral types.");

  static_assert(sizeof(T) == 1 || sizeof(T) == 2,
    "This function is only defined for 1- or 2-byte integral types.");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DLM_ASSERT(data.width    != 0, DLM_RUNTIME_ERROR);
  DLM_ASSERT(data.height   != 0, DLM_RUNTIME_ERROR);
  DLM_ASSERT(data.channels != 0, DLM_RUNTIME_ERROR);
  DLM_ASSERT(data.channels <  5, DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FILE* file = fopen(filename.c_str(), "wb");

  DLM_ASSERT(file != NULL, DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Create auxiliary data structures.
  png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);

  if (png_ptr == NULL)
  {
    fclose(file);

    throw DLM_RUNTIME_ERROR("WritePNG: png_create_write_struct failed");
  }

  png_infop info_ptr = png_create_info_struct(png_ptr);

  if (info_ptr == NULL)
  {
    png_destroy_write_struct(&png_ptr, 0);
    fclose(file);

    throw DLM_RUNTIME_ERROR("WritePNG: png_create_info_struct failed");
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // In case of an error, libpng will longjmp back, so setjmp here.
  if (setjmp(png_jmpbuf(png_ptr)))
  {
    png_destroy_write_struct(&png_ptr, &info_ptr);
    fclose(file);

    throw DLM_RUNTIME_ERROR("WritePNG: libpng initialization error");
  }

  png_init_io(png_ptr, file);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  int const bit_depth = 8 * sizeof(T);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  int const colorTypes[] =
  {
    PNG_COLOR_TYPE_GRAY,
    PNG_COLOR_TYPE_GRAY_ALPHA,
    PNG_COLOR_TYPE_RGB,
    PNG_COLOR_TYPE_RGB_ALPHA
  };

  int const color_type = colorTypes[data.channels - 1];

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  png_set_IHDR(png_ptr, info_ptr,
    data.width, data.height,
    bit_depth, color_type,
    PNG_INTERLACE_NONE,
    PNG_COMPRESSION_TYPE_BASE,
    PNG_FILTER_TYPE_BASE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  png_write_info(png_ptr, info_ptr);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (bit_depth == 16 && IsLittleEndian())
  {
    png_set_swap(png_ptr);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Write file data.
  std::unique_ptr<png_bytep[]> row_pointers(new png_bytep[data.height]);

  for (png_uint_32 i = 0; i < data.height; ++i)
  {
    png_uint_32 const offset = i * data.width * data.channels;

    row_pointers[i] = reinterpret_cast<png_bytep>(data.data.get() + offset);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (setjmp(png_jmpbuf(png_ptr)))
  {
    png_destroy_write_struct(&png_ptr, &info_ptr);
    fclose(file);

    throw DLM_RUNTIME_ERROR("WritePNG: libpng writing error");
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  png_write_image(png_ptr, row_pointers.get());

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //clean up
  png_write_end(png_ptr, 0);
  png_destroy_write_struct(&png_ptr, &info_ptr);

  fclose(file);
}

//------------------------------------------------------------------------------
template <typename T, dlm::uint32 CHANNELS, typename COLOR> 
dlmImage::Image<COLOR> ReadPNG(std::string const& filename)
{
  DataExchangeStructure<T> data = ReadPNG<T>(filename);

  DLM_ASSERT(data.channels == CHANNELS, DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  dlmImage::Image<COLOR> image(data.width, data.height);

  for (dlm::uint32 i = 0; i < data.width; ++i)
  {
    for (dlm::uint32 j = 0; j < data.height; ++j)
    {
      dlm::uint64 const offset = (j * data.width + i) * CHANNELS;

      COLOR& color = image(i, j);

      for (dlm::uint32 c = 0; c < CHANNELS; ++c)
      {
        color.At(c) = data.data[offset + c];
      }
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return image;
}

//------------------------------------------------------------------------------
template <typename T, dlm::uint32 CHANNELS, typename COLOR>
void WritePNG(
  dlmImage::Image<COLOR> const& image,
  std::string            const& filename)
{
  DLM_ASSERT(image.Width () < dlm::UInt32Max, DLM_RUNTIME_ERROR);
  DLM_ASSERT(image.Height() < dlm::UInt32Max, DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DataExchangeStructure<T> data;

  data.width    = static_cast<png_uint_32>(image.Width ());
  data.height   = static_cast<png_uint_32>(image.Height());
  data.channels = CHANNELS;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  dlm::uint64 const elements = image.ArraySize() * CHANNELS;

  data.data = std::move(std::unique_ptr<T[]>(new T[elements]));

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (png_uint_32 i = 0; i < data.width; ++i)
  {
    for (png_uint_32 j = 0; j < data.height; ++j)
    {
      dlm::uint64 const offset = (j * data.width + i) * CHANNELS;

      COLOR const& color = image(i, j);

      for (dlm::uint32 c = 0; c < CHANNELS; ++c)
      {
        data.data[offset + c] = color.At(c);
      }
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  WritePNG(data, filename);
}

//==============================================================================
// Read
//------------------------------------------------------------------------------
template <typename T>
dlmImage::Image<T> Read(std::string const& filename);

//------------------------------------------------------------------------------
template <>
inline dlmImage::Image<dlmImage::Gray8U> Read(std::string const& filename)
{
  return ReadPNG<png_byte, 1, dlmImage::Gray8U>(filename);
}

//------------------------------------------------------------------------------
template <>
inline dlmImage::Image<dlmImage::Gray16U> Read(std::string const& filename)
{
  return ReadPNG<png_uint_16, 1, dlmImage::Gray16U>(filename);
}

//------------------------------------------------------------------------------
template <>
inline dlmImage::Image<dlmImage::GrayAlpha8U> Read(std::string const& filename)
{
  return ReadPNG<png_byte, 2, dlmImage::GrayAlpha8U>(filename);
}

//------------------------------------------------------------------------------
template <>
inline dlmImage::Image<dlmImage::GrayAlpha16U> Read(std::string const& filename)
{
  return ReadPNG<png_uint_16, 2, dlmImage::GrayAlpha16U>(filename);
}

//------------------------------------------------------------------------------
template <>
inline dlmImage::Image<dlmImage::RGB8U> Read(std::string const& filename)
{
  return ReadPNG<png_byte, 3, dlmImage::RGB8U>(filename);
}

//------------------------------------------------------------------------------
template <>
inline dlmImage::Image<dlmImage::RGB16U> Read(std::string const& filename)
{
  return ReadPNG<png_uint_16, 3, dlmImage::RGB16U>(filename);
}

//------------------------------------------------------------------------------
template <>
inline dlmImage::Image<dlmImage::RGBA8U> Read(std::string const& filename)
{
  return ReadPNG<png_byte, 4, dlmImage::RGBA8U>(filename);
}

//------------------------------------------------------------------------------
template <>
inline dlmImage::Image<dlmImage::RGBA16U> Read(std::string const& filename)
{
  return ReadPNG<png_uint_16, 4, dlmImage::RGBA16U>(filename);
}

//------------------------------------------------------------------------------
template <typename T>
void Write(dlmImage::Image<T> const& image, std::string const& filename);

//------------------------------------------------------------------------------
template <>
inline void Write(
  dlmImage::Image<dlmImage::Gray8U> const& image, std::string const& filename)
{
  WritePNG<png_byte, 1, dlmImage::Gray8U>(image, filename);
}

//------------------------------------------------------------------------------
template <>
inline void Write(
  dlmImage::Image<dlmImage::Gray16U> const& image, std::string const& filename)
{
  WritePNG<png_uint_16, 1, dlmImage::Gray16U>(image, filename);
}

//------------------------------------------------------------------------------
template <>
inline void Write(
  dlmImage::Image<dlmImage::GrayAlpha8U> const& image, std::string const& filename)
{
  WritePNG<png_byte, 2, dlmImage::GrayAlpha8U>(image, filename);
}

//------------------------------------------------------------------------------
template <>
inline void Write(
  dlmImage::Image<dlmImage::GrayAlpha16U> const& image, std::string const& filename)
{
  WritePNG<png_uint_16, 2, dlmImage::GrayAlpha16U>(image, filename);
}

//------------------------------------------------------------------------------
template <>
inline void Write(
  dlmImage::Image<dlmImage::RGB8U> const& image, std::string const& filename)
{
  WritePNG<png_byte, 3, dlmImage::RGB8U>(image, filename);
}

//------------------------------------------------------------------------------
template <>
inline void Write(
  dlmImage::Image<dlmImage::RGB16U> const& image, std::string const& filename)
{
  WritePNG<png_uint_16, 3, dlmImage::RGB16U>(image, filename);
}

//------------------------------------------------------------------------------
template <>
inline void Write(
  dlmImage::Image<dlmImage::RGBA8U> const& image, std::string const& filename)
{
  WritePNG<png_byte, 4, dlmImage::RGBA8U>(image, filename);
}

//------------------------------------------------------------------------------
template <>
inline void Write(
  dlmImage::Image<dlmImage::RGBA16U> const& image, std::string const& filename)
{
  WritePNG<png_uint_16, 4, dlmImage::RGBA16U>(image, filename);
}

//==============================================================================
// Implementation namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
template <typename T>
dlmImage::Image<T> PNGAssistant::Load(std::string const& filename)
{
  // To do template specialization, there has to be an additional layer of
  // indirection, because template specializations cannot go into class scope.
  return Implementation::Read<T>(filename);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
template <typename T>
void PNGAssistant::Store(
  dlmImage::Image<T> const& image, std::string const& filename)
{
  // To do template specialization, there has to be an additional layer of
  // indirection, because template specializations cannot go into class scope.
  Implementation::Write(image, filename);
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
