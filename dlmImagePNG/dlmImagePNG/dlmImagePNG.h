//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmImagePNG library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmImagePNG_dlmImagePNG_h_
#define dlmImagePNG_dlmImagePNG_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmImagePNG/PNGAssistant.h"
#include "dlmImagePNG/PNGAssistant.inline.h"

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
