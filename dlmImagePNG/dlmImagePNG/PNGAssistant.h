//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmImagePNG library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmImagePNG_PNGAssistant_h_
#define dlmImagePNG_PNGAssistant_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmImage/dlmImage.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <string>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmImagePNG
{

//==============================================================================
// PNGAssistant class
//------------------------------------------------------------------------------
class PNGAssistant
{
public:
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  template <typename T>
  static dlmImage::Image<T> Load(std::string const& filename);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <typename T>
  static void Store(dlmImage::Image<T> const& image, std::string const& filename);
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
