//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmViewer_GLHelper_inline_h_
#define dlmViewer_GLHelper_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/GLHelper.h"

//------------------------------------------------------------------------------
#include "dlm/entity/Camera.inline.h"
#include "dlm/entity/Image.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Matrix.inline.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
template <typename T>
inline void GLHelper::UniformMatrix2fv(
  GLint const location, dlm::Matrix<2, 2, T> const& matrix)
{
  GLfloat fArray[4] = { static_cast<GLfloat>(0.0) };

  // The array assignment handles the transposition into the column-major format
  // that's used by OpenGL.
  fArray[ 0] = static_cast<GLfloat>(matrix(0, 0));
  fArray[ 1] = static_cast<GLfloat>(matrix(1, 0));
  fArray[ 2] = static_cast<GLfloat>(matrix(0, 1));
  fArray[ 3] = static_cast<GLfloat>(matrix(1, 1));

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  gl::UniformMatrix2fv(location, 1, GL_FALSE, fArray);
}

//------------------------------------------------------------------------------
template <typename T>
inline void GLHelper::UniformMatrix4fv(
  GLint const location, dlm::Matrix<4, 4, T> const& matrix)
{
  GLfloat fArray[16] = { static_cast<GLfloat>(0.0) };
  
  // The array assignment handles the transposition into the column-major format
  // that's used by OpenGL.
  fArray[ 0] = static_cast<GLfloat>(matrix(0, 0));
  fArray[ 1] = static_cast<GLfloat>(matrix(1, 0));
  fArray[ 2] = static_cast<GLfloat>(matrix(2, 0));
  fArray[ 3] = static_cast<GLfloat>(matrix(3, 0));
  fArray[ 4] = static_cast<GLfloat>(matrix(0, 1));
  fArray[ 5] = static_cast<GLfloat>(matrix(1, 1));
  fArray[ 6] = static_cast<GLfloat>(matrix(2, 1));
  fArray[ 7] = static_cast<GLfloat>(matrix(3, 1));
  fArray[ 8] = static_cast<GLfloat>(matrix(0, 2));
  fArray[ 9] = static_cast<GLfloat>(matrix(1, 2));
  fArray[10] = static_cast<GLfloat>(matrix(2, 2));
  fArray[11] = static_cast<GLfloat>(matrix(3, 2));
  fArray[12] = static_cast<GLfloat>(matrix(0, 3));
  fArray[13] = static_cast<GLfloat>(matrix(1, 3));
  fArray[14] = static_cast<GLfloat>(matrix(2, 3));
  fArray[15] = static_cast<GLfloat>(matrix(3, 3));

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  gl::UniformMatrix4fv(location, 1, GL_FALSE, fArray);
}

//------------------------------------------------------------------------------
template <typename T>
inline dlm::Matrix<4, 4, T> GLHelper::Perspective(
  T const aspectRatio, T const f, T const zNear, T const zFar)
{
  T const one = static_cast<T>(1.0);
  T const two = static_cast<T>(2.0);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  dlm::Matrix<4, 4, T> projection;

  projection(0, 0) =  f / aspectRatio * zNear;
  projection(1, 1) =  f               * zNear;
  projection(2, 2) = -(zFar + zNear)     / (zFar - zNear);
  projection(2, 3) = -two * zFar * zNear / (zFar - zNear);
  projection(3, 2) = -one;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return projection;
}

//------------------------------------------------------------------------------
template <typename T>
inline dlm::Matrix<4, 4, T> GLHelper::Ortho(
  T const aspectRatio,
  T const f,
  T const zNear,
  T const zFar,
  T const distance)
{
  T const N1 = static_cast<T>(1.0);
  T const N2 = static_cast<T>(2.0);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  dlm::Matrix<4, 4, T> projection;

  projection(0, 0) =  f / aspectRatio * zNear / distance;
  projection(1, 1) =  f               * zNear / distance;
  projection(2, 2) = -N2             / (zFar - zNear);
  projection(2, 3) = -(zFar + zNear) / (zFar - zNear);
  projection(3, 3) =  N1;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return projection;
}

//------------------------------------------------------------------------------
template <typename T>
inline dlm::Matrix<4, 4, T> GLHelper::Frustum(
  dlm::Camera const& camera,
  dlm::Image  const& image,
  T           const  windowAspectRatio,
  T           const  zNear,
  T           const  zFar)
{
  T const imageAspectRatio =
    static_cast<T>(image.Size().X()) / static_cast<T>(image.Size().Y());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T width  = static_cast<T>(0.0);
  T height = static_cast<T>(0.0);

  if (imageAspectRatio < windowAspectRatio)
  {
    width  = static_cast<T>(image.Size().Y()) * windowAspectRatio;
    height = static_cast<T>(image.Size().Y());
  }
  else
  {
    width  = static_cast<T>(image.Size().X());
    height = static_cast<T>(image.Size().X()) / windowAspectRatio;
  }

  width  /= camera.FocalLength();
  height /= camera.FocalLength();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T const xOffset = (camera.PrincipalPoint().X() - image.Size().X() / 2) / camera.FocalLength();
  T const yOffset = (camera.PrincipalPoint().Y() - image.Size().Y() / 2) / camera.FocalLength();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T const half = static_cast<T>(0.5);

  T const left   = (-half * width  - xOffset) * zNear;
  T const right  = ( half * width  - xOffset) * zNear;
  T const bottom = (-half * height + yOffset) * zNear;
  T const top    = ( half * height + yOffset) * zNear;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T const N1 = static_cast<T>(1.0);
  T const N2 = static_cast<T>(2.0);

  dlm::Matrix4f projection;

  projection(0, 0) =   N2 * zNear        / (right - left  );
  projection(1, 1) =   N2 * zNear        / (top   - bottom);
  projection(0, 2) =  (right + left  )   / (right - left  );
  projection(1, 2) =  (top   + bottom)   / (top   - bottom);
  projection(2, 2) = -(zFar  + zNear )   / (zFar  - zNear );
  projection(3, 2) = - N1;
  projection(2, 3) = - N2 * zFar * zNear / (zFar  - zNear );

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return projection;
}

//------------------------------------------------------------------------------
template <typename T>
inline void GLHelper::CompensateOrientation(dlm::Matrix<4, 4, T>& matrix)
{
  matrix(0, 1) *= -1.0;
  matrix(0, 2) *= -1.0;
  matrix(1, 1) *= -1.0;
  matrix(1, 2) *= -1.0;
  matrix(2, 1) *= -1.0;
  matrix(2, 2) *= -1.0;
  matrix(3, 1) *= -1.0;
  matrix(3, 2) *= -1.0;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
