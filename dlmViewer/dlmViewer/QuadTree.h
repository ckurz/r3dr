//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmViewer_QuadTree_h_
#define dlmViewer_QuadTree_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/AxisAlignedBoundingRectangle.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/ArrayObjectTable.h"
#include "dlm/core/Types.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <vector>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// QuadTree class
//------------------------------------------------------------------------------
class QuadTree
{
public:
  //============================================================================
  // Constructors
  //----------------------------------------------------------------------------
  QuadTree();

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  void Setup(AABR           const& area,
             dlm::uint64    const  depth          = 10,
             dlm::uint64    const  cellSize       = 10,
             dlm::FloatType const  overlapFactor  = 2.0);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Initialize(dlm::ArrayObjectTable<AABR> const& data);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool Validate(bool const verbose = false);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  std::vector<dlm::uint64> Query(dlm::Vector2f const& point) const;
  std::vector<dlm::uint64> Query(AABR          const& range) const;

private:
  //============================================================================
  // Nested
  //----------------------------------------------------------------------------
  typedef dlm::Vector<2, dlm::uint64> LocCode;

  //----------------------------------------------------------------------------
  struct Cell
  {
  public:
    //==========================================================================
    // Constructors
    //--------------------------------------------------------------------------
    Cell(dlm::uint64 const  parent    = 0,
         dlm::uint64 const  level     = 0,
         LocCode     const& locCode   = LocCode(),
         AABR        const& area      = AABR(),
         AABR        const& looseArea = AABR());

    //==========================================================================
    // Member variables
    //--------------------------------------------------------------------------
    AABR      area;
    AABR looseArea;

    std::vector<dlm::uint64>      elements;
    std::vector<dlm::uint64> looseElements;

    LocCode locCode;

    dlm::uint64 children[4]; //0 if there are none
    dlm::uint64 parent;      //QTParentId if root

    dlm::uint64 level;  //level in the hierarchy; lowest is 0
  };

  //----------------------------------------------------------------------------
  typedef dlm::ArrayObjectTable<Cell> CellStorage;
  typedef dlm::ArrayObjectTable<AABR> DataStorage;

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  void BuildRoot();

  void SetupLocCoords();

  LocCode  GetLocCode(dlm::Vector2f const& pos) const;

  dlm::uint64 GetLowestLevel(dlm::Vector2f const& size) const;

  dlm::uint64 GetLeafCell(
    dlm::Vector2f const& loc, dlm::uint64 const constlowestLevel = 0) const;

  void RefineCell(dlm::uint64 const cellId,  dlm::uint64 const cellSize);

  void SubdivideCell(dlm::uint64 cellId);

  bool ValidateCells(bool const verbose);
  bool ValidateData (bool const verbose);

  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  CellStorage cells_;
  DataStorage data_;

  std::vector<dlm::uint64> cellIndices_;

  std::vector<dlm::Vector2f> maxLengths_;

  AABR      area_;
  AABR looseArea_;

  dlm::Vector2f locCodeConversion_;

  dlm::FloatType overlapFactor_;

  dlm::uint64 depth_;
  dlm::uint64 cellSize_;

  bool initialized_;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
