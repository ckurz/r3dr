//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmViewer_AxisAlignedBoundingRectangleAssistant_inline_h_
#define dlmViewer_AxisAlignedBoundingRectangleAssistant_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/AxisAlignedBoundingRectangleAssistant.h"

//------------------------------------------------------------------------------
#include "dlmViewer/AxisAlignedBoundingRectangle.inline.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
template <typename T>
inline AxisAlignedBoundingRectangle<T>&
  AxisAlignedBoundingRectangleAssistant::Expand(
  AxisAlignedBoundingRectangle<T>& aabr, T const factor)
{
  dlm::Vector<2, T> const center    = aabr.Center   ();
  dlm::Vector<2, T>       dimension = aabr.Dimension();

  dimension /= static_cast<T>(2.0);
  dimension *= factor;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  aabr.LowerLeft () = center - dimension;
  aabr.UpperRight() = center + dimension;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return aabr;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
