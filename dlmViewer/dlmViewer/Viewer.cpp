//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/Viewer.h"

//------------------------------------------------------------------------------
//#include "dlmViewer/Mouse.h"
//#include "dlmViewer/CameraControl.h"
//#include "dlmViewer/WindowConfiguration.h"
//#include "dlmViewer/TextureStorage.h"
//#include "dlmViewer/ShaderStorage.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//#include "GLSubsystem/Receiver.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//#include "GLSubsystem/Event.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//#include "dlm/math/Vector.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlmViewer/GLHelper.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "GLWrapper/GLWrapper.h"

#include "dlm/timer/Timer.h"

#include <iostream>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Global Viewer object
//------------------------------------------------------------------------------
Viewer viewer;

//==============================================================================
// Constructor
//------------------------------------------------------------------------------
Viewer::Viewer()
  : scenePointer_      (nullptr    )
  , mouseFocus_        (nullptr    )
  , viewmode_          (VIEWMODE_3D)
  , transition_        (NONE       )
  , transitionTime_    (0          )
  , transitionDuration_(600        )
  , modifier_          (false      )
  , running_           (true       )
  , hasTexture_        (false      )
  , converted_         (false      )
  , drawCameraView_    (false      )
  , boxSize_(8.0, 4.0, 2.0)
{}

//==============================================================================
// Accessors
//------------------------------------------------------------------------------
CameraControl const& Viewer::GetCameraControl() const
{
  return cameraControl_;
}

//------------------------------------------------------------------------------
WindowConfiguration const& Viewer::GetWindowConfiguration() const
{
  return wConfig_;
}

//------------------------------------------------------------------------------
ShaderStorage const& Viewer::GetShaderStorage() const
{
  return shaderStorage_;
}

//------------------------------------------------------------------------------
TextureStorage const& Viewer::GetTextureStorage() const
{
  return textureStorage_;
}

//------------------------------------------------------------------------------
Mouse const& Viewer::GetMouse() const
{
  return mouse_;
}

//------------------------------------------------------------------------------
bool Viewer::Running() const
{
  return running_;
}

//==============================================================================
// Mutators
//------------------------------------------------------------------------------
CameraControl& Viewer::GetCameraControl()
{
  return cameraControl_;
}

//------------------------------------------------------------------------------
WindowConfiguration& Viewer::GetWindowConfiguration()
{
  return wConfig_;
}

//------------------------------------------------------------------------------
ShaderStorage& Viewer::GetShaderStorage()
{
  return shaderStorage_;
}

//------------------------------------------------------------------------------
TextureStorage& Viewer::GetTextureStorage()
{
  return textureStorage_;
}

//------------------------------------------------------------------------------
Mouse& Viewer::GetMouse()
{
  return mouse_;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
void Viewer::GrabMouse(GLSubsystem::Receiver* entity)
{
  if (entity == nullptr)
  {
    throw("entity is 0");
  }

  if (mouseFocus_ != nullptr)
  {
    throw("viewer already has mouse focus entity");
  }

  mouseFocus_ = entity;
}

//------------------------------------------------------------------------------
void Viewer::ReleaseMouse(GLSubsystem::Receiver* entity)
{
  if (entity == nullptr)
  {
    throw("entity is 0");
  }

  if (mouseFocus_ == nullptr)
  {
    throw("viewer has no mouse focus entity");
  }

  mouseFocus_ = nullptr;
  
  GLSubsystem::Event inputEvent;

  inputEvent.type = GLSubsystem::Event::MouseEvent;

  inputEvent.control = false;
  inputEvent.shift   = false;

  inputEvent.Mouse.type = GLSubsystem::Event::MouseEventType::Move;

  inputEvent.Mouse.xPosition = static_cast<int>(                            mouse_.Position().X());
  inputEvent.Mouse.yPosition = static_cast<int>(wConfig_.WindowSize().Y() - mouse_.Position().Y());
  
  inputEvent.Mouse.wheelMotion = 0;

  inputEvent.Mouse.buttonStates = 0;

  OnEvent(inputEvent);
}

//------------------------------------------------------------------------------
void Viewer::InitializeGL()
{
  glShadeModel(GL_SMOOTH);
  //glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  //glReadBuffer(GL_BACK);
  //glDrawBuffer(GL_BACK);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);
  //glDepthMask(TRUE);
  //glDisable(GL_STENCIL_TEST);
  //glStencilMask(0xFFFFFFFF);
  //glStencilFunc(GL_EQUAL, 0x00000000, 0x00000001);
  //glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
  //glFrontFace(GL_CCW);
  //glCullFace(GL_BACK);
  //glEnable(GL_CULL_FACE);
  glClearColor(1.0, 1.0, 1.0, 0.0);
  glClearDepth(1.0);
  //glClearStencil(0);
  //glDisable(GL_BLEND);
  //glDisable(GL_ALPHA_TEST);
  //glDisable(GL_DITHER);
  //glActiveTexture(GL_TEXTURE0);
  //glEnable(GL_MULTISAMPLE_ARB); //irrelevant

//  glPointSize(2);
}

//------------------------------------------------------------------------------
void Viewer::Create()
{
  circle_.Create();
  letterbox_.Create();
  hexagonalGrid_.Create();
  coordinateSystem_.Create();

  simpleObjectPointRenderer_.Create();
  multiCameraRenderer_.Create();
  localizationRenderer_.Create();

  boxRenderer_.Create();

  imageRenderer_.Create();

  cuboidRenderer_.Create();

  scene_.Create();

  cameraControl_.Create();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  coordinateSystem_.SetArrowLength(12.0);
  coordinateSystem_.SetArrowBaseLength(8.0);

  coordinateSystem_.SetArrowWidth(-3.0);
  coordinateSystem_.SetArrowBaseWidth(-1.5);

  coordinateSystem_.SetCut(false);

//  coordinateSystem_.SetX0Offset(dlm::Vector3f(-50.0, -60.0, 0.0));
//  coordinateSystem_.SetY0Offset(dlm::Vector3f(-60.0, -50.0, 0.0));

  coordinateSystem_.XColor() = dlm::Vector3f(0.4, 0.4, 0.4);
  coordinateSystem_.YColor() = dlm::Vector3f(0.4, 0.4, 0.4);
  
  coordinateSystem_.DrawX1() = false;
  coordinateSystem_.DrawY1() = false;
  coordinateSystem_.DrawZ0() = false;
  coordinateSystem_.DrawZ1() = false;
}

//------------------------------------------------------------------------------
void Viewer::Delete()
{
  circle_.Delete();
  letterbox_.Delete();
  hexagonalGrid_.Delete();
  coordinateSystem_.Delete();

  simpleObjectPointRenderer_.Delete();
  multiCameraRenderer_.Delete();
  localizationRenderer_.Delete();

  boxRenderer_.Delete();

  imageRenderer_.Delete();

  cuboidRenderer_.Delete();

  scene_.Delete();

  cameraControl_.Delete();

  shaderStorage_.Delete();
}

//------------------------------------------------------------------------------
void Viewer::Update(dlm::uint64 const delta)
{
  if (hasTexture_)
  {
    texture_.Delete();

    image_.Flip();
  
    texture_.Create(
      static_cast<GLsizei>(image_.Width()),
      static_cast<GLsizei>(image_.Height()),
      reinterpret_cast<GLubyte*>(image_.Data()),
      true);

    wConfig_.SetImageSize(dlm::Vector2u(
      static_cast<unsigned>(image_.Width()),
      static_cast<unsigned>(image_.Height())));

    imageRenderer_.SetArea(dlm::Vector2d(), dlm::Vector2d(
      static_cast<double>(image_.Width()),
      static_cast<double>(image_.Height())));

    converted_ = true;

    hasTexture_ = false;
  }

  circle_.Update(delta);
  letterbox_.Update(delta);
  hexagonalGrid_.Update(delta);
  coordinateSystem_.Update(static_cast<unsigned>(delta));

  simpleObjectPointRenderer_.Update(delta);
  multiCameraRenderer_.Update(delta);
  localizationRenderer_.Update(delta);

  boxRenderer_.Update(delta);

  imageRenderer_.Update(delta);

  cuboidRenderer_.Update(delta);

  scene_.Update(delta);

  cameraControl_.Update(delta);


  if (transition_ != NONE)
  {
    transitionTime_ += static_cast<unsigned>(delta);

    if (transitionTime_ >= transitionDuration_)
    {
      if (transition_ == FROM_2D_TO_3D)
      {
        viewmode_ = VIEWMODE_3D;
      }

      if (transition_ == FROM_3D_TO_2D)
      {
        viewmode_ = VIEWMODE_2D;
      }

      transitionTime_ = 0;

      transition_ = NONE;
    }
  }
//  wConfig_.Update(delta);
}

void Viewer::SetTexture(dlmImage::ImageRGB8U const& image)
{
  image_ = image;

  hasTexture_ = true;
}

//------------------------------------------------------------------------------
void Viewer::Draw(dlm::Matrix4f const& matrix) const
{
  dlm::Vector2u const& windowSize = wConfig_.WindowSize();

  glViewport(0, 0, windowSize.X(), windowSize.Y());

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

  if (drawCameraView_ && converted_)
  {
    texture_.Bind();

    glDepthMask(GL_FALSE);

    imageRenderer_.Draw(wConfig_.ImageProjection());

    glDepthMask(GL_TRUE);

    texture_.Unbind();
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Draw the scene.
//  if (viewmode_   == VIEWMODE_3D ||
//      transition_ != NONE)
//  {
////    workbench_.Draw3D(cameraControl_.ModelviewProjection());
//  }
//  else //(viewmode_ == VIEWMODE_2D)
//  {
////    workbench_.Draw2D(wConfig_.ImageProjection());
//    //workbench_.Draw3D(cameraControl_.ModelviewProjection());
//  }

  scene_.Draw(cameraControl_.ModelviewProjection());
//  scene_.Draw(cameraControl_.Projection());

  simpleObjectPointRenderer_.Draw(cameraControl_.ModelviewProjection());

  if (!drawCameraView_)
  {
    multiCameraRenderer_.Draw(cameraControl_.ModelviewProjection());
    localizationRenderer_.Draw(cameraControl_.ModelviewProjection());
  }

//  boxRenderer_.Draw(matrix);

  //if (!drawCameraView_)
  {
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Draw additional stuff including eye candy.
//  circle_.Draw(matrix);

  // HexagonalGrid and CameraControl have some z-fighting problems, because the
  // FocusPointRenderer draws in the xy-plane. The stencil buffer is used to
  // sort it out.
  hexagonalGrid_.Draw(matrix);
  cameraControl_.Draw(matrix);
  // They should therefore not be separated.

  cameraControl_.Draw2D(wConfig_.WindowProjection());

//  scene_.Draw2D(wConfig_.ImageProjection());

  coordinateSystem_.Draw(matrix);

  cuboidRenderer_.Draw(cameraControl_.ModelviewProjection());
  }

//  wConfig_.Draw(wConfig_.ImageProjection());
}

//------------------------------------------------------------------------------
void Viewer::Set(dlm::Scene const& scene, dlm::IRepId const current)
{
  dlm::Timer timer;

  timer.Tick();

  scene_.Set(scene, current);

  timer.Tick();

//  std::cout << "Time for construction: " << timer.Delta() << " [ms]"  << std::endl;

  wConfig_.SetImageSize(scene.Get(current).Size());

  scenePointer_ = &scene;
}

//------------------------------------------------------------------------------
void Viewer::Set(dlm::Scene const& scene, std::vector<int> const& closest)
{
  scene_.Set(scene, closest);
}

//------------------------------------------------------------------------------
void
Viewer::SetMasks(
  std::vector<std::tuple<
    dlm::Transformation,
    dlm::Vector3f,
    dlm::Vector4f>> const& masks)
{
  cuboidRenderer_.Set(masks);
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
