//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmViewer_Mouse_h_
#define dlmViewer_Mouse_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "GLSubsystem/Receiver.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "GLSubsystem/Event.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Mouse class
//------------------------------------------------------------------------------
class Mouse : public GLSubsystem::Receiver
{
public:
  //============================================================================
  // Constructors
  //----------------------------------------------------------------------------
  Mouse();

  //============================================================================
  // Accessors
  //----------------------------------------------------------------------------
  dlm::Vector2f const& Position() const;
  dlm::Vector2f const& Previous() const;
  dlm::Vector2f const& Offset  () const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  dlm::Vector2f const& LDragStart() const;
  dlm::Vector2f const& RDragStart() const;
  dlm::Vector2f const& MDragStart() const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool LButtonDown() const;
  bool RButtonDown() const;
  bool MButtonDown() const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool LDragged() const;
  bool RDragged() const;
  bool MDragged() const;

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  GLSubsystem::Receiver* OnEvent(GLSubsystem::Event const& inputEvent);

private:
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  void SetWindowSize(int const width, int const height);

  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  dlm::Vector2f position_;
  dlm::Vector2f previous_;
  dlm::Vector2f offset_;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  dlm::Vector2f windowSize_;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  dlm::Vector2f lDragStart_;
  dlm::Vector2f rDragStart_;
  dlm::Vector2f mDragStart_;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool lButtonDown_;
  bool rButtonDown_;
  bool mButtonDown_;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool lDragged_;
  bool rDragged_;
  bool mDragged_;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
