//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmViewer_CameraControl_h_
#define dlmViewer_CameraControl_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "GLSubsystem/Receiver.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "GLSubsystem/Event.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlmViewer/Renderer.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlmViewer/renderer/PivotPointRenderer.h"
#include "dlmViewer/renderer/FocusPointRenderer.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/Scene.h" // Yeah, I'm being lazy...
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.h"
#include "dlm/math/Matrix.h"
#include "dlm/math/Quaternion.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <vector>
//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// CameraControl
//------------------------------------------------------------------------------
class CameraControl : public GLSubsystem::Receiver, public Renderer
{
public:
  //============================================================================
  // Nested
  //----------------------------------------------------------------------------
  enum ViewMode
  {
    FREE   = 0,
    STATIC = 1,
    CUSTOM = 2
  };

  enum TransitionMode
  {
    NONE             = 0,
    FREE_TO_FREE     = 1,
    FREE_TO_STATIC   = 2,
    FREE_TO_CUSTOM   = 3,
    STATIC_TO_FREE   = 4,
    STATIC_TO_STATIC = 5,
    STATIC_TO_CUSTOM = 6,
    CUSTOM_TO_FREE   = 7,
    CUSTOM_TO_STATIC = 8,
    CUSTOM_TO_CUSTOM = 9
  };

  //============================================================================
  // Constructor
  //----------------------------------------------------------------------------
  CameraControl();

  //============================================================================
  // Accessors
  //----------------------------------------------------------------------------
  dlm::Matrix4f Projection() const;
  dlm::Matrix4f Modelview() const;
  dlm::Matrix4f ModelviewProjection() const;

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  GLSubsystem::Receiver* OnEvent(GLSubsystem::Event const& inputEvent);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Create();
  void Delete();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Update(dlm::uint64 const delta);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Draw  (dlm::Matrix4f const& matrix) const;
  void Draw2D(dlm::Matrix4f const& matrix) const;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //dlm::Matrix4f CustomProjection() const;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void   Lock();
  void Unlock();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool Locked() const;

  void SetCustom(
    dlm::Image          const& image,
    dlm::Camera         const& camera,
    dlm::Transformation const& t);

  void ClearCustom();

  bool ToCustom();
  bool ToFree  ();

  void Invalidate();
  void UpdateMatrices();
  void UpdateProjectionMatrices();
  void UpdatePivotPosition();
  void UpdateCustomConfiguration();

//private:
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  void PrepareTransitionToFree  ();
  void PrepareTransitionToStatic(dlm::uint32 const index);
  void PrepareTransitionToCustom();

  void InitiateTransition();

  dlm::Matrix4d BuildModelviewMatrix () const;
  dlm::Matrix4d BuildProjectionMatrix() const;

  void ApplyExtrinsics (dlm::Matrix4d& matrix) const;
  void ApplyFocusOffset(dlm::Matrix4d& matrix) const;

  GLSubsystem::Receiver* HandleKeyPress  (GLSubsystem::Event const& inputEvent);
  GLSubsystem::Receiver* HandleKeyRelease(GLSubsystem::Event const& inputEvent);
  GLSubsystem::Receiver* HandleMouse     (GLSubsystem::Event const& inputEvent);
  GLSubsystem::Receiver* HandleMouseMove (GLSubsystem::Event const& inputEvent);

  void RotateView (dlm::Vector2f offset);
  void RotateScene(dlm::Vector2f offset);

  void MoveFocus(dlm::Vector2f offset);
  void MoveScene(dlm::Vector2f offset);
  void MovePivot(dlm::Vector2f offset);

  void UpdatePivotSelection(dlm::Vector2f const& mousePosition);

  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  struct Configuration
  {
    dlm::Vector3f focus;
    dlm::Quatd view;
    dlm::Matrix4f projection;
  };

  Configuration current_;
  Configuration target_;

  Configuration free_;
  Configuration static_[3];

  Configuration custom_;

  PivotPointRenderer ppRenderer_;
  FocusPointRenderer fpRenderer_;

  dlm::Image          customImage_;
  dlm::Camera         customCamera_;
  dlm::Transformation customT_;

  dlm::Vector3f scenePivot_;

  dlm::Vector3f sceneTranslation_;
  dlm::Quatd    sceneRotation_;

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //output matrices
  dlm::Matrix4f projection_;
  dlm::Matrix4f modelview_;
  dlm::Matrix4f modelviewProjection_;

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  dlm::Vector3f cameraAngles_; 

  dlm::Vector2f pivotPosition_;
  dlm::Vector2f lastMousePosition_;

  dlm::Vector2f windowSize_;

  dlm::FloatType aspectRatio_;

  dlm::FloatType cameraDistance_;

  dlm::FloatType zNear_;
  dlm::FloatType zFar_;

  dlm::FloatType f_;

        ViewMode       viewMode_;
  TransitionMode transitionMode_;

  dlm::uint32 currentIndex_;
  dlm::uint32  targetIndex_;

  dlm::uint32 transitionTime_;
  dlm::uint32 transitionDuration_;

  dlm::uint32 locked_;

  bool overPivot_;
  bool movePivot_;

  bool move_;
  bool modifier_;

  bool inTransition_;
  bool valid_;

  bool hasCustom_;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
