//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmViewer_InterpolationAssistant_inline_h_
#define dlmViewer_InterpolationAssistant_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/InterpolationAssistant.h"

//------------------------------------------------------------------------------
#include "dlm/math/Vector.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <cmath>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
template <typename T>
inline dlm::FloatType InterpolationAssistant::MapLinear(
  T const value, T const high, T const low)
{
  DLM_ASSERT(static_cast<dlm::FloatType>(high - low) > dlm::Epsilon, DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (value >= high) { return static_cast<dlm::FloatType>(1.0); }
  if (value <= low ) { return static_cast<dlm::FloatType>(0.0); }

  return static_cast<dlm::FloatType>(value - low) /
         static_cast<dlm::FloatType>(high  - low);
}

//------------------------------------------------------------------------------
template <typename T>
inline dlm::FloatType InterpolationAssistant::H01(
  T const value, T const high, T const low)
{
  dlm::FloatType const t  = MapLinear(value, high, low);
  dlm::FloatType const t2 = t * t;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return t2 * (3 - 2 * t);
}

//------------------------------------------------------------------------------
template <typename T, typename U>
inline dlm::Vector<3, T> Slerp(
  dlm::Vector<3, T> const& a,
  dlm::Vector<3, T> const& b,
  U const value,
  U const high = static_cast<U>(1.0),
  U const low  = static_cast<U>(0.0))
{
  dlm::FloatType const t = MapLinear(value, high, low);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  dlm::FloatType const cosOmega = a.Dot(b);
  dlm::FloatType const    omega = acos(cosOmega);
  dlm::FloatType const sinOmega =  sin(   omega);

  //if (sinOmega == 0.0)
  //{
  //  return a * (1.0 - t) + b * t;
  //}

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  dlm::FloatType const N1 = static_cast<dlm::FloatType>(1.0);

  dlm::Vector<3, T> result = a * (sin((N1 - t) * omega) / sinOmega);
  result                  += b * (sin(      t  * omega) / sinOmega);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
