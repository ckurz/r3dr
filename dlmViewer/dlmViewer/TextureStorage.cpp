//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/TextureStorage.h"

//------------------------------------------------------------------------------
#include "dlmImagePNG/dlmImagePNG.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlmImage/dlmImage.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/ArrayObjectTable.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//#include <iostream>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
TextureStorage::TextureStorage()
  : totalMemoryAllowed_(10 * 1024 * 1024)
  , totalMemoryUsed_   (0                )
{}

//==============================================================================
// Destructor
//------------------------------------------------------------------------------
TextureStorage::~TextureStorage()
{
  for (dlm::uint64 i = 0; i < data_.Range(); ++i)
  {
    if (data_.Active(i))
    {
      data_.At(i).texture.Delete();
    }
  }
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
TextureId TextureStorage::AddTexture(TextureInformation const& textureInfo)
{
  auto it = lookupTable_.find(textureInfo.reference);

  if (it != lookupTable_.end())
  {
    return it->second;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Data data;

  data.texInfo = textureInfo;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  TextureId const textureId = TextureId(data_.PushBack(data));

  lookupTable_[textureInfo.reference] = textureId;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return textureId;
}

//------------------------------------------------------------------------------
void TextureStorage::RemoveTexture(std::string const& reference)
{
  auto it = lookupTable_.find(reference);

  if (it == lookupTable_.end())
  {
    throw("TextureStorage::GetTexture: texture not found");
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return RemoveTexture(it->second);
}

//------------------------------------------------------------------------------
void TextureStorage::RemoveTexture(TextureId const textureId)
{
  bool found = false;

  for (auto it = lookupTable_.begin(); it != lookupTable_.end(); ++it)
  {
    if (it->second == textureId)
    {
      lookupTable_.erase(it);
      
      found = true;
      break;
    }
  }

  if (!found)
  {
    throw("TextureStorage::RemoveTexture: texture not found");
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (data_.At(textureId.Get()).present)
  {
    UnloadTexture(textureId);
  }
}

//------------------------------------------------------------------------------
TextureId TextureStorage::GetTextureId(std::string const& reference)
{
  auto it = lookupTable_.find(reference);

  if (it == lookupTable_.end())
  {
    throw("TextureStorage::GetTextureIndex: texture not found");
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return it->second;
}

//------------------------------------------------------------------------------
GLWrapper::GLTexture const& TextureStorage::GetTexture(
  std::string const& reference)
{
  auto it = lookupTable_.find(reference);

  if (it == lookupTable_.end())
  {
    throw("TextureStorage::GetTexture: texture not found");
  }

  return GetTexture(it->second);
}

//------------------------------------------------------------------------------
GLWrapper::GLTexture const& TextureStorage::GetTexture(
  TextureId const textureId)
{
  if (!data_.At(textureId.Get()).present)
  {
    LoadTexture(textureId);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Data& data = data_.At(textureId.Get());
  
  data.ticksNotUsed = 0;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return data.texture;
}

//------------------------------------------------------------------------------
void TextureStorage::LoadTexture(TextureId const textureId)
{
  Data& data = data_.At(textureId.Get());

  DLM_ASSERT(!data.present, DLM_RUNTIME_ERROR);

//std::cout << "Loading texture " << data.texInfo.filename << std::endl;

  dlmImage::ImageRGB8U image =
    dlmImagePNG::PNGAssistant::Load<dlmImage::RGB8U>(data.texInfo.filename);

  image.Flip();
  
  data.texture.Create(
    static_cast<GLsizei>(image.Width()),
    static_cast<GLsizei>(image.Height()),
    reinterpret_cast<GLubyte*>(image.Data()),
    true);

  data.memorySize = image.Width() * image.Height() * 3;

  data.present = true;
  data.ticksNotUsed = 0;

  totalMemoryUsed_ += data.memorySize;

//std::cout << totalMemoryUsed_ << " of " << totalMemoryAllowed_ << std::endl;
}

//------------------------------------------------------------------------------
void TextureStorage::UnloadTexture(TextureId const textureId)
{
  Data& data = data_.At(textureId.Get());

//std::cout << "Unloading texture " << data.texInfo.filename << std::endl;

  data.texture.Delete();

  data.present = false;

  totalMemoryUsed_ -= data.memorySize;

  data.memorySize = 0;

//std::cout << data.memorySize << std::endl;
//std::cout << totalMemoryUsed_ << " of " << totalMemoryAllowed_ << std::endl;
}

//------------------------------------------------------------------------------
void TextureStorage::Increment()
{
  // Update tick counters.
  for (dlm::uint64 i = 0; i < data_.Range(); ++i)
  {
    if (data_.Active(i))
    {
      data_.At(i).ticksNotUsed += 1;
    }
  }
}

//------------------------------------------------------------------------------
void TextureStorage::Process()
{
  while (totalMemoryUsed_ > totalMemoryAllowed_)
  {
    dlm::uint64 highestTickCount = 0;
    dlm::uint64 index = 0;

    for (dlm::uint64 i = 0; i < data_.Range(); ++i)
    {
      if (!data_.Active(i)) { continue; }

      Data& data = data_.At(i);

      if (data.present && !data.texInfo.persistent)
      {
        if (data.ticksNotUsed > highestTickCount)
        {
          highestTickCount = data.ticksNotUsed;
          index = i;
        }
      }
    }

    if (highestTickCount == 0)
    {
      throw "Unable to free texture memory: All textures in use!";
    }
    
    UnloadTexture(TextureId(index));
  }
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
