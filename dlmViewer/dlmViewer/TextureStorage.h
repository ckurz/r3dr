//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmViewer_TextureStorage_h_
#define dlmViewer_TextureStorage_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "GLWrapper/GLTexture.h"
#include "GLWrapper/GLWrapper.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Types.h"
#include "dlm/core/Identifier.h"
#include "dlm/core/Identifier.inline.h"
#include "dlm/core/ArrayObjectTable.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <vector>
#include <string>
#include <map>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Shorthand typedefs
//------------------------------------------------------------------------------
typedef dlm::Identifier<GLWrapper::GLTexture> TextureId;

//==============================================================================
// TextureStorage class
//------------------------------------------------------------------------------
class TextureStorage
{
public:
  //============================================================================
  // Nested
  //----------------------------------------------------------------------------
  struct TextureInformation
  {
    TextureInformation()
      : persistent(false)
    {}

    std::string filename;
    std::string reference;

    bool persistent;
  };

  //============================================================================
  // Constructors
  //----------------------------------------------------------------------------
  TextureStorage();

  //============================================================================
  // Destructor
  //----------------------------------------------------------------------------
  ~TextureStorage();

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  TextureId AddTexture(TextureInformation const& textureInfo);

  void RemoveTexture(std::string const& reference);
  void RemoveTexture(TextureId   const  reference);

  TextureId GetTextureId(std::string const& reference);

  GLWrapper::GLTexture const& GetTexture(std::string const& reference);
  GLWrapper::GLTexture const& GetTexture(TextureId   const  reference);

  void Increment();
  void Process();

private:
  //============================================================================
  // Nested
  //----------------------------------------------------------------------------
  struct Data
  {
    Data()
      : memorySize  (0    )
      , ticksNotUsed(0    )
      , present     (false)
    {}

    TextureInformation texInfo;

    GLWrapper::GLTexture texture;

    dlm::uint64 memorySize;

    dlm::uint64 ticksNotUsed;

    bool present;
  };

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  void   LoadTexture(TextureId const textureId);
  void UnloadTexture(TextureId const textureId);

  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  dlm::ArrayObjectTable<Data> data_;
  std::map<std::string, TextureId> lookupTable_;

  dlm::uint64 totalMemoryAllowed_;
  dlm::uint64 totalMemoryUsed_;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
