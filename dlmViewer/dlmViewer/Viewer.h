//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmViewer_Viewer_h_
#define dlmViewer_Viewer_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/renderer/CircleRenderer.h"
#include "dlmViewer/renderer/HexagonalGridRenderer.h"
#include "dlmViewer/renderer/CoordinateSystemRenderer.h"
#include "dlmViewer/renderer/LetterboxRenderer.h"
#include "dlmViewer/renderer/SceneRenderer.h"
#include "dlmViewer/renderer/SimpleObjectPointRenderer.h"
#include "dlmViewer/renderer/MultiCameraRenderer.h"
#include "dlmViewer/renderer/LocalizationRenderer.h"
#include "dlmViewer/renderer/BoxRenderer.h"
#include "dlmViewer/renderer/CuboidRenderer.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlmViewer/Mouse.h"
#include "dlmViewer/CameraControl.h"
#include "dlmViewer/WindowConfiguration.h"
#include "dlmViewer/TextureStorage.h"
#include "dlmViewer/ShaderStorage.h"
#include "dlmViewer/Renderer.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlmImage/dlmImage.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "GLSubsystem/Receiver.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "GLSubsystem/Event.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/Scene.h"

#include <iostream>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Global Viewer object
//------------------------------------------------------------------------------
class Viewer;
extern Viewer viewer;

//==============================================================================
// Viewer
//------------------------------------------------------------------------------
class Viewer : public GLSubsystem::Receiver, public Renderer
{
public:
  //============================================================================
  // Nested
  //----------------------------------------------------------------------------
  enum Viewmode
  {
    VIEWMODE_2D,
    VIEWMODE_3D
  };

  enum Transition
  {
    NONE = 0x0,
    FROM_2D_TO_3D,
    FROM_3D_TO_2D
  };

  //============================================================================
  // Constructor
  //----------------------------------------------------------------------------
  Viewer();

  //============================================================================
  // Accessors
  //----------------------------------------------------------------------------
  CameraControl       const& GetCameraControl      () const;
  WindowConfiguration const& GetWindowConfiguration() const;
  ShaderStorage       const& GetShaderStorage      () const;
  TextureStorage      const& GetTextureStorage     () const;
  Mouse               const& GetMouse              () const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool Running() const;

  //============================================================================
  // Mutators
  //----------------------------------------------------------------------------
  CameraControl      & GetCameraControl      ();
  WindowConfiguration& GetWindowConfiguration();
  ShaderStorage      & GetShaderStorage      ();
  TextureStorage     & GetTextureStorage     ();
  Mouse              & GetMouse              ();

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  GLSubsystem::Receiver* OnEvent(GLSubsystem::Event const& inputEvent);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void    GrabMouse(GLSubsystem::Receiver* entity);
  void ReleaseMouse(GLSubsystem::Receiver* entity);

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  void InitializeGL();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Create();
  void Delete();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Update(dlm::uint64 const delta);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Draw(dlm::Matrix4f const& matrix) const;

  void Set(dlm::Scene const& scene, dlm::IRepId const current = dlm::IRepId(0));
  void Set(dlm::Scene const& scene, std::vector<int> const& closest);

  void Set(std::vector<dlm::Vector3d> const& points)
  {
    std::cout << "Expecting crash..." << std::endl;

    simpleObjectPointRenderer_.Set(points);

    std::cout << "Apparently not." << std::endl;
  }

  void Set(dlm::Scene const& scene, std::vector<std::pair<dlm::IRepId, dlm::Vector4f>> const& selection, std::vector<dlm::Transformation> const& path)
  {
    multiCameraRenderer_.Set(scene, selection, path);
  }

  void Set(
    dlm::Vector2u const& imageSize, 
    dlm::Transformation const& transformation,
    dlm::Camera const& camera,
    std::vector<dlm::Vector3f> const& points)
  {
    localizationRenderer_.Set(imageSize, transformation, camera, points);
  }
  
  void SetSelected(std::set<dlm::FTrkId> const& selected)
  {
    scene_.SetSelected(selected);
  }

  void SetTexture(dlmImage::ImageRGB8U const& image);
  
  void
  SetMasks(
    std::vector<std::tuple<
      dlm::Transformation,
      dlm::Vector3f,
      dlm::Vector4f>> const& masks);

//private:
  //============================================================================
  // Member variables - Viewer components
  //----------------------------------------------------------------------------
  CircleRenderer circle_;
  LetterboxRenderer letterbox_;
  HexagonalGridRenderer hexagonalGrid_;
  CoordinateSystemRenderer coordinateSystem_;

  SimpleObjectPointRenderer simpleObjectPointRenderer_;
  MultiCameraRenderer multiCameraRenderer_;
  LocalizationRenderer localizationRenderer_;

  BoxRenderer boxRenderer_;

  ImageRenderer imageRenderer_;
  GLWrapper::GLTexture texture_;

  CuboidRenderer cuboidRenderer_;

  SceneRenderer scene_;

  dlm::Scene const* scenePointer_;
  
  Mouse mouse_;

  ShaderStorage   shaderStorage_;
  TextureStorage textureStorage_;

  CameraControl       cameraControl_;
  WindowConfiguration wConfig_;

  GLSubsystem::Receiver* mouseFocus_;

  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  Viewmode   viewmode_;
  Transition transition_;

  unsigned transitionTime_;
  unsigned transitionDuration_;

  bool modifier_;

  bool running_;

  bool hasTexture_;
  bool converted_;

  bool drawCameraView_;

  dlmImage::ImageRGB8U image_;

  dlm::Vector3f boxSize_;

  //============================================================================
  // Disabled
  //----------------------------------------------------------------------------
  Viewer           (Viewer const& other);
  Viewer& operator=(Viewer const& other);
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
