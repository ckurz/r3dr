//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/Viewer.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// GLSubsystem::Receiver interface - OnEvent function
//------------------------------------------------------------------------------
GLSubsystem::Receiver* Viewer::OnEvent(GLSubsystem::Event const& inputEvent)
{
  if (inputEvent.type == GLSubsystem::Event::ResizeEvent)
  {
    // Resize events are simply propagated. Receivers are not supposed to return
    // anything but zero regardless of what they do with the event.
            mouse_.OnEvent(inputEvent);
    cameraControl_.OnEvent(inputEvent);
          wConfig_.OnEvent(inputEvent);

    return 0; // The subsystem won't check.
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (inputEvent.type == GLSubsystem::Event::KeyboardEvent)
  {
    if (inputEvent.Keyboard.keyCode == GLSubsystem::KeyCode::KEY_Shift_L)
    {
      modifier_ = inputEvent.Keyboard.type == GLSubsystem::Event::Press;
    }

    // Keyboard events are first handed to the camera control. If they are not
    // consumed, the can be handled by the viewer.
    if (cameraControl_.OnEvent(inputEvent) != nullptr)
    {
      return 0; // The subsystem won't check.
    }

    // The keyboard event is ours.
    if (inputEvent.Keyboard.type == GLSubsystem::Event::KeyboardEventType::Press)
    {
      if (inputEvent.Keyboard.keyCode == GLSubsystem::KeyCode::KEY_Escape)
      {
        // We've got an Esc that nobody else wants to use -> quit.
        running_ = false;
        
        return 0; // The subsystem won't check.
      }

      if (inputEvent.Keyboard.keyCode == GLSubsystem::KeyCode::KEY_X ||
          inputEvent.Keyboard.keyCode == GLSubsystem::KeyCode::KEY_x)
      {
        if (scenePointer_ != nullptr)
        {
          cameraControl_.SetCustom(
            scenePointer_->Get(dlm::IRepId(0)),
            scenePointer_->Get(dlm::CRepId(0)),
            scenePointer_->Get(dlm::TRepId(0)));
        }

        cameraControl_.ToCustom();
        cameraControl_.Update(1000);

        drawCameraView_ = true;
      }

      if (inputEvent.Keyboard.keyCode == GLSubsystem::KeyCode::KEY_Z ||
          inputEvent.Keyboard.keyCode == GLSubsystem::KeyCode::KEY_z)
      {
        cameraControl_.ToFree();
        cameraControl_.Update(1000);

        drawCameraView_ = false;
      }

      if (inputEvent.Keyboard.keyCode == GLSubsystem::KeyCode::KEY_1)
      {
        boxSize_ *= 0.9;

        boxRenderer_.Set(boxSize_);
      }

      if (inputEvent.Keyboard.keyCode == GLSubsystem::KeyCode::KEY_2)
      {
        boxSize_ /= 0.9;

        boxRenderer_.Set(boxSize_);
      }
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (inputEvent.type == GLSubsystem::Event::MouseEvent)
  {
    mouse_.OnEvent(inputEvent); // The mouse state is updated first.

    // Hand the event to the Receiver that currently has the mouse focus.
    if (mouseFocus_!= nullptr)
    {
      // The return value is not checked because a Receiver will exclusively
      // receive all mouse events (with the exception of the mouse) until it
      // calls ReleaseMouse().
      mouseFocus_->OnEvent(inputEvent);
      
      return 0; // The subsystem won't check.
    }

    // Hand the event to the camera control.
    if (cameraControl_.OnEvent(inputEvent))
    {
      return 0; // The subsystem won't check.
    }

    // The mouse event is ours.
    if (inputEvent.Mouse.type == GLSubsystem::Event::MouseEventType::Wheel)
    {
      if (inputEvent.Mouse.wheelMotion > 0)
      {
        scene_.Next();
      }
      else if (inputEvent.Mouse.wheelMotion < 0)
      {
        scene_.Previous();
      }
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return 0; // The subsystem won't check.
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//if (inputEvent.Keyboard.keyCode == KEY_T ||
//    inputEvent.Keyboard.keyCode == KEY_t)
//{
//  if (transition_ != NONE)
//  {
//    return 0;
//  }

//  if (viewmode_ == VIEWMODE_2D)
//  {
//    transition_ = FROM_2D_TO_3D;
//    cameraControl_.ToFree();

//    workbench_.Enter3D();

//    wConfig_.Deactivate();

//    Grid* gridRenderer = dynamic_cast<Grid*>(gridRenderer_);
//    gridRenderer->FadeIn();

//    CSRenderer* csRenderer =
//      dynamic_cast<CSRenderer*>(csRenderer_);
//    csRenderer->FadeIn();

//    CircleRenderer* circleRenderer =
//      dynamic_cast<CircleRenderer*>(circleRenderer_);
//    circleRenderer->FadeIn();
//  }

//  if (viewmode_ == VIEWMODE_3D)
//  {
//    transition_ = FROM_3D_TO_2D;
//    cameraControl_.ToCustom();

//    wConfig_.Activate();
//    Grid* gridRenderer = dynamic_cast<Grid*>(gridRenderer_);
//    //gridRenderer->FadeOut();

//    CSRenderer* csRenderer =
//      dynamic_cast<CSRenderer*>(csRenderer_);
//    //csRenderer->FadeOut();

//    CircleRenderer* circleRenderer =
//      dynamic_cast<CircleRenderer*>(circleRenderer_);
//    //circleRenderer->FadeOut();
//  }

//  return 0;
//}

//if (inputEvent.Keyboard.keyCode == KEY_V ||
//    inputEvent.Keyboard.keyCode == KEY_v)
//{
//  workbench_.NextImage();
//}

//if (inputEvent.Keyboard.keyCode == KEY_U ||
//    inputEvent.Keyboard.keyCode == KEY_u)
//{
//  //workbench_.EstimateTransformation();
//  //workbench_.EstimatePlane();
//  //workbench_.WriteSelection();
//}
