//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/renderer/CameraRenderer.h"

//------------------------------------------------------------------------------
#include "dlmViewer/Viewer.h"
#include "dlmViewer/GLHelper.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/SceneAssistant.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Constructor
//------------------------------------------------------------------------------
CameraRenderer::CameraRenderer()
  : shaderIndex_(0    )
  , projection_ (0    )
  , color_      (0    )
  , valid_      (false)
{}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
void CameraRenderer::Create()
{
  Delete();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  {
    ShaderStorage::ShaderInformation shaderInfo;
    shaderInfo.vertShader = "shader/draw3D.vert";
    shaderInfo.fragShader = "shader/draw3D.frag";
    shaderInfo.reference  = "draw3D";
    shaderInfo.attribLocations.push_back(
      ShaderStorage::AttribLocationInfo(0, "InVertex"));

    shaderIndex_ = viewer.GetShaderStorage().AddShader(shaderInfo);
  }

  GLWrapper::GLProgram const& program =
    viewer.GetShaderStorage().GetShader(shaderIndex_);

  projection_ = program.GetUniformLocation("ProjectionModelviewMatrix");
  color_      = program.GetUniformLocation("DrawColor");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  vArray_.Create();
  vBuffer_.Create();

  vBuffer_.Data(16 * sizeof(dlm::Vector3f), vertices_);

  vArray_.Attach(vBuffer_, 0, 3, GL_DOUBLE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  valid_ = true;
}

//------------------------------------------------------------------------------
void CameraRenderer::Delete()
{
  vArray_.Delete();
  vBuffer_.Delete();

  shaderIndex_ = 0;

  projection_ = 0;
  color_      = 0;
}


//------------------------------------------------------------------------------
void CameraRenderer::Update(dlm::uint64 const)
{
  if (!valid_)
  {
    vBuffer_.SubData(16 * sizeof(dlm::Vector3f), vertices_);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    valid_ = true;
  }
}

//------------------------------------------------------------------------------
void CameraRenderer::Draw(dlm::Matrix4f const& matrix) const
{
  viewer.GetShaderStorage().GetShader(shaderIndex_).Use();

  glh::UniformMatrix4fv(projection_, matrix);

  gl::Uniform4f(color_, 0.5f, 0.5f, 0.5f, 1.0f);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  vArray_.Bind();

  glDrawArrays(GL_LINES, 0, 16);
  
  vArray_.Unbind();
}

//------------------------------------------------------------------------------
void CameraRenderer::Set(dlm::Scene const& scene, dlm::IRepId const iRepId)
{
  valid_ = false;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  IRep const& iRep = scene.Get(iRepId);

//  CRep const& cRep = scene.Get(iRep.Reference<CRep>::Get());

  Transformation transformation = SA::Collapse(scene, iRep.Reference<TSeq>::Get());

  DLM_ASSERT(transformation.Invert(), DLM_RUNTIME_ERROR);

  transformation.Invert() = false;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//  dlm::FloatType const f = cRep.FocalLength();

//  dlm::FloatType const targetWidth  = iRep.Size().X() * iRep.PixelSize().X();
//  dlm::FloatType const targetHeight = iRep.Size().Y() * iRep.PixelSize().Y();
//
//  dlm::FloatType const hx = cRep.PPO().X();
//  dlm::FloatType const hy = cRep.PPO().Y();
//
////  const double left   = (-0.5 * targetWidth  - hx);// * near_scale;
////  const double right  = ( 0.5 * targetWidth  - hx);// * near_scale;
////  const double bottom = (-0.5 * targetHeight + hy);// * near_scale;
////  const double top    = ( 0.5 * targetHeight + hy);// * near_scale;
//  dlm::FloatType const left   = -static_cast<dlm::FloatType>(0.5) * targetWidth  - hx;
//  dlm::FloatType const right  =  static_cast<dlm::FloatType>(0.5) * targetWidth  - hx;
//  dlm::FloatType const bottom = -static_cast<dlm::FloatType>(0.5) * targetHeight + hy;
//  dlm::FloatType const top    =  static_cast<dlm::FloatType>(0.5) * targetHeight + hy;
//
//  dlm::Vector3f lowerLeft (left,  bottom, f);
//  dlm::Vector3f lowerRight(right, bottom, f);
//  dlm::Vector3f upperLeft (left,  top,    f);
//  dlm::Vector3f upperRight(right, top,    f);
//
//  lowerLeft  = transformation * lowerLeft;
//  lowerRight = transformation * lowerRight;
//  upperLeft  = transformation * upperLeft;
//  upperRight = transformation * upperRight;
//
//  dlm::Vector3f const& center = transformation.Translation();
//
//  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//  vertices_[ 0] = lowerLeft;
//  vertices_[ 1] = lowerRight;
//  
//  vertices_[ 2] = lowerRight;
//  vertices_[ 3] = upperRight;
//  
//  vertices_[ 4] = upperRight;
//  vertices_[ 5] = upperLeft;
//  
//  vertices_[ 6] = upperLeft;
//  vertices_[ 7] = lowerLeft;
//  
//  vertices_[ 8] = lowerLeft;
//  vertices_[ 9] = center;
//  
//  vertices_[10] = lowerRight;
//  vertices_[11] = center;
//  
//  vertices_[12] = upperLeft;
//  vertices_[13] = center;
//  
//  vertices_[14] = upperRight;
//  vertices_[15] = center;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
