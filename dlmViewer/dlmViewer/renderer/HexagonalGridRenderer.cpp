//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/renderer/HexagonalGridRenderer.h"

//------------------------------------------------------------------------------
#include "dlmViewer/Viewer.h"
#include "dlmViewer/GLHelper.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.h"
#include "dlm/math/Vector.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <vector>
#include <cmath>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
HexagonalGridRenderer::HexagonalGridRenderer(
  dlm::FloatType const edgeLength, dlm::uint64 const elements)
  : shaderIndex_(0    )
  , projection_ (0    )
  , color_      (0    )
  , valid_      (false)
{
  Set(edgeLength, elements);
}

//------------------------------------------------------------------------------
void HexagonalGridRenderer::Create()
{
  {
    dlmViewer::ShaderStorage::ShaderInformation shaderInfo;
    shaderInfo.vertShader = "shader/draw3D.vert";
    shaderInfo.fragShader = "shader/draw3D.frag";
    shaderInfo.reference  = "draw3D";
    shaderInfo.attribLocations.push_back(
      dlmViewer::ShaderStorage::AttribLocationInfo(0, "InVertex"));

    shaderIndex_ = dlmViewer::viewer.GetShaderStorage().AddShader(shaderInfo);
  }

  GLWrapper::GLProgram const& program =
    viewer.GetShaderStorage().GetShader(shaderIndex_);

  color_      = program.GetUniformLocation("DrawColor");
  projection_ = program.GetUniformLocation("ProjectionModelviewMatrix");

  vArray_.Create();
  vBuffer_.Create();
  iBuffer_.Create(GL_ELEMENT_ARRAY_BUFFER);

  vBuffer_.Data(points_.size() * sizeof(dlm::Vector3f), points_.data());

  vArray_.Attach(vBuffer_, 0, 3, GL_DOUBLE);

  iBuffer_.Bind();
  iBuffer_.Data(indices_.size() * sizeof(GLuint), indices_.data());
  iBuffer_.Unbind();

  valid_ = true;
}

//------------------------------------------------------------------------------
void HexagonalGridRenderer::Update(dlm::uint64 const delta)
{
  Fader::Update(delta);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (!valid_)
  {
    vBuffer_.Data( points_.size() * sizeof(dlm::Vector3f),  points_.data());
    iBuffer_.Data(indices_.size() * sizeof(GLuint),        indices_.data());

    valid_ = true;
  }
}

//------------------------------------------------------------------------------
void HexagonalGridRenderer::Delete()
{
  vArray_.Delete();
  vBuffer_.Delete();
  iBuffer_.Delete();

  shaderIndex_ = 0;

  projection_  = 0;
  color_       = 0;
}

//------------------------------------------------------------------------------
void HexagonalGridRenderer::Draw(const dlm::Matrix4d& matrix) const
{
  if (!IsVisible())
  {
    return;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  GLWrapper::GLProgram const& program =
    viewer.GetShaderStorage().GetShader(shaderIndex_);

  program.Use();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  glh::UniformMatrix4fv(projection_, matrix);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  gl::Uniform4f(color_, 0.75f, 0.75f, 0.75f, Alpha());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  vArray_.Bind();
  iBuffer_.Bind();
  
  //use the stencil buffer to get rid of z-fighting artifacts with the
  //FocusPointRenderer
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glEnable(GL_STENCIL_TEST);
  glStencilFunc(GL_ALWAYS, 1, 1);
  glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

  glDrawElements(GL_LINES, static_cast<GLsizei>(indices_.size()), GL_UNSIGNED_INT, 0);

  glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
  glDisable(GL_STENCIL_TEST);

  glDisable(GL_BLEND);

  iBuffer_.Unbind();
  vArray_.Unbind();
}

//------------------------------------------------------------------------------
void HexagonalGridRenderer::Set(
  dlm::FloatType const edgeLength, dlm::uint64 const elements)
{
  points_.clear();
  indices_.clear();

  valid_ = false;

  dlm::uint64 const segments = (1 + (elements - 3) / 2) * 3;

  dlm::Vector3d a;
  dlm::Vector3d b;

  a.X() = edgeLength / static_cast<dlm::FloatType>(segments);

  b.X() = a.X() * 0.5;
  b.Y() = b.X() * sqrt(3.0);

  dlm::Vector3d base(- edgeLength / 2.0, - edgeLength / 2.0, 0.0);
  dlm::uint64 counter = 0;

  {
    dlm::Vector3d tmpBase = base;

    for (dlm::uint64 i = 0; i < (elements - 1) / 2 + 1; ++i)
    {
      points_.push_back(tmpBase - b);
      points_.push_back(tmpBase - b + a);

      indices_.push_back(static_cast<unsigned>(counter++));
      indices_.push_back(static_cast<unsigned>(counter++));

      tmpBase += a * 3.0;
    }
  }

  dlm::uint64 previousIndexBase = 0;
  bool even = true;

  dlm::uint64 repetitions = static_cast<dlm::uint64>(edgeLength / b.Y());
  ++repetitions;

  if (repetitions & 0x00000001)
  {
    ++repetitions;
  }

  for (dlm::uint64 repCounter = 0; repCounter < repetitions; ++repCounter)
  {
    dlm::uint64 currentIndexBase = previousIndexBase;
    previousIndexBase = points_.size();

    dlm::Vector3d tmpBase = base;

    if (even)
    {
      for (dlm::uint64 i = 0; i < (elements - 1) / 2 + 1; ++i)
      {
        if (i)
        {
          indices_.push_back(static_cast<unsigned>(counter - 1));
          indices_.push_back(static_cast<unsigned>(counter    ));
        }

        points_.push_back(tmpBase - a);
        points_.push_back(tmpBase + a);

        indices_.push_back(static_cast<dlm::uint32>(currentIndexBase + i * 2));
        indices_.push_back(static_cast<dlm::uint32>(counter++));
        indices_.push_back(static_cast<dlm::uint32>(currentIndexBase + i * 2 + 1));
        indices_.push_back(static_cast<dlm::uint32>(counter++));

        tmpBase += a * 3.0;
      }

      base.Y() += 2.0 * b.Y();
    }
    else
    {
      for (dlm::uint64 i = 0; i < (elements - 1) / 2 + 1; ++i)
      {
        points_.push_back(tmpBase - b);
        points_.push_back(tmpBase - b + a);

        indices_.push_back(static_cast<dlm::uint32>(currentIndexBase + 2 * i));
        indices_.push_back(static_cast<dlm::uint32>(counter));

        indices_.push_back(static_cast<dlm::uint32>(counter++));
        indices_.push_back(static_cast<dlm::uint32>(counter));

        indices_.push_back(static_cast<dlm::uint32>(counter++));
        indices_.push_back(static_cast<dlm::uint32>(currentIndexBase + 2 * i + 1));

        tmpBase += a * 3.0;
      }
    }

    even = !even;
  }
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
