//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2014  TrackMen Ltd.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/renderer/MultiCameraRenderer.h"

//------------------------------------------------------------------------------
#include "dlmViewer/Viewer.h"
#include "dlmViewer/GLHelper.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/SceneAssistant.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"

#include "dlm/math/OStream.h"

#include <iostream>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Constructor
//------------------------------------------------------------------------------
MultiCameraRenderer::MultiCameraRenderer()
  : shaderIndex_(0    )
  , projection_ (0    )
  , valid_      (false)
{}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
void MultiCameraRenderer::Create()
{
  Delete();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  {
    ShaderStorage::ShaderInformation shaderInfo;
    shaderInfo.vertShader = "shader/Draw3DColorPerVertex.vert";
    shaderInfo.fragShader = "shader/Draw3DColorPerVertex.frag";
    shaderInfo.reference  = "Draw3DColorPerVertex";
    shaderInfo.attribLocations.push_back(
      ShaderStorage::AttribLocationInfo(0, "InVertex"));
    shaderInfo.attribLocations.push_back(
      ShaderStorage::AttribLocationInfo(1, "InColor"));

    shaderIndex_ = viewer.GetShaderStorage().AddShader(shaderInfo);
  }

  GLWrapper::GLProgram const& program =
    viewer.GetShaderStorage().GetShader(shaderIndex_);

  projection_ = program.GetUniformLocation("ModelviewProjectionMatrix");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  vArray_.Create();
  vBuffer_.Create();
  cBuffer_.Create();

  vArray_.Attach(vBuffer_, 0, 3, GL_DOUBLE);
  vArray_.Attach(cBuffer_, 1, 4, GL_DOUBLE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  valid_ = true;
}

//------------------------------------------------------------------------------
void MultiCameraRenderer::Delete()
{
  vArray_.Delete();
  vBuffer_.Delete();
  cBuffer_.Delete();

  shaderIndex_ = 0;

  projection_ = 0;
}


//------------------------------------------------------------------------------
void MultiCameraRenderer::Update(dlm::uint64 const)
{
  if (!valid_)
  {
    vBuffer_.Data(vertices_.size() * sizeof(dlm::Vector3f), vertices_.data());
    cBuffer_.Data(  colors_.size() * sizeof(dlm::Vector4f),   colors_.data());

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    valid_ = true;
  }
}

//------------------------------------------------------------------------------
void MultiCameraRenderer::Draw(dlm::Matrix4f const& matrix) const
{
  viewer.GetShaderStorage().GetShader(shaderIndex_).Use();

  glh::UniformMatrix4fv(projection_, matrix);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  vArray_.Bind();

  glDrawArrays(GL_LINES, 0, static_cast<GLsizei>(vertices_.size()));
  
  vArray_.Unbind();
}

//------------------------------------------------------------------------------
void MultiCameraRenderer::Set(
  dlm::Scene                                         const& scene,
  std::vector<std::pair<dlm::IRepId, dlm::Vector4f>> const  selection,
  std::vector<dlm::Transformation> const& path,
  dlm::FloatType const fScale,
  dlm::FloatType const sizeHint)
{
  valid_ = false;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  vertices_.clear();
    colors_.clear();

  vertices_.reserve(selection.size() * 16 + path.size() * 16);
    colors_.reserve(selection.size() * 16 + path.size() * 16);

  dlm::Image  image;
  dlm::Camera camera;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (auto const& element : selection)
  {
    colors_.resize(colors_.size() + 16, element.second);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    IRep const& iRep = scene.Get(element.first);
    CRep const& cRep = scene.Get(iRep.Reference<CRep>::Get());

    image  = iRep;
    camera = cRep;

    Transformation transformation =
      SA::Collapse(scene, iRep.Reference<TSeq>::Get());

    DLM_ASSERT(transformation.Invert(), DLM_RUNTIME_ERROR);
    
    transformation.Invert() = false;

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    FloatType const scale = sizeHint / iRep.Size().X() * fScale; 

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    FloatType const f   = cRep.FocalLength() * scale;
//    FloatType const par = cRep.PixelAspectRatio();
    //FloatType const s   = cRep.Skew();

    FloatType const aspectRatio = static_cast<FloatType>(iRep.Size().X()) / iRep.Size().Y();

    FloatType const targetWidth  = sizeHint;
    FloatType const targetHeight = sizeHint / aspectRatio;

    static FloatType const half = static_cast<FloatType>(0.5);

    FloatType const hx = (cRep.PrincipalPoint().X() - iRep.Size().X() * half) * scale;
    FloatType const hy = (cRep.PrincipalPoint().Y() - iRep.Size().Y() * half) * scale;

    FloatType const left   = hx - half * targetWidth;
    FloatType const right  = hx + half * targetWidth;
    FloatType const bottom = hy - half * targetHeight;
    FloatType const top    = hy + half * targetHeight;

    Vector3f lowerLeft (left,  bottom, f);
    Vector3f lowerRight(right, bottom, f);
    Vector3f upperLeft (left,  top,    f);
    Vector3f upperRight(right, top,    f);

    lowerLeft  = transformation * lowerLeft;
    lowerRight = transformation * lowerRight;
    upperLeft  = transformation * upperLeft;
    upperRight = transformation * upperRight;

    Vector3f const& center = transformation.Translation();

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    vertices_.push_back(lowerLeft);
    vertices_.push_back(lowerRight);
  
    vertices_.push_back(lowerRight);
    vertices_.push_back(upperRight);
  
    vertices_.push_back(upperRight);
    vertices_.push_back(upperLeft);
  
    vertices_.push_back(upperLeft);
    vertices_.push_back(lowerLeft);
  
    vertices_.push_back(lowerLeft);
    vertices_.push_back(center);
  
    vertices_.push_back(lowerRight);
    vertices_.push_back(center);
  
    vertices_.push_back(upperLeft);
    vertices_.push_back(center);
  
    vertices_.push_back(upperRight);
    vertices_.push_back(center);
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (auto const element : path)
  {
    colors_.resize(colors_.size() + 16, dlm::Vector4f(0.0, 1.0, 0.0, 1.0));

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Transformation transformation = element;

    DLM_ASSERT(transformation.Invert(), DLM_RUNTIME_ERROR);
    
    transformation.Invert() = false;

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    FloatType const scale = sizeHint / image.Size().X() * fScale; 

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    FloatType const f   = camera.FocalLength() * scale;
//    FloatType const par = camera.PixelAspectRatio();
    //FloatType const s   = cRep.Skew();

    FloatType const aspectRatio = static_cast<FloatType>(image.Size().X()) / image.Size().Y();

    FloatType const targetWidth  = sizeHint;
    FloatType const targetHeight = sizeHint / aspectRatio;

    static FloatType const half = static_cast<FloatType>(0.5);

    FloatType const hx = (camera.PrincipalPoint().X() - image.Size().X() * half) * scale;
    FloatType const hy = (camera.PrincipalPoint().Y() - image.Size().Y() * half) * scale;

    FloatType const left   = hx - half * targetWidth;
    FloatType const right  = hx + half * targetWidth;
    FloatType const bottom = hy - half * targetHeight;
    FloatType const top    = hy + half * targetHeight;

    Vector3f lowerLeft (left,  bottom, f);
    Vector3f lowerRight(right, bottom, f);
    Vector3f upperLeft (left,  top,    f);
    Vector3f upperRight(right, top,    f);

    lowerLeft  = transformation * lowerLeft;
    lowerRight = transformation * lowerRight;
    upperLeft  = transformation * upperLeft;
    upperRight = transformation * upperRight;

    Vector3f const& center = transformation.Translation();

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    vertices_.push_back(lowerLeft);
    vertices_.push_back(lowerRight);
  
    vertices_.push_back(lowerRight);
    vertices_.push_back(upperRight);
  
    vertices_.push_back(upperRight);
    vertices_.push_back(upperLeft);
  
    vertices_.push_back(upperLeft);
    vertices_.push_back(lowerLeft);
  
    vertices_.push_back(lowerLeft);
    vertices_.push_back(center);
  
    vertices_.push_back(lowerRight);
    vertices_.push_back(center);
  
    vertices_.push_back(upperLeft);
    vertices_.push_back(center);
  
    vertices_.push_back(upperRight);
    vertices_.push_back(center);
  }
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
