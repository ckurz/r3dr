//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/renderer/ImageSequence2DRenderer.h"

//------------------------------------------------------------------------------
#include "dlmViewer/Viewer.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/VectorFunctions.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
void ImageSequence2DRenderer::Create()
{
  Delete();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  renderer_.Create();
}

//------------------------------------------------------------------------------
void ImageSequence2DRenderer::Delete()
{
  renderer_.Delete();
}

//------------------------------------------------------------------------------
void ImageSequence2DRenderer::Update(dlm::uint64 const delta)
{
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  auto it = data_.find(current_);

  if (it != data_.cend())
  {
    if (renderSize_.X() != it->second.size.X() ||
        renderSize_.Y() != it->second.size.Y())
    {
      Vector2f const lowerLeft;
      Vector2f const upperRight = VF::Convert<dlm::FloatType>(it->second.size);

      renderer_.SetArea(lowerLeft, upperRight);
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  renderer_.Update(delta);
}

//------------------------------------------------------------------------------
void ImageSequence2DRenderer::Draw(dlm::Matrix4f const& matrix) const
{
  auto it = data_.find(current_);

  if (it != data_.cend())
  {
    viewer.GetTextureStorage().GetTexture(it->second.textureId).Bind();

    renderer_.Draw(matrix);
  }
}

//------------------------------------------------------------------------------
void ImageSequence2DRenderer::Set(
  dlm::Scene const& scene, dlm::IRepId const current)
{
  // TODO: Clean up old textures!

  using namespace dlm;

  for (dlm::uint64 i = 0; i < scene.IRepS().Range(); ++i)
  {
    IRepId const iRepId = IRepId(i);

    if (!scene.IsValid(iRepId)) { continue; }

    IRep const& iRep = scene.Get(iRepId);

    if (iRep.Filename().empty()) { continue; }

    Data data;

    data.filename = iRep.Filename();
    data.size     = iRep.Size();

    TextureStorage::TextureInformation texInfo;

    texInfo.filename  = iRep.Filename();
    texInfo.reference = iRep.Filename();

    data.textureId = viewer.GetTextureStorage().AddTexture(texInfo);

    data_[iRepId] = data;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  current_ = current;
}

//------------------------------------------------------------------------------
void ImageSequence2DRenderer::SetCurrent(dlm::IRepId const current)
{
  current_ = current;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
