//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/renderer/SimpleObjectPointRenderer.h"

//------------------------------------------------------------------------------
#include "dlmViewer/Viewer.h"
#include "dlmViewer/GLHelper.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/SceneAssistant.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.inline.h"
#include "dlm/math/Matrix.inline.h"


#include <iostream>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Constructor
//------------------------------------------------------------------------------
SimpleObjectPointRenderer::SimpleObjectPointRenderer()
  : color_      (0.0, 0.0, 1.0)
  , shaderIndex_(0            )
  , projection_ (0            )
  , valid_      (false        )
{}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
void SimpleObjectPointRenderer::Create()
{
  Delete();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  {
    ShaderStorage::ShaderInformation shaderInfo;
    shaderInfo.vertShader = "shader/op3D.vert";
    shaderInfo.fragShader = "shader/op3D.frag";
    shaderInfo.reference  = "op3D";
    shaderInfo.attribLocations.push_back(
      ShaderStorage::AttribLocationInfo(0, "InVertex"));
    shaderInfo.attribLocations.push_back(
      ShaderStorage::AttribLocationInfo(1, "InColor"));

    shaderIndex_ = viewer.GetShaderStorage().AddShader(shaderInfo);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  GLWrapper::GLProgram const& program =
    viewer.GetShaderStorage().GetShader(shaderIndex_);

  projection_ = program.GetUniformLocation("ProjectionModelviewMatrix");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  pointsVertexArray_.Create();

  pointsBuffer_.Create();
  colorsBuffer_.Create();

  pointsVertexArray_.Attach(pointsBuffer_, 0, 3, GL_DOUBLE);
  pointsVertexArray_.Attach(colorsBuffer_, 1, 3, GL_DOUBLE);
}

//------------------------------------------------------------------------------
void SimpleObjectPointRenderer::Delete()
{
  points_.clear();
  colors_.clear();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  pointsVertexArray_.Delete();

  pointsBuffer_.Delete();
  colorsBuffer_.Delete();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  shaderIndex_ = 0;

  projection_  = 0;
}

//------------------------------------------------------------------------------
void SimpleObjectPointRenderer::Update(dlm::uint64 const)
{
  if (!valid_)
  {
    pointsBuffer_.Data(points_.size() * sizeof(dlm::Vector3f), points_.data());
    colorsBuffer_.Data(colors_.size() * sizeof(dlm::Vector3f), colors_.data());

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    valid_ = true;
  }
}

//------------------------------------------------------------------------------
void SimpleObjectPointRenderer::Draw(dlm::Matrix4f const& matrix) const
{
  viewer.GetShaderStorage().GetShader(shaderIndex_).Use();

  glh::UniformMatrix4fv(projection_, matrix);

  glPointSize(2.0f);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  pointsVertexArray_.Bind();

  glDrawArrays(GL_POINTS, 0, static_cast<GLsizei>(points_.size()));

  pointsVertexArray_.Unbind();

  glPointSize(1.0f);
}

//------------------------------------------------------------------------------
void SimpleObjectPointRenderer::Set(std::vector<dlm::Vector3f> const& points)
{
  valid_ = false;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  colors_.clear();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  points_ = points;

  colors_.resize(points.size(), color_);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
