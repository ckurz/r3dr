//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/renderer/CoordinateSystemRenderer.h"

//------------------------------------------------------------------------------
//#include "GLWrapper/GLWrapper.h"
//#include "GLWrapper/GLVertexArray.h"
//#include "GLWrapper/GLBuffer.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlmViewer/Viewer.h"
#include "dlmViewer/GLHelper.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//#include "dlm/math/Vector.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//#include <vector>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//#include "CoordinateSystemRenderer.h"
//
//#include "../Viewer.h"
//
//#include "../glwrapper/GLHelper.h"
//
//======================================================================
// Constructor
//----------------------------------------------------------------------
CoordinateSystemRenderer::CoordinateSystemRenderer()
  : xColor_         (1.0, 0.0, 0.0),
    yColor_         (0.0, 1.0, 0.0),
    zColor_         (0.0, 0.0, 1.0),
    arrowLength_    (        100.0),
    arrowWidth_     (         20.0),
    arrowBaseLength_(         60.0),
    arrowBaseWidth_ (         10.0),
//    vArray_         (            0),
//    vBuffer_        (            0),
    shaderIndex_    (            0),
    projection_     (            0),
    color_          (            0),
    valid_          (        false),
    cut_            (         true),
    drawX0_         (         true),
    drawX1_         (         true),
    drawY0_         (         true),
    drawY1_         (         true),
    drawZ0_         (         true),
    drawZ1_         (         true)
{}

//======================================================================
// Accessors
//----------------------------------------------------------------------
const dlm::Vector3d& CoordinateSystemRenderer::XColor() const
{
  return xColor_;
}

//----------------------------------------------------------------------
const dlm::Vector3d& CoordinateSystemRenderer::YColor() const
{
  return yColor_;
}

//----------------------------------------------------------------------
const dlm::Vector3d& CoordinateSystemRenderer::ZColor() const
{
  return zColor_;
}

//----------------------------------------------------------------------
bool CoordinateSystemRenderer::DrawX0() const
{
  return drawX0_;
}

//----------------------------------------------------------------------
bool CoordinateSystemRenderer::DrawX1() const
{
  return drawX1_;
}

//----------------------------------------------------------------------
bool CoordinateSystemRenderer::DrawY0() const
{
  return drawY0_;
}

//----------------------------------------------------------------------
bool CoordinateSystemRenderer::DrawY1() const
{
  return drawY1_;
}

//----------------------------------------------------------------------
bool CoordinateSystemRenderer::DrawZ0() const
{
  return drawZ0_;
}

//----------------------------------------------------------------------
bool CoordinateSystemRenderer::DrawZ1() const
{
  return drawZ1_;
}

//======================================================================
// Mutators
//----------------------------------------------------------------------
dlm::Vector3d& CoordinateSystemRenderer::XColor()
{
  return xColor_;
}

//----------------------------------------------------------------------
dlm::Vector3d& CoordinateSystemRenderer::YColor()
{
  return yColor_;
}

//----------------------------------------------------------------------
dlm::Vector3d& CoordinateSystemRenderer::ZColor()
{
  return zColor_;
}

//----------------------------------------------------------------------
bool& CoordinateSystemRenderer::DrawX0()
{
  return drawX0_;
}

//----------------------------------------------------------------------
bool& CoordinateSystemRenderer::DrawX1()
{
  return drawX1_;
}

//----------------------------------------------------------------------
bool& CoordinateSystemRenderer::DrawY0()
{
  return drawY0_;
}

//----------------------------------------------------------------------
bool& CoordinateSystemRenderer::DrawY1()
{
  return drawY1_;
}

//----------------------------------------------------------------------
bool& CoordinateSystemRenderer::DrawZ0()
{
  return drawZ0_;
}

//----------------------------------------------------------------------
bool& CoordinateSystemRenderer::DrawZ1()
{
  return drawZ1_;
}

//======================================================================
// Member functions
//----------------------------------------------------------------------
void CoordinateSystemRenderer::Create()
{
  {
    dlmViewer::ShaderStorage::ShaderInformation shaderInfo;
    shaderInfo.vertShader = "shader/draw3D.vert";
    shaderInfo.fragShader = "shader/draw3D.frag";
    shaderInfo.reference  = "draw3D";
    shaderInfo.attribLocations.push_back(
      dlmViewer::ShaderStorage::AttribLocationInfo(0, "InVertex"));

    shaderIndex_ = dlmViewer::viewer.GetShaderStorage().AddShader(shaderInfo);
  }

  const GLWrapper::GLProgram& program =
    dlmViewer::viewer.GetShaderStorage().GetShader(shaderIndex_);

  projection_ = program.GetUniformLocation("ProjectionModelviewMatrix");
  color_      = program.GetUniformLocation("DrawColor");

  vBuffer_.Create();
  vBuffer_.Data(30 * 3 * sizeof(double), 0, GL_STATIC_DRAW);

  vArray_.Create();
  vArray_.Attach(vBuffer_, 0, 3, GL_DOUBLE);
}

//----------------------------------------------------------------------
void CoordinateSystemRenderer::Update(const unsigned)
{
//  Fader::Update(delta);

  if (valid_)
  {
    return;
  }

  UpdateArrows();
  valid_ = true;
}

//----------------------------------------------------------------------
void CoordinateSystemRenderer::Drop()
{
//  gl::DeleteVertexArrays(1, &vArray_ );
//  gl::DeleteBuffers     (1, &vBuffer_);
  vArray_.Delete();
  vBuffer_.Delete();

//  vArray_  = 0;
//  vBuffer_ = 0;

  //viewer.GetShaderStorage().RemoveShader(shaderIndex_);

  shaderIndex_  = 0;

  projection_ = 0;
  color_      = 0;

  valid_ = false;
}

//----------------------------------------------------------------------
void CoordinateSystemRenderer::Draw(const dlm::Matrix4d& matrix) const
{
  const GLWrapper::GLProgram& program =
    dlmViewer::viewer.GetShaderStorage().GetShader(shaderIndex_);

  program.Use();
  dlmViewer::GLHelper::UniformMatrix4fv(projection_, matrix);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

//  gl::BindVertexArray(vArray_);
  vArray_.Bind();

  gl::Uniform4f(color_, float(xColor_.X()), float(xColor_.Y()), float(xColor_.Z()), 1.0f);//float(Alpha()));
  if (drawX0_) { glDrawArrays(GL_TRIANGLE_STRIP,  0, 5); }
  if (drawX1_) { glDrawArrays(GL_TRIANGLE_STRIP,  5, 5); }

  gl::Uniform4f(color_, float(yColor_.X()), float(yColor_.Y()), float(yColor_.Z()), 1.0f);//float(Alpha()));
  if (drawY0_) { glDrawArrays(GL_TRIANGLE_STRIP, 10, 5); }
  if (drawY1_) { glDrawArrays(GL_TRIANGLE_STRIP, 15, 5); }

  gl::Uniform4f(color_, float(zColor_.X()), float(zColor_.Y()), float(zColor_.Z()), 1.0f);//float(Alpha()));
  if (drawZ0_) { glDrawArrays(GL_TRIANGLE_STRIP, 20, 5); }
  if (drawZ1_) { glDrawArrays(GL_TRIANGLE_STRIP, 25, 5); }

  vArray_.Unbind();
//  gl::BindVertexArray(0);

  glDisable(GL_BLEND);
}

//----------------------------------------------------------------------
void CoordinateSystemRenderer::UpdateArrows()
{
  using dlm::Vector3d;

  //arrow in x-direction, xy-plane
  vertices_[ 0] = Vector3d(0.0, arrowBaseWidth_, 0.0);
  vertices_[ 1] = Vector3d();
  vertices_[ 2] = Vector3d(arrowBaseLength_, arrowBaseWidth_, 0.0);
  vertices_[ 3] = Vector3d(arrowLength_, 0.0, 0.0);
  vertices_[ 4] = Vector3d(arrowBaseLength_, arrowWidth_, 0.0);

  //arrow in x-direction, xz-plane
  vertices_[ 5] = Vector3d(0.0, 0.0, arrowBaseWidth_);
  vertices_[ 6] = Vector3d();
  vertices_[ 7] = Vector3d(arrowBaseLength_, 0.0, arrowBaseWidth_);
  vertices_[ 8] = Vector3d(arrowLength_, 0.0, 0.0);
  vertices_[ 9] = Vector3d(arrowBaseLength_, 0.0, arrowWidth_);

  //arrow in y-direction, xy-plane
  vertices_[10] = Vector3d(arrowBaseWidth_, 0.0, 0.0);
  vertices_[11] = Vector3d();
  vertices_[12] = Vector3d(arrowBaseWidth_, arrowBaseLength_, 0.0);
  vertices_[13] = Vector3d(0.0, arrowLength_, 0.0);
  vertices_[14] = Vector3d(arrowWidth_, arrowBaseLength_, 0.0);

  //arrow in y-direction, yz-plane
  vertices_[15] = Vector3d(0.0, 0.0, arrowBaseWidth_);
  vertices_[16] = Vector3d();
  vertices_[17] = Vector3d(0.0, arrowBaseLength_, arrowBaseWidth_);
  vertices_[18] = Vector3d(0.0, arrowLength_, 0.0);
  vertices_[19] = Vector3d(0.0, arrowBaseLength_, arrowWidth_);

  //arrow in z-direction, xz-plane
  vertices_[20] = Vector3d(0.0, arrowBaseWidth_, 0.0);
  vertices_[21] = Vector3d();
  vertices_[22] = Vector3d(0.0, arrowBaseWidth_, arrowBaseLength_);
  vertices_[23] = Vector3d(0.0, 0.0, arrowLength_);
  vertices_[24] = Vector3d(0.0, arrowWidth_, arrowBaseLength_);

  //arrow in x-direction, yz-plane
  vertices_[25] = Vector3d(arrowBaseWidth_, 0.0, 0.0);
  vertices_[26] = Vector3d();
  vertices_[27] = Vector3d(arrowBaseWidth_, 0.0, arrowBaseLength_);
  vertices_[28] = Vector3d(0.0, 0.0, arrowLength_);
  vertices_[29] = Vector3d(arrowWidth_, 0.0, arrowBaseLength_);

  if (cut_)
  {
    vertices_[ 0].X() += arrowBaseWidth_;
    vertices_[ 5].X() += arrowBaseWidth_;
    vertices_[10].Y() += arrowBaseWidth_;
    vertices_[15].Y() += arrowBaseWidth_;
    vertices_[20].Z() += arrowBaseWidth_;
    vertices_[25].Z() += arrowBaseWidth_;
  }

  vertices_[ 0] += offsetX0_;
  vertices_[ 1] += offsetX0_;
  vertices_[ 2] += offsetX0_;
  vertices_[ 3] += offsetX0_;
  vertices_[ 4] += offsetX0_;

  vertices_[ 5] += offsetX1_;
  vertices_[ 6] += offsetX1_;
  vertices_[ 7] += offsetX1_;
  vertices_[ 8] += offsetX1_;
  vertices_[ 9] += offsetX1_;

  vertices_[10] += offsetY0_;
  vertices_[11] += offsetY0_;
  vertices_[12] += offsetY0_;
  vertices_[13] += offsetY0_;
  vertices_[14] += offsetY0_;

  vertices_[15] += offsetY1_;
  vertices_[16] += offsetY1_;
  vertices_[17] += offsetY1_;
  vertices_[18] += offsetY1_;
  vertices_[19] += offsetY1_;

  vertices_[20] += offsetZ0_;
  vertices_[21] += offsetZ0_;
  vertices_[22] += offsetZ0_;
  vertices_[23] += offsetZ0_;
  vertices_[24] += offsetZ0_;

  vertices_[25] += offsetZ1_;
  vertices_[26] += offsetZ1_;
  vertices_[27] += offsetZ1_;
  vertices_[28] += offsetZ1_;
  vertices_[29] += offsetZ1_;

  vBuffer_.SubData(30 * 3 * sizeof(double), vertices_);
}

//----------------------------------------------------------------------
void CoordinateSystemRenderer::SetArrowLength(const double& value)
{
  arrowLength_ = value;
  valid_       = false;
}

//----------------------------------------------------------------------
void CoordinateSystemRenderer::SetArrowWidth(const double& value)
{
  arrowWidth_ = value;
  valid_      = false;
}

//----------------------------------------------------------------------
void CoordinateSystemRenderer::SetArrowBaseLength(const double& value)
{
  arrowBaseLength_ = value;
  valid_           = false;
}

//----------------------------------------------------------------------
void CoordinateSystemRenderer::SetArrowBaseWidth(const double& value)
{
  arrowBaseWidth_ = value;
  valid_          = false;
}

//----------------------------------------------------------------------
void CoordinateSystemRenderer::SetCut(const bool value)
{
  cut_   = value;
  valid_ = false;
}

//----------------------------------------------------------------------
void CoordinateSystemRenderer::SetX0Offset(const dlm::Vector3d& offset)
{
  offsetX0_ = offset;
  valid_    = false;
}

//----------------------------------------------------------------------
void CoordinateSystemRenderer::SetX1Offset(const dlm::Vector3d& offset)
{
  offsetX1_ = offset;
  valid_    = false;
}

//----------------------------------------------------------------------
void CoordinateSystemRenderer::SetY0Offset(const dlm::Vector3d& offset)
{
  offsetY0_ = offset;
  valid_    = false;
}

//----------------------------------------------------------------------
void CoordinateSystemRenderer::SetY1Offset(const dlm::Vector3d& offset)
{
  offsetY1_ = offset;
  valid_    = false;
}

//----------------------------------------------------------------------
void CoordinateSystemRenderer::SetZ0Offset(const dlm::Vector3d& offset)
{
  offsetZ0_ = offset;
  valid_    = false;
}

//----------------------------------------------------------------------
void CoordinateSystemRenderer::SetZ1Offset(const dlm::Vector3d& offset)
{
  offsetZ1_ = offset;
  valid_    = false;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
