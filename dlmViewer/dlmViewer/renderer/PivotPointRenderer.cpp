//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/renderer/PivotPointRenderer.h"

//------------------------------------------------------------------------------
#include "dlmViewer/Viewer.h"
#include "dlmViewer/GLHelper.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.inline.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Constructor
//----------------------------------------------------------------------
PivotPointRenderer::PivotPointRenderer()
  : vertices_    (     )
  , texCoords_   (     )
  , alpha_       (     )
  , shaderIndex_ (0    )
  , size_        (8.0f )
  , angle_       (0.0f )
  , projection_  (0    )
  , color_       (0    )
//, valid_       (false)
{
  selectionFader_.Hide();

  texCoords_[ 0] = 0.0f;
  texCoords_[ 1] = 0.5f;

  texCoords_[ 2] = 0.5f;
  texCoords_[ 3] = 0.5f;

  texCoords_[ 4] = 0.5f;
  texCoords_[ 5] = 1.0f;

  texCoords_[ 6] = 0.5f;
  texCoords_[ 7] = 1.0f;

  texCoords_[ 8] = 0.0f;
  texCoords_[ 9] = 1.0f;

  texCoords_[10] = 0.0f;
  texCoords_[11] = 0.5f;


  texCoords_[12] = 0.5f;
  texCoords_[13] = 0.5f;

  texCoords_[14] = 1.0f;
  texCoords_[15] = 0.5f;

  texCoords_[16] = 1.0f;
  texCoords_[17] = 1.0f;

  texCoords_[18] = 1.0f;
  texCoords_[19] = 1.0f;

  texCoords_[20] = 0.5f;
  texCoords_[21] = 1.0f;

  texCoords_[22] = 0.5f;
  texCoords_[23] = 0.5f;
}

//==============================================================================
// Accessors
//------------------------------------------------------------------------------
GLfloat PivotPointRenderer::Size() const
{
  return size_;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
void PivotPointRenderer::Create()
{
  TextureStorage::TextureInformation texInfo;
  texInfo.filename  = "textures/pp.png";
  texInfo.reference = "pivotPoint";

  textureId_ = viewer.GetTextureStorage().AddTexture(texInfo);

  {
    ShaderStorage::ShaderInformation shaderInfo;
    shaderInfo.vertShader = "shader/pp2D.vert";
    shaderInfo.fragShader = "shader/pp2D.frag";
    shaderInfo.reference  = "pp2D";
    shaderInfo.attribLocations.push_back(
      ShaderStorage::AttribLocationInfo(0, "InVertex"));
    shaderInfo.attribLocations.push_back(
      ShaderStorage::AttribLocationInfo(1, "InTexCoord"));
    shaderInfo.attribLocations.push_back(
      ShaderStorage::AttribLocationInfo(2, "InAlpha"));

    shaderIndex_ = viewer.GetShaderStorage().AddShader(shaderInfo);
  }

  GLWrapper::GLProgram const& program =
    viewer.GetShaderStorage().GetShader(shaderIndex_);

  projection_ = program.GetUniformLocation("ProjectionModelviewMatrix");
  color_      = program.GetUniformLocation("DrawColor");
  
  vBuffer_.Create();
  tBuffer_.Create();
  aBuffer_.Create();

  vBuffer_.Data(24 * sizeof(GLfloat), vertices_ );
  tBuffer_.Data(24 * sizeof(GLfloat), texCoords_);
  aBuffer_.Data(12 * sizeof(GLfloat), alpha_    );

  vArray_.Create();

  vArray_.Attach(vBuffer_, 0, 2, GL_FLOAT);
  vArray_.Attach(tBuffer_, 1, 2, GL_FLOAT);
  vArray_.Attach(aBuffer_, 2, 1, GL_FLOAT);
}

//------------------------------------------------------------------------------
void PivotPointRenderer::Delete()
{
//  viewer.GetTextureStorage().RemoveTexture(textureId_);
  
//  textureId_ = 0;

  vArray_.Delete();

  vBuffer_.Delete();
  tBuffer_.Delete();
  aBuffer_.Delete();

  shaderIndex_  = 0;
//  textureId_ = 0;

  projection_   = 0;
  color_        = 0;
}

//------------------------------------------------------------------------------
void PivotPointRenderer::Update(dlm::uint64 const delta)
{
  Fader::Update(delta);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  selectionFader_.Update(delta);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  GLfloat alpha = Alpha();

  for (dlm::uint32 i = 0; i < 6; ++i)
  {
    alpha_[i] =  alpha;
  }

  alpha *= 0.4f + selectionFader_.Alpha() * 0.6f;

  for (dlm::uint32 i = 6; i < 12; ++i)
  {
    alpha_[i] = alpha;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (dlm::uint32 i = 0; i < 2; ++i)
  {
    dlm::uint32 const base = i * 12;

    vertices_[base +  0] = -size_;
    vertices_[base +  1] = -size_;

    vertices_[base +  2] =  size_;
    vertices_[base +  3] = -size_;

    vertices_[base +  4] =  size_;
    vertices_[base +  5] =  size_;


    vertices_[base +  6] =  size_;
    vertices_[base +  7] =  size_;

    vertices_[base +  8] = -size_;
    vertices_[base +  9] =  size_;

    vertices_[base + 10] = -size_;
    vertices_[base + 11] = -size_;
  }

  angle_ += 0.002f * static_cast<float>(delta);
  
  dlm::Matrix2f rotation;
  
  rotation(0, 0) =  cos(angle_);
  rotation(1, 1) =  cos(angle_);
  rotation(0, 1) =  sin(angle_);
  rotation(1, 0) = -sin(angle_);

  for (dlm::uint32 i = 6; i < 12; ++i)
  {
    dlm::uint32 const base = i * 2;

    dlm::Vector2f tmp(vertices_[base + 0], vertices_[base + 1]);
    
    tmp = rotation * tmp;
    
    vertices_[base + 0] = static_cast<GLfloat>(tmp.X());
    vertices_[base + 1] = static_cast<GLfloat>(tmp.Y());
  }

  for (dlm::uint32 i = 0; i < 12; ++i)
  {
    dlm::uint32 const base = i * 2;

    vertices_[base + 0] += static_cast<GLfloat>(position_.X());
    vertices_[base + 1] += static_cast<GLfloat>(position_.Y());
  }

  vBuffer_.SubData(24 * sizeof(GLfloat), vertices_);
  aBuffer_.SubData(12 * sizeof(GLfloat), alpha_   );
}

//------------------------------------------------------------------------------
void PivotPointRenderer::Draw(dlm::Matrix4f const& matrix) const
{
  viewer.GetTextureStorage().GetTexture(textureId_).Bind();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  viewer.GetShaderStorage().GetShader(shaderIndex_).Use();

  glh::UniformMatrix4fv(projection_, matrix);

  gl::Uniform3f(color_, 0.0, 0.0, 0.0);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  vArray_.Bind();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  glDisable(GL_DEPTH_TEST);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glDrawArrays(GL_TRIANGLES, 0, 12);

  glDisable(GL_BLEND);

  glEnable(GL_DEPTH_TEST);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  vArray_.Unbind();
}

//------------------------------------------------------------------------------
void PivotPointRenderer::Set(dlm::Vector2f const& position)
{
  position_ = position;
}

//------------------------------------------------------------------------------
void PivotPointRenderer::Select()
{
  selectionFader_.FadeIn();
}

//------------------------------------------------------------------------------
void PivotPointRenderer::Deselect()
{
  selectionFader_.FadeOut();
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
