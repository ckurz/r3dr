//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/renderer/CameraPathRenderer.h"

//------------------------------------------------------------------------------
//#include "GLWrapper/GLWrapper.h"
//#include "GLWrapper/GLVertexArray.h"
//#include "GLWrapper/GLBuffer.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlmViewer/Viewer.h"
#include "dlmViewer/GLHelper.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/SceneAssistant.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//#include <vector>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Constructor
//------------------------------------------------------------------------------
CameraPathRenderer::CameraPathRenderer()
  : shaderIndex_(0    )
  , projection_ (0    )
  , color_      (0    )
  , valid_      (false)
{}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
void CameraPathRenderer::Create()
{
  Delete();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  {
    ShaderStorage::ShaderInformation shaderInfo;
    shaderInfo.vertShader = "shader/draw3D.vert";
    shaderInfo.fragShader = "shader/draw3D.frag";
    shaderInfo.reference  = "draw3D";
    shaderInfo.attribLocations.push_back(
      ShaderStorage::AttribLocationInfo(0, "InVertex"));

    shaderIndex_ = viewer.GetShaderStorage().AddShader(shaderInfo);
  }

  GLWrapper::GLProgram const& program =
    dlmViewer::viewer.GetShaderStorage().GetShader(shaderIndex_);

  projection_ = program.GetUniformLocation("ProjectionModelviewMatrix");
  color_      = program.GetUniformLocation("DrawColor");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (dlm::uint32 i = 0; i < 4; ++i)
  {
    vArrays_ [i].Create();
    vBuffers_[i].Create();

    vBuffers_[i].Data(
      vertices_[i].size() * sizeof(dlm::Vector3d), vertices_[i].data());

    vArrays_[i].Attach(vBuffers_[i], 0, 3, GL_DOUBLE);
  }
}

//------------------------------------------------------------------------------
void CameraPathRenderer::Delete()
{
  for (dlm::uint32 i = 0; i < 4; ++i)
  {
    vertices_[i].clear();

    vArrays_[i].Delete();

    vBuffers_[i].Delete();
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  shaderIndex_ = 0;
  
  projection_ = 0;
  color_      = 0;
}

//------------------------------------------------------------------------------
void CameraPathRenderer::Update(dlm::uint64 const)
{
  if (!valid_)
  {
    for (dlm::uint32 i = 0; i < 4; ++i)
    {
      vBuffers_[i].Data(
        vertices_[i].size() * sizeof(dlm::Vector3d), vertices_[i].data());
    }
   
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    valid_ = true;
  }
}

//------------------------------------------------------------------------------
void CameraPathRenderer::Draw(dlm::Matrix4f const& matrix) const
{
  viewer.GetShaderStorage().GetShader(shaderIndex_).Use();

  GLHelper::UniformMatrix4fv(projection_, matrix);

  gl::Uniform4f(color_, 0.0f, 0.0f, 1.0f, 1.0f);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  vArrays_[0].Bind();
  glDrawArrays(GL_LINES, 0, static_cast<GLsizei>(vertices_[0].size()));
  vArrays_[0].Unbind();

  gl::Uniform4f(color_, 1.0f, 0.0f, 0.0f, 1.0f);

  vArrays_[1].Bind();
  glDrawArrays(GL_LINES, 0, static_cast<GLsizei>(vertices_[1].size()));
  vArrays_[1].Unbind();

  gl::Uniform4f(color_, 0.0f, 1.0f, 0.0f, 1.0f);

  vArrays_[2].Bind();
  glDrawArrays(GL_LINES, 0, static_cast<GLsizei>(vertices_[2].size()));
  vArrays_[2].Unbind();

  gl::Uniform4f(color_, 1.0f, 1.0f, 0.0f, 1.0f);

  vArrays_[3].Bind();
  glDrawArrays(GL_LINE_STRIP, 0, static_cast<GLsizei>(vertices_[3].size()));
  vArrays_[3].Unbind();
}

//------------------------------------------------------------------------------
void CameraPathRenderer::Set(dlm::Scene const& scene)
{
  vertices_[0].clear();
  vertices_[1].clear();
  vertices_[2].clear();
  vertices_[3].clear();

  valid_ = false;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (dlm::uint64 i = 0; i < scene.IRepS().Range(); ++i)
  {
    dlm::IRepId const iRepId = dlm::IRepId(i);

    if (!scene.IsValid(iRepId)) { continue; }

    dlm::IRep const& iRep = scene.Get(iRepId);

    if (!iRep.dlm::Reference<dlm::TSeq>::IsValid()) { continue; }
    if (!iRep.dlm::Reference<dlm::CRep>::IsValid()) { continue; }

//  DLM_ASSERT(iRep.dlm::Reference<dlm::TSeq>::IsValid(), DLM_RUNTIME_ERROR);
//  DLM_ASSERT(iRep.dlm::Reference<dlm::CRep>::IsValid(), RUNTIME_ERROR);

//  dlm::CRep const& cRep = scene.Get(iRep.dlm::Reference<dlm::CRep>::Get());

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    dlm::Transformation transformation =
      dlm::SA::Collapse(scene, iRep.dlm::Reference<dlm::TSeq>::Get());

    dlm::Matrix3f const rotation = dlm::QF::Rotation(transformation.Rotation());

    dlm::Vector3f const h = dlm::MF::GetCol(rotation, 0);
    dlm::Vector3f const v = dlm::MF::GetCol(rotation, 1);
    dlm::Vector3f const a = dlm::MF::GetCol(rotation, 2);

    dlm::Vector3f const c = transformation.Translation();

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    vertices_[0].push_back(c    );
    vertices_[0].push_back(c + a);

    vertices_[1].push_back(c    );
    vertices_[1].push_back(c + h);

    vertices_[2].push_back(c    );
    vertices_[2].push_back(c + v);

    vertices_[3].push_back(c);
  }
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
