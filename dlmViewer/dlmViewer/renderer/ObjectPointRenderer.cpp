//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/renderer/ObjectPointRenderer.h"

//------------------------------------------------------------------------------
#include "dlmViewer/Viewer.h"
#include "dlmViewer/GLHelper.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/SceneAssistant.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.inline.h"
#include "dlm/math/Matrix.inline.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Constructor
//------------------------------------------------------------------------------
ObjectPointRenderer::ObjectPointRenderer()
  : unreferencedColor_(0.0, 0.0, 0.0)
  , referencedColor_  (0.0, 1.0, 0.0)
  , projectableColor_ (0.0, 0.0, 1.0)
  , visibleColor_     (1.0, 0.6, 0.2)
  , selectedColor_    (1.0, 0.0, 0.0)
  , shaderIndex_      (0            )
  , projection_       (0            )
  , valid_            (false        )
{}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
void ObjectPointRenderer::Create()
{
  Delete();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  {
    ShaderStorage::ShaderInformation shaderInfo;
    shaderInfo.vertShader = "shader/op3D.vert";
    shaderInfo.fragShader = "shader/op3D.frag";
    shaderInfo.reference  = "op3D";
    shaderInfo.attribLocations.push_back(
      ShaderStorage::AttribLocationInfo(0, "InVertex"));
    shaderInfo.attribLocations.push_back(
      ShaderStorage::AttribLocationInfo(1, "InColor"));

    shaderIndex_ = viewer.GetShaderStorage().AddShader(shaderInfo);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  GLWrapper::GLProgram const& program =
    viewer.GetShaderStorage().GetShader(shaderIndex_);

  projection_ = program.GetUniformLocation("ProjectionModelviewMatrix");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  pointsVertexArray_.Create();

  pointsBuffer_.Create();
  colorsBuffer_.Create();

  pointsVertexArray_.Attach(pointsBuffer_, 0, 3, GL_DOUBLE);
  pointsVertexArray_.Attach(colorsBuffer_, 1, 3, GL_DOUBLE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  unreferencedPointsVertexArray_.Create();

  unreferencedPointsBuffer_.Create();
  unreferencedColorsBuffer_.Create();

  unreferencedPointsVertexArray_.Attach(unreferencedPointsBuffer_, 0, 3, GL_DOUBLE);
  unreferencedPointsVertexArray_.Attach(unreferencedColorsBuffer_, 1, 3, GL_DOUBLE);
}

//------------------------------------------------------------------------------
void ObjectPointRenderer::Delete()
{
  points_.clear();
  colors_.clear();

  unreferencedPoints_.clear();
  unreferencedColors_.clear();

  projectable_.clear();
  visible_.clear();
//  selected_.clear();

  fTrkIdConversion_.clear();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  pointsVertexArray_.Delete();

  pointsBuffer_.Delete();
  colorsBuffer_.Delete();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  unreferencedPointsVertexArray_.Delete();

  unreferencedPointsBuffer_.Delete();
  unreferencedColorsBuffer_.Delete();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  shaderIndex_ = 0;

  projection_  = 0;
}

//------------------------------------------------------------------------------
void ObjectPointRenderer::Update(dlm::uint64 const)
{
  if (!valid_)
  {
    UpdateColors();

    valid_ = true;
  }
}

//------------------------------------------------------------------------------
void ObjectPointRenderer::Draw(dlm::Matrix4f const& matrix) const
{
  viewer.GetShaderStorage().GetShader(shaderIndex_).Use();

  glh::UniformMatrix4fv(projection_, matrix);

  glPointSize(2.0f);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  pointsVertexArray_.Bind();

  glDrawArrays(GL_POINTS, 0, static_cast<GLsizei>(points_.size()));

  pointsVertexArray_.Unbind();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  unreferencedPointsVertexArray_.Bind();

  glDrawArrays(GL_POINTS, 0, static_cast<GLsizei>(unreferencedPoints_.size()));

  unreferencedPointsVertexArray_.Unbind();





/*
  GLWrapper::GLVertexArray myVA;
  
  myVA.Create();
  
  GLWrapper::GLBuffer myB0;
  GLWrapper::GLBuffer myB1;
  
  myB0.Create();
  myB1.Create();

  myB0.Data(
    unreferencedPoints_.size() * sizeof(dlm::Vector3f),
    unreferencedPoints_.data());

  myB1.Data(
    unreferencedColors_.size() * sizeof(dlm::Vector3f),
    unreferencedColors_.data());
    
  myVA.Attach(myB0, 0, 3, GL_DOUBLE);
  myVA.Attach(myB1, 1, 3, GL_DOUBLE);

  myVA.Bind();

  glDrawArrays(GL_POINTS, 0, unreferencedPoints_.size());
  
  myVA.Unbind();

  myVA.Delete();
  
  myB0.Delete();
  myB1.Delete();
*/

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  glPointSize(1.0f);
}

//------------------------------------------------------------------------------
void ObjectPointRenderer::Set(dlm::Scene const& scene)
{
  SetReferencedObjectPoints(scene);
  SetUnreferencedObjectPoints(scene);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  UpdateColors();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//  valid_ = true;
}

//------------------------------------------------------------------------------
void ObjectPointRenderer::SetReferencedObjectPoints(dlm::Scene const& scene)
{
  points_.clear();
  colors_.clear();

  projectable_.clear();
  visible_.clear();
//  selected_.clear();

  fTrkIdConversion_.clear();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Scene::FTrkStorage const& fTrkS = scene.FTrkS();

  fTrkIdConversion_.resize(fTrkS.Range(), dlm::Invalid);

  for (dlm::uint64 i = 0; i < fTrkS.Range(); ++i)
  {
    FTrkId const fTrkId = FTrkId(i);
    
    if (!scene.IsValid(fTrkId)) { continue; }

    FTrk const& fTrk = scene.Get(fTrkId);

    if (!fTrk.Reference<ORep>::IsValid()) { continue; }

    fTrkIdConversion_.at(i) = points_.size();

    points_.push_back(SA::Transform(scene, fTrkId));

    if (!fTrk.empty())
    {
      projectable_.push_back(fTrkId);

      colors_.push_back(projectableColor_);
    }
    else
    {
      colors_.push_back(referencedColor_);
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  pointsBuffer_.Data(points_.size() * sizeof(dlm::Vector3f), points_.data());
  colorsBuffer_.Data(colors_.size() * sizeof(dlm::Vector3f), colors_.data());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  valid_ = false;
}

//------------------------------------------------------------------------------
void ObjectPointRenderer::SetUnreferencedObjectPoints(dlm::Scene const& scene)
{
  unreferencedPoints_.clear();
  unreferencedColors_.clear();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Scene::ORepStorage const& oRepS = scene.ORepS();

  std::vector<dlm::uint64> oRepIdConversion_;

  for (dlm::uint64 i = 0; i < oRepS.Range(); ++i)
  {
    dlm::ORepId const oRepId = dlm::ORepId(i);

    if (!scene.IsValid(oRepId)) { continue; }

    ORep const& oRep = scene.Get(oRepId);

    auto referenceSet = oRep.ReferenceSet<FTrk>::Get();

    if (referenceSet.empty())
    {
      unreferencedPoints_.push_back(SA::Transform(scene, oRepId));
      unreferencedColors_.push_back(unreferencedColor_);
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//  std::cout << "SETTING DATA" << std::endl;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  unreferencedPointsBuffer_.Data(
    unreferencedPoints_.size() * sizeof(dlm::Vector3f),
    unreferencedPoints_.data());

  unreferencedColorsBuffer_.Data(
    unreferencedColors_.size() * sizeof(dlm::Vector3f),
    unreferencedColors_.data());
}

//------------------------------------------------------------------------------
void ObjectPointRenderer::SetVisible(std::vector<dlm::FTrkId> const& indices)
{
  visible_ = indices;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  valid_ = false;
}

//------------------------------------------------------------------------------
void ObjectPointRenderer::SetProjectable(
  std::vector<dlm::FTrkId> const& indices)
{
  projectable_ = indices;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  valid_ = false;
}

//------------------------------------------------------------------------------
void ObjectPointRenderer::SetSelected(std::vector<dlm::FTrkId> const& indices)
{
  selected_ = indices;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  valid_ = false;
}

//------------------------------------------------------------------------------
void ObjectPointRenderer::SetSelected(std::set<dlm::FTrkId> const& indices)
{
  selected_.clear();
  
  selected_.reserve(indices.size());
  
  for (auto const& element : indices)
  {
    selected_.push_back(element);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  valid_ = false;
}

//------------------------------------------------------------------------------
void ObjectPointRenderer::UpdateColors()
{
  for (auto it = colors_.begin(); it != colors_.end(); ++it)
  {
    *it = referencedColor_;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (auto it = projectable_.cbegin(); it != projectable_.cend(); ++it)
  {
    auto const idx = it->Get();
    
    if (!(idx < fTrkIdConversion_.size()))
    {
//      std::cerr << "Invalid FTrkId conversion index -- Projectable." << std::endl;
      
      continue;
    }
    
    auto const converted = fTrkIdConversion_.at(idx);
    
    if (!(converted < colors_.size()))
    {
//      std::cerr << "Invalid converted index -- Projectable." << std::endl;
      
      continue;
    }
  
    colors_.at(converted) = projectableColor_;
  }

  for (auto it = visible_.cbegin(); it != visible_.cend(); ++it)
  {
    auto const idx = it->Get();
    
    if (!(idx < fTrkIdConversion_.size()))
    {
//      std::cerr << "Invalid FTrkId conversion index -- Visible." << std::endl;
      
      continue;
    }
    
    auto const converted = fTrkIdConversion_.at(idx);
    
    if (!(converted < colors_.size()))
    {
//      std::cerr << "Invalid converted index -- Visible." << std::endl;
      
      continue;
    }
  
    colors_.at(converted) = visibleColor_;
  }

  auto counter = 0;

  for (auto it = selected_.cbegin(); it != selected_.cend(); ++it)
  {
    auto const idx = it->Get();
    
    if (!(idx < fTrkIdConversion_.size()))
    {
//      std::cerr << "Invalid FTrkId conversion index -- Selected." << std::endl;
      
      continue;
    }
    
    auto const converted = fTrkIdConversion_.at(idx);
    
    if (!(converted < colors_.size()))
    {
//      std::cerr << "Invalid converted index -- Selected." << std::endl;
      
      continue;
    }
  ++counter;
    colors_.at(converted) = selectedColor_;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  colorsBuffer_.SubData(colors_.size() * sizeof(dlm::Vector3f), colors_.data());
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
