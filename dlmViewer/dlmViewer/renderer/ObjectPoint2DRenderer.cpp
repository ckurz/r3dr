//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/renderer/ObjectPoint2DRenderer.h"

//------------------------------------------------------------------------------
#include "dlmViewer/Viewer.h"
#include "dlmViewer/GLHelper.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/SceneAssistant.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/VectorFunctions.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
ObjectPoint2DRenderer::ObjectPoint2DRenderer()
  : baseColor_       (0.21f, 0.46f, 0.53f, 1.0f)
  , shaderIndex_     (0                        )
  , bufferSize_      (0                        )
  , size_            (5.0f                     )
  , projectionMatrix_(0                        )
  , sizeX_           (0                        )
  , sizeY_           (0                        )
  , color_           (0                        )
  , valid_           (false                    )
{}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
void ObjectPoint2DRenderer::Create()
{
  Delete();

  {
    TextureStorage::TextureInformation texInfo;
    texInfo.filename  = "textures/gradient.png";
    texInfo.reference = "gradient";

    textureId_ = viewer.GetTextureStorage().AddTexture(texInfo);
  }

  {
    ShaderStorage::ShaderInformation shaderInfo;
    shaderInfo.vertShader = "shader/OP2DRenderer.vert";
    shaderInfo.geomShader = "shader/OP2DRenderer.geom";
    shaderInfo.fragShader = "shader/OP2DRenderer.frag";
    shaderInfo.reference  = "OP2DRenderer";
    shaderInfo.attribLocations.push_back(
      ShaderStorage::AttribLocationInfo(0, "InPosition"));

    shaderIndex_ = viewer.GetShaderStorage().AddShader(shaderInfo);
  }

  GLWrapper::GLProgram const& program =
    viewer.GetShaderStorage().GetShader(shaderIndex_);

  projectionMatrix_ = program.GetUniformLocation("ModelviewProjection");
  sizeX_            = program.GetUniformLocation("SizeX");
  sizeY_            = program.GetUniformLocation("SizeY");
  color_            = program.GetUniformLocation("Color");

  vArray_.Create();

  vBuffer_.Create();

  vArray_.Attach(vBuffer_, 0, 2, GL_FLOAT);
}

//------------------------------------------------------------------------------
void ObjectPoint2DRenderer::Delete()
{
  vArray_.Delete();

  vBuffer_.Delete();

  bufferSize_ = 0;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  valid_ = false;
}

//------------------------------------------------------------------------------
void ObjectPoint2DRenderer::Update(dlm::uint64 const)
{
  if (!valid_)
  {
    auto it = points_.find(current_);

    if (it != points_.cend())
    {
      auto targetSize = it->second.size() * sizeof(PointType);

      if (targetSize > bufferSize_)
      {
        vBuffer_.Data(targetSize, it->second.data());
      
        bufferSize_ = targetSize;
      }
      else
      {
        vBuffer_.SubData(it->second.size() * sizeof(PointType), it->second.data());
      }
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    valid_ = true;
  }
}

//------------------------------------------------------------------------------
void ObjectPoint2DRenderer::Draw(dlm::Matrix4f const& matrix) const
{
  viewer.GetTextureStorage().GetTexture(textureId_).Bind();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  viewer.GetShaderStorage().GetShader(shaderIndex_).Use();

  GLHelper::UniformMatrix4fv(projectionMatrix_, matrix);

  dlm::Vector2u const& windowSize =
    viewer.GetWindowConfiguration().WindowSize();

  gl::Uniform1f(sizeX_, size_ / static_cast<GLfloat>(windowSize.X()));
  gl::Uniform1f(sizeY_, size_ / static_cast<GLfloat>(windowSize.Y()));

  gl::Uniform4f(color_, baseColor_.X(), baseColor_.Y(), baseColor_.Z(), baseColor_.W());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  glDisable(GL_DEPTH_TEST);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  vArray_.Bind();

  glDrawArrays(GL_POINTS, 0, static_cast<GLsizei>(points_.size()));
  
  vArray_.Unbind();

  glDisable(GL_BLEND);

  glEnable(GL_DEPTH_TEST);
}

//------------------------------------------------------------------------------
void ObjectPoint2DRenderer::Set(
  dlm::Scene const& scene, dlm::IRepId const initial)
{
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (dlm::uint64 i = 0; i < scene.IRepS().Range(); ++i)
  {
    IRepId const iRepId = IRepId(i);

    if (!scene.IsValid(iRepId)) { continue; }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    IRep const& iRep = scene.Get(iRepId);

    if (!iRep.Reference<FSet>::IsValid()) { continue; }
    if (!iRep.Reference<CRep>::IsValid()) { continue; }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//    FSetId const fSetId = iRep.Reference<FSet>::Get();

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//    auto& points = points_[iRepId];

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//    for (dlm::uint64 i = 0; i < scene.Get(fSetId).Range(); ++i)
//    {
//      FRepId const fRepId = FRepId(i);

//      if (scene.IsValid(fSetId, fRepId))
//      {
//        dlm::Vector2f const projected = SA::Project(scene, fSetId, fRepId);

//        points.push_back(VF::Convert<GLfloat>(iRep.Flip(iRep.ToImg(projected))));
//      }
//    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  SetCurrent(initial);
}

//------------------------------------------------------------------------------
void ObjectPoint2DRenderer::SetCurrent(dlm::IRepId const current)
{
  current_ = current;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  valid_ = false;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
