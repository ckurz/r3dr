//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmViewer_renderer_SceneRenderer_h_
#define dlmViewer_renderer_SceneRenderer_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/renderer/ImageSequenceRenderer.h"
#include "dlmViewer/renderer/ImageSequence2DRenderer.h"
#include "dlmViewer/renderer/FeaturePointSetRenderer.h"
#include "dlmViewer/renderer/CameraRenderer.h"
#include "dlmViewer/renderer/CameraPathRenderer.h"
#include "dlmViewer/renderer/ClosestRenderer.h"
#include "dlmViewer/renderer/ObjectPointRenderer.h"
#include "dlmViewer/renderer/ObjectPoint2DRenderer.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlmViewer/Renderer.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/Scene.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <set>
#include <vector>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// SceneRenderer class
//------------------------------------------------------------------------------
class SceneRenderer : public Renderer
{
public:
  //============================================================================
  // Constructor
  //----------------------------------------------------------------------------
  //SceneRenderer();

  //============================================================================
  // Accessors
  //----------------------------------------------------------------------------

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  void Create();
  void Delete();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Update(dlm::uint64 const delta);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Draw  (dlm::Matrix4f const& matrix) const;
  void Draw2D(dlm::Matrix4f const& matrix) const;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Set(dlm::Scene const& scene, dlm::IRepId const current = dlm::IRepId(0));

  void Set(dlm::Scene const& scene, std::vector<int> const& closest);
  
  void SetSelected(std::set<dlm::FTrkId> const& selected);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Next();
  void Previous();

private:
  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  ImageSequenceRenderer imageSequenceRenderer_;
  ImageSequence2DRenderer imageSequence2DRenderer_;
  FeaturePointSetRenderer featurePointSetRenderer_;
  CameraPathRenderer cameraPath_;
  ObjectPointRenderer objectPoints_;
  ObjectPoint2DRenderer objectPoints2D_;
  ClosestRenderer closestRenderer_;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  std::map<dlm::IRepId, CameraRenderer> cameras_;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  dlm::IRepId current_;
  dlm::IRepId range_;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
