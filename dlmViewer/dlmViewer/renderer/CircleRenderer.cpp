//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/renderer/CircleRenderer.h"

//------------------------------------------------------------------------------
#include "dlmViewer/Viewer.h"
#include "dlmViewer/GLHelper.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.inline.h"
#include "dlm/math/Matrix.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <cmath>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Constructor
//------------------------------------------------------------------------------
CircleRenderer::CircleRenderer(
  dlm::uint64    const numElements,
  dlm::FloatType const radius,
  dlm::FloatType const elementLength,
  dlm::FloatType const elementWidth)
  : maxElements_  (0                               )
  , angle_        (static_cast<dlm::FloatType>(0.0))
  , radius_       (radius                          )
  , elementLength_(elementLength                   )
  , elementWidth_ (elementWidth                    )
  , valid_        (false                           )
{
  SetNumElements(numElements);
}

//==============================================================================
// Accessors
//------------------------------------------------------------------------------
dlm::FloatType CircleRenderer::Angle() const
{
  return angle_;
}

//------------------------------------------------------------------------------
dlm::FloatType CircleRenderer::Radius() const
{
  return radius_;
}

//------------------------------------------------------------------------------
dlm::FloatType CircleRenderer::ElementLength() const
{
  return elementLength_;
}

//------------------------------------------------------------------------------
dlm::FloatType CircleRenderer::ElementWidth() const
{
  return elementWidth_;
}

//==============================================================================
// Mutators
//------------------------------------------------------------------------------
dlm::FloatType& CircleRenderer::Angle()
{
  return angle_;
}

//------------------------------------------------------------------------------
dlm::FloatType& CircleRenderer::Radius()
{
  return radius_;
}

//------------------------------------------------------------------------------
dlm::FloatType& CircleRenderer::ElementLength()
{
  return elementLength_;
}

//------------------------------------------------------------------------------
dlm::FloatType& CircleRenderer::ElementWidth()
{
  return elementWidth_;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
void CircleRenderer::Create()
{
  {
    ShaderStorage::ShaderInformation shaderInfo;
    shaderInfo.vertShader = "shader/CircleRenderer.vert";
    shaderInfo.geomShader = "shader/CircleRenderer.geom";
    shaderInfo.fragShader = "shader/CircleRenderer.frag";
    shaderInfo.reference  = "circleRenderer";
    shaderInfo.attribLocations.push_back(
      ShaderStorage::AttribLocationInfo(0, "InPosition"));

    shaderIndex_ = viewer.GetShaderStorage().AddShader(shaderInfo);
  }

  GLWrapper::GLProgram const& program =
    viewer.GetShaderStorage().GetShader(shaderIndex_);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  transformation_ = program.GetUniformLocation("Transformation");
  length_         = program.GetUniformLocation("Length");
  width_          = program.GetUniformLocation("Width");
  projection_     = program.GetUniformLocation("ModelviewProjection");
  color_          = program.GetUniformLocation("Color");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  vArray_.Create();
  vBuffer_.Create();

  vBuffer_.Data(sizeof(dlm::Vector2f) * positions_.size(), positions_.data());

  vArray_.Attach(vBuffer_, 0, 2, GL_DOUBLE);
}

//------------------------------------------------------------------------------
void CircleRenderer::Update(dlm::uint64 const delta)
{
  Fader::Update(delta);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  angle_ += static_cast<double>(delta) * 0.00004;

  if (angle_ > 2.0 * M_PI)
  {
    angle_ -= 2.0 * M_PI;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (!valid_)
  {
    if (positions_.size() > maxElements_)
    {
      vBuffer_.Data(
        sizeof(dlm::Vector2f) * positions_.size(), positions_.data());
      
      maxElements_ = positions_.size();
    }
    else
    {
      vBuffer_.SubData(
        sizeof(dlm::Vector2f) * positions_.size(), positions_.data());
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    valid_ = true;
  }
}

//------------------------------------------------------------------------------
void CircleRenderer::Delete()
{
  vArray_.Delete();
  vBuffer_.Delete();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  shaderIndex_ = 0;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  transformation_ = 0;
  length_         = 0;
  width_          = 0;
  projection_     = 0;
  color_          = 0;
}

//------------------------------------------------------------------------------
void CircleRenderer::Draw(dlm::Matrix4f const& matrix) const
{
  GLWrapper::GLProgram const& program =
    viewer.GetShaderStorage().GetShader(shaderIndex_);

  program.Use();
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  GLHelper::UniformMatrix4fv(projection_, matrix);

  dlm::Matrix2f transformation;

  transformation.At(0, 0) = radius_ *  cos(angle_);
  transformation.At(0, 1) = radius_ * -sin(angle_);
  transformation.At(1, 0) = radius_ *  sin(angle_);
  transformation.At(1, 1) = radius_ *  cos(angle_);

  GLHelper::UniformMatrix2fv(transformation_, transformation);

  gl::Uniform1f(length_, static_cast<GLfloat>(elementLength_));
  gl::Uniform1f(width_,  static_cast<GLfloat>(elementWidth_ ));

  gl::Uniform4f(color_, 0.0f, 0.0f, 0.0f, Alpha());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  vArray_.Bind();

  glDrawArrays(GL_POINTS, 0, static_cast<GLsizei>(positions_.size()));

  vArray_.Unbind();

  glDisable(GL_BLEND);
}


//------------------------------------------------------------------------------
void CircleRenderer::SetNumElements(dlm::uint64 const numElements)
{
  if (numElements == positions_.size())
  {
    return;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  positions_.clear();

  valid_ = false;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (dlm::uint64 i = 0; i < numElements; ++i)
  {
    dlm::FloatType const angle =
      static_cast<dlm::FloatType>(
        2.0 * M_PI * static_cast<dlm::FloatType>(i) / static_cast<dlm::FloatType>(numElements));
    
    dlm::Vector2f const position(
      static_cast<dlm::FloatType>(cos(angle)),
      static_cast<dlm::FloatType>(sin(angle)));

    positions_.push_back(position);
  }
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
