//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/renderer/ClosestRenderer.h"

//------------------------------------------------------------------------------
//#include "GLWrapper/GLWrapper.h"
//#include "GLWrapper/GLVertexArray.h"
//#include "GLWrapper/GLBuffer.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlmViewer/Viewer.h"
#include "dlmViewer/GLHelper.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/SceneAssistant.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//#include <vector>

#include "dlm/math/OStream.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Constructor
//------------------------------------------------------------------------------
ClosestRenderer::ClosestRenderer()
  : shaderIndex_(0    )
  , projection_ (0    )
  , color_      (0    )
  , valid_      (false)
{}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
void ClosestRenderer::Create()
{
  Delete();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  {
    ShaderStorage::ShaderInformation shaderInfo;
    shaderInfo.vertShader = "shader/draw3D.vert";
    shaderInfo.fragShader = "shader/draw3D.frag";
    shaderInfo.reference  = "draw3D";
    shaderInfo.attribLocations.push_back(
      ShaderStorage::AttribLocationInfo(0, "InVertex"));

    shaderIndex_ = viewer.GetShaderStorage().AddShader(shaderInfo);
  }

  GLWrapper::GLProgram const& program =
    dlmViewer::viewer.GetShaderStorage().GetShader(shaderIndex_);

  projection_ = program.GetUniformLocation("ProjectionModelviewMatrix");
  color_      = program.GetUniformLocation("DrawColor");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  vArrays_ .Create();
  vBuffers_.Create();

  vBuffers_.Data(
    vertices_.size() * sizeof(dlm::Vector3d), vertices_.data());

  vArrays_.Attach(vBuffers_, 0, 3, GL_DOUBLE);
}

//------------------------------------------------------------------------------
void ClosestRenderer::Delete()
{
//  vertices_.clear();

  vArrays_.Delete();

  vBuffers_.Delete();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  shaderIndex_ = 0;
  
  projection_ = 0;
  color_      = 0;
}

//------------------------------------------------------------------------------
void ClosestRenderer::Update(dlm::uint64 const)
{
  if (!valid_)
  {
    vBuffers_.Data(
      vertices_.size() * sizeof(dlm::Vector3d), vertices_.data());
   
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    valid_ = true;
  }
}

//------------------------------------------------------------------------------
void ClosestRenderer::Draw(dlm::Matrix4f const& matrix) const
{
  viewer.GetShaderStorage().GetShader(shaderIndex_).Use();

  GLHelper::UniformMatrix4fv(projection_, matrix);

  gl::Uniform4f(color_, 1.0f, 0.6f, 0.0f, 1.0f);

  vArrays_.Bind();
  glDrawArrays(GL_LINES, 0, static_cast<GLsizei>(vertices_.size()));
  vArrays_.Unbind();
}

//------------------------------------------------------------------------------
void ClosestRenderer::Set(dlm::Scene const& scene, std::vector<int> const& closest)
{
  vertices_.clear();

  valid_ = false;
  
  if (scene.IRepS().Range() == 0)
  {
    return;
  }
  
  auto last = dlm::IRepId{scene.IRepS().Range() - 1};
  
  if (!scene.IsValid(last))
  {
    std::cout << "ClosestRenderer INVALID" << std::endl;
  
    return;
  }

  auto t0 = dlm::SA::Collapse(scene, scene.Get(last).TSeq());
  
  for (auto const& element : closest)
  {
    auto const iRepId = dlm::IRepId{static_cast<dlm::uint64>(element)};
    
    if (!scene.IsValid(iRepId)) { continue; }
    
    dlm::IRep const& iRep = scene.Get(iRepId);

    if (!iRep.dlm::Reference<dlm::TSeq>::IsValid()) { continue; }
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    auto t1 = dlm::SA::Collapse(scene, iRep.dlm::Reference<dlm::TSeq>::Get());

    vertices_.push_back(t0.Translation());
    vertices_.push_back(t1.Translation());
  }
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
