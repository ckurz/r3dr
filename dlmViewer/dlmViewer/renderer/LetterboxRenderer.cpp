//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/renderer/LetterboxRenderer.h"

//------------------------------------------------------------------------------
//#include "GLWrapper/GLWrapper.h"
//#include "GLWrapper/GLVertexArray.h"
//#include "GLWrapper/GLBuffer.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlmViewer/Viewer.h"
#include "dlmViewer/GLHelper.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//#include "dlm/core/Types.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//#include <vector>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Constructor
//------------------------------------------------------------------------------
LetterboxRenderer::LetterboxRenderer()
  : vertices_   (     )
  , shaderIndex_(0    )
  , projection_ (0    )
  , alpha_      (0    )
  , valid_      (false)
{}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
void LetterboxRenderer::Create()
{
  {
    ShaderStorage::ShaderInformation shaderInfo;
    shaderInfo.vertShader = "shader/letterbox.vert";
    shaderInfo.fragShader = "shader/letterbox.frag";
    shaderInfo.reference  = "letterbox";
    shaderInfo.attribLocations.push_back(
      ShaderStorage::AttribLocationInfo(0, "InVertex"));

    shaderIndex_ = viewer.GetShaderStorage().AddShader(shaderInfo);
  }

  GLWrapper::GLProgram const& program =
    viewer.GetShaderStorage().GetShader(shaderIndex_);

  projection_ = program.GetUniformLocation("ProjectionModelviewMatrix");
  alpha_      = program.GetUniformLocation("Alpha");

  vArray_.Create();
  vBuffer_.Create();

  vBuffer_.Data(24 * sizeof(GLfloat), vertices_);

  vArray_.Attach(vBuffer_, 0, 2, GL_FLOAT);

  valid_ = true;
}

//------------------------------------------------------------------------------
void LetterboxRenderer::Update(dlm::uint64 const delta)
{
  Fader::Update(delta);

  if (!valid_)
  {
    vBuffer_.SubData(24 * sizeof(GLfloat), vertices_);

    valid_ = true;
  }
}

//------------------------------------------------------------------------------
void LetterboxRenderer::Delete()
{
  vArray_.Delete();
  vBuffer_.Delete();
}

//------------------------------------------------------------------------------
void LetterboxRenderer::Draw(dlm::Matrix4f const& matrix) const
{
  if (!IsVisible())
  {
    return;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  viewer.GetShaderStorage().GetShader(shaderIndex_).Use();

  glh::UniformMatrix4fv(projection_, matrix);

  gl::Uniform1f(alpha_, float(0.8 * Alpha()));

  glDisable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  vArray_.Bind();
  glDrawArrays(GL_TRIANGLES, 0, 12);
  vArray_.Unbind();

  glDisable(GL_BLEND);
  glEnable(GL_DEPTH_TEST);
}

//------------------------------------------------------------------------------
void LetterboxRenderer::Set(
  dlm::FloatType const& left, dlm::FloatType const& right,
  dlm::FloatType const& top,  dlm::FloatType const& bottom)
{
  if (left == static_cast<dlm::FloatType>(0.0))
  {
    //vertical bars
    vertices_[ 0] = static_cast<GLfloat>(0.0);
    vertices_[ 1] = static_cast<GLfloat>(bottom);

    vertices_[ 2] = static_cast<GLfloat>(right);
    vertices_[ 3] = static_cast<GLfloat>(bottom);

    vertices_[ 4] = static_cast<GLfloat>(right);
    vertices_[ 5] = static_cast<GLfloat>(0.0);

    vertices_[ 6] = static_cast<GLfloat>(right);
    vertices_[ 7] = static_cast<GLfloat>(0.0);

    vertices_[ 8] = static_cast<GLfloat>(left);
    vertices_[ 9] = static_cast<GLfloat>(0.0);

    vertices_[10] = static_cast<GLfloat>(0.0);
    vertices_[11] = static_cast<GLfloat>(bottom);

    vertices_[12] = static_cast<GLfloat>(0.0);
    vertices_[13] = static_cast<GLfloat>(top + bottom);

    vertices_[14] = static_cast<GLfloat>(right);
    vertices_[15] = static_cast<GLfloat>(top + bottom);

    vertices_[16] = static_cast<GLfloat>(right);
    vertices_[17] = static_cast<GLfloat>(top);

    vertices_[18] = static_cast<GLfloat>(right);
    vertices_[19] = static_cast<GLfloat>(top);

    vertices_[20] = static_cast<GLfloat>(0.0);
    vertices_[21] = static_cast<GLfloat>(top);

    vertices_[22] = static_cast<GLfloat>(0.0);
    vertices_[23] = static_cast<GLfloat>(top + bottom);
  }
  else
  {
    //horizontal bars
    vertices_[ 0] = static_cast<GLfloat>(left);
    vertices_[ 1] = static_cast<GLfloat>(0.0);

    vertices_[ 2] = static_cast<GLfloat>(0.0);
    vertices_[ 3] = static_cast<GLfloat>(0.0);

    vertices_[ 4] = static_cast<GLfloat>(0.0);
    vertices_[ 5] = static_cast<GLfloat>(top);

    vertices_[ 6] = static_cast<GLfloat>(0.0);
    vertices_[ 7] = static_cast<GLfloat>(top);

    vertices_[ 8] = static_cast<GLfloat>(left);
    vertices_[ 9] = static_cast<GLfloat>(top);

    vertices_[10] = static_cast<GLfloat>(left);
    vertices_[11] = static_cast<GLfloat>(0.0);

    vertices_[12] = static_cast<GLfloat>(right + left);
    vertices_[13] = static_cast<GLfloat>(0.0);

    vertices_[14] = static_cast<GLfloat>(right);
    vertices_[15] = static_cast<GLfloat>(0.0);

    vertices_[16] = static_cast<GLfloat>(right);
    vertices_[17] = static_cast<GLfloat>(top);

    vertices_[18] = static_cast<GLfloat>(right);
    vertices_[19] = static_cast<GLfloat>(top);

    vertices_[20] = static_cast<GLfloat>(right + left);
    vertices_[21] = static_cast<GLfloat>(top);

    vertices_[22] = static_cast<GLfloat>(right + left);
    vertices_[23] = static_cast<GLfloat>(0.0);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  valid_ = false;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
