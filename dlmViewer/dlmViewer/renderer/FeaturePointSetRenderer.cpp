//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/renderer/FeaturePointSetRenderer.h"

//------------------------------------------------------------------------------
//#include "GLWrapper/GLWrapper.h"
//#include "GLWrapper/GLVertexArray.h"
//#include "GLWrapper/GLBuffer.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlmViewer/Viewer.h"
#include "dlmViewer/GLHelper.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//#include "dlm/scene/Scene.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//#include "dlm/math/Vector.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//#include <vector>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
FeaturePointSetRenderer::FeaturePointSetRenderer()
  : baseColor_       (1.0f, 0.0f, 0.0f, 1.0f)
  , shaderIndex_     (0)
  , bufferSize_      (0)
  , size_            (6.0f)
  , projectionMatrix_(0)
  , sizeX_           (0)
  , sizeY_           (0)
  , valid_           (false)
{}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
void FeaturePointSetRenderer::Create()
{
  Delete();

  {
    TextureStorage::TextureInformation texInfo;
    texInfo.filename  = "textures/fp2.png";
    texInfo.reference = "fp";

    textureId_ = viewer.GetTextureStorage().AddTexture(texInfo);
  }

  {
    ShaderStorage::ShaderInformation shaderInfo;
    shaderInfo.vertShader = "shader/FP2DRenderer.vert";
    shaderInfo.geomShader = "shader/FP2DRenderer.geom";
    shaderInfo.fragShader = "shader/FP2DRenderer.frag";
    shaderInfo.reference  = "fp2D";
    shaderInfo.attribLocations.push_back(
      ShaderStorage::AttribLocationInfo(0, "InPosition"));
    shaderInfo.attribLocations.push_back(
      ShaderStorage::AttribLocationInfo(1, "InColor"));

    shaderIndex_ = viewer.GetShaderStorage().AddShader(shaderInfo);
  }

  GLWrapper::GLProgram const& program =
    viewer.GetShaderStorage().GetShader(shaderIndex_);

  projectionMatrix_ = program.GetUniformLocation("ModelviewProjection");
  sizeX_       = program.GetUniformLocation("SizeX");
  sizeY_       = program.GetUniformLocation("SizeY");

  vArray_.Create();

  vBuffer_.Create();
  cBuffer_.Create();

  vArray_.Attach(vBuffer_, 0, 2, GL_FLOAT);
  vArray_.Attach(cBuffer_, 1, 4, GL_FLOAT);
}

//------------------------------------------------------------------------------
void FeaturePointSetRenderer::Delete()
{
  vArray_.Delete();

  vBuffer_.Delete();
  cBuffer_.Delete();

  bufferSize_ = 0;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  valid_ = false;
}

//------------------------------------------------------------------------------
void FeaturePointSetRenderer::Update(dlm::uint64 const)
{
  if (!valid_)
  {
    auto pIt = points_.find(current_);
    auto cIt = colors_.find(current_);

    if (pIt != points_.cend() && cIt != colors_.cend())
    {
      auto targetSize = pIt->second.size() * sizeof(PointType);

      if (targetSize > bufferSize_)
      {
        vBuffer_.Data(targetSize, pIt->second.data());
        cBuffer_.Data(cIt->second.size() * sizeof(ColorType), cIt->second.data());
      
        bufferSize_ = targetSize;
      }
      else
      {
        vBuffer_.SubData(pIt->second.size() * sizeof(PointType), pIt->second.data());
        cBuffer_.SubData(cIt->second.size() * sizeof(ColorType), cIt->second.data());
      }
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    valid_ = true;
  }
}

//------------------------------------------------------------------------------
void FeaturePointSetRenderer::Draw(dlm::Matrix4f const& matrix) const
{
  viewer.GetTextureStorage().GetTexture(textureId_).Bind();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  viewer.GetShaderStorage().GetShader(shaderIndex_).Use();

  GLHelper::UniformMatrix4fv(projectionMatrix_, matrix);

//  dlm::Vector2u const& windowSize =
//    viewer.GetWindowConfiguration().WindowSize();

  gl::Uniform1f(sizeX_, size_);// / windowSize.X());
  gl::Uniform1f(sizeY_, size_);// / windowSize.Y());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  glDisable(GL_DEPTH_TEST);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  vArray_.Bind();

  glDrawArrays(GL_POINTS, 0, static_cast<GLsizei>(points_.size()));
  
  vArray_.Unbind();

  glDisable(GL_BLEND);

  glEnable(GL_DEPTH_TEST);
}

//------------------------------------------------------------------------------
void FeaturePointSetRenderer::Set(
  dlm::Scene const& scene, dlm::IRepId const initial)
{
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (dlm::uint64 i = 0; i < scene.IRepS().Range(); ++i)
  {
    IRepId const iRepId = IRepId(i);

    if (!scene.IsValid(iRepId)) { continue; }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    IRep const& iRep = scene.Get(iRepId);

    if (!iRep.Reference<FSet>::IsValid()) { continue; }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    FSetId const fSetId = iRep.Reference<FSet>::Get();

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//    auto& points = points_[iRepId];
    auto& colors = colors_[iRepId];

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    for (dlm::uint64 j = 0; j < scene.Get(fSetId).Range(); ++j)
    {
      FRepId const fRepId = FRepId(j);

      if (scene.IsValid(fSetId, fRepId))
      {
        FRep const& fRep = scene.Get(fSetId, fRepId);

//        points.push_back(VF::Convert<GLfloat>(iRep.Flip(fRep)));

        if (fRep.Reference<FTrk>::IsValid())
        {
          if (scene.Get(fRep.Reference<FTrk>::Get()).Reference<ORep>::IsValid())
          {
            colors.push_back(ColorType(0.0f, 0.0f, 1.0f, 1.0f));
          }
          else
          {
            colors.push_back(ColorType(0.0f, 1.0f, 0.0f, 1.0f));
          }
        }
        else
        {
          colors.push_back(ColorType(1.0f, 0.0f, 0.0f, 1.0f));
        }
      }
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  SetCurrent(initial);
}

//------------------------------------------------------------------------------
void FeaturePointSetRenderer::SetCurrent(dlm::IRepId const current)
{
  current_ = current;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  valid_ = false;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
