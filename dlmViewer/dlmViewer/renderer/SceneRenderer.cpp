//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/renderer/SceneRenderer.h"

//------------------------------------------------------------------------------
#include "dlmViewer/Viewer.h"
#include "dlmViewer/GLHelper.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/SceneAssistant.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.inline.h"
#include "dlm/math/Matrix.inline.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Constructor
//------------------------------------------------------------------------------

//==============================================================================
// Accessors
//------------------------------------------------------------------------------

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
void SceneRenderer::Create()
{
  Delete();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  imageSequenceRenderer_.Create();
  imageSequence2DRenderer_.Create();
  //featurePointSetRenderer_.Create();
  cameraPath_.Create();
  closestRenderer_.Create();
  objectPoints_.Create();
  //objectPoints2D_.Create();
}

//------------------------------------------------------------------------------
void SceneRenderer::Delete()
{
  for (auto it = cameras_.begin(); it != cameras_.end(); ++it)
  {
    it->second.Delete();
  }

  cameras_.clear();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  imageSequenceRenderer_.Delete();
  imageSequence2DRenderer_.Delete();
  //featurePointSetRenderer_.Delete();
  cameraPath_.Delete();
  closestRenderer_.Delete();
  objectPoints_.Delete();
  //objectPoints2D_.Delete();
}

//------------------------------------------------------------------------------
void SceneRenderer::Update(dlm::uint64 const delta)
{
  for (auto it = cameras_.begin(); it != cameras_.end(); ++it)
  {
    it->second.Update(delta);
  }

  imageSequenceRenderer_.Update(delta);
  imageSequence2DRenderer_.Update(delta);
  //featurePointSetRenderer_.Update(delta);
  cameraPath_.Update(delta);
  closestRenderer_.Update(delta);
  objectPoints_.Update(delta);
  //objectPoints2D_.Update(delta);
}

//------------------------------------------------------------------------------
void SceneRenderer::Draw(dlm::Matrix4f const& matrix) const
{
  {
    auto it = cameras_.find(current_);

    if (it != cameras_.cend())
    {
//      it->second.Draw(matrix);
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  imageSequenceRenderer_.Draw(matrix);
  cameraPath_.Draw(matrix);
  closestRenderer_.Draw(matrix);
  objectPoints_.Draw(matrix);
//  objectPoints2D_.Draw(matrix);
}

//------------------------------------------------------------------------------
void SceneRenderer::Draw2D(dlm::Matrix4f const&) const
{
//  imageSequence2DRenderer_.Draw(matrix);
  //objectPoints2D_.Draw(matrix);
  //featurePointSetRenderer_.Draw(matrix);
}

//------------------------------------------------------------------------------
void SceneRenderer::Set(dlm::Scene const& scene, dlm::IRepId const current)
{
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  current_ = current;
  range_   = dlm::IRepId(scene.IRepS().Range());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Create();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (dlm::uint64 i = 0; i < scene.IRepS().Range(); ++i)
  {
    IRepId const iRepId = IRepId(i);

    if (!scene.IsValid(iRepId)) { continue; }
    
    if (!scene.Get(iRepId).dlm::Reference<dlm::CRep>::IsValid()) { continue; }
    if (!scene.Get(iRepId).dlm::Reference<dlm::TSeq>::IsValid()) { continue; }

    {
      CameraRenderer camera;

      camera.Create();
      camera.Set(scene, iRepId);

      cameras_[iRepId] = camera;
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//    IRep const& iRep = scene.Get(iRepId);

//    CRep const& cRep = scene.Get(iRep.Reference<CRep>::Get());
    
//    Transformation t = SA::Collapse(scene, iRep.Reference<TSeq>::Get());

  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  imageSequenceRenderer_.Set(scene, current);
  imageSequence2DRenderer_.Set(scene, current);
  //featurePointSetRenderer_.Set(scene, current);
  cameraPath_.Set(scene);
  objectPoints_.Set(scene);
  //objectPoints2D_.Set(scene, current);
}

//------------------------------------------------------------------------------
void SceneRenderer::Set(
  dlm::Scene const& scene,
  std::vector<int> const& closest)
{
  closestRenderer_.Set(scene, closest);
}

//------------------------------------------------------------------------------
void SceneRenderer::SetSelected(
  std::set<dlm::FTrkId> const& selected)
{
  objectPoints_.SetSelected(selected);
}

//------------------------------------------------------------------------------
void SceneRenderer::Next()
{
  if (current_ != dlm::IRepId(0))
  {
    current_ = dlm::IRepId(current_.Get() - 1);
    //objectPoints2D_.SetCurrent(current_);
    imageSequenceRenderer_.SetCurrent(current_);
    imageSequence2DRenderer_.SetCurrent(current_);
    //featurePointSetRenderer_.SetCurrent(current_);
  }
}

//------------------------------------------------------------------------------
void SceneRenderer::Previous()
{
  if (range_ != dlm::IRepId(0) && current_ != dlm::IRepId(range_.Get() - 1))
  {
    current_ = dlm::IRepId(current_.Get() + 1);
    //objectPoints2D_.SetCurrent(current_);
    imageSequenceRenderer_.SetCurrent(current_);
    imageSequence2DRenderer_.SetCurrent(current_);
    //featurePointSetRenderer_.SetCurrent(current_);
  }
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
