//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2014  TrackMen Ltd.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/renderer/BoxRenderer.h"

//------------------------------------------------------------------------------
#include "dlmViewer/Viewer.h"
#include "dlmViewer/GLHelper.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"

#include "dlm/math/OStream.h"

#include <iostream>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Constructor
//------------------------------------------------------------------------------
BoxRenderer::BoxRenderer()
  : vertices_   ()
  , normals_    ()
  , colors_     ()
  , shaderIndex_(0    )
  , projection_ (0    )
  , normalMat_  (0    )
  , valid_      (false)
{}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
void BoxRenderer::Create()
{
  Delete();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  {
    ShaderStorage::ShaderInformation shaderInfo;
    shaderInfo.vertShader = "shader/BoxRenderer.vert";
    shaderInfo.fragShader = "shader/BoxRenderer.frag";
    shaderInfo.reference  = "BoxRenderer";
    shaderInfo.attribLocations.push_back(
      ShaderStorage::AttribLocationInfo(0, "InVertex"));
    shaderInfo.attribLocations.push_back(
      ShaderStorage::AttribLocationInfo(1, "InColor"));
    shaderInfo.attribLocations.push_back(
      ShaderStorage::AttribLocationInfo(2, "InNormal"));

    shaderIndex_ = viewer.GetShaderStorage().AddShader(shaderInfo);
  }

  GLWrapper::GLProgram const& program =
    viewer.GetShaderStorage().GetShader(shaderIndex_);

  projection_ = program.GetUniformLocation("ModelviewProjectionMatrix");

  normalMat_ = program.GetUniformLocation("NormalMatrix");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  vArray_.Create();

  vBuffer_.Create();
  nBuffer_.Create();
  cBuffer_.Create();

  vArray_.Attach(vBuffer_, 0, 3, GL_DOUBLE);
  vArray_.Attach(nBuffer_, 2, 3, GL_DOUBLE);
  vArray_.Attach(cBuffer_, 1, 4, GL_DOUBLE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Set();
}

//------------------------------------------------------------------------------
void BoxRenderer::Delete()
{
  vArray_.Delete();

  vBuffer_.Delete();
  nBuffer_.Delete();
  cBuffer_.Delete();

  shaderIndex_ = 0;

  projection_ = 0;
  normalMat_  = 0;
}


//------------------------------------------------------------------------------
void BoxRenderer::Update(dlm::uint64 const)
{
  if (!valid_)
  {
    vBuffer_.Data(36 * sizeof(dlm::Vector3f), vertices_);
    nBuffer_.Data(36 * sizeof(dlm::Vector3f), normals_ );
    cBuffer_.Data(36 * sizeof(dlm::Vector4f), colors_  );

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    valid_ = true;
  }
}

//------------------------------------------------------------------------------
void BoxRenderer::Draw(dlm::Matrix4f const& matrix) const
{
  viewer.GetShaderStorage().GetShader(shaderIndex_).Use();

  glh::UniformMatrix4fv(projection_, matrix);

  dlm::Matrix4f const normalMatrix = dlm::MF::Transpose(dlm::MF::Inverse(viewer.GetCameraControl().Modelview()));

  glh::UniformMatrix4fv(normalMat_, normalMatrix);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  vArray_.Bind();

  glDrawArrays(GL_TRIANGLES, 0, 36);
  
  vArray_.Unbind();
}

//------------------------------------------------------------------------------
void BoxRenderer::Set(
  dlm::Vector3f const& size,
  dlm::Vector4f const& color)
{
  valid_ = false;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  vertices_[ 0] = dlm::Vector3f(size.X(), 0.0,      0.0     );
  vertices_[ 1] = dlm::Vector3f(size.X(), size.Y(), 0.0     );
  vertices_[ 2] = dlm::Vector3f(size.X(), size.Y(), size.Z());
  vertices_[ 3] = dlm::Vector3f(size.X(), size.Y(), size.Z());
  vertices_[ 4] = dlm::Vector3f(size.X(), 0.0,      size.Z());
  vertices_[ 5] = dlm::Vector3f(size.X(), 0.0,      0.0     );

  vertices_[ 6] = dlm::Vector3f(size.X(), size.Y(), 0.0     );
  vertices_[ 7] = dlm::Vector3f(0.0,      size.Y(), 0.0     );
  vertices_[ 8] = dlm::Vector3f(0.0,      size.Y(), size.Z());
  vertices_[ 9] = dlm::Vector3f(0.0,      size.Y(), size.Z());
  vertices_[10] = dlm::Vector3f(size.X(), size.Y(), size.Z());
  vertices_[11] = dlm::Vector3f(size.X(), size.Y(), 0.0     );

  vertices_[12] = dlm::Vector3f(0.0,      0.0,      size.Z());
  vertices_[13] = dlm::Vector3f(size.X(), 0.0,      size.Z());
  vertices_[14] = dlm::Vector3f(size.X(), size.Y(), size.Z());
  vertices_[15] = dlm::Vector3f(size.X(), size.Y(), size.Z());
  vertices_[16] = dlm::Vector3f(0.0,      size.Y(), size.Z());
  vertices_[17] = dlm::Vector3f(0.0,      0.0,      size.Z());

  vertices_[18] = dlm::Vector3f(0.0,      size.Y(), 0.0     );
  vertices_[19] = dlm::Vector3f(0.0,      0.0,      0.0     );
  vertices_[20] = dlm::Vector3f(0.0,      0.0,      size.Z());
  vertices_[21] = dlm::Vector3f(0.0,      0.0,      size.Z());
  vertices_[22] = dlm::Vector3f(0.0,      size.Y(), size.Z());
  vertices_[23] = dlm::Vector3f(0.0,      size.Y(), 0.0     );

  vertices_[24] = dlm::Vector3f(0.0,      0.0,      0.0     );
  vertices_[25] = dlm::Vector3f(size.X(), 0.0,      0.0     );
  vertices_[26] = dlm::Vector3f(size.X(), 0.0,      size.Z());
  vertices_[27] = dlm::Vector3f(size.X(), 0.0,      size.Z());
  vertices_[28] = dlm::Vector3f(0.0,      0.0,      size.Z());
  vertices_[29] = dlm::Vector3f(0.0,      0.0,      0.0     );

  vertices_[30] = dlm::Vector3f(size.X(), 0.0,      0.0     );
  vertices_[31] = dlm::Vector3f(0.0,      0.0,      0.0     );
  vertices_[32] = dlm::Vector3f(0.0,      size.Y(), 0.0     );
  vertices_[33] = dlm::Vector3f(0.0,      size.Y(), 0.0     );
  vertices_[34] = dlm::Vector3f(size.X(), size.Y(), 0.0     );
  vertices_[35] = dlm::Vector3f(size.X(), 0.0,      0.0     );

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  static dlm::Vector3f const normals[] =
  {
    dlm::Vector3f( 1.0,  0.0,  0.0),
    dlm::Vector3f( 0.0,  1.0,  0.0),
    dlm::Vector3f( 0.0,  0.0,  1.0),
    dlm::Vector3f(-1.0,  0.0,  0.0),
    dlm::Vector3f( 0.0, -1.0,  0.0),
    dlm::Vector3f( 0.0,  0.0, -1.0)
  };

  for (dlm::uint32 i = 0; i < 6; ++i)
  {
    for (dlm::uint32 j = 0; j < 6; ++j)
    {
      normals_[i * 6 + j] = normals[i];
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (dlm::uint32 i = 0; i < 36; ++i)
  {
    colors_[i] = color;
  }
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
