//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/renderer/ImageSequenceRenderer.h"

//------------------------------------------------------------------------------
#include "dlmViewer/Viewer.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/SceneAssistant.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/VectorFunctions.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
void ImageSequenceRenderer::Create()
{
  Delete();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  renderer_.Create();
}

//------------------------------------------------------------------------------
void ImageSequenceRenderer::Delete()
{
  renderer_.Delete();
}

//------------------------------------------------------------------------------
void ImageSequenceRenderer::Update(dlm::uint64 const delta)
{
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  auto it = data_.find(current_);

  if (it != data_.cend())
  {
    renderer_.SetArea(it->second.lowerLeft, it->second.upperRight);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  renderer_.Update(delta);
}

//------------------------------------------------------------------------------
void ImageSequenceRenderer::Draw(dlm::Matrix4f const& matrix) const
{
  auto it = data_.find(current_);

  if (it != data_.cend())
  {
    viewer.GetTextureStorage().GetTexture(it->second.textureId).Bind();

    renderer_.Draw(matrix * it->second.modelview);
  }
}

//------------------------------------------------------------------------------
void ImageSequenceRenderer::Set(
  dlm::Scene const& scene, dlm::IRepId const current)
{
  // TODO: Clean up old textures!

  using namespace dlm;

  for (dlm::uint64 i = 0; i < scene.IRepS().Range(); ++i)
  {
    IRepId const iRepId = IRepId(i);

    if (!scene.IsValid(iRepId)) { continue; }

    IRep const& iRep = scene.Get(iRepId);

    if (iRep.Filename().empty()) { continue; }
    
    if (!iRep.Reference<CRep>::IsValid()) { continue; }
    if (!iRep.Reference<TSeq>::IsValid()) { continue; }

    Data data;

    Transformation t = SA::Collapse(scene, iRep.Reference<TSeq>::Get());

    DLM_ASSERT(t.Invert(), DLM_RUNTIME_ERROR);

    data.modelview = t.Inverse().Matrix();

    data.filename = iRep.Filename();

    CRep const& cRep = scene.Get(iRep.Reference<CRep>::Get());

    FloatType const f = cRep.FocalLength();

    Matrix4f fOffset;
    fOffset.At(0, 0) = static_cast<FloatType>(1.0);
    fOffset.At(1, 1) = static_cast<FloatType>(1.0);
    fOffset.At(2, 2) = static_cast<FloatType>(1.0);
    fOffset.At(3, 3) = static_cast<FloatType>(1.0);

    fOffset.At(2, 3) = f;

    data.modelview *= fOffset;

//    FloatType const targetWidth  = iRep.Size().X() * iRep.PixelSize().X();
//    FloatType const targetHeight = iRep.Size().Y() * iRep.PixelSize().Y();
//
//    FloatType const hx = cRep.PPO().X();
//    FloatType const hy = cRep.PPO().Y();
//
//    FloatType const left   = (-0.5 * targetWidth  - hx);// * near_scale;
//    FloatType const right  = ( 0.5 * targetWidth  - hx);// * near_scale;
//    FloatType const bottom = (-0.5 * targetHeight + hy);// * near_scale;
//    FloatType const top    = ( 0.5 * targetHeight + hy);// * near_scale;
//
////  data.lowerLeft  = Vector2f(left,  bottom); // Image is flipped with this
////  data.upperRight = Vector2f(right, top   ); // configuration.
//    data.lowerLeft  = Vector2f(left,  top   );
//    data.upperRight = Vector2f(right, bottom);
//
//    TextureStorage::TextureInformation texInfo;
//
//    texInfo.filename  = iRep.Filename();
//    texInfo.reference = iRep.Filename();
//
//    data.textureId = viewer.GetTextureStorage().AddTexture(texInfo);
//
//    data_[iRepId] = data;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  current_ = current;
}

//------------------------------------------------------------------------------
void ImageSequenceRenderer::SetCurrent(dlm::IRepId const current)
{
  current_ = current;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
