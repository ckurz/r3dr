//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmViewer_renderer_ImageSequence2DRenderer_h_
#define dlmViewer_renderer_ImageSequence2DRenderer_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/renderer/ImageRenderer.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlmViewer/TextureStorage.h"
#include "dlmViewer/Renderer.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/Scene.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <vector>
#include <map>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// ImageSequence2DRenderer class
//------------------------------------------------------------------------------
class ImageSequence2DRenderer : public Renderer
{
public:
  //============================================================================
  // Constructor
  //----------------------------------------------------------------------------

  //============================================================================
  // Accessors
  //----------------------------------------------------------------------------

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  void Create();
  void Delete();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Update(dlm::uint64 const delta);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Draw(dlm::Matrix4f const& matrix) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Set(dlm::Scene const& scene, dlm::IRepId const current);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void SetCurrent(dlm::IRepId const current);

private:
  //============================================================================
  // Nested
  //----------------------------------------------------------------------------
  struct Data
  {
    std::string   filename;
    dlm::Vector2u size;
    TextureId     textureId;
  };

  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  ImageRenderer renderer_;
  
  std::map<dlm::IRepId, Data> data_;

  dlm::Vector2u renderSize_;

  dlm::IRepId current_;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
