//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmViewer_CoordinateSystemRenderer_h_
#define dlmViewer_CoordinateSystemRenderer_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "GLWrapper/GLWrapper.h"
#include "GLWrapper/GLVertexArray.h"
#include "GLWrapper/GLBuffer.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlmViewer/Renderer.h"
#include "dlmViewer/Fader.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <vector>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//======================================================================
// CoordinateSystemRenderer class
// draws arrows on the coordinate axes
// there are 6 arrows, 2 for each coordinate axis
//----------------------------------------------------------------------
class CoordinateSystemRenderer : public Renderer, public Fader
{
public:
  //====================================================================
  // Constructor
  //--------------------------------------------------------------------
  CoordinateSystemRenderer();

  //====================================================================
  // Accessors
  //--------------------------------------------------------------------
  const dlm::Vector3d& XColor() const;
  const dlm::Vector3d& YColor() const;
  const dlm::Vector3d& ZColor() const;

  bool DrawX0() const;
  bool DrawX1() const;
  bool DrawY0() const;
  bool DrawY1() const;
  bool DrawZ0() const;
  bool DrawZ1() const;

  //====================================================================
  // Mutators
  //--------------------------------------------------------------------
  dlm::Vector3d& XColor();
  dlm::Vector3d& YColor();
  dlm::Vector3d& ZColor();

  bool& DrawX0();
  bool& DrawX1();
  bool& DrawY0();
  bool& DrawY1();
  bool& DrawZ0();
  bool& DrawZ1();

  //====================================================================
  // Member functions
  //--------------------------------------------------------------------
  void Create();
  void Update(const unsigned delta);
  void Drop();

  void Draw(const dlm::Matrix4d& matrix) const;

  void SetArrowLength    (const double& value); //values can be either
  void SetArrowWidth     (const double& value); //positive or negativ
  void SetArrowBaseLength(const double& value); //to achieve different
  void SetArrowBaseWidth (const double& value); //arrow layouts

  void SetCut(const bool value); //cut first vertex to make it look neat

  void SetX0Offset(const dlm::Vector3d& offset);
  void SetX1Offset(const dlm::Vector3d& offset);
  void SetY0Offset(const dlm::Vector3d& offset);
  void SetY1Offset(const dlm::Vector3d& offset);
  void SetZ0Offset(const dlm::Vector3d& offset);
  void SetZ1Offset(const dlm::Vector3d& offset);

private:
  //====================================================================
  // Member functions
  //--------------------------------------------------------------------
  void UpdateArrows();

  //====================================================================
  // Member variables
  //--------------------------------------------------------------------
  dlm::Vector3d vertices_[30]; //6 arrows, 5 vertices each

  dlm::Vector3d offsetX0_;
  dlm::Vector3d offsetX1_;
  dlm::Vector3d offsetY0_;
  dlm::Vector3d offsetY1_;
  dlm::Vector3d offsetZ0_;
  dlm::Vector3d offsetZ1_;

  dlm::Vector3d xColor_;
  dlm::Vector3d yColor_;
  dlm::Vector3d zColor_;

  double arrowLength_;
  double arrowWidth_;
  double arrowBaseLength_;
  double arrowBaseWidth_;

//  GLuint vArray_;
//  GLuint vBuffer_;
  GLWrapper::GLVertexArray vArray_;
  GLWrapper::GLBuffer vBuffer_;

  dlm::uint64 shaderIndex_;

  GLint projection_;
  GLint color_;

  bool valid_;
  bool cut_;

  bool drawX0_;
  bool drawX1_;
  bool drawY0_;
  bool drawY1_;
  bool drawZ0_;
  bool drawZ1_;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
