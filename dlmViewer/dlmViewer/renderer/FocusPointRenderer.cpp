//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/renderer/FocusPointRenderer.h"

//------------------------------------------------------------------------------
#include "dlmViewer/Viewer.h"
#include "dlmViewer/GLHelper.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.inline.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Constructor
//------------------------------------------------------------------------------
FocusPointRenderer::FocusPointRenderer()
  : vertices_   (     )
  , indices_    (     )
  , shaderIndex_(0    )
  , projection_ (0    )
  , color_      (0    )
  , valid_      (false)
{
  indices_[ 0] = 0;
  indices_[ 1] = 1;
  indices_[ 2] = 2;

  indices_[ 3] = 2;
  indices_[ 4] = 3;
  indices_[ 5] = 0;

  indices_[ 6] = 1;
  indices_[ 7] = 2;
  indices_[ 8] = 5;

  indices_[ 9] = 5;
  indices_[10] = 4;
  indices_[11] = 1;

  indices_[12] = 2;
  indices_[13] = 3;
  indices_[14] = 6;

  indices_[15] = 6;
  indices_[16] = 5;
  indices_[17] = 2;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
void FocusPointRenderer::Create()
{
  Delete();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  {
    ShaderStorage::ShaderInformation shaderInfo;
    shaderInfo.vertShader = "shader/draw3D.vert";
    shaderInfo.fragShader = "shader/draw3D.frag";
    shaderInfo.reference  = "draw3D";
    shaderInfo.attribLocations.push_back(
      ShaderStorage::AttribLocationInfo(0, "InVertex"));

    shaderIndex_ = viewer.GetShaderStorage().AddShader(shaderInfo);
  }

  GLWrapper::GLProgram const& program =
    viewer.GetShaderStorage().GetShader(shaderIndex_);

  projection_ = program.GetUniformLocation("ProjectionModelviewMatrix");
  color_      = program.GetUniformLocation("DrawColor");

  vArray_.Create();

  vBuffer_.Create();
  iBuffer_.Create(GL_ELEMENT_ARRAY_BUFFER);

  vBuffer_.Data(21 * sizeof(GLfloat), vertices_);

  vArray_.Attach(vBuffer_, 0, 3, GL_FLOAT);

  vArray_.Bind();

  iBuffer_.Data(18 * sizeof(GLuint), indices_);
  iBuffer_.Bind(); // The vertex array apparently doesn't like it when the Data
                   // call unbinds the buffer. ;)

  vArray_.Unbind();
}

//------------------------------------------------------------------------------
void FocusPointRenderer::Delete()
{
  vArray_.Delete();

  vBuffer_.Delete();
  iBuffer_.Delete();

  shaderIndex_ = 0;
  
  projection_ = 0;
  color_      = 0;
}

//------------------------------------------------------------------------------
void FocusPointRenderer::Update(dlm::uint64 const delta)
{
  Fader::Update(delta);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (!valid_)
  {
    vBuffer_.SubData(21 * sizeof(GLfloat), vertices_);

    valid_ = true;
  }
}

//------------------------------------------------------------------------------
void FocusPointRenderer::Draw(dlm::Matrix4f const& matrix) const
{
  if (!IsVisible()) { return; }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  viewer.GetShaderStorage().GetShader(shaderIndex_).Use();

  GLHelper::UniformMatrix4fv(projection_, matrix);

  gl::Uniform4f(color_, 0.0f, 0.0f, 0.0f, 0.7f * Alpha());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  vArray_.Bind();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // The FocusPointRenderer is z-fighting with the grid, so do some nifty
  // multi-pass stuff.

  // Prepare for first draw call.
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glEnable(GL_STENCIL_TEST);
  glStencilFunc(GL_EQUAL, 0, 1);

  glDepthMask(GL_FALSE);

  glDrawElements(GL_TRIANGLES, 18, GL_UNSIGNED_INT, 0);

  glDepthMask(GL_TRUE);

  // Prepare for second draw call.
  glStencilFunc(GL_EQUAL, 1, 1);

  glDisable(GL_DEPTH_TEST);

  glDrawElements(GL_TRIANGLES, 18, GL_UNSIGNED_INT, 0);

  glEnable(GL_DEPTH_TEST);

  glDisable(GL_STENCIL_TEST);
  glDisable(GL_BLEND);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  vArray_.Unbind();
}

//------------------------------------------------------------------------------
void FocusPointRenderer::Set(dlm::Vector3f const& focusPoint)
{
  vertices_[ 3] = static_cast<GLfloat>(focusPoint.X());
//vertices_[ 4] = 0.0f;
//vertices_[ 5] = 0.0f;

  vertices_[ 6] = static_cast<GLfloat>(focusPoint.X());
  vertices_[ 7] = static_cast<GLfloat>(focusPoint.Y());
//vertices_[ 8] = 0.0f;

//vertices_[ 9] = 0.0f;
  vertices_[10] = static_cast<GLfloat>(focusPoint.Y());
//vertices_[11] = 0.0f;

  vertices_[12] = static_cast<GLfloat>(focusPoint.X());
//vertices_[13] = 0.0f;
  vertices_[14] = static_cast<GLfloat>(focusPoint.Z());

  vertices_[15] = static_cast<GLfloat>(focusPoint.X());
  vertices_[16] = static_cast<GLfloat>(focusPoint.Y());
  vertices_[17] = static_cast<GLfloat>(focusPoint.Z());

//vertices_[18] = 0.0f;
  vertices_[19] = static_cast<GLfloat>(focusPoint.Y());
  vertices_[20] = static_cast<GLfloat>(focusPoint.Z());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  valid_ = false;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
