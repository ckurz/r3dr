//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmViewer_renderer_FeaturePointSetRenderer_h_
#define dlmViewer_renderer_FeaturePointSetRenderer_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "GLWrapper/GLWrapper.h"
#include "GLWrapper/GLVertexArray.h"
#include "GLWrapper/GLBuffer.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlmViewer/Renderer.h"
#include "dlmViewer/TextureStorage.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/Scene.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <vector>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// FeaturePointSetRenderer class
//------------------------------------------------------------------------------
class FeaturePointSetRenderer : public Renderer
{
public:
  //============================================================================
  // Constructors
  //----------------------------------------------------------------------------
  FeaturePointSetRenderer();

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  void Create();
  void Delete();

  void Update(dlm::uint64 const delta);

  void Draw(dlm::Matrix4f const& matrix) const;

  void Set(dlm::Scene const& scene, dlm::IRepId const current);

  void SetCurrent(dlm::IRepId const current);



//  void SetActive  (const std::vector<unsigned>& active  );
//  void SetSelected(const std::vector<unsigned>& selected);

private:
  //============================================================================
  // Nested
  //----------------------------------------------------------------------------
  typedef dlm::Vector<2, GLfloat> PointType;
  typedef dlm::Vector<4, GLfloat> ColorType;

  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  std::map<dlm::IRepId, std::vector<PointType> > points_;
  std::map<dlm::IRepId, std::vector<ColorType> > colors_;

  ColorType baseColor_;

  GLWrapper::GLVertexArray vArray_;

  GLWrapper::GLBuffer vBuffer_;
  GLWrapper::GLBuffer cBuffer_;

  dlm::uint64 shaderIndex_;
  TextureId textureId_;

  dlm::uint64 bufferSize_;

  dlm::IRepId current_;

  GLfloat size_;

  GLint projectionMatrix_;
  GLint sizeX_;
  GLint sizeY_;

  bool valid_;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
