//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/renderer/ImageRenderer.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlmViewer/Viewer.h"
#include "dlmViewer/GLHelper.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.inline.h"
#include "dlm/math/Matrix.inline.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Constructor
//------------------------------------------------------------------------------
ImageRenderer::ImageRenderer()
  : vertices_   ( )
  , texCoords_  ( )
  , shaderIndex_(0)
  , projection_ (0)
{
  texCoords_[0] = 1.0f;
  texCoords_[1] = 0.0f;

  texCoords_[2] = 1.0f;
  texCoords_[3] = 1.0f;

  texCoords_[4] = 0.0f;
  texCoords_[5] = 0.0f;

  texCoords_[6] = 0.0f;
  texCoords_[7] = 1.0f;
}

//==============================================================================
// Accessors
//------------------------------------------------------------------------------
dlm::Vector2f const& ImageRenderer::ImageSize() const
{
  return imageSize_;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
void ImageRenderer::Create()
{
  Delete();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  {
    ShaderStorage::ShaderInformation shaderInfo;
    shaderInfo.vertShader = "shader/imageRenderer.vert";
    shaderInfo.fragShader = "shader/imageRenderer.frag";
    shaderInfo.reference  = "imageRenderer";
    shaderInfo.attribLocations.push_back(
      ShaderStorage::AttribLocationInfo(0, "InVertex"));
    shaderInfo.attribLocations.push_back(
      ShaderStorage::AttribLocationInfo(1, "InTexCoord"));

    shaderIndex_ = viewer.GetShaderStorage().AddShader(shaderInfo);
  }

  GLWrapper::GLProgram const& program =
    viewer.GetShaderStorage().GetShader(shaderIndex_);

  projection_ = program.GetUniformLocation("ProjectionModelviewMatrix");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  vBuffer_.Create();
  tBuffer_.Create();

  vBuffer_.Data(8 * sizeof(GLfloat), vertices_ );
  tBuffer_.Data(8 * sizeof(GLfloat), texCoords_);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  vArray_.Create();

  vArray_.Attach(vBuffer_, 0, 2, GL_FLOAT);
  vArray_.Attach(tBuffer_, 1, 2, GL_FLOAT);
}

//------------------------------------------------------------------------------
void ImageRenderer::Delete()
{
  tBuffer_.Delete();
  vBuffer_.Delete();

  vArray_.Delete();
}

//------------------------------------------------------------------------------
void ImageRenderer::Draw(dlm::Matrix4f const& matrix) const
{
  GLWrapper::GLProgram const& program =
    viewer.GetShaderStorage().GetShader(shaderIndex_);

  program.Use();

  glh::UniformMatrix4fv(projection_, matrix);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  vArray_.Bind();

  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

  vArray_.Unbind();
}

//------------------------------------------------------------------------------
void ImageRenderer::SetArea(
  dlm::Vector2d const& lowerLeft, dlm::Vector2d const& upperRight)
{
  imageSize_ = upperRight - lowerLeft;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  vertices_[0] = static_cast<GLfloat>(upperRight.X());
  vertices_[1] = static_cast<GLfloat>( lowerLeft.Y());

  vertices_[2] = static_cast<GLfloat>(upperRight.X());
  vertices_[3] = static_cast<GLfloat>(upperRight.Y());

  vertices_[4] = static_cast<GLfloat>( lowerLeft.X());
  vertices_[5] = static_cast<GLfloat>( lowerLeft.Y());

  vertices_[6] = static_cast<GLfloat>( lowerLeft.X());
  vertices_[7] = static_cast<GLfloat>(upperRight.Y());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  vBuffer_.SubData(8 * sizeof(GLfloat), vertices_);
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
