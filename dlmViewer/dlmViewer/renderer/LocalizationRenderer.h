//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2014  TrackMen Ltd.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmViewer_LocalizationRenderer_h_
#define dlmViewer_LocalizationRenderer_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "GLWrapper/GLWrapper.h"
#include "GLWrapper/GLVertexArray.h"
#include "GLWrapper/GLBuffer.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlmViewer/Renderer.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/Scene.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Types.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <vector>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// LocalizationRenderer class
//------------------------------------------------------------------------------
class LocalizationRenderer : public Renderer
{
public:
  //============================================================================
  // Constructor
  //----------------------------------------------------------------------------
  LocalizationRenderer();

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  void Create();
  void Delete();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Update(dlm::uint64 const);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Draw(dlm::Matrix4f const& matrix) const;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Set(
    dlm::Vector2u const& imageSize,
    dlm::Transformation transformation,
    dlm::Camera const& camera,
    std::vector<dlm::Vector3f> const& points,
    dlm::FloatType const fScale   = static_cast<dlm::FloatType>(1.0),
    dlm::FloatType const sizeHint = static_cast<dlm::FloatType>(0.5));

private:
  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  std::vector<dlm::Vector3f> vertices_;
  std::vector<dlm::Vector4f> colors_;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  GLWrapper::GLVertexArray vArray_;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  GLWrapper::GLBuffer vBuffer_;
  GLWrapper::GLBuffer cBuffer_;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  dlm::uint64 shaderIndex_;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  GLint projection_;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool valid_;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
