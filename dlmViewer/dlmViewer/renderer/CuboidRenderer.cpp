//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/renderer/CuboidRenderer.h"

//------------------------------------------------------------------------------
#include "dlmViewer/Viewer.h"
#include "dlmViewer/GLHelper.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/entity/Transformation.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.inline.h"

#include <array>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Constructor
//------------------------------------------------------------------------------
CuboidRenderer::CuboidRenderer()
  : indices_()
{
  indices_[ 0] = 0;
  indices_[ 1] = 1;
  indices_[ 2] = 2;

  indices_[ 3] = 2;
  indices_[ 4] = 3;
  indices_[ 5] = 0;

  indices_[ 6] = 1;
  indices_[ 7] = 2;
  indices_[ 8] = 5;

  indices_[ 9] = 5;
  indices_[10] = 4;
  indices_[11] = 1;

  indices_[12] = 2;
  indices_[13] = 3;
  indices_[14] = 6;

  indices_[15] = 6;
  indices_[16] = 5;
  indices_[17] = 2;

  indices_[18] = 5;
  indices_[19] = 6;
  indices_[20] = 7;

  indices_[21] = 7;
  indices_[22] = 4;
  indices_[23] = 5;

  indices_[24] = 7;
  indices_[25] = 0;
  indices_[26] = 1;

  indices_[27] = 1;
  indices_[28] = 4;
  indices_[29] = 7;

  indices_[30] = 3;
  indices_[31] = 0;
  indices_[32] = 6;

  indices_[33] = 6;
  indices_[34] = 0;
  indices_[35] = 7;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
void CuboidRenderer::Create()
{
  Delete();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  {
    ShaderStorage::ShaderInformation shaderInfo;
    shaderInfo.vertShader = "shader/draw3D.vert";
    shaderInfo.fragShader = "shader/draw3D.frag";
    shaderInfo.reference  = "draw3D";
    shaderInfo.attribLocations.push_back(
      ShaderStorage::AttribLocationInfo(0, "InVertex"));

    shaderIndex_ = viewer.GetShaderStorage().AddShader(shaderInfo);
  }

  GLWrapper::GLProgram const& program =
    viewer.GetShaderStorage().GetShader(shaderIndex_);

  projection_ = program.GetUniformLocation("ProjectionModelviewMatrix");
  color_      = program.GetUniformLocation("DrawColor");

  vArray_.Create();

  vBuffer_.Create();
  iBuffer_.Create(GL_ELEMENT_ARRAY_BUFFER);

  vArray_.Attach(vBuffer_, 0, 3, GL_FLOAT);

  vArray_.Bind();

  iBuffer_.Data(36 * sizeof(GLuint), indices_);
  iBuffer_.Bind(); // The vertex array apparently doesn't like it when the Data
                   // call unbinds the buffer. ;)

  vArray_.Unbind();
}

//------------------------------------------------------------------------------
void CuboidRenderer::Delete()
{
  vArray_.Delete();

  vBuffer_.Delete();
  iBuffer_.Delete();

  shaderIndex_ = 0;

  projection_ = 0;
  color_      = 0;
}

//------------------------------------------------------------------------------
void CuboidRenderer::Update(dlm::uint64 const)
{
  if (!valid_)
  {
    vBuffer_.Data(sizeof(GLfloat) * vertices_.size(), vertices_.data());

    valid_ = true;
  }
}

//------------------------------------------------------------------------------
void CuboidRenderer::Draw(dlm::Matrix4f const& matrix) const
{
  viewer.GetShaderStorage().GetShader(shaderIndex_).Use();

  GLHelper::UniformMatrix4fv(projection_, matrix);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  vArray_.Bind();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Prepare for first draw call.
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glDepthMask(GL_FALSE);

  for (auto i = 0; i < static_cast<int>(colors_.size()); ++i)
  {
    auto const& color = colors_[i];

    gl::Uniform4f(
      color_,
      static_cast<GLfloat>(color.X()),
      static_cast<GLfloat>(color.Y()),
      static_cast<GLfloat>(color.Z()),
      static_cast<GLfloat>(color.W()));

    gl::DrawElementsBaseVertex(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0, i * 8);
  }

  glDepthMask(GL_TRUE);

  glDisable(GL_BLEND);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  vArray_.Unbind();
}

//------------------------------------------------------------------------------
void
CuboidRenderer::Set(
  std::vector<std::tuple<dlm::Transformation, dlm::Vector3f, dlm::Vector4f>> const& cuboids)
{
  colors_.clear();

  vertices_.clear();

  vertices_.reserve(cuboids.size() * 8);

  for (auto i = 0; i < static_cast<int>(cuboids.size()); ++i)
  {
    auto pose = dlm::Transformation{};

    auto sizes = dlm::Vector3f{};
    auto color = dlm::Vector4f{};

    std::tie(pose, sizes, color) = cuboids[i];

    colors_.push_back(color);

    auto points = std::array<dlm::Vector3f, 8>();

    points[0] = pose * dlm::Vector3f{ sizes.X(),  sizes.Y(), -sizes.Z()};
    points[1] = pose * dlm::Vector3f{-sizes.X(),  sizes.Y(), -sizes.Z()};
    points[2] = pose * dlm::Vector3f{-sizes.X(), -sizes.Y(), -sizes.Z()};
    points[3] = pose * dlm::Vector3f{ sizes.X(), -sizes.Y(), -sizes.Z()};
    points[4] = pose * dlm::Vector3f{-sizes.X(),  sizes.Y(),  sizes.Z()};
    points[5] = pose * dlm::Vector3f{-sizes.X(), -sizes.Y(),  sizes.Z()};
    points[6] = pose * dlm::Vector3f{ sizes.X(), -sizes.Y(),  sizes.Z()};
    points[7] = pose * dlm::Vector3f{ sizes.X(),  sizes.Y(),  sizes.Z()};

    for (auto j = 0; j < 8; ++j)
    {
      auto const& point = points[j];

      vertices_.push_back(static_cast<GLfloat>(point.X()));
      vertices_.push_back(static_cast<GLfloat>(point.Y()));
      vertices_.push_back(static_cast<GLfloat>(point.Z()));
    }
  }

  valid_ = false;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
