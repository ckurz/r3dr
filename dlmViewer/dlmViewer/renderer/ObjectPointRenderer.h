//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmViewer_ObjectPointRenderer_h_
#define dlmViewer_ObjectPointRenderer_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "GLWrapper/GLWrapper.h"
#include "GLWrapper/GLVertexArray.h"
#include "GLWrapper/GLBuffer.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlmViewer/Renderer.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/Scene.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <set>
#include <vector>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// ObjectPointRenderer
//------------------------------------------------------------------------------
class ObjectPointRenderer : public Renderer
{
public:
  //============================================================================
  // Constructor
  //----------------------------------------------------------------------------
  ObjectPointRenderer();

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  void Create();
  void Delete();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Update(dlm::uint64 const);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Draw(dlm::Matrix4f const& matrix) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Set                        (dlm::Scene const& scene);
  void SetReferencedObjectPoints  (dlm::Scene const& scene);
  void SetUnreferencedObjectPoints(dlm::Scene const& scene);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void SetVisible    (std::vector<dlm::FTrkId> const& indices);
  void SetProjectable(std::vector<dlm::FTrkId> const& indices);
  void SetSelected   (std::vector<dlm::FTrkId> const& indices);

  void SetSelected(std::set<dlm::FTrkId> const& indices);
  
private:
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  void UpdateColors();

  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  std::vector<dlm::Vector3f> points_;
  std::vector<dlm::Vector3f> colors_;

  std::vector<dlm::Vector3f> unreferencedPoints_;
  std::vector<dlm::Vector3f> unreferencedColors_;

  std::vector<dlm::FTrkId> projectable_;
  std::vector<dlm::FTrkId> visible_;
  std::vector<dlm::FTrkId> selected_;

  std::vector<dlm::uint64> fTrkIdConversion_;

  dlm::Vector3f unreferencedColor_;
  dlm::Vector3f   referencedColor_;
  dlm::Vector3f  projectableColor_;
  dlm::Vector3f      visibleColor_;
  dlm::Vector3f     selectedColor_;

  GLWrapper::GLBuffer unreferencedPointsBuffer_;
  GLWrapper::GLBuffer unreferencedColorsBuffer_;
  GLWrapper::GLBuffer             pointsBuffer_;
  GLWrapper::GLBuffer             colorsBuffer_;

  GLWrapper::GLVertexArray unreferencedPointsVertexArray_;
  GLWrapper::GLVertexArray             pointsVertexArray_;

  dlm::uint64 shaderIndex_;

  GLint projection_;

  bool valid_;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
