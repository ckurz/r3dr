//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmViewer_FocusPointRenderer_h_
#define dlmViewer_FocusPointRenderer_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "GLWrapper/GLWrapper.h"
#include "GLWrapper/GLVertexArray.h"
#include "GLWrapper/GLBuffer.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlmViewer/Renderer.h"
#include "dlmViewer/Fader.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//#include <vector>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// FocusPointRenderer class
//------------------------------------------------------------------------------
class FocusPointRenderer : public Renderer, public Fader
{
public:
  //============================================================================
  // Constructor
  //----------------------------------------------------------------------------
  FocusPointRenderer();

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  void Create();
  void Delete();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Update(dlm::uint64 const delta);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Draw(dlm::Matrix4f const& matrix) const;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Set(dlm::Vector3f const& focusPoint);

private:
  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  GLfloat vertices_[21];
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  GLuint indices_[18];
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  GLWrapper::GLVertexArray vArray_;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  GLWrapper::GLBuffer vBuffer_;
  GLWrapper::GLBuffer iBuffer_;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  dlm::uint64 shaderIndex_;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  GLint projection_;
  GLint color_;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool valid_;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
