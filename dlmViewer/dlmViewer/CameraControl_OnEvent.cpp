//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/CameraControl.h"

//------------------------------------------------------------------------------
#include "dlmViewer/Viewer.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/QuaternionFunctions.h"


#include <iostream>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
GLSubsystem::Receiver* CameraControl::OnEvent(
  GLSubsystem::Event const& inputEvent)
{
  if (inputEvent.type == GLSubsystem::Event::ResizeEvent)
  {
    windowSize_.X() = static_cast<dlm::FloatType>(inputEvent.Resize.width );
    windowSize_.Y() = static_cast<dlm::FloatType>(inputEvent.Resize.height);
  
    aspectRatio_ = windowSize_.X() / windowSize_.Y();

    UpdateProjectionMatrices();
//    UpdateCustomConfiguration();

    return nullptr;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (inputEvent.type == GLSubsystem::Event::KeyboardEvent)
  {
    if (inputEvent.Keyboard.type == GLSubsystem::Event::KeyboardEventType::Press)
    {
      return HandleKeyPress(inputEvent);
    }
    else
    {
      return HandleKeyRelease(inputEvent);
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (inputEvent.type == GLSubsystem::Event::MouseEvent)
  {
      return HandleMouse(inputEvent);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return nullptr;
}

//------------------------------------------------------------------------------
GLSubsystem::Receiver* CameraControl::HandleKeyPress(
  GLSubsystem::Event const& inputEvent)
{
  if (inputEvent.Keyboard.keyCode == GLSubsystem::KeyCode::KEY_Shift_L)
  {
    modifier_ = true;

    if (viewMode_ == FREE)
    {
      fpRenderer_.FadeIn();
    }

    return nullptr;
  }

  if (Locked())
  {
    return nullptr;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Change between free and static viewmode.
  if (inputEvent.Keyboard.keyCode == GLSubsystem::KeyCode::KEY_T ||
      inputEvent.Keyboard.keyCode == GLSubsystem::KeyCode::KEY_t)
  {
    if (viewMode_ == FREE)
    {
      PrepareTransitionToStatic(currentIndex_);
      InitiateTransition();

      return this;
    }

    if (viewMode_ == STATIC)
    {
      PrepareTransitionToFree();
      InitiateTransition();

      return this;
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Cycle static viewmode.
  if (inputEvent.Keyboard.keyCode == GLSubsystem::KeyCode::KEY_N ||
      inputEvent.Keyboard.keyCode == GLSubsystem::KeyCode::KEY_n)
  {
    if (viewMode_ == STATIC)
    {
      PrepareTransitionToStatic((currentIndex_ + 1) % 3);
      InitiateTransition();

      return this;
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return nullptr;
}

//------------------------------------------------------------------------------
GLSubsystem::Receiver* CameraControl::HandleKeyRelease(GLSubsystem::Event const& inputEvent)
{
  if (inputEvent.Keyboard.keyCode == GLSubsystem::KeyCode::KEY_Shift_L)
  {
    modifier_ = false;

    if (viewMode_ == FREE)
    {
      fpRenderer_.FadeOut();
    }

    return nullptr;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return nullptr;
}

//------------------------------------------------------------------------------
GLSubsystem::Receiver* CameraControl::HandleMouse(GLSubsystem::Event const& inputEvent)
{
  if (inputEvent.Mouse.type == GLSubsystem::Event::MouseEventType::LeftButtonUp)
  {
    if (move_)
    {
      move_ = false;

      viewer.ReleaseMouse(this);

      return this;
    }

    if (movePivot_)
    {
      movePivot_ = false;

      viewer.ReleaseMouse(this);

      return this;
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (Locked())
  {
    return nullptr;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (inputEvent.Mouse.type == GLSubsystem::Event::MouseEventType::Move)
  {
    return HandleMouseMove(inputEvent);
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (inputEvent.Mouse.type == GLSubsystem::Event::MouseEventType::LeftButtonDown)
  {
    if (viewMode_ == ViewMode::STATIC)
    {
      if (overPivot_)
      {
        viewer.GrabMouse(this);
    
        movePivot_ = true;

        return this;
      }
    }

    if (viewMode_ == ViewMode::FREE || viewMode_ == ViewMode::STATIC)
    {
      viewer.GrabMouse(this);

      move_ = true;

      return this;
    }


  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (inputEvent.Mouse.type == GLSubsystem::Event::MouseEventType::Wheel)
  {
    if (!modifier_)
    {
      if (inputEvent.Mouse.wheelMotion > 0)
      {
        cameraDistance_ *= static_cast<dlm::FloatType>(0.8);
      }
      else if (inputEvent.Mouse.wheelMotion < 0)
      {
        cameraDistance_ /= static_cast<dlm::FloatType>(0.8);
      }

      UpdateProjectionMatrices();
      //UpdateCustomConfiguration();

      return this;
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return nullptr;
}

//------------------------------------------------------------------------------
GLSubsystem::Receiver* CameraControl::HandleMouseMove(
  GLSubsystem::Event const&)// inputEvent)
{
  Mouse const& mouse = viewer.GetMouse();

  UpdatePivotSelection(mouse.Position());

  lastMousePosition_ = mouse.Position();

  if (move_)
  {
    if (viewMode_ == ViewMode::FREE)
    {
      if (modifier_)
      {
        MoveFocus(mouse.Offset());
      }
      else
      {
        RotateView(mouse.Offset());
      }
    }

    if (viewMode_ == ViewMode::STATIC)
    {
      if (modifier_)
      {
        RotateScene(mouse.Offset());
      }
      else
      {
        MoveScene(mouse.Offset());
        MovePivot(mouse.Offset());
      }
    }
    
    return this;
  }

  if (movePivot_)
  {
    MovePivot(mouse.Offset());

    return this;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return nullptr;
}

//------------------------------------------------------------------------------
void CameraControl::RotateView(dlm::Vector2f offset)
{
  offset *= 0.01;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  cameraAngles_.At(1) -= offset.Y();
  cameraAngles_.At(2) += offset.X();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (cameraAngles_.At(1) < -M_PI)
  {
    cameraAngles_.At(1) += 2.0 * M_PI;
  }
  else if (cameraAngles_.At(1) > M_PI)
  {
    cameraAngles_.At(1) -= 2.0 * M_PI;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (cameraAngles_.At(2) < -M_PI)
  {
    cameraAngles_.At(2) += 2.0 * M_PI;
  }
  else if (cameraAngles_.At(2) > M_PI)
  {
    cameraAngles_.At(2) -= 2.0 * M_PI;
  }

//   dlm::core::Quatd rotZ;
//   rotZ.Xyz() = dlm::core::Vector3d(0.0, 0.0, 1.0);
//   rotZ.W()   = -offset.X();
//   rotZ.ConvertToAngleAxis();
//
//   dlm::core::Quatd rotX;
//   rotX.Xyz() = currentView_ * dlm::core::Vector3d(1.0, 0.0, 0.0);
//   rotX.W()   = offset.Y();
//   rotX.ConvertToAngleAxis();
//
//   currentView_ = rotZ * rotX * currentView_;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Invalidate();
}

//------------------------------------------------------------------------------
void CameraControl::RotateScene(dlm::Vector2f offset)
{
  offset *= 0.01;

  dlm::Vector3f axis;
  dlm::FloatType angle = static_cast<dlm::FloatType>(0.0);

  if      (currentIndex_ == 0)
  {
    axis.X() = 1.0;
    angle    = -offset.X();
  }
  else if (currentIndex_ == 1)
  {
    axis.Y() = 1.0;
    angle    = offset.X();
  }
  else
  {
    axis.Z() = 1.0;
    angle    = -offset.X();
  }

  dlm::Quatd rotation = dlm::QF::FromAxisAngle(axis, angle);

  //rotate scene translation around pivot point
  sceneTranslation_ -= scenePivot_;
  sceneTranslation_  = rotation * sceneTranslation_;
  sceneTranslation_ += scenePivot_;

  sceneRotation_ = rotation * sceneRotation_;

//  UpdateCustomConfiguration();

  valid_ = false;
}

//------------------------------------------------------------------------------
void CameraControl::MoveFocus(dlm::Vector2f offset)
{
  offset.X() /= windowSize_.X();
  offset.Y() /= windowSize_.Y();
  offset.X() /= f_ / aspectRatio_ * zNear_ / cameraDistance_;
  offset.Y() /= f_                * zNear_ / cameraDistance_;
  offset.X() *= 2.0;
  offset.Y() *= 2.0;

  dlm::Vector3f projectedOffset;
  projectedOffset.X() =  offset.X();
  projectedOffset.Y() =  offset.Y();

  projectedOffset = current_.view * projectedOffset;

  if (cameraAngles_.At(1) <  M_PI_4 &&
      cameraAngles_.At(1) > -M_PI_4)
  {
    projectedOffset.Z() = 0.0;
  }
  else
  {
    projectedOffset.X() = 0.0;
    projectedOffset.Y() = 0.0;
  }

  current_.focus += projectedOffset;

  fpRenderer_.Set(-current_.focus);

  valid_ = false;
}

//------------------------------------------------------------------------------
void CameraControl::MoveScene(dlm::Vector2f offset)
{
//  MovePivot(offset); //move the pivot with the scene
//                     //and do so before messing with the offset

  offset.X() /= windowSize_.X();
  offset.Y() /= windowSize_.Y();
  offset.X() /= f_ / aspectRatio_ * zNear_ / cameraDistance_;
  offset.Y() /= f_                * zNear_ / cameraDistance_;
  offset.X() *= 2.0;
  offset.Y() *= 2.0;

  if      (currentIndex_ == 0)
  {
    sceneTranslation_.Y() += offset.X();
    sceneTranslation_.Z() += offset.Y();
  }
  else if (currentIndex_ == 1)
  {
    sceneTranslation_.X() += offset.X();
    sceneTranslation_.Z() += offset.Y();
  }
  else // (currentIndex_ == 2)
  {
    sceneTranslation_.X() += offset.X();
    sceneTranslation_.Y() += offset.Y();
  }

//  UpdateCustomConfiguration();

  Invalidate();
}

//------------------------------------------------------------------------------
void CameraControl::MovePivot(dlm::Vector2f offset)
{
  offset.X() /= windowSize_.X();
  offset.Y() /= windowSize_.Y();
  
  offset.X() /= f_ / aspectRatio_ * zNear_ / cameraDistance_;
  offset.Y() /= f_                * zNear_ / cameraDistance_;
  
  offset.X() *= static_cast<dlm::FloatType>(2.0);
  offset.Y() *= static_cast<dlm::FloatType>(2.0);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (currentIndex_ == 0)
  {
    scenePivot_.Y() += offset.X();
    scenePivot_.Z() += offset.Y();
  }
  else if (currentIndex_ == 1)
  {
    scenePivot_.X() += offset.X();
    scenePivot_.Z() += offset.Y();
  }
  else
  {
    scenePivot_.X() += offset.X();
    scenePivot_.Y() += offset.Y();
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Invalidate();
}

//------------------------------------------------------------------------------
void CameraControl::UpdatePivotSelection(dlm::Vector2f const& mousePosition)
{
  dlm::FloatType const distance = (mousePosition - pivotPosition_).Magnitude();

  if (static_cast<dlm::FloatType>(distance) < ppRenderer_.Size())
  {
    if (!overPivot_)
    {
      ppRenderer_.Select();
      overPivot_ = true;
    }
  }
  else
  {
    if (overPivot_ && !movePivot_)
    {
      ppRenderer_.Deselect();
      overPivot_ = false;
    }
  }
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
