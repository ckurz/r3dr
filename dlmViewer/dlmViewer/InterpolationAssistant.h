//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmViewer_InterpolationAssistant_h_
#define dlmViewer_InterpolationAssistant_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/math/Vector.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Types.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// InterpolationAssistant class
//------------------------------------------------------------------------------
class InterpolationAssistant
{
public:
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  // Linear interpolation of value from range [low, high] to [0, 1].
  template <typename T>
  static dlm::FloatType MapLinear(
    T const value,
    T const high,
    T const low  = static_cast<T>(0.0));

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Use the cubic Hermite spline basis function H01 to map value in the range
  // [low, high] to a sine-shaped function in the range [0, 1].
  template <typename T>
  static dlm::FloatType H01(
    T const value,
    T const high = static_cast<T>(1.0),
    T const low  = static_cast<T>(0.0));

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <typename T, typename U>
  static dlm::Vector<3, T> Slerp(
    dlm::Vector<3, T> const& a,
    dlm::Vector<3, T> const& b,
    U const value,
    U const high = static_cast<U>(1.0),
    U const low  = static_cast<U>(0.0));
};

//==============================================================================
// Shorthand
//------------------------------------------------------------------------------
typedef InterpolationAssistant IA;

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// Inline code
//------------------------------------------------------------------------------
// This file is likely to be included from source files only, and for
// convenience includes the corresponding inline file automatically.
//------------------------------------------------------------------------------
#include "dlmViewer/InterpolationAssistant.inline.h"

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
