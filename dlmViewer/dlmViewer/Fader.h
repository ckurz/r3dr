//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmViewer_Fader_h_
#define dlmViewer_Fader_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "GLWrapper/GLWrapper.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Types.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Fader class
// Provides the deriving class with a simple interface to control the alpha
// value when the deriving class should be able to fade in and out of view.
//------------------------------------------------------------------------------
class Fader
{
public:
  //============================================================================
  // Nested
  //----------------------------------------------------------------------------
  enum FaderState
  {
    Visible,
    Invisible,
    FadingIn,
    FadingOut
  };

  //============================================================================
  // Constructor
  //----------------------------------------------------------------------------
  Fader(dlm::uint64 const fadingDuration = 200);

  //============================================================================
  // Accessors
  //----------------------------------------------------------------------------
  dlm::uint64 FadingDuration() const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  GLfloat Alpha() const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FaderState CurrentState() const; // Simply 'State' would be a Windows/Linux
                                   // macro naming clash waiting to happen...

  //============================================================================
  // Mutators
  //----------------------------------------------------------------------------
  dlm::uint64& FadingDuration();

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  bool IsVisible() const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Show();
  void Hide();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void FadeIn ();
  void FadeOut();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Toggle();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Update(dlm::uint64 const delta);

private:
  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  dlm::uint64 fadingDuration_; // In case of a duration of longer than 49 days
  dlm::uint64 fadingTime_;     // being absolutely required. ;)
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  GLfloat alpha_;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FaderState state_;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// Inline code
//------------------------------------------------------------------------------
// This file includes the inline header file automatically so that files using
// classes derived from Fader do not have to include it explicitely.
//------------------------------------------------------------------------------
#include "dlmViewer/Fader.inline.h"

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
