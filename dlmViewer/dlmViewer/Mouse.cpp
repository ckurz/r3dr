//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/Mouse.h"

//------------------------------------------------------------------------------
#include "dlm/math/Vector.inline.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
Mouse::Mouse()
  : lButtonDown_(false)
  , rButtonDown_(false)
  , mButtonDown_(false)
  , lDragged_   (false)
  , rDragged_   (false)
  , mDragged_   (false)
{}

//==============================================================================
// Accessors
//------------------------------------------------------------------------------
dlm::Vector2d const& Mouse::Position() const
{
  return position_;
}

//------------------------------------------------------------------------------
dlm::Vector2d const& Mouse::Previous() const
{
  return previous_;
}

//------------------------------------------------------------------------------
dlm::Vector2d const& Mouse::Offset() const
{
  return offset_;
}

//------------------------------------------------------------------------------
dlm::Vector2d const& Mouse::LDragStart() const
{
  return lDragStart_;
}

//------------------------------------------------------------------------------
dlm::Vector2d const& Mouse::RDragStart() const
{
  return rDragStart_;
}

//------------------------------------------------------------------------------
dlm::Vector2d const& Mouse::MDragStart() const
{
  return mDragStart_;
}

//------------------------------------------------------------------------------
bool Mouse::LButtonDown() const
{
  return lButtonDown_;
}

//------------------------------------------------------------------------------
bool Mouse::RButtonDown() const
{
  return rButtonDown_;
}

//------------------------------------------------------------------------------
bool Mouse::MButtonDown() const
{
  return mButtonDown_;
}

//------------------------------------------------------------------------------
bool Mouse::LDragged() const
{
  return lDragged_;
}

//------------------------------------------------------------------------------
bool Mouse::RDragged() const
{
  return rDragged_;
}

//------------------------------------------------------------------------------
bool Mouse::MDragged() const
{
  return mDragged_;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
GLSubsystem::Receiver* Mouse::OnEvent(GLSubsystem::Event const& inputEvent)
{
  if (inputEvent.type == GLSubsystem::Event::ResizeEvent)
  {
    SetWindowSize(inputEvent.Resize.width, inputEvent.Resize.height);
  }

  if (inputEvent.type == GLSubsystem::Event::MouseEvent)
  {
    switch (inputEvent.Mouse.type)
    {
    case GLSubsystem::Event::MouseEventType::Move:
      {
        previous_     = position_;

        position_.X() =                   inputEvent.Mouse.xPosition;
        position_.Y() = windowSize_.Y() - inputEvent.Mouse.yPosition;

        offset_       = position_ - previous_;

        if (lButtonDown_ && !lDragged_)
        {
          lDragged_   = true;
          lDragStart_ = previous_;
        }

        if (rButtonDown_ && !rDragged_)
        {
          rDragged_   = true;
          rDragStart_ = previous_;
        }

        if (mButtonDown_ && !mDragged_)
        {
          mDragged_   = true;
          mDragStart_ = previous_;
        }
      }
      break;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    case GLSubsystem::Event::MouseEventType::LeftButtonDown:
      {
        lButtonDown_ = true;
      }
      break;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    case GLSubsystem::Event::MouseEventType::LeftButtonUp:
      {
        lButtonDown_ = false;
        lDragged_    = false;
        lDragStart_  = dlm::Vector2f();
      }
      break;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    case GLSubsystem::Event::MouseEventType::RightButtonDown:
      {
        rButtonDown_ = true;
      }
      break;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    case GLSubsystem::Event::MouseEventType::RightButtonUp:
      {
        rButtonDown_ = false;
        rDragged_    = false;
        rDragStart_  = dlm::Vector2f();
      }
      break;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    case GLSubsystem::Event::MouseEventType::MiddleButtonDown:
      {
        mButtonDown_ = true;
      }
      break;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    case GLSubsystem::Event::MouseEventType::MiddleButtonUp:
      {
        mButtonDown_ = false;
        mDragged_    = false;
        mDragStart_  = dlm::Vector2f();
      }
      break;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    case GLSubsystem::Event::MouseEventType::Wheel:
      {}
      break;
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return nullptr; // We only keep track of the state.
}

//------------------------------------------------------------------------------
void Mouse::SetWindowSize(int const width, int const height)
{
  windowSize_ = dlm::Vector2f(width, height);
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
