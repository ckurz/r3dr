//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmViewer_Fader_inline_h_
#define dlmViewer_Fader_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/Fader.h"

//------------------------------------------------------------------------------
#include "dlmViewer/InterpolationAssistant.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
inline Fader::Fader(dlm::uint64 const fadingDuration)
  : fadingDuration_(fadingDuration     )
  , fadingTime_    (0                  )
  , alpha_         (1.0f               )
  , state_         (FaderState::Visible)
{}

//==============================================================================
// Accessors
//------------------------------------------------------------------------------
inline dlm::uint64 Fader::FadingDuration() const
{
  return fadingDuration_;
}

//------------------------------------------------------------------------------
inline GLfloat Fader::Alpha() const
{
  return alpha_;
}

//------------------------------------------------------------------------------
inline Fader::FaderState Fader::CurrentState() const
{
  return state_;
}

//==============================================================================
// Mutators
//------------------------------------------------------------------------------
inline dlm::uint64& Fader::FadingDuration()
{
  return fadingDuration_;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
inline bool Fader::IsVisible() const
{
  return state_ != FaderState::Invisible;
}

//------------------------------------------------------------------------------
inline void Fader::Show()
{
  fadingTime_ = 0;
  alpha_      = 1.0f;
  state_      = FaderState::Visible;
}

//------------------------------------------------------------------------------
inline void Fader::Hide()
{
  fadingTime_ = 0;
  alpha_      = 0.0f;
  state_      = FaderState::Invisible;
}

//------------------------------------------------------------------------------
inline void Fader::FadeIn()
{
  switch (state_)
  {
  case FaderState::FadingOut:
    {
      fadingTime_ = fadingDuration_ - fadingTime_;
      // Fall-through is intended at this point.
    }
    [[fallthrough]];
  case FaderState::Invisible:
    {
      state_ = FaderState::FadingIn;
      break;
    }
  default:
    {
      break;
    }
  }
}

//------------------------------------------------------------------------------
inline void Fader::FadeOut()
{
  switch (state_)
  {
  case FaderState::FadingIn:
    {
      fadingTime_ = fadingDuration_ - fadingTime_;
      // Fall-through is intended at this point.
    }
    [[fallthrough]];
  case FaderState::Visible:
    {
      state_ = FaderState::FadingOut;
      break;
    }
  default:
    {
      break;
    }
  }
}

//------------------------------------------------------------------------------
inline void Fader::Toggle()
{
  switch (state_)
  {
  case FaderState::Visible:
  case FaderState::FadingIn:
    {
      FadeOut();
      break;
    }
  case FaderState::Invisible:
  case FaderState::FadingOut:
    {
      FadeIn();
      break;
    }
  }
}

//------------------------------------------------------------------------------
inline void Fader::Update(dlm::uint64 const delta)
{
  if (state_ != FaderState::FadingIn &&
      state_ != FaderState::FadingOut)
  {
    return;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Update the time and check for completion.
  fadingTime_ += delta;

  if (fadingTime_ < fadingDuration_)
  {
    // Update the alpha value.
    alpha_ = static_cast<GLfloat>(IA::H01(fadingTime_, fadingDuration_));

    if (state_ == FaderState::FadingOut)
    {
      alpha_ = 1.0f - alpha_;
    }
  }
  else
  {
    // The transition has finished, so set the appropriate state.
    if (state_ == FaderState::FadingIn)
    {
      Show();
    }
    else
    {
      Hide();
    }
  }
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
