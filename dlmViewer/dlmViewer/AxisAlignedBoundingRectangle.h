//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmViewer_AxisAlignedBoundingRectangle_h_
#define dlmViewer_AxisAlignedBoundingRectangle_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/math/Vector.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Types.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// AxisAlignedBoundingRectangle class
//------------------------------------------------------------------------------
template <typename T>
class AxisAlignedBoundingRectangle
{
public:
  //====================================================================
  // Constructors
  //--------------------------------------------------------------------
  AxisAlignedBoundingRectangle(
    dlm::Vector<2, T> const& lowerLeft  = dlm::Vector<2, T>(),
    dlm::Vector<2, T> const& upperRight = dlm::Vector<2, T>());

  //====================================================================
  // Accessors
  //--------------------------------------------------------------------
  dlm::Vector<2, T> const& LowerLeft () const;
  dlm::Vector<2, T> const& UpperRight() const;

  //====================================================================
  // Mutators
  //--------------------------------------------------------------------
  dlm::Vector<2, T>& LowerLeft ();
  dlm::Vector<2, T>& UpperRight();

  //====================================================================
  // Member functions
  //--------------------------------------------------------------------
  dlm::Vector<2, T> Dimension() const;
  dlm::Vector<2, T> Center   () const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool Contains  (dlm::Vector<2, T>            const& point) const;
  bool Contains  (AxisAlignedBoundingRectangle const& other) const;
  bool Intersects(AxisAlignedBoundingRectangle const& other) const;

private:
  //====================================================================
  // Member variables
  //--------------------------------------------------------------------
  dlm::Vector<2, T> lowerLeft_;
  dlm::Vector<2, T> upperRight_;
};

//==============================================================================
// Type definitions
//------------------------------------------------------------------------------
typedef AxisAlignedBoundingRectangle<dlm::FloatType> AABR;

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
