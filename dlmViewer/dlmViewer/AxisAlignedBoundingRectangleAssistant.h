//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmViewer_AxisAlignedBoundingRectangleAssistant_h_
#define dlmViewer_AxisAlignedBoundingRectangleAssistant_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/AxisAlignedBoundingRectangle.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// AxisAlignedBoundingRectangleAssistant class
//------------------------------------------------------------------------------
class AxisAlignedBoundingRectangleAssistant
{
public:
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  template <typename T>
  static AxisAlignedBoundingRectangle<T>& Expand(
    AxisAlignedBoundingRectangle<T>& aabr, T const factor);
};

//==============================================================================
// Shorthand
//------------------------------------------------------------------------------
typedef AxisAlignedBoundingRectangleAssistant AABRA;

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// Inline code
//------------------------------------------------------------------------------
// This file is likely to be included from source files only, and for
// convenience includes the corresponding inline file automatically.
//------------------------------------------------------------------------------
#include "dlmViewer/AxisAlignedBoundingRectangleAssistant.inline.h"

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
