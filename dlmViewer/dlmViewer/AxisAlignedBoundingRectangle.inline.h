//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmViewer_AxisAlignedBoundingRectangle_inline_h_
#define dlmViewer_AxisAlignedBoundingRectangle_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/AxisAlignedBoundingRectangle.h"

//------------------------------------------------------------------------------
#include "dlm/math/Vector.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Types.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
template <typename T>
inline AxisAlignedBoundingRectangle<T>::AxisAlignedBoundingRectangle(
  dlm::Vector<2, T> const& lowerLeft,
  dlm::Vector<2, T> const& upperRight)
  : lowerLeft_ (lowerLeft )
  , upperRight_(upperRight)
{}

//==============================================================================
// Accessors
//------------------------------------------------------------------------------
template <typename T>
inline dlm::Vector<2, T> const&
  AxisAlignedBoundingRectangle<T>::LowerLeft() const
{
  return lowerLeft_;
}

//------------------------------------------------------------------------------
template <typename T>
inline dlm::Vector<2, T> const&
  AxisAlignedBoundingRectangle<T>::UpperRight() const
{
  return upperRight_;
}

//==============================================================================
// Mutators
//------------------------------------------------------------------------------
template <typename T>
inline dlm::Vector<2, T>& AxisAlignedBoundingRectangle<T>::LowerLeft()
{
  return lowerLeft_;
}

//------------------------------------------------------------------------------
template <typename T>
inline dlm::Vector<2, T>& AxisAlignedBoundingRectangle<T>::UpperRight()
{
  return upperRight_;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
template <typename T>
inline dlm::Vector<2, T> AxisAlignedBoundingRectangle<T>::Dimension() const
{
  return upperRight_ - lowerLeft_;
}

//------------------------------------------------------------------------------
template <typename T>
inline dlm::Vector<2, T> AxisAlignedBoundingRectangle<T>::Center() const
{
  return lowerLeft_ + (Dimension() / static_cast<T>(2.0));
}

//------------------------------------------------------------------------------
template <typename T>
inline bool AxisAlignedBoundingRectangle<T>::Contains(
  dlm::Vector<2, T> const& point) const
{
  return !(point.X() < lowerLeft_.X()) &&
           point.X() < upperRight_.X()  &&
         !(point.Y() < lowerLeft_.Y()) &&
           point.Y() < upperRight_.Y();
}

//------------------------------------------------------------------------------
template <typename T>
inline bool AxisAlignedBoundingRectangle<T>::Contains(
  AxisAlignedBoundingRectangle<T> const& other) const
{
  return !(other.lowerLeft_.X() < lowerLeft_.X()) &&
         !(other.lowerLeft_.Y() < lowerLeft_.Y()) &&
         !(upperRight_.X() < other.upperRight_.X()) &&
         !(upperRight_.Y() < other.upperRight_.Y());
}

//------------------------------------------------------------------------------
template <typename T>
inline bool AxisAlignedBoundingRectangle<T>::Intersects(
  AxisAlignedBoundingRectangle<T> const& other) const
{
  return lowerLeft_.X() < other.upperRight_.X() &&
         lowerLeft_.Y() < other.upperRight_.Y() &&
         other.lowerLeft_.X() < upperRight_.X() &&
         other.lowerLeft_.Y() < upperRight_.Y();
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
