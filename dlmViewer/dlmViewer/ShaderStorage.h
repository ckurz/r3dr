//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmViewer_ShaderStorage_h_
#define dlmViewer_ShaderStorage_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "GLWrapper/GLWrapper.h"
#include "GLWrapper/GLProgram.h"
#include "GLWrapper/GLShader.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Types.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <vector>
#include <string>
#include <map>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// ShaderStorage class
//----------------------------------------------------------------------
class ShaderStorage
{
public:
  //============================================================================
  // Nested
  //--------------------------------------------------------------------
  typedef std::pair<unsigned, std::string> AttribLocationInfo;

  struct ShaderInformation
  {
    std::string vertShader;
    std::string geomShader;
    std::string fragShader;
    std::string reference;
    std::vector<AttribLocationInfo> attribLocations;
  };

  //============================================================================
  // Destructor
  //----------------------------------------------------------------------------
//  ~ShaderStorage();

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  dlm::uint64 AddShader(ShaderInformation const& shaderInfo);

  dlm::uint64 GetShaderIndex(std::string const& reference);

  GLWrapper::GLProgram const& GetShader(std::string const& reference);
  GLWrapper::GLProgram const& GetShader(dlm::uint64 const  reference);

  void Delete();

private:
  //============================================================================
  // Nested
  //----------------------------------------------------------------------------
  struct Data
  {
    GLWrapper::GLProgram  program;
    GLWrapper::GLShader   vShader;
    GLWrapper::GLShader   gShader;
    GLWrapper::GLShader   fShader;
    dlm::uint64 referenced;
    bool hasGShader;
  };

  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  std::vector<Data> data_;
  std::map<std::string, dlm::uint64> lut_;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
