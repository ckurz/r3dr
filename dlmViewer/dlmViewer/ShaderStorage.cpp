//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/ShaderStorage.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Destructor
//------------------------------------------------------------------------------
//ShaderStorage::~ShaderStorage()
//{
//  for (auto it = data_.begin(); it < data_.end(); ++it)
//  {
//    it->program.Delete();
//    it->fShader.Delete();
//    it->vShader.Delete();
//
//    if (it->hasGShader)
//    {
//      it->gShader.Delete();
//    }
//  }
//}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
dlm::uint64 ShaderStorage::AddShader(ShaderInformation const& shaderInfo)
{
  {
    auto it = lut_.find(shaderInfo.reference);

    if (it != lut_.end())
    {
      // The shader is already present.
      data_.at(it->second).referenced += 1;

      return it->second;
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Add shader.
  dlm::uint64 const index = static_cast<dlm::uint64>(data_.size());

  Data data;

  data.vShader.Create(GL_VERTEX_SHADER,   shaderInfo.vertShader);
  data.fShader.Create(GL_FRAGMENT_SHADER, shaderInfo.fragShader);

  if (shaderInfo.geomShader.size())
  {
    data.hasGShader = true;

    // GL_GEOMETRY_SHADER was not defined on my Linux machine.
    // GL_GEOMETRY_SHADER_ARB defines the same value.
    data.gShader.Create(GL_GEOMETRY_SHADER_ARB, shaderInfo.geomShader);
  }
  else
  {
    data.hasGShader = false;
  }

  data.program.Create();

  data.program.Attach(data.vShader);
  data.program.Attach(data.fShader);

  if (data.hasGShader)
  {
    data.program.Attach(data.gShader);
  }

  for (auto it  = shaderInfo.attribLocations.begin();
            it != shaderInfo.attribLocations.end  (); ++it)
  {
    data.program.BindAttribLocation(it->first, it->second);
  }

  data.program.Link();
  data.program.Use();

  data_.push_back(data);

  lut_[shaderInfo.reference] = index;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return index;
}

//------------------------------------------------------------------------------
dlm::uint64 ShaderStorage::GetShaderIndex(std::string const& reference)
{
  auto it = lut_.find(reference);

  if (it == lut_.end())
  {
    throw("ShaderStorage::GetShaderIndex: shader not found");
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return it->second;
}

//------------------------------------------------------------------------------
GLWrapper::GLProgram const& ShaderStorage::GetShader(
  std::string const& reference)
{
  auto it = lut_.find(reference);

  if (it == lut_.end())
  {
    throw("ShaderStorage::GetShader: shader not found");
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return GetShader(it->second);
}

//------------------------------------------------------------------------------
const GLWrapper::GLProgram& ShaderStorage::GetShader(
  dlm::uint64 const reference)
{
  return data_.at(reference).program;
}

//------------------------------------------------------------------------------
void ShaderStorage::Delete()
{
  for (auto it = data_.begin(); it < data_.end(); ++it)
  {
    it->program.Delete();
    it->fShader.Delete();
    it->vShader.Delete();

    if (it->hasGShader)
    {
      it->gShader.Delete();
    }
  }
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
