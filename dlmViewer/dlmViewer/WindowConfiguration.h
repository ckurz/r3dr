//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmViewer_WindowConfiguration_h_
#define dlmViewer_WindowConfiguration_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "GLSubsystem/Receiver.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "GLSubsystem/Event.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.h"
#include "dlm/math/Matrix.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// WindowConfiguration class
//------------------------------------------------------------------------------
class WindowConfiguration : public GLSubsystem::Receiver
{
public:
  //============================================================================
  // Constructor
  //----------------------------------------------------------------------------
  WindowConfiguration();

  //============================================================================
  // Accessors
  //----------------------------------------------------------------------------
  dlm::Matrix4f const& WindowProjection() const;
  dlm::Matrix4f const&  ImageProjection() const;

  dlm::Vector2u const& WindowSize() const;
  dlm::Vector2u const&  ImageSize() const;

  dlm::FloatType const& WindowAspectRatio() const;
  dlm::FloatType const&  ImageAspectRatio() const;

  dlm::FloatType const& Left  () const;
  dlm::FloatType const& Right () const;
  dlm::FloatType const& Top   () const;
  dlm::FloatType const& Bottom() const;

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  GLSubsystem::Receiver* OnEvent(GLSubsystem::Event const& inputEvent);

  void SetImageSize(dlm::Vector2u const& imageSize);

  dlm::Vector2f WindowToImage(dlm::Vector2f const& windowPosition);

private:
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  void Recalculate();

  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  dlm::Matrix4f windowProjection_;
  dlm::Matrix4f  imageProjection_;

  dlm::Vector2u windowSize_;
  dlm::Vector2u  imageSize_;

  dlm::FloatType wAspectRatio_;
  dlm::FloatType iAspectRatio_;

  dlm::FloatType left_;
  dlm::FloatType right_;

  dlm::FloatType top_;
  dlm::FloatType bottom_;

  dlm::FloatType width_;
  dlm::FloatType height_;

  bool letterBoxNeeded_;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
