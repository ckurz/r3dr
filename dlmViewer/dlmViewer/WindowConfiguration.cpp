//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/WindowConfiguration.h"

//------------------------------------------------------------------------------
#include "dlm/math/Vector.inline.h"
#include "dlm/math/Matrix.inline.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Constructor
//------------------------------------------------------------------------------
WindowConfiguration::WindowConfiguration()
  : windowSize_     (1, 1                            )
  , imageSize_      (1, 1                            )
  , wAspectRatio_   (static_cast<dlm::FloatType>(1.0))
  , iAspectRatio_   (static_cast<dlm::FloatType>(1.0))
  , left_           (static_cast<dlm::FloatType>(0.0))
  , right_          (static_cast<dlm::FloatType>(1.0))
  , top_            (static_cast<dlm::FloatType>(1.0))
  , bottom_         (static_cast<dlm::FloatType>(0.0))
  , width_          (static_cast<dlm::FloatType>(1.0))
  , height_         (static_cast<dlm::FloatType>(1.0))
  , letterBoxNeeded_(false                           )
{}

//==============================================================================
// Accessors
//------------------------------------------------------------------------------
dlm::Matrix4f const& WindowConfiguration::WindowProjection() const
{
  return windowProjection_;
}

//------------------------------------------------------------------------------
dlm::Matrix4f const& WindowConfiguration::ImageProjection() const
{
  return imageProjection_;
}

//------------------------------------------------------------------------------
dlm::Vector2u const& WindowConfiguration::WindowSize() const
{
  return windowSize_;
}

//------------------------------------------------------------------------------
dlm::Vector2u const& WindowConfiguration::ImageSize() const
{
  return imageSize_;
}

//------------------------------------------------------------------------------
dlm::FloatType const& WindowConfiguration::WindowAspectRatio() const
{
  return wAspectRatio_;
}

//------------------------------------------------------------------------------
dlm::FloatType const& WindowConfiguration::ImageAspectRatio() const
{
  return iAspectRatio_;
}

//------------------------------------------------------------------------------
dlm::FloatType const& WindowConfiguration::Left() const
{
  return left_;
}

//------------------------------------------------------------------------------
dlm::FloatType const& WindowConfiguration::Right() const
{
  return right_;
}

//------------------------------------------------------------------------------
dlm::FloatType const& WindowConfiguration::Top() const
{
  return top_;
}

//------------------------------------------------------------------------------
dlm::FloatType const& WindowConfiguration::Bottom() const
{
  return bottom_;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
GLSubsystem::Receiver* WindowConfiguration::OnEvent(
  GLSubsystem::Event const& inputEvent)
{
  if (inputEvent.type == GLSubsystem::Event::ResizeEvent)
  {
    windowSize_ =
      dlm::Vector2u(inputEvent.Resize.width, inputEvent.Resize.height);

    Recalculate();
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return nullptr;
}

//------------------------------------------------------------------------------
void WindowConfiguration::SetImageSize(
  const dlm::Vector2u& imageSize)
{
  imageSize_ = imageSize;

  Recalculate();
}

//------------------------------------------------------------------------------
dlm::Vector2f WindowConfiguration::WindowToImage(
  dlm::Vector2f const& windowPosition)
{
  dlm::FloatType x = windowPosition.X();
  dlm::FloatType y = windowPosition.Y();

  y = windowSize_.Y() - y; //make the lower left corner the origin

  x /= windowSize_.X();
  y /= windowSize_.Y();

  x *= width_;
  y *= height_;

  x += left_;
  y += bottom_;

  return dlm::Vector2f(x, y);
}

//------------------------------------------------------------------------------
void WindowConfiguration::Recalculate()
{
  using dlm::FloatType;

  FloatType const N0 = static_cast<FloatType>(0.0);
  FloatType const N1 = static_cast<FloatType>(1.0);
  FloatType const N2 = static_cast<FloatType>(2.0);

  FloatType const half = static_cast<FloatType>(0.5);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  letterBoxNeeded_ = 
    windowSize_.X() == imageSize_.X() && windowSize_.Y() == imageSize_.Y();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  {
    dlm::Matrix4f matrix;

    matrix(0, 0) =  N2 / windowSize_.X();
    matrix(1, 1) =  N2 / windowSize_.Y();
    matrix(2, 2) = -N1;
    matrix(3, 3) =  N1;

    matrix(0, 3) = -N1;// + N1 / windowSize_.X();
    matrix(1, 3) = -N1;// + N1 / windowSize_.Y();
    matrix(2, 3) =  N0;

    windowProjection_ = matrix;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  wAspectRatio_ = windowSize_.X() / static_cast<FloatType>(windowSize_.Y());
  iAspectRatio_ =  imageSize_.X() / static_cast<FloatType>( imageSize_.Y());

  if (wAspectRatio_ < iAspectRatio_)
  {
    width_  = imageSize_.X();
    height_ = imageSize_.X() / wAspectRatio_;

    FloatType const diff = (height_ - imageSize_.Y()) * half;

    top_    =  diff + imageSize_.Y();
    bottom_ = -diff;

    left_   = N0;
    right_  = imageSize_.X();
  }
  else
  {
    width_  = imageSize_.Y() * wAspectRatio_;
    height_ = imageSize_.Y();

    FloatType const diff = (width_  - imageSize_.X()) * half;

    top_    = imageSize_.Y();
    bottom_ = N0;

    left_   = -diff;
    right_  =  diff + imageSize_.X();
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  {
    dlm::Matrix4d matrix;

    matrix(0, 0)  =  N2 / (right_ - left_);
    matrix(1, 1)  =  N2 / (top_ - bottom_);
    matrix(2, 2)  = -N1;
    matrix(3, 3)  =  N1;

    matrix(0, 3)  = -(right_ + left_) / (right_ - left_);
    matrix(1, 3)  = -(top_ + bottom_) / (top_ - bottom_);
    matrix(2, 3)  =  N0;

    //offset for openGL pixel coordinates
//    matrix.At(0, 3) += N1 / windowSize_.X();
//    matrix.At(1, 3) += N1 / windowSize_.Y();

    imageProjection_ = matrix;
  }
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
