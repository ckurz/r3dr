//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/CameraControl.h"

//------------------------------------------------------------------------------
#include "dlmViewer/GLHelper.h"
#include "dlmViewer/GLHelper.inline.h"
#include "dlmViewer/InterpolationAssistant.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.inline.h"
#include "dlm/math/Matrix.inline.h"
#include "dlm/math/Quaternion.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/VectorFunctions.h"
#include "dlm/math/MatrixFunctions.h"
#include "dlm/math/QuaternionFunctions.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"

#include "dlm/math/OStream.h"
#include <iostream>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// Unnamed namespace
//------------------------------------------------------------------------------
namespace
{

//==============================================================================
// Local functions
//------------------------------------------------------------------------------
CameraControl::TransitionMode GetTransitionMode(
  CameraControl::ViewMode const current,
  CameraControl::ViewMode const target)
{
  if (current == CameraControl::FREE)
  {
    if (target == CameraControl::FREE  ) { return CameraControl::FREE_TO_FREE;   }
    if (target == CameraControl::STATIC) { return CameraControl::FREE_TO_STATIC; }
    if (target == CameraControl::CUSTOM) { return CameraControl::FREE_TO_CUSTOM; }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (current == CameraControl::STATIC)
  {
    if (target == CameraControl::FREE  ) { return CameraControl::STATIC_TO_FREE;   }
    if (target == CameraControl::STATIC) { return CameraControl::STATIC_TO_STATIC; }
    if (target == CameraControl::CUSTOM) { return CameraControl::STATIC_TO_CUSTOM; }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (current == CameraControl::CUSTOM)
  {
    if (target == CameraControl::FREE  ) { return CameraControl::CUSTOM_TO_FREE;   }
    if (target == CameraControl::STATIC) { return CameraControl::CUSTOM_TO_STATIC; }
    if (target == CameraControl::CUSTOM) { return CameraControl::CUSTOM_TO_CUSTOM; }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return CameraControl::NONE;
}

//==============================================================================
// Unnamed namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// Constructor
//------------------------------------------------------------------------------
CameraControl::CameraControl()
  : windowSize_        (1.0, 1.0)
  , aspectRatio_       (     1.0)
  , cameraDistance_    (   300.0)
  , zNear_             (     0.1)
  , zFar_              ( 10000.0)
  , f_                 (    35.0)
  , viewMode_          (    FREE)
  , transitionMode_    (    NONE)
  , currentIndex_      (       0)
  , targetIndex_       (       0)
  , transitionTime_    (       0)
  , transitionDuration_(     600)
  , locked_            (       0)
  , overPivot_         (   false)
  , movePivot_         (   false)
  , move_              (   false)
  , modifier_          (   false)
  , inTransition_      (   false)
  , valid_             (   false)
  , hasCustom_         (   false)
{
  using dlm::Vector3f;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  cameraAngles_.Y() = -M_PI_4;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Static view 0: Make the x axis point toward the viewer.
  static_[0].view.Xyz() = Vector3f(0.5, 0.5, 0.5);
  static_[0].view.W()   = 0.5;
  
  // Static view 1: Make the y axis point toward the viewer.
  static_[1].view = dlm::QF::FromAxisAngle(Vector3f(1.0, 0.0, 0.0), M_PI_2);
  
  // Static view 2: is the unit quaternion, since it displays the scene in the
  // default OpenGL camera configuration, which means x is right, y is up, and z
  // is toward the viewer.
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  UpdateProjectionMatrices();
  
  current_ = free_;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  ppRenderer_.Hide();
  fpRenderer_.Hide();
}

//==============================================================================
// Accessors
//------------------------------------------------------------------------------
dlm::Matrix4d CameraControl::Projection() const
{
  return projection_;
}

//------------------------------------------------------------------------------
dlm::Matrix4d CameraControl::Modelview() const
{
  return modelview_;
}

//------------------------------------------------------------------------------
dlm::Matrix4d CameraControl::ModelviewProjection() const
{
  return modelviewProjection_;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
void CameraControl::Create()
{
  ppRenderer_.Create();
  fpRenderer_.Create();
}

//------------------------------------------------------------------------------
void CameraControl::Delete()
{
  ppRenderer_.Delete();
  fpRenderer_.Delete();
}

//------------------------------------------------------------------------------
void CameraControl::Update(dlm::uint64 const delta)
{
  ppRenderer_.Update(delta);
  fpRenderer_.Update(delta);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (inTransition_)
  {
    Invalidate();

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    transitionTime_ += static_cast<dlm::uint32>(delta);

    if (transitionTime_ >= transitionDuration_)
    {
      current_ = target_;

      switch (transitionMode_)
      {
      case   FREE_TO_STATIC:
      case STATIC_TO_STATIC:
      case CUSTOM_TO_STATIC:
        {
          viewMode_     = STATIC;
          currentIndex_ = targetIndex_;
  
          ppRenderer_.FadeIn();
          
          break;
        }
      case   FREE_TO_CUSTOM:
      case STATIC_TO_CUSTOM:
      case CUSTOM_TO_CUSTOM:
        {
          viewMode_ = CUSTOM;
          break;
        }
      default:
        {
          viewMode_ = FREE;
          break;
        }
      }

      transitionMode_ = NONE;

      transitionTime_ = 0;

      inTransition_   = false;

      if (viewMode_ != CUSTOM)
      {
        Unlock();
      }
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  UpdateMatrices();
}

//------------------------------------------------------------------------------
void CameraControl::Draw(dlm::Matrix4f const& matrix) const
{
  fpRenderer_.Draw(matrix);
}

//------------------------------------------------------------------------------
void CameraControl::Draw2D(dlm::Matrix4f const& matrix) const
{
  ppRenderer_.Draw(matrix);
}

//------------------------------------------------------------------------------
void CameraControl::Lock()
{
  ++locked_;
}

//------------------------------------------------------------------------------
void CameraControl::Unlock()
{
  DLM_ASSERT(locked_ > 0, DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  --locked_;
}

//------------------------------------------------------------------------------
bool CameraControl::Locked() const
{
  return locked_ != 0;
}

//------------------------------------------------------------------------------
void CameraControl::SetCustom(
  dlm::Image          const& image,
  dlm::Camera         const& camera,
  dlm::Transformation const& t)
{
  customImage_  = image;
  customCamera_ = camera;
  customT_      = t;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  hasCustom_ = true;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  UpdateCustomConfiguration();
}

//------------------------------------------------------------------------------
void CameraControl::ClearCustom()
{
  hasCustom_ = false;

  if (viewMode_ == ViewMode::CUSTOM)
  {
    PrepareTransitionToFree();
    InitiateTransition();
  }
}

//------------------------------------------------------------------------------
bool CameraControl::ToCustom()
{
  DLM_ASSERT(hasCustom_, DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (viewMode_ != ViewMode::FREE || transitionMode_ != TransitionMode::NONE)
  {
    return false;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  PrepareTransitionToCustom();
  InitiateTransition();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return true;
}

//------------------------------------------------------------------------------
bool CameraControl::ToFree()
{
  if (viewMode_ != ViewMode::CUSTOM || transitionMode_ != TransitionMode::NONE)
  {
    return false;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  PrepareTransitionToFree();
  InitiateTransition();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return true;
}

//------------------------------------------------------------------------------
void CameraControl::Invalidate()
{
  valid_ = false;
}

//------------------------------------------------------------------------------
void CameraControl::UpdateMatrices()
{
  if (!valid_)
  {
    projection_ = BuildProjectionMatrix();
    modelview_  = BuildModelviewMatrix ();

    if (viewMode_ == FREE)
    {
      current_.view = dlm::QF::FromEulerAnglesYXZr(cameraAngles_).Conjugated();
    }

    ApplyExtrinsics (projection_);
    ApplyFocusOffset(projection_);

    modelviewProjection_ = projection_ * modelview_;

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    UpdatePivotPosition();

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    valid_ = true;
  }
}

//------------------------------------------------------------------------------
void CameraControl::UpdateProjectionMatrices()
{
  free_.projection = GLHelper::Perspective(aspectRatio_, f_, zNear_, zFar_);
  
  static_[0].projection =
    GLHelper::Ortho(aspectRatio_, f_, zNear_, zFar_, cameraDistance_);
  static_[1].projection = static_[0].projection;
  static_[2].projection = static_[0].projection;

  if (viewMode_ == FREE  )
  {
    current_.projection = free_.projection;
  }
  else if (viewMode_ == STATIC)
  {
    current_.projection = static_[currentIndex_].projection;
  }
  else
  {
    current_.projection = custom_.projection;
  }

  Invalidate();
}

//------------------------------------------------------------------------------
void CameraControl::UpdatePivotPosition()
{
  if (viewMode_ == STATIC)
  {
    dlm::Vector4f point = dlm::VF::Homogenize(scenePivot_);
  
    point  = projection_ * point;
    point /= point.W();

    dlm::FloatType const halfWindowSizeX = windowSize_.X() * 0.5;
    dlm::FloatType const halfWindowSizeY = windowSize_.Y() * 0.5;

    point.X() *= halfWindowSizeX;
    point.Y() *= halfWindowSizeY;

    point.X() += halfWindowSizeX;
    point.Y() += halfWindowSizeY;

    pivotPosition_.X() = point.X();
    pivotPosition_.Y() = point.Y();

    ppRenderer_.Set(pivotPosition_);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  UpdatePivotSelection(lastMousePosition_);
}

//dlm::core::Matrix4d CameraControl::CustomProjection()
//{
//  dlm::core::Matrix4d projection =
//    glh::Frustum(customCamera_, aspectRatio_, zNear_, zFar_);
//
//  dlm::core::Matrix4d compensation;
//  compensation.At(0, 0) =  1.0;
//  compensation.At(1, 1) = -1.0;
//  compensation.At(2, 2) = -1.0;
//  compensation.At(3, 3) =  1.0;
//
//  projection *= compensation * customT_.Matrix();
//
//  //UpdateMatrices();
//  //std::cout << modelviewProjection_ << std::endl;
//  //std::cout << projection << std::endl;
//
//  //int* a = 0; *a = 1;
//
//  return projection;
//}
//
//


//------------------------------------------------------------------------------
void CameraControl::UpdateCustomConfiguration()
{
  DLM_ASSERT(hasCustom_, DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  custom_.projection =
    GLHelper::Frustum(customCamera_, customImage_, aspectRatio_, zNear_, zFar_);

  // Cameras currently point down the positive z axis. OpenGL cam doesn't
  Quatd compensation = QF::FromAxisAngle(Vector3f(1.0, 0.0, 0.0), M_PI);

  custom_.view  = sceneRotation_ * customT_.Rotation() * compensation;

  //construct focus point
  Vector3d focus(0.0, 0.0, cameraDistance_);
  focus  = customT_.Translation()   + customT_.Rotation()   * focus;
  focus  = sceneTranslation_        + sceneRotation_        * focus;

  custom_.focus = -focus; // Taking the OpenGL camera orientation into
                          // consideration, this should make sense.

  if (viewMode_ == CUSTOM)
  {
    current_ = custom_;

    Invalidate();
  }
}

//------------------------------------------------------------------------------
void CameraControl::PrepareTransitionToFree()
{
  DLM_ASSERT(!inTransition_, DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (viewMode_ == FREE)
  {
    free_ = current_;
  }

  if (viewMode_ == STATIC)
  {
    ppRenderer_.FadeOut();
  }

  target_ = free_;

  transitionMode_ = GetTransitionMode(viewMode_, FREE);
}

//------------------------------------------------------------------------------
void CameraControl::PrepareTransitionToStatic(dlm::uint32 const index)
{
  DLM_ASSERT(!inTransition_, DLM_RUNTIME_ERROR);
  DLM_ASSERT(index < 3,      DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (viewMode_ == FREE)
  {
    free_ = current_;
  }

  target_ = static_[index];

  targetIndex_ = index;

  transitionMode_ = GetTransitionMode(viewMode_, STATIC);
}

//------------------------------------------------------------------------------
void CameraControl::PrepareTransitionToCustom()
{
  DLM_ASSERT(!inTransition_, DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (viewMode_ == FREE)
  {
    free_ = current_;
  }

  target_ = custom_;

  transitionMode_ = GetTransitionMode(viewMode_, CUSTOM);
}

//------------------------------------------------------------------------------
void CameraControl::InitiateTransition()
{
  DLM_ASSERT(!inTransition_, DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  transitionTime_ = 0;

  if (viewMode_ != CUSTOM)
  {
    Lock();
  }

  inTransition_ = true;
}

//------------------------------------------------------------------------------
dlm::Matrix4f CameraControl::BuildModelviewMatrix() const
{
  dlm::Matrix3f const rotation    = dlm::QF::Rotation  (sceneRotation_   );
  dlm::Vector4f const translation = dlm::VF::Homogenize(sceneTranslation_);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  dlm::Matrix4f modelview;

  dlm::MF::SetSubmatrix<3, 3>(modelview, 0, 0, rotation);

  dlm::MF::SetCol(modelview, 3, translation);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return modelview;
}

//------------------------------------------------------------------------------
dlm::Matrix4f CameraControl::BuildProjectionMatrix() const
{
  dlm::Matrix4f projection = current_.projection;

  if (!inTransition_                       ||
       transitionMode_ == FREE_TO_FREE     ||
       transitionMode_ == STATIC_TO_STATIC)
  {
    // For these cases: done.
    return projection;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // The projection matrix requires some sort of interpolation
  dlm::FloatType t = 0.0;

  if      (transitionMode_ == FREE_TO_STATIC   ||
           transitionMode_ == CUSTOM_TO_STATIC)
  {
    t = IA::MapLinear(transitionTime_, transitionDuration_);
    t = 1 - exp(-3.0 * M_PI * t);
  }
  else if (transitionMode_ == STATIC_TO_FREE   ||
           transitionMode_ == STATIC_TO_CUSTOM)
  {
    t = IA::MapLinear(transitionTime_, transitionDuration_);
    t = exp((t - 1.0) * 3.0 * M_PI);
  }
  else
  {
    t = IA::H01(transitionTime_, transitionDuration_);
  }

  projection *= 1.0 - t;
  projection += target_.projection * t;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return projection;
}

//------------------------------------------------------------------------------
void CameraControl::ApplyExtrinsics(dlm::Matrix4f& matrix) const
{
  dlm::Quatd orientation = current_.view;

  if (inTransition_)
  {
    dlm::FloatType const t = IA::H01(transitionTime_, transitionDuration_);

    orientation = dlm::QF::Slerp(orientation, target_.view, t);
  }

  orientation.Conjugate();

  dlm::Matrix4f modelview;

  dlm::MF::SetSubmatrix<3, 3>(modelview, 0, 0, dlm::QF::Rotation(orientation));

  modelview.At(2, 3) = -cameraDistance_;
  modelview.At(3, 3) = 1.0;

  matrix *= modelview;
}

//------------------------------------------------------------------------------
void CameraControl::ApplyFocusOffset(dlm::Matrix4f& matrix) const
{
  dlm::Vector3f focusPoint = current_.focus;

  if (inTransition_)
  {
    dlm::FloatType const t = IA::H01(transitionTime_, transitionDuration_);

    focusPoint *= 1.0 - t;
    focusPoint += target_.focus * t;
  }

  dlm::Matrix4f focusOffset;

  focusOffset.At(0, 0) = 1.0;
  focusOffset.At(1, 1) = 1.0;
  focusOffset.At(2, 2) = 1.0;
  focusOffset.At(3, 3) = 1.0;

  focusOffset.At(0, 3) = focusPoint.X();
  focusOffset.At(1, 3) = focusPoint.Y();
  focusOffset.At(2, 3) = focusPoint.Z();

  matrix *= focusOffset;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
