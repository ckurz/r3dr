//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmViewer_GLHelper_h_
#define dlmViewer_GLHelper_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "GLWrapper/GLWrapper.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/entity/Camera.h"
#include "dlm/entity/Image.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Matrix.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// GLHelper class
//------------------------------------------------------------------------------
class GLHelper
{
public:
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  // Convert a 2x2 matrix of T values to an array of float values. Note that
  // OpenGL can't handle double values in glUniform calls.
  // This function implicitely transposes the dlm::Matrix to take into account
  // the column-major storage format used by OpenGL.
  template <typename T>
  static void UniformMatrix2fv(
    GLint const location, dlm::Matrix<2, 2, T> const& matrix);

  // Convert a 4x4 matrix of T values to an array of float values. Note that
  // OpenGL can't handle double values in glUniform calls.
  // This function implicitely transposes the dlm::Matrix to take into account
  // the column-major storage format used by OpenGL.
  template <typename T>
  static void UniformMatrix4fv(
    GLint const location, dlm::Matrix<4, 4, T> const& matrix);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Create a perspective projection matrix with the given parameters.
  template <typename T>
  static dlm::Matrix<4, 4, T> Perspective(
    T const aspectRatio, T const f, T const zNear, T const zFar);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Create an orthographic projection matrix with the given parameters.
  template <typename T>
  static dlm::Matrix<4, 4, T> Ortho(
    T const aspectRatio,
    T const f,
    T const zNear,
    T const zFar,
    T const distance);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Create a view frustum matrix with the given parameters.
  template <typename T>
  static dlm::Matrix<4, 4, T> Frustum(
    dlm::Camera const& camera,
    dlm::Image  const& image,
    T           const  windowAspectRatio,
    T           const  zNear,
    T           const  zFar);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Compensate negative z-direction.
  template <typename T>
  static void CompensateOrientation(dlm::Matrix<4, 4, T>& matrix);
};

//==============================================================================
// Shorthand
//------------------------------------------------------------------------------
typedef GLHelper glh;

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// Inline code
//------------------------------------------------------------------------------
// This file is likely to be included from source files only, and for
// convenience includes the corresponding inline file automatically.
//------------------------------------------------------------------------------
#include "dlmViewer/GLHelper.inline.h"

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
