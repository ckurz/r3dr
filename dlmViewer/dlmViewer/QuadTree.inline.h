//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmViewer library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmViewer_QuadTree_inline_h_
#define dlmViewer_QuadTree_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmViewer/QuadTree.h"

//------------------------------------------------------------------------------
#include "dlmViewer/AxisAlignedBoundingRectangle.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlmViewer/AxisAlignedBoundingRectangleAssistant.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//#include "dlm/math/OStream.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/ArrayObjectTable.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <iostream>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmViewer
{

//==============================================================================
// QuadTree
//------------------------------------------------------------------------------
// Constructors
//------------------------------------------------------------------------------
inline QuadTree::QuadTree()
  : overlapFactor_ (static_cast<dlm::FloatType>(0.0))
  , depth_         (0                               )
  , cellSize_      (0                               )
  , initialized_   (false                           )
{}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
inline void QuadTree::Setup(
  AABR           const& area,
  dlm::uint64    const  depth,
  dlm::uint64    const  cellSize,
  dlm::FloatType const  overlapFactor)
{
  area_           = area;
  looseArea_      = area;

  depth_          = depth;
  cellSize_       = cellSize;

  overlapFactor_  = overlapFactor;

  AABRA::Expand(looseArea_, overlapFactor_);

  SetupLocCoords();

  initialized_ = true;
}

//------------------------------------------------------------------------------
inline void QuadTree::Initialize(dlm::ArrayObjectTable<AABR> const& data)
{
  if (!initialized_)
  {
    throw("QuadTree::Initialize: call Setup first");
  }

  BuildRoot();

  data_ = data;

  cellIndices_.resize(data_.Range());

  for (unsigned i = 0; i < data_.Range(); ++i)
  {
    if (data_.Active(i))
    {
      cells_.At(0).elements.push_back(i);
      cellIndices_.at(i) = 0;
    }
  }

  RefineCell(0, cellSize_);
}

//------------------------------------------------------------------------------
inline bool QuadTree::Validate(const bool verbose)
{
  bool valid = true;

  valid = valid && ValidateCells(verbose);
  valid = valid && ValidateData (verbose);

  if (verbose)
  {
    std::cerr << "QuadTree Validation ";

    if (valid)
    {
      std::cerr << "succeeded" << std::endl;
    }
    else
    {
      std::cerr << "FAILED" << std::endl;
    }
  }

  return valid;
}

//------------------------------------------------------------------------------
inline  std::vector<dlm::uint64> QuadTree::Query(
  dlm::Vector2f const& point) const
{
  std::vector<dlm::uint64> result;

  std::vector<dlm::uint64> stack;
  stack.push_back(0);

  while (!stack.empty())
  {
    dlm::uint64 const currentCellId = stack.back();
    stack.pop_back();

    Cell const& cell = cells_.At(currentCellId);

    if (cell.looseArea.Contains(point))
    {
      if (cell.children[0])
      {
        stack.push_back(cell.children[0]);
        stack.push_back(cell.children[1]);
        stack.push_back(cell.children[2]);
        stack.push_back(cell.children[3]);
      }
      else
      {
        for (dlm::uint64 i = 0; i < cell.elements.size(); ++i)
        {
          dlm::uint64 const elementId = cell.elements.at(i);
          AABR const& aabr = data_.At(elementId);

          if (aabr.Contains(point))
          {
            result.push_back(elementId);
          }
        }
      }

      for (dlm::uint64 i = 0; i < cell.looseElements.size(); ++i)
      {
        dlm::uint64 const elementId = cell.looseElements.at(i);
        AABR const& aabr = data_.At(elementId);

        if (aabr.Contains(point))
        {
          result.push_back(elementId);
        }
      }
    }
  }

  return result;
}

//------------------------------------------------------------------------------
inline std::vector<dlm::uint64> QuadTree::Query(AABR const& range) const
{
  std::vector<dlm::uint64> result;

  std::vector<dlm::uint64> stack;
  stack.push_back(0);

  while (!stack.empty())
  {
    dlm::uint64 const currentCellId = stack.back();
    stack.pop_back();

    Cell const& cell = cells_.At(currentCellId);

    if (cell.looseArea.Intersects(range))
    {
      if (cell.children[0])
      {
        stack.push_back(cell.children[0]);
        stack.push_back(cell.children[1]);
        stack.push_back(cell.children[2]);
        stack.push_back(cell.children[3]);
      }
      else
      {
        for (dlm::uint64 i = 0; i < cell.elements.size(); ++i)
        {
          dlm::uint64 const elementId = cell.elements.at(i);
          AABR const& aabr = data_.At(elementId);

          if (aabr.Intersects(range))
          {
            result.push_back(elementId);
          }
        }
      }

      for (dlm::uint64 i = 0; i < cell.looseElements.size(); ++i)
      {
        dlm::uint64 const elementId = cell.looseElements.at(i);
        AABR const& aabr = data_.At(elementId);

        if (aabr.Intersects(range))
        {
          result.push_back(elementId);
        }
      }
    }
  }

  return result;
}

//------------------------------------------------------------------------------
inline void QuadTree::BuildRoot()
{
  cells_.Clear();
  cells_.PushBack(Cell(dlm::Invalid, depth_, LocCode(0, 0), area_, looseArea_));

  cellIndices_.clear();
}

//------------------------------------------------------------------------------
inline void QuadTree::SetupLocCoords()
{
  dlm::Vector2f difference = area_.Dimension();

  locCodeConversion_.X() = (0x00000001 << depth_) / difference.X();
  locCodeConversion_.Y() = (0x00000001 << depth_) / difference.Y();

  maxLengths_.clear();
  maxLengths_.resize(depth_);

  for (dlm::int64 i = static_cast<dlm::int64>(depth_) - 1; i > 0; --i)
  {
    maxLengths_.at(i) = difference * (overlapFactor_ - 1.0) * 0.5;

    difference *= 0.5;
  }
}

//------------------------------------------------------------------------------
inline QuadTree::LocCode QuadTree::GetLocCode(
  const dlm::Vector2f& pos) const
{
  //convert point to quadtree area
  dlm::Vector2f loc = pos - area_.LowerLeft();

  loc.X() *= locCodeConversion_.X();
  loc.Y() *= locCodeConversion_.Y();

  return LocCode(unsigned(loc.X()), unsigned(loc.Y()));
}

//------------------------------------------------------------------------------
inline dlm::uint64 QuadTree::GetLowestLevel(
  dlm::Vector2f const& size) const
{
  dlm::uint64 level = depth_;

  while (level > 0)
  {
    dlm::Vector2f const& maxLength = maxLengths_.at(level - 1);

    if (maxLength.X() < size.X() ||
        maxLength.Y() < size.Y())
    {
      break;
    }

    --level;
  }

  return level;
}

//------------------------------------------------------------------------------
inline dlm::uint64 QuadTree::GetLeafCell(
  dlm::Vector2f const& loc,
  dlm::uint64 const             lowestLevel) const
{
  LocCode locCode = GetLocCode(loc);

  locCode.X() <<= 2;
  locCode.Y() <<= 2;

  dlm::uint64 cell = 0;

  dlm::uint64 nextLevel = depth_ - 1;

  dlm::uint64 children[4];

  {
    const Cell& current = cells_.At(cell);

    children[0] = current.children[0];
    children[1] = current.children[1];
    children[2] = current.children[2];
    children[3] = current.children[3];
  }

  dlm::uint64 counter = nextLevel - lowestLevel + 1;

  while (counter-- && children[0])
  {
    const dlm::uint64 childBranchBit = 1 << (nextLevel + 2);
    const dlm::uint64 childIndex =
      ((locCode.X() & childBranchBit) >> (nextLevel + 2)) +
      ((locCode.Y() & childBranchBit) >> (nextLevel + 1));

     cell = children[childIndex];

    --nextLevel;

    {
      const Cell& current = cells_.At(cell);

      children[0] = current.children[0];
      children[1] = current.children[1];
      children[2] = current.children[2];
      children[3] = current.children[3];
    }
  }

  return cell;
}

//------------------------------------------------------------------------------
inline void QuadTree::RefineCell(const dlm::uint64 cellId,
                                 const dlm::uint64 cellSize)
{
  const dlm::uint64 size  = cells_.At(cellId).elements.size();
  const dlm::uint64 level = cells_.At(cellId).level;

  if (cellSize > size || !level)
  {
    return;
  }

  SubdivideCell(cellId); //this potentially kills any reference

  dlm::uint64 children[4];

  std::vector<dlm::uint64> elements;

  {
    const Cell& cell = cells_.At(cellId);
    //let the reference go out of scope asap

    children[0] = cell.children[0];
    children[1] = cell.children[1];
    children[2] = cell.children[2];
    children[3] = cell.children[3];

    elements = cell.elements;
  }

  typedef std::vector<dlm::uint64>::const_iterator Iterator;

  for (Iterator it  = elements.begin(); it != elements.end(); ++it)
  {
    const dlm::uint64 elementId   = *it;
    const AABR&    data        = data_.At(elementId);
    const dlm::uint64 lowestLevel = GetLowestLevel(data.Dimension());
    const dlm::uint64 newCell     = GetLeafCell   (data.Center(),
                                                lowestLevel);

    if (newCell == children[0] ||
        newCell == children[1] ||
        newCell == children[2] ||
        newCell == children[3])
    {
      //move element to child cell
      Cell& child = cells_.At(newCell);

      if (child.level == lowestLevel)
      {
        child.looseElements.push_back(elementId);
      }
      else
      {
        child.elements.push_back(elementId);
      }

      cellIndices_.at(elementId) = newCell;
    }
    else if (newCell == cellId)
    {
      //move element to loose voxel
      cells_.At(cellId).looseElements.push_back(elementId);
      cellIndices_.at(elementId) = cellId;
    }
    else
    {
      //this might happen if an update is made and an element has moved
      //out of a cell that the quadtree is trying to refine
      throw("QuadTree::RefineCell: point is not contained in area of "
            "cell to be refined");
    }
  }

  //everything sorted in -> cleanup
  Cell& cell = cells_.At(cellId);
  cell.elements.clear();

  //see if children can be subdivided
  RefineCell(children[0], cellSize);
  RefineCell(children[1], cellSize);
  RefineCell(children[2], cellSize);
  RefineCell(children[3], cellSize);
}

//------------------------------------------------------------------------------
inline void QuadTree::SubdivideCell(const dlm::uint64 cellId)
{
  const Cell& cell = cells_.At(cellId);

  if (cell.children[0])
  {
    throw("QuadTree::SubdivideCell: cell already subdivided");
  }

  if (!cell.level)
  {
    return;
  }

  const LocCode cellLocCode = cell.locCode;

  const dlm::uint64 newLevel = cell.level - 1;
  const dlm::uint64 newValue = (0x00000001 << (newLevel + 2));

  const dlm::Vector2f minimum = cell.area.LowerLeft();
  const dlm::Vector2f maximum = cell.area.UpperRight();
  const dlm::Vector2f center  = cell.area.Center();

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  dlm::uint64 children[4] = { 0 };

  {
    const AABR area(minimum, center);

    AABR looseArea(area);
    AABRA::Expand(looseArea, overlapFactor_);

    const LocCode locCode = cellLocCode;

    children[0] = cells_.PushBack(
      Cell(cellId, newLevel, locCode, area, looseArea));
  }

  {
    const dlm::Vector2f lowerLeft (center.X(), minimum.Y());
    const dlm::Vector2f upperRight(maximum.X(), center.Y());

    const AABR area(lowerLeft, upperRight);

    AABR looseArea(area);
    AABRA::Expand(looseArea, overlapFactor_);

    const LocCode locCode(cellLocCode.X() | newValue,
                          cellLocCode.Y()           );

    children[1] = cells_.PushBack(
      Cell(cellId, newLevel, locCode, area, looseArea));
  }

  {
    const dlm::Vector2f lowerLeft (minimum.X(), center.Y());
    const dlm::Vector2f upperRight(center.X(), maximum.Y());

    const AABR area(lowerLeft, upperRight);

    AABR looseArea(area);
    AABRA::Expand(looseArea, overlapFactor_);

    const LocCode locCode(cellLocCode.X(),
                          cellLocCode.Y() | newValue);

    children[2] = cells_.PushBack(
      Cell(cellId, newLevel, locCode, area, looseArea));
  }

  {
    const AABR area(center, maximum);

    AABR looseArea(area);
    AABRA::Expand(looseArea, overlapFactor_);

    const LocCode locCode(cellLocCode.X() | newValue,
                          cellLocCode.Y() | newValue);

    children[3] = cells_.PushBack(
      Cell(cellId, newLevel, locCode, area, looseArea));
  }

  {
    Cell& cell = cells_.At(cellId);
    cell.children[0] = children[0];
    cell.children[1] = children[1];
    cell.children[2] = children[2];
    cell.children[3] = children[3];
  }
}

//------------------------------------------------------------------------------
inline bool QuadTree::ValidateCells(const bool verbose)
{
  bool valid = true;

  dlm::uint64 checked = 0;
  dlm::uint64 errors  = 0;

  for (dlm::uint64 i = 0; i < cells_.Range(); ++i)
  {
    if (!cells_.Active(i))
    {
      continue;
    }

    ++checked;

    bool fail = false;

    const Cell& cell = cells_.At(i);

    if (cell.parent != dlm::Invalid)
    {
      const Cell& parent = cells_.At(cell.parent);

      if (!parent.children[0] == i &&
          !parent.children[1] == i &&
          !parent.children[2] == i &&
          !parent.children[3] == i)
      {
        fail = true;

        if (verbose)
        {
          std::cerr << "cell " << i << " is not referenced in "
                       "parent cell " << cell.parent << std::endl;
        }
      }

      if (!parent.area.Contains(cell.area))
      {
        fail = true;

        if (verbose)
        {
          std::cerr << "cell " << i << " area is not contained in "
                       "parent cell " << cell.parent << " area"
                    << std::endl;
          std::cerr << "parent aabr:" << parent.area.LowerLeft() << " "
                                      << parent.area.UpperRight() << std::endl;
          std::cerr << "cell   aabr:" << cell.area.LowerLeft() << " "
                                      << cell.area.UpperRight() << std::endl;
        }
      }

      if (cell.elements.size() > cellSize_)
      {
        fail = true;

        if (verbose)
        {
          std::cerr << "cell " << i << " elements size ("
                    << cell.elements.size() << ") greater than "
                       "cellSize_ (" << cellSize_ << ")" << std::endl;
        }
      }

      for (dlm::uint64 j = 0; j < cell.elements.size(); ++j)
      {
        const dlm::uint64 element       = cell.elements.at(j);
        const dlm::uint64 elementCellId = cellIndices_.at(element);

        if (elementCellId != i)
        {
          fail = true;

          if (verbose)
          {
            std::cerr << "element " << element << " referenced by cell "
                      << i << " points to cell " << elementCellId
                      << std::endl;
          }
        }
      }

      for (dlm::uint64 j = 0; j < cell.looseElements.size(); ++j)
      {
        const dlm::uint64 element       = cell.looseElements.at(j);
        const dlm::uint64 elementCellId = cellIndices_.at(element);

        if (elementCellId != i)
        {
          fail = true;

          if (verbose)
          {
            std::cerr << "element " << element << " referenced by cell "
                      << i << " points to cell " << elementCellId
                      << std::endl;
          }
        }
      }
    }
    else
    {
      if (!area_.Contains(cell.area))
      {
        fail = true;

        if (verbose)
        {
          std::cerr << "cell 0 not contained in tree area" << std::endl;
          std::cerr << area_.LowerLeft() << " " << area_.UpperRight() << std::endl;
          std::cerr << cell.area.LowerLeft() << " "
                    << cell.area.UpperRight() << std::endl;
        }
      }
    }

    if (fail)
    {
      valid = false;
      ++errors;
    }
  }

  if (verbose)
  {
    std::cerr << "ValidateCells: " << checked << " cells    - ";

    if (errors)
    {
      std::cerr << "check FAILED (" << errors << " errors)"
                << std::endl;
    }
    else
    {
      std::cerr << "check successful" << std::endl;
    }
  }

  return valid;
}

//------------------------------------------------------------------------------
inline bool QuadTree::ValidateData(const bool verbose)
{
  bool valid = true;

  dlm::uint64 checked = 0;
  dlm::uint64 errors  = 0;

  for (dlm::uint64 i = 0; i < data_.Range(); ++i)
  {
    if (!data_.Active(i))
    {
      continue;
    }

    ++checked;


    const AABR& data = data_.At(i);

    const dlm::uint64 cellId = cellIndices_.at(i);
    const Cell&    cell   = cells_.At(cellId);

    if (!cell.area.Contains(data.Center()))
    {
      ++errors;
      valid = false;

      if (verbose)
      {
        std::cerr << "element " << i << " is not contained in cell "
                  << cellId << std::endl;
        std::cerr << "element center: " << data.Center() << std::endl;
        std::cerr << "cell aabr: " << cell.area.LowerLeft() << " "
                  << cell.area.UpperRight() << std::endl;

      }
    }
  }

  if (verbose)
  {
    std::cerr << "ValidateData : " << checked << " elements - ";

    if (errors)
    {
      std::cerr << "check FAILED (" << errors << " errors)"
                << std::endl;
    }
    else
    {
      std::cerr << "check successful" << std::endl;
    }
  }

  return valid;
}

//==============================================================================
// QuadTree::Cell
//------------------------------------------------------------------------------
// Constructors
//------------------------------------------------------------------------------
inline QuadTree::Cell::Cell(
  dlm::uint64 const  parent,
  dlm::uint64 const  level,
  LocCode     const& locCode,
  AABR        const& area,
  AABR        const& looseArea)
  : area     (area     )
  , looseArea(looseArea)
  , locCode  (locCode  )
  , children (         ) // Value-initialization.
  , parent   (parent   )
  , level    (level    )
{}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
