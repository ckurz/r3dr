//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/ApplicationModules library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef R3DR_ApplicationModules_Socket_Socket_h_
#define R3DR_ApplicationModules_Socket_Socket_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "ApplicationModules/Core/Types.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <utility>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace R3DR
{

//==============================================================================
// Forward declarations
//------------------------------------------------------------------------------
class SocketAddress;

//==============================================================================
// Socket class
//
// WARNING: Copying simply duplicates the file descriptor. Changing the state of
//          one of the copies might have undesirable side effects.
//
// This class abstracts parts of the native socket interface, not requiring any
// system calls on the user side during operation. The sole socket type
// available is a non-blocking streaming IPv4 TCP socket. The timeout value, if
// provided for a function, determines how long the function tries to complete
// before throwing a Timeout exception. Select will not throw an exception, but
// instead returns false if the action cannot be completed in the given time.
// Read will indicate if the other side closed the connection (sent EOF) by
// returning false. All other conditions will trigger a RuntimeError exception.
// The socket uses Open and Close automatically, so there is no need for these
// functions to be used during normal operation (Close happens on destruction).
// For server operation, the Socket has to be bound and listening. Connections
// can then be accepted, which will produce a new Socket for the connection.
// For client operation, a call to connect with the appropriate sever address is
// sufficient.
//------------------------------------------------------------------------------
class Socket
{
public:
  //============================================================================
  // Nested
  //----------------------------------------------------------------------------
  typedef unsigned char Byte;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  static_assert(sizeof(Byte) == 1, "Type Socket::Byte has incorrect size.");
  
  //----------------------------------------------------------------------------
  enum ESelectionType
  {
    FDRead,
    FDWrite,
    FDExcept
  };

  //============================================================================
  // Constructors
  //----------------------------------------------------------------------------
  Socket();
  Socket(int    const  fd   );
  Socket(Socket const& other);

  //============================================================================
  // Destructor
  //----------------------------------------------------------------------------
  ~Socket();

  //============================================================================
  // Operators
  //----------------------------------------------------------------------------
  Socket& operator=(Socket const& other);

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  void Open ();
  void Close();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool Select(ESelectionType const selection, uint32 const timeout) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool Read (void      * const buffer, uint32      & bytes,
                                       uint32 const  timeout = 500) const;
  void Write(void const* const buffer, uint32 const  bytes,
                                       uint32 const  timeout = 500) const;

  //----------------------------------------------------------------------------
  // Client side
  //----------------------------------------------------------------------------
  void Connect(SocketAddress const& remote, uint32 const timeout = 500) const;

  //----------------------------------------------------------------------------
  // Server side
  //----------------------------------------------------------------------------
  void Bind(SocketAddress const& local) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Listen(uint32 const backlog = 32) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  std::pair<Socket, SocketAddress> Accept(uint32 const timeout = 500) const;

private:
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  void Close_(bool const allowedToThrow);

  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  int fd_;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace R3DR

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //R3DR_ApplicationModules_Socket_Socket_h_
