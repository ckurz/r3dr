//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/ApplicationModules library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef R3DR_ApplicationModules_Socket_SocketAddress_h_
#define R3DR_ApplicationModules_Socket_SocketAddress_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "ApplicationModules/Core/Types.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <string>

//==============================================================================
// Forward declarations
//------------------------------------------------------------------------------
struct sockaddr_in;

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace R3DR
{

//==============================================================================
// SocketAddress class
//
// This class represents an (IP-)address/port pair. The address should be
// specified as a string in the standard xxx.xxx.xxx.xxx format. Invalid
// addresses will cause a RuntimeError exception to be raised during conversion.
//------------------------------------------------------------------------------
class SocketAddress
{
public:
  //============================================================================
  // Constructors
  //----------------------------------------------------------------------------
  SocketAddress(
    std::string const& address = "",
    uint16 const port = static_cast<uint16>(-1));
  SocketAddress(sockaddr_in const& other);

  //============================================================================
  // Operators
  //----------------------------------------------------------------------------
  SocketAddress&
  operator=(sockaddr_in const& other);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  operator sockaddr_in() const;

  //============================================================================
  // Accessors
  //----------------------------------------------------------------------------
  std::string const&
  Address() const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint16
  Port() const;

  //============================================================================
  // Mutators
  //----------------------------------------------------------------------------
  std::string&
  Address();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint16&
  Port();

private:
  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  std::string address_;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint16 port_;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} // namespace R3DR

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif // R3DR_ApplicationModules_Socket_SocketAddress_h_
