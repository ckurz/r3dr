//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/ApplicationModules library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "ApplicationModules/Socket/Socket.h"

//------------------------------------------------------------------------------
#include "ApplicationModules/Socket/SocketAddress.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "ApplicationModules/Core/Exception.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <iostream>
#include <iomanip>
#include <sstream>
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <cstring>     // Contains strerror.
#include <cerrno>      // Contains errno.
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <sys/types.h>
#include <sys/socket.h>
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <netinet/in.h>
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <unistd.h>
#include <fcntl.h>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace R3DR
{

//==============================================================================
// Unnamed namespace
//------------------------------------------------------------------------------
namespace
{

//==============================================================================
// Local classes
//------------------------------------------------------------------------------
class CurrentTime
{};

//------------------------------------------------------------------------------
class ErrorString
{};

//==============================================================================
// Local operators
//------------------------------------------------------------------------------
std::ostream& operator<<(std::ostream& stream, CurrentTime const&)
{
  time_t const t = time(0);
  
  tm const& now = *localtime(&t);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  char fill = stream.fill();
  
  stream.fill('0');
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  stream <<                 1900 + now.tm_year << "-"
         << std::setw(2) << 1    + now.tm_mon  << "-"
         << std::setw(2) <<        now.tm_mday << " "
         << std::setw(2) << now.tm_hour << ":"
         << std::setw(2) << now.tm_min  << ":"
         << std::setw(2) << now.tm_sec;
         
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  stream.fill(fill);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
std::ostream& operator<<(std::ostream& stream, ErrorString const&)
{
  stream << "(" << strerror(errno) << ")";
  
  return stream;
}

//==============================================================================
// Unnamed namespace
//------------------------------------------------------------------------------
} //namespace

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
Socket::Socket()
  : fd_(-1)
{
  Open();
}

//------------------------------------------------------------------------------
Socket::Socket(int const fd)
  : fd_(fd)
{}

//------------------------------------------------------------------------------
Socket::Socket(Socket const& other)
  : fd_(-1)
{
  if (other.fd_ < 0) { return; }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  fd_ = dup(other.fd_);
  
  if (fd_ < 0)
  {
    std::stringstream message;

    message << CurrentTime() << ": "
            << "ERROR: In function " << __PRETTY_FUNCTION__ << ", "
            << "with other.fd_ = " << other.fd_ << ": "
            << "Could not duplicate file descriptor "
            << ErrorString() << ".";
   
    throw RuntimeError(message.str());
  }
}

//==============================================================================
// Destructor
//------------------------------------------------------------------------------
Socket::~Socket()
{
  Close_(false);
}

//==============================================================================
// Operator
//------------------------------------------------------------------------------
Socket& Socket::operator=(Socket const& other)
{
  if (this == &other || fd_ == other.fd_)
  {
    return *this;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  Close();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  if (other.fd_ < 0)
  {
    return *this;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  fd_ = dup(other.fd_);
  
  if (fd_ < 0)
  {
    std::stringstream message;

    message << CurrentTime() << ": "
            << "ERROR: In function " << __PRETTY_FUNCTION__ << ", "
            << "with other.fd_ = " << other.fd_ << ": "
            << "Could not duplicate file descriptor "
            << ErrorString() << ".";
   
    throw RuntimeError(message.str());
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  return *this;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
void Socket::Open()
{
  if (fd_ >= 0) { return; }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  fd_ = socket(AF_INET, SOCK_STREAM, 0);
  
  if (fd_ < 0)
  {
    std::stringstream message;

    message << CurrentTime() << ": "
            << "ERROR: In function " << __PRETTY_FUNCTION__ << ", "
            << "with fd_ = " << fd_ << ": "
            << "Could not open socket "
            << ErrorString() << ".";
   
    throw RuntimeError(message.str());
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  // Set socket mode to non-blocking.
  int flags = fcntl(fd_, F_GETFL, 0);
  
  if (flags < 0)
  {
    std::stringstream message;

    message << CurrentTime() << ": "
            << "ERROR: In function " << __PRETTY_FUNCTION__ << ", "
            << "with fd_ = " << fd_ << ": "
            << "Could not retrieve file descriptor flags "
            << ErrorString() << ".";
   
    throw RuntimeError(message.str());
  }

  if (fcntl(fd_, F_SETFL, flags | O_NONBLOCK) < 0)
  {
    std::stringstream message;

    message << CurrentTime() << ": "
            << "ERROR: In function " << __PRETTY_FUNCTION__ << ", "
            << "with fd_ = " << fd_ << ": "
            << "Could not set file descriptor flags "
            << ErrorString() << ".";
   
    throw RuntimeError(message.str());
  }
}

//------------------------------------------------------------------------------
void Socket::Close()
{
  Close_(true);
}

//------------------------------------------------------------------------------
bool Socket::Select(ESelectionType const selection, uint32 const timeout) const
{
  fd_set fdSet;
  
  FD_ZERO(     &fdSet);
  FD_SET (fd_, &fdSet);
  
  int const maximum = fd_ + 1; // See documentation of select.
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  timeval tvTimeout;
  
  tvTimeout.tv_sec  =  timeout                            / 1000;
  tvTimeout.tv_usec = (timeout - tvTimeout.tv_sec * 1000) * 1000;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  int result = -1;
  
  switch (selection)
  {
  case FDRead:   { result = select(maximum, &fdSet, 0, 0, &tvTimeout); } break;
  case FDWrite:  { result = select(maximum, 0, &fdSet, 0, &tvTimeout); } break;
  case FDExcept: { result = select(maximum, 0, 0, &fdSet, &tvTimeout); } break;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  default:
    {
      std::stringstream message;

      message << CurrentTime() << ": "
              << "ERROR: In function " << __PRETTY_FUNCTION__ << ", "
              << "with fd_ = " << fd_ << ", "
              << "selection = " << selection << ", and "
              << "timeout = " << timeout << ": "
              << "default branch of switch reached.";
   
      throw RuntimeError(message.str());
    }
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (result < 0)
  {
    std::stringstream message;

    message << CurrentTime() << ": "
            << "ERROR: In function " << __PRETTY_FUNCTION__ << ", "
            << "with fd_ = " << fd_ << ", "
            << "selection = " << selection << ", and "
            << "timeout = " << timeout << ": "
            << "Could not select file descriptor "
            << ErrorString() << ".";
   
    throw RuntimeError(message.str());
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (result == 0) // Timeout.
  {
    return false;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (result != 1)
  {
    std::stringstream message;

    message << CurrentTime() << ": "
            << "ERROR: In function " << __PRETTY_FUNCTION__ << ", "
            << "with fd_ = " << fd_ << ", "
            << "selection = " << selection << ", and "
            << "timeout = " << timeout << ": "
            << "Received " << result << " file descriptors instead of one.";
   
    throw RuntimeError(message.str());
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (!FD_ISSET(fd_, &fdSet))
  {
    std::stringstream message;

    message << CurrentTime() << ": "
            << "ERROR: In function " << __PRETTY_FUNCTION__ << ", "
            << "with fd_ = " << fd_ << ", "
            << "selection = " << selection << ", and "
            << "timeout = " << timeout << ": "
            << "File descriptor not contained in set.";
   
    throw RuntimeError(message.str());
  }
    
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return true;
}

//------------------------------------------------------------------------------
bool Socket::Read(
  void* const buffer, uint32& bytes, uint32 const timeout) const
{
  if (bytes == 0)
  {
    std::stringstream message;

    message << CurrentTime() << ": "
            << "ERROR: In function " << __PRETTY_FUNCTION__ << ", "
            << "with fd_ = " << fd_ << ", "
            << "buffer = " << buffer << ", "
            << "bytes = " << bytes << ", and "
            << "timeout = " << timeout << ": "
            << "bytes cannot be zero.";
   
    throw RuntimeError(message.str());
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Byte* const pointer = static_cast<Byte*>(buffer); // Avoid void* arithmetic.

  ssize_t totalBytesRead = 0;
  
  uint32 passes     = 0;
  uint32 interrupts = 0;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  while (totalBytesRead < ssize_t(bytes))
  {
    ++passes;
    
    ssize_t const remaining = bytes - totalBytesRead;
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    if (!Select(FDRead, timeout))
    {
      std::stringstream message;

      message << CurrentTime() << ": "
              << "ERROR: In function " << __PRETTY_FUNCTION__ << ", "
              << "with fd_ = " << fd_ << ", "
              << "buffer = " << buffer << ", "
              << "bytes = " << bytes << ", and "
              << "timeout = " << timeout << ": "
              << "Could not read from socket (timeout).";
   
      throw Timeout(message.str());
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ssize_t const bytesRead = read(fd_, pointer + totalBytesRead, remaining);
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    if (bytesRead < 0 && errno == EINTR)
    {
      ++interrupts;
      
      continue;
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    if (bytesRead < 0)
    {
      std::stringstream message;

      message << CurrentTime() << ": "
              << "ERROR: In function " << __PRETTY_FUNCTION__ << ", "
              << "with fd_ = " << fd_ << ", "
              << "buffer = " << buffer << ", "
              << "bytes = " << bytes << ", and "
              << "timeout = " << timeout << ": "
              << "Could not read from socket "
              << ErrorString() << ".";
   
      throw RuntimeError(message.str());
    }
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    if (bytesRead == 0)
    {
      return false; // EOF; connection has been closed.
    }
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    totalBytesRead += bytesRead;
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  if (passes != 1)
  {
    std::cout << CurrentTime() << ": "
              << "NOTICE: In function " << __PRETTY_FUNCTION__ << ", "
              << "with fd_ = " << fd_ << ", "
              << "buffer = " << buffer << ", "
              << "bytes = " << bytes << ", and "
              << "timeout = " << timeout << ": "
              << "Reading took " << passes << " passes "
              << "(" << interrupts << " interrupts)." << std::endl;
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  return true;
}

//------------------------------------------------------------------------------
void Socket::Write(
  void const* const buffer, uint32 const bytes, uint32 const timeout) const
{
  if (bytes == 0)
  {
    std::stringstream message;

    message << CurrentTime() << ": "
            << "ERROR: In function " << __PRETTY_FUNCTION__ << ", "
            << "with fd_ = " << fd_ << ", "
            << "buffer = " << buffer << ", "
            << "bytes = " << bytes << ", and "
            << "timeout = " << timeout << ": "
            << "bytes cannot be zero.";
   
    throw RuntimeError(message.str());
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Byte const* const pointer = static_cast<Byte const*>(buffer); // Avoid void* 
                                                                // arithmetic.

  ssize_t totalBytesWritten = 0;
  
  uint32 passes     = 0;
  uint32 interrupts = 0;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  while (totalBytesWritten < ssize_t(bytes))
  {
    ++passes;
    
    ssize_t const remaining = bytes - totalBytesWritten;
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    if (!Select(FDWrite, timeout))
    {
      std::stringstream message;

      message << CurrentTime() << ": "
              << "ERROR: In function " << __PRETTY_FUNCTION__ << ", "
              << "with fd_ = " << fd_ << ", "
              << "buffer = " << buffer << ", "
              << "bytes = " << bytes << ", and "
              << "timeout = " << timeout << ": "
              << "Could not write to socket (timeout).";
   
      throw Timeout(message.str());
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ssize_t const bytesWritten =
      write(fd_, pointer + totalBytesWritten, remaining);
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    if (bytesWritten < 0 && errno == EINTR)
    {
      ++interrupts;
      
      continue;
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    if (bytesWritten < 0)
    {
      std::stringstream message;

      message << CurrentTime() << ": "
              << "ERROR: In function " << __PRETTY_FUNCTION__ << ", "
              << "with fd_ = " << fd_ << ", "
              << "buffer = " << buffer << ", "
              << "bytes = " << bytes << ", and "
              << "timeout = " << timeout << ": "
              << "Could not write to socket "
              << ErrorString() << ".";
   
      throw RuntimeError(message.str());
    }
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    if (bytesWritten == 0)
    {
      std::stringstream message;

      message << CurrentTime() << ": "
              << "ERROR: In function " << __PRETTY_FUNCTION__ << ", "
              << "with fd_ = " << fd_ << ", "
              << "buffer = " << buffer << ", "
              << "bytes = " << bytes << ", and "
              << "timeout = " << timeout << ": "
              << "There were no bytes written.";
   
      throw RuntimeError(message.str());
    }
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    totalBytesWritten += bytesWritten;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  if (passes != 1)
  {
    std::cout << CurrentTime() << ": "
              << "NOTICE: In function " << __PRETTY_FUNCTION__ << ", "
              << "with fd_ = " << fd_ << ", "
              << "buffer = " << buffer << ", "
              << "bytes = " << bytes << ", and "
              << "timeout = " << timeout << ": "
              << "Writing took " << passes << " passes "
              << "(" << interrupts << " interrupts)." << std::endl;
  }
}

//------------------------------------------------------------------------------
void Socket::Connect(
  SocketAddress const& remote, uint32 const timeout) const
{
  sockaddr_in const remoteSA = remote;
  
  sockaddr const* pointer = reinterpret_cast<sockaddr const*>(&remoteSA);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  int const result = connect(fd_, pointer, sizeof(remoteSA));

  if (result < 0 && errno != EINPROGRESS)
  {
    std::stringstream message;

    message << CurrentTime() << ": "
            << "ERROR: In function " << __PRETTY_FUNCTION__ << ", "
            << "with fd_ = " << fd_ << ", "
            << "remote.Address() = " << remote.Address() << ", "
            << "remote.Port() = " << remote.Port() << ", and "
            << "timeout = " << timeout << ": "
            << "Could not connect to remote socket "
            << ErrorString() << ".";
   
    throw RuntimeError(message.str());
  }
  
  if (result < 0)
  {
    if (!Select(FDWrite, timeout))
    {
      std::stringstream message;

      message << CurrentTime() << ": "
              << "ERROR: In function " << __PRETTY_FUNCTION__ << ", "
              << "with fd_ = " << fd_ << ", "
              << "remote.Address() = " << remote.Address() << ", "
              << "remote.Port() = " << remote.Port() << ", and "
              << "timeout = " << timeout << ": "
              << "Could not connect to remote socket (timeout).";
   
      throw Timeout(message.str());
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -    
    int error = 0;

    socklen_t length = sizeof(error);
    
    if (getsockopt(fd_, SOL_SOCKET, SO_ERROR, &error, &length) < 0)
    {
      std::stringstream message;

      message << CurrentTime() << ": "
              << "ERROR: In function " << __PRETTY_FUNCTION__ << ", "
              << "with fd_ = " << fd_ << ", "
              << "remote.Address() = " << remote.Address() << ", "
              << "remote.Port() = " << remote.Port() << ", and "
              << "timeout = " << timeout << ": "
              << "Could not obtain socket option "
              << ErrorString() << ".";
   
      throw RuntimeError(message.str());
    }
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -    
    if (error != 0)
    {
      std::stringstream message;

      message << CurrentTime() << ": "
              << "ERROR: In function " << __PRETTY_FUNCTION__ << ", "
              << "with fd_ = " << fd_ << ", "
              << "remote.Address() = " << remote.Address() << ", "
              << "remote.Port() = " << remote.Port() << ", and "
              << "timeout = " << timeout << ": "
              << "Could not connect to remote socket "
              << ErrorString() << ".";
   
      throw RuntimeError(message.str());
    }
  }
}

//------------------------------------------------------------------------------
void Socket::Bind(SocketAddress const& local) const
{
  int const on = 1;

  // Set the socket to reuse the address. This has to happen before bind.
  // If the address is not reused, the server will have to wait for a certain
  // time before it can reopen the socket (if the server ends the connection).
  if (setsockopt(fd_, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)) < 0)
  {
    std::stringstream message;

    message << CurrentTime() << ": "
            << "ERROR: In function " << __PRETTY_FUNCTION__ << ", "
            << "with fd_ = " << fd_ << ", "
            << "local.Address() = " << local.Address() << ", and "
            << "local.Port() = " << local.Port() << ": "
            << "Could not set socket option "
            << ErrorString() << ".";
   
    throw RuntimeError(message.str());
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  sockaddr_in const localSA = local;
  
  sockaddr const* pointer = reinterpret_cast<sockaddr const*>(&localSA);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  if (bind(fd_, pointer, sizeof(localSA)) < 0)
  {
    std::stringstream message;

    message << CurrentTime() << ": "
            << "ERROR: In function " << __PRETTY_FUNCTION__ << ", "
            << "with fd_ = " << fd_ << ", "
            << "local.Address() = " << local.Address() << ", and "
            << "local.Port() = " << local.Port() << ": "
            << "Could not bind address to socket "
            << ErrorString() << ".";
   
    throw RuntimeError(message.str());
  }
}

//------------------------------------------------------------------------------
void Socket::Listen(uint32 const backlog) const
{
  if (listen(fd_, backlog) < 0)
  {
    std::stringstream message;

    message << CurrentTime() << ": "
            << "ERROR: In function " << __PRETTY_FUNCTION__ << ", "
            << "with fd_ = " << fd_ << "and "
            << "backlog = " << backlog << ": "
            << "Could not listen to socket "
            << ErrorString() << ".";
   
    throw RuntimeError(message.str());
  }
}

//------------------------------------------------------------------------------
std::pair<Socket, SocketAddress> Socket::Accept(uint32 const timeout) const
{
  sockaddr_in clientSA;
  socklen_t   clientSALength = sizeof(clientSA);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  bzero(reinterpret_cast<char*>(&clientSA), sizeof(clientSA)); // Returns void.
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  sockaddr* pointer = reinterpret_cast<sockaddr*>(&clientSA);
  
  errno = 0; // To have reliable information if something went wrong.
  
  uint32 passes = 0;

  int connection = -1;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  while (connection < 0 && (errno == 0            ||
                            errno == EAGAIN       ||
                            errno == EWOULDBLOCK  ||
                            errno == ENETDOWN     ||
                            errno == EPROTO       ||
                            errno == ENOPROTOOPT  ||
                            errno == EHOSTDOWN    ||
                            errno == ENONET       ||
                            errno == EHOSTUNREACH ||
                            errno == EOPNOTSUPP   ||
                            errno == ENETUNREACH))
  {
    ++passes;

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    if (!Select(FDRead, timeout))
    {
      std::stringstream message;

      message << CurrentTime() << ": "
              << "ERROR: In function " << __PRETTY_FUNCTION__ << ", "
              << "with fd_ = " << fd_ << " and "
              << "timeout = " << timeout << ": "
              << "Could not accept connection on socket (timeout).";
   
      throw Timeout(message.str());
    }
  
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    connection = accept4(fd_, pointer, &clientSALength, SOCK_NONBLOCK);
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  if (connection < 0)
  {
    std::stringstream message;

    message << CurrentTime() << ": "
            << "ERROR: In function " << __PRETTY_FUNCTION__ << ", "
            << "with fd_ = " << fd_ << " and "
            << "timeout = " << timeout << ": "
            << "Could not accept connection on socket "
            << ErrorString() << ".";
   
    throw RuntimeError(message.str());
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  if (passes != 1)
  {
    std::cout << CurrentTime() << ": "
              << "NOTICE: In function " << __PRETTY_FUNCTION__ << ", "
              << "with fd_ = " << fd_ << " and "
              << "timeout = " << timeout << ": "
              << "Accept took " << passes << " passes." << std::endl;
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return std::pair<Socket, SocketAddress>(connection, clientSA);
}

//------------------------------------------------------------------------------
void Socket::Close_(bool const allowedToThrow)
{
  if (fd_ < 0) { return; }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  if (close(fd_) < 0)
  {
    std::stringstream message;

    message << CurrentTime() << ": "
            << "ERROR: In function " << __PRETTY_FUNCTION__ << ": "
            << "Could not close file descriptor "
            << ErrorString() << ".";
   
    if (allowedToThrow)
    {
      throw RuntimeError(message.str());
    }
    else
    {
      std::cerr << std::endl << message.str() << std::endl;
    }
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  fd_ = -1;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace R3DR
