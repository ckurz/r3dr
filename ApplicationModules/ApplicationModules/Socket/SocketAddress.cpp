//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/ApplicationModules library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "ApplicationModules/Socket/SocketAddress.h"

//------------------------------------------------------------------------------
#include "ApplicationModules/Core/Exception.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <iostream>
#include <iomanip>
#include <sstream>
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <cstring>     // Contains strerror.
#include <cerrno>      // Contains errno.
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <netinet/in.h>
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <arpa/inet.h>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace R3DR
{

//==============================================================================
// Unnamed namespace
//------------------------------------------------------------------------------
namespace
{

//==============================================================================
// Local classes
//------------------------------------------------------------------------------
class CurrentTime
{};

//------------------------------------------------------------------------------
//class ErrorString
//{};

//==============================================================================
// Local operators
//------------------------------------------------------------------------------
std::ostream& operator<<(std::ostream& stream, CurrentTime const&)
{
  time_t const t = time(0);
  
  tm const& now = *localtime(&t);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  char fill = stream.fill();
  
  stream.fill('0');
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  stream <<                 1900 + now.tm_year << "-"
         << std::setw(2) << 1    + now.tm_mon  << "-"
         << std::setw(2) <<        now.tm_mday << " "
         << std::setw(2) << now.tm_hour << ":"
         << std::setw(2) << now.tm_min  << ":"
         << std::setw(2) << now.tm_sec;
         
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  stream.fill(fill);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
//std::ostream& operator<<(std::ostream& stream, ErrorString const&)
//{
//  stream << "(" << strerror(errno) << ")";
//  
//  return stream;
//}

//==============================================================================
// Unnamed namespace
//------------------------------------------------------------------------------
} //namespace

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
SocketAddress::SocketAddress(std::string const& address, uint16 const port)
  : address_(address)
  , port_   (port   )
{}

SocketAddress::SocketAddress(sockaddr_in const& other)
  : address_(inet_ntoa(other.sin_addr))
  , port_   (ntohs    (other.sin_port))
{}

//==============================================================================
// Operators
//------------------------------------------------------------------------------
SocketAddress& SocketAddress::operator=(sockaddr_in const& other)
{
  address_ = inet_ntoa(other.sin_addr);
  port_    = ntohs    (other.sin_port);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
SocketAddress::operator sockaddr_in() const
{
  sockaddr_in result;

  bzero(reinterpret_cast<char*>(&result), sizeof(result)); // Returns void.
  
  result.sin_family = AF_INET;
  result.sin_port   = htons(port_);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (inet_aton(address_.c_str(), &result.sin_addr) == 0)
  {
    std::stringstream message;

    message << CurrentTime() << ": "
            << "ERROR: In function " << __PRETTY_FUNCTION__ << ", with"
            << "address_ = " << address_ << " and "
            << "port_ = " << port_ << ": "
            << "Address is invalid.";
   
    throw RuntimeError(message.str());
  }
 
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result; 
}

//==============================================================================
// Accessors
//------------------------------------------------------------------------------
std::string const& SocketAddress::Address() const
{
  return address_;
}

//------------------------------------------------------------------------------
uint16 SocketAddress::Port() const
{
  return port_;
}
  
//==============================================================================
// Mutators
//------------------------------------------------------------------------------
std::string& SocketAddress::Address()
{
  return address_;
}

//------------------------------------------------------------------------------
uint16& SocketAddress::Port()
{
  return port_;
}  

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace R3DR
