//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/ApplicationModules library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef R3DR_ApplicationModules_Core_Exception_h_
#define R3DR_ApplicationModules_Core_Exception_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include <stdexcept>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace R3DR
{

//==============================================================================
// RuntimeError exception class
//------------------------------------------------------------------------------
class RuntimeError : public std::runtime_error
{
public:
  //============================================================================
  // Constructors
  //----------------------------------------------------------------------------
  RuntimeError(std::string const& description);

  //============================================================================
  // Destructor
  //----------------------------------------------------------------------------
  virtual ~RuntimeError() throw();
};

//==============================================================================
// RuntimeError exception class
//------------------------------------------------------------------------------
class Timeout : public RuntimeError
{
public:
  //============================================================================
  // Constructors
  //----------------------------------------------------------------------------
  Timeout(std::string const& description);

  //============================================================================
  // Destructor
  //----------------------------------------------------------------------------
  virtual ~Timeout() throw();
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace R3DR

//==============================================================================
// Inline code
//------------------------------------------------------------------------------
// This file is likely to be included from source files only, and
// for convenience includes the corresponding inline file automatically.
//------------------------------------------------------------------------------
#include "ApplicationModules/Core/Exception.inline.h"

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //R3DR_ApplicationModules_Core_Exception_h_
