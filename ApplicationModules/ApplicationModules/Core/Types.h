//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/ApplicationModules library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef R3DR_ApplicationModules_Core_Types_h_
#define R3DR_ApplicationModules_Core_Types_h_

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace R3DR
{

//==============================================================================
// Type definitions
//------------------------------------------------------------------------------
// Signed integer types
//------------------------------------------------------------------------------
typedef int           int32;
typedef long long int int64;

//------------------------------------------------------------------------------
// Unsigned integer types
//------------------------------------------------------------------------------
typedef unsigned short         uint16;
typedef unsigned int           uint32;
typedef unsigned long long int uint64;

//------------------------------------------------------------------------------
// Floating point types
//------------------------------------------------------------------------------
typedef float  float32;
typedef double float64;

//==============================================================================
// uint64 value to specify invalid contents
//------------------------------------------------------------------------------
static uint64 const Invalid = uint64(-1);

//==============================================================================
// Sanity checks
//------------------------------------------------------------------------------
namespace
{

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static_assert(sizeof(int32) == 4, "Type int32 has incorrect size.");
static_assert(sizeof(int64) == 8, "Type int64 has incorrect size.");

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static_assert(sizeof(uint16) == 2, "Type uint16 has incorrect size.");
static_assert(sizeof(uint32) == 4, "Type uint32 has incorrect size.");
static_assert(sizeof(uint64) == 8, "Type uint64 has incorrect size.");

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static_assert(sizeof(float32) == 4, "Type float32 has incorrect size.");
static_assert(sizeof(float64) == 8, "Type float64 has incorrect size.");

} // namespace

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} // namespace R3DR

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif // R3DR_ApplicationModules_Core_Types_h_
