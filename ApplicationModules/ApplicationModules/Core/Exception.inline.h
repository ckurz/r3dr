//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/ApplicationModules library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef R3DR_ApplicationModules_Core_Exception_inline_h_
#define R3DR_ApplicationModules_Core_Exception_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "ApplicationModules/Core/Exception.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace R3DR
{

//==============================================================================
// RuntimeError
//------------------------------------------------------------------------------
// Constructors
//------------------------------------------------------------------------------
inline RuntimeError::RuntimeError(std::string const& description)
  : std::runtime_error(description)
{}

//==============================================================================
// Destructor
//------------------------------------------------------------------------------
inline RuntimeError::~RuntimeError() throw()
{}

//==============================================================================
// Timeout
//------------------------------------------------------------------------------
// Constructors
//------------------------------------------------------------------------------
inline Timeout::Timeout(std::string const& description)
  : RuntimeError(description)
{}

//==============================================================================
// Destructor
//------------------------------------------------------------------------------
inline Timeout::~Timeout() throw()
{}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace R3DR

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //R3DR_Daemon_Exception_inline_h_
