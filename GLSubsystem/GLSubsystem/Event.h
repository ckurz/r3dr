//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/GLSubsystem library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef GLSubsystem_Event_h_
#define GLSubsystem_Event_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "GLSubsystem/KeyCode.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace GLSubsystem
{

//==============================================================================
// Event struct
//------------------------------------------------------------------------------
struct Event
{
  //============================================================================
  // Nested
  //----------------------------------------------------------------------------
  enum Type
  {
    MouseEvent,
    KeyboardEvent,
    ResizeEvent
  };

  //----------------------------------------------------------------------------
  enum KeyboardEventType
  {
    Press,
    Release
  };

  //----------------------------------------------------------------------------
  enum MouseEventType
  {
    LeftButtonDown,
    LeftButtonUp,
    RightButtonDown,
    RightButtonUp,
    MiddleButtonDown,
    MiddleButtonUp,
    Move,
    Wheel
  };

  //============================================================================
  // Constructor
  //----------------------------------------------------------------------------
  Event()
    : type   ()
    , shift  ()
    , control()
    , Mouse  ()
  {}

  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  Type type;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool shift;
  bool control;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  union
  {
    struct
    {
      MouseEventType type;
      int xPosition;
      int yPosition;
      int wheelMotion;
      int buttonStates;
    } Mouse;

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    struct
    {
      KeyboardEventType type;
      KeyCode keyCode;
      char character;
    } Keyboard;

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    struct
    {
      int width;
      int height;
    } Resize;
  };
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
