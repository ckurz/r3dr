//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/GLSubsystem library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef GLSubsystem_KeyCode_h_
#define GLSubsystem_KeyCode_h_

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace GLSubsystem
{

//==============================================================================
// KeyCode enum
// This enumeration provides definitions for the key codes issued by the
// subsystem. It is based on X11/keysymdef.h.
//------------------------------------------------------------------------------
enum KeyCode
{
  //============================================================================
  // #ifdef XK_MISCELLANY
  //----------------------------------------------------------------------------
  KEY_BackSpace         = 0xff08,
  KEY_Tab               = 0xff09,
//KEY_Linefeed          = 0xff0a,
  KEY_Clear             = 0xff0b,
  KEY_Return            = 0xff0d,
  KEY_Pause             = 0xff13,
  KEY_Scroll_Lock       = 0xff14,
//KEY_Sys_Req           = 0xff15,
  KEY_Escape            = 0xff1b,
  KEY_Delete            = 0xffff,
  //
  // Omitted international & multi-key character composition.
  // Omitted Japanese keyboard support.
  //
  KEY_Home              = 0xff50,
  KEY_Left              = 0xff51,
  KEY_Up                = 0xff52,
  KEY_Right             = 0xff53,
  KEY_Down              = 0xff54,
//KEY_Prior             = 0xff55,
  KEY_Page_Up           = 0xff55,
//KEY_Next              = 0xff56,
  KEY_Page_Down         = 0xff56,
  KEY_End               = 0xff57,
  KEY_Begin             = 0xff58,
  //
  KEY_Select            = 0xff60,
//KEY_Print             = 0xff61,
//KEY_Execute           = 0xff62,
  KEY_Insert            = 0xff63,
//KEY_Undo              = 0xff65,
//KEY_Redo              = 0xff66,
  KEY_Menu              = 0xff67,
//KEY_Find              = 0xff68,
  KEY_Cancel            = 0xff69,
  KEY_Help              = 0xff6a,
//KEY_Break             = 0xff6b,
  KEY_Mode_switch       = 0xff7e,
//KEY_script_switch     = 0xff7e,
  KEY_Num_Lock          = 0xff7f,
  //
  KEY_KP_Space          = 0xff80,
  KEY_KP_Tab            = 0xff89,
  KEY_KP_Enter          = 0xff8d,
  KEY_KP_F1             = 0xff91,
  KEY_KP_F2             = 0xff92,
  KEY_KP_F3             = 0xff93,
  KEY_KP_F4             = 0xff94,
  KEY_KP_Home           = 0xff95,
  KEY_KP_Left           = 0xff96,
  KEY_KP_Up             = 0xff97,
  KEY_KP_Right          = 0xff98,
  KEY_KP_Down           = 0xff99,
//KEY_KP_Prior          = 0xff9a,
  KEY_KP_Page_Up        = 0xff9a,
//KEY_KP_Next           = 0xff9b,
  KEY_KP_Page_Down      = 0xff9b,
  KEY_KP_End            = 0xff9c,
  KEY_KP_Begin          = 0xff9d,
  KEY_KP_Insert         = 0xff9e,
  KEY_KP_Delete         = 0xff9f,
  KEY_KP_Equal          = 0xffbd,
  KEY_KP_Multiply       = 0xffaa,
  KEY_KP_Add            = 0xffab,
  KEY_KP_Separator      = 0xffac,
  KEY_KP_Subtract       = 0xffad,
  KEY_KP_Decimal        = 0xffae,
  KEY_KP_Divide         = 0xffaf,
  //
  KEY_KP_0              = 0xffb0,
  KEY_KP_1              = 0xffb1,
  KEY_KP_2              = 0xffb2,
  KEY_KP_3              = 0xffb3,
  KEY_KP_4              = 0xffb4,
  KEY_KP_5              = 0xffb5,
  KEY_KP_6              = 0xffb6,
  KEY_KP_7              = 0xffb7,
  KEY_KP_8              = 0xffb8,
  KEY_KP_9              = 0xffb9,
  //
  KEY_F1                = 0xffbe,
  KEY_F2                = 0xffbf,
  KEY_F3                = 0xffc0,
  KEY_F4                = 0xffc1,
  KEY_F5                = 0xffc2,
  KEY_F6                = 0xffc3,
  KEY_F7                = 0xffc4,
  KEY_F8                = 0xffc5,
  KEY_F9                = 0xffc6,
  KEY_F10               = 0xffc7,
  KEY_F11               = 0xffc8,
  KEY_F12               = 0xffc9,
  //
  // Omitted F keys 13-35, L keys and R keys.
  //
  KEY_Shift_L           = 0xffe1,
  KEY_Shift_R           = 0xffe2,
  KEY_Control_L         = 0xffe3,
  KEY_Control_R         = 0xffe4,
  KEY_Caps_Lock         = 0xffe5,
  KEY_Shift_Lock        = 0xffe6,
  //
  KEY_Meta_L            = 0xffe7,
  KEY_Meta_R            = 0xffe8,
  KEY_Alt_L             = 0xffe9,
  KEY_Alt_R             = 0xffea,
  //
  // Omitted Super & Hyper.
  //
  // Omitted XKB keys.
  //
  // Omitted 3270 terminal keys.
  //
  //============================================================================
  // #ifdef XK_LATIN1
  //----------------------------------------------------------------------------
  KEY_space             = 0x0020,
  KEY_exclam            = 0x0021,
  KEY_quotedbl          = 0x0022,
  KEY_numbersign        = 0x0023,
  KEY_dollar            = 0x0024,
  KEY_percent           = 0x0025,
  KEY_ampersand         = 0x0026,
  KEY_apostrophe        = 0x0027,
//KEY_quoteright        = 0x0027, // Deprecated.
  KEY_parenleft         = 0x0028,
  KEY_parenright        = 0x0029,
  KEY_asterisk          = 0x002a,
  KEY_plus              = 0x002b,
  KEY_comma             = 0x002c,
  KEY_minus             = 0x002d,
  KEY_period            = 0x002e,
  KEY_slash             = 0x002f,
  KEY_0                 = 0x0030,
  KEY_1                 = 0x0031,
  KEY_2                 = 0x0032,
  KEY_3                 = 0x0033,
  KEY_4                 = 0x0034,
  KEY_5                 = 0x0035,
  KEY_6                 = 0x0036,
  KEY_7                 = 0x0037,
  KEY_8                 = 0x0038,
  KEY_9                 = 0x0039,
  KEY_colon             = 0x003a,
  KEY_semicolon         = 0x003b,
  KEY_less              = 0x003c,
  KEY_equal             = 0x003d,
  KEY_greater           = 0x003e,
  KEY_question          = 0x003f,
  KEY_at                = 0x0040,
  KEY_A                 = 0x0041,
  KEY_B                 = 0x0042,
  KEY_C                 = 0x0043,
  KEY_D                 = 0x0044,
  KEY_E                 = 0x0045,
  KEY_F                 = 0x0046,
  KEY_G                 = 0x0047,
  KEY_H                 = 0x0048,
  KEY_I                 = 0x0049,
  KEY_J                 = 0x004a,
  KEY_K                 = 0x004b,
  KEY_L                 = 0x004c,
  KEY_M                 = 0x004d,
  KEY_N                 = 0x004e,
  KEY_O                 = 0x004f,
  KEY_P                 = 0x0050,
  KEY_Q                 = 0x0051,
  KEY_R                 = 0x0052,
  KEY_S                 = 0x0053,
  KEY_T                 = 0x0054,
  KEY_U                 = 0x0055,
  KEY_V                 = 0x0056,
  KEY_W                 = 0x0057,
  KEY_X                 = 0x0058,
  KEY_Y                 = 0x0059,
  KEY_Z                 = 0x005a,
  KEY_bracketleft       = 0x005b,
  KEY_backslash         = 0x005c,
  KEY_bracketright      = 0x005d,
  KEY_asciicircum       = 0x005e,
  KEY_underscore        = 0x005f,
  KEY_grave             = 0x0060,
//KEY_quoteleft         = 0x0060, // Deprecated.
  KEY_a                 = 0x0061,
  KEY_b                 = 0x0062,
  KEY_c                 = 0x0063,
  KEY_d                 = 0x0064,
  KEY_e                 = 0x0065,
  KEY_f                 = 0x0066,
  KEY_g                 = 0x0067,
  KEY_h                 = 0x0068,
  KEY_i                 = 0x0069,
  KEY_j                 = 0x006a,
  KEY_k                 = 0x006b,
  KEY_l                 = 0x006c,
  KEY_m                 = 0x006d,
  KEY_n                 = 0x006e,
  KEY_o                 = 0x006f,
  KEY_p                 = 0x0070,
  KEY_q                 = 0x0071,
  KEY_r                 = 0x0072,
  KEY_s                 = 0x0073,
  KEY_t                 = 0x0074,
  KEY_u                 = 0x0075,
  KEY_v                 = 0x0076,
  KEY_w                 = 0x0077,
  KEY_x                 = 0x0078,
  KEY_y                 = 0x0079,
  KEY_z                 = 0x007a,
  KEY_braceleft         = 0x007b,
  KEY_bar               = 0x007c,
  KEY_braceright        = 0x007d,
  KEY_asciitilde        = 0x007e,
  //
  // Starting here, only selected keys are given.
  //
  KEY_section           = 0x00a7,
  KEY_degree            = 0x00b0,
  KEY_acute             = 0x00b4,
  KEY_mu                = 0x00b5,
  //
  KEY_Adiaeresis        = 0x00c4,
  KEY_Odiaeresis        = 0x00d6,
  KEY_Udiaeresis        = 0x00dc,
  //
  KEY_ssharp            = 0x00df,
  //
  KEY_adiaeresis        = 0x00e4,
  KEY_odiaeresis        = 0x00f6,
  KEY_udiaeresis        = 0x00fc,
  //
  KEY_EuroSign          = 0x20ac
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
