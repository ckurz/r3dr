//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmImage library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmImage_Gray_inline_h_
#define dlmImage_Gray_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmImage/Gray.h"

//------------------------------------------------------------------------------
#include "dlm/math/Vector.inline.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmImage
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
template <typename T>
inline Gray<T>::Gray(BaseType const value)
  : BaseClass(value)
{}

//------------------------------------------------------------------------------
template <typename T>
inline Gray<T>::Gray(BaseClass const& value)
  : BaseClass(value)
{}

//==============================================================================
// Operators
//------------------------------------------------------------------------------
template <typename T>
inline Gray<T>& Gray<T>::operator=(BaseType const value)
{
  BaseClass::X() = value;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
template <typename T>
inline Gray<T>& Gray<T>::operator=(BaseClass const& vector)
{
  BaseClass::operator=(vector);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
template <typename T>
inline Gray<T>::operator T() const
{
  return BaseClass::X();
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
