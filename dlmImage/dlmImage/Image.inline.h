//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmImage library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmImage_Image_inline_h_
#define dlmImage_Image_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmImage/Image.h"

//------------------------------------------------------------------------------
#include "dlm/core/AutoArrayPtr.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmImage
{

//==============================================================================
// Friends
//------------------------------------------------------------------------------
template <typename T>
inline void swap(Image<T>& first, Image<T>& second)
{
  using std::swap;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  swap(first.elements_, second.elements_);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  swap(first.width_,  second.width_ );
  swap(first.height_, second.height_);
}

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
template <typename T>
inline Image<T>::Image(dlm::uint64 const width, dlm::uint64 const height)
  : elements_(new T[width * height]()) // Value-initialization.
  , width_   (width                  )
  , height_  (height                 )
{}

//------------------------------------------------------------------------------
template <typename T>
inline Image<T>::Image(Image const& other)
  : elements_(new T[other.ArraySize()])
  , width_   (other.width_            )
  , height_  (other.height_           )
{
  for (dlm::uint64 i = 0; i < other.ArraySize(); ++i)
  {
    elements_[i] = other.elements_[i];
  }
}

//------------------------------------------------------------------------------
template <typename T>
inline Image<T>::Image(Image&& other)
  : elements_(std::move(other.elements_))
  , width_   (other.width_              )
  , height_  (other.height_             )
{
  other.width_  = 0;
  other.height_ = 0;
}

//==============================================================================
// Operators
//------------------------------------------------------------------------------
template <typename T>
inline Image<T>& Image<T>::operator=(Image other)
{
  swap(*this, other);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//==============================================================================
// Operators: Accessors
//------------------------------------------------------------------------------
template <typename T>
inline T const& Image<T>::operator()(
  dlm::uint64 const x, dlm::uint64 const y) const
{
  DLM_ASSERT(x < width_,  DLM_OUT_OF_RANGE);
  DLM_ASSERT(y < height_, DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[y * width_ + x];
}

//==============================================================================
// Operators: Mutators
//------------------------------------------------------------------------------
template <typename T>
inline T& Image<T>::operator()(dlm::uint64 const x, dlm::uint64 const y)
{
  DLM_ASSERT(x < width_,  DLM_OUT_OF_RANGE);
  DLM_ASSERT(y < height_, DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[y * width_ + x];
}

//==============================================================================
// Accessors
//------------------------------------------------------------------------------
template <typename T>
inline dlm::uint64 Image<T>::Width() const
{
  return width_;
}

//------------------------------------------------------------------------------
template <typename T>
inline dlm::uint64 Image<T>::Height() const
{
  return height_;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
template <typename T>
inline dlm::uint64 Image<T>::ArraySize() const
{
  return width_ * height_;
}

//------------------------------------------------------------------------------
template <typename T>
inline void Image<T>::Flip()
{
  for (dlm::uint64 i = 0; i < width_; ++i)
  {
    for (dlm::uint64 j = 0; j < height_ / 2; ++j)
    {
      std::swap(operator()(i, j), operator()(i, height_ - 1 - j));
    }
  }
}

//------------------------------------------------------------------------------
template <typename T>
inline T* Image<T>::Data() const
{
  return elements_.get();
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
