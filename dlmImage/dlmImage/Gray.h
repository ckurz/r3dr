//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmImage library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmImage_Gray_h_
#define dlmImage_Gray_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/math/Vector.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmImage
{

//==============================================================================
// Gray class
//------------------------------------------------------------------------------
template <typename T>
class Gray : public dlm::Vector<1, T>
{
public:
  //============================================================================
  // Nested
  //----------------------------------------------------------------------------
  typedef T                 BaseType;
  typedef dlm::Vector<1, T> BaseClass;

  //============================================================================
  // Constructors
  //----------------------------------------------------------------------------
  Gray(BaseType  const  value = static_cast<BaseType>(0));
  Gray(BaseClass const& vector);

  //============================================================================
  // Operators
  //----------------------------------------------------------------------------
  Gray& operator=(BaseType  const  value );
  Gray& operator=(BaseClass const& vector);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  operator BaseType() const;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
