//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmImage library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmImage_ImageAssistant_h_
#define dlmImage_ImageAssistant_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmImage/Image.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlmImage/Gray.h"
#include "dlmImage/GrayAlpha.h"
#include "dlmImage/RGB.h"
#include "dlmImage/RGBA.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlmImage/MaximumValue.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/DynamicVector.h"

#include <functional>
#include <type_traits>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmImage
{

//==============================================================================
// ImageAssistant class
//------------------------------------------------------------------------------
class ImageAssistant
{
public:
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  // Base type conversion.
  //----------------------------------------------------------------------------
  template <typename T, template <class> class U, typename V>
  static Image<U<T> > Convert(Image<U<V> > const& image);

  //----------------------------------------------------------------------------
  // Add alpha channel.
  //----------------------------------------------------------------------------
  template <typename T>
  static Image<GrayAlpha<T> > AddAlpha(
    Image<Gray<T> > const& image, T const value = MaximumValue<T>());
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <typename T>
  static Image<RGBA<T> > AddAlpha(
    Image<RGB<T> > const& image, T const value = MaximumValue<T>());

  //----------------------------------------------------------------------------
  // Remove alpha channel.
  //----------------------------------------------------------------------------
  template <typename T>
  static Image<Gray<T> > RemoveAlpha(Image<GrayAlpha<T> > const& image);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <typename T>
  static Image<RGB<T> > RemoveAlpha(Image<RGBA<T> > const& image);

  //----------------------------------------------------------------------------
  // Gray scale to color conversion.
  //----------------------------------------------------------------------------
  template <typename T>
  static Image<RGB<T> > ToRGB(Image<Gray<T> > const& image);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <typename T>
  static Image<RGBA<T> > ToRGBA(Image<GrayAlpha<T> > const& image);

  //----------------------------------------------------------------------------
  // Color to gray scale conversion.
  //----------------------------------------------------------------------------
  template <typename T>
  static Image<Gray<T> > ToGray(
    Image<RGB<T> > const& image,
    dlm::Vector3f const& weights = dlm::Vector3f(0.2126, 0.7152, 0.0722));
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <typename T>
  static Image<GrayAlpha<T> > ToGrayAlpha(
    Image<RGBA<T> > const& image,
    dlm::Vector3f const& weights = dlm::Vector3f(0.2126, 0.7152, 0.0722));

  //----------------------------------------------------------------------------
  // Resize.
  //----------------------------------------------------------------------------
  template <typename T>
  static T InterpolateBilinear(
    Image<T> const& image, dlm::Vector2f const& position);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <typename T>
  static Image<T> ResizeNearest(
    Image<T> const& image, dlm::uint64 const width, dlm::uint64 const height);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <typename T>
  static Image<T> ResizeBilinear(
    Image<T> const& image, dlm::Vector2u const& size);

  //----------------------------------------------------------------------------
  // Decimate.
  //----------------------------------------------------------------------------
  template <typename T, template <class> class U>
  static Image<U<T> > Decimate(Image<U<T> > const& image);

  //----------------------------------------------------------------------------
  // Kernels.
  //----------------------------------------------------------------------------
  template <typename T>
  static dlm::DynamicVector<T> GaussianKernel(
    T const sigma, T const cutoffValue = static_cast<T>(0.01));
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <typename T>
  static dlm::DynamicVector<T> GaussianDerivativeKernel(
    T const sigma, T const cutoffValue = static_cast<T>(0.01));

};

//==============================================================================
// Shorthand
//------------------------------------------------------------------------------
typedef ImageAssistant IA;

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// Inline code
//------------------------------------------------------------------------------
// This file is likely to be included from source files only, and for
// convenience includes the corresponding inline file automatically.
//------------------------------------------------------------------------------
#include "dlmImage/ImageAssistant.inline.h"

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
