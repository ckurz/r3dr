//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmImage library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmImage_ColorAssistant_h_
#define dlmImage_ColorAssistant_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmImage/Gray.h"
#include "dlmImage/GrayAlpha.h"
#include "dlmImage/RGB.h"
#include "dlmImage/RGBA.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlmImage/MaximumValue.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Types.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmImage
{

//==============================================================================
// ColorAssistant class
//------------------------------------------------------------------------------
class ColorAssistant
{
public:
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  // Base type conversion.
  //----------------------------------------------------------------------------
  template <typename T, template <class> class U, typename V>
  static U<T> Convert(U<V> const& color);

  //----------------------------------------------------------------------------
  // Add alpha channel.
  //----------------------------------------------------------------------------
  template <typename T>
  static GrayAlpha<T> AddAlpha(
    Gray<T> const& color, T const value = MaximumValue<T>());
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <typename T>
  static RGBA<T> AddAlpha(
    RGB<T> const& color, T const value = MaximumValue<T>());

  //----------------------------------------------------------------------------
  // Remove alpha channel.
  //----------------------------------------------------------------------------
  template <typename T>
  static Gray<T> RemoveAlpha(GrayAlpha<T> const& color);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <typename T>
  static RGB<T> RemoveAlpha(RGBA<T> const& color);

  //----------------------------------------------------------------------------
  // Gray scale to color conversion.
  //----------------------------------------------------------------------------
  template <typename T>
  static RGB<T> ToRGB(Gray<T> const& value);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <typename T>
  static RGBA<T> ToRGBA(GrayAlpha<T> const& value);

  //----------------------------------------------------------------------------
  // Color to gray scale conversion.
  //----------------------------------------------------------------------------
  template <typename T>
  static Gray<T> ToGray(
    RGB<T> const& color,
    dlm::Vector3f const& weights = dlm::Vector3f(0.2126, 0.7152, 0.0722));
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <typename T>
  static GrayAlpha<T> ToGrayAlpha(
    RGBA<T> const& color,
    dlm::Vector3f const& weights = dlm::Vector3f(0.2126, 0.7152, 0.0722));
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// Inline code
//------------------------------------------------------------------------------
// This file is likely to be included from source files only, and for
// convenience includes the corresponding inline file automatically.
//------------------------------------------------------------------------------
#include "dlmImage/ColorAssistant.inline.h"

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
