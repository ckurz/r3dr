//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmImage library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmImage_GrayAlpha_h_
#define dlmImage_GrayAlpha_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/math/Vector.h"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlmImage/MaximumValue.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmImage
{

//==============================================================================
// RGBA class
//------------------------------------------------------------------------------
template <typename T>
class GrayAlpha : public dlm::Vector<2, T>
{
public:
  //============================================================================
  // Nested
  //----------------------------------------------------------------------------
  typedef T BaseType;
  typedef dlm::Vector<2, T> BaseClass;

  //============================================================================
  // Constructors
  //----------------------------------------------------------------------------
  GrayAlpha(T const gray  = static_cast<T>(0),
            T const alpha = MaximumValue<T>());
  GrayAlpha(BaseClass const& vector);

  //============================================================================
  // Operators
  //----------------------------------------------------------------------------
  GrayAlpha& operator=(BaseClass const& vector);

  //============================================================================
  // Member functions: Accessors
  //----------------------------------------------------------------------------
  T Gray () const;
  T Alpha() const;

  //============================================================================
  // Member functions: Mutators
  //----------------------------------------------------------------------------
  T& Gray ();
  T& Alpha();
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
