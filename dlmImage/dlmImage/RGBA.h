//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmImage library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmImage_RGBA_h_
#define dlmImage_RGBA_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmImage/MaximumValue.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmImage
{

//==============================================================================
// RGBA class
//------------------------------------------------------------------------------
template <typename T>
class RGBA : public dlm::Vector<4, T>
{
public:
  //============================================================================
  // Nested
  //----------------------------------------------------------------------------
  typedef T BaseType;
  typedef dlm::Vector<4, T> BaseClass;

  //============================================================================
  // Constructors
  //----------------------------------------------------------------------------
  RGBA(T const r = static_cast<T>(0),
       T const g = static_cast<T>(0),
       T const b = static_cast<T>(0),
       T const a = MaximumValue<T>());
  RGBA(BaseClass const& vector);

  //============================================================================
  // Operators
  //----------------------------------------------------------------------------
  RGBA& operator=(BaseClass const& vector);

  //============================================================================
  // Member functions: Accessors
  //----------------------------------------------------------------------------
  T R() const;
  T G() const;
  T B() const;
  T A() const;

  //============================================================================
  // Member functions: Mutators
  //----------------------------------------------------------------------------
  T& R();
  T& G();
  T& B();
  T& A();
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
