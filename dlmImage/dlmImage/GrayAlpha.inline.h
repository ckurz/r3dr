//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmImage library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmImage_GrayAlpha_inline_h_
#define dlmImage_GrayAlpha_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmImage/GrayAlpha.h"

//------------------------------------------------------------------------------
#include "dlm/math/Vector.inline.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmImage
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
template <typename T>
inline GrayAlpha<T>::GrayAlpha(T const gray, T const alpha)
  : BaseClass(gray, alpha)
{}

//------------------------------------------------------------------------------
template <typename T>
inline GrayAlpha<T>::GrayAlpha(BaseClass const& vector)
  : BaseClass(vector)
{}

//==============================================================================
// Operators
//------------------------------------------------------------------------------
template <typename T>
inline GrayAlpha<T>& GrayAlpha<T>::operator=(BaseClass const& vector)
{
  BaseClass::operator=(vector);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//==============================================================================
// Member functions: Accessors
//------------------------------------------------------------------------------
template <typename T>
inline T GrayAlpha<T>::Gray() const
{
  return BaseClass::X();
}

//------------------------------------------------------------------------------
template <typename T>
inline T GrayAlpha<T>::Alpha() const
{
  return BaseClass::Y();
}

//==============================================================================
// Member functions: Mutators
//------------------------------------------------------------------------------
template <typename T>
inline T& GrayAlpha<T>::Gray()
{
  return BaseClass::X();
}

//------------------------------------------------------------------------------
template <typename T>
inline T& GrayAlpha<T>::Alpha()
{
  return BaseClass::Y();
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
