//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmImage library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmImage_RGB_inline_h_
#define dlmImage_RGB_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmImage/RGB.h"

//------------------------------------------------------------------------------
#include "dlm/math/Vector.inline.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmImage
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
template <typename T>
inline RGB<T>::RGB(T const r, T const g, T const b)
  : BaseClass(r, g, b)
{}

//------------------------------------------------------------------------------
template <typename T>
inline RGB<T>::RGB(BaseClass const& vector)
  : BaseClass(vector)
{}

//==============================================================================
// Operators
//------------------------------------------------------------------------------
template <typename T>
inline RGB<T>& RGB<T>::operator=(BaseClass const& vector)
{
  BaseClass::operator=(vector);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//==============================================================================
// Member functions: Accessors
//------------------------------------------------------------------------------
template <typename T>
inline T RGB<T>::R() const
{
  return BaseClass::X();
}

//------------------------------------------------------------------------------
template <typename T>
inline T RGB<T>::G() const
{
  return BaseClass::Y();
}

//------------------------------------------------------------------------------
template <typename T>
inline T RGB<T>::B() const
{
  return BaseClass::Z();
}

//==============================================================================
// Member functions: Mutators
//------------------------------------------------------------------------------
template <typename T>
inline T& RGB<T>::R()
{
  return BaseClass::X();
}

//------------------------------------------------------------------------------
template <typename T>
inline T& RGB<T>::G()
{
  return BaseClass::Y();
}

//------------------------------------------------------------------------------
template <typename T>
inline T& RGB<T>::B()
{
  return BaseClass::Z();
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
