//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmImage library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmImage_ImageAssistant_inline_h_
#define dlmImage_ImageAssistant_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmImage/ImageAssistant.h"

//------------------------------------------------------------------------------
#include "dlmImage/Image.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlmImage/ColorAssistant.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/VectorFunctions.h"
#include "dlm/math/DynamicVectorFunctions.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <iostream>
#include <vector>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmImage
{

//==============================================================================
// Implementation namespace
//------------------------------------------------------------------------------
namespace Implementation
{

//==============================================================================
// Local functions
//------------------------------------------------------------------------------
template <typename T>
inline T Gauss(T const x, T const sigma)
{
  return std::exp(-(x * x / (static_cast<T>(2.0) * sigma * sigma)));
}
  
//==============================================================================
// Implementation namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
template <typename T, template<class> class U, typename V>
inline Image<U<T> > ImageAssistant::Convert(Image<U<V> > const& image)
{
  Image<U<T> > result(image.Width(), image.Height());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (dlm::uint64 i = 0; i < image.Width(); ++i)
  {
    for (dlm::uint64 j = 0; j < image.Height(); ++j)
    {
      result(i, j) = ColorAssistant::Convert<T>(image(i, j));
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}
  
//------------------------------------------------------------------------------
template <typename T>
inline Image<GrayAlpha<T> > ImageAssistant::AddAlpha(
  Image<Gray<T> > const& image, T const value)
{
  Image<GrayAlpha<T> > result(image.Width(), image.Height());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (dlm::uint64 i = 0; i < image.Width(); ++i)
  {
    for (dlm::uint64 j = 0; j < image.Height(); ++j)
    {
      result(i, j) = ColorAssistant::AddAlpha(image(i, j), value);
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <typename T>
inline Image<RGBA<T> > ImageAssistant::AddAlpha(
  Image<RGB<T> > const& image, T const value)
{
  Image<RGBA<T> > result(image.Width(), image.Height());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (dlm::uint64 i = 0; i < image.Width(); ++i)
  {
    for (dlm::uint64 j = 0; j < image.Height(); ++j)
    {
      result(i, j) = ColorAssistant::AddAlpha<T>(image(i, j), value);
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}
  
//------------------------------------------------------------------------------
template <typename T>
inline Image<Gray<T> > ImageAssistant::RemoveAlpha(
  Image<GrayAlpha<T> > const& image)
{
  Image<Gray<T> > result(image.Width(), image.Height());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (dlm::uint64 i = 0; i < image.Width(); ++i)
  {
    for (dlm::uint64 j = 0; j < image.Height(); ++j)
    {
      result(i, j) = ColorAssistant::RemoveAlpha<T>(image(i, j));
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <typename T>
inline Image<RGB<T> > ImageAssistant::RemoveAlpha(Image<RGBA<T> > const& image)
{
  Image<RGB<T> > result(image.Width(), image.Height());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (dlm::uint64 i = 0; i < image.Width(); ++i)
  {
    for (dlm::uint64 j = 0; j < image.Height(); ++j)
    {
      result(i, j) = ColorAssistant::RemoveAlpha(image(i, j));
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <typename T>
inline Image<RGB<T> > ImageAssistant::ToRGB(Image<Gray<T> > const& image)
{
  Image<RGB<T> > result(image.Width(), image.Height());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (dlm::uint64 i = 0; i < image.Width(); ++i)
  {
    for (dlm::uint64 j = 0; j < image.Height(); ++j)
    {
      result(i, j) = ColorAssistant::ToRGB(image(i, j));
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <typename T>
inline Image<RGBA<T> > ImageAssistant::ToRGBA(Image<GrayAlpha<T> > const& image)
{
  Image<RGBA<T> > result(image.Width(), image.Height());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (dlm::uint64 i = 0; i < image.Width(); ++i)
  {
    for (dlm::uint64 j = 0; j < image.Height(); ++j)
    {
      result(i, j) = ColorAssistant::ToRGBA(image(i, j));
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <typename T>
inline Image<Gray<T> > ImageAssistant::ToGray(
  Image<RGB<T> > const& image, dlm::Vector3f const& weights)
{
  Image<Gray<T> > result(image.Width(), image.Height());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (dlm::uint64 i = 0; i < image.Width(); ++i)
  {
    for (dlm::uint64 j = 0; j < image.Height(); ++j)
    {
      result(i, j) = ColorAssistant::ToGray(image(i, j), weights);
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <typename T>
inline Image<GrayAlpha<T> > ImageAssistant::ToGrayAlpha(
  Image<RGBA<T> > const& image, dlm::Vector3f const& weights)
{
  Image<GrayAlpha<T> > result(image.Width(), image.Height());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (dlm::uint64 i = 0; i < image.Width(); ++i)
  {
    for (dlm::uint64 j = 0; j < image.Height(); ++j)
    {
      result(i, j) = ColorAssistant::ToGrayAlpha(image(i, j), weights);
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <typename T>
inline Image<T> ImageAssistant::ResizeNearest(
  Image<T> const& image, dlm::uint64 const width, dlm::uint64 const height)
{
  using dlm::uint64;
  using dlm::FloatType;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Image<T> result(width, height);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // That's what's happening here: A value of 0.5 is added to the indices to get
  // the location of the pixel center (which is not located at integer values
  // but at half offsets). The resulting value is divided by the size of the
  // target image and multiplied by the size of the original image. Finally,
  // the value is truncated to integer. The truncation ensures that the correct
  // pixel is selected (since the center of the nearest pixel is located at a
  // half offset).
  FloatType const scaleWidth  = static_cast<FloatType>(image.Width ()) / width;
  FloatType const scaleHeight = static_cast<FloatType>(image.Height()) / height;

  FloatType const offsetWidth  = scaleWidth  * static_cast<FloatType>(0.5);
  FloatType const offsetHeight = scaleHeight * static_cast<FloatType>(0.5);

  for (uint64 i = 0; i < width; ++i)
  {
    for (uint64 j = 0; j < height; ++j)
    {
      uint64 const x = static_cast<uint64>(i * scaleWidth  + offsetWidth );
      uint64 const y = static_cast<uint64>(j * scaleHeight + offsetHeight);
      
      result(i, j) = image(x, y);
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <typename T, template <class> class U>
inline Image<U<T> > ImageAssistant::Decimate(Image<U<T> > const& image)
{
  using dlm::uint64;
  using dlm::FloatType;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DLM_ASSERT(image.Width () % 2 == 0, DLM_RUNTIME_ERROR);
  DLM_ASSERT(image.Height() % 2 == 0, DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Image<U<T> > result(image.Width() / 2, image.Height() / 2);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < result.Width(); ++i)
  {
    uint64 const iBase = 2 * i;

    for (uint64 j = 0; j < result.Height(); ++j)
    {
      uint64 const jBase = 2 * j;

      // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      // Decimation (by a factor of 2) is achieved by average over 4 pixels of
      // the input image for every pixel of the output image.
      typename U<FloatType>::BaseClass sum;

      sum += dlm::VF::Convert<FloatType>(image(iBase,     jBase    ));
      sum += dlm::VF::Convert<FloatType>(image(iBase,     jBase + 1));
      sum += dlm::VF::Convert<FloatType>(image(iBase + 1, jBase    ));
      sum += dlm::VF::Convert<FloatType>(image(iBase + 1, jBase + 1));

      sum /= static_cast<FloatType>(4.0);

      // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      result(i, j) = dlm::VF::Convert<T>(sum);
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <typename T>
inline dlm::DynamicVector<T> ImageAssistant::GaussianKernel(
  T const sigma, T const cutoffValue)
{
  using dlm::uint64;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  typedef typename std::vector<T>::size_type SizeType;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Determine the maximum size of the vector that will contain the values for
  // the kernel. The width of the kernel has to be an odd number (so that the
  // kernel doesn't implicitely shift the image). The values are related via
  // [kernel width] = 2 * [maximum vector size] - 1.
  SizeType const maximumSize = static_cast<SizeType>(64 + 1);
  // The '+ 1' takes into account that we complain if it is ever reached.

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  std::vector<T> values;

  for (SizeType i = 0; i < maximumSize; ++i)
  {
    T const value = Implementation::Gauss(static_cast<T>(i), sigma);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    if (value < cutoffValue) { break; }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    values.push_back(value);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DLM_ASSERT(values.size() > 0,           DLM_RUNTIME_ERROR);
  DLM_ASSERT(values.size() < maximumSize, DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Create the actual kernel from the calculated values.
  uint64 const kernelWidth = 2 * static_cast<uint64>(values.size()) - 1;

  dlm::DynamicVector<T> kernel(kernelWidth);

  SizeType const center = values.size() - 1;
  
  for (SizeType i = 0; i < values.size(); ++i)
  {
    T const value = values.at(i);

    // The center value is assigned twice, but that doesn't hurt.
    kernel.At(static_cast<uint64>(center - i)) = value;
    kernel.At(static_cast<uint64>(center + i)) = value;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Normalize the kernel so that the values sum up to 1 (like what is achieved
  // by the normalizing factor in the normal distribution).
  T sum = static_cast<T>(0.0);
    
  for (uint64 i = 0; i < kernelWidth; ++i)
  {
    sum += kernel.At(i);
  }

  for (uint64 i = 0; i < kernelWidth; ++i)
  {
    kernel.At(i) /= sum; // HACK! TODO!
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return kernel;
}

//------------------------------------------------------------------------------
template <typename T>
inline dlm::DynamicVector<T> ImageAssistant::GaussianDerivativeKernel(
  T const sigma, T const cutoffValue)
{
  using dlm::uint64;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  typedef typename std::vector<T>::size_type SizeType;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Determine the maximum size of the vector that will contain the values for
  // the kernel. The width of the kernel has to be an odd number (so that the
  // kernel doesn't implicitely shift the image). The values are related via
  // [kernel width] = 2 * [maximum vector size] - 1.
  SizeType const maximumSize = static_cast<SizeType>(64 + 1);
  // The '+ 1' takes into account that we complain if it is ever reached.

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T const maximumValue = sigma * std::exp(static_cast<T>(-0.5));

  std::cout << "maximumValue: " << maximumValue << std::endl;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  std::vector<T> values;

  values.push_back(static_cast<T>(0.0));

  for (SizeType i = 1; i < maximumSize; ++i)
  {
    T const gauss = Implementation::Gauss(static_cast<T>(i), sigma);
    T const value = -static_cast<T>(i) * gauss;

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    if (std::abs(value / maximumValue) < cutoffValue) { break; }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    values.push_back(value);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DLM_ASSERT(values.size() > 0,           DLM_RUNTIME_ERROR);
  DLM_ASSERT(values.size() < maximumSize, DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Create the actual kernel from the calculated values.
  uint64 const kernelWidth = 2 * static_cast<uint64>(values.size()) - 1;

  dlm::DynamicVector<T> kernel(kernelWidth);

  T sum = static_cast<T>(0.0);

  SizeType const center = values.size() - 1;

  for (SizeType i = 0; i < values.size(); ++i)
  {
    T const value = values.at(i);

    // The center value is assigned twice, but that doesn't hurt.
    kernel.At(static_cast<uint64>(center - i)) = -value; // Note the sign.
    kernel.At(static_cast<uint64>(center + i)) =  value;

    sum -= 2 * i * value;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Normalize the kernel.
  for (uint64 i = 0; i < kernelWidth; ++i)
  {
    kernel.At(i) /= sum; // HACK! TODO!
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return kernel;
}

//------------------------------------------------------------------------------
template <dlm::uint32 SIZE, typename T>
dlm::Vector<SIZE, T> Clamp(dlm::Vector<SIZE, T> const& vector, T const upper)
{
  dlm::Vector<SIZE, T> result;

  for (dlm::uint32 i = 0; i < SIZE; ++i)
  {
    T current = vector(i);

    current = (current < static_cast<T>(0.0)) ? static_cast<T>(0.0) : current;
    current = (current > upper              ) ? upper               : current;
  
    result(i) = current;
  }

  return result;
}

//------------------------------------------------------------------------------
template <typename T>
inline T ImageAssistant::InterpolateBilinear(
  Image<T> const& image, dlm::Vector2f const& position)
{
  using dlm::FloatType;
  using dlm::Vector2f;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FloatType const x = position.X() + 0.5;
  FloatType const y = position.Y() + 0.5;

  dlm::uint64 xBase = static_cast<dlm::uint64>(x);
  dlm::uint64 yBase = static_cast<dlm::uint64>(y);

  dlm::uint64 xOffset = 1;
  dlm::uint64 yOffset = 1;

  FloatType const xCenter = xBase + static_cast<FloatType>(0.5);
  FloatType const yCenter = yBase + static_cast<FloatType>(0.5);

  if (x < xCenter)
  {
    if (xBase == 0)
    {
      xOffset = 0;
    }
    else
    {
      xBase -= 1;
    }
  }

  if (y < yCenter)
  {
    if (yBase == 0)
    {
      yOffset = 0;
    }
    else
    {
      yBase -= 1;
    }
  }

  FloatType const xRatio = x - xBase;
  FloatType const yRatio = y - yBase;

  FloatType const xRatioInv = static_cast<FloatType>(1.0) - xRatio;
  FloatType const yRatioInv = static_cast<FloatType>(1.0) - yRatio;

  if (xBase == 0) { xOffset = 0; }
  if (yBase == 0) { yOffset = 0; }

  if (xBase >= image.Width() || yBase >= image.Height())
  {
    return T();
  }

  auto const c00 = dlm::VF::Convert<FloatType>(image(xBase - xOffset, yBase - yOffset)) * xRatioInv * yRatioInv;
  auto const c01 = dlm::VF::Convert<FloatType>(image(xBase - xOffset, yBase          )) * xRatioInv * yRatio;
  auto const c10 = dlm::VF::Convert<FloatType>(image(xBase,           yBase - yOffset)) * xRatio    * yRatioInv;
  auto const c11 = dlm::VF::Convert<FloatType>(image(xBase,           yBase          )) * xRatio    * yRatio;

  auto value = Clamp(c00 + c01 + c10 + c11,
    static_cast<FloatType>(MaximumValue<typename T::BaseType>()));

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return dlm::VF::Convert<typename T::BaseType>(value);
}

//------------------------------------------------------------------------------
template <typename T>
inline Image<T> ImageAssistant::ResizeBilinear(
  Image<T> const& image, dlm::Vector2u const& size)
{
  using dlm::FloatType;
  using dlm::Vector2f;

  Image<T> result(size.X(), size.Y());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FloatType const xScale = image.Width () / static_cast<FloatType>(size.X());
  FloatType const yScale = image.Height() / static_cast<FloatType>(size.Y());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (dlm::uint64 i = 0; i < size.X(); ++i)
  {
    for (dlm::uint64 j = 0; j < size.Y(); ++j)
    {
      FloatType const x = (i + static_cast<FloatType>(0.5)) * xScale;
      FloatType const y = (j + static_cast<FloatType>(0.5)) * yScale;

      dlm::uint64 xBase = static_cast<dlm::uint64>(x);
      dlm::uint64 yBase = static_cast<dlm::uint64>(y);

      dlm::uint64 xOffset = 1;
      dlm::uint64 yOffset = 1;

      FloatType const xCenter = xBase + static_cast<FloatType>(0.5);
      FloatType const yCenter = yBase + static_cast<FloatType>(0.5);

      if (x < xCenter)
      {
        if (xBase == 0)
        {
          xOffset = 0;
        }
        else
        {
          xBase -= 1;
        }
      }

      if (y < yCenter)
      {
        if (yBase == 0)
        {
          yOffset = 0;
        }
        else
        {
          yBase -= 1;
        }
      }

      FloatType const xRatio = x - xBase;
      FloatType const yRatio = y - yBase;

      FloatType const xRatioInv = static_cast<FloatType>(1.0) - xRatio;
      FloatType const yRatioInv = static_cast<FloatType>(1.0) - yRatio;


      //std::cout << "(" << xBase << "," << yBase << ") of (" << image.Width() << "," << image.Height() << ")" << std::endl;

      if (xBase == 0) { xOffset = 0; }
      if (yBase == 0) { yOffset = 0; }

      Vector2f const c00 = dlm::VF::Convert<FloatType>(image(xBase - xOffset, yBase - yOffset)) * xRatioInv * yRatioInv;
      Vector2f const c01 = dlm::VF::Convert<FloatType>(image(xBase - xOffset, yBase          )) * xRatioInv * yRatio;
      Vector2f const c10 = dlm::VF::Convert<FloatType>(image(xBase,           yBase - yOffset)) * xRatio    * yRatioInv;
      Vector2f const c11 = dlm::VF::Convert<FloatType>(image(xBase,           yBase          )) * xRatio    * yRatio;

//      result(i, j) = dlm::VF::Convert<T::BaseType>(c00 + c01 + c10 + c11);
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
