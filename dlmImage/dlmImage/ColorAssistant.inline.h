//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmImage library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmImage_ColorAssistant_inline_h_
#define dlmImage_ColorAssistant_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmImage/ColorAssistant.h"

//------------------------------------------------------------------------------
#include "dlmImage/Gray.inline.h"
#include "dlmImage/GrayAlpha.inline.h"
#include "dlmImage/RGB.inline.h"
#include "dlmImage/RGBA.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/VectorFunctions.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmImage
{

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
template <typename T, template<class> class U, typename V>
inline U<T> ColorAssistant::Convert(U<V> const& color)
{
  using dlm::FloatType;
  using dlm::VF;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  U<FloatType> intermediate = VF::Convert<FloatType>(color);

  intermediate /= static_cast<FloatType>(MaximumValue<V>());
  intermediate *= static_cast<FloatType>(MaximumValue<T>());

//  intermediate = std::min<FloatType>(intermediate, MaximumValue<T>());
//  intermediate = std::max<FloatType>(intermediate, static_cast<FloatType>(0.0));

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return VF::Convert<T>(intermediate);
}

//------------------------------------------------------------------------------
template <typename T>
inline GrayAlpha<T> ColorAssistant::AddAlpha(
  Gray<T> const& color, T const value)
{
  return GrayAlpha<T>(color, value);
}

//------------------------------------------------------------------------------
template <typename T>
inline RGBA<T> ColorAssistant::AddAlpha(RGB<T> const& color, T const value)
{
  return RGBA<T>(color.R(), color.G(), color.B(), value);
}
  
//------------------------------------------------------------------------------
template <typename T>
inline Gray<T> ColorAssistant::RemoveAlpha(GrayAlpha<T> const& color)
{
  return Gray<T>(color.Gray());
}

//------------------------------------------------------------------------------
template <typename T>
inline RGB<T> ColorAssistant::RemoveAlpha(RGBA<T> const& color)
{
  return RGB<T>(color.R(), color.G(), color.B());
}

//------------------------------------------------------------------------------
template <typename T>
inline RGB<T> ColorAssistant::ToRGB(Gray<T> const& value)
{
  return RGB<T>(value, value, value);
}

//------------------------------------------------------------------------------
template <typename T>
inline RGBA<T> ColorAssistant::ToRGBA(GrayAlpha<T> const& value)
{
  T const gray = value.Gray();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return dlm::Vector<4, T>(gray, gray, gray, value.Alpha());
}

//------------------------------------------------------------------------------
template <typename T>
inline Gray<T> ColorAssistant::ToGray(
  RGB<T> const& color, dlm::Vector3f const& weights)
{
  return static_cast<T>(dlm::VF::Convert<dlm::FloatType>(color).Dot(weights));
}

//------------------------------------------------------------------------------
template <typename T>
inline GrayAlpha<T> ColorAssistant::ToGrayAlpha(
  RGBA<T> const& color, dlm::Vector3f const& weights)
{
  dlm::Vector<3, T> const rgb(color.R(), color.G(), color.B()); 

  dlm::Vector3f intermediary = dlm::VF::Convert<dlm::FloatType>(rgb);
  intermediary /= static_cast<dlm::FloatType>(MaximumValue<T>());

  dlm::FloatType grayValue = intermediary.Dot(weights);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  GrayAlpha<T> result;

  result.Gray() = static_cast<T>(grayValue * MaximumValue<T>());
  
  result.Alpha() = color.A();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
