//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmImage library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmImage_dlmImage_h_
#define dlmImage_dlmImage_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlmImage/Gray.h"
#include "dlmImage/Gray.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlmImage/GrayAlpha.h"
#include "dlmImage/GrayAlpha.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlmImage/RGB.h"
#include "dlmImage/RGB.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlmImage/RGBA.h"
#include "dlmImage/RGBA.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlmImage/ColorAssistant.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlmImage/Image.h"
#include "dlmImage/Image.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlmImage/ImageAssistant.h" 
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Types.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmImage
{

//==============================================================================
// Type definitions
//------------------------------------------------------------------------------
// Color / gray value types
//------------------------------------------------------------------------------
typedef Gray<dlm::uint8  > Gray8U;
typedef Gray<dlm::uint16 > Gray16U;
typedef Gray<dlm::float32> Gray32F;
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
typedef GrayAlpha<dlm::uint8  > GrayAlpha8U;
typedef GrayAlpha<dlm::uint16 > GrayAlpha16U;
typedef GrayAlpha<dlm::float32> GrayAlpha32F;
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
typedef RGB<dlm::uint8  > RGB8U;
typedef RGB<dlm::uint16 > RGB16U;
typedef RGB<dlm::float32> RGB32F;
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
typedef RGBA<dlm::uint8  > RGBA8U;
typedef RGBA<dlm::uint16 > RGBA16U;
typedef RGBA<dlm::float32> RGBA32F;

//------------------------------------------------------------------------------
// Image types
//------------------------------------------------------------------------------
typedef Image<Gray8U > ImageGray8U;
typedef Image<Gray16U> ImageGray16U;
typedef Image<Gray32F> ImageGray32F;
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
typedef Image<GrayAlpha8U > ImageGrayAlpha8U;
typedef Image<GrayAlpha16U> ImageGrayAlpha16U;
typedef Image<GrayAlpha32F> ImageGrayAlpha32F;
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
typedef Image<RGB8U > ImageRGB8U;
typedef Image<RGB16U> ImageRGB16U;
typedef Image<RGB32F> ImageRGB32F;
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
typedef Image<RGBA8U > ImageRGBA8U;
typedef Image<RGBA16U> ImageRGBA16U;
typedef Image<RGBA32F> ImageRGBA32F;

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
