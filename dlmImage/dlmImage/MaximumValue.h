//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlmImage library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlmImage_MaximumValue_h_
#define dlmImage_MaximumValue_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/core/Types.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlmImage
{

//==============================================================================
// Implementation namespace
//------------------------------------------------------------------------------
//namespace Implementation
//{

// Note: There's a compiler bug in VS2012 that prevents the template argument
// deduction if the function is enclosed in a different namespace.

//==============================================================================
// Local functions
//------------------------------------------------------------------------------
template <typename T>
inline T MaximumValue();

//------------------------------------------------------------------------------
template <>
inline dlm::uint8 MaximumValue<dlm::uint8>()
{
  return 0xFF; // 255
}

//------------------------------------------------------------------------------
template <>
inline dlm::uint16 MaximumValue<dlm::uint16>()
{
  return 0xFFFF; // 65535
}

//------------------------------------------------------------------------------
template <>
inline dlm::float32 MaximumValue<dlm::float32>()
{
  return 1.0f;
}

//------------------------------------------------------------------------------
template <>
inline dlm::float64 MaximumValue<dlm::float64>()
{
  return 1.0;
}

//==============================================================================
// Implementation namespace
//------------------------------------------------------------------------------
//}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
