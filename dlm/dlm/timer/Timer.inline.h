//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_timer_Timer_inline_h_
#define dlm_timer_Timer_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/timer/Timer.h"

//------------------------------------------------------------------------------
#include <chrono>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
inline Timer::Timer()
  : start_   (0)
  , time_    (0)
  , previous_(0)
{
  Reset();
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
inline void Timer::Tick()
{
  previous_ = time_;
  time_     = SystemTime();
}

//------------------------------------------------------------------------------
inline uint64 Timer::Time() const
{
  return time_ - start_;
}

//------------------------------------------------------------------------------
inline uint64 Timer::Delta() const
{
  return time_ - previous_;
}

//------------------------------------------------------------------------------
inline uint64 Timer::SystemTime() const
{
  using namespace std::chrono;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  auto const currentTime = high_resolution_clock::now().time_since_epoch();
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return static_cast<uint64>(duration_cast<milliseconds>(currentTime).count());
}

//------------------------------------------------------------------------------
inline void Timer::Reset()
{
  previous_ = time_ = start_ = SystemTime();
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
