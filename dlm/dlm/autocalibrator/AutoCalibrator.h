//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/math/Matrix.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <utility>

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_autocalibrator_AutoCalibrator_h_
#define dlm_autocalibrator_AutoCalibrator_h_

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Forward declarations
//------------------------------------------------------------------------------
class Scene;

//==============================================================================
// AutoCalibrator class
//------------------------------------------------------------------------------
class AutoCalibrator
{
public:
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  static std::pair<Matrix4f, Matrix4f> Upgrade(Scene& scene);
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
