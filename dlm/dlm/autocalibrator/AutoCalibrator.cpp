//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/autocalibrator/AutoCalibrator.h"

//------------------------------------------------------------------------------
#include "dlm/scene/Scene.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/svd/SVD.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/VectorFunctions.h"
#include "dlm/math/MatrixFunctions.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/DynamicVectorFunctions.h"
#include "dlm/math/DynamicMatrixFunctions.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/DynamicMatrix.h"
#include "dlm/math/DynamicMatrix.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Matrix.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Quaternion.h"
#include "dlm/math/Quaternion.inline.h"
#include "dlm/math/QuaternionFunctions.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Unnamed namespace
//------------------------------------------------------------------------------
namespace
{

//==============================================================================
// Local type definitions
//------------------------------------------------------------------------------
typedef Vector<10, FloatType> Coefficients;

//==============================================================================
// Local functions
//------------------------------------------------------------------------------
// Given the product a^T.Q*.b, calculate the coefficients for the entries of Q*.
// There are only ten coefficients because Q* is symmetric.
Coefficients Calculate(Vector4d const& a, Vector4d const& b)
{
  Coefficients result;

  result.At(0) = a.X() * b.X();
  result.At(1) = a.Y() * b.X() + a.X() * b.Y();
  result.At(2) = a.Z() * b.X() + a.X() * b.Z();
  result.At(3) = a.W() * b.X() + a.X() * b.W();

  result.At(4) = a.Y() * b.Y();
  result.At(5) = a.Z() * b.Y() + a.Y() * b.Z();
  result.At(6) = a.W() * b.Y() + a.Y() * b.W();

  result.At(7) = a.Z() * b.Z();
  result.At(8) = a.W() * b.Z() + a.Z() * b.W();

  result.At(9) = a.W() * b.W();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}
  
//------------------------------------------------------------------------------
// Given a number of normalized projection matrices, corresponding scale factors
// nu, and a weighting factor for the focal length, calculate the upgrading
// transformation using the absolute dual quadric (ADQ) Q*.
// The equations used for the constraints can be found [3].
// Three or more projection matrices are required. The scale factors should
// initially be set to 1. The weight for the focal length is suggested as 9.
Matrix4d UpgradingTransformation(
  std::vector<Matrix3x4d> const& normalizedProjectionMatrices,
  std::vector<FloatType > const& scaleFactors,
  FloatType               const  fWeight)
{
  uint64 const views = static_cast<uint64>(normalizedProjectionMatrices.size());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Voodoo has a large amount of complicated Maple code to deal with the two
  // view case, which is rarely (i.e., never) used. Since there's also no
  // documentation, less than three views are just not allowed here.
  DLM_ASSERT(views > 2,                    DLM_RUNTIME_ERROR);
  DLM_ASSERT(views == scaleFactors.size(), DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DMatrixd matrix(views * 6, 10);

  for (uint64 i = 0; i < views; ++i)
  {
    Matrix3x4d const& p = normalizedProjectionMatrices.at(i);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // Construct the entries of the dual image of the absolute conic (DIAC) w in
    // terms of coefficients of the ADQ (w = PQ*P^T).
    Coefficients const w00 = Calculate(MF::GetRow(p, 0), MF::GetRow(p, 0));
    Coefficients const w01 = Calculate(MF::GetRow(p, 0), MF::GetRow(p, 1));
    Coefficients const w02 = Calculate(MF::GetRow(p, 0), MF::GetRow(p, 2));

    Coefficients const w11 = Calculate(MF::GetRow(p, 1), MF::GetRow(p, 1));
    Coefficients const w12 = Calculate(MF::GetRow(p, 1), MF::GetRow(p, 2));

    Coefficients const w22 = Calculate(MF::GetRow(p, 2), MF::GetRow(p, 2));

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    FloatType const nu = scaleFactors.at(i);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // Create constraints by requiring expressions to equal 0. The weighting
    // (division) takes the appropriate uncertainties into account.
    Coefficients const c0 = (w00 - w22) / (                     fWeight * nu);
    Coefficients const c1 = (w11 - w22) / (                     fWeight * nu);
    
    Coefficients const c2 = (w00 - w11) / (static_cast<FloatType>(0.2 ) * nu);
    
    Coefficients const c3 =  w02        / (static_cast<FloatType>(0.1 ) * nu);
    Coefficients const c4 =  w12        / (static_cast<FloatType>(0.1 ) * nu);
    
    Coefficients const c5 =  w01        / (static_cast<FloatType>(0.01) * nu);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // Assemble the constraints in a single matrix.
    uint64 const base = i * 6;

    DMF::SetRow(matrix, base + 0, VF::Convert(c0));
    DMF::SetRow(matrix, base + 1, VF::Convert(c1));
    DMF::SetRow(matrix, base + 2, VF::Convert(c2));
    DMF::SetRow(matrix, base + 3, VF::Convert(c3));
    DMF::SetRow(matrix, base + 4, VF::Convert(c4));
    DMF::SetRow(matrix, base + 5, VF::Convert(c5));
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Obtain Q* by SVD of the matrix containing all the constraints.
  Matrix4d qStar;

  {
    SVD::Result const svd = SVD::Decompose(matrix, SVD::Omit, SVD::Full);

    qStar.At(0, 0) =                  svd.V.At(0, 9);
    qStar.At(0, 1) = qStar.At(1, 0) = svd.V.At(1, 9);
    qStar.At(0, 2) = qStar.At(2, 0) = svd.V.At(2, 9);
    qStar.At(0, 3) = qStar.At(3, 0) = svd.V.At(3, 9);

    qStar.At(1, 1) =                  svd.V.At(4, 9);
    qStar.At(1, 2) = qStar.At(2, 1) = svd.V.At(5, 9);
    qStar.At(1, 3) = qStar.At(3, 1) = svd.V.At(6, 9);

    qStar.At(2, 2)                  = svd.V.At(7, 9);
    qStar.At(2, 3) = qStar.At(3, 2) = svd.V.At(8, 9);

    qStar.At(3, 3) =                  svd.V.At(9, 9);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Decompose Q* to get the actual upgrading transformation exploiting the fact
  // that Q* is given as diag(1, 1, 1, 0) for a Euclidean frame.
  Matrix4d h;

  {
    SVD::Result const svd = SVD::Decompose(MF::Convert(qStar), SVD::Full);

    MF::SetCol(h, 0, DVF::Convert<4>(DMF::GetCol(svd.U, 0)) * ::sqrt(svd.Sigma.At(0)));
    MF::SetCol(h, 1, DVF::Convert<4>(DMF::GetCol(svd.U, 1)) * ::sqrt(svd.Sigma.At(1)));
    MF::SetCol(h, 2, DVF::Convert<4>(DMF::GetCol(svd.U, 2)) * ::sqrt(svd.Sigma.At(2)));

    h.At(3, 3) = static_cast<FloatType>(1.0);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return h;
}

//------------------------------------------------------------------------------
// Creates a Givens-rotation matrix. The rotation axis is specified by setting
// axis to the appropriate value (0 for x, 1 for y, 2 for z). The corresponding
// main-diagonal entry of the resulting matrix will contain 1.
Matrix3d GivensRotation(FloatType const a, FloatType const b, uint32 const axis)
{
  DLM_ASSERT(axis < 3, DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FloatType const r = std::hypot(a, b);
  FloatType const c =  a / r; // Cosine of rotation angle.
  FloatType const s = -b / r; //   Sine of rotation angle.

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Matrix3d rotation;

  rotation(axis, axis) = static_cast<FloatType>(1.0);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Set the rotation matrix components with appropriate indices.
  uint32 const i0 = 0 + (axis == 0); // Makes the first  index 1 if axis is 0.
  uint32 const i1 = 2 - (axis == 2); // Makes the second index 1 if axis is 2.

  rotation(i0, i0) =  c;
  rotation(i0, i1) = -s;
  rotation(i1, i0) =  s;
  rotation(i1, i1) =  c;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return rotation;
}

//------------------------------------------------------------------------------
// Perform an RQ decomposition of the given matrix using three consecutive
// Givens-rotations. The first matrix of the result is R, the second is Q.
std::pair<Matrix3d, Matrix3d> RQDecomposition(Matrix3d const& matrix)
{
  std::pair<Matrix3d, Matrix3d> result;

  result.first = matrix;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  {
    FloatType const a = result.first(2, 2);
    FloatType const b = result.first(2, 1);

    Matrix3d rotation = GivensRotation(a, b, 0);

    result.first  *= rotation;
    result.second  = MF::Transpose(rotation);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  {
    FloatType const a = result.first(2, 2);
    FloatType const b = result.first(2, 0);

    Matrix3d rotation = GivensRotation(a, b, 1);

    result.first  *= rotation;
    result.second  = MF::Transpose(rotation) * result.second;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  {
    FloatType const a = result.first(1, 1);
    FloatType const b = result.first(1, 0);

    Matrix3d rotation = GivensRotation(a, b, 2);

    result.first  *= rotation;
    result.second  = MF::Transpose(rotation) * result.second;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
struct Result
{
  Matrix3d K;
  Matrix3d R;
  Vector3d C;
};

//------------------------------------------------------------------------------
// Decompose a projection matrix P into a calibration matrix K, a rotation
// matrix R, and a translation vector C, so that P = K[R|-RC]. To get the actual
// orientation of the camera in the world coordinate frame, R has to be inverted
// or transposed. The calibration matrix K is normalized to have a value of 1 in
// the lower-right corner.
Result Decompose(Matrix3x4d const& projection)
{
  Result result;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Matrix3d const KR = MF::GetSubmatrix<3, 3>(projection, 0, 0);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  std::pair<Matrix3d, Matrix3d> const rq = RQDecomposition(KR);

  result.K = rq.first;
  result.R = rq.second;

  result.K /= result.K.At(2, 2); // Make the lower-left corner value 1.

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  result.C = -MF::Inverse(KR) * MF::GetCol(projection, 3);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
class MathAssistant
{
public:
  template <typename T>
  static T Sqr(T const number)
  {
    return number * number;
  }
};

typedef MathAssistant MA;

//------------------------------------------------------------------------------
// Evaluate the calibration matrix using the cost function from [1], section 4.
FloatType Evaluate(Matrix3d const& calibration)
{
  Matrix3d const normalizedK = calibration / calibration.At(2, 2);

  // Time to get a little paranoid: If the focal lengths have different signs
  // for whatever reason, the computation doesn't work as intended.
  FloatType const k1 = std::fabs(normalizedK(0, 0)); // Focal length, x.
  FloatType const k4 = std::fabs(normalizedK(1, 1)); // Focal length, y.

  // The remaining terms are not used in any summations/subtractions, so the
  // sign is not of relevance.
  FloatType const k2 = normalizedK(0, 1); // Skew.

  FloatType const k3 = normalizedK(0, 2); // Principal point offset, x.
  FloatType const k5 = normalizedK(1, 2); // Principal point offset, y.

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Calculate the actual cost function. The paper [2] disagrees with what is
  // given in [1] by omitting the focal length in y-direction in the denominator
  // and instead introduces a term based on the aspect ratio in the numerator.
  // Voodoo doesn't implement this, however, leaving the numerator unchanged
  // except for a slight difference in the cost for the principal point offset
  // (i.e., (|k3| + |k5|)^2 instead of k3^2 + k5^2), which might be attributable
  // to a mistake, as there is a pair of parentheses in the equation in [1], but
  // for different reasons. The focal length in y-direction is omitted from the
  // denominator for no apparent reason, though.
  // There were a number of parameters expressing the confidence in the
  // underlying assumptions governing the cost function (e.g., skew is 0, etc.),
  // but they were given as 1 in the paper and so are omitted here.
  FloatType const numerator =
    MA::Sqr(k2) + MA::Sqr(k3) + MA::Sqr(k5) + MA::Sqr(k1 - k4);
  
  FloatType const denominator = MA::Sqr(k1 + k4); // As given in [1].
  
  FloatType const r = numerator / denominator; // The actual cost.

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return r;
}

//------------------------------------------------------------------------------
// Evaluate the upgrading transformation using the given normalized projection
// matrices. For evaluation, each projection matrix is multiplied by the
// upgrading transformation from the right, then the upper left 3x3 block which
// contains the calibration matrix and the rotation matrix is extracted and
// decomposed, finally leaving the calibration matrix to be evaluated.
FloatType Evaluate(
  Matrix4d                const& upgradingTransformation,
  std::vector<Matrix3x4d> const& normalizedProjectionMatrices)
{
  FloatType cost = static_cast<FloatType>(0.0);

  for (auto it = normalizedProjectionMatrices.cbegin();
    it != normalizedProjectionMatrices.cend(); ++it)
  {
    Matrix3x4d const upgraded = *it * upgradingTransformation;

    Result const decomposition = Decompose(upgraded);

    cost += Evaluate(decomposition.K);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return cost;
}

//------------------------------------------------------------------------------
// Estimate an upgrading transformation from a number (of at least 3) normalized
// projection matrices following the approach in [2].
Matrix4d RobustLinearAutoCalibration(
  std::vector<Matrix3x4d> normalizedProjectionMatrices)
{
  FloatType const highNumber = static_cast<FloatType>(1e32);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Matrix4d upgrade;

  FloatType bestCost = highNumber;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Use 50 iterations with different weights to determine the best upgrading
  // transformation.
  for (uint32 n = 0; n < 50; ++n)
  {
    // To calculate the weight of each iteration, the paper gives the formula
    // beta = 0.1e^(0.3n) and weights the appropriate equations with 1/beta.
    // Voodoo uses a different formula, 0.01e^((log(9.0/0.1)/15)n). There is no
    // reason given for the change from 0.1 to 0.01, but log(9.0/0.1)/15
    // evaluates to approximately 0.3. Keep in mind that the log given here is
    // actually a natural logarithm.
    // The formula is apparently motivated by the uncertainty for the focal
    // length given in [3], which is 9. It makes the weight start at 0.1 for the
    // first iteration and then grow exponentially. The value 9 is reached at
    // iteration n = 15 (for a factor of 0.1).
    // It appears that the change from 0.1 to 0.01 was intended to give higher
    // initial weights to the focal length constraints at the expense of range
    // in the area where there is no significance to them any more anyway.
    FloatType const current  = static_cast<FloatType>(n);
    FloatType const exponent = static_cast<FloatType>(0.3) * current;
    FloatType const factor   = static_cast<FloatType>(0.01);

    FloatType const beta = factor * std::exp(exponent);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // The original paper [3] mentions that the process can be iterated with
    // different scale factors until the factors converge.
    // Anyway, here's the initial scale factors, all 1.
    std::vector<FloatType> scales;

    scales.resize(
      normalizedProjectionMatrices.size(), static_cast<FloatType>(1.0));

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // Calculate the upgrading transformation from normalized projection
    // matrices using the current weight.
    Matrix4d const t =
      UpgradingTransformation(normalizedProjectionMatrices, scales, beta);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // Decide whether the current transformation is the best so far.
    FloatType const cost = Evaluate(t, normalizedProjectionMatrices);

    if (cost < bestCost)
    {
      upgrade = t;

      bestCost = cost;
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Currently, there's just a crude safeguard to ensure that at least one time
  // a transformation has been stored in upgrade.
  DLM_ASSERT(bestCost < highNumber, DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return upgrade;
}

//==============================================================================
// Unnamed namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
std::pair<Matrix4f, Matrix4f> AutoCalibrator::Upgrade(Scene& scene)
{
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  static FloatType const N2 = static_cast<FloatType>(2.0);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  std::vector<Matrix3x4d> normalized;

  for (uint64 i = 0; i < scene.IRepS().Range(); ++i)
  {
    IRepId const iRepId = IRepId(i);

    if (!scene.IsValid(iRepId)) { continue; }

    IRep const& iRep = scene.Get(iRepId);

    if (!iRep.Reference<PRep>::IsValid()) { continue; }
    
    PRep const& pRep = scene.Get(iRep.Reference<PRep>::Get());

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // Construct the normalization matrix.
    Matrix3d K;

    uint32 const widthPlusHeight = iRep.Size().X() + iRep.Size().Y();

    K(0, 0) = static_cast<FloatType>(widthPlusHeight);
    K(1, 1) = static_cast<FloatType>(widthPlusHeight);

    K(0, 2) = static_cast<FloatType>(iRep.Size().X()) / N2;
    K(1, 2) = static_cast<FloatType>(iRep.Size().Y()) / N2;
      
    K(2, 2) = static_cast<FloatType>(1.0);

    MF::Invert(K);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    normalized.push_back(K * pRep);
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Determine the upgrading transformation.
  Matrix4f const upgrade = RobustLinearAutoCalibration(normalized);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Upgrade the projective cameras by applying the upgrading transformation.
  // Again, this only works for projection matrices connected to an image,
  // because there is insufficient information otherwise.
  for (uint64 i = 0; i < scene.IRepS().Range(); ++i)
  {
    IRepId const iRepId = IRepId(i);

    if (!scene.IsValid(iRepId)) { continue; }

    IRep const& iRep = scene.Get(iRepId);

    if (!iRep.Reference<PRep>::IsValid()) { continue; }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Matrix3x4d projection = scene.Get(iRep.Reference<PRep>::Get());

    projection *= upgrade; // Perform the actual upgrade.

    Result const decomposition = Decompose(projection);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // Get rid of the PRep.
    scene.Delete(iRep.Reference<PRep>::Get());

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // Provide the necessary elements for a metric camera.
    CRepId const cRepId = scene.CreateCRep();
    TSeqId const tSeqId = scene.CreateTSeq();
    TRepId const tRepId = scene.CreateTRep();

    scene.Assign(iRepId, cRepId);
    scene.Assign(iRepId, tSeqId);

    scene.Append(tSeqId, tRepId);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    CRep& cRep = scene.Get(cRepId);

    cRep.FocalLength() = std::fabs(decomposition.K(0, 0));

    cRep.PixelAspectRatio() = std::fabs(decomposition.K(1, 1)) /
                              std::fabs(decomposition.K(0, 0));

    cRep.Skew() = decomposition.K(0, 1);

    cRep.PrincipalPoint().X() = decomposition.K(0, 2);
    cRep.PrincipalPoint().Y() = decomposition.K(1, 2);
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    TRep& tRep = scene.Get(tRepId);

    Quatd const compensation = QF::FromAxisAngle(Vector3f(1.0, 0.0, 0.0), M_PI);

    Quatd const rotation = QF::FromRotation(MF::Transpose(decomposition.R));

    if (decomposition.K(0, 0) < 0.0)
    {
      tRep = Transformation(rotation * compensation, decomposition.C, true);
    }
    else
    {
      tRep = Transformation(rotation, decomposition.C, true);
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Upgrade the 3D points by applying the inverse of the upgrading
  // transformation.
  Matrix4d const upgradeInverse = MF::Inverse(upgrade);

  for (uint64 i = 0; i < scene.ORepS().Range(); ++i)
  {
    ORepId const oRepId = ORepId(i);

    if (!scene.IsValid(oRepId)) { continue; }

    ORep& oRep = scene.Get(oRepId);

    oRep = VF::Inhomogenize(upgradeInverse * VF::Homogenize(Vector3d(oRep)));
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return std::make_pair(upgrade, upgradeInverse);
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// Bibliography
//------------------------------------------------------------------------------
// [1] Calibration with Robust Use of Cheirality by Quasi-Affine Reconstruction
//     of the Set of Camera Projection Centres, Nistér, ICCV 2001.
// [2] Robust Linear Auto-Calibration of a Moving Camera from Image Sequences,
//     Thormählen et al., ACCV 200x
// [3] Visual modeling with a hand-held camera, Pollefeys et al., IJCV 2004.
