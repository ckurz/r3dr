//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/differentiator/ProjectiveDifferentiator.h"

//------------------------------------------------------------------------------
#include <cmath>
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.inline.h"
#include "dlm/math/Matrix.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/VectorFunctions.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/entity/ProjectiveCamera.h"
#include "dlm/entity/ProjectiveCamera.inline.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
ProjectiveDifferentiator::Result ProjectiveDifferentiator::Differentiate(
  ProjectiveCamera const& camera,
  Vector3d         const& point)
{
  Vector3d const pCam = camera * VF::Homogenize(point);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  float64 const dInv    = 1    / pCam.Z();
  float64 const dInvSqr = dInv / pCam.Z();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Result result;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Matrix2x12d& pDeriv = result.pDeriv;

  pDeriv.At(0,  0) = point.X() * dInv;
  pDeriv.At(0,  1) = point.Y() * dInv;
  pDeriv.At(0,  2) = point.Z() * dInv;
  pDeriv.At(0,  3) =             dInv;

  pDeriv.At(1,  4) = point.X() * dInv;
  pDeriv.At(1,  5) = point.Y() * dInv;
  pDeriv.At(1,  6) = point.Z() * dInv;
  pDeriv.At(1,  7) =             dInv;

  pDeriv.At(0,  8) = - (pCam.X() * point.X() * dInvSqr);
  pDeriv.At(0,  9) = - (pCam.X() * point.Y() * dInvSqr);
  pDeriv.At(0, 10) = - (pCam.X() * point.Z() * dInvSqr);
  pDeriv.At(0, 11) = - (pCam.X() *             dInvSqr);

  pDeriv.At(1,  8) = - (pCam.Y() * point.X() * dInvSqr);
  pDeriv.At(1,  9) = - (pCam.Y() * point.Y() * dInvSqr);
  pDeriv.At(1, 10) = - (pCam.Y() * point.Z() * dInvSqr);
  pDeriv.At(1, 11) = - (pCam.Y() *             dInvSqr);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Matrix2x3d& oDeriv = result.oDeriv;

  oDeriv.At(0, 0) = camera.At(0, 0) * dInv - pCam.X() * camera.At(2, 0) * dInvSqr;
  oDeriv.At(1, 0) = camera.At(1, 0) * dInv - pCam.Y() * camera.At(2, 0) * dInvSqr;

  oDeriv.At(0, 1) = camera.At(0, 1) * dInv - pCam.X() * camera.At(2, 1) * dInvSqr;
  oDeriv.At(1, 1) = camera.At(1, 1) * dInv - pCam.Y() * camera.At(2, 1) * dInvSqr;

  oDeriv.At(0, 2) = camera.At(0, 2) * dInv - pCam.X() * camera.At(2, 2) * dInvSqr;
  oDeriv.At(1, 2) = camera.At(1, 2) * dInv - pCam.Y() * camera.At(2, 2) * dInvSqr;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  result.pProj = VF::Inhomogenize(pCam);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm
