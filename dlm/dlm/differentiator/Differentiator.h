//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_differentiator_Differentiator_h_
#define dlm_differentiator_Differentiator_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include <vector>
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.h"
#include "dlm/math/Matrix.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Forward declarations
//------------------------------------------------------------------------------
class Camera;
class Distortion;
class Transformation;

//==============================================================================
// Derivator class
//------------------------------------------------------------------------------
class Differentiator
{
public:
  //============================================================================
  // Nested
  //----------------------------------------------------------------------------
  struct Result
  {
    std::vector<Matrix2x6d> tDeriv;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Matrix2x5d              cDeriv;
    Matrix2x8d              dDeriv;
    Matrix2x3d              oDeriv;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Vector2d                pProj;
  };

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  static Result Differentiate(
    Camera                      const& camera,
    Distortion                  const& distortion,
    std::vector<Transformation> const& transformations,
    Vector3d                    const& point);

private:
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  static Vector2d DifferentiateCamera(
    Camera                      const& camera,
    Vector2d                    const& pDist,
    Result                           & result);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  static Vector2d DifferentiateDistortion(
    Distortion                  const& distortion,
    Vector3d                    const& pCam,
    Result                           & result);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  static Vector3d DifferentiateTransformations(
    std::vector<Transformation> const& transformations,
    Vector3d                    const& point,
    Result                           & result);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  static Vector3d DifferentiatePoint(
    Matrix3x4d                  const& t,
    Vector3d                    const& point,
    Result                           & result);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  static Matrix2x6d DifferentiateInverseTransformation(
    Transformation const& t,
    Vector3d       const& pCam,
    Matrix3x4d     const& superordinate,
    Vector3d       const&   subordinate);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  static Matrix2x6d DifferentiateTransformation(
    Transformation const& t,
    Vector3d       const& pCam,
    Matrix3x4d     const& superordinate,
    Vector3d       const&   subordinate);
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_differentiator_Differentiator_h_
