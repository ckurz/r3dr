//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/differentiator/Differentiator.h"

//------------------------------------------------------------------------------
#include <cmath>
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.inline.h"
#include "dlm/math/Matrix.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Quaternion.h"
#include "dlm/math/Quaternion.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/MatrixFunctions.h"
#include "dlm/math/VectorFunctions.h"
#include "dlm/math/QuaternionFunctions.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/entity/Camera.h"
#include "dlm/entity/Camera.inline.h"
#include "dlm/entity/Distortion.h"
#include "dlm/entity/Distortion.inline.h"
#include "dlm/entity/Transformation.h"
#include "dlm/entity/Transformation.inline.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Unnamed namespace
//------------------------------------------------------------------------------
namespace
{

//==============================================================================
// Local functions
//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline Matrix<2, SIZE, T> Update(
  Matrix<2, SIZE, T> const& matrix,
  FloatType          const  f,
  FloatType          const  s,
  FloatType          const  eta)
{
  Matrix<2, SIZE, T> result;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  auto r0 = MF::GetRow(matrix, 0);
  auto r1 = MF::GetRow(matrix, 1);
    
  MF::SetRow(result, 0, r0 * f + r1 * s      );
  MF::SetRow(result, 1,          r1 * f * eta);  

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//==============================================================================
// Unnamed namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
Differentiator::Result Differentiator::Differentiate(
  Camera                      const& projection,
  Distortion                  const& distortion,
  std::vector<Transformation> const& transformations,
  Vector3d                    const& point)
{
  Result result;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Vector3d const pCam  =
    DifferentiateTransformations(transformations, point, result);
  Vector2d const pDist =
    DifferentiateDistortion     (distortion,      pCam,  result);
  Vector2d const pProj =
    DifferentiateCamera         (projection,      pDist, result);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  result.pProj = pProj;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
Vector2d Differentiator::DifferentiateCamera(
  Camera const& projection, Vector2d const& pDist, Result& result)
{
  //update subordinate derivatives
  float64 const f   = projection.FocalLength();
  float64 const eta = projection.PixelAspectRatio();
  float64 const s   = projection.Skew();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (auto it = result.tDeriv.begin(); it != result.tDeriv.end(); ++it)
  {
    *it = Update(*it, f, s, eta);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  result.oDeriv = Update(result.oDeriv, f, s, eta);
  result.dDeriv = Update(result.dDeriv, f, s, eta);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Calculate the derivatives with respect to the focal length.
  result.cDeriv(0, 0) = pDist.X();
  result.cDeriv(1, 0) = pDist.Y() * eta;

  // Calculate the derivatives with respect to the principal point offset.
  result.cDeriv(0, 1) = static_cast<FloatType>(1.0);
  result.cDeriv(1, 1) = static_cast<FloatType>(0.0);
  result.cDeriv(0, 2) = static_cast<FloatType>(0.0);
  result.cDeriv(1, 2) = static_cast<FloatType>(1.0);
  
  // Calculate the derivatives with respect to the pixel aspect ratio parameter.
  result.cDeriv(0, 3) = static_cast<FloatType>(0.0);
  result.cDeriv(1, 3) = pDist.Y() * f;

  // Calculate the derivatives with respect to the skew parameter.
  result.cDeriv(0, 4) = pDist.Y();
  result.cDeriv(1, 4) = static_cast<FloatType>(0.0);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return projection(pDist);
}

//------------------------------------------------------------------------------
Vector2d Differentiator::DifferentiateDistortion(
  Distortion const& distortion, Vector3d const& pCam, Result& result)
{
  Vector2d pDiv(pCam.X(), pCam.Y());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  pDiv /= pCam.Z(); //perspective division

  Vector6d const& k = distortion.K();
  Vector2d const& p = distortion.P();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //calculate auxiliary values
  float64 const r2 = pDiv.Dot(pDiv);
  float64 const r4 = r2 * r2;
  float64 const r6 = r4 * r2;

  float64 const radialN = 1.0 + k.At(0) * r2 + k.At(1) * r4 + k.At(2) * r6;
  float64 const radialD = 1.0 + k.At(3) * r2 + k.At(4) * r4 + k.At(5) * r6;

  float64 const radial = radialN / radialD;

  float64 const radialD2Inv = 1.0 / (radialD * radialD);

  float64 const r  = sqrt(r2);
  float64 const r3 = r2 * r;
  float64 const r5 = r4 * r;

  float64 const radialNDiffR =
    2.0 * k.At(0) * r  + 4.0 * k.At(1) * r3 + 6.0 * k.At(2) * r5;

  float64 const radialDDiffR =
    2.0 * k.At(3) * r  + 4.0 * k.At(4) * r3 + 6.0 * k.At(5) * r5;

  float64 const rScale =
    (radialNDiffR * radialD - radialDDiffR * radialN) * radialD2Inv;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //update subordinate derivatives

  uint64 const tCount = result.tDeriv.size();

  //transformation derivatives
  for (uint64 i = 0; i < tCount; ++i)
  {
    Matrix2x6d& tDeriv = result.tDeriv.at(i);

    //auxiliary values
    Vector6d const xDeriv = MF::GetRow(tDeriv, 0);
    Vector6d const yDeriv = MF::GetRow(tDeriv, 1);

    Vector6d const rDiff = (xDeriv * pDiv.X() + yDeriv * pDiv.Y()) / r;

    Vector6d xNew = xDeriv * radial;
    Vector6d yNew = yDeriv * radial;
    
    for (uint32 j = 0; j < 6; ++j)
    {
      xNew.At(j) += rDiff.At(j) * rScale * pDiv.X();
      yNew.At(j) += rDiff.At(j) * rScale * pDiv.Y();
    }

    xNew += (xDeriv * pDiv.Y() + yDeriv * pDiv.X()) * 2.0 * p.At(0);
    xNew += (rDiff  * r + xDeriv * pDiv.X() * 2.0 ) * 2.0 * p.At(1);
    yNew += (rDiff  * r + yDeriv * pDiv.Y() * 2.0 ) * 2.0 * p.At(0);
    yNew += (xDeriv * pDiv.Y() + yDeriv * pDiv.X()) * 2.0 * p.At(1);

    //write back
    MF::SetRow(tDeriv, 0, xNew);
    MF::SetRow(tDeriv, 1, yNew);
  }

  //point derivatives
  Vector3d const xDeriv = MF::GetRow(result.oDeriv, 0);
  Vector3d const yDeriv = MF::GetRow(result.oDeriv, 1);

  Vector3d const rDiff = (xDeriv * pDiv.X() + yDeriv * pDiv.Y()) / r;

  Vector3d xNew = xDeriv * radial;
  Vector3d yNew = yDeriv * radial;
    
  for (unsigned i = 0; i < 3; ++i)
  {
    xNew.At(i) += rDiff.At(i) * rScale * pDiv.X();
    yNew.At(i) += rDiff.At(i) * rScale * pDiv.Y();
  }

  xNew += (xDeriv * pDiv.Y() + yDeriv * pDiv.X()) * 2.0 * p.At(0);
  xNew += (rDiff  * r + xDeriv * pDiv.X() * 2.0 ) * 2.0 * p.At(1);
  yNew += (rDiff  * r + yDeriv * pDiv.Y() * 2.0 ) * 2.0 * p.At(0);
  yNew += (xDeriv * pDiv.Y() + yDeriv * pDiv.X()) * 2.0 * p.At(1);

  //write back
  MF::SetRow(result.oDeriv, 0, xNew);
  MF::SetRow(result.oDeriv, 1, yNew);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //calculate derivatives with respect to k
  float64 const factor0 = 1.0 / radialD;

  MF::SetCol(result.dDeriv, 2, pDiv * r2 * factor0);
  MF::SetCol(result.dDeriv, 3, pDiv * r4 * factor0);
  MF::SetCol(result.dDeriv, 4, pDiv * r6 * factor0);

  float64 const factor1 = -factor0 * radial;

  MF::SetCol(result.dDeriv, 5, pDiv * r2 * factor1);
  MF::SetCol(result.dDeriv, 6, pDiv * r4 * factor1);
  MF::SetCol(result.dDeriv, 7, pDiv * r6 * factor1);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //calculate derivatives with respect to p
  result.dDeriv.At(0, 0) = 2.0 * pDiv.X() * pDiv.Y();
  result.dDeriv.At(0, 1) = 2.0 * pDiv.X() * pDiv.X() + r2;
  result.dDeriv.At(1, 0) = 2.0 * pDiv.Y() * pDiv.Y() + r2;
  result.dDeriv.At(1, 1) = 2.0 * pDiv.X() * pDiv.Y();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //distort point
  Vector2d tangential = p * 2.0 * pDiv.X() * pDiv.Y();
  tangential         += Vector2d(p.Y(), p.X()) * r2;
  tangential         += Vector2d(p.Y() * pDiv.X() * pDiv.X(),
                                 p.X() * pDiv.Y() * pDiv.Y()) * 2.0;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return pDiv * radial + tangential;
}

//------------------------------------------------------------------------------
Vector3d Differentiator::DifferentiateTransformations(
  std::vector<Transformation> const& transformations,
  Vector3d                    const& point,
  Result                           & result)
{
  uint64 const tCount = transformations.size();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //calculate cumulative transformation stack
  std::vector<Matrix3x4d> stack;

  stack.reserve(tCount + 1);

  stack.push_back(Matrix3x4d());

  MF::MakeIdentity(stack.back());

  for (uint64 i = 0; i < tCount; ++i)
  {
    stack.push_back(stack.back() * transformations.at(i).Matrix());
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //assemble point derivatives
  Vector3d const pCam  = DifferentiatePoint(stack.back(), point, result);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //assemble transformation derivatives
  std::vector<Matrix2x6d>& tDeriv = result.tDeriv;
  tDeriv.resize(tCount); //ensure that there's enough space

  Vector3d current = point;

  for (uint64 i = 0; i < tCount; ++i)
  {
    //iterate in reverse order to facilitate computation of current
    uint64 const index = tCount - 1 - i;

    Transformation const& t = transformations.at(index);

    if (t.Invert())
    {
      tDeriv.at(index) = DifferentiateInverseTransformation(t, pCam, stack.at(index), current);
    }
    else
    {
      tDeriv.at(index) = DifferentiateTransformation       (t, pCam, stack.at(index), current); 
    }

    current = t * current;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return pCam;
}

//------------------------------------------------------------------------------
Vector3d Differentiator::DifferentiatePoint(
  Matrix3x4d const& t, Vector3d const& point, Result& result)
{
  Vector3d const pCam =  t * VF::Homogenize(point);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  float64 const dInv    = 1    / pCam.Z();
  float64 const dInvSqr = dInv / pCam.Z();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Matrix2x3d& oDeriv = result.oDeriv;

  oDeriv.At(0, 0) = t.At(0, 0) * dInv - pCam.X() * t.At(2, 0) * dInvSqr;
  oDeriv.At(1, 0) = t.At(1, 0) * dInv - pCam.Y() * t.At(2, 0) * dInvSqr;

  oDeriv.At(0, 1) = t.At(0, 1) * dInv - pCam.X() * t.At(2, 1) * dInvSqr;
  oDeriv.At(1, 1) = t.At(1, 1) * dInv - pCam.Y() * t.At(2, 1) * dInvSqr;

  oDeriv.At(0, 2) = t.At(0, 2) * dInv - pCam.X() * t.At(2, 2) * dInvSqr;
  oDeriv.At(1, 2) = t.At(1, 2) * dInv - pCam.Y() * t.At(2, 2) * dInvSqr;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return pCam;
}

//------------------------------------------------------------------------------
Matrix2x6d Differentiator::DifferentiateInverseTransformation(
  Transformation const& t,
  Vector3d       const& pCam,
  Matrix3x4d     const& superordinate,
  Vector3d       const&   subordinate)
{
  Vector3d const rotation    = QF::EulerAnglesYXZr(t.Rotation());
  Vector3d const translation = t.Translation();

  Quatd    const quat = QF::FromEulerAnglesYXZr(rotation).Conjugated();

  Matrix3d const tRes(MF::GetSubmatrix<3, 3>(superordinate, 0, 0) *
    QF::Rotation(quat));

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  float64 const n0 = pCam.X();
  float64 const n1 = pCam.Y();
  float64 const d  = pCam.Z();

  float64 const dInvSqr = 1.0 / (d * d);

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Matrix2x6d result;

  result.At(0, 3) = dInvSqr * (n0 * tRes.At(2, 0) - d * tRes.At(0, 0));
  result.At(1, 3) = dInvSqr * (n1 * tRes.At(2, 0) - d * tRes.At(1, 0));

  result.At(0, 4) = dInvSqr * (n0 * tRes.At(2, 1) - d * tRes.At(0, 1));
  result.At(1, 4) = dInvSqr * (n1 * tRes.At(2, 1) - d * tRes.At(1, 1));

  result.At(0, 5) = dInvSqr * (n0 * tRes.At(2, 2) - d * tRes.At(0, 2));
  result.At(1, 5) = dInvSqr * (n1 * tRes.At(2, 2) - d * tRes.At(1, 2));

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  float64 const xx = subordinate.X() - translation.X();
  float64 const yy = subordinate.Y() - translation.Y();
  float64 const zz = subordinate.Z() - translation.Z();

  float64 const pan  = rotation.X();
  float64 const tilt = rotation.Y();
  float64 const roll = rotation.Z();

  float64 const cp = cos(pan );
  float64 const ct = cos(tilt);
  float64 const cr = cos(roll);

  float64 const sp = sin(pan );
  float64 const st = sin(tilt);
  float64 const sr = sin(roll);

  // d sin / dt =  cos
  // d cos / dt = -sin

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // float64 const  r00   =  cp *  cr + sp * st * sr;
  float64 const    dr00dp = -sp *  cr + cp * st * sr;
  float64 const    dr00dt =             sp * ct * sr;
  float64 const    dr00dr =  cp * -sr + sp * st * cr;

  // float64 const  r01   =  ct * sr;
  // float64 const dr01dp =  0;
  float64 const    dr01dt = -st * sr;
  float64 const    dr01dr =  ct * cr;

  // float64 const  r02   =  cp * st * sr - sp * cr;
  float64 const    dr02dp = -sp * st * sr - cp * cr;
  float64 const    dr02dt =  cp * ct * sr;
  float64 const    dr02dr =  cp * st * cr + sp * sr;

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // float64 const  r10   =  sp * st *  cr - cp * sr;
  float64 const    dr10dp =  cp * st *  cr + sp * sr;
  float64 const    dr10dt =  sp * ct *  cr;
  float64 const    dr10dr =  sp * st * -sr - cp * cr;

  // float64 const  r11   =  ct *  cr;
  // float64 const dr11dp =  0;
  float64 const    dr11dt = -st *  cr;
  float64 const    dr11dr =  ct * -sr;

  // float64 const  r12   =  sp * sr + cp * st * cr;
  float64 const    dr12dp =  cp * sr - sp * st * cr;
  float64 const    dr12dt =            cp * ct * cr;
  float64 const    dr12dr =  sp * cr - cp * st * sr;

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // float64 const  r20   =  sp * ct;
  float64 const    dr20dp =  cp *  ct;
  float64 const    dr20dt =  sp * -st;
  // float64 const dr20dr =  0;

  // float64 const  r21   = -st;
  // float64 const dr21dp =  0;
  float64 const    dr21dt = -ct;
  // float64 const dr21dr =  0;

  // float64 const  r22   =  cp * ct;
  float64 const    dr22dp = -sp *  ct;
  float64 const    dr22dt =  cp * -st;
  // float64 const dr22dr =  0;

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  float64 const tmp00 = dr00dp * xx + dr02dp * zz;
  float64 const tmp01 = dr10dp * xx + dr12dp * zz;
  float64 const tmp02 = dr20dp * xx + dr22dp * zz;

  float64 const val00 = superordinate.At(0, 0) * tmp00 +
                        superordinate.At(0, 1) * tmp01 +
                        superordinate.At(0, 2) * tmp02;
  float64 const val01 = superordinate.At(1, 0) * tmp00 +
                        superordinate.At(1, 1) * tmp01 +
                        superordinate.At(1, 2) * tmp02;
  float64 const val0d = superordinate.At(2, 0) * tmp00 +
                        superordinate.At(2, 1) * tmp01 +
                        superordinate.At(2, 2) * tmp02;

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  float64 const tmp10 = dr00dt * xx + dr01dt * yy + dr02dt * zz;
  float64 const tmp11 = dr10dt * xx + dr11dt * yy + dr12dt * zz;
  float64 const tmp12 = dr20dt * xx + dr21dt * yy + dr22dt * zz;

  float64 const val10 = superordinate.At(0, 0) * tmp10 +
                        superordinate.At(0, 1) * tmp11 +
                        superordinate.At(0, 2) * tmp12;
  float64 const val11 = superordinate.At(1, 0) * tmp10 +
                        superordinate.At(1, 1) * tmp11 +
                        superordinate.At(1, 2) * tmp12;
  float64 const val1d = superordinate.At(2, 0) * tmp10 +
                        superordinate.At(2, 1) * tmp11 +
                        superordinate.At(2, 2) * tmp12;

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  float64 const    tmp20 = dr00dr * xx + dr01dr * yy + dr02dr * zz;
  float64 const    tmp21 = dr10dr * xx + dr11dr * yy + dr12dr * zz;
  // float64 const tmp22 = 0.0;

  float64 const val20 = superordinate.At(0, 0) * tmp20 +
                        superordinate.At(0, 1) * tmp21;
  float64 const val21 = superordinate.At(1, 0) * tmp20 +
                        superordinate.At(1, 1) * tmp21;
  float64 const val2d = superordinate.At(2, 0) * tmp20 +
                        superordinate.At(2, 1) * tmp21;

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  result.At(0, 0) = dInvSqr * (d * val00 - n0 * val0d);
  result.At(1, 0) = dInvSqr * (d * val01 - n1 * val0d);

  result.At(0, 1) = dInvSqr * (d * val10 - n0 * val1d);
  result.At(1, 1) = dInvSqr * (d * val11 - n1 * val1d);

  result.At(0, 2) = dInvSqr * (d * val20 - n0 * val2d);
  result.At(1, 2) = dInvSqr * (d * val21 - n1 * val2d);

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
Matrix2x6d Differentiator::DifferentiateTransformation(
  Transformation const& t,
  Vector3d       const& pCam,
  Matrix3x4d     const& superordinate,
  Vector3d       const&   subordinate)
{
  float64 const n0 = pCam.X();
  float64 const n1 = pCam.Y();
  float64 const d  = pCam.Z();

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  float64 const dInvSqr = 1.0 / (d * d);

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Matrix2x6d result;

  //derivatives with respect to the translational component
  result.At(0, 3) = dInvSqr *
    (d * superordinate.At(0, 0) - n0 * superordinate.At(2, 0));
  result.At(1, 3) = dInvSqr *
    (d * superordinate.At(1, 0) - n1 * superordinate.At(2, 0));

  result.At(0, 4) = dInvSqr *
    (d * superordinate.At(0, 1) - n0 * superordinate.At(2, 1));
  result.At(1, 4) = dInvSqr *
    (d * superordinate.At(1, 1) - n1 * superordinate.At(2, 1));

  result.At(0, 5) = dInvSqr *
    (d * superordinate.At(0, 2) - n0 * superordinate.At(2, 2));
  result.At(1, 5) = dInvSqr *
    (d * superordinate.At(1, 2) - n1 * superordinate.At(2, 2));

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Vector3d const rotation = QF::EulerAnglesYXZr(t.Rotation());

  float64 const pan  = rotation.X();
  float64 const tilt = rotation.Y();
  float64 const roll = rotation.Z();

  float64 const cp = cos(pan );
  float64 const ct = cos(tilt);
  float64 const cr = cos(roll);

  float64 const sp = sin(pan );
  float64 const st = sin(tilt);
  float64 const sr = sin(roll);

  // d sin / dt =  cos
  // d cos / dt = -sin

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // float64 const  r00   =  cp *  cr + sp * st * sr;
  float64 const    dr00dp = -sp *  cr + cp * st * sr;
  float64 const    dr00dt =             sp * ct * sr;
  float64 const    dr00dr =  cp * -sr + sp * st * cr;

  // float64 const  r01   =  sp * st *  cr - cp * sr;
  float64 const    dr01dp =  cp * st *  cr + sp * sr;
  float64 const    dr01dt =  sp * ct *  cr;
  float64 const    dr01dr =  sp * st * -sr - cp * cr;

  // float64 const  r02   =  sp *  ct;
  float64 const    dr02dp =  cp *  ct;
  float64 const    dr02dt =  sp * -st;
  // float64 const dr02dr =  0;

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // float64 const  r10   =  ct * sr;
  // float64 const dr10dp =  0;
  float64 const    dr10dt = -st * sr;
  float64 const    dr10dr =  ct * cr;

  // float64 const  r11   =  ct *  cr;
  // float64 const dr11dp =  0;
  float64 const    dr11dt = -st *  cr;
  float64 const    dr11dr =  ct * -sr;

  // float64 const  r12   = -st;
  // float64 const dr12dp =  0;
  float64 const    dr12dt = -ct;
  // float64 const dr12dr =  0;

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // float64 const  r20   =  cp * st * sr - sp * cr;
  float64 const    dr20dp = -sp * st * sr - cp * cr;
  float64 const    dr20dt =  cp * ct * sr;
  float64 const    dr20dr =  cp * st * cr + sp * sr;

  // float64 const  r21   =  sp * sr + cp * st * cr;
  float64 const    dr21dp =  cp * sr - sp * st * cr;
  float64 const    dr21dt =            cp * ct * cr;
  float64 const    dr21dr =  sp * cr - cp * st * sr;

  // float64 const  r22   =  cp *  ct;
  float64 const    dr22dp = -sp *  ct;
  float64 const    dr22dt =  cp * -st;
  // float64 const dr22dr =  0;

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  float64 const x0 = subordinate.X();
  float64 const x1 = subordinate.Y();
  float64 const x2 = subordinate.Z();

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  {
    float64 const    tmp00 = dr00dp * x0 + dr01dp * x1 + dr02dp * x2;
    // float64 const tmp01 = 0;
    float64 const    tmp02 = dr20dp * x0 + dr21dp * x1 + dr22dp * x2;

    float64 const val00 = superordinate.At(0, 0) * tmp00 +
                          superordinate.At(0, 2) * tmp02;
    float64 const val01 = superordinate.At(1, 0) * tmp00 +
                          superordinate.At(1, 2) * tmp02;
    float64 const val0d = superordinate.At(2, 0) * tmp00 +
                          superordinate.At(2, 2) * tmp02;

    result.At(0, 0) = dInvSqr * (d * val00 - n0 * val0d);
    result.At(1, 0) = dInvSqr * (d * val01 - n1 * val0d);
  }

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  {
    float64 const tmp10 = dr00dt * x0 + dr01dt * x1 + dr02dt * x2;
    float64 const tmp11 = dr10dt * x0 + dr11dt * x1 + dr12dt * x2;
    float64 const tmp12 = dr20dt * x0 + dr21dt * x1 + dr22dt * x2;

    float64 const val10 = superordinate.At(0, 0) * tmp10 +
                          superordinate.At(0, 1) * tmp11 +
                          superordinate.At(0, 2) * tmp12;
    float64 const val11 = superordinate.At(1, 0) * tmp10 +
                          superordinate.At(1, 1) * tmp11 +
                          superordinate.At(1, 2) * tmp12;
    float64 const val1d = superordinate.At(2, 0) * tmp10 +
                          superordinate.At(2, 1) * tmp11 +
                          superordinate.At(2, 2) * tmp12;

    result.At(0, 1) = dInvSqr * (d * val10 - n0 * val1d);
    result.At(1, 1) = dInvSqr * (d * val11 - n1 * val1d);
  }

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  {
    float64 const tmp20 = dr00dr * x0 + dr01dr * x1;
    float64 const tmp21 = dr10dr * x0 + dr11dr * x1;
    float64 const tmp22 = dr20dr * x0 + dr21dr * x1;

    float64 const val20 = superordinate.At(0, 0) * tmp20 +
                          superordinate.At(0, 1) * tmp21 +
                          superordinate.At(0, 2) * tmp22;
    float64 const val21 = superordinate.At(1, 0) * tmp20 +
                          superordinate.At(1, 1) * tmp21 +
                          superordinate.At(1, 2) * tmp22;
    float64 const val2d = superordinate.At(2, 0) * tmp20 +
                          superordinate.At(2, 1) * tmp21 +
                          superordinate.At(2, 2) * tmp22;

    result.At(0, 2) = dInvSqr * (d * val20 - n0 * val2d);
    result.At(1, 2) = dInvSqr * (d * val21 - n1 * val2d);
  }

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm
