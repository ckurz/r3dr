//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_differentiator_ProjectiveDifferentiator_h_
#define dlm_differentiator_ProjectiveDifferentiator_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include <vector>
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.h"
#include "dlm/math/Matrix.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Forward declarations
//------------------------------------------------------------------------------
class ProjectiveCamera;

//==============================================================================
// ProjectiveDifferentiator class
//------------------------------------------------------------------------------
class ProjectiveDifferentiator
{
public:
  //============================================================================
  // Nested
  //----------------------------------------------------------------------------
  struct Result
  {
    Matrix2x12d pDeriv;
    Matrix2x3d  oDeriv;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Vector2d    pProj;
  };

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  static Result Differentiate(
    ProjectiveCamera const& camera,
    Vector3d         const& point);
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_differentiator_ProjectiveDifferentiator_h_
