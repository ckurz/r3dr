//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_differentiator_NumericalDifferentiator_h_
#define dlm_differentiator_NumericalDifferentiator_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include <vector>
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.h"
#include "dlm/math/Matrix.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Forward declarations
//------------------------------------------------------------------------------
class Camera;
class Distortion;
class Transformation;

//==============================================================================
// NumericalDifferentiator class
//------------------------------------------------------------------------------
class NumericalDifferentiator
{
public:
  //============================================================================
  // Nested
  //----------------------------------------------------------------------------
  struct Result
  {
    std::vector<Matrix2x6d> tDeriv;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Matrix2x5d              cDeriv;
    Matrix2x8d              dDeriv;
    Matrix2x3d              oDeriv;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Vector2d                pProj;
  };

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  static Result Differentiate(
    Camera                      const& camera,
    Distortion                  const& distortion,
    std::vector<Transformation> const& transformations,
    Vector3d                    const& point);

private:
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  static Matrix2x5d DifferentiateCamera(
    Camera                      const& camera,
    Distortion                  const& distortion,
    std::vector<Transformation> const& transformations,
    Vector3d                    const& point);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  static Matrix2x8d DifferentiateDistortion(
    Camera                      const& camera,
    Distortion                  const& distortion,
    std::vector<Transformation> const& transformations,
    Vector3d                    const& point);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  static std::vector<Matrix2x6d> DifferentiateTransformations(
    Camera                      const& camera,
    Distortion                  const& distortion,
    std::vector<Transformation> const& transformations,
    Vector3d                    const& point);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  static Matrix2x3d DifferentiatePoint(
    Camera                      const& camera,
    Distortion                  const& distortion,
    std::vector<Transformation> const& transformations,
    Vector3d                    const& point);
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_differentiator_NumericalDifferentiator_h_
