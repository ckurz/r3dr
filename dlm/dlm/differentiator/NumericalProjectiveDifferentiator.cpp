//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/differentiator/NumericalProjectiveDifferentiator.h"

//------------------------------------------------------------------------------
#include <cmath>
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <limits>
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.inline.h"
#include "dlm/math/Matrix.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/entity/ProjectiveCamera.h"
#include "dlm/entity/ProjectiveCamera.inline.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
NumericalProjectiveDifferentiator::Result
  NumericalProjectiveDifferentiator::Differentiate(
    ProjectiveCamera const& camera, Vector3d const& point)
{
  Result result;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  result.pDeriv = DifferentiateCamera(camera, point);
  result.oDeriv = DifferentiatePoint (camera, point);

  result.pProj  = camera(point);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
Matrix2x12d NumericalProjectiveDifferentiator::DifferentiateCamera(
    ProjectiveCamera const& camera,
    Vector3d         const& point)
{
  Matrix2x12d result;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  volatile float64 e = sqrt(std::numeric_limits<float64>::epsilon());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 row = 0; row < 3; ++row)
  {
    for (uint32 col = 0; col < 4; ++col)
    {
      volatile float64 x  = camera.At(row, col);
      volatile float64 h  = e * x;
      volatile float64 xi = x + h;
        
      h = xi - x;
        
      ProjectiveCamera camera0 = camera;
      ProjectiveCamera camera1 = camera;
        
      camera0.At(row, col) += h;
      camera1.At(row, col) -= h;
        
      // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      Vector2d const p0Proj = camera0(point);
      Vector2d const p1Proj = camera1(point);

      uint32 const index = row * 4 + col;
       
      result.At(0, index) = (p0Proj.X() - p1Proj.X()) / (2 * h);    
      result.At(1, index) = (p0Proj.Y() - p1Proj.Y()) / (2 * h);    
    }
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
Matrix2x3d NumericalProjectiveDifferentiator::DifferentiatePoint(
    ProjectiveCamera const& camera, Vector3d const& point)
{
  Matrix2x3d result;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  volatile float64 e = sqrt(std::numeric_limits<float64>::epsilon());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < 3; ++i)
  {
    volatile float64 x  = point.At(i);
    volatile float64 h  = e * x;
    volatile float64 xi = x + h;
        
    h = xi - x;
        
    Vector3d p0 = point;
    Vector3d p1 = point;
      
    p0.At(i) += h;
    p1.At(i) -= h;
        
    Vector2d const p0Proj = camera(p0);
    Vector2d const p1Proj = camera(p1);
        
    result.At(0, i) = (p0Proj.X() - p1Proj.X()) / (2 * h);    
    result.At(1, i) = (p0Proj.Y() - p1Proj.Y()) / (2 * h);    
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm
