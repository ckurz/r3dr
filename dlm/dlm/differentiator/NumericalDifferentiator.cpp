//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/differentiator/NumericalDifferentiator.h"

//------------------------------------------------------------------------------
#include <cmath>
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <limits>
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.inline.h"
#include "dlm/math/Matrix.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Quaternion.h"
#include "dlm/math/Quaternion.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/MatrixFunctions.h"
#include "dlm/math/VectorFunctions.h"
#include "dlm/math/QuaternionFunctions.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/entity/Camera.h"
#include "dlm/entity/Camera.inline.h"
#include "dlm/entity/Distortion.h"
#include "dlm/entity/Distortion.inline.h"
#include "dlm/entity/Transformation.h"
#include "dlm/entity/Transformation.inline.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
NumericalDifferentiator::Result NumericalDifferentiator::Differentiate(
    Camera                      const& camera,
    Distortion                  const& distortion,
    std::vector<Transformation> const& transformations,
    Vector3d                    const& point)
{
  Result result;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  result.cDeriv =
    DifferentiateCamera         (camera, distortion, transformations, point);
  result.dDeriv =
    DifferentiateDistortion     (camera, distortion, transformations, point);
  result.tDeriv =
    DifferentiateTransformations(camera, distortion, transformations, point);
  result.oDeriv =
    DifferentiatePoint          (camera, distortion, transformations, point);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Determine projected point position
  {
    Vector3d p = point;
    
    for (uint64 j = 0; j < transformations.size(); ++j)
    {
      p = transformations.at(transformations.size() - 1 - j) * p;
    }
    
    Vector2d const pDist = distortion(p);
    Vector2d const pProj = camera(pDist);
    
    result.pProj = pProj;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
Matrix2x5d NumericalDifferentiator::DifferentiateCamera(
    Camera                      const& camera,
    Distortion                  const& distortion,
    std::vector<Transformation> const& transformations,
    Vector3d                    const& point)
{
  Matrix2x5d result;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Vector3d p = point;
        
  for (uint64 i = 0; i < transformations.size(); ++i)
  {
    p = transformations.at(transformations.size() - 1 - i) * p;
  }
        
  Vector2d const pDist = distortion(p);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  volatile float64 e = sqrt(std::numeric_limits<float64>::epsilon());
    
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // derivatives with respect to the focal length
  {
    volatile float64 x  = camera.FocalLength();
    volatile float64 h  = e * x;
    volatile float64 xi = x + h;
        
    h = xi - x;
        
    Camera camera0 = camera;
    Camera camera1 = camera;
        
    camera0.FocalLength() += h;
    camera1.FocalLength() -= h;
        
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Vector2d const p0Proj = camera0(pDist);
    Vector2d const p1Proj = camera1(pDist);
       
    result.At(0, 0) = (p0Proj.X() - p1Proj.X()) / (2 * h);    
    result.At(1, 0) = (p0Proj.Y() - p1Proj.Y()) / (2 * h);    
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // derivatives with respect to the principal point
  for (uint32 i = 0; i < 2; ++i)
  {
    volatile float64 x  = camera.PrincipalPoint().At(i);
    volatile float64 h  = e * x;
    volatile float64 xi = x + h;
        
    h = xi - x;
        
    Camera camera0 = camera;
    Camera camera1 = camera;
        
    camera0.PrincipalPoint().At(i) += h;
    camera1.PrincipalPoint().At(i) -= h;
              
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Vector2d const p0Proj = camera0(pDist);
    Vector2d const p1Proj = camera1(pDist);
        
    result.At(0, i + 1) = (p0Proj.X() - p1Proj.X()) / (2 * h);    
    result.At(1, i + 1) = (p0Proj.Y() - p1Proj.Y()) / (2 * h);    
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // derivatives with respect to the pixel aspect ratio parameter
  {
    volatile float64 x  = camera.PixelAspectRatio();
    volatile float64 h  = e * x;
    volatile float64 xi = x + h;
        
    h = xi - x;
        
    Camera camera0 = camera;
    Camera camera1 = camera;
        
    camera0.PixelAspectRatio() += h;
    camera1.PixelAspectRatio() -= h;
        
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Vector2d const p0Proj = camera0(pDist);
    Vector2d const p1Proj = camera1(pDist);
       
    result.At(0, 3) = (p0Proj.X() - p1Proj.X()) / (2 * h);    
    result.At(1, 3) = (p0Proj.Y() - p1Proj.Y()) / (2 * h);    
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // derivatives with respect to the skew parameter
  {
    volatile float64 x  = camera.Skew();
    volatile float64 h  = e * x;
    volatile float64 xi = x + h;
        
    h = xi - x;
        
    Camera camera0 = camera;
    Camera camera1 = camera;
        
    camera0.Skew() += h;
    camera1.Skew() -= h;
        
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Vector2d const p0Proj = camera0(pDist);
    Vector2d const p1Proj = camera1(pDist);
       
    result.At(0, 4) = (p0Proj.X() - p1Proj.X()) / (2 * h);    
    result.At(1, 4) = (p0Proj.Y() - p1Proj.Y()) / (2 * h);    
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
Matrix2x8d NumericalDifferentiator::DifferentiateDistortion(
    Camera                      const& camera,
    Distortion                  const& distortion,
    std::vector<Transformation> const& transformations,
    Vector3d                    const& point)
{
  Matrix2x8d result;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Vector3d p = point;
        
  for (uint64 i = 0; i < transformations.size(); ++i)
  {
    p = transformations.at(transformations.size() - 1 - i) * p;
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  volatile float64 e = sqrt(std::numeric_limits<float64>::epsilon());
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < 6; ++i)
  {
    volatile float64 x  = distortion.K().At(i);
    volatile float64 h  = e * x;
    volatile float64 xi = x + h;
        
    h = xi - x;

    Distortion distortion0 = distortion;
    Distortion distortion1 = distortion;
        
    distortion0.K().At(i) += h;
    distortion1.K().At(i) -= h;
        
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Vector2d const p0Dist = distortion0(p);
    Vector2d const p1Dist = distortion1(p);
        
    Vector2d const p0Proj = camera(p0Dist);
    Vector2d const p1Proj = camera(p1Dist);
        
    result.At(0, i + 2) = (p0Proj.X() - p1Proj.X()) / (2 * h);    
    result.At(1, i + 2) = (p0Proj.Y() - p1Proj.Y()) / (2 * h);    
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < 2; ++i)
  {
    volatile float64 x  = distortion.P().At(i);
    volatile float64 h  = e * x;
    volatile float64 xi = x + h;
        
    h = xi - x;

    Distortion distortion0 = distortion;
    Distortion distortion1 = distortion;
        
    distortion0.P().At(i) += h;
    distortion1.P().At(i) -= h;

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Vector2d const p0Dist = distortion0(p);
    Vector2d const p1Dist = distortion1(p);
       
    Vector2d const p0Proj = camera(p0Dist);
    Vector2d const p1Proj = camera(p1Dist);
        
    result.At(0, i) = (p0Proj.X() - p1Proj.X()) / (2 * h);    
    result.At(1, i) = (p0Proj.Y() - p1Proj.Y()) / (2 * h);    
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
std::vector<Matrix2x6d> NumericalDifferentiator::DifferentiateTransformations(
    Camera                      const& camera,
    Distortion                  const& distortion,
    std::vector<Transformation> const& transformations,
    Vector3d                    const& point)
{
  std::vector<Matrix2x6d> result;

  result.resize(transformations.size());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  volatile float64 e = sqrt(std::numeric_limits<float64>::epsilon());
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 t = 0; t < transformations.size(); ++t)
  {      
    for (uint32 i = 0; i < 3; ++i)
    {
      volatile float64 x  =
        QF::EulerAnglesYXZr(transformations.at(t).Rotation()).At(i);
      volatile float64 h  = e * x;
      volatile float64 xi = x + h;
        
      h = xi - x;
          
      std::vector<Transformation> t0 = transformations;
      std::vector<Transformation> t1 = transformations;
     
      Vector3d tmp0 = QF::EulerAnglesYXZr(transformations.at(t).Rotation());
      Vector3d tmp1 = QF::EulerAnglesYXZr(transformations.at(t).Rotation());
          
      tmp0.At(i) += h;
      tmp1.At(i) -= h;
     
      t0.at(t).Rotation() = QF::FromEulerAnglesYXZr(tmp0);
      t1.at(t).Rotation() = QF::FromEulerAnglesYXZr(tmp1);

      // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      Vector3d p0 = point;
      Vector3d p1 = point;
        
      for (uint64 j = 0; j < transformations.size(); ++j)
      {
        p0 = t0.at(transformations.size() - 1 - j) * p0;
        p1 = t1.at(transformations.size() - 1 - j) * p1;
      }
        
      Vector2d const p0Dist = distortion(p0);
      Vector2d const p1Dist = distortion(p1);
        
      Vector2d const p0Proj = camera(p0Dist);
      Vector2d const p1Proj = camera(p1Dist);
        
      result.at(t).At(0, i) =  (p0Proj.X() - p1Proj.X()) / (2 * h);    
      result.at(t).At(1, i) =  (p0Proj.Y() - p1Proj.Y()) / (2 * h);    
    }
  }  
      
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 t = 0; t < transformations.size(); ++t)
  {      
    for (uint32 i = 0; i < 3; ++i)
    {
      volatile float64 x  = transformations.at(t).Translation().At(i);
      volatile float64 h  = e * x;
      volatile float64 xi = x + h;
     
      h = xi - x;
          
      std::vector<Transformation> t0 = transformations;
      std::vector<Transformation> t1 = transformations;
          
      t0.at(t).Translation().At(i) += h;
      t1.at(t).Translation().At(i) -= h;

      Vector3d p0 = point;
      Vector3d p1 = point;
        
      for (uint64 j = 0; j < transformations.size(); ++j)
      {
        p0 = t0.at(transformations.size() - 1 - j) * p0;
        p1 = t1.at(transformations.size() - 1 - j) * p1;
      }
        
      Vector2d const p0Dist = distortion(p0);
      Vector2d const p1Dist = distortion(p1);
      
      Vector2d const p0Proj = camera(p0Dist);
      Vector2d const p1Proj = camera(p1Dist);
        
      result.at(t).At(0, i + 3) = (p0Proj.X() - p1Proj.X()) / (2 * h);    
      result.at(t).At(1, i + 3) = (p0Proj.Y() - p1Proj.Y()) / (2 * h);    
    }
  }  
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
Matrix2x3d NumericalDifferentiator::DifferentiatePoint(
    Camera                      const& camera,
    Distortion                  const& distortion,
    std::vector<Transformation> const& transformations,
    Vector3d                    const& point)
{
  Matrix2x3d result;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  volatile float64 e = sqrt(std::numeric_limits<float64>::epsilon());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < 3; ++i)
  {
    volatile float64 x  = point.At(i);
    volatile float64 h  = e * x;
    volatile float64 xi = x + h;
        
    h = xi - x;
        
    Vector3d p0 = point;
    Vector3d p1 = point;
      
    p0.At(i) += h;
    p1.At(i) -= h;
        
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    for (uint64 j = 0; j < transformations.size(); ++j)
    {
      p0 = transformations.at(transformations.size() - 1 - j) * p0;
      p1 = transformations.at(transformations.size() - 1 - j) * p1;
    }
      
    Vector2d const p0Dist = distortion(p0);
    Vector2d const p1Dist = distortion(p1);
        
    Vector2d const p0Proj = camera(p0Dist);
    Vector2d const p1Proj = camera(p1Dist);
        
    result.At(0, i) = (p0Proj.X() - p1Proj.X()) / (2 * h);    
    result.At(1, i) = (p0Proj.Y() - p1Proj.Y()) / (2 * h);    
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm
