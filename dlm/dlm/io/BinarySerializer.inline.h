//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_io_BinarySerializer_inline_h_
#define dlm_io_BinarySerializer_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/io/BinarySerializer.h"

//------------------------------------------------------------------------------
#include "dlm/scene/Scene.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/ArrayObjectTable.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"
#include "dlm/core/Types.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <iostream>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Implementation namespace
//------------------------------------------------------------------------------
namespace Implementation
{

//==============================================================================
// Serialization of scalar/enum/etc. types
//------------------------------------------------------------------------------
template <typename T>
inline std::ostream& Serialize(T const& data, std::ostream& stream)
{
  return stream.write(reinterpret_cast<char const*>(&data), sizeof(T));
}

//==============================================================================
// Serialization of pointer types
//------------------------------------------------------------------------------
template <typename T>
inline std::ostream& Serialize(T const* data, uint64 const bytes, std::ostream& stream)
{
  std::streamsize const n = static_cast<std::streamsize>(bytes);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream.write(reinterpret_cast<char const*>(data), n);
}

//==============================================================================
// Serialization of STL pairs
//------------------------------------------------------------------------------
template <typename T, typename U>
inline std::ostream& Serialize(std::pair<T, U> const& data, std::ostream& stream)
{
  Implementation::Serialize(data.first,  stream);
  Implementation::Serialize(data.second, stream);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//==============================================================================
// Serialization of STL containers
//------------------------------------------------------------------------------
template <typename T>
inline std::ostream& SerializeSTLContainer(
  std::vector<T> const& container, std::ostream& stream)
{
  uint64 const containerSize = container.size();
  
  Implementation::Serialize(containerSize, stream);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (auto it = container.cbegin(); it != container.cend(); ++it)
  {
    Implementation::Serialize(*it, stream);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
template <typename T>
inline std::ostream& SerializeSTLContainer(
  std::set<T> const& container, std::ostream& stream)
{
  uint64 const containerSize = container.size();
  
  Implementation::Serialize(containerSize, stream);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (auto it = container.cbegin(); it != container.cend(); ++it)
  {
    Implementation::Serialize(*it, stream);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
template <typename T, typename U>
inline std::ostream& SerializeSTLContainer(
  std::map<T, U> const& container, std::ostream& stream)
{
  uint64 const containerSize = container.size();
  
  Implementation::Serialize(containerSize, stream);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (auto it = container.cbegin(); it != container.cend(); ++it)
  {
    Implementation::Serialize(it->first,  stream);
    Implementation::Serialize(it->second, stream);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//==============================================================================
// Serialization of STL strings
//------------------------------------------------------------------------------
inline std::ostream& SerializeSTLString(
  std::string const& string, std::ostream& stream)
{
  uint64 const size = (string.length() < 512) ? string.length() : 512;

  Serialize(size, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (size > 0)
  {
    Serialize(string.c_str(), size, stream);
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//==============================================================================
// Implementation namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
inline std::ostream& BinarySerializer::Serialize(
  Scene const& scene, std::ostream& stream)
{
  DLM_ASSERT(stream.good(), DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Serialize non-storage member variables.
  Implementation::Serialize(scene.autoDeleteFSet_, stream);
  Implementation::Serialize(scene.autoDeleteWMat_, stream);
  Implementation::Serialize(scene.autoDeleteFTrk_, stream);
  Implementation::Serialize(scene.autoDeletePRep_, stream);
  Implementation::Serialize(scene.autoDeleteCRep_, stream);
  Implementation::Serialize(scene.autoDeleteDRep_, stream);
  Implementation::Serialize(scene.autoDeleteORep_, stream);
  Implementation::Serialize(scene.autoDeleteTSeq_, stream);
  Implementation::Serialize(scene.autoDeleteTRep_, stream);

  // Serialize storage member variables.
  SerializeArrayObjectTable(scene.iRepS_, stream);
  SerializeArrayObjectTable(scene.fSetS_, stream);
  SerializeArrayObjectTable(scene.wMatS_, stream);
  SerializeArrayObjectTable(scene.fTrkS_, stream);
  SerializeArrayObjectTable(scene.pRepS_, stream);
  SerializeArrayObjectTable(scene.cRepS_, stream);
  SerializeArrayObjectTable(scene.dRepS_, stream);
  SerializeArrayObjectTable(scene.oRepS_, stream);
  SerializeArrayObjectTable(scene.tSeqS_, stream);
  SerializeArrayObjectTable(scene.tRepS_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DLM_ASSERT(stream.good(), DLM_RUNTIME_ERROR);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
template <typename T>
inline std::ostream& BinarySerializer::SerializeArrayObjectTable(
  ArrayObjectTable<T> const& storage,
  std::ostream             & stream)
{
  Implementation::Serialize(storage.inactive_, stream);
  Implementation::Serialize(storage.size_,     stream);
  Implementation::Serialize(storage.range_,    stream);
  Implementation::Serialize(storage.compact_,  stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Size has to go first, so that it can be correctly deserialized.
  uint64 const vectorSize = storage.vector_.size();
  
  Implementation::Serialize(vectorSize, stream);

  // Serialize contents.
  for (uint64 i = 0; i < vectorSize; ++i)
  {
    SerializeArrayObjectTableObject<T>(storage.vector_.at(i), stream);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
template <typename T>
inline std::ostream& BinarySerializer::SerializeArrayObjectTableObject(
  typename ArrayObjectTable<T>::Object const& object, std::ostream& stream)
{
  Implementation::Serialize(object.state_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Serialize(object.contents_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::ostream& BinarySerializer::Serialize(
  IRep const& iRep, std::ostream& stream)
{
  Implementation::SerializeSTLString(iRep.filename_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::Serialize(iRep.sequenceNumber_, stream);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::Serialize(iRep.Reference<PRep>::id_, stream);
  Implementation::Serialize(iRep.Reference<CRep>::id_, stream);
  Implementation::Serialize(iRep.Reference<DRep>::id_, stream);
  Implementation::Serialize(iRep.Reference<FSet>::id_, stream);
  Implementation::Serialize(iRep.Reference<TSeq>::id_, stream);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Serialize(IRep::BaseClass(iRep), stream);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::ostream& BinarySerializer::Serialize(
  FSet const& fSet, std::ostream& stream)
{
  Implementation::Serialize(fSet.Reference<IRep>::id_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  SerializeArrayObjectTable(fSet, stream);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::ostream& BinarySerializer::Serialize(
  FRep const& fRep, std::ostream& stream)
{
  Implementation::Serialize(fRep.Reference<FTrk>::id_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Serialize(FRep::BaseClass(fRep), stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::ostream& BinarySerializer::Serialize(
  WMat const& wMat, std::ostream& stream)
{
  Implementation::SerializeSTLContainer(
    wMat.ReferencePairSet<FSet, FRep>::set_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Serialize(WMat::BaseClass(wMat), stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::ostream& BinarySerializer::Serialize(
  FTrk const& fTrk, std::ostream& stream)
{
  Implementation::Serialize(fTrk.Reference<TSeq>::id_, stream);
  Implementation::Serialize(fTrk.Reference<ORep>::id_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::SerializeSTLContainer(fTrk, stream);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::ostream& BinarySerializer::Serialize(
  PRep const& pRep, std::ostream& stream)
{
  Implementation::Serialize(pRep.Reference<IRep>::id_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Serialize(PRep::BaseClass(pRep), stream);
  Serialize(PRep::FlagClass(pRep), stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::ostream& BinarySerializer::Serialize(
  CRep const& cRep, std::ostream& stream)
{
  Implementation::SerializeSTLContainer(cRep.ReferenceSet<IRep>::set_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Serialize(CRep::BaseClass(cRep), stream);
  Serialize(CRep::FlagClass(cRep), stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::ostream& BinarySerializer::Serialize(
  DRep const& dRep, std::ostream& stream)
{
  Implementation::SerializeSTLContainer(dRep.ReferenceSet<IRep>::set_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Serialize(DRep::BaseClass(dRep), stream);
  Serialize(DRep::FlagClass(dRep), stream);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::ostream& BinarySerializer::Serialize(
  ORep const& oRep, std::ostream& stream)
{
  Implementation::SerializeSTLContainer(oRep.ReferenceSet<FTrk>::set_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::Serialize(oRep.Reference<TSeq>::id_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Serialize(ORep::BaseClass(oRep), stream);
  Serialize(ORep::FlagClass(oRep), stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::ostream& BinarySerializer::Serialize(
  TSeq const& tSeq, std::ostream& stream)
{
  Implementation::SerializeSTLContainer(tSeq.ReferenceSet<IRep>::set_, stream);
  Implementation::SerializeSTLContainer(tSeq.ReferenceSet<TSeq>::set_, stream);
  Implementation::SerializeSTLContainer(tSeq.ReferenceSet<FTrk>::set_, stream);
  Implementation::SerializeSTLContainer(tSeq.ReferenceSet<ORep>::set_, stream);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::Serialize(tSeq.Reference<TSeq>::id_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::SerializeSTLContainer(tSeq, stream);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::ostream& BinarySerializer::Serialize(
  TRep const& tRep, std::ostream& stream)
{
  Implementation::SerializeSTLContainer(tRep.tSeqM_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Serialize(TRep::BaseClass(tRep), stream);
  Serialize(TRep::FlagClass(tRep), stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::ostream& BinarySerializer::Serialize(
  Image const& image, std::ostream& stream)
{
  Serialize(image.size_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::ostream& BinarySerializer::Serialize(
  FeaturePoint const& featurePoint, std::ostream& stream)
{
  Serialize(FeaturePoint::BaseClass(featurePoint), stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::ostream& BinarySerializer::Serialize(
  WeightingMatrix const& weightingMatrix, std::ostream& stream)
{
  Serialize(WeightingMatrix::BaseClass(weightingMatrix), stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::ostream& BinarySerializer::Serialize(
  ProjectiveCamera const& projectiveCamera, std::ostream& stream)
{
  Serialize(ProjectiveCamera::BaseClass(projectiveCamera), stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::ostream& BinarySerializer::Serialize(
  Camera const& camera, std::ostream& stream)
{
  Serialize(camera.principalPoint_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::Serialize(camera.focalLength_,      stream);
  Implementation::Serialize(camera.pixelAspectRatio_, stream);
  Implementation::Serialize(camera.skew_,             stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::ostream& BinarySerializer::Serialize(
  Distortion const& distortion, std::ostream& stream)
{
  Serialize(distortion.k_, stream);
  Serialize(distortion.p_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::ostream& BinarySerializer::Serialize(
  ObjectPoint const& objectPoint, std::ostream& stream)
{
  Serialize(ObjectPoint::BaseClass(objectPoint), stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::ostream& BinarySerializer::Serialize(
  Transformation const& transformation, std::ostream& stream)
{
  Serialize(transformation.rotation_,    stream);
  Serialize(transformation.translation_, stream);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::Serialize(transformation.invert_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline std::ostream& BinarySerializer::Serialize(
  Vector<SIZE, T> const& vector, std::ostream& stream)
{
  uint64 const bytes = SIZE * sizeof(T);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::Serialize(vector.elements_, bytes, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
inline std::ostream& BinarySerializer::Serialize(
  Matrix<ROWS, COLS, T> const& matrix, std::ostream& stream)
{
  uint64 const bytes = ROWS * COLS * sizeof(T);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::Serialize(matrix.elements_, bytes, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
template <typename T>
inline std::ostream& BinarySerializer::Serialize(
  Quaternion<T> const& quaternion, std::ostream& stream)
{
  Serialize(quaternion.xyz_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::Serialize(quaternion.w_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} // namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif // dlm_io_BinarySerializer_inline_h_
