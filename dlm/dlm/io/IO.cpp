//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/io/IO.h"

//------------------------------------------------------------------------------
#include "dlm/io/BinarySerializer.h"
#include "dlm/io/BinarySerializer.inline.h"
#include "dlm/io/BinaryDeserializer.h"
#include "dlm/io/BinaryDeserializer.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/Scene.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <fstream>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Unnamed namespace
//------------------------------------------------------------------------------
namespace {

//==============================================================================
// Local variables
//------------------------------------------------------------------------------
char const dlmSceneFileHeader[] = "dlmSceneFileHeader V1.5.0";

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
uint32 const dlmSceneFileHeaderLength = 25;

//==============================================================================
// Unnamed namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// Load function
//------------------------------------------------------------------------------
Scene Load(std::string const& filename)
{
  std::ifstream file(filename, std::ios::binary);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Read and check file type string.
  char buffer[dlmSceneFileHeaderLength + 1] = { '\0' };
  
  file.read(buffer, dlmSceneFileHeaderLength);
 
  std::string const   testString(buffer            );
  std::string const headerString(dlmSceneFileHeader);
  
  DLM_ASSERT(testString == headerString,DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Scene scene;
  
  BinaryDeserializer::Deserialize(scene, file);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DLM_ASSERT(file.peek() == EOF,DLM_RUNTIME_ERROR);
  DLM_ASSERT(file.eof(),        DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return scene;
}

//==============================================================================
// Store function
//------------------------------------------------------------------------------
void Store(Scene const& scene, std::string const& filename)
{
  std::ofstream file(filename, std::ios::binary);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Write file type string.
  file.write(dlmSceneFileHeader, dlmSceneFileHeaderLength);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  BinarySerializer::Serialize(scene, file);
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
