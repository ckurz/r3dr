//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_io_BinaryDeserializer_inline_h_
#define dlm_io_BinaryDeserializer_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/io/BinaryDeserializer.h"

//------------------------------------------------------------------------------
#include "dlm/scene/Scene.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/ArrayObjectTable.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"
#include "dlm/core/Types.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <iostream>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Implementation namespace
//------------------------------------------------------------------------------
namespace Implementation
{

//==============================================================================
// Deserialization of scalar/enum/etc. types
//------------------------------------------------------------------------------
template <typename T>
inline std::istream& Deserialize(T& data, std::istream& stream)
{
  return stream.read(reinterpret_cast<char*>(&data), sizeof(T));
}

//==============================================================================
// Deserialization of pointer types
//------------------------------------------------------------------------------
template <typename T>
inline std::istream& Deserialize(T* data, uint64 const bytes, std::istream& stream)
{
  std::streamsize const n = static_cast<std::streamsize>(bytes);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream.read(reinterpret_cast<char*>(data), n);
}

//==============================================================================
// Deserialization of STL pairs
//------------------------------------------------------------------------------
template <typename T, typename U>
inline std::istream& Deserialize(std::pair<T, U>& data, std::istream& stream)
{
  Implementation::Deserialize(data.first,  stream);
  Implementation::Deserialize(data.second, stream);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//==============================================================================
// Deserialization of STL containers
//------------------------------------------------------------------------------
template <typename T>
inline std::istream& DeserializeSTLContainer(
  std::vector<T>& container, std::istream& stream)
{
  DLM_ASSERT(container.empty(),DLM_RUNTIME_ERROR);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint64 containerSize = 0;
  
  Deserialize(containerSize, stream);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  container.resize(containerSize);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (auto it = container.begin(); it != container.end(); ++it)
  {
    Deserialize(*it, stream);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
template <typename T>
inline std::istream& DeserializeSTLContainer(
  std::set<T>& container, std::istream& stream)
{
  DLM_ASSERT(container.empty(),DLM_RUNTIME_ERROR);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint64 containerSize = 0;
  
  Deserialize(containerSize, stream);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < containerSize; ++i)
  {
    T element = T();
    
    Deserialize(element, stream);
    
    container.insert(element);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
template <typename T, typename U>
inline std::istream& DeserializeSTLContainer(
  std::map<T, U>& container, std::istream& stream)
{
  DLM_ASSERT(container.empty(),DLM_RUNTIME_ERROR);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint64 containerSize = 0;
  
  Deserialize(containerSize, stream);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < containerSize; ++i)
  {
    T key   = T();
    U value = U();
    
    Deserialize(key,   stream);
    Deserialize(value, stream);
    
    container[key] = value;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//==============================================================================
// Deserialization of STL strings
//------------------------------------------------------------------------------
inline std::istream& DeserializeSTLString(
  std::string& string, std::istream& stream)
{
  DLM_ASSERT(string.empty(),DLM_RUNTIME_ERROR);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint64 size = 0;
  
  Deserialize(size, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DLM_ASSERT(size <= 512,DLM_RUNTIME_ERROR);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (size > 0)
  {
    char buffer[512 + 1] = { '\0' };
    
    Deserialize(buffer, size, stream);
    
    string = buffer;
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//==============================================================================
// Implementation namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
inline std::istream& BinaryDeserializer::Deserialize(
  Scene& scene, std::istream& stream)
{
  DLM_ASSERT(stream.good(),DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Deserialize non-storage member variables.
  Implementation::Deserialize(scene.autoDeleteFSet_, stream);
  Implementation::Deserialize(scene.autoDeleteWMat_, stream);
  Implementation::Deserialize(scene.autoDeleteFTrk_, stream);
  Implementation::Deserialize(scene.autoDeletePRep_, stream);
  Implementation::Deserialize(scene.autoDeleteCRep_, stream);
  Implementation::Deserialize(scene.autoDeleteDRep_, stream);
  Implementation::Deserialize(scene.autoDeleteORep_, stream);
  Implementation::Deserialize(scene.autoDeleteTSeq_, stream);
  Implementation::Deserialize(scene.autoDeleteTRep_, stream);

  // Deserialize storage member variables.
  DeserializeArrayObjectTable(scene.iRepS_, stream);
  DeserializeArrayObjectTable(scene.fSetS_, stream);
  DeserializeArrayObjectTable(scene.wMatS_, stream);
  DeserializeArrayObjectTable(scene.fTrkS_, stream);
  DeserializeArrayObjectTable(scene.pRepS_, stream);
  DeserializeArrayObjectTable(scene.cRepS_, stream);
  DeserializeArrayObjectTable(scene.dRepS_, stream);
  DeserializeArrayObjectTable(scene.oRepS_, stream);
  DeserializeArrayObjectTable(scene.tSeqS_, stream);
  DeserializeArrayObjectTable(scene.tRepS_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DLM_ASSERT(stream.good(),DLM_RUNTIME_ERROR);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//----------------------------------------------------------------------------
template <typename T>
inline std::istream& BinaryDeserializer::DeserializeArrayObjectTable(
  ArrayObjectTable<T>& storage, std::istream& stream)
{
  DLM_ASSERT(storage.IsEmpty(),DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::Deserialize(storage.inactive_, stream);
  Implementation::Deserialize(storage.size_    , stream);
  Implementation::Deserialize(storage.range_   , stream);
  Implementation::Deserialize(storage.compact_ , stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint64 vectorSize = 0;

  Implementation::Deserialize(vectorSize, stream);

  storage.vector_.resize(vectorSize);

  // Deserialize contents.
  for (uint64 i = 0; i < vectorSize; ++i)
  {
    DeserializeArrayObjectTableObject<T>(storage.vector_.at(i), stream);
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
template <typename T>
inline std::istream& BinaryDeserializer::DeserializeArrayObjectTableObject(
  typename ArrayObjectTable<T>::Object& object, std::istream& stream)
{
  Implementation::Deserialize(object.state_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Deserialize(object.contents_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::istream& BinaryDeserializer::Deserialize(
  IRep& iRep, std::istream& stream)
{
  Implementation::DeserializeSTLString(iRep.filename_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::Deserialize(iRep.sequenceNumber_, stream);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::Deserialize(iRep.Reference<PRep>::id_, stream);
  Implementation::Deserialize(iRep.Reference<CRep>::id_, stream);
  Implementation::Deserialize(iRep.Reference<DRep>::id_, stream);
  Implementation::Deserialize(iRep.Reference<FSet>::id_, stream);
  Implementation::Deserialize(iRep.Reference<TSeq>::id_, stream);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  IRep::BaseClass image;
  
  Deserialize(image, stream);

  iRep = image;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::istream& BinaryDeserializer::Deserialize(
  FSet& fSet, std::istream& stream)
{
  Implementation::Deserialize(fSet.Reference<IRep>::id_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DeserializeArrayObjectTable(fSet, stream);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::istream& BinaryDeserializer::Deserialize(
  FRep& fRep, std::istream& stream)
{
  Implementation::Deserialize(fRep.Reference<FTrk>::id_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FRep::BaseClass featurePoint;
  
  Deserialize(featurePoint, stream);
  
  fRep = featurePoint;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::istream& BinaryDeserializer::Deserialize(
  WMat& wMat, std::istream& stream)
{
  Implementation::DeserializeSTLContainer(
    wMat.ReferencePairSet<FSet, FRep>::set_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  WMat::BaseClass weightingMatrix;
  
  Deserialize(weightingMatrix, stream);
  
  wMat = weightingMatrix;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::istream& BinaryDeserializer::Deserialize(
  FTrk& fTrk, std::istream& stream)
{
  Implementation::Deserialize(fTrk.Reference<TSeq>::id_, stream);
  Implementation::Deserialize(fTrk.Reference<ORep>::id_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::DeserializeSTLContainer(fTrk, stream);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::istream& BinaryDeserializer::Deserialize(
  PRep& pRep, std::istream& stream)
{
  Implementation::Deserialize(pRep.Reference<IRep>::id_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  PRep::BaseClass projectiveCamera;
  PRep::FlagClass projectiveCameraFlags;
  
  Deserialize(projectiveCamera,      stream);
  Deserialize(projectiveCameraFlags, stream);
  
  pRep = projectiveCamera;
  pRep = projectiveCameraFlags;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::istream& BinaryDeserializer::Deserialize(
  CRep& cRep, std::istream& stream)
{
  Implementation::DeserializeSTLContainer(cRep.ReferenceSet<IRep>::set_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  CRep::BaseClass camera;
  CRep::FlagClass cameraFlags;
  
  Deserialize(camera,      stream);
  Deserialize(cameraFlags, stream);
  
  cRep = camera;
  cRep = cameraFlags;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::istream& BinaryDeserializer::Deserialize(
  DRep& dRep, std::istream& stream)
{
  Implementation::DeserializeSTLContainer(dRep.ReferenceSet<IRep>::set_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DRep::BaseClass distortion;
  DRep::FlagClass distortionFlags;
  
  Deserialize(distortion,      stream);
  Deserialize(distortionFlags, stream);

  dRep = distortion;
  dRep = distortionFlags;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::istream& BinaryDeserializer::Deserialize(
  ORep& oRep, std::istream& stream)
{
  Implementation::DeserializeSTLContainer(oRep.ReferenceSet<FTrk>::set_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::Deserialize(oRep.Reference<TSeq>::id_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  ORep::BaseClass objectPoint;
  ORep::FlagClass objectPointFlags;
  
  Deserialize(objectPoint,      stream);
  Deserialize(objectPointFlags, stream);
  
  oRep = objectPoint;
  oRep = objectPointFlags;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::istream& BinaryDeserializer::Deserialize(
  TSeq& tSeq, std::istream& stream)
{
  Implementation::DeserializeSTLContainer(tSeq.ReferenceSet<IRep>::set_, stream);
  Implementation::DeserializeSTLContainer(tSeq.ReferenceSet<TSeq>::set_, stream);
  Implementation::DeserializeSTLContainer(tSeq.ReferenceSet<FTrk>::set_, stream);
  Implementation::DeserializeSTLContainer(tSeq.ReferenceSet<ORep>::set_, stream);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::Deserialize(tSeq.Reference<TSeq>::id_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::DeserializeSTLContainer(tSeq, stream);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::istream& BinaryDeserializer::Deserialize(
  TRep& tRep, std::istream& stream)
{
  Implementation::DeserializeSTLContainer(tRep.tSeqM_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  TRep::BaseClass transformation;
  TRep::FlagClass transformationFlags;
  
  Deserialize(transformation,      stream);
  Deserialize(transformationFlags, stream);
  
  tRep = transformation;
  tRep = transformationFlags;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::istream& BinaryDeserializer::Deserialize(
  Image& image, std::istream& stream)
{
  Deserialize(image.size_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::istream& BinaryDeserializer::Deserialize(
  FeaturePoint& featurePoint, std::istream& stream)
{
  FeaturePoint::BaseClass vector;
  
  Deserialize(vector, stream);
  
  featurePoint = vector;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::istream& BinaryDeserializer::Deserialize(
  WeightingMatrix& weightingMatrix, std::istream& stream)
{
  WeightingMatrix::BaseClass matrix;
  
  Deserialize(matrix, stream);
  
  weightingMatrix = matrix;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::istream& BinaryDeserializer::Deserialize(
  ProjectiveCamera& projectiveCamera, std::istream& stream)
{
  ProjectiveCamera::BaseClass matrix;

  Deserialize(matrix, stream);
  
  projectiveCamera = matrix;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::istream& BinaryDeserializer::Deserialize(
  Camera& camera, std::istream& stream)
{
  Deserialize(camera.principalPoint_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::Deserialize(camera.focalLength_,      stream);
  Implementation::Deserialize(camera.pixelAspectRatio_, stream);
  Implementation::Deserialize(camera.skew_,             stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::istream& BinaryDeserializer::Deserialize(
  Distortion& distortion, std::istream& stream)
{
  Deserialize(distortion.k_, stream);
  Deserialize(distortion.p_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::istream& BinaryDeserializer::Deserialize(
  ObjectPoint& objectPoint, std::istream& stream)
{
  ObjectPoint::BaseClass vector;

  Deserialize(vector, stream);
  
  objectPoint = vector;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
inline std::istream& BinaryDeserializer::Deserialize(
  Transformation& transformation, std::istream& stream)
{
  Deserialize(transformation.rotation_,    stream);
  Deserialize(transformation.translation_, stream);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::Deserialize(transformation.invert_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline std::istream& BinaryDeserializer::Deserialize(
  Vector<SIZE, T>& vector, std::istream& stream)
{
  uint64 const bytes = SIZE * sizeof(T);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::Deserialize(vector.elements_, bytes, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
inline std::istream& BinaryDeserializer::Deserialize(
  Matrix<ROWS, COLS, T>& matrix, std::istream& stream)
{
  uint64 const bytes = ROWS * COLS * sizeof(T);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::Deserialize(matrix.elements_, bytes, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//------------------------------------------------------------------------------
template <typename T>
inline std::istream& BinaryDeserializer::Deserialize(
  Quaternion<T>& quaternion, std::istream& stream)
{
  Deserialize(quaternion.xyz_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::Deserialize(quaternion.w_, stream);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} // namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif // dlm_io_BinaryDeserializer_inline_h_
