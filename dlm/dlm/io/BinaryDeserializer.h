//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_io_BinaryDeserializer_h_
#define dlm_io_BinaryDeserializer_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/scene/Identifiers.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.h"
#include "dlm/math/Matrix.h"
#include "dlm/math/Quaternion.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/ArrayObjectTable.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <iosfwd>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Forward declarations
//------------------------------------------------------------------------------
class Scene;
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
class Image;
class FeaturePoint;
class WeightingMatrix;
class ProjectiveCamera;
class Camera;
class Distortion;
class ObjectPoint;
class Transformation;

//==============================================================================
// BinaryDeserializer class
//------------------------------------------------------------------------------
class BinaryDeserializer
{
public:
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  static std::istream& Deserialize(Scene& scene, std::istream& stream);

private:
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  // Deserialization of array object tables
  //----------------------------------------------------------------------------
  template <typename T>
  static std::istream& DeserializeArrayObjectTable(
    ArrayObjectTable<T> & storage, std::istream& stream);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <typename T>
  static std::istream& DeserializeArrayObjectTableObject(
    typename ArrayObjectTable<T>::Object & object, std::istream& stream);

  //----------------------------------------------------------------------------
  // Deserialization of scene objects
  //----------------------------------------------------------------------------
  static std::istream& Deserialize(IRep& iRep, std::istream& stream);
  static std::istream& Deserialize(FSet& fSet, std::istream& stream);
  static std::istream& Deserialize(FRep& fRep, std::istream& stream);
  static std::istream& Deserialize(WMat& cMat, std::istream& stream);
  static std::istream& Deserialize(FTrk& fTrk, std::istream& stream);
  static std::istream& Deserialize(PRep& pRep, std::istream& stream);
  static std::istream& Deserialize(CRep& cRep, std::istream& stream);
  static std::istream& Deserialize(DRep& dRep, std::istream& stream);
  static std::istream& Deserialize(ORep& oRep, std::istream& stream);
  static std::istream& Deserialize(TSeq& tSeq, std::istream& stream);
  static std::istream& Deserialize(TRep& tRep, std::istream& stream);

  //----------------------------------------------------------------------------
  // Deserialization of entities
  //----------------------------------------------------------------------------
  static std::istream& Deserialize(Image            & image,
                                   std::istream     & stream);
  static std::istream& Deserialize(FeaturePoint     & featurePoint,
                                   std::istream     & stream);
  static std::istream& Deserialize(WeightingMatrix  & covarianceMatrix,
                                   std::istream     & stream);
  static std::istream& Deserialize(ProjectiveCamera & projectiveCamera,
                                   std::istream     & stream);
  static std::istream& Deserialize(Camera           & camera,
                                   std::istream     & stream);
  static std::istream& Deserialize(Distortion       & distortion,
                                   std::istream     & stream);
  static std::istream& Deserialize(ObjectPoint      & objectPoint,
                                   std::istream     & stream);
  static std::istream& Deserialize(Transformation   & transformation,
                                   std::istream     & stream);
  
  //----------------------------------------------------------------------------
  // Deserialization of low-level types
  //----------------------------------------------------------------------------
  template <uint32 SIZE, typename T>
  static std::istream& Deserialize(
    Vector<SIZE, T>& vector, std::istream& stream);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <uint32 ROWS, uint32 COLS, typename T>
  static std::istream& Deserialize(
    Matrix<ROWS, COLS, T>& matrix, std::istream& stream);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <typename T>
  static std::istream& Deserialize(
    Quaternion<T>& quaternion, std::istream& stream);
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif 
