//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_io_BinarySerializer_h_
#define dlm_io_BinarySerializer_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/scene/Identifiers.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.h"
#include "dlm/math/Matrix.h"
#include "dlm/math/Quaternion.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/ArrayObjectTable.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <iosfwd>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Forward declarations
//------------------------------------------------------------------------------
class Scene;
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
class Image;
class FeaturePoint;
class WeightingMatrix;
class ProjectiveCamera;
class Camera;
class Distortion;
class ObjectPoint;
class Transformation;

//==============================================================================
// BinarySerializer class
//------------------------------------------------------------------------------
class BinarySerializer
{
public:
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  static std::ostream& Serialize(Scene const& scene, std::ostream& stream);

private:
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  // Serialization of array object tables
  //----------------------------------------------------------------------------
  template <typename T>
  static std::ostream& SerializeArrayObjectTable(
    ArrayObjectTable<T> const& storage, std::ostream& stream);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <typename T>
  static std::ostream& SerializeArrayObjectTableObject(
    typename ArrayObjectTable<T>::Object const& object, std::ostream& stream);

  //----------------------------------------------------------------------------
  // Serialization of scene objects
  //----------------------------------------------------------------------------
  static std::ostream& Serialize(IRep const& iRep, std::ostream& stream);
  static std::ostream& Serialize(FSet const& fSet, std::ostream& stream);
  static std::ostream& Serialize(FRep const& fRep, std::ostream& stream);
  static std::ostream& Serialize(WMat const& wMat, std::ostream& stream);
  static std::ostream& Serialize(FTrk const& fTrk, std::ostream& stream);
  static std::ostream& Serialize(PRep const& cRep, std::ostream& stream);
  static std::ostream& Serialize(CRep const& cRep, std::ostream& stream);
  static std::ostream& Serialize(DRep const& dRep, std::ostream& stream);
  static std::ostream& Serialize(ORep const& oRep, std::ostream& stream);
  static std::ostream& Serialize(TSeq const& tSeq, std::ostream& stream);
  static std::ostream& Serialize(TRep const& tRep, std::ostream& stream);
  
  //----------------------------------------------------------------------------
  // Serialization of entities
  //----------------------------------------------------------------------------
  static std::ostream& Serialize(Image            const& image,
                                 std::ostream          & stream);
  static std::ostream& Serialize(FeaturePoint     const& featurePoint,
                                 std::ostream          & stream);
  static std::ostream& Serialize(WeightingMatrix const& weightingMatrix,
                                 std::ostream          & stream);
  static std::ostream& Serialize(ProjectiveCamera const& projectiveCamera,
                                 std::ostream          & stream);
  static std::ostream& Serialize(Camera           const& camera,
                                 std::ostream          & stream);
  static std::ostream& Serialize(Distortion       const& distortion,
                                 std::ostream          & stream);
  static std::ostream& Serialize(ObjectPoint      const& objectPoint,
                                 std::ostream          & stream);
  static std::ostream& Serialize(Transformation   const& transformation,
                                 std::ostream          & stream);
  
  //----------------------------------------------------------------------------
  // Serialization of low-level types
  //----------------------------------------------------------------------------
  template <uint32 SIZE, typename T>
  static std::ostream& Serialize(
    Vector<SIZE, T> const& vector, std::ostream& stream);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <uint32 ROWS, uint32 COLS, typename T>
  static std::ostream& Serialize(
    Matrix<ROWS, COLS, T> const& matrix, std::ostream& stream);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <typename T>
  static std::ostream& Serialize(
    Quaternion<T> const& quaternion, std::ostream& stream);
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
