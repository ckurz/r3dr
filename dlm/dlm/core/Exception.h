//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_core_Exception_h_
#define dlm_core_Exception_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/core/Types.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <stdexcept>
#include <string>

//==============================================================================
// Preprocessor macros
//------------------------------------------------------------------------------
//! \brief Custom assertion macro.
//!
//! \sa DLM_DOMAIN_ERROR, DLM_OUT_OF_RANGE, DLM_RUNTIME_ERROR,
//! DLM_SIZE_MISMATCH, DLM_DEGENERATE_SET.
#define DLM_ASSERT(expression, exception) \
if (!(expression))                        \
{                                         \
  throw exception(#expression);           \
}

//------------------------------------------------------------------------------
//! \brief Custom assertion macro with annotation.
//!
//! \sa DLM_DOMAIN_ERROR, DLM_OUT_OF_RANGE, DLM_RUNTIME_ERROR,
//! DLM_SIZE_MISMATCH, DLM_DEGENERATE_SET.
#define DLM_ANNOTATED_ASSERT(expression, exception, explanation) \
if (!(expression))                                               \
{                                                                \
  throw exception(#expression " (" explanation ")");             \
}

//------------------------------------------------------------------------------
//! \brief Macro for the creation of a DomainError exception with additional
//! information.
//!
//! \sa DLM_ASSERT, DLM_ANNOTATED_ASSERT.
#define DLM_DOMAIN_ERROR(description) \
dlm::DomainError(__FILE__, __LINE__, description)

//------------------------------------------------------------------------------
//! \brief Macro for the creation of a OutOfRange exception with additional
//! information.
//!
//! \sa DLM_ASSERT, DLM_ANNOTATED_ASSERT.
#define DLM_OUT_OF_RANGE(description) \
dlm::OutOfRange(__FILE__, __LINE__, description)

//------------------------------------------------------------------------------
//! \brief Macro for the creation of a DomainError exception with additional
//! information.
//!
//! \sa DLM_ASSERT, DLM_ANNOTATED_ASSERT.
#define DLM_RUNTIME_ERROR(description) \
dlm::RuntimeError(__FILE__, __LINE__, description)

//------------------------------------------------------------------------------
//! \brief Macro for the creation of a DomainError exception with additional
//! information.
//!
//! \sa DLM_ASSERT, DLM_ANNOTATED_ASSERT.
#define DLM_SIZE_MISMATCH(description) \
dlm::SizeMismatch(__FILE__, __LINE__, description)

//------------------------------------------------------------------------------
//! \brief Macro for the creation of a DegenerateSet exception with additional
//! information.
//!
//! \sa DLM_ASSERT, DLM_ANNOTATED_ASSERT.
#define DLM_DEGENERATE_SET(description) \
dlm::DegenerateSet(__FILE__, __LINE__, description)

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Exception class
//------------------------------------------------------------------------------
//! \brief Abstract base class for all other exception classes.
class Exception
{
public:
  //----------------------------------------------------------------------------
  // Constructors
  //----------------------------------------------------------------------------
  Exception(char const* file, int32 const line) throw();

  //----------------------------------------------------------------------------
  // Destructor
  //----------------------------------------------------------------------------
  virtual ~Exception() throw();

  //----------------------------------------------------------------------------
  // Accessors
  //----------------------------------------------------------------------------
  char const* File() const throw();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  int32 Line() const throw();

  //----------------------------------------------------------------------------
  // Member functions
  //----------------------------------------------------------------------------
  //! \brief Provides the exception type string in derived classes.
  //! @return The exception type string.
  virtual char const* Type() const throw() = 0;

private:
  //----------------------------------------------------------------------------
  // Member variables
  //----------------------------------------------------------------------------
  char const* file_; //!< \brief The name of the file the exception originated.
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  int32 line_; //!< \brief The line in the file the exception originated.
};

//==============================================================================
// DomainError exception class
//------------------------------------------------------------------------------
//! \brief Exception class for domain errors.
class DomainError : public Exception, public std::domain_error
{
public:
  //----------------------------------------------------------------------------
  // Constructors
  //----------------------------------------------------------------------------
  explicit DomainError(
    char        const* file,
    int32       const  line,
    std::string const& description);

  //----------------------------------------------------------------------------
  // Destructor
  //----------------------------------------------------------------------------
  virtual ~DomainError() throw();

  //----------------------------------------------------------------------------
  // Member functions
  //----------------------------------------------------------------------------
  virtual char const* Type() const throw();
};

//==============================================================================
// OutOfRange exception class
//------------------------------------------------------------------------------
//! \brief Exception class for out-of-range conditions.
class OutOfRange : public Exception, public std::out_of_range
{
public:
  //----------------------------------------------------------------------------
  // Constructors
  //----------------------------------------------------------------------------
  explicit OutOfRange(
    char        const* file,
    int32       const  line,
    std::string const& description);

  //----------------------------------------------------------------------------
  // Destructor
  //----------------------------------------------------------------------------
  virtual ~OutOfRange() throw();

  //----------------------------------------------------------------------------
  // Member functions
  //----------------------------------------------------------------------------
  virtual char const* Type() const throw();
};

//==============================================================================
// RuntimeError exception class
//------------------------------------------------------------------------------
//! \brief Exception class for runtime errors.
class RuntimeError : public Exception, public std::runtime_error
{
public:
  //----------------------------------------------------------------------------
  // Constructors
  //----------------------------------------------------------------------------
  explicit RuntimeError(
    char        const* file,
    int32       const  line,
    std::string const& description);

  //----------------------------------------------------------------------------
  // Destructor
  //----------------------------------------------------------------------------
  virtual ~RuntimeError() throw();

  //----------------------------------------------------------------------------
  // Member functions
  //----------------------------------------------------------------------------
  virtual char const* Type() const throw();
};

//==============================================================================
// SizeMismatch exception class
//------------------------------------------------------------------------------
//! \brief Exception class for size mismatches.
class SizeMismatch : public RuntimeError
{
public:
  //----------------------------------------------------------------------------
  // Constructors
  //----------------------------------------------------------------------------
  explicit SizeMismatch(
    char        const* file,
    int32       const  line,
    std::string const& description);

  //----------------------------------------------------------------------------
  // Destructor
  //----------------------------------------------------------------------------
  virtual ~SizeMismatch() throw();

  //----------------------------------------------------------------------------
  // Member functions
  //----------------------------------------------------------------------------
  virtual char const* Type() const throw();
};

//==============================================================================
// DegenerateSet exception class
//------------------------------------------------------------------------------
//! \brief Exception class for degenerate sets.
class DegenerateSet : public RuntimeError
{
public:
  //----------------------------------------------------------------------------
  // Constructors
  //----------------------------------------------------------------------------
  explicit DegenerateSet(
    char        const* file,
    int32       const  line,
    std::string const& description);

  //----------------------------------------------------------------------------
  // Destructor
  //----------------------------------------------------------------------------
  virtual ~DegenerateSet() throw();

  //----------------------------------------------------------------------------
  // Member functions
  //----------------------------------------------------------------------------
  virtual char const* Type() const throw();
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
