//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_core_ArrayObjectTable_inline_h_
#define dlm_core_ArrayObjectTable_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/core/ArrayObjectTable.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// ArrayObjectTable
//------------------------------------------------------------------------------
// Constructors
//------------------------------------------------------------------------------
//! \brief The default constructor.
//!
//! Creates an empty, compact array object table.
template <typename T>
inline ArrayObjectTable<T>::ArrayObjectTable()
  : inactive_(-1  )
  , size_    (0   )
  , range_   (0   )
  , compact_ (true)
{}

//==============================================================================
// Operators
//------------------------------------------------------------------------------
//! \brief Returns the element at the specified position.
//! @param i The subscript index for the desired element.
//! @return A \b const \b reference to the desired element.
//!
//! If `i` is not in the valid range for the container, a OutOfRange exception
//! is thrown.
//! If The element at position `i` is not active, a RuntimeError exception is
//! thrown.
template <typename T>
inline T const& ArrayObjectTable<T>::operator()(uint64 const i) const
{
  DLM_ASSERT(i < range_,            DLM_OUT_OF_RANGE );
  DLM_ASSERT(vector_[i].IsActive(), DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return vector_[i].Contents();
}

//------------------------------------------------------------------------------
//! \brief Returns the element at the specified position.
//! @param i The subscript index for the desired element.
//! @return A \b reference to the desired element.
//!
//! If `i` is not in the valid range for the container, a OutOfRange exception
//! is thrown.
//! If The element at position `i` is not active, a RuntimeError exception is
//! thrown.
template <typename T>
inline T& ArrayObjectTable<T>::operator()(uint64 const i)
{
  DLM_ASSERT(i < range_,            DLM_OUT_OF_RANGE );
  DLM_ASSERT(vector_[i].IsActive(), DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return vector_[i].Contents();
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
//! \brief Checks whether the container is compact.
//! @return `true` if the container is compact, `false` otherwise.
template <typename T>
inline bool ArrayObjectTable<T>::IsCompact() const
{
  return compact_;
}

//------------------------------------------------------------------------------
//! \brief Checks whether the container is empty.
//! @return `true` if the conatainer is empty, `false` otherwise.
template <typename T>
inline bool ArrayObjectTable<T>::IsEmpty() const
{
  return size_ == 0;
}

//------------------------------------------------------------------------------
//! \brief Returns the number of elements the container has already claimed from
//! its internal storage.
//! @return The current capacity.
template <typename T>
inline uint64 ArrayObjectTable<T>::Capacity() const
{
  return vector_.size();
}

//------------------------------------------------------------------------------
//! \brief Reserve storage.
//! @param n The desired capacity of the container.
template <typename T>
inline void ArrayObjectTable<T>::Reserve(uint64 const n)
{
  vector_.reserve(n);
}

//------------------------------------------------------------------------------
//! \brief Increase the number of elements in the container.
//! @param n The desired size of the container.
//!
//! If `n` is smaller than or equal to the current size of the container, this
//! function has no effect.
//!
//! If `n` is larger than the current size of the container, the internal size
//! is increased and the size and range of the container are set to `n`.
//!
//! If the container is not compact, a RuntimeError is thrown.
template <typename T>
inline void ArrayObjectTable<T>::Expand(uint64 const n)
{
  DLM_ASSERT(compact_, DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (n <= size_) { return; }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (n > vector_.size())
  {
    vector_.resize(n);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  size_  = n;
  range_ = n;
}

//------------------------------------------------------------------------------
//! \brief Adds elements to the container.
//! @param element The element to be added.
//! return The index of the position the element was inserted into the
//! container.
//!
//! A copy of the given element is inserted into the container. If the container
//! is compact, the element is appended at the end, otherwise it is inserted at
//! the position that was last released.
template <typename T>
inline uint64 ArrayObjectTable<T>::Push(T const& element)
{
  uint64 const i = (inactive_ == -1) ? New() : Activate();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  ++size_; //only place where size is increased

  vector_[i].Contents() = element;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return i;
}

//------------------------------------------------------------------------------
//! \brief Adds elements to the container.
//! @param element The element to be added.
//! return The index of the position the element was inserted into the
//! container.
//!
//! A copy of the given element is inserted into the container. If the container
//! is compact, the element is appended at the end, otherwise it is inserted at
//! the position that was last released.
template <typename T>
inline uint64 ArrayObjectTable<T>::PushBack(T const& element)
{
  return Push(element);
}

//------------------------------------------------------------------------------
//! \brief Checks whether the element at the given position is active.
//! @param i The position of the element.
//! @return `true` if the element is active, `false` if not.
//!
//! If `i` is not within range, an OutOfRange exception is thrown.
template <typename T>
inline bool ArrayObjectTable<T>::Active(uint64 const i) const
{
  DLM_ASSERT(i < range_, DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return vector_[i].IsActive();
}

//------------------------------------------------------------------------------
//! \brief Deletes the element at the given position.
//! @param i The position of the element.
//!
//! The element at position `i` is marked as inactive and replaced by a default-
//! constructed copy.
//! After a call to this function, the container is no longer compact, even if
//! the element released was the last in the container.
//!
//! If `i` is not within range, an OutOfRange exception is thrown.
//! If the element at position `i` is not active, a RuntimeError exception is
//! thrown.
template <typename T>
inline void ArrayObjectTable<T>::Release(uint64 const i)
{
  DLM_ASSERT(i < range_,            DLM_OUT_OF_RANGE );
  DLM_ASSERT(vector_[i].IsActive(), DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  compact_ = false;

  vector_[i].Contents() = T(); //possibly free up some memory
  vector_[i].State   () = inactive_;

  inactive_ = - 2 - static_cast<int64>(i);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  --size_;
}

//------------------------------------------------------------------------------
//! \brief Remove all elements from the container.
//!
//! The internal storage is cleared, size and range are set to 0, compact is set
//! to `true`, and the index to the next inactive element is invalidated.
template <typename T>
inline void ArrayObjectTable<T>::Clear()
{
  inactive_ = -1;
  
  size_     = 0;
  range_    = 0;
  
  compact_  = true;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  vector_.clear();
}

//------------------------------------------------------------------------------
//! \brief Compact the container and create a lookup table for subscript index
//! transformation.
//! @param compress Specifies whether the internal storage shall be resized.
//! @return The lookup table for subscript index transformation.
//!
//! Released elements are removed from the container and in the process a lookup
//! table is produced that has to be applied to all indices refering to this
//! container.
//!
//! \attention Application of the lookup table is \b mandatory, as all indices
//! are potentially invalidated.
//!
//! If compress is set to `true`, the internal storage is resized to fit the
//! range of the container.
template <typename T>
inline std::vector<uint64> ArrayObjectTable<T>::Purge(bool const compress)
{
  std::vector<uint64> lookupTable;

  uint64 vacancy = 0;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < range_; ++i)
  {
    if (vector_[i].IsActive())
    {
      if (vacancy != i)
      {
        vector_[vacancy] = vector_[i];
      }

      lookupTable.push_back(vacancy);

      ++vacancy;
    }
    else
    {
      lookupTable.push_back(dlm::Invalid);
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  inactive_ = -1;
  
  range_    = size_;
  compact_  = true;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (compress)
  {
    vector_.resize(range_);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return lookupTable;
}

//------------------------------------------------------------------------------
//! \brief Activate a deactivated element of the container.
//! @return The position of the activated element.
//!
//! The positions of the activated elements are the positions of the released
//! elements in reverse order.
template <typename T>
inline uint64 ArrayObjectTable<T>::Activate()
{
  uint64 const i = static_cast<uint64>(- 2 - inactive_);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Object& object = vector_[i];

  inactive_ = object.State();

  object.State() = 0;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return i;
}

//------------------------------------------------------------------------------
//! \brief Request a new element from internal storage.
//! @return The index of the new element.
//!
//! The range of the container is increased by 1.
//!
//! If the current range of the container is equal to the size of the internal
//! storage, the size of the internal storage is increased by 1.
template <typename T>
inline uint64 ArrayObjectTable<T>::New()
{
  if (range_ == static_cast<uint64>(vector_.size()))
  {
    // There is no space left. Create some.
    vector_.push_back(T());
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint64 const i = range_;

  vector_[i].State() = 0;

  ++range_;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return i;
}

//------------------------------------------------------------------------------
//! \brief Determine the index of the first active element in the container.
//! @return The index of the first active element.
//!
//! If there are no active elements in the container, the current range will be
//! returned.
template <typename T>
uint64 ArrayObjectTable<T>::FirstValidIndex() const
{
  uint64 index = 0;

  while (index < range_ && !Active(index))
  {
    ++index;
  }

  return index;
}

//==============================================================================
// Accessors/Mutators
//------------------------------------------------------------------------------
//! \brief Returns the current range.
//! @return The current range.
template <typename T>
inline uint64 ArrayObjectTable<T>::Range() const
{
  return range_;
}

//------------------------------------------------------------------------------
//! \brief Returns the current size.
//! @return The current size.
template <typename T>
inline uint64 ArrayObjectTable<T>::Size() const
{
  return size_;
}

//------------------------------------------------------------------------------
//! \brief Returns the element at the specified position.
//! @param i The subscript index for the desired element.
//! @return A \b const \b reference to the desired element.
//!
//! If `i` is not in the valid range for the container, a OutOfRange exception
//! is thrown.
//! If The element at position `i` is not active, a RuntimeError exception is
//! thrown.
//! \deprecated This function was declared deprecated; use #operator()()
//! instead.
template <typename T>
inline T const& ArrayObjectTable<T>::At(uint64 const i) const
{
  return operator()(i);
}

//------------------------------------------------------------------------------
//! \brief Returns the element at the specified position.
//! @param i The subscript index for the desired element.
//! @return A \b reference to the desired element.
//!
//! If `i` is not in the valid range for the container, a OutOfRange exception
//! is thrown.
//! If The element at position `i` is not active, a RuntimeError exception is
//! thrown.
//! \deprecated This function was declared deprecated; use #operator()()
//! instead.
template <typename T>
inline T& ArrayObjectTable<T>::At(uint64 const i)
{
  return operator()(i);
}

//==============================================================================
// STL iterator interface
//------------------------------------------------------------------------------
//! \brief Returns a #const_iterator to the beginning.
//! @return The #const_iterator pointing to the first element.
//! \sa #begin, #end, #cend.
template <typename T>
inline typename ArrayObjectTable<T>::const_iterator
  ArrayObjectTable<T>::cbegin() const
{
  return const_iterator(this, FirstValidIndex());
}

//------------------------------------------------------------------------------
//! \brief Returns a #const_iterator to the beginning.
//! @return The #const_iterator pointing to the first element.
//! \sa #cbegin, #end, #cend.
template <typename T>
inline typename ArrayObjectTable<T>::const_iterator
  ArrayObjectTable<T>::begin() const
{
  return const_iterator(this, FirstValidIndex());
}

//------------------------------------------------------------------------------
//! \brief Returns an #iterator to the beginning.
//! @return The #iterator pointing to the first element.
//! \sa #cbegin, #end, #cend.
template <typename T>
inline typename ArrayObjectTable<T>::iterator
  ArrayObjectTable<T>::begin()
{
  return iterator(this, FirstValidIndex());
}

//------------------------------------------------------------------------------
//! \brief Returns a #const_iterator to the end.
//! @return The #const_iterator pointing to the element following the last
//! element of the container.
//! \note Accessing this element will lead to an OutOfRange exception being
//! raised.
//! \sa #begin, #cbegin, #end.
template <typename T>
inline typename ArrayObjectTable<T>::const_iterator
  ArrayObjectTable<T>::cend() const
{
  return const_iterator(this, range_);
}

//------------------------------------------------------------------------------
//! \brief Returns a #const_iterator to the end.
//! @return The #const_iterator pointing to the element following the last
//! element of the container.
//! \note Accessing this element will lead to an OutOfRange exception being
//! raised.
//! \sa #begin, #cbegin, #cend.
template <typename T>
inline typename ArrayObjectTable<T>::const_iterator
  ArrayObjectTable<T>::end() const
{
  return const_iterator(this, range_);
}

//------------------------------------------------------------------------------
//! \brief Returns a #iterator to the end.
//! @return The #iterator pointing to the element following the last element of
//! the container.
//! \note Accessing this element will lead to an OutOfRange exception being
//! raised.
//! \sa #begin, #cbegin, #cend.
template <typename T>
inline typename ArrayObjectTable<T>::iterator
  ArrayObjectTable<T>::end()
{
  return iterator(this, range_);
}

//==============================================================================
// ArrayObjectTable<T>::Iterator
//------------------------------------------------------------------------------
// Constructors
//------------------------------------------------------------------------------
//! \brief The default constructor.
//! @param object A pointer to the ArrayObjectTable this iterator is pointing
//! to.
//! @param index The index into the ArrayObjectTable.
template <typename T>
template <bool isConst>
ArrayObjectTable<T>::Iterator<isConst>::Iterator(
  ObjectPointer object,
  uint64 const index)
  : object_(object)
  , index_ (index )
{}

//------------------------------------------------------------------------------
//! \brief The copy-constructor for conversion from iterators to
//! const_iterators.
//! @param other The iterator that shall be converted.
template <typename T>
template <bool isConst>
ArrayObjectTable<T>::Iterator<isConst>::Iterator(
  Iterator<false> const& other)
  : object_(other.object_)
  , index_ (other.index_ )
{}

//------------------------------------------------------------------------------
//! \brief Returns the object the iterator points to.
template <typename T>
template <bool isConst>
typename ArrayObjectTable<T>::template Iterator<isConst>::reference
  ArrayObjectTable<T>::Iterator<isConst>::operator*() const
{
  return object_->operator()(index_);
}

//------------------------------------------------------------------------------
//! \brief Returns the address to the object the iterator points to.
template <typename T>
template <bool isConst>
typename ArrayObjectTable<T>::template Iterator<isConst>::pointer
  ArrayObjectTable<T>::Iterator<isConst>::operator->() const
{
  return &(object_->operator()(index_));
}

//------------------------------------------------------------------------------
//! \brief Increments the iterator.
//template <typename T>
//template <bool isConst>
//typename ArrayObjectTable<T>::Iterator<isConst>&
//  ArrayObjectTable<T>::Iterator<isConst>::operator++()
//{
//  uint64 const range = object_->Range();
//
//  while (index_ < range)
//  {
//    ++index_;
//
//    if (index_ == range || object_->Active(index_))
//    {
//      break;
//    }
//  }
//
//  return *this;
//}

//------------------------------------------------------------------------------
//! \brief Increments the iterator.
//template <typename T>
//template <bool isConst>
//typename ArrayObjectTable<T>::Iterator<isConst>
//  ArrayObjectTable<T>::Iterator<isConst>::operator++(int)
//{
//  auto tmp(*this);
//
//  this->operator++();
//
//  return tmp;
//}

//------------------------------------------------------------------------------
//! \brief Retrieve the current subscript index of the iterator.
//! @return The current subscript index.
template <typename T>
template <bool isConst>
uint64 ArrayObjectTable<T>::Iterator<isConst>::Index() const
{
  return index_;
}

//==============================================================================
// ArrayObjectTable<T>::Object
//------------------------------------------------------------------------------
// Constructors
//------------------------------------------------------------------------------
//! \brief The default constructor.
//! @param element The desired value of the contents.
//!
//! The object is constructed with a state of 0, making it active by default.
template <typename T>
inline ArrayObjectTable<T>::Object::Object(T const& element)
  : contents_(element)
  , state_   (0      )
{}

//==============================================================================
// Accessors/Mutators
//------------------------------------------------------------------------------
//! \brief Returns the contents of the object.
//! @return A \b const \b reference to the contents.
template <typename T>
inline T const& ArrayObjectTable<T>::Object::Contents() const
{
  return contents_;
}

//------------------------------------------------------------------------------
//! \brief Returns the contents of the object.
//! @return A \b reference to the contents.
template <typename T>
inline T& ArrayObjectTable<T>::Object::Contents()
{
  return contents_;
}

//------------------------------------------------------------------------------
//! \brief Returns the current state of the object.
//! @return The \b value of to the current state.
template <typename T>
inline int64 ArrayObjectTable<T>::Object::State() const
{
  return state_;
}

//------------------------------------------------------------------------------
//! \brief Returns the current state of the object.
//! @return A \b reference to the current state.
template <typename T>
inline int64& ArrayObjectTable<T>::Object::State()
{
  return state_;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
//! \brief Checks whether the object is active.
//! @return `true` if the object is active, `false` otherwise.
//!
//! The object is active if the internal state is equal to or greater than 0.
template <typename T>
inline bool ArrayObjectTable<T>::Object::IsActive() const
{
  return state_ >= 0;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
