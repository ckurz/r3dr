//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_core_AutoArrayPtr_h_
#define dlm_core_AutoArrayPtr_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/core/Types.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
//! \brief A stripped-down smart pointer template class for dynamic arrays.
//!
//! This class manages an array of objects created by a new[] expression. When
//! the AutoArrayPtr is destroyed, the managed array of objects is destroyed
//! with it. It provides exception safety for dynamically-allocated objects.
//!
//! \warning This class is \b not \b safe for use in STL containers, as the
//! right hand arguments are modified on copy assignment and copy construction.
//! \note This class is missing the conversion functionality provided by
//! std::auto_ptr.
//!
//! Usage example:
//! \code
//! std::size_t const size = 10;
//!
//! {
//!   AutoArrayPtr<int> ptr(new int[size]);
//!
//!   // Use the allocated memory for computations.
//!   ptr[0] = ...;
//!   ptr[1] = ...;
//!
//! } //At this point, ptr goes out of scope and the allocated memory is
//!   // deleted automatically.
//! \endcode
//!
// AutoArrayPtr template class
//------------------------------------------------------------------------------
template <typename T>
class AutoArrayPtr
{
public:
  //----------------------------------------------------------------------------
  // Constructors
  //----------------------------------------------------------------------------
  explicit AutoArrayPtr(T* ptr = 0) throw();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  AutoArrayPtr(AutoArrayPtr& other) throw();

  //----------------------------------------------------------------------------
  // Destructor
  //----------------------------------------------------------------------------
  ~AutoArrayPtr();

  //----------------------------------------------------------------------------
  // Operators
  //----------------------------------------------------------------------------
  AutoArrayPtr& operator=(AutoArrayPtr& other) throw();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T& operator[](uint64 const i) const throw();

  //----------------------------------------------------------------------------
  // Member functions
  //----------------------------------------------------------------------------
  T* Get() const throw();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T* Release() throw();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Reset(T* ptr = 0) throw();

private:
  //----------------------------------------------------------------------------
  // Member variables
  //----------------------------------------------------------------------------
  T* ptr_; //!< \brief The managed array of objects.
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
