//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_core_AutoArrayPtr_inline_h_
#define dlm_core_AutoArrayPtr_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/core/AutoArrayPtr.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
//! \brief The default constructor.
//! @param ptr The pointer to be managed.
template <typename T>
inline AutoArrayPtr<T>::AutoArrayPtr(T* ptr) throw()
  : ptr_(ptr)
{}

//------------------------------------------------------------------------------
//! \brief The copy constructor; ownership of the managed pointer is
//! transferred from the other object.
//! @param other The object the managed pointer should be retrieved from.
template <typename T>
inline AutoArrayPtr<T>::AutoArrayPtr(AutoArrayPtr& other) throw()
  : ptr_(other.Release())
{}

//==============================================================================
// Destructor
//------------------------------------------------------------------------------
//! \brief The destructor frees the memory associated with the managed pointer
//! if the pointer is not first retrieved by calling #Release.
template <typename T>
inline AutoArrayPtr<T>::~AutoArrayPtr()
{
  delete[] ptr_;
}

//==============================================================================
// Operators
//------------------------------------------------------------------------------
//! \brief The copy assignment operator; ownership of the managed pointer is
//! transferred from the other object.
//! @param other The object the managed pointer should be retrieved from.
//! @return A \b reference to the newly created object.
template <typename T>
inline AutoArrayPtr<T>& AutoArrayPtr<T>::operator=(AutoArrayPtr& other) throw()
{
  Reset(other.Release());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
//! \brief Pointer subscript operator.
//! @param i The subscript index.
//! @return A \b reference to the object at the specified index.
//!
//! This operator allows this class to be used just like an ordinary pointer.
//!
//! \sa Get.
template <typename T>
inline T& AutoArrayPtr<T>::operator[](uint64 const i) const throw()
{
  return ptr_[i];
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
//! \brief Returns the pointer that is held by `*this`.
//! @return The pointer held by `*this`.
//! \sa #operator[].
template <typename T>
inline T* AutoArrayPtr<T>::Get() const throw()
{
  return ptr_;
}

//------------------------------------------------------------------------------
//! \brief Relinquish control of the managed pointer.
//! @return The managed pointer.
//! \note Calling this function does not free the memory associated with the
//! managed pointer. To free the memory, use #Reset instead.
//! \sa Reset.
template <typename T>
inline T* AutoArrayPtr<T>::Release() throw()
{
  T* ptr = ptr_;

  ptr_ = 0;

  return ptr;
}

//------------------------------------------------------------------------------
//! \brief Set the managed pointer to the given pointer.
//! @param ptr The new pointer to be managed.
//! \note Calling this function frees the memory associated with the managed
//! pointer. To just have control of the managed pointer relinquished call
//! #Release instead.
//! \sa Release.
template <typename T>
inline void AutoArrayPtr<T>::Reset(T* ptr) throw()
{
  if (ptr != ptr_)
  {
    delete[] ptr_;

    ptr_ = ptr;
  }
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
