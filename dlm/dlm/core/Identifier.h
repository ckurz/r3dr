//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_core_Identifier_h_
#define dlm_core_Identifier_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/core/Types.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
//! \brief Typed encapsulation of subscript indices.
//!
//! This class provides rudimentary type safety for the various index types of
//! the Scene data structure.
// Identifier template class
//------------------------------------------------------------------------------
template <typename T>
class Identifier
{
public:
  //----------------------------------------------------------------------------
  // Constructors
  //----------------------------------------------------------------------------
  explicit Identifier(uint64 const index = Invalid);

  //----------------------------------------------------------------------------
  // Operators
  //----------------------------------------------------------------------------
  Identifier& operator=(uint64 const index);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool operator==(Identifier const& other) const;
  bool operator!=(Identifier const& other) const;
  bool operator< (Identifier const& other) const;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool operator==(uint64 const index) const;
  bool operator!=(uint64 const index) const;
  bool operator< (uint64 const index) const;

  //----------------------------------------------------------------------------
  // Accessors
  //----------------------------------------------------------------------------
  uint64 Get() const;

private:
  //----------------------------------------------------------------------------
  // Member variables
  //----------------------------------------------------------------------------
  uint64 index_; //! \brief The encapsulated subscript index.
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
