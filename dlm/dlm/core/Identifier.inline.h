//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_core_Identifier_inline_h_
#define dlm_core_Identifier_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/core/Identifier.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Constructor
//------------------------------------------------------------------------------
//! \brief The default constructor.
//! @param index The desired subscript index.
//! Creates an Identifier object from the given subscript index.
template <typename T>
inline Identifier<T>::Identifier(uint64 const index)
  : index_(index)
{}

//==============================================================================
// Operators
//------------------------------------------------------------------------------
//! \brief Assignment operator for subscript indices.
//! @param index The new subscript index to assign.
//! @return A reference to `*this`.
template <typename T>
inline Identifier<T>& Identifier<T>::operator=(uint64 const index)
{
  index_ = index;

  return *this;
}

//------------------------------------------------------------------------------
//! \brief Equality comparison operator for Identifiers of the same type.
//! @param other The other Identifier.
//! @return The result of the comparison.
template <typename T>
inline bool Identifier<T>::operator==(Identifier const& other) const
{
  return index_ == other.index_;
}

//------------------------------------------------------------------------------
//! \brief Inequality comparison operator for Identifiers of the same type.
//! @param other The other Identifier.
//! @return The result of the comparison.
template <typename T>
inline bool Identifier<T>::operator!=(Identifier const& other) const
{
  return index_ != other.index_;
}

//------------------------------------------------------------------------------
//! \brief Less than comparison operator for Identifiers of the same type.
//! @param other The other Identifier.
//! @return The result of the comparison.
template <typename T>
inline bool Identifier<T>::operator<(Identifier const& other) const
{
  return index_ < other.index_;
}

//------------------------------------------------------------------------------
//! \brief Equality comparison operator for plain subscript indices.
//! @param index The subscript index.
//! @return The result of the comparison.
template <typename T>
inline bool Identifier<T>::operator==(uint64 const index) const
{
  return index_ == index;
}

//------------------------------------------------------------------------------
//! \brief Inequality comparison operator for plain subscript indices.
//! @param index The subscript index.
//! @return The result of the comparison.
template <typename T>
inline bool Identifier<T>::operator!=(uint64 const index) const
{
  return index_ != index;
}

//------------------------------------------------------------------------------
//! \brief Less than comparison operator for plain subscript indices.
//! @param index The subscript index.
//! @return The result of the comparison.
template <typename T>
inline bool Identifier<T>::operator<(uint64 const index) const
{
  return index_ < index;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
//! \brief Get the encapsulated subscript index.
//! @return The encapsulated subscript index.
template <typename T>
inline uint64 Identifier<T>::Get() const
{
  return index_;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
