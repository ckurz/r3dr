//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/core/Exception.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Exception class
//------------------------------------------------------------------------------
// Constructors
//------------------------------------------------------------------------------
//! \brief The default constructor.
//! @param file The name of the file the exception originated from.
//! @param line The line in the file the exception originated from.
Exception::Exception(char const* file, int32 const line) throw()
  : file_(file)
  , line_(line)
{}

//==============================================================================
// Destructor
//------------------------------------------------------------------------------
//! \brief The destructor; for virtual inheritance.
Exception::~Exception() throw()
{}

//==============================================================================
// Accessors
//------------------------------------------------------------------------------
//! \brief Returns the name of the file the exception originated from.
//! @return The name of the file the exception originated from.
char const* Exception::File() const throw()
{
  return file_;
}

//------------------------------------------------------------------------------
//! \brief Returns the line in the file the exception originated from.
//! @return The line in the file the exception originated from.
int32 Exception::Line() const throw()
{
  return line_;
}

//==============================================================================
// DomainError exception class
//------------------------------------------------------------------------------
// Constructors
//------------------------------------------------------------------------------
//! \brief The default constructor.
//! @param file The name of the file the exception originated from.
//! @param line The line in the file the exception originated from.
//! @param description A description for the exception.
DomainError::DomainError(
  char        const* file,
  int32       const  line,
  std::string const& description)
  : Exception        (file, line )
  , std::domain_error(description)
{}

//==============================================================================
// Destructor
//------------------------------------------------------------------------------
//! \brief The destructor; for virtual inheritance.
DomainError::~DomainError() throw()
{}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
//! \brief Provides the exception type string: `dlm::DomainError`.
//! @return The exception type string.
char const* DomainError::Type() const throw()
{
  return "dlm::DomainError";
}

//==============================================================================
// OutOfRange exception class
//------------------------------------------------------------------------------
// Constructors
//------------------------------------------------------------------------------
//! \brief The default constructor.
//! @param file The name of the file the exception originated from.
//! @param line The line in the file the exception originated from.
//! @param description A description for the exception.
OutOfRange::OutOfRange(
  char        const* file,
  int32       const  line,
  std::string const& description)
  : Exception        (file, line )
  , std::out_of_range(description)
{}

//==============================================================================
// Destructor
//------------------------------------------------------------------------------
//! \brief The destructor; for virtual inheritance.
OutOfRange::~OutOfRange() throw()
{}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
//! \brief Provides the exception type string: `dlm::OutOfRange`.
//! @return The exception type string.
char const* OutOfRange::Type() const throw()
{
  return "dlm::OutOfRange";
}

//==============================================================================
// RuntimeError exception class
//------------------------------------------------------------------------------
// Constructors
//------------------------------------------------------------------------------
//! \brief The default constructor.
//! @param file The name of the file the exception originated from.
//! @param line The line in the file the exception originated from.
//! @param description A description for the exception.
RuntimeError::RuntimeError(
  char        const* file,
  int32       const  line,
  std::string const& description)
  : Exception         (file, line )
  , std::runtime_error(description)
{}

//==============================================================================
// Destructor
//------------------------------------------------------------------------------
//! \brief The destructor; for virtual inheritance.
RuntimeError::~RuntimeError() throw()
{}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
//! \brief Provides the exception type string: `dlm::RuntimeError`.
//! @return The exception type string.
char const* RuntimeError::Type() const throw()
{
  return "dlm::RuntimeError";
}

//==============================================================================
// SizeMismatch exception class
//------------------------------------------------------------------------------
// Constructors
//------------------------------------------------------------------------------
//! \brief The default constructor.
//! @param file The name of the file the exception originated from.
//! @param line The line in the file the exception originated from.
//! @param description A description for the exception.
SizeMismatch::SizeMismatch(
  char        const* file,
  int32       const  line,
  std::string const& description)
  : RuntimeError(file, line, description)
{}

//==============================================================================
// Destructor
//------------------------------------------------------------------------------
//! \brief The destructor; for virtual inheritance.
SizeMismatch::~SizeMismatch() throw()
{}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
//! \brief Provides the exception type string: `dlm::SizeMismatch`.
//! @return The exception type string.
char const* SizeMismatch::Type() const throw()
{
  return "dlm::SizeMismatch";
}

//==============================================================================
// DegenerateSet exception class
//------------------------------------------------------------------------------
// Constructors
//------------------------------------------------------------------------------
//! \brief The default constructor.
//! @param file The name of the file the exception originated from.
//! @param line The line in the file the exception originated from.
//! @param description A description for the exception.
DegenerateSet::DegenerateSet(
  char        const* file,
  int32       const  line,
  std::string const& description)
  : RuntimeError(file, line, description)
{}

//==============================================================================
// Destructor
//------------------------------------------------------------------------------
//! \brief The destructor; for virtual inheritance.
DegenerateSet::~DegenerateSet() throw()
{}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
//! \brief Provides the exception type string: `dlm::DegenerateSet`.
//! @return The exception type string.
char const* DegenerateSet::Type() const throw()
{
  return "dlm::DegenerateSet";
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
