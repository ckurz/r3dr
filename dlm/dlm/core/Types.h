//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_core_Types_h_
#define dlm_core_Types_h_

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Type definitions
//------------------------------------------------------------------------------
// Signed integer types
//------------------------------------------------------------------------------
//! @name Signed integer types
//!
//! @{
typedef char          int8;  //!< \brief  8-bit signed integer type.
typedef short int     int16; //!< \brief 16-bit signed integer type.
typedef int           int32; //!< \brief 32-bit signed integer type.
typedef long long int int64; //!< \brief 64-bit signed integer type.
//! @}

//------------------------------------------------------------------------------
// Unsigned integer types
//------------------------------------------------------------------------------
//! @name Unsigned integer types
//!
//! @{
typedef unsigned char          uint8;  //!< \brief  8-bit unsigned integer type.
typedef unsigned short int     uint16; //!< \brief 16-bit unsigned integer type.
typedef unsigned int           uint32; //!< \brief 32-bit unsigned integer type.
typedef unsigned long long int uint64; //!< \brief 64-bit unsigned integer type.
//! @}

//------------------------------------------------------------------------------
// Floating point types
//------------------------------------------------------------------------------
//! @name Floating point types
//!
//! @{
typedef float  float32; //!< \brief 32-bit floating point type.
typedef double float64; //!< \brief 64-bit floating point type.
//! @}

//==============================================================================
// Default floating point type
//------------------------------------------------------------------------------
typedef float64 FloatType; //!< \brief The default floating point type.

//==============================================================================
// Default epsilon value for comparison with FloatType values to 0
//------------------------------------------------------------------------------
//! \brief Epsilon value for floating point comparison to zero.
static FloatType const Epsilon = static_cast<FloatType>(1e-6);

//==============================================================================
// Maximum size of uint32
//------------------------------------------------------------------------------
//! \brief The maximum value of .
static uint64 const UInt32Max = static_cast<uint32>(-1);

//==============================================================================
// uint64 value to specify invalid contents
//------------------------------------------------------------------------------
static uint64 const Invalid = static_cast<uint64>(-1);

//==============================================================================
// Sanity checks
//------------------------------------------------------------------------------
namespace
{
static_assert(sizeof(int8) == 1, "Type int8 has incorrect size.");
static_assert(sizeof(int16) == 2, "Type int16 has incorrect size.");
static_assert(sizeof(int32) == 4, "Type int32 has incorrect size.");
static_assert(sizeof(int64) == 8, "Type int64 has incorrect size.");
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static_assert(sizeof(uint8) == 1, "Type uint8 has incorrect size.");
static_assert(sizeof(uint16) == 2, "Type uint16 has incorrect size.");
static_assert(sizeof(uint32) == 4, "Type uint32 has incorrect size.");
static_assert(sizeof(uint64) == 8, "Type uint64 has incorrect size.");
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static_assert(sizeof(float32) == 4, "Type float32 has incorrect size.");
static_assert(sizeof(float64) == 8, "Type float64 has incorrect size.");
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
