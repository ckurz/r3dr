//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_core_ArrayObjectTable_h_
#define dlm_core_ArrayObjectTable_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include <vector>
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Types.h"
#include "dlm/core/Exception.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

template <typename T>
class ArrayObjectTable;

template <bool isConst, typename IsTrue, typename IsFalse>
struct Choose;

template <typename IsTrue, typename IsFalse>
struct Choose<true, IsTrue, IsFalse>
{
  typedef IsTrue type;
};

template <typename IsTrue, typename IsFalse>
struct Choose<false, IsTrue, IsFalse>
{
  typedef IsFalse type;
};

//==============================================================================
//! \brief AOT class
// ArrayObjectTable template class
//------------------------------------------------------------------------------
template <typename T>
class ArrayObjectTable
{
public:
  //----------------------------------------------------------------------------
  // Friends
  //----------------------------------------------------------------------------
  friend class BinarySerializer;   //!< \brief For serialization.
  friend class BinaryDeserializer; //!< \brief For deserialization.

  //----------------------------------------------------------------------------
  // Nested
  //----------------------------------------------------------------------------
  //! \brief STL-compliant iterator class.
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <bool isConst = false>
  class Iterator
  {
  public:
    //--------------------------------------------------------------------------
    // Nested
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    typedef std::forward_iterator_tag iterator_category;

    typedef T value_type;

    typedef std::ptrdiff_t difference_type;

    typedef typename Choose<isConst, T const&, T&>::type reference;
    typedef typename Choose<isConst, T const*, T*>::type pointer;

    typedef typename Choose<isConst,
      dlm::ArrayObjectTable<T> const*,
      dlm::ArrayObjectTable<T>*>::type ObjectPointer;

    //--------------------------------------------------------------------------
    // Friends
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    friend class Iterator<true>;

    //--------------------------------------------------------------------------
    // Constructors
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Iterator(
      ObjectPointer object = 0,
      uint64 const index   = 0);

    Iterator(Iterator<false> const& other);

    //--------------------------------------------------------------------------
    // Operators
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    reference operator*() const;

    pointer operator->() const;

    Iterator& operator++()
    {
      uint64 const range = object_->Range();

      while (index_ < range)
      {
        ++index_;

        if (index_ == range || object_->Active(index_))
        {
          break;
        }
      }

      return *this;
    }

    Iterator  operator++(int)
    {
      auto tmp(*this);

      this->operator++();

      return tmp; 
    }

    // Implementing the comparison operators apparently requires the use of the
    // Barton-Nachman trick, thus putting the implementation in this header.
    // [http://en.wikipedia.org/wiki/Barton%E2%80%93Nackman_trick]
    //!
    friend bool operator==(
      Iterator const& a,
      Iterator const& b)
    {
      DLM_ASSERT(a.object_ == b.object_, DLM_DOMAIN_ERROR);

      return a.index_ == b.index_;
    }
    
    friend bool operator!=(
      Iterator const& a,
      Iterator const& b)
    {
      return !(a == b);
    }

    //--------------------------------------------------------------------------
    // Accessors/Mutators
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    uint64 Index() const;

  private:
    ObjectPointer object_;

    uint64 index_;
  };

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  typedef Iterator<>           iterator; //!< \brief STL-compliant definition.
  typedef Iterator<true> const_iterator; //!< \brief STL-compliant definition.

  //----------------------------------------------------------------------------
  // Constructors
  //----------------------------------------------------------------------------
  ArrayObjectTable();

  //----------------------------------------------------------------------------
  // Operators
  //----------------------------------------------------------------------------
  T const& operator()(uint64 const i) const;
  T&       operator()(uint64 const i);

  //----------------------------------------------------------------------------
  // Member functions
  //----------------------------------------------------------------------------
  bool IsCompact() const;
  bool IsEmpty  () const;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint64 Capacity() const;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Reserve(uint64 const n);
  void Expand (uint64 const n);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint64 Push(T const& element);
  uint64 PushBack(T const& element);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool Active (uint64 const i) const;
  void Release(uint64 const i);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Clear();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Remove dead elements from the vector. All indices pointing to elements in
  // this vector have to be updated with the returned vector.
  // If compress is true, the internal vector will be resized.
  std::vector<uint64> Purge(bool const compress = false);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint64 FirstValidIndex() const;

  //----------------------------------------------------------------------------
  // Accessors/Mutators
  //----------------------------------------------------------------------------
  uint64 Range() const;
  uint64 Size () const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T const& At(uint64 const i) const;
  T&       At(uint64 const i);

  //----------------------------------------------------------------------------
  // STL iterator interface
  //----------------------------------------------------------------------------
  //! @name Iterators
  //!
  //! @{
  const_iterator cbegin() const;
  const_iterator  begin() const;
  iterator        begin();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  const_iterator cend() const;
  const_iterator  end() const;
  iterator        end();
  //! @}

private:
  //----------------------------------------------------------------------------
  // Nested
  //----------------------------------------------------------------------------
  //! \brief The encapsulation for the objects used in ArrayObjectTable.
  class Object
  {
  public:
    //--------------------------------------------------------------------------
    // Friends
    //--------------------------------------------------------------------------
    friend class BinarySerializer;   //!< \brief For serialization.
    friend class BinaryDeserializer; //!< \brief For deserialization.

    //--------------------------------------------------------------------------
    // Constructors
    //--------------------------------------------------------------------------
    Object(T const& element = T());

    //--------------------------------------------------------------------------
    // Accessors/Mutators
    //--------------------------------------------------------------------------
    T const& Contents() const;
    T&       Contents();
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    int64  State() const;
    int64& State();

    //--------------------------------------------------------------------------
    // Member functions
    //--------------------------------------------------------------------------
    bool IsActive() const;

  private:
    //--------------------------------------------------------------------------
    // Member variables
    //--------------------------------------------------------------------------
    T contents_; //!< \brief The contents of the object.
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    int64 state_; //!< \brief The state of the object.
  };

  //----------------------------------------------------------------------------
  // Member functions
  //----------------------------------------------------------------------------
  uint64 Activate();
  uint64 New     ();

  //----------------------------------------------------------------------------
  // Member variables
  //----------------------------------------------------------------------------
  std::vector<Object> vector_;

  int64 inactive_; //index of the next inactive element
                   // >= 0   ->  valid inactive element
                   //   -1   ->     no inactive element

  uint64 size_;  //number of active elements
  uint64 range_; //total storage available

  bool compact_; //false after Release, true again after Purge or Clear
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
