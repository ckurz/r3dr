//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/estimator/ProjectiveCameraEstimator.h"

//------------------------------------------------------------------------------
#include "dlm/entity/ProjectiveCamera.h"
#include "dlm/entity/ProjectiveCamera.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/svd/SVD.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/adapter/EigenAdapter.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/normalizer/Normalizer.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/DynamicVector.h"
#include "dlm/math/DynamicMatrix.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.inline.h"
#include "dlm/math/Matrix.inline.h"
#include "dlm/math/DynamicVector.inline.h"
#include "dlm/math/DynamicMatrix.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/DynamicVectorFunctions.h"
#include "dlm/math/DynamicMatrixFunctions.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
ProjectiveCameraEstimator::ProjectiveCameraEstimator(
  std::vector<Correspondence> const& correspondences,
  bool                        const  normalizeSpacePoints)
  : correspondences_(correspondences)
{
  auto normalization = NormalizeFeaturePoints(normalizeSpacePoints);

  normalization2D_ = normalization.first;
  normalization3D_ = normalization.second;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
ProjectiveCameraEstimator::Result ProjectiveCameraEstimator::Estimate(
  FloatType const threshold)
{
  Result result = Run(threshold);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Perform denormalization of the estimated projective camera.
  result.first =
    MF::Inverse(normalization2D_) * result.first * normalization3D_;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//==============================================================================
// Virtual member functions
//------------------------------------------------------------------------------
uint64 ProjectiveCameraEstimator::NumberOfSamples() const
{
  return correspondences_.size();
}

//------------------------------------------------------------------------------
uint64 ProjectiveCameraEstimator::NumberOfMSSSamples() const
{
  return 6;
}

//------------------------------------------------------------------------------
std::vector<ProjectiveCameraEstimator::Model> ProjectiveCameraEstimator::Create(
  std::set<uint32> const& selection) const
{
  uint64 const samples = selection.size();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DLM_ASSERT(samples >= NumberOfMSSSamples(),DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DMatrixd matrix(samples * 2, 12);

  {
    uint64 i = 0;

    for (auto it = selection.cbegin(); it != selection.cend(); ++it, ++i)
    {
      Correspondence const& correspondence = correspondences_.at(*it);

      Vector2d const& featurePoint = correspondence.first;
      Vector3d const&  objectPoint = correspondence.second;

      uint64 base = i * 2;

      matrix.At(base,  4) = -objectPoint.X();
      matrix.At(base,  5) = -objectPoint.Y();
      matrix.At(base,  6) = -objectPoint.Z();
      matrix.At(base,  7) = -1.0;

      matrix.At(base,  8) =  featurePoint.Y() * objectPoint.X();
      matrix.At(base,  9) =  featurePoint.Y() * objectPoint.Y();
      matrix.At(base, 10) =  featurePoint.Y() * objectPoint.Z();
      matrix.At(base, 11) =  featurePoint.Y();

      ++base;

      matrix.At(base,  0) =  objectPoint.X();
      matrix.At(base,  1) =  objectPoint.Y();
      matrix.At(base,  2) =  objectPoint.Z();
      matrix.At(base,  3) =  1.0;

      matrix.At(base,  8) = -featurePoint.X() * objectPoint.X();
      matrix.At(base,  9) = -featurePoint.X() * objectPoint.Y();
      matrix.At(base, 10) = -featurePoint.X() * objectPoint.Z();
      matrix.At(base, 11) = -featurePoint.X();
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  SVD::Result svd = SVD::Decompose(matrix, SVD::Omit, SVD::Full);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  ProjectiveCamera result;
  
  result = VF::AsRows<3, 4>(DVF::Convert<12>(DMF::GetCol(svd.V, 11)));

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  std::vector<Model> resultVector;
  
  resultVector.push_back(result);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return resultVector;
}

//------------------------------------------------------------------------------
ProjectiveCameraEstimator::Evaluation ProjectiveCameraEstimator::Evaluate(
  Model const& model, FloatType const threshold) const
{
  Evaluation result;

  result.second = FloatType(0.0);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint32 index = 0;

  for (auto it = correspondences_.cbegin(); it != correspondences_.cend(); ++it)
  {
    // Calculate the error, i.e., the geometric distance in the image plane.
    Vector2d const error = model(it->second) - it->first;

    FloatType const sqrDistance = error.Dot(error);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // The distance is calculated using normalized coordinates, so
    // denormalization has to be performed.
    FloatType const scale = normalization2D_.At(0, 0);

    FloatType const denormalizedSqrDistance = sqrDistance / (scale * scale);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // Determine whether a correspondence is an inlier or outlier.
    if (denormalizedSqrDistance < threshold)
    {
      result.first.insert(index);
      result.second += denormalizedSqrDistance; // Provides MSAC behavior.
    }
    else
    {
      result.second += threshold;
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ++index;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
std::pair<Matrix3d, Matrix4d> ProjectiveCameraEstimator::NormalizeFeaturePoints(
  bool const normalizeSpacePoints)
{
  std::vector<Vector2d> points2D;
  std::vector<Vector3d> points3D;

  for (auto it = correspondences_.cbegin(); it != correspondences_.cend(); ++it)
  {
    points2D.push_back(it->first );
    points3D.push_back(it->second);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Matrix3d normalization2D = Normalizer::Calculate(points2D);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Matrix4d normalization3D;
  
  if (normalizeSpacePoints)
  {
    normalization3D = Normalizer::Calculate(points3D);
  }
  else
  {
    MF::MakeIdentity(normalization3D);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (auto it = correspondences_.begin(); it != correspondences_.end(); ++it)
  {
    it->first  = VF::Inhomogenize(normalization2D * VF::Homogenize(it->first ));
    it->second = VF::Inhomogenize(normalization3D * VF::Homogenize(it->second));
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return std::make_pair(normalization2D, normalization3D);
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
