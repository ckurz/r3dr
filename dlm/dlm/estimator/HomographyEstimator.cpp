//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/estimator/HomographyEstimator.h"

//------------------------------------------------------------------------------
#include "dlm/svd/SVD.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/adapter/EigenAdapter.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/normalizer/Normalizer.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/DynamicVector.h"
#include "dlm/math/DynamicMatrix.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.inline.h"
#include "dlm/math/Matrix.inline.h"
#include "dlm/math/DynamicVector.inline.h"
#include "dlm/math/DynamicMatrix.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/VectorFunctions.h"
#include "dlm/math/MatrixFunctions.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/DynamicVectorFunctions.h"
#include "dlm/math/DynamicMatrixFunctions.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
HomographyEstimator::HomographyEstimator(
  std::vector<Correspondence> const& correspondences)
  : correspondences_(correspondences)
{
  NormalizeFeaturePoints();
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
HomographyEstimator::Result HomographyEstimator::Estimate(
  FloatType const threshold)
{
  Result result = Run(threshold);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Perform denormalization of the estimated homography.
  result.first = MF::Inverse(normalization1_) * result.first * normalization0_;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//==============================================================================
// Virtual member functions
//------------------------------------------------------------------------------
uint64 HomographyEstimator::NumberOfSamples() const
{
  return correspondences_.size();
}

//------------------------------------------------------------------------------
uint64 HomographyEstimator::NumberOfMSSSamples() const
{
  return 4;
}

//------------------------------------------------------------------------------
std::vector<HomographyEstimator::Model> HomographyEstimator::Create(
  std::set<uint32> const& selection) const
{
  uint64 const samples = selection.size();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DLM_ASSERT(samples >= NumberOfMSSSamples(),DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DMatrixd matrix(samples * 2, 9);

  {
    uint64 i = 0;

    for (auto it = selection.cbegin(); it != selection.cend(); ++it, ++i)
    {
      Correspondence const& correspondence = correspondences_.at(*it);

      Vector2d const& featurePoint0 = correspondence.first;
      Vector2d const& featurePoint1 = correspondence.second;

      uint64 base = i * 2;

      matrix.At(base, 3) = -featurePoint0.X();
      matrix.At(base, 4) = -featurePoint0.Y();
      matrix.At(base, 5) = -1.0;

      matrix.At(base, 6) =  featurePoint1.Y() * featurePoint0.X();
      matrix.At(base, 7) =  featurePoint1.Y() * featurePoint0.Y();
      matrix.At(base, 8) =  featurePoint1.Y();

      ++base;

      matrix.At(base, 0) =  featurePoint0.X();
      matrix.At(base, 1) =  featurePoint0.Y();
      matrix.At(base, 2) =  1.0;

      matrix.At(base, 6) = -featurePoint1.X() * featurePoint0.X();
      matrix.At(base, 7) = -featurePoint1.X() * featurePoint0.Y();
      matrix.At(base, 8) = -featurePoint1.X();
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  SVD::Result svd = SVD::Decompose(matrix, SVD::Omit, SVD::Full);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Model result = VF::AsRows<3, 3>(DVF::Convert<9>(DMF::GetCol(svd.V, 9)));

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  std::vector<Model> resultVector;
  
  resultVector.push_back(result);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return resultVector;
}

//------------------------------------------------------------------------------
HomographyEstimator::Evaluation HomographyEstimator::Evaluate(
  Model const& model, FloatType const threshold) const
{
  Evaluation result;

  result.second = FloatType(0.0);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Model const inverse = MF::Inverse(model);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint32 index = 0;

  for (auto it = correspondences_.cbegin(); it != correspondences_.cend(); ++it)
  {
    // Calculate the error, i.e., the geometric distance in the image plane.
    // For homography estimation, the symmetric square distance is calculated.
    // The homography transfers image positions from the first image to the
    // second one. In order to get the position in the first image from
    // positions in the second image, the homography has to be inverted.
    Vector2d const tPos0 = VF::Inhomogenize(inverse * VF::Homogenize(it->second));
    Vector2d const tPos1 = VF::Inhomogenize(model   * VF::Homogenize(it->first ));
    
    Vector2d const error0 = tPos0 - it->first;
    Vector2d const error1 = tPos1 - it->second;

    FloatType sqrDistance0 = error0.Dot(error0);
    FloatType sqrDistance1 = error1.Dot(error1);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // The distance is calculated using normalized coordinates, so
    // denormalization has to be performed.
    FloatType const scale0 = normalization0_.At(0, 0);
    FloatType const scale1 = normalization1_.At(0, 0);

    FloatType const denormalizedSqrDistance0 = sqrDistance0 / (scale0 * scale0);
    FloatType const denormalizedSqrDistance1 = sqrDistance1 / (scale1 * scale1);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // Determine whether a correspondence is an inlier or outlier.
    // Checking both points is more restrictive than just summing up.
    if (denormalizedSqrDistance0 < threshold &&
        denormalizedSqrDistance1 < threshold)
    {
      result.first.insert(index);

      // The factor of 0.5 takes into account that the error should be composed
      // from the two symmetric components and not exceed the threshold.
      result.second += denormalizedSqrDistance0 * static_cast<FloatType>(0.5);
      result.second += denormalizedSqrDistance1 * static_cast<FloatType>(0.5);
    }
    else
    {
      result.second += threshold;
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ++index;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
void HomographyEstimator::NormalizeFeaturePoints()
{
  std::vector<Vector2d> points0;
  std::vector<Vector2d> points1;

  for (auto it = correspondences_.cbegin(); it != correspondences_.cend(); ++it)
  {
    points0.push_back(it->first );
    points1.push_back(it->second);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Matrix3d normalization0 = Normalizer::Calculate(points0);
  Matrix3d normalization1 = Normalizer::Calculate(points1);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (auto it = correspondences_.begin(); it != correspondences_.end(); ++it)
  {
    it->first  = VF::Inhomogenize(normalization0 * VF::Homogenize(it->first ));
    it->second = VF::Inhomogenize(normalization1 * VF::Homogenize(it->second));
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Store normalization matrices.
  normalization0_ = normalization0;
  normalization1_ = normalization1;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
