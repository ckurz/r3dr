//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_estimator_ProjectiveCameraEstimator_h_
#define dlm_estimator_ProjectiveCameraEstimator_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/ransac/RANSAC.h"
#include "dlm/ransac/RANSAC.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.h"
#include "dlm/math/Matrix.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Types.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <vector>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Forward declarations
//------------------------------------------------------------------------------
class ProjectiveCamera;

//==============================================================================
// ProjectiveCameraEstimator class
//------------------------------------------------------------------------------
class ProjectiveCameraEstimator : public RANSAC<ProjectiveCamera>
{
public:
  //============================================================================
  // Nested
  //----------------------------------------------------------------------------
  typedef ProjectiveCamera Model;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  typedef std::pair<Vector2d, Vector3d> Correspondence;

  //============================================================================
  // Constructors
  //----------------------------------------------------------------------------
  ProjectiveCameraEstimator(
    std::vector<Correspondence> const& correspondences,
    bool                        const  normalizeSpacePoints = true);

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  Result Estimate(FloatType const threshold);

  //============================================================================
  // Virtual member functions
  //----------------------------------------------------------------------------
  virtual uint64 NumberOfSamples   () const;
  virtual uint64 NumberOfMSSSamples() const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  virtual std::vector<Model> Create(std::set<uint32> const& selection) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  virtual Evaluation Evaluate(
    Model const& model, FloatType const threshold) const;

private:
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  std::pair<Matrix3d, Matrix4d> NormalizeFeaturePoints(
    bool const normalizeSpacePoints);

  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  Matrix3d normalization2D_;
  Matrix4d normalization3D_;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  std::vector<Correspondence> correspondences_;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
