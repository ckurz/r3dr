//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Authors: Aref Ariyapour
//          Christian Kurz
//          Valentin Savenko

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/estimator/FundamentalMatrixEstimator.h"

//------------------------------------------------------------------------------
#include "dlm/svd/SVD.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/adapter/EigenAdapter.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/normalizer/Normalizer.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/DynamicVector.h"
#include "dlm/math/DynamicMatrix.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.inline.h"
#include "dlm/math/Matrix.inline.h"
#include "dlm/math/DynamicVector.inline.h"
#include "dlm/math/DynamicMatrix.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/VectorFunctions.h"
#include "dlm/math/MatrixFunctions.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/DynamicVectorFunctions.h"
#include "dlm/math/DynamicMatrixFunctions.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Unnamed namespace
//------------------------------------------------------------------------------
namespace {

//==============================================================================
// Local functions
//------------------------------------------------------------------------------
// Calculate the coefficients of the polynomial of degree three in lambda
// resulting from (a0 + lambda * a1) * (b0 + lambda * b1) * (c0 + lambda * c1).
// The resulting coefficients are sorted in descending order in the degree of
// lambda for the return value: [lambda^3, lambda^2, lambda^1, lambda^0].
inline Vector4d Coefficients(
  FloatType const a0, FloatType const b0, FloatType const c0,
  FloatType const a1, FloatType const b1, FloatType const c1)
{
  Vector4d result;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  result.At(0) = a1 * b1 * c1;
  result.At(1) = a1 * b1 * c0 + a1 * b0 * c1 + a0 * b1 * c1;
  result.At(2) = a1 * b0 * c0 + a0 * b1 * c0 + a0 * b0 * c1;
  result.At(3) = a0 * b0 * c0;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
// Calculate the coefficients of the polynomial of degree three in lambda
// resulting from det(m0 + lambda * m1) = 0.
// The resulting coefficients are sorted in descending order in the degree of
// lambda for the return value: [lambda^3, lambda^2, lambda^1, lambda^0].
inline Vector4d Coefficients(Matrix3d const& m0, Matrix3d const& m1)
{
  Vector4d result;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // The determinant is calculated using the Rule of Sarrus:
  //                  [a b c]
  // For a matrix A = [d e f], det(A) = aei + bfg + cdh - gec - hfa - idb.
  //                  [g h i]
  // Every addition/subtraction accounts for one of the six products.
  result += Coefficients(m0.At(0, 0), m0.At(1, 1), m0.At(2, 2),
                         m1.At(0, 0), m1.At(1, 1), m1.At(2, 2));

  result += Coefficients(m0.At(0, 1), m0.At(1, 2), m0.At(2, 0),
                         m1.At(0, 1), m1.At(1, 2), m1.At(2, 0));

  result += Coefficients(m0.At(0, 2), m0.At(1, 0), m0.At(2, 1),
                         m1.At(0, 2), m1.At(1, 0), m1.At(2, 1));

  result -= Coefficients(m0.At(2, 0), m0.At(1, 1), m0.At(0, 2),
                         m1.At(2, 0), m1.At(1, 1), m1.At(0, 2));

  result -= Coefficients(m0.At(2, 1), m0.At(1, 2), m0.At(0, 0),
                         m1.At(2, 1), m1.At(1, 2), m1.At(0, 0));

  result -= Coefficients(m0.At(2, 2), m0.At(1, 0), m0.At(0, 1),
                         m1.At(2, 2), m1.At(1, 0), m1.At(0, 1));

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
// Calculate the roots of the polynomial of degree three of x specified by the
// coefficients provided. The coefficients are sorted in descending order of the
// degree of x, i.e., for coefficients [a b c d] the polynomial is given by
// a * x^3 + b * x^2 + c * x + d = 0.
// The coefficient a must not be zero.
// Apparently, the implementation in the old library had a slightly higher
// numerical precision, but this implementation is easier to understand.
inline Vector<3, std::complex<FloatType> > Roots(Vector4d const& coefficients)
{
  FloatType const epsilon = static_cast<FloatType>(1e-6);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Define some values to make the following code easier to read.
  FloatType const N1  = static_cast<FloatType>( 1.0);
  FloatType const N2  = static_cast<FloatType>( 2.0);
  FloatType const N3  = static_cast<FloatType>( 3.0);
  FloatType const N4  = static_cast<FloatType>( 4.0);
  FloatType const N9  = static_cast<FloatType>( 9.0);
  FloatType const N18 = static_cast<FloatType>(18.0);
  FloatType const N27 = static_cast<FloatType>(27.0);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FloatType const a = coefficients.At(0);
  FloatType const b = coefficients.At(1);
  FloatType const c = coefficients.At(2);
  FloatType const d = coefficients.At(3);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DLM_ASSERT(std::abs(a) > epsilon, DLM_DEGENERATE_SET);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FloatType const abc = a * b * c;

  FloatType const a2  = a * a;
  FloatType const b2  = b * b;
  FloatType const c2  = c * c;
  FloatType const d2  = d * d;

  FloatType const b3  = b * b2;
  FloatType const c3  = c * c2;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // The discriminant delta decides whether there are
  // delta >  0: three distinct real roots
  // delta == 0: a multiple root and all roots are real
  // delta <  0: one real root and two nonreal complex conjugate roots.
  std::complex<FloatType> const delta =
    N18 * abc * d - N4 * b3 * d + b2 * c2 - N4 * a * c3 - N27 * a2 * d2;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Calculate helper variables.
  std::complex<FloatType> const delta0 = b2 - N3 * a * c;
  std::complex<FloatType> const delta1 = N2 * b3 - N9 * abc + N27 * a2 * d;

  std::complex<FloatType> C =
    std::pow((delta1 + std::sqrt(-N27 * a2 * delta)) / N2, N1 / N3);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Treat the case where delta is non-zero but delta0 is zero.
  if (std::abs(delta) > epsilon && std::abs(delta0) < epsilon)
  {
    C = std::pow(delta1, N1 / N3);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Vector<3, std::complex<FloatType> > result;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Treat the case where delta and delta0 are zero.
  if (std::abs(delta) < epsilon && std::abs(delta0) < epsilon)
  {
    result.At(0) = result.At(1) = result.At(2) =
      std::complex<FloatType>(-b / (N3 * a));
  }
  else
  {
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // Specify the three cubic roots of unity.
    auto const u0 = std::complex<FloatType>( N1);
    auto const u1 = std::complex<FloatType>(-N1,  sqrt(N3)) / N2;
    auto const u2 = std::complex<FloatType>(-N1, -sqrt(N3)) / N2;

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // Calculate the roots of the polynomial.
    result.At(0) = -(b + u0 * C + delta0 / (u0 * C)) / (N3 * a);
    result.At(1) = -(b + u1 * C + delta0 / (u1 * C)) / (N3 * a);
    result.At(2) = -(b + u2 * C + delta0 / (u2 * C)) / (N3 * a);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//==============================================================================
// Unnamed namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
FundamentalMatrixEstimator::FundamentalMatrixEstimator(
  std::vector<Correspondence> const& correspondences)
  : correspondences_(correspondences)
{
  NormalizeFeaturePoints();
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
FundamentalMatrixEstimator::Result FundamentalMatrixEstimator::Estimate(
  FloatType const threshold)
{
  Result result = Run(threshold);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Perform denormalization of the estimated fundamental matrix.
  result.first = MF::Transpose(normalization1_) * result.first * normalization0_;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//==============================================================================
// Virtual member functions
//------------------------------------------------------------------------------
uint64 FundamentalMatrixEstimator::NumberOfSamples() const
{
  return correspondences_.size();
}

//------------------------------------------------------------------------------
uint64 FundamentalMatrixEstimator::NumberOfMSSSamples() const
{
  return 7;
}

//------------------------------------------------------------------------------
std::vector<FundamentalMatrixEstimator::Model>
  FundamentalMatrixEstimator::Create(
    std::set<uint32> const& selection) const
{
  uint64 const samples = selection.size();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DLM_ASSERT(samples >= NumberOfMSSSamples(),DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DMatrixd matrix(samples, 9);

  {
    uint64 i = 0;

    for (auto it = selection.cbegin(); it != selection.cend(); ++it, ++i)
    {
      Correspondence const& correspondence = correspondences_.at(*it);

      Vector2d const& featurePoint0 = correspondence.first;
      Vector2d const& featurePoint1 = correspondence.second;

      // Given a number of correspondences, build an equation system of the form
      // A * f = 0, where f is the vector containing the entries of F given by
      // the relation x'^T * F * x = 0.
      matrix.At(i, 0) = featurePoint1.X() * featurePoint0.X();
      matrix.At(i, 1) = featurePoint1.X() * featurePoint0.Y();
      matrix.At(i, 2) = featurePoint1.X();
      matrix.At(i, 3) = featurePoint1.Y() * featurePoint0.X();
      matrix.At(i, 4) = featurePoint1.Y() * featurePoint0.Y();
      matrix.At(i, 5) = featurePoint1.Y();
      matrix.At(i, 6) =                     featurePoint0.X();
      matrix.At(i, 7) =                     featurePoint0.Y();
      matrix.At(i, 8) = static_cast<FloatType>(1.0);
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  SVD::Result const svd = SVD::Decompose(matrix, SVD::Omit, SVD::Full);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Extract the generators of the right null-space of the matrix and put them
  // into matrices that span the 2-dimensional space of the solution obtained
  // via the SVD above, given as: lambda * F1 + (1 - lambda) * F2.
  Model F1 = VF::AsRows<3, 3>(DVF::Convert<9>(DMF::GetCol(svd.V, 8)));
  Model F2 = VF::AsRows<3, 3>(DVF::Convert<9>(DMF::GetCol(svd.V, 7)));

  Model difference = F1 - F2;

  // The constraint det(F) = 0 can be used to rewrite the above statement to:
  // det(F2 + lambda * (F1 - F2)) = 0, which leads to a cubic polynomial in
  // lambda, for which the coefficients can be determined easily.
  Vector4d const coefficients = Coefficients(F2, difference);

  // There exists a general solution for the roots of a cubic polynomial.
  Vector<3, std::complex<FloatType> > roots = Roots(coefficients);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  std::vector<Model> resultVector;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < 3; ++i)
  {
    // Ensure the the root has no imaginary component.
    if (std::abs(roots.At(i).imag()) < static_cast<FloatType>(1e-6))
    {
      // Apply the lambda to select the correct entries from the 2-dimensional
      // space of solutions.
      resultVector.push_back(F2 + difference * roots.At(i).real());
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return resultVector;
}

//------------------------------------------------------------------------------
FundamentalMatrixEstimator::Evaluation FundamentalMatrixEstimator::Evaluate(
  Model const& model, FloatType const threshold) const
{
  Evaluation result;

  result.second = FloatType(0.0);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Determine the transpose of the fundamental matrix in order to not only
  // get epipolar lines for the second image, but also for the first.
  Model const transpose = MF::Transpose(model);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint32 index = 0;

  for (auto it = correspondences_.cbegin(); it != correspondences_.cend(); ++it)
  {
    // Calculate the error, i.e., the symmetric epipolar square distance.
    // First, homogenize the points to simplify computation.
    Vector3d const fp0 = VF::Homogenize(it->first );
    Vector3d const fp1 = VF::Homogenize(it->second);

    // Second, determine the epipolar lines.
    Vector3d const l0 = transpose * fp1;
    Vector3d const l1 = model     * fp0;
    
    // Determine the raw distance.
    // In contrast to the homography estimation, there is only one value, since
    // the fundamental matrix is only transposed for the inverse direction.
    FloatType const    rawDistance = fp0.Dot(l0);
    FloatType const sqrRawDistance = rawDistance * rawDistance;

    // Get the actual square distances.
    FloatType const denominator0 = l0.At(0) * l0.At(0) + l0.At(1) * l0.At(1); 
    FloatType const denominator1 = l1.At(0) * l1.At(0) + l1.At(1) * l1.At(1);

    FloatType const sqrDistance0 = sqrRawDistance / denominator0;
    FloatType const sqrDistance1 = sqrRawDistance / denominator1;

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // The distance is calculated using normalized coordinates, so
    // denormalization has to be performed.
    FloatType const scale0 = normalization0_.At(0, 0);
    FloatType const scale1 = normalization1_.At(0, 0);

    FloatType const denormalizedSqrDistance0 = sqrDistance0 / (scale0 * scale0);
    FloatType const denormalizedSqrDistance1 = sqrDistance1 / (scale1 * scale1);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // Determine whether a correspondence is an inlier or outlier.
    // Checking both points is more restrictive than just summing up.
    if (denormalizedSqrDistance0 < threshold &&
        denormalizedSqrDistance1 < threshold)
    {
      result.first.insert(index);

      // The factor of 0.5 takes into account that the error should be composed
      // from the two symmetric components and not exceed the threshold.
      result.second += denormalizedSqrDistance0 * static_cast<FloatType>(0.5);
      result.second += denormalizedSqrDistance1 * static_cast<FloatType>(0.5);
    }
    else
    {
      result.second += threshold;
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ++index;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
void FundamentalMatrixEstimator::NormalizeFeaturePoints()
{
  std::vector<Vector2d> points0;
  std::vector<Vector2d> points1;

  points0.reserve(correspondences_.size());
  points1.reserve(correspondences_.size());
  
  for (auto it = correspondences_.cbegin(); it != correspondences_.cend(); ++it)
  {
    points0.push_back(it->first );
    points1.push_back(it->second);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Matrix3d normalization0 = Normalizer::Calculate(points0);
  Matrix3d normalization1 = Normalizer::Calculate(points1);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (auto it = correspondences_.begin(); it != correspondences_.end(); ++it)
  {
    it->first  = VF::Inhomogenize(normalization0 * VF::Homogenize(it->first ));
    it->second = VF::Inhomogenize(normalization1 * VF::Homogenize(it->second));
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Store normalization matrices.
  normalization0_ = normalization0;
  normalization1_ = normalization1;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
