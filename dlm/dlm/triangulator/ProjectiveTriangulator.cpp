//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/triangulator/ProjectiveTriangulator.h"

//------------------------------------------------------------------------------
#include "dlm/svd/SVD.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/MatrixFunctions.h"
#include "dlm/math/VectorFunctions.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/DynamicMatrixFunctions.h"
#include "dlm/math/DynamicVectorFunctions.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.inline.h"
#include "dlm/math/Matrix.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Types.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Unnamed namespace
//------------------------------------------------------------------------------
namespace
{

//==============================================================================
// Local functions
//------------------------------------------------------------------------------
// Calculate a normalization matrix from a single point. There is no
// translational component, obviously, and for the scale care has to be taken
// in order to avoid a division by 0. If the point is close to the origin,
// the identity matrix is returned.
Matrix3f Normalization(Vector2f const& point)
{
  Matrix3f result;

  MF::MakeIdentity(result);

  FloatType const distance = point.Magnitude();

  // Don't perform the scaling if the point is too close to the origin.
  if (distance > Epsilon)
  {
    FloatType const scale = static_cast<FloatType>(M_SQRT2) / distance;

    result(0, 0) = scale;
    result(1, 1) = scale;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//==============================================================================
// Unnamed namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
Vector3f ProjectiveTriangulator::Calculate(
  Matrix3x4f const& projection0,
  Matrix3x4f const& projection1,
  Vector2f   const& point0,
  Vector2f   const& point1)
{
  Matrix3f const normalization0 = Normalization(point0);
  Matrix3f const normalization1 = Normalization(point1);

  Matrix3x4f const p0 = normalization0 * projection0; 
  Matrix3x4f const p1 = normalization1 * projection1; 
  
  Vector2f const x0 = VF::Inhomogenize(normalization0 * VF::Homogenize(point0));
  Vector2f const x1 = VF::Inhomogenize(normalization1 * VF::Homogenize(point1));

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Split the projection matrices into rows for easier use.
  Vector4f const p0r0 = MF::GetRow(p0, 0);
  Vector4f const p0r1 = MF::GetRow(p0, 1);
  Vector4f const p0r2 = MF::GetRow(p0, 2);

  Vector4f const p1r0 = MF::GetRow(p1, 0);
  Vector4f const p1r1 = MF::GetRow(p1, 1);
  Vector4f const p1r2 = MF::GetRow(p1, 2);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Construct the matrix of constraints.
  Matrix4f matrix;

  MF::SetRow(matrix, 0, p0r2 * x0.X() - p0r0);
  MF::SetRow(matrix, 1, p0r2 * x0.Y() - p0r1);
  MF::SetRow(matrix, 2, p1r2 * x1.X() - p1r0);
  MF::SetRow(matrix, 3, p1r2 * x1.Y() - p1r1);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Obtain the solution via SVD.
  Vector3f result;

  {
    SVD::Result const svd =
      SVD::Decompose(MF::Convert(matrix), SVD::Omit, SVD::Full);
  
    result = VF::Inhomogenize(DVF::Convert<4>(DMF::GetCol(svd.V, 3)));
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
