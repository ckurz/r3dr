//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_entity_ProjectiveCamera_inline_h_
#define dlm_entity_ProjectiveCamera_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/entity/ProjectiveCamera.h"

//------------------------------------------------------------------------------
#include "dlm/math/Vector.inline.h"
#include "dlm/math/Matrix.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/VectorFunctions.h"
#include "dlm/math/MatrixFunctions.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
//! \brief The default constructor.
//! @param matrix An arbitrary 3-by-4 FloatType matrix.
//!
//! A default-constructed projective camera will be a zero matrix with three
//! ones on the main diagonal.
inline ProjectiveCamera::ProjectiveCamera(BaseClass const& matrix)
  : BaseClass(matrix)
{}

//==============================================================================
// Operators
//------------------------------------------------------------------------------
//! \brief Projection of a 3D point onto the image plane.
//! @param op The 3D point to be projected.
//! @return The corresponding 2D point in the image plane.
//!
//! This function converts the given 3D object point to homogeneous coordinates,
//! multiplies the projection matrix with the resulting vector, and then
//! performs perspective division.
inline Vector2f ProjectiveCamera::operator()(Vector3f const& op) const
{
  return VF::Inhomogenize(BaseClass::operator*(VF::Homogenize(op)));
}

//------------------------------------------------------------------------------
//! \brief Projection of a 3D point given in homogeneous coordinates onto the
//! image plane.
//! @param op The 3D point in homogeneous coordinates to be projected. The w-
//! component has to be one; otherwise a RuntimeError will be raised.
//! @return The corresponding 2D point in the image plane.
//!
//! This function multiplies the projection matrix with the given 3D point in
//! homogeneous coordinates and then performs perspective division.
//!
//! \deprecated This functionality has been declared deprecated in dlm 1.6.0. It
//! may be removed in a future release.
inline Vector2f ProjectiveCamera::operator()(Vector4f const& op) const
{
  DLM_ASSERT(op.W() == static_cast<FloatType>(1.0), DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return VF::Inhomogenize(BaseClass::operator*(op));
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
