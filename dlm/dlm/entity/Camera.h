//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_entity_Camera_h_
#define dlm_entity_Camera_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/math/Vector.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
//! \brief The base class for CameraRepresentation and camera model used
//! throughout the library for metric cameras.
//!
//! This class models the intrinsic camera parameters \link focalLength_ focal
//! length \f$f\f$\endlink, \link principalPoint_ principal point
//! \f$\left(p_x,p_y\right)^{\top}\f$\endlink, \link pixelAspectRatio_ pixel
//! aspect ratio \f$\eta\f$\endlink, and \link skew_ skew \f$s\f$\endlink of
//! the pinhole camera model.
//!
//! The mathematical description of the transfer from the canonical image plane
//! to the desired virtual image plane is given by
//!
/*! \f[
 *      \begin{pmatrix}
 *        x' \\
 *        y' \\
 *        1
 *      \end{pmatrix}
 *      =
 *      \begin{bmatrix}
 *        f & s        & p_x \\
 *        0 & f * \eta & p_y \\
 *        0 & 0        & 1   \\
 *      \end{bmatrix}
 *      \begin{pmatrix}
 *        x \\
 *        y \\
 *        1
 *      \end{pmatrix}
 *      \quad,
 *  \f]
 */
//!
//! which is implemented by \link Camera::operator()(Vector2f const&) const
//! operator()\endlink for data of type Vector2f.
//!
//! If desired, points may also be transferred from the virtual image plane to
//! the canonical image plane by using #InverseRelation.
//! 
//! \attention Contrary to versions before dlm 1.5.0, the current projection
//! model uses the \b principal \b point and not the \b principal \b point
//! \b offset.
//! 
//! \note Starting with dlm 1.5.0, the parameters are no longer required to be
//! given in millimeters; instead the paramters may be provided directly in the
//! appropriate dimensions.
//! \note Support for the direct application of
//! \link Camera::operator()(Vector2f const&) const operator()\endlink to data
//! of types Vector3f and Vector4f has been removed with the release of dlm
//! 1.5.0.
//!
// Camera class
//------------------------------------------------------------------------------
class Camera
{
public:
  //----------------------------------------------------------------------------
  // Friends
  //----------------------------------------------------------------------------
  friend class BinarySerializer;   //!< For serialization.
  friend class BinaryDeserializer; //!< For deserialization.

  //----------------------------------------------------------------------------
  // Constructors
  //----------------------------------------------------------------------------
  Camera(
    Vector2f const& principalPoint   = Vector2f(),
    FloatType const focalLength      = static_cast<FloatType>(0.0),
    FloatType const pixelAspectRatio = static_cast<FloatType>(1.0),
    FloatType const skew             = static_cast<FloatType>(0.0));

  //----------------------------------------------------------------------------
  // Operators
  //----------------------------------------------------------------------------
  Vector2f operator()(Vector2f const& point) const;

  //----------------------------------------------------------------------------
  // Member functions
  //----------------------------------------------------------------------------
  Vector2f InverseRelation(Vector2f const& point) const;

  //----------------------------------------------------------------------------
  // Accessors/Mutators
  //----------------------------------------------------------------------------
  //! @name Principal point
  //!
  //! @{
  Vector2f const& PrincipalPoint() const;
  Vector2f&       PrincipalPoint();
  //! @}

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //! @name Focal length
  //!
  //! @{
  FloatType  FocalLength() const;
  FloatType& FocalLength();
  //! @}

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //! @name Pixel aspect ratio
  //!
  //! @{
  FloatType  PixelAspectRatio() const;
  FloatType& PixelAspectRatio();  
  //! @}

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //! @name Skew
  //!
  //! @{
  FloatType  Skew() const;
  FloatType& Skew();
  //! @}

private:
  //----------------------------------------------------------------------------
  // Member variables
  //----------------------------------------------------------------------------
  //! \brief The principal point of the modelled camera.
  //!
  //! The principal point \f$\left(p_x,p_y\right)^{\top}\f$ is the point of
  //! intersection between the optical axis of the camera and the image plane.
  //! If the image sensor of the camera was perfectly placed, the principal
  //! point would coincide with the center of the image.
  //!
  //! \note Setting the principal point to the position of the image center is
  //! usually a good starting point for optimization.
  Vector2f principalPoint_;

  //! \brief The focal length of the modelled camera.
  //!
  //! The focal length \f$f\f$ is the distance of the camera center from the
  //! image plane.
  //!
  //! \note A reasonable estimate of the focal length is crucial for
  //! optimization.
  FloatType focalLength_;

  //! \brief The pixel aspect ratio of the modelled camera.
  //!
  //! The pixel aspect ratio \f$\eta\f$ accounts for differences in the pixel
  //! pitch in x- and y-direction.
  //!
  //! \note This parameter is a replacement for the implicit encoding of the
  //! pixel aspect ratio in the pixel size of the Image class before dlm 1.5.0.
  FloatType pixelAspectRatio_;

  //! \brief The skew of the modelled camera.
  //!
  //! The skew parameter \f$s\f$ models a skewing of the camera coordinate
  //! system. This means that the axes of the coordinate system are not
  //! perpendicular to one another. With a digital camera, this should only
  //! occur when taking an image  of an image.
  //!
  //! \note The skew should be set to zero and not optimized for most
  //! applications.
  FloatType skew_;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
