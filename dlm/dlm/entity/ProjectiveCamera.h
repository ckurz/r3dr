//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_entity_ProjectiveCamera_h_
#define dlm_entity_ProjectiveCamera_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/math/Vector.h"
#include "dlm/math/Matrix.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/MatrixFunctions.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
//! \brief The base class for ProjectiveCameraRepresentation and camera model
//! used throughout the library for projective cameras (projection matrices).
//!
//! This class models the projective case of the pinhole camera model, a 3-by-4
//! projection matrix:
//!
/*! \f[
 *      \begin{pmatrix}
 *        x' \\
 *        y' \\
 *        1
 *      \end{pmatrix}
 *      \simeq
 *      \begin{bmatrix}
 *        p_{0,0} & p_{0,1} & p_{0,2} & p_{0,3} \\
 *        p_{1,0} & p_{1,1} & p_{1,2} & p_{1,3} \\
 *        p_{2,0} & p_{2,1} & p_{2,2} & p_{2,3} 
 *      \end{bmatrix}
 *      \begin{pmatrix}
 *        X \\
 *        Y \\
 *        Z \\
 *        1
 *      \end{pmatrix}
 *      \quad,
 *  \f]
 */
//!
//! which is implemented by 
//! \link ProjectiveCamera::operator()(Vector3f const&) const operator()\endlink
//! for data of type Vector3f and Vector4f.
//!
//! \note In the projective case, the projection matrix models the whole
//! camera model, which for the metric case consists of Camera, Distortion, and
//! Transformation.
//!
//! \attention The projective camera model does not take image distortion into
//! account, which may lead to difficulties while performing a projective
//! reconstruction when the images exhibit a high degree of distortion.
//! 
// ProjectiveCamera class
//------------------------------------------------------------------------------
class ProjectiveCamera : public Matrix3x4f
{
public:
  //----------------------------------------------------------------------------
  // Friends
  //----------------------------------------------------------------------------
  friend class BinarySerializer;   //!< \brief For serialization.
  friend class BinaryDeserializer; //!< \brief For deserialization.

  //----------------------------------------------------------------------------
  // Nested
  //----------------------------------------------------------------------------
  typedef Matrix3x4f BaseClass; //!< \brief For convenience.
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //! \brief Allow direct assignment.
  using BaseClass::operator=;

  //----------------------------------------------------------------------------
  // Constructors
  //----------------------------------------------------------------------------
  //! \note The default constructor has to use the deprecated function
  //! MatrixFunctions::Identity3x4f() to create the default parameter due to a
  //! compiler bug in Visual Studio 2012. This bug causes the compiler to emit
  //! an error C2783 when a function template is referred to by its fully
  //! qualified name in a default argument of a member function. See
  //! [https://connect.microsoft.com/VisualStudio/feedback/details/759770/error-c2783-in-function-default-argument].
  ProjectiveCamera(BaseClass const& matrix = MF::Identity3x4f());

  //----------------------------------------------------------------------------
  // Operators
  //----------------------------------------------------------------------------
  Vector2f operator()(Vector3f const& op) const;
  Vector2f operator()(Vector4f const& op) const;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
