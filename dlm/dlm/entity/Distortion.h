//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_entity_Distortion_h_
#define dlm_entity_Distortion_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/math/Vector.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
//! \brief The base class for DistortionRepresentation and distortion model used
//! throughout the library for metric cameras.
//!
// Distortion class
//------------------------------------------------------------------------------
class Distortion
{
public:
  //----------------------------------------------------------------------------
  // Friends
  //----------------------------------------------------------------------------
  friend class BinarySerializer;   //!< \brief For serialization.
  friend class BinaryDeserializer; //!< \brief For deserialization.

  //----------------------------------------------------------------------------
  // Constructors
  //----------------------------------------------------------------------------
  Distortion(
    Vector6f const& k = Vector6f(),
    Vector2f const& p = Vector2f());

  //----------------------------------------------------------------------------
  // Operators
  //----------------------------------------------------------------------------
  Vector2f operator()(Vector2f const& op) const;
  Vector2f operator()(Vector3f const& op) const;
  Vector2f operator()(Vector4f        op) const;

  //----------------------------------------------------------------------------
  // Member functions
  //----------------------------------------------------------------------------
  Vector2f Undistort(
    Vector2f  const& point,
    FloatType const  accuracy      = Epsilon,
    uint64    const  maxIterations = 10) const;

  //----------------------------------------------------------------------------
  // Accessors/Mutators
  //----------------------------------------------------------------------------
  //! @name Radial distortion coefficients
  //!
  //! @{
  Vector6f const& K() const;
  Vector6f&       K();
  //! @}

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //! @name Tangential distortion coefficients
  //!
  //! @{
  Vector2f const& P() const;
  Vector2f&       P();
  //! @}

private:
  //----------------------------------------------------------------------------
  // Member variables
  //----------------------------------------------------------------------------
  //! \brief The radial distortion coefficients.
  Vector6f k_;

  //! \brief The tangential distortion coefficients.
  Vector2f p_;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
