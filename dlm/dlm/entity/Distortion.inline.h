//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_entity_Distortion_inline_h_
#define dlm_entity_Distortion_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/entity/Distortion.h"

//------------------------------------------------------------------------------
#include "dlm/core/Exception.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Matrix.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.inline.h"
#include "dlm/math/Matrix.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/VectorFunctions.h"
#include "dlm/math/MatrixFunctions.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Implementation namespace
//------------------------------------------------------------------------------
namespace Implementation
{

//==============================================================================
// Local functions
//------------------------------------------------------------------------------
inline Matrix2f CalculateJacobian(
  Vector2f const point,
  Vector6f const k,
  Vector2f const p)
{
  static FloatType const one  = static_cast<FloatType>(1.0);
  static FloatType const two  = static_cast<FloatType>(2.0);
  static FloatType const four = static_cast<FloatType>(4.0);
  static FloatType const six  = static_cast<FloatType>(6.0);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FloatType x = point.X();
  FloatType y = point.Y();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FloatType const r2 = point.Dot(point);
  FloatType const r4 = r2 * r2;
  FloatType const r6 = r4 * r2;
  
  FloatType const r  = std::sqrt(r2);
  FloatType const r3 = r2 * r;
  FloatType const r5 = r4 * r;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FloatType const rDiffX = x / r;
  FloatType const rDiffY = y / r;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FloatType const u = one + k(0) * r2 + k(1) * r4 + k(2) * r6;
  FloatType const v = one + k(3) * r2 + k(4) * r4 + k(5) * r6;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FloatType const uDiffR = two * k(0) * r  + four * k(1) * r3 + six * k(2) * r5;
  FloatType const vDiffR = two * k(3) * r  + four * k(4) * r3 + six * k(5) * r5;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FloatType const v2Inv = one / (v * v);
  
  FloatType const diffBase = uDiffR * v - u * vDiffR;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Matrix2f J;
  
  // Radial component.
  J(0, 0)  = (u * v + x * diffBase * rDiffX) * v2Inv;
  J(0, 1)  = (        x * diffBase * rDiffY) * v2Inv;
  J(1, 0)  = (        y * diffBase * rDiffX) * v2Inv;
  J(1, 1)  = (u * v + y * diffBase * rDiffY) * v2Inv;

  // Tangential component.
  J(0, 0) += two * p(0) * y + p(1) * (two * r * rDiffX + four * x);
  J(0, 1) += two * p(0) * x + p(1) * (two * r * rDiffY           );
  J(1, 0) += two * p(1) * y + p(0) * (two * r * rDiffX           );
  J(1, 1) += two * p(1) * x + p(0) * (two * r * rDiffY + four * y);  

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return J;
}

//==============================================================================
// Implementation namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
//! \brief The default constructor.
//! @param k The \link k_ radial distortion coefficients\endlink.
//! @param p The \link p_ tangential distortion coefficients\endlink.
inline Distortion::Distortion(
  Vector6f const& k,
  Vector2f const& p)
  : k_(k)
  , p_(p)
{}

//==============================================================================
// Operators
//------------------------------------------------------------------------------
inline Vector2f Distortion::operator()(Vector2f const& op) const
{
  static FloatType const N1 = static_cast<FloatType>(1.0);
  static FloatType const N2 = static_cast<FloatType>(2.0);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FloatType const r2 = op.Dot(op);
  FloatType const r4 = r2 * r2;
  FloatType const r6 = r4 * r2;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Calculate the radial distortion.
  FloatType const radial =
    (N1 + k_(0) * r2 + k_(1) * r4 + k_(2) * r6) /
    (N1 + k_(3) * r2 + k_(4) * r4 + k_(5) * r6);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Calculate the tangential distortion.
  Vector2f tangential = p_ * N2 * op.X() * op.Y();
  tangential         += Vector2f(p_.Y(), p_.X()) * r2;
  tangential         += Vector2f(p_.Y() * op.X() * op.X(),
                                 p_.X() * op.Y() * op.Y()) * N2;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return op * radial + tangential;
}

//------------------------------------------------------------------------------
  // Applies distortion to a point assumed to be in the camera frame. For 3- and
  // 4-vectors, perspective division is performed. The w-component of a 4-vector
  // has to be 1.0. After distortion, the point is then in the camera coordinate
  // system and still in [mm].
inline Vector2f Distortion::operator()(Vector3f const& op) const
{
  return operator()(VF::Inhomogenize(op)); // Perspective division.
}

//------------------------------------------------------------------------------
inline Vector2f Distortion::operator()(Vector4f p) const
{
  DLM_ASSERT(p.W() == static_cast<FloatType>(1.0), DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  p /= p.Z(); //perspective division

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return operator()(Vector2f(p.X(), p.Y()));
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
inline Vector2f Distortion::Undistort(
  Vector2f  const& point,
  FloatType const  accuracy,
  uint64    const  maxIterations) const
{
  // Determine the initial value (see the 3DE4 LPDK).
  Vector2f initial = point * static_cast<FloatType>(2.0) - operator()(point);
  Vector2f result  = initial;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint64 iteration = 0;
  
  do
  {
    DLM_ASSERT(iteration < maxIterations, DLM_RUNTIME_ERROR);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ++iteration;
  
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    initial = result; // Store the old value for the termination criterion.
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Matrix2f const J = Implementation::CalculateJacobian(initial, k_, p_);
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    result -= MF::Inverse(J) * (operator()(initial) - point);
  }
  while ((result - initial).Magnitude() >= accuracy);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//==============================================================================
// Accessors/Mutators
//------------------------------------------------------------------------------
//! \brief The accessor method for the \link p_ radial distortion
//! coefficients\endlink.
//! @return A \b const \b reference to the currently set \link p_ radial
//! distortion coefficients\endlink.
inline Vector6f const& Distortion::K() const
{
  return k_;
}

//------------------------------------------------------------------------------
//! \brief The mutator method for the \link p_ radial distortion
//! coefficients\endlink.
//! @return A \b reference to the currently set \link p_ radial distortion
//! coefficients\endlink.
inline Vector6f& Distortion::K()
{
  return k_;
}

//------------------------------------------------------------------------------
//! \brief The accessor method for the \link p_ tangential distortion
//! coefficients\endlink.
//! @return A \b const \b reference to the currently set \link p_ tangential
//! distortion coefficients\endlink.
inline Vector2f const& Distortion::P() const
{
  return p_;
}

//------------------------------------------------------------------------------
//! \brief The mutator method for the \link p_ tangential distortion
//! coefficients\endlink.
//! @return A \b reference to the currently set \link p_ tangential distortion
//! coefficients\endlink.
inline Vector2f& Distortion::P()
{
  return p_;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
