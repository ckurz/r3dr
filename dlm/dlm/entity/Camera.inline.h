//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_entity_Camera_inline_h_
#define dlm_entity_Camera_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/entity/Camera.h"

//------------------------------------------------------------------------------
#include "dlm/math/Vector.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/VectorFunctions.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
//! \brief The default constructor.
//! @param principalPoint The coordinates of the \link principalPoint_ principal
//! point\endlink.
//! @param focalLength The \link focalLength_ focal length\endlink.
//! @param pixelAspectRatio The \link pixelAspectRatio_ pixel aspect
//! ratio\endlink.
//! @param skew The \link skew_ skew\endlink.
inline Camera::Camera(
  Vector2f const& principalPoint,
  FloatType const focalLength,
  FloatType const pixelAspectRatio,
  FloatType const skew)
  : principalPoint_  (principalPoint  )
  , focalLength_     (focalLength     )
  , pixelAspectRatio_(pixelAspectRatio)
  , skew_            (skew            )
{}

//==============================================================================
// Operators
//------------------------------------------------------------------------------
//! \brief Application of the camera parameters.
//!
//! @param point The point \f$\left(x,y\right)^{\top}\f$ to which the camera
//!              parameters should be applied.
//! @return The point \f$\left(x',y'\right)^{\top}\f$ with all camera parameters
//!         applied.
//!
//! This function applies the parameters of the camera model in the following
//! fashion:
/*! \f{eqnarray*}{
 *      x' &=&           f * x  +  \hphantom{\eta *} s * y + p_x \\
 *      y' &=& \hphantom{f * x  +}            f * \eta * y + p_y
 *  \f}
 */
//!
//! \sa InverseRelation.
inline Vector2f Camera::operator()(Vector2f const& point) const
{
  FloatType const x = focalLength_ * point.X() + skew_ * point.Y();
  FloatType const y = focalLength_ * pixelAspectRatio_ * point.Y();

  return Vector2f(x, y) + principalPoint_;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
//! \brief Application of the inverse camera parameters.
//! @param point The point \f$\left(x',y'\right)^{\top}\f$ to which the inverse
//!              camera parameters should be applied.
//! @return The point \f$\left(x,y\right)^{\top}\f$ with all inverse camera
//!         parameters applied.
//!
//! This function applies the inverse of the parameters of the camera model in
//! the following fashion:
/*! \f{eqnarray*}{
 *      y &=& \frac{y' - p_y}{f * \eta}\\
 *      x &=& \frac{x' - p_x - s * y}{f}
 *  \f}
 */
//! \sa \link Camera::operator()(Vector2f const&) const operator()\endlink.
inline Vector2f Camera::InverseRelation(Vector2f const& point) const
{
  Vector2f result = point - principalPoint_;

  result.Y() /= focalLength_ * pixelAspectRatio_;

  result.X() -= skew_ * result.Y();
  result.X() /= focalLength_;

  return result;
}

//==============================================================================
// Accessors/Mutators
//------------------------------------------------------------------------------
//! \brief The accessor method for the \link principalPoint_ principal
//! point\endlink.
//! @return A \b const \b reference to the currently set \link principalPoint_
//! principal point\endlink.
inline const Vector2f& Camera::PrincipalPoint() const
{
  return principalPoint_;
}

//------------------------------------------------------------------------------
//! \brief The mutator method for the \link principalPoint_ principal
//! point\endlink.
//! @return A \b reference to the currently set \link principalPoint_
//! principal point\endlink.
inline Vector2f& Camera::PrincipalPoint()
{
  return principalPoint_;
}

//------------------------------------------------------------------------------
//! \brief The accessor method for the \link focalLength_ focal length\endlink.
//! @return The \b value of the currently set \link focalLength_ focal
//! length\endlink.
inline FloatType Camera::FocalLength() const
{
  return focalLength_;
}

//------------------------------------------------------------------------------
//! \brief The mutator method for the \link focalLength_ focal length\endlink.
//! @return A \b reference to the currently set \link focalLength_ focal
//! length\endlink.
inline FloatType& Camera::FocalLength()
{
  return focalLength_;
}

//------------------------------------------------------------------------------
//! \brief The accessor method for the \link pixelAspectRatio_ pixel aspect
//! ratio\endlink.
//! @return The \b value of the currently set \link pixelAspectRatio_ pixel
//! aspect ratio\endlink.
inline FloatType Camera::PixelAspectRatio() const
{
  return pixelAspectRatio_;
}

//------------------------------------------------------------------------------
//! \brief The mutator method for the \link pixelAspectRatio_ pixel aspect
//! ratio\endlink.
//! @return A \b reference to the currently set \link pixelAspectRatio_ pixel
//! aspect ratio\endlink.
inline FloatType& Camera::PixelAspectRatio()
{
  return pixelAspectRatio_;
}

//------------------------------------------------------------------------------
//! \brief The accessor method for the \link skew_ skew\endlink.
//! @return The \b value of the currently set \link skew_ skew\endlink.
inline FloatType Camera::Skew() const
{
  return skew_;
}

//------------------------------------------------------------------------------
//! \brief The mutator method for the \link skew_ skew\endlink.
//! @return A \b reference to the currently set \link skew_ skew\endlink.
inline FloatType& Camera::Skew()
{
  return skew_;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
