//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_entity_Transformation_h_
#define dlm_entity_Transformation_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/core/Types.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.h"
#include "dlm/math/Matrix.h"
#include "dlm/math/Quaternion.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
//! \brief The base class for TransformationRepresentation and transformation
//! used throughout the library for metric scenes.
//!
//! Transformations are used mainly for the specification of the camera position
//! and orientation for metric scenes. In addition, the may be used to formulate
//! constraints and model dependencies by forming transformation sequences.
//!
//! The transformations store an additional \link invert_ inversion flag\endlink
//! that determines if the transformation should transform a point from the
//! superordinate coordinate system to the local coordinate system of the
//! transformation (flag set to \b true) or from the local coordinate system to
//! the superordinate one (flag set to \b false).
//! The corresponding formulas are:
//!
//! \sa TransformationSequence.
//!
// Transformation class
//------------------------------------------------------------------------------
class Transformation
{
public:
  //----------------------------------------------------------------------------
  // Friends
  //----------------------------------------------------------------------------
  friend class BinarySerializer;   //!< \brief For serialization.
  friend class BinaryDeserializer; //!< \brief For deserialization.

  //----------------------------------------------------------------------------
  // Constructors
  //----------------------------------------------------------------------------
  Transformation(
    Quatd const&    rotation    = Quatd(),
    Vector3d const& translation = Vector3f(),
    bool const      invert      = false);

  //----------------------------------------------------------------------------
  // Operators
  //----------------------------------------------------------------------------
  //! @name Transformation chaining
  //!
  //! @{
  Transformation  operator* (Transformation const& other) const;
  Transformation& operator*=(Transformation const& other);
  //! @}

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Vector3f operator*(Vector3f const& vector) const;

  //----------------------------------------------------------------------------
  // Member functions
  //----------------------------------------------------------------------------
  Transformation Inverse() const;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //! @name Transformation matrix generation
  //!
  //! @{
  Matrix4f InverseMatrix() const;
  Matrix4f Matrix() const;
  //! @}

  //----------------------------------------------------------------------------
  // Accessors/Mutators
  //----------------------------------------------------------------------------
  //! @name Rotation
  //!
  //! @{
  Quatd const& Rotation() const;
  Quatd&       Rotation();
  //! @}

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //! @name Translation
  //!
  //! @{
  Vector3f const& Translation() const;
  Vector3f&       Translation();
  //! @}

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //! @name Inversion flag
  //!
  //! @{
  bool  Invert() const;
  bool& Invert();
  //! @}

private:
  //----------------------------------------------------------------------------
  // Member variables
  //----------------------------------------------------------------------------
  //! \brief The rotation component of the transformation.
  Quatd rotation_;

  //! \brief The translation component of the transformation.
  Vector3f translation_;

  //! \brief %Transformation inversion flag.
  //!
  //! This flag determines whether the given rotation and translation should be
  //! used to construct a matrix from the superordinate coordinate system to the
  //! local coordinate system (if the flag is set to \b true) or from the local
  //! coordinate system to the superordinate coordinate system (if the flag is
  //! set to \b false).
  bool invert_;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
