//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_entity_Transformation_inline_h_
#define dlm_entity_Transformation_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/entity/Transformation.h"

//------------------------------------------------------------------------------
#include "dlm/math/VectorFunctions.h"
#include "dlm/math/MatrixFunctions.h"
#include "dlm/math/QuaternionFunctions.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
//! \brief The default constructor.
//! @param rotation The desired \link rotation_ rotation\endlink.
//! @param translation The desired \link translation_ translation\endlink.
//! @param invert The \link invert_ inversion flag\endlink. 
inline Transformation::Transformation(
  Quatd const&    rotation, 
  Vector3f const& translation,
  bool const      invert)
  : rotation_   (rotation   )
  , translation_(translation)
  , invert_     (invert     )
{}

//==============================================================================
// Operators
//------------------------------------------------------------------------------
//! \brief Chains two transformations together by multiplying the current with
//! the another one.
//! \param other The second transformation involved in the chain.
//! \return The new transformation created by the chaining.
//!
//! \attention The state of the \link invert_ inversion flag\endlink for the
//! resulting transformation is the same as for the transformation on the left
//! side of the multiplication sign.
//!
//! \sa \link Transformation::operator*=(Transformation const&) operator*=\endlink.
inline Transformation Transformation::operator*(
  Transformation const& other) const
{
  return Transformation(*this) *= other;
}

//------------------------------------------------------------------------------
//! \brief Chains a transformation to the current one.
//! \param other The second transformation involved in the chain.
//! \return A \b reference the assignee.
//!
//! Depending on the \link invert_ inversion flags\endlink, different operations
//! are performed.
//!
//! \note The \link invert_ inversion flag\endlink is not changed by this
//! operation.
inline Transformation& Transformation::operator*=(
  Transformation const& other)
{
  if (invert_)
  {
    if (other.invert_)
    {
      translation_  = other.rotation_ * translation_;
      translation_ += other.translation_;
      rotation_     = other.rotation_ * rotation_;
    }
    else
    {
      Quatd const quat = other.rotation_.Conjugated();

      translation_ = quat * (translation_ - other.translation_);
      rotation_    = quat *  rotation_;
    }
  }
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  else
  {
    if (other.invert_)
    {
      Quatd const quat = other.rotation_.Conjugated();

      translation_ -= rotation_ * quat * other.translation_;
      rotation_    *= quat;
    }
    else
    {
      translation_ += rotation_ * other.translation_;
      rotation_    *= other.rotation_;
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
//! \brief Transforms the given vector without the explicit construction of the
//! transformation matrix.
//! @param vector The vector to be transformed.
//! @return The transformed vector.
//!
//! The \link invert_ inversion falg\endlink is taken into account during
//! transformation, meaning that the vector will be transformed to the
//! superordinate coordinate system if it is set, and to the local coordinate
//! system if not.
//!
//! \note This function may a offer a performance benefit if only a few vectors
//! need to be transformed by the transformation. Otherwise the construction of
//! the transformation matrix should be worthwhile.
inline Vector3f Transformation::operator*(
  Vector3f const& vector) const
{
  if (invert_)
  {
    return rotation_.Conjugated() * (vector - translation_);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return rotation_ * vector + translation_;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
//! \brief Get the inverted transformation.
//! @return The inverted transformation.
//!
//! This method constructs a new Transformation with the \link invert_ inversion
//! flag\endlink inverted. \link rotation_ Rotation\endlink and
//! \link translation_ translation\endlink are simply copied to the output
//! transformation.
inline Transformation Transformation::Inverse() const
{
  return Transformation(rotation_, translation_, !invert_);
}

//------------------------------------------------------------------------------
//! \brief Get the inverted transformation matrix corresponding to this
//! transformation.
//! @return The 4-by-4 transformation matrix constructed from the stored
//! \link rotation_ rotation\endlink and \link translation_ translation\endlink
//! suitable for projection from the superordinate coordinate system to the
//! local coordinate system described by the transformation.
//!
//! The stored parameters are used to create a matrix of the form
//!
/*! \f[
 *      \begin{bmatrix}
 *        \mathtt{R}^{-1}   & -\mathtt{R}^{-1}\mathbf{t} \\
 *        \mathbf{0}^{\top} & 1                      
 *      \end{bmatrix}
 *      \quad,
 *  \f]
 */
//!
//! with \f$\mathtt{R}\f$ being the 3-by-3 rotation matrix corresponding to the
//! \link rotation_ rotation\endlink and \f$t\f$ the \link translation_
//! translation\endlink.
//!
//! \attention Unlike \link Transformation::Matrix() const Matrix\endlink, this
//! function does not take the \link invert_ inversion flag\endlink into
//! account; the resulting transformation always inverts the transformation
//! parameters during construction.
inline Matrix4f Transformation::InverseMatrix() const
{
  Matrix4d result;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Quatd const quat = rotation_.Conjugated();

  MF::SetSubmatrix(result, 0, 0, QF::Rotation(quat));
  MF::SetCol(result, 3, VF::Homogenize(-(quat * translation_)));

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
//! \brief Get the transformation matrix encoded in this transformation.
//! @return The 4-by-4 transformation matrix constructed from the stored
//! \link rotation_ rotation\endlink and \link translation_ translation\endlink.
//!
//! If the \link invert_ inversion flag\endlink is not set, then the stored
//! parameters are used to create a matrix of the form
//!
/*! \f[
 *      \begin{bmatrix}
 *        \mathtt{R}\hphantom{^{\top}} & \mathbf{t} \\
 *        \mathbf{0}^{\top}            & 1                      
 *      \end{bmatrix}
 *      \quad,
 *  \f]
 */
//!
//! with \f$\mathtt{R}\f$ being the 3-by-3 rotation matrix corresponding to the
//! \link rotation_ rotation\endlink and \f$t\f$ the \link translation_
//! translation\endlink.
//! This matrix is suitable for a transformation from the local coordinate
//! system described by the transformation to the superordinate coordinate
//! system.
//!
//! If the \link invert_ inversion flag\endlink is set, InverseMatrix is called
//! for the construction of the matrix instead, yielding a transformation matrix
//! suitable for the transformation from the superordinate coordinate system to
//! the local coordinate system.
inline Matrix4f Transformation::Matrix() const
{
  if (invert_)
  {
    return InverseMatrix();
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Matrix4d result;

  MF::SetSubmatrix(result, 0, 0, QF::Rotation(rotation_));
  MF::SetCol(result, 3, VF::Homogenize(translation_));

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//==============================================================================
// Accessors/Mutators
//------------------------------------------------------------------------------
//! \brief The accessor method for the \link rotation_ rotation\endlink.
//! @return A \b const \b reference to the currently set \link rotation_
//! rotation\endlink.
inline Quatd const& Transformation::Rotation() const
{
  return rotation_;
}

//------------------------------------------------------------------------------
//! \brief The mutator method for the \link rotation_ rotation\endlink.
//! @return A \b reference to the currently set \link rotation_
//! rotation\endlink.
inline Quatd& Transformation::Rotation()
{
  return rotation_;
}

//------------------------------------------------------------------------------
//! \brief The accessor method for the \link translation_ translation\endlink.
//! @return A \b const \b reference to the currently set \link translation_
//! translation\endlink.
inline Vector3d const& Transformation::Translation() const
{
  return translation_;
}

//------------------------------------------------------------------------------
//! \brief The mutator method for the \link translation_ translation\endlink.
//! @return A \b reference to the currently set \link translation_
//! translation\endlink.
inline Vector3d& Transformation::Translation()
{
  return translation_;
}

//------------------------------------------------------------------------------
//! \brief The accessor method for the \link invert_ inversion flag\endlink.
//! @return The \b value of the \link invert_ inversion flag\endlink.
inline bool Transformation::Invert() const
{
  return invert_;
}

//------------------------------------------------------------------------------
//! \brief The mutator method for the \link invert_ inversion flag\endlink.
//! @return A \b reference to the \link invert_ inversion flag\endlink.
inline bool& Transformation::Invert()
{
  return invert_;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
