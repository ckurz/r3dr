//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_entity_ObjectPoint_inline_h_
#define dlm_entity_ObjectPoint_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/entity/ObjectPoint.h"

//------------------------------------------------------------------------------
#include "dlm/math/Vector.inline.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
//! \brief The default constructor.
//! @param vector The coordinates of the object point.
inline ObjectPoint::ObjectPoint(BaseClass const& vector)
  : BaseClass (vector)
{}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
