//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_entity_Image_h_
#define dlm_entity_Image_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/math/Vector.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
//! \brief The base class for ImageRepresentation.
//!
//! As of dlm 1.5.0, this class only stores the size of the corresponding image.
//!
//! \note Support for the specification of the image origin, the pixel
//! center, absolute and relative coordinates, and all corresponding functions
//! have been removed with dlm 1.5.0.
//! \note The pixel size parameter and all corresponding functions for the
//! conversion between the image and camera coordinate system (pixel to
//! millimeter) have been removed with dlm 1.5.0.
//!
// Image class
//------------------------------------------------------------------------------
class Image
{
public:
  //----------------------------------------------------------------------------
  // Friends
  //----------------------------------------------------------------------------
  friend class BinarySerializer;   //!< \brief For serialization.
  friend class BinaryDeserializer; //!< \brief For deserialization.

  //----------------------------------------------------------------------------
  // Constructors
  //----------------------------------------------------------------------------
  Image(Vector2u const& size = Vector2u());

  //----------------------------------------------------------------------------
  // Accessors/Mutators
  //----------------------------------------------------------------------------
  //! @name Image size
  //!
  //! @{
  Vector2u const& Size() const;
  Vector2u&       Size();
  //! @}

private:
  //----------------------------------------------------------------------------
  // Member variables
  //----------------------------------------------------------------------------
  //! \brief The image size.
  //!
  //! \note Due to the changes in dlm 1.5.0, the image size is no longer
  //! required during optimization.
  Vector2u size_;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
