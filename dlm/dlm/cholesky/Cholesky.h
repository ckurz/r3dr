//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_cholesky_Cholesky_h_
#define dlm_cholesky_Cholesky_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/math/DynamicVector.h"
#include "dlm/math/DynamicMatrix.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Cholesky class
//------------------------------------------------------------------------------
class Cholesky
{
public:
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  // Perform a Cholesky decomposition of matrix and use it to solve the linear
  // system matrix * x = vector. The Eigen3 LLT decomposition is used
  // internally (accuracy/reliability dependent on condition number of matrix).
  static DVectord Solve      (DMatrixd const& matrix, DVectord const& vector);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Perform a Cholesky decomposition of matrix and use it to solve the linear
  // system matrix * x = vector. The Eigen3 LDLT decomposition is used
  // internally (more stable, with pivoting, but slower).
  static DVectord SolveRobust(DMatrixd const& matrix, DVectord const& vector);
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
