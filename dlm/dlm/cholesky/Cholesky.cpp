//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/cholesky/Cholesky.h"

//------------------------------------------------------------------------------
#include "dlm/math/DynamicVector.inline.h"
#include "dlm/math/DynamicMatrix.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/adapter/EigenAdapter.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
DVectord Cholesky::Solve(DMatrixd const& matrix, DVectord const& vector)
{
  typedef Eigen::Matrix<float64, Eigen::Dynamic, Eigen::Dynamic> MatrixType;
  typedef Eigen::Matrix<float64, Eigen::Dynamic, 1             > VectorType;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  MatrixType mCopy = EA::Convert<float64>(matrix);
  VectorType vCopy = EA::Convert<float64>(vector);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  mCopy.llt().solveInPlace(vCopy);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return EA::Convert<float64>(vCopy);
}

//------------------------------------------------------------------------------
DVectord Cholesky::SolveRobust(DMatrixd const& matrix, DVectord const& vector)
{
  typedef Eigen::Matrix<float64, Eigen::Dynamic, Eigen::Dynamic> MatrixType;
  typedef Eigen::Matrix<float64, Eigen::Dynamic, 1             > VectorType;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  MatrixType mCopy = EA::Convert<float64>(matrix);
  VectorType vCopy = EA::Convert<float64>(vector);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  mCopy.ldlt().solveInPlace(vCopy);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return EA::Convert<float64>(vCopy);
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
