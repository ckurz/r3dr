//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_adapter_EigenAdapter_h_
#define dlm_adapter_EigenAdapter_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/math/Vector.h"
#include "dlm/math/Matrix.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/DynamicVector.h"
#include "dlm/math/DynamicMatrix.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/adapter/EigenWrapper.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// EigenAdapter class
//------------------------------------------------------------------------------
class EigenAdapter
{
public:
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  // Conversion of fixed-size vectors and matrices
  //----------------------------------------------------------------------------
  template <typename T, uint32 SIZE, typename U>
  static Eigen::Matrix<T, SIZE, 1> Convert(Vector<SIZE, U> const& vector);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <typename T, uint32 ROWS, uint32 COLS, typename U>
  static Eigen::Matrix<T, ROWS, COLS> Convert(
    Matrix<ROWS, COLS, U> const& matrix);

  //----------------------------------------------------------------------------
  template <typename T, int32 SIZE, typename U>
  static Vector<SIZE, T> Convert(Eigen::Matrix<U, SIZE, 1> const& vector);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <typename T, int32 ROWS, int32 COLS, typename U>
  static Matrix<ROWS, COLS, T> Convert(
    Eigen::Matrix<U, ROWS, COLS> const& matrix);


  //----------------------------------------------------------------------------
  // Conversion of vectors and matrices of arbitrary size
  //----------------------------------------------------------------------------
  template <typename T, typename U>
  static Eigen::Matrix<T, Eigen::Dynamic, 1> Convert(
    DynamicVector<U> const& vector);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <typename T, typename U>
  static Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> Convert(
    DynamicMatrix<U> const& matrix);
    
  //----------------------------------------------------------------------------
  template <typename T, typename U>
  static DynamicVector<T> Convert(
    Eigen::Matrix<U, Eigen::Dynamic, 1> const& vector);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <typename T, typename U>
  static DynamicMatrix<T> Convert(
    Eigen::Matrix<U, Eigen::Dynamic, Eigen::Dynamic> const& matrix);
};

//==============================================================================
// Shorthand
//------------------------------------------------------------------------------
typedef EigenAdapter EA;

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// Inline code
//------------------------------------------------------------------------------
// This file is likely to be included from source files only, and for
// convenience includes the corresponding inline file automatically.
//------------------------------------------------------------------------------
#include "dlm/adapter/EigenAdapter.inline.h"

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_adapter_EigenAdapter_h_
