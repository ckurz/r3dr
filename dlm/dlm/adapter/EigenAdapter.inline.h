//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_adapter_EigenAdapter_inline_h_
#define dlm_adapter_EigenAdapter_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/adapter/EigenAdapter.h"

//------------------------------------------------------------------------------
#include "dlm/math/Vector.inline.h"
#include "dlm/math/Matrix.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/DynamicVector.inline.h"
#include "dlm/math/DynamicMatrix.inline.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
template <typename T, uint32 SIZE, typename U>
inline Eigen::Matrix<T, SIZE, 1> EigenAdapter::Convert(
  Vector<SIZE, U> const& vector)
{
  typedef Eigen::Matrix<T, SIZE, 1> VectorType;
  typedef typename VectorType::Index IndexType;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  VectorType result;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < SIZE; ++i)
  {
    result(static_cast<IndexType>(i)) = static_cast<T>(vector.At(i));
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;  
}

//------------------------------------------------------------------------------
template <typename T, uint32 ROWS, uint32 COLS, typename U>
inline Eigen::Matrix<T, ROWS, COLS> EigenAdapter::Convert(
  Matrix<ROWS, COLS, U> const& matrix)
{
  typedef Eigen::Matrix<T, ROWS, COLS> MatrixType;
  typedef typename MatrixType::Index IndexType;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  MatrixType result;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < ROWS; ++i)
  {
    for (uint32 j = 0; j < COLS; ++j)
    {
      result(static_cast<IndexType>(i), static_cast<IndexType>(j)) =
        static_cast<T>(matrix.At(i, j));
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;  
}

//------------------------------------------------------------------------------
template <typename T, int32 SIZE, typename U>
inline Vector<SIZE, T> EigenAdapter::Convert(
  Eigen::Matrix<U, SIZE, 1> const& vector)
{
  typedef typename Eigen::Matrix<U, SIZE, 1>::Index IndexType;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Vector<SIZE, T> result;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < SIZE; ++i)
  {
    result.At(i) = static_cast<T>(vector(static_cast<IndexType>(i)));
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <typename T, int32 ROWS, int32 COLS, typename U>
inline Matrix<ROWS, COLS, T> EigenAdapter::Convert(
  Eigen::Matrix<U, ROWS, COLS> const& matrix)
{
  typedef typename Eigen::Matrix<U, ROWS, COLS>::Index IndexType;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Matrix<ROWS, COLS, T> result;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (int32 i = 0; i < ROWS; ++i)
  {
    for (int32 j = 0; j < COLS; ++j)
    {
      result.At(i, j) = static_cast<T>(
        matrix(static_cast<IndexType>(i), static_cast<IndexType>(j)));
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <typename T, typename U>
inline Eigen::Matrix<T, Eigen::Dynamic, 1> EigenAdapter::Convert(
    DynamicVector<U> const& vector)
{
  typedef Eigen::Matrix<T, Eigen::Dynamic, 1> VectorType;
  typedef typename VectorType::Index IndexType;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  VectorType result(static_cast<IndexType>(vector.Size()));
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < vector.Size(); ++i)
  {
    result(static_cast<IndexType>(i)) = static_cast<T>(vector.At(i));
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;  
}

//------------------------------------------------------------------------------
template <typename T, typename U>
inline Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> EigenAdapter::Convert(
  DynamicMatrix<U> const& matrix)
{
  typedef Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> MatrixType;
  typedef typename MatrixType::Index IndexType;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  MatrixType result(
    static_cast<IndexType>(matrix.Rows()),
      static_cast<IndexType>(matrix.Cols()));
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < matrix.Rows(); ++i)
  {
    for (uint64 j = 0; j < matrix.Cols(); ++j)
    {
      result(static_cast<IndexType>(i), static_cast<IndexType>(j)) =
        static_cast<T>(matrix.At(i, j));
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;  
}

//------------------------------------------------------------------------------
template <typename T, typename U>
inline DynamicVector<T> EigenAdapter::Convert(
  Eigen::Matrix<U, Eigen::Dynamic, 1> const& vector)
{
  typedef typename Eigen::Matrix<U, Eigen::Dynamic, 1>::Index IndexType;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicVector<T> result(static_cast<uint64>(vector.size()));
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < uint64(vector.size()); ++i)
  {
    result.At(i) = static_cast<T>(vector(static_cast<IndexType>(i)));
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <typename T, typename U>
inline DynamicMatrix<T> EigenAdapter::Convert(
  Eigen::Matrix<U, Eigen::Dynamic, Eigen::Dynamic> const& matrix)
{
  typedef typename Eigen::Matrix<U, Eigen::Dynamic, Eigen::Dynamic>::Index
    IndexType;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicMatrix<T> result(
    static_cast<uint64>(matrix.rows()), static_cast<uint64>(matrix.cols()));
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < uint64(matrix.rows()); ++i)
  {
    for (uint64 j = 0; j < uint64(matrix.cols()); ++j)
    {
      result.At(i, j) = static_cast<T>(
        matrix(static_cast<IndexType>(i), static_cast<IndexType>(j)));
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_adapter_EigenAdapter_inline_h_
