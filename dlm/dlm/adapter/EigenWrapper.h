//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_adapter_EigenWrapper_h_
#define dlm_adapter_EigenWrapper_h_

//==============================================================================
// GCC directive to disable warnings coming from Eigen
//------------------------------------------------------------------------------
#if defined(__GNUC__) && defined(__GNUC_MINOR__) && defined(__GNUC_PATCHLEVEL__) 
#pragma GCC system_header
#endif

//==============================================================================
// MSVC directive to disable warnings coming from Eigen (1/2)
//------------------------------------------------------------------------------
#if defined(_MSC_VER)
#pragma warning(push, 0)
//#pragma warning(disable: 4244) //conversion, possible loss of data
//#pragma warning(disable: 4714) //marked as __forceinline not inlined
#endif

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#define EIGEN_INITIALIZE_MATRICES_BY_ZERO
#include <Eigen/Dense>
#include <Eigen/SVD>

//==============================================================================
// MSVC directive to disable warnings coming from Eigen (2/2)
//------------------------------------------------------------------------------
#if defined(_MSC_VER)
#pragma warning(pop)
#endif

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_adapter_EigenWrapper_h_
