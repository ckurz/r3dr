//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
//------------------------------------------------------------------------------
// Copyright (C)  2014  TrackMen Ltd.
//
//------------------------------------------------------------------------------
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/optimizer/ProjectiveOptimizer.h"

//------------------------------------------------------------------------------
#include "dlm/optimizer/CostFunctions.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/optimizer/CommonFunctions.h"
#include "dlm/optimizer/CommonFunctions.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/Scene.h"
#include "dlm/scene/SceneAssistant.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/normalizer/Normalizer.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/differentiator/ProjectiveDifferentiator.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/cholesky/Cholesky.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Unnamed namespace
//------------------------------------------------------------------------------
namespace
{

//==============================================================================
// Local classes/structs
//------------------------------------------------------------------------------
struct RequiredElements
{
  std::set<                  PRepId  > pRep;
  std::set<                  ORepId  > oRep;
  std::set<std::pair<FSetId, FRepId> > fRep;
  std::set<                  WMatId  > wMat;
};

//==============================================================================
// Local functions
//------------------------------------------------------------------------------
RequiredElements Analyze(Scene const& scene);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
ProjectiveOptimizer::OptimizationData Acquire(
  Scene       const& scene,
  RequiredElements const& elements,
  ProjectiveOptimizer::MetaData& metaData);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
ProjectiveOptimizer::Structure Process(ProjectiveOptimizer::MetaData& metaData);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
std::pair<std::vector<Matrix3f>, std::vector<Matrix3f>>
  NormalizeFeaturePoints(
    ProjectiveOptimizer::OptimizationData& data,
    ProjectiveOptimizer::MetaData& metaData);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
std::pair<Matrix4f, Matrix4f> NormalizeObjectPoints(
  ProjectiveOptimizer::OptimizationData& data);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
template <typename T>
ProjectiveOptimizer::Vectors SolveImplementation(
  ProjectiveOptimizer::SystemMatrix       const& jtj,
  ProjectiveOptimizer::Vectors   const& epsilon,
  ProjectiveOptimizer::Structure const& structure);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
template <typename T>
void UpdateImplementation(
  ProjectiveOptimizer::OptimizationData& scene,
  ProjectiveOptimizer::Vectors const& delta);

//==============================================================================
// Unnamed namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
ProjectiveOptimizer::ProjectiveOptimizer(
  bool const normalizeFeaturePoints,
  bool const normalizeObjectPoints,
  BaseClass::OptimizationParameters const& parameters)
  : BaseClass(parameters)
  , normalizeFeaturePoints_(normalizeFeaturePoints)
  , normalizeObjectPoints_ (normalizeObjectPoints )
{}

//==============================================================================
// Destructor
//------------------------------------------------------------------------------
ProjectiveOptimizer::~ProjectiveOptimizer()
{}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
ProjectiveOptimizer::OptimizationData ProjectiveOptimizer::Pack(
  Scene const& scene)
{
    normalization_ = Normalization();
  denormalization_ = Normalization();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  OptimizationData data = Acquire(scene, Analyze(scene), metaData_);

  structure_ = Process(metaData_);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (normalizeFeaturePoints_)
  {
    auto const result = NormalizeFeaturePoints(data, metaData_);

      normalization_.featurePoint = result.first;
    denormalization_.featurePoint = result.second;
  }

  if (normalizeObjectPoints_)
  {
    auto const result = NormalizeObjectPoints(data);

      normalization_.objectPoint = result.second;
    denormalization_.objectPoint = result.second;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return data;
}

//------------------------------------------------------------------------------
std::pair<FloatType, FloatType> ProjectiveOptimizer::CreateSystem(
  OptimizationData const& data,
  SystemMatrix& A, Vectors& b,
  CostFunction const& costFunction)
{
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  A = SystemMatrix();
  b = Vectors();

  A.uPP = DynamicMatrix<Matrix12f  >(structure_.pRepCount, structure_.pRepCount);
  A.vOO = DynamicVector<Matrix3f   >(structure_.oRepCount);
  A.wPO = DynamicVector<Matrix12x3f>(structure_.wPO.size);

  b.p = DynamicVector<Vector12f>(structure_.pRepCount);
  b.o = DynamicVector<Vector3f >(structure_.oRepCount);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FloatType overallCost         = 0.0;
  FloatType overallWeightedCost = 0.0;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (auto it = metaData_.fRep.cbegin(); it != metaData_.fRep.cend(); ++it)
  {
    ProjectiveCamera const& pRep = data.pRep.at(it->pRepId);
    ObjectPoint      const& oRep = data.oRep.at(it->oRepId);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // Calculate derivatives.
    auto result = ProjectiveDifferentiator::Differentiate(pRep, oRep);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // Determine the error offset epsilon.
    Vector2d const scaledEpsilon = it->position - result.pProj;
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // The offset is not an accurate representation of the true error: it is
    // scaled and affected by the pixel aspect ratio. In addition, the weight
    // for the robust cost function cannot be calculated from the offset
    // directly. It has thus to be scaled properly.
    FloatType inverseScale = static_cast<FloatType>(1.0);

    if (normalizeFeaturePoints_)
    {
      inverseScale = denormalization_.featurePoint.at(it->pRepId)(0, 0);
    }

    Vector2f epsilon = scaledEpsilon * inverseScale;

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // The properly scaled error offset may now be used to calculate the RMSE
    // and the weighting for the robust cost function.
    overallCost += epsilon.Dot(epsilon);
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    FloatType const magnitude = std::max(epsilon.Magnitude(), Epsilon);

    FloatType const cost = costFunction(magnitude);

    FloatType const weight = std::sqrt(cost) / magnitude;

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Matrix2f weighting;

    weighting(0, 0) = weight * weight;
    weighting(1, 1) = weight * weight;

    if (it->hasWMat)
    {
      weighting *= metaData_.wMat.at(it->wMatId);
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    overallWeightedCost += epsilon.Dot(weighting * epsilon);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // Remove undesired derivatives.
    Implementation::Cull(result.pDeriv, metaData_.pRepFlags.at(it->pRepId));
    Implementation::Cull(result.oDeriv, metaData_.oRepFlags.at(it->oRepId));

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // Prepare matrices.
    Matrix2x12f const& pDeriv  = result.pDeriv;
    Matrix2x3f  const& oDeriv  = result.oDeriv;

    Matrix12x2f const  pDerivT = MF::Transpose(pDeriv);
    Matrix3x2f  const  oDerivT = MF::Transpose(oDeriv);
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    A.uPP(it->pRepId, it->pRepId) += pDerivT * weighting * pDeriv;
    A.vOO(it->oRepId)             += oDerivT * weighting * oDeriv;
    A.wPO(it->wPOIndex)           += pDerivT * weighting * oDeriv;
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    b.p(it->pRepId) += pDerivT * weighting * scaledEpsilon;
    b.o(it->oRepId) += oDerivT * weighting * scaledEpsilon;
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FloatType const rmse =
    std::sqrt(overallCost / static_cast<FloatType>(metaData_.fRep.size()));

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return std::make_pair(rmse, overallWeightedCost);
}

//------------------------------------------------------------------------------
FloatType ProjectiveOptimizer::LambdaScaleHint(SystemMatrix const& systemMatrix)
{
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FloatType sum = 0;
  uint64 numValues = 0;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < systemMatrix.uPP.Rows(); ++i)
  {
    auto const& matrix = systemMatrix.uPP(i, i);

    for (uint32 j = 0; j < 6; ++j)
    {
      if (matrix(j, j) != 0)
      {
        sum += std::abs(matrix(j, j));
        ++numValues;
      }
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < systemMatrix.vOO.Size(); ++i)
  {
    auto const& matrix = systemMatrix.vOO(i);

    for (uint32 j = 0; j < 3; ++j)
    {
      if (matrix(j, j) != 0)
      {
        sum += std::abs(matrix(j, j));
        ++numValues;
      }
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FloatType const average = sum / static_cast<FloatType>(numValues);

  DLM_ASSERT(std::abs(average) > Epsilon,DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return average;
}

//------------------------------------------------------------------------------
void ProjectiveOptimizer::AugmentAdditive(
  SystemMatrix& A, FloatType const value)
{
  Implementation::AdditiveUpdate(A.uPP, value);

  for (uint64 i = 0; i < A.vOO.Size(); ++i)
  {
    Implementation::AdditiveUpdate(A.vOO(i), value);
  }
}

//------------------------------------------------------------------------------
void ProjectiveOptimizer::AugmentMultiplicative(
  SystemMatrix& A, FloatType const value)
{
  Implementation::MultiplicativeUpdate(A.uPP, value);

  for (uint64 i = 0; i < A.vOO.Size(); ++i)
  {
    Implementation::MultiplicativeUpdate(A.vOO(i), value);
  }
}

//------------------------------------------------------------------------------
ProjectiveOptimizer::Vectors ProjectiveOptimizer::Solve(
  SystemMatrix& A, Vectors& b)
{
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < A.vOO.Size(); ++i)
  {
    Matrix3d& v = A.vOO.At(i);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // Inversion - pay attention to zero entries.
    Flags<3> const& oRepFlags = metaData_.oRepFlags.at(i);

    for (uint32 j = 0; j < 3; ++j)
    {
      if (oRepFlags.At(j)) { continue; }

      v.At(j, j) = 1.0;
    }
            
    MF::Invert(v);

    for (uint32 j = 0; j < 3; ++j)
    {
      if (oRepFlags.At(j)) { continue; }

      v.At(j, j) = 0.0;
    }
  }

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::SubtractYWT(A.uPP, A.vOO, A.wPO, A.wPO, structure_.wPO, structure_.wPO, true);

  Implementation::SubtractYEpO(b.p, A.wPO, A.vOO, b.o, structure_.wPO);

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Vectors const x = SolveImplementation<FloatType>(A, b, structure_);

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return x;
}

//------------------------------------------------------------------------------
void ProjectiveOptimizer::Update(OptimizationData& data, Vectors const& delta)
{
  UpdateImplementation<FloatType>(data, delta);
}

//------------------------------------------------------------------------------
std::pair<FloatType, FloatType> ProjectiveOptimizer::Evaluate(
  OptimizationData const& data,
  CostFunction const& costFunction)
{
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FloatType overallCost         = 0.0;
  FloatType overallWeightedCost = 0.0;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (auto it = metaData_.fRep.cbegin(); it != metaData_.fRep.cend(); ++it)
  {
    ProjectiveCamera const& pRep = data.pRep.at(it->pRepId);
    ObjectPoint      const& oRep = data.oRep.at(it->oRepId);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // Determine the error offset epsilon.
    Vector2d const scaledEpsilon = it->position - pRep(oRep);
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // The offset is not an accurate representation of the true error: it is
    // scaled and affected by the pixel aspect ratio. In addition, the weight
    // for the robust cost function cannot be calculated from the offset
    // directly. It has thus to be scaled properly.
    FloatType inverseScale = static_cast<FloatType>(1.0);

    if (normalizeFeaturePoints_)
    {
      inverseScale = denormalization_.featurePoint.at(it->pRepId)(0, 0);
    }

    Vector2f const epsilon = scaledEpsilon * inverseScale;

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // The properly scaled error offset may now be used to calculate the RMSE
    // and the weighting for the robust cost function.
    overallCost += epsilon.Dot(epsilon);
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    FloatType const magnitude = std::max(epsilon.Magnitude(), Epsilon);

    FloatType const cost = costFunction(magnitude);

    FloatType const weight = std::sqrt(cost) / magnitude;

    Vector2f const weightedEpsilon = epsilon * weight;
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Matrix2f weighting;

    if (it->hasWMat)
    {
      weighting = metaData_.wMat.at(it->wMatId);
    }
    else
    {
      MF::MakeIdentity(weighting);
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    overallWeightedCost += weightedEpsilon.Dot(weighting * weightedEpsilon);
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FloatType const rmse =
    std::sqrt(overallCost / static_cast<FloatType>(metaData_.fRep.size()));

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return std::make_pair(rmse, overallWeightedCost);
}

//------------------------------------------------------------------------------
void ProjectiveOptimizer::Unpack(OptimizationData const& data, Scene& scene)
{
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < structure_.pRepCount; ++i)
  {
    ProjectiveCamera projection = data.pRep.at(i);

    if (normalizeFeaturePoints_)
    {
      projection = denormalization_.featurePoint.at(i) * projection;
    }

    if (normalizeObjectPoints_)
    {
      projection = projection * normalization_.objectPoint;
    }

    scene.Get(metaData_.pRepId.at(i)) = projection;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < structure_.oRepCount; ++i)
  {
    ObjectPoint objectPoint = data.oRep.at(i);

    if (normalizeObjectPoints_)
    {
      objectPoint = VF::Inhomogenize(denormalization_.objectPoint * VF::Homogenize(objectPoint));
    }

    scene.Get(metaData_.oRepId.at(i)) = objectPoint;
  }
}

//==============================================================================
// Unnamed namespace
//------------------------------------------------------------------------------
namespace
{

//==============================================================================
// Local functions
//------------------------------------------------------------------------------
RequiredElements Analyze(Scene const& scene)
{
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  RequiredElements required;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint64 const oRepRange = scene.ORepS().Range();

  for (uint64 i = 0; i < oRepRange; ++i)
  {
    ORepId const oRepId = ORepId(i);

    if (!scene.IsValid(oRepId)) { continue; }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ORep const& oRep = scene.Get(oRepId);
    
    DLM_ASSERT(!oRep.Reference<TSeq>::IsValid(),DLM_RUNTIME_ERROR);

    auto fTrkSet = oRep.ReferenceSet<FTrk>::Get();

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    for (auto it = fTrkSet.cbegin(); it != fTrkSet.cend(); ++it)
    {
      FTrk const& fTrk = scene.Get(*it);
      
      DLM_ASSERT(!fTrk.Reference<TSeq>::IsValid(),DLM_RUNTIME_ERROR);

      // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      for (auto fTrkIt = fTrk.cbegin(); fTrkIt != fTrk.cend(); ++fTrkIt)
      {
        FSet const& fSet = scene.Get(fTrkIt->first);

        if (!fSet.Reference<IRep>::IsValid()) { continue; }

        // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        IRep const& iRep = scene.Get(fSet.Reference<IRep>::Get());

        DLM_ASSERT(!iRep.Reference<CRep>::IsValid(),DLM_RUNTIME_ERROR);
        DLM_ASSERT(!iRep.Reference<DRep>::IsValid(),DLM_RUNTIME_ERROR);
        DLM_ASSERT(!iRep.Reference<TSeq>::IsValid(),DLM_RUNTIME_ERROR);

        if (!iRep.Reference<PRep>::IsValid()) { continue; }

        // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        PRepId const pRepId = iRep.Reference<PRep>::Get();
        
        PRep const& pRep = scene.Get(pRepId);

        // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        if (oRep.Count() != 0 || pRep.Count() != 0)
        {
          required.fRep.insert(std::make_pair(fTrkIt->first, fTrkIt->second));

          FRep const& fRep = scene.Get(fTrkIt->first, fTrkIt->second);

          if (fRep.Reference<WMat>::IsValid())
          {
            required.wMat.insert(fRep.Reference<WMat>::Get());
          }

          required.pRep.insert(pRepId);
          required.oRep.insert(oRepId);
        }
      }
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return required;
}

//------------------------------------------------------------------------------
ProjectiveOptimizer::OptimizationData Acquire(
  Scene            const& scene,
  RequiredElements const& elements,
  ProjectiveOptimizer::MetaData& metaData)
{
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  metaData = ProjectiveOptimizer::MetaData();

  ProjectiveOptimizer::OptimizationData data;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  std::map<PRepId, uint64> pRepIdMap;
  std::map<ORepId, uint64> oRepIdMap;
  std::map<WMatId, uint64> wMatIdMap;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Acquire projective camera data.
  for (auto it = elements.pRep.cbegin(); it != elements.pRep.cend(); ++it)
  {
    pRepIdMap[*it] = data.pRep.size();

    PRep const& pRep = scene.Get(*it);

    data.pRep.push_back(pRep);
    
    metaData.pRepFlags.push_back(pRep);
    metaData.pRepId.push_back(*it);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Acquire object point data.
  for (auto it = elements.oRep.cbegin(); it != elements.oRep.cend(); ++it)
  {
    oRepIdMap[*it] = data.oRep.size();

    ORep const& oRep = scene.Get(*it);

    data.oRep.push_back(oRep);

    metaData.oRepFlags.push_back(oRep);
    metaData.oRepId.push_back(*it);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // acquire weighting matrix data
  for (auto it = elements.wMat.begin(); it != elements.wMat.end(); ++it)
  {
    wMatIdMap[*it] = metaData.wMat.size();

    metaData.wMat.push_back(scene.Get(*it));
    metaData.wMatId.push_back(*it);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Assemble feature point data.
  for (auto it = elements.fRep.cbegin(); it != elements.fRep.cend(); ++it)
  {    
    FSet const& fSet = scene.Get(it->first);
    FRep const& fRep = scene.Get(it->first, it->second);
    
    IRep const& iRep = scene.Get(fSet.Reference<IRep>::Get());
    FTrk const& fTrk = scene.Get(fRep.Reference<FTrk>::Get());
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ProjectiveOptimizer::FRepData fRepData(fRep);

    if (fRep.Reference<WMat>::IsValid())
    {
      fRepData.wMatId = wMatIdMap[fRep.Reference<WMat>::Get()];
    }

    fRepData.pRepId = pRepIdMap[iRep.Reference<PRep>::Get()];
    fRepData.oRepId = oRepIdMap[fTrk.Reference<ORep>::Get()];
             
    fRepData.hasWMat = fRep.Reference<WMat>::IsValid();  

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    metaData.fRep.push_back(fRepData);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return data;
}

//------------------------------------------------------------------------------
ProjectiveOptimizer::Structure Process(ProjectiveOptimizer::MetaData& metaData)
{
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint64 const pRepCount = metaData.pRepId.size();
  uint64 const oRepCount = metaData.oRepId.size();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  ProjectiveOptimizer::Structure structure;
  
  structure.pRepCount = pRepCount;
  structure.oRepCount = oRepCount;

  {
    uint64 sSize     = 0;
    uint64 optimized = 0;

    structure.s.pRepIdx = 
      Implementation::ObtainLayout(metaData.pRepFlags, sSize, optimized);

    structure.s.size = sSize;
    
    structure.pRepOptimizedCount = optimized;
  }
  
  // Determine number of optimized object points.
  {
    uint64 optimized = 0;
    
    for (auto it = metaData.oRepFlags.begin(); it != metaData.oRepFlags.end();
      ++it)
    {
      optimized += it->Count() > 0;
    }
    
    structure.oRepOptimizedCount = optimized;
  }

  // determine w matrix structure
  DynamicMatrix<bool> wPOStructure(pRepCount, oRepCount);

  uint64 const fCount = metaData.fRep.size();

  for (uint64 i = 0; i < fCount; ++i)
  {
    ProjectiveOptimizer::FRepData const& fRepData = metaData.fRep.at(i);

    wPOStructure.At(fRepData.pRepId, fRepData.oRepId) = true;
  }

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // determine w matrix indices
  DynamicMatrix<uint64> wPOIndices(pRepCount, oRepCount);

  uint64 wPOSize = 0;

  for (uint64 j = 0; j < oRepCount; ++j)
  {
    for (uint64 i = 0; i < pRepCount; ++i)
    {
      if (!wPOStructure.At(i, j)) { continue; }

      wPOIndices.At(i, j) = wPOSize;

      structure.wPO.colIdx.push_back(i);

      ++wPOSize;
    }
    
    structure.wPO.rowEnd.push_back(wPOSize);
  }

  structure.wPO.size = wPOSize;

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // update feature point data with the indices
  for (uint64 i = 0; i < fCount; ++i)
  {
    ProjectiveOptimizer::FRepData& fRepData = metaData.fRep.at(i);

    fRepData.wPOIndex = wPOIndices.At(fRepData.pRepId, fRepData.oRepId);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return structure;  
}

//------------------------------------------------------------------------------
std::pair<std::vector<Matrix3f>, std::vector<Matrix3f>> NormalizeFeaturePoints(
  ProjectiveOptimizer::OptimizationData& data,
  ProjectiveOptimizer::MetaData& metaData)
{
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  std::vector<std::vector<Vector2f>> featurePoints;

  featurePoints.resize(data.pRep.size());

  for (auto const& featurePoint : metaData.fRep)
  {
    featurePoints.at(featurePoint.pRepId).push_back(featurePoint.position);
  }

  std::vector<Matrix3f> normalizations;

  for (auto const& points : featurePoints)
  {
    normalizations.push_back(Normalizer::Calculate(points));
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (std::vector<PRep>::size_type i = 0; i < data.pRep.size(); ++i)
  {
    data.pRep.at(i) = normalizations.at(i) * data.pRep.at(i);
  }
     
  for (auto& featurePoint : metaData.fRep)
  {
    Matrix3f const& normalization = normalizations.at(featurePoint.pRepId);

    featurePoint.position = VF::Inhomogenize(normalization * VF::Homogenize(featurePoint.position));
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  std::vector<Matrix3f> denormalizations;

  denormalizations.reserve(normalizations.size());

  for (auto const& normalization : normalizations)
  {
    denormalizations.push_back(MF::Inverse(normalization));
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return std::make_pair(normalizations, denormalizations);
}

//------------------------------------------------------------------------------
std::pair<Matrix4f, Matrix4f> NormalizeObjectPoints(
  ProjectiveOptimizer::OptimizationData& data)
{
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  std::vector<Vector3f> objectPoints;

  objectPoints.reserve(data.oRep.size());

  for (auto const& objectPoint : data.oRep)
  {
    objectPoints.push_back(objectPoint);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Matrix4f const   normalization = Normalizer::Calculate(objectPoints);
  Matrix4f const denormalization = MF::Inverse(normalization);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (std::vector<ORep>::size_type i = 0; i < data.oRep.size(); ++i)
  {
    data.oRep.at(i) = VF::Inhomogenize(normalization * VF::Homogenize(data.oRep.at(i)));
  }

  for (std::vector<PRep>::size_type i = 0; i < data.pRep.size(); ++i)
  {
    data.pRep.at(i) = data.pRep.at(i) * denormalization;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return std::make_pair(normalization, denormalization);
}

//------------------------------------------------------------------------------
template <typename T>
inline ProjectiveOptimizer::Vectors SolveImplementation(
  ProjectiveOptimizer::SystemMatrix const& A,
  ProjectiveOptimizer::Vectors      const& b,
  ProjectiveOptimizer::Structure const& structure)
{
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicMatrix<T> s(structure.s.size, structure.s.size);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::Insert(s, A.uPP, structure.s.pRepIdx, structure.s.pRepIdx, true);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicVector<T> e(structure.s.size);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::Insert(e, b.p, structure.s.pRepIdx);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DVectord solution = Cholesky::Solve(s, e);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  ProjectiveOptimizer::Vectors delta;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  delta.p = Implementation::Extract<12>(solution, structure.s.pRepIdx);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  delta.o = DynamicVector<Vector<3, T> >(structure.oRepCount);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::SubtractWTDl(delta.o, A.wPO, delta.p, structure.wPO);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < structure.oRepCount; ++i)
  {
    delta.o(i) = A.vOO(i) * (b.o(i) + delta.o(i));
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return delta;
}

//------------------------------------------------------------------------------
template <typename T>
inline void UpdateImplementation(
  ProjectiveOptimizer::OptimizationData& scene,
  ProjectiveOptimizer::Vectors const& delta)
{
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < delta.p.Size(); ++i)
  {
    Vector12f const& d = delta.p(i);
      
    ProjectiveCamera& pRep = scene.pRep.at(i);
    
    pRep.At(0, 0) += d( 0);
    pRep.At(0, 1) += d( 1);
    pRep.At(0, 2) += d( 2);
    pRep.At(0, 3) += d( 3);

    pRep.At(1, 0) += d( 4);
    pRep.At(1, 1) += d( 5);
    pRep.At(1, 2) += d( 6);
    pRep.At(1, 3) += d( 7);

    pRep.At(2, 0) += d( 8);
    pRep.At(2, 1) += d( 9);
    pRep.At(2, 2) += d(10);
    pRep.At(2, 3) += d(11);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < delta.o.Size(); ++i)
  {
    scene.oRep.at(i) += delta.o(i);
  }
}

//==============================================================================
// Unnamed namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
