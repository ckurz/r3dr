//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
//------------------------------------------------------------------------------
// Copyright (C)  2014  TrackMen Ltd.
//
//------------------------------------------------------------------------------
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_optimizer_MetricOptimizer_h_
#define dlm_optimizer_MetricOptimizer_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/optimizer/Optimizer.h"
#include "dlm/optimizer/Optimizer.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/entity/Camera.h"
#include "dlm/entity/Distortion.h"
#include "dlm/entity/Transformation.h"
#include "dlm/entity/ObjectPoint.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/Identifiers.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/flags/Flags.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/DynamicVector.h"
#include "dlm/math/DynamicMatrix.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <vector>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
//
//------------------------------------------------------------------------------
struct MetricOptimizerOptimizationData
{
  std::vector<Camera        > cRep;
  std::vector<Distortion    > dRep;
  std::vector<Transformation> tRep;
  std::vector<ObjectPoint   > oRep;
};

struct MetricOptimizerSystemMatrix
{
  //----------------------------------------------------------------------------
  // Member variables
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // U matrix
  DynamicMatrix<Matrix6d  > uTT;
  DynamicMatrix<Matrix6x5d> uTC;
  DynamicMatrix<Matrix6x8d> uTD;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicMatrix<Matrix5d  > uCC;
  DynamicMatrix<Matrix5x8d> uCD;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicMatrix<Matrix8d  > uDD;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // V matrix
  DynamicVector<Matrix3d  > vOO;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // W matrix
  DynamicVector<Matrix6x3d> wTO;
  DynamicVector<Matrix5x3d> wCO;
  DynamicVector<Matrix8x3d> wDO;
};

struct MetricOptimizerVectors
{
  //----------------------------------------------------------------------------
  // Member variables
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicVector<Vector6d> t;
  DynamicVector<Vector5d> c;
  DynamicVector<Vector8d> d;
  DynamicVector<Vector3d> o;
};

//==============================================================================
// MetricOptimizer class
//------------------------------------------------------------------------------
class MetricOptimizer : public Optimizer<MetricOptimizerOptimizationData,
                                         MetricOptimizerSystemMatrix,
                                         MetricOptimizerVectors>
{
public:
  //============================================================================
  // Nested
  //----------------------------------------------------------------------------
  // MatrixLayout struct
  //----------------------------------------------------------------------------
  struct MatrixLayout
  {
    //--------------------------------------------------------------------------
    // Constructors
    //--------------------------------------------------------------------------
    MatrixLayout()
      : size(0)
    {}

    //--------------------------------------------------------------------------
    // Member variables
    //--------------------------------------------------------------------------
    std::vector<uint64> cRepIdx;
    std::vector<uint64> dRepIdx;
    std::vector<uint64> tRepIdx;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    uint64 size;
  };

  //----------------------------------------------------------------------------
  // MatrixData struct
  //----------------------------------------------------------------------------
  struct MatrixData
  {
    //--------------------------------------------------------------------------
    // Constructors
    //--------------------------------------------------------------------------
    MatrixData()
      : size(0)
    {}

    //--------------------------------------------------------------------------
    // Member variables
    //--------------------------------------------------------------------------
    std::vector<uint64> rowEnd;
    std::vector<uint64> colIdx;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    uint64 size;
  };
  
  //----------------------------------------------------------------------------
  // Structure struct
  //----------------------------------------------------------------------------
  struct Structure
  {
    //--------------------------------------------------------------------------
    // Constructors
    //--------------------------------------------------------------------------
    Structure()
      : cRepCount         (0)
      , dRepCount         (0)
      , tRepCount         (0)
      , oRepCount         (0)
      , cRepOptimizedCount(0)
      , dRepOptimizedCount(0)
      , tRepOptimizedCount(0)
      , oRepOptimizedCount(0)
    {}

    //--------------------------------------------------------------------------
    // Member variables
    //--------------------------------------------------------------------------
    MatrixLayout s;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    MatrixData wCO;
    MatrixData wDO;
    MatrixData wTO;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    uint64 cRepCount;
    uint64 dRepCount;
    uint64 tRepCount;
    uint64 oRepCount;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    uint64 cRepOptimizedCount;
    uint64 dRepOptimizedCount;
    uint64 tRepOptimizedCount;
    uint64 oRepOptimizedCount;
  };

  //----------------------------------------------------------------------------
  // FRepData struct
  //----------------------------------------------------------------------------
  struct FRepData
  {
    //--------------------------------------------------------------------------
    // Constructors
    //--------------------------------------------------------------------------
    FRepData()
      : cRepId  (Invalid)
      , dRepId  (Invalid)
      , oRepId  (Invalid)
      , wCOIndex(Invalid)
      , wDOIndex(Invalid)
      , hasDRep (false  )
      , hasWMat (false  )
    {}

    //--------------------------------------------------------------------------
    // Member variables
    //--------------------------------------------------------------------------
    std::vector<std::pair<uint64, bool> > tRepIds;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    std::vector<uint64> wTOIndices;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Vector2d position;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    uint64 cRepId;
    uint64 dRepId;
    uint64 oRepId;
    uint64 wMatId;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    uint64 wCOIndex;
    uint64 wDOIndex;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    bool hasDRep;
    bool hasWMat;
  };

  //----------------------------------------------------------------------------
  // MetaData struct
  //----------------------------------------------------------------------------
  struct MetaData
  {
    //--------------------------------------------------------------------------
    // Member variables
    //--------------------------------------------------------------------------
    std::vector<FRepData> fRep;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    std::vector<Matrix2f> wMat;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    std::vector<Flags<5> > cRepFlags;
    std::vector<Flags<8> > dRepFlags;
    std::vector<Flags<6> > tRepFlags;
    std::vector<Flags<3> > oRepFlags;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    std::vector<CRepId> cRepId;
    std::vector<DRepId> dRepId;
    std::vector<TRepId> tRepId;
    std::vector<ORepId> oRepId;
    std::vector<WMatId> wMatId;
  };

  //----------------------------------------------------------------------------
  // Denormalization struct
  //----------------------------------------------------------------------------
  struct Denormalization
  {
    std::vector<Matrix3f> featurePoint;
  };

  //============================================================================
  // Constructors
  //----------------------------------------------------------------------------
  MetricOptimizer(
    bool const normalizeFeaturePoints = false,
    BaseClass::OptimizationParameters const& parameters =
      BaseClass::OptimizationParameters());

  //============================================================================
  // Destructor
  //----------------------------------------------------------------------------
  virtual ~MetricOptimizer();

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  virtual OptimizationData Pack(Scene const& scene);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  virtual std::pair<FloatType, FloatType> CreateSystem(
    OptimizationData const& data,
    SystemMatrix& A, Vectors& b,
    CostFunction const& costFunction); 

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  virtual FloatType LambdaScaleHint(SystemMatrix const& A);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  virtual void AugmentAdditive      (SystemMatrix& A, FloatType const value);
  virtual void AugmentMultiplicative(SystemMatrix& A, FloatType const value);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  virtual Vectors Solve(SystemMatrix& A, Vectors& b);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  virtual void Update(OptimizationData& data, Vectors const& delta);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  virtual std::pair<FloatType, FloatType> Evaluate(
    OptimizationData const& data,
    CostFunction const& costFunction);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  virtual void Unpack(OptimizationData const& data, Scene& scene);


private:
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  Structure structure_;
  MetaData metaData_;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Denormalization denormalization_;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool normalizeFeaturePoints_;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
