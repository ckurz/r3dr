//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
//------------------------------------------------------------------------------
// Copyright (C)  2014  TrackMen Ltd.
//
//------------------------------------------------------------------------------
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz


//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_optimizer_CommonFunctions_inline_h_
#define dlm_optimizer_CommonFunctions_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/optimizer/CommonFunctions.h"

//------------------------------------------------------------------------------
#include "dlm/flags/Flags.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Matrix.inline.h"
#include "dlm/math/DynamicVector.inline.h"
#include "dlm/math/DynamicMatrix.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/MatrixFunctions.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Implementation namespace
//------------------------------------------------------------------------------
namespace Implementation
{

//==============================================================================
// Functions
//------------------------------------------------------------------------------
template <uint32 SIZE>
inline std::vector<uint64> ObtainLayout(
  std::vector<Flags<SIZE> > const& flags,
  uint64                         & currentSize,
  uint64                         & optimized)
{
  std::vector<uint64> layout;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (auto it = flags.begin(); it != flags.end(); ++it)
  {
    for (uint32 i = 0; i < SIZE; ++i)
    {
      if (it->At(i))
      {
        layout.push_back(currentSize);

        ++currentSize;
      }
      else
      {
        layout.push_back(Invalid);
      }
    }
    
    optimized += it->Count() > 0;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return layout;
}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline void Cull(Matrix<2, SIZE, T>& matrix, Flags<SIZE> const& flags)
{
  for (uint32 i = 0; i < SIZE; ++i)
  {
    if (flags.At(i)) { continue; }

    MF::SetCol(matrix, i, Vector2d());
  }
}

//------------------------------------------------------------------------------
template <uint32 DIM, typename T>
inline void MultiplicativeUpdate(Matrix<DIM, DIM, T>& matrix, T const value)
{
  for (uint32 i = 0; i < DIM; ++i)
  {
    matrix(i, i) *= value;
  }
}

//------------------------------------------------------------------------------
template <uint32 DIM, typename T>
inline void MultiplicativeUpdate(
  DynamicMatrix<Matrix<DIM, DIM, T> >& matrix, T const value)
{
  for (uint64 i = 0; i < matrix.Rows(); ++i)
  {
    MultiplicativeUpdate(matrix(i, i), value);
  }
}

//------------------------------------------------------------------------------
template <uint32 DIM, typename T>
inline void AdditiveUpdate(Matrix<DIM, DIM, T>& matrix, T const value)
{
  for (uint32 i = 0; i < DIM; ++i)
  {
    matrix(i, i) += value;
  }
}

//------------------------------------------------------------------------------
template <uint32 DIM, typename T>
inline void AdditiveUpdate(
  DynamicMatrix<Matrix<DIM, DIM, T> >& matrix, T const value)
{
  for (uint64 i = 0; i < matrix.Rows(); ++i)
  {
    AdditiveUpdate(matrix(i, i), value);
  }
}

//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, uint32 DIM3, typename T, typename MatrixData>
inline void SubtractYWT(
  DynamicMatrix<Matrix<ROWS, COLS, T> >      & u,
  DynamicVector<Matrix<DIM3, DIM3, T> > const& vVector,
  DynamicVector<Matrix<ROWS, DIM3, T> > const& w0,
  DynamicVector<Matrix<COLS, DIM3, T> > const& w1,
  MatrixData                     const& w0Data,
  MatrixData                     const& w1Data,
  bool                                            const  reduce)
{
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < w0Data.rowEnd.size(); ++i)
  {
    uint64 const start0 = i ? w0Data.rowEnd.at(i - 1) : 0;
    uint64 const   end0 =     w0Data.rowEnd.at(i    );
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    uint64 const start1 = i ? w1Data.rowEnd.at(i - 1) : 0;
    uint64 const   end1 =     w1Data.rowEnd.at(i    );
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Matrix<DIM3, DIM3, T> const& v = vVector.At(i);
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//    #pragma omp parallel for num_threads(7)
    //for (uint64 j = start0; j < end0; ++j)  // VS didn't like that...
    for (int64 j = static_cast<int64>(start0); j < static_cast<int64>(end0); ++j)
    {
      Matrix<ROWS, DIM3, T> const y = w0.At(static_cast<uint64>(j)) * v;
      
      // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      uint64 const rStart = reduce ? static_cast<uint64>(j) : start1;
      
      uint64 const i0 = w0Data.colIdx.at(
        static_cast<std::vector<uint64>::size_type>(j));
      
      // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      for (uint64 k = rStart; k < end1; ++k)
      {
        uint64 const i1 = w1Data.colIdx.at(k);
        
        u.At(i0, i1) -= y * MF::Transpose(w1.At(k));
      }
    }
  }
}

//------------------------------------------------------------------------------
template <uint32 SIZE, uint32 DIM2, typename T, typename MatrixData>
inline void SubtractYEpO(
  DynamicVector<Vector<SIZE,       T> >      & e,
  DynamicVector<Matrix<SIZE, DIM2, T> > const& w,
  DynamicVector<Matrix<DIM2, DIM2, T> > const& v,
  DynamicVector<Vector<DIM2,       T> > const& epO,
  MatrixData                     const& wData)
{
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < wData.rowEnd.size(); ++i)
  {
    uint64 const start = i ? wData.rowEnd.at(i - 1) : 0;
    uint64 const end   =     wData.rowEnd.at(i    );
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Vector<DIM2, T> const rhs = v.At(i) * epO.At(i);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    for (uint64 j = start; j < end; ++j)
    {
      e.At(wData.colIdx.at(j)) -= w.At(j) * rhs;
    }
  }
}

//----------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
inline void Insert(
  DynamicMatrix<T>                           & s,
  DynamicMatrix<Matrix<ROWS, COLS, T> > const& jtj,
  std::vector<uint64>                   const& writeIdx0,
  std::vector<uint64>                   const& writeIdx1,
  bool                                  const  reduce)
{
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 row = 0; row < jtj.Rows(); ++row)
  {
    uint64 const rBase = row * ROWS;

    uint64 const colStart = reduce ? row : 0;

    for (uint64 col = colStart; col < jtj.Cols(); ++col)
    {
      uint64 const cBase = col * COLS;

      Matrix<ROWS, COLS, T> const& m = jtj.At(row, col);

      for (uint32 i = 0; i < ROWS; ++i)
      {
        uint64 const i0 = writeIdx0.at(rBase + i);

        if (i0 == Invalid) { continue; }

        for (uint32 j = 0; j < COLS; ++j)
        {
          uint64 const i1 = writeIdx1.at(cBase + j);

          if (i1 == Invalid) { continue; }

          s.At(i0, i1) = m.At(i, j);
          s.At(i1, i0) = m.At(i, j);
        }
      }
    }
  }
}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline void Insert(
  DynamicVector<T>                     & e,
  DynamicVector<Vector<SIZE, T> > const& epsilon,
  std::vector<uint64>             const& writeIdx)
{
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < epsilon.Size(); ++i)
  {
    uint64 const base = i * SIZE;
    
    Vector<SIZE, T> const& vector = epsilon.At(i);

    for (uint32 j = 0; j < SIZE; ++j)
    {
      uint64 const i0 = writeIdx.at(base + j);

      if (i0 == Invalid) { continue; }

      e.At(i0) = vector.At(j);
    }
  }
}

//------------------------------------------------------------------------------
template <unsigned SIZE, typename T>
inline DynamicVector<Vector<SIZE, T> > Extract(
  DynamicVector<T> const& e, std::vector<uint64> const& indices)
{
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//  DLM_ASSERT(!(e.Size() % SIZE), RUNTIME_ERROR);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint64 const elementCount = indices.size() / SIZE;

  DynamicVector<Vector<SIZE, T> > delta(elementCount);

  uint64 current = 0;

  for (uint64 i = 0; i < elementCount; ++i)
  {
    Vector<SIZE, T> element;

    for (uint32 j = 0; j < SIZE; ++j)
    {
      uint64 const index = indices.at(current);

      ++current;

      if (index == uint64(-1)) { continue; }

      element.At(j) = e.At(index);
    }

    delta.At(i) = element;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return delta;
}

//------------------------------------------------------------------------------
template <uint32 SIZE, uint32 DIM2, typename T, typename MatrixData>
inline void SubtractWTDl(
  DynamicVector<Vector<DIM2,       T> >      & dlO,
  DynamicVector<Matrix<SIZE, DIM2, T> > const& w,
  DynamicVector<Vector<SIZE,       T> > const& d,
  MatrixData                 const& wData)
{
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < wData.rowEnd.size(); ++i)
  {
    uint64 const start = i ? wData.rowEnd.at(i - 1) : 0;
    uint64 const end   =     wData.rowEnd.at(i    );

    Vector<DIM2, T>& lhs = dlO.At(i);

    for (uint64 j = start; j < end; ++j)
    {
      lhs -= MF::Transpose(w.At(j)) * d.At(wData.colIdx.at(j));
    }
  }
}

//==============================================================================
// Implementation namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
