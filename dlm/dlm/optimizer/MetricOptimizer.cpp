//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
//------------------------------------------------------------------------------
// Copyright (C)  2014  TrackMen Ltd.
//
//------------------------------------------------------------------------------
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/optimizer/MetricOptimizer.h"

//------------------------------------------------------------------------------
#include "dlm/optimizer/CostFunctions.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/optimizer/CommonFunctions.h"
#include "dlm/optimizer/CommonFunctions.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/Scene.h"
#include "dlm/scene/SceneAssistant.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/normalizer/Normalizer.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/differentiator/Differentiator.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/cholesky/Cholesky.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Unnamed namespace
//------------------------------------------------------------------------------
namespace
{

//==============================================================================
// Local classes/structs
//------------------------------------------------------------------------------
struct RequiredElements
{
  std::set<                  CRepId  > cRep;
  std::set<                  DRepId  > dRep;
  std::set<                  TRepId  > tRep;
  std::set<                  ORepId  > oRep;
  std::set<std::pair<FSetId, FRepId> > fRep;
  std::set<                  WMatId  > wMat;
};

//==============================================================================
// Local functions
//------------------------------------------------------------------------------
RequiredElements Analyze(Scene const& scene);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void Acquire(
  Scene       const& scene,
  RequiredElements const& elements,
  MetricOptimizer::OptimizationData& data,
  MetricOptimizer::MetaData      & metaData);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
MetricOptimizer::Structure Process(MetricOptimizer::MetaData& metaData);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
std::vector<Matrix3f> NormalizeFeaturePoints(
  MetricOptimizer::OptimizationData& data,
  MetricOptimizer::MetaData& metaData);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
template <typename T>
MetricOptimizer::Vectors SolveImplementation(
  MetricOptimizer::SystemMatrix       const& jtj,
  MetricOptimizer::Vectors   const& epsilon,
  MetricOptimizer::Structure const& structure);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
template <typename T>
void UpdateImplementation(
  MetricOptimizer::OptimizationData& scene,
  MetricOptimizer::Vectors const& delta);

//==============================================================================
// Unnamed namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
MetricOptimizer::MetricOptimizer(
  bool const normalizeFeaturePoints,
  BaseClass::OptimizationParameters const& parameters)
  : BaseClass(parameters)
  , normalizeFeaturePoints_(normalizeFeaturePoints)
{}

//==============================================================================
// Destructor
//------------------------------------------------------------------------------
MetricOptimizer::~MetricOptimizer()
{}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
MetricOptimizer::OptimizationData MetricOptimizer::Pack(Scene const& scene)
{
  denormalization_ = Denormalization();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  OptimizationData data;

  Acquire(scene, Analyze(scene), data, metaData_);

  structure_ = Process(metaData_);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (normalizeFeaturePoints_)
  {
    denormalization_.featurePoint = NormalizeFeaturePoints(data, metaData_);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return data;
}

//------------------------------------------------------------------------------
std::pair<FloatType, FloatType> MetricOptimizer::CreateSystem(
  OptimizationData const& data,
  SystemMatrix& A, Vectors& b,
  CostFunction const& costFunction)
{
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  A = SystemMatrix();
  b = Vectors();

  A.uTT = DynamicMatrix<Matrix6d  >(structure_.tRepCount, structure_.tRepCount);
  A.uTC = DynamicMatrix<Matrix6x5d>(structure_.tRepCount, structure_.cRepCount);
  A.uTD = DynamicMatrix<Matrix6x8d>(structure_.tRepCount, structure_.dRepCount);
  A.uCC = DynamicMatrix<Matrix5d  >(structure_.cRepCount, structure_.cRepCount);
  A.uCD = DynamicMatrix<Matrix5x8d>(structure_.cRepCount, structure_.dRepCount);
  A.uDD = DynamicMatrix<Matrix8d  >(structure_.dRepCount, structure_.dRepCount);
  A.vOO = DynamicVector<Matrix3d  >(structure_.oRepCount);
  A.wTO = DynamicVector<Matrix6x3d>(structure_.wTO.size);
  A.wCO = DynamicVector<Matrix5x3d>(structure_.wCO.size);
  A.wDO = DynamicVector<Matrix8x3d>(structure_.wDO.size);

  b.t = DynamicVector<Vector6d>(structure_.tRepCount);
  b.c = DynamicVector<Vector5d>(structure_.cRepCount);
  b.d = DynamicVector<Vector8d>(structure_.dRepCount);
  b.o = DynamicVector<Vector3d>(structure_.oRepCount);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FloatType overallCost         = 0.0;
  FloatType overallWeightedCost = 0.0;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (auto it = metaData_.fRep.cbegin(); it != metaData_.fRep.cend(); ++it)
  {
    Camera      const& cRep = data.cRep.at(it->cRepId);
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Distortion  const& dRep =
      (it->hasDRep) ?         data.dRep.at(it->dRepId) : Distortion();
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ObjectPoint const& oRep = data.oRep.at(it->oRepId);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // assemble transformation vector
    std::vector<Transformation> tReps;

    for (auto tIt = it->tRepIds.cbegin(); tIt != it->tRepIds.cend(); ++tIt)
    {
      if (tIt->second)
      {
        // This is the 'special' case where we want to use the inverse of what's
        // stored to handle the camera center contraints.
        tReps.push_back(data.tRep.at(tIt->first).Inverse());
      }
      else
      {
        tReps.push_back(data.tRep.at(tIt->first));
      }
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // calculate derivatives
    Differentiator::Result result =
      Differentiator::Differentiate(cRep, dRep, tReps, oRep);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // Determine the error offset epsilon.
    Vector2f const scaledEpsilon = it->position - result.pProj;
 
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // The offset is not an accurate representation of the true error: it is
    // scaled and affected by the pixel aspect ratio. In addition, the weight
    // for the robust cost function cannot be calculated from the offset
    // directly. It has thus to be scaled properly.
    FloatType inverseScale = static_cast<FloatType>(1.0);

    if (normalizeFeaturePoints_)
    {
      inverseScale = denormalization_.featurePoint.at(it->cRepId)(0, 0);
    }

    Vector2f epsilon = scaledEpsilon * inverseScale;

    if (cRep.PixelAspectRatio() > static_cast<FloatType>(1.0))
    {
      // In this case, the focal length has to be increased for the y-axis,
      // which actually means that the x-axis is compressed.
      epsilon.X() *= cRep.PixelAspectRatio();
    }
    else
    {
      // In this case, the focal length has to be decreased for the y-axis,
      // which means that the y-axis is compressed.
      epsilon.Y() /= cRep.PixelAspectRatio();
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // The properly scaled error offset may now be used to calculate the RMSE
    // and the weighting for the robust cost function.
    overallCost += epsilon.Dot(epsilon);
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    FloatType const magnitude = std::max(epsilon.Magnitude(), Epsilon);

    FloatType const cost = costFunction(magnitude);

    FloatType const weight = std::sqrt(cost) / magnitude;

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Matrix2f weighting;

    weighting(0, 0) = weight * weight;
    weighting(1, 1) = weight * weight;

    if (it->hasWMat)
    {
      weighting *= metaData_.wMat.at(it->wMatId);
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    overallWeightedCost += epsilon.Dot(weighting * epsilon);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // remove undesired derivatives
    Flags<8> const& dRepFlags =
      (it->hasDRep) ?   metaData_.dRepFlags.at(it->dRepId) : Flags<8>();

    Implementation::Cull(result.cDeriv, metaData_.cRepFlags.at(it->cRepId));
    Implementation::Cull(result.dDeriv,         dRepFlags               );
    Implementation::Cull(result.oDeriv, metaData_.oRepFlags.at(it->oRepId));

    for (uint64 i = 0; i < it->tRepIds.size(); ++i)
    {
      Implementation::Cull(result.tDeriv.at(i), metaData_.tRepFlags.at(it->tRepIds.at(i).first));
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // prepare matrices
    Matrix2x5d const& cDeriv  = result.cDeriv;
    Matrix2x8d const& dDeriv  = result.dDeriv;
    Matrix2x3d const& oDeriv  = result.oDeriv;

    Matrix5x2d const  cDerivT = MF::Transpose(cDeriv);
    Matrix8x2d const  dDerivT = MF::Transpose(dDeriv);
    Matrix3x2d const  oDerivT = MF::Transpose(oDeriv);
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // write U matrix
    A.uCC.At(it->cRepId, it->cRepId)   += cDerivT * weighting * cDeriv;

    if (it->hasDRep)
    {
      A.uCD.At(it->cRepId, it->dRepId) += cDerivT * weighting * dDeriv;
      A.uDD.At(it->dRepId, it->dRepId) += dDerivT * weighting * dDeriv;
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // write V vector
    A.vOO.At(it->oRepId) += oDerivT * weighting * oDeriv;

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // write W matrices
    A.wCO.At(it->wCOIndex)   += cDerivT * weighting * oDeriv;
    
    if (it->hasDRep)
    {   
      A.wDO.At(it->wDOIndex) += dDerivT * weighting * oDeriv;
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // write epsilon vector
    b.c.At(it->cRepId)   += cDerivT * weighting * scaledEpsilon;
    b.o.At(it->oRepId)   += oDerivT * weighting * scaledEpsilon;

    if (it->hasDRep)
    {
      b.d.At(it->dRepId) += dDerivT * weighting * scaledEpsilon;
    }
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // write transformation stuff
    for (uint64 i = 0; i < it->tRepIds.size(); ++i)
    {
      uint64 const tRepId = it->tRepIds.at(i).first;
     
      Matrix2x6d const& tDeriv  = result.tDeriv.at(i);
      Matrix6x2d const  tDerivT = MF::Transpose(tDeriv);

      A.uTC.At(tRepId, it->cRepId) += tDerivT * weighting * cDeriv;

      if (it->hasDRep)
      {
        A.uTD.At(tRepId, it->dRepId) += tDerivT * weighting * dDeriv;
      }

      A.wTO.At(it->wTOIndices.at(i)) += tDerivT * weighting * oDeriv;

      b.t.At(tRepId) += tDerivT * weighting * scaledEpsilon;

      for (uint64 j = i; j < it->tRepIds.size(); ++j)
      {
        uint64 const otherTRepId = it->tRepIds.at(j).first;

        if (otherTRepId < tRepId)
        {
          A.uTT.At(otherTRepId, tRepId) +=
            MF::Transpose(result.tDeriv.at(j)) * weighting * tDeriv;
        }
        else
        {
          A.uTT.At(tRepId, otherTRepId) += tDerivT * weighting * result.tDeriv.at(j);
        }
      }
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FloatType const rmse =
    std::sqrt(overallCost / static_cast<FloatType>(metaData_.fRep.size()));

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return std::make_pair(rmse, overallWeightedCost);
}

//------------------------------------------------------------------------------
FloatType MetricOptimizer::LambdaScaleHint(
  SystemMatrix const& systemMatrix)
{
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FloatType sum = 0;
  uint64 numValues = 0;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < systemMatrix.uTT.Rows(); ++i)
  {
    auto const& matrix = systemMatrix.uTT(i, i);

    for (uint32 j = 0; j < 6; ++j)
    {
      if (matrix(j, j) != 0)
      {
        sum += std::abs(matrix(j, j));
        ++numValues;
      }
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < systemMatrix.uCC.Rows(); ++i)
  {
    auto const& matrix = systemMatrix.uCC(i, i);

    for (uint32 j = 0; j < 5; ++j)
    {
      if (matrix(j, j) != 0)
      {
        sum += std::abs(matrix(j, j));
        ++numValues;
      }
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < systemMatrix.uDD.Rows(); ++i)
  {
    auto const& matrix = systemMatrix.uDD(i, i);

    for (uint32 j = 0; j < 8; ++j)
    {
      if (matrix(j, j) != 0)
      {
        sum += std::abs(matrix(j, j));
        ++numValues;
      }
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < systemMatrix.vOO.Size(); ++i)
  {
    auto const& matrix = systemMatrix.vOO(i);

    for (uint32 j = 0; j < 3; ++j)
    {
      if (matrix(j, j) != 0)
      {
        sum += std::abs(matrix(j, j));
        ++numValues;
      }
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FloatType const average = sum / static_cast<FloatType>(numValues);

  DLM_ASSERT(std::abs(average) > Epsilon,DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return average;
}

//------------------------------------------------------------------------------
void MetricOptimizer::AugmentAdditive(SystemMatrix& A, FloatType const value)
{
  Implementation::AdditiveUpdate(A.uTT, value);
  Implementation::AdditiveUpdate(A.uCC, value);
  Implementation::AdditiveUpdate(A.uDD, value);

  for (uint64 i = 0; i < A.vOO.Size(); ++i)
  {
    Implementation::AdditiveUpdate(A.vOO(i), value);
  }
}

//------------------------------------------------------------------------------
void MetricOptimizer::AugmentMultiplicative(SystemMatrix& A, FloatType const value)
{
  Implementation::MultiplicativeUpdate(A.uTT, value);
  Implementation::MultiplicativeUpdate(A.uCC, value);
  Implementation::MultiplicativeUpdate(A.uDD, value);

  for (uint64 i = 0; i < A.vOO.Size(); ++i)
  {
    Implementation::MultiplicativeUpdate(A.vOO(i), value);
  }
}

//------------------------------------------------------------------------------
MetricOptimizer::Vectors MetricOptimizer::Solve(SystemMatrix& A, Vectors& b)
{
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < A.vOO.Size(); ++i)
  {
    Matrix3d& v = A.vOO.At(i);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // Inversion - pay attention to zero entries.
    Flags<3> const& oRepFlags = metaData_.oRepFlags.at(i);

    for (uint32 j = 0; j < 3; ++j)
    {
      if (oRepFlags.At(j)) { continue; }

      v.At(j, j) = 1.0;
    }
            
    MF::Invert(v);

    for (uint32 j = 0; j < 3; ++j)
    {
      if (oRepFlags.At(j)) { continue; }

      v.At(j, j) = 0.0;
    }
  }

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::SubtractYWT(A.uTT, A.vOO, A.wTO, A.wTO, structure_.wTO, structure_.wTO, true);
  Implementation::SubtractYWT(A.uTC, A.vOO, A.wTO, A.wCO, structure_.wTO, structure_.wCO);
  Implementation::SubtractYWT(A.uTD, A.vOO, A.wTO, A.wDO, structure_.wTO, structure_.wDO);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::SubtractYWT(A.uCC, A.vOO, A.wCO, A.wCO, structure_.wCO, structure_.wCO, true);
  Implementation::SubtractYWT(A.uCD, A.vOO, A.wCO, A.wDO, structure_.wCO, structure_.wDO);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::SubtractYWT(A.uDD, A.vOO, A.wDO, A.wDO, structure_.wDO, structure_.wDO, true);

  Implementation::SubtractYEpO(b.t, A.wTO, A.vOO, b.o, structure_.wTO);
  Implementation::SubtractYEpO(b.c, A.wCO, A.vOO, b.o, structure_.wCO);
  Implementation::SubtractYEpO(b.d, A.wDO, A.vOO, b.o, structure_.wDO);

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Vectors const x = SolveImplementation<FloatType>(A, b, structure_);

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return x;
}

//------------------------------------------------------------------------------
void MetricOptimizer::Update(OptimizationData& data, Vectors const& delta)
{
  UpdateImplementation<FloatType>(data, delta);
}

//------------------------------------------------------------------------------
std::pair<FloatType, FloatType> MetricOptimizer::Evaluate(
  OptimizationData const& data,
  CostFunction const& costFunction)
{
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FloatType overallCost         = 0.0;
  FloatType overallWeightedCost = 0.0;
    
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (auto it = metaData_.fRep.cbegin(); it != metaData_.fRep.cend(); ++it)
  {
    Camera      const& cRep = data.cRep.at(it->cRepId);
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Distortion  const& dRep =
      (it->hasDRep) ?         data.dRep.at(it->dRepId) : Distortion();
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ObjectPoint const& oRep = data.oRep.at(it->oRepId);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // transform point
    Vector3f p = oRep;

    for (auto rIt = it->tRepIds.crbegin(); rIt != it->tRepIds.crend(); ++rIt)
    {
      if (rIt->second)
      {
        // This is the 'special' case where we want to use the inverse of what's
        // stored to handle the camera center contraints.
        p = data.tRep.at(rIt->first).Inverse() * p;
      }
      else
      {
        p = data.tRep.at(rIt->first) * p;
      }
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // Determine the error offset epsilon.
    Vector2f const scaledEpsilon = it->position - cRep(dRep(p));
 
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // The offset is not an accurate representation of the true error: it is
    // scaled and affected by the pixel aspect ratio. In addition, the weight
    // for the robust cost function cannot be calculated from the offset
    // directly. It has thus to be scaled properly.
    FloatType inverseScale = static_cast<FloatType>(1.0);

    if (normalizeFeaturePoints_)
    {
      inverseScale = denormalization_.featurePoint.at(it->cRepId)(0, 0);
    }

    Vector2f epsilon = scaledEpsilon * inverseScale;

    if (cRep.PixelAspectRatio() > static_cast<FloatType>(1.0))
    {
      // In this case, the focal length has to be increased for the y-axis,
      // which actually means that the x-axis is compressed.
      epsilon.X() *= cRep.PixelAspectRatio();
    }
    else
    {
      // In this case, the focal length has to be decreased for the y-axis,
      // which means that the y-axis is compressed.
      epsilon.Y() /= cRep.PixelAspectRatio();
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // The properly scaled error offset may now be used to calculate the RMSE
    // and the weighting for the robust cost function.
    overallCost += epsilon.Dot(epsilon);
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    FloatType const magnitude = std::max(epsilon.Magnitude(), Epsilon);

    FloatType const cost = costFunction(magnitude);

    FloatType const weight = std::sqrt(cost) / magnitude;

    // Weight epsilon with the robust cost function.
    Vector2f const weightedEpsilon = epsilon * weight;

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Matrix2f weighting;

    if (it->hasWMat)
    {
      weighting = metaData_.wMat.at(it->wMatId);
    }
    else
    {
      MF::MakeIdentity(weighting);
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    overallWeightedCost += weightedEpsilon.Dot(weighting * weightedEpsilon);
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FloatType const rmse =
    std::sqrt(overallCost / static_cast<FloatType>(metaData_.fRep.size()));

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return std::make_pair(rmse, overallWeightedCost);
}

//------------------------------------------------------------------------------
void MetricOptimizer::Unpack(OptimizationData const& data, Scene& scene)
{
  if (normalizeFeaturePoints_)
  {
    for (uint64 i = 0; i < structure_.cRepCount; ++i)
    {
      Camera cRep = data.cRep.at(i);

      Matrix3f const& denormalization = denormalization_.featurePoint.at(i);

      FloatType const scaling = denormalization(0, 0);

      Vector2f const offset(denormalization(0, 2), denormalization(1, 2));

      cRep.PrincipalPoint() *= scaling;
      cRep.PrincipalPoint() += offset;

      cRep.FocalLength() *= scaling;
      cRep.Skew       () *= scaling;

      scene.Get(metaData_.cRepId.at(i)) = cRep;
    }
  }
  else
  {
    for (uint64 i = 0; i < structure_.cRepCount; ++i)
    {
      scene.Get(metaData_.cRepId.at(i)) = data.cRep.at(i);
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < structure_.dRepCount; ++i)
  {
    scene.Get(metaData_.dRepId.at(i)) = data.dRep.at(i);
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < structure_.tRepCount; ++i)
  {
    scene.Get(metaData_.tRepId.at(i)) = data.tRep.at(i);
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < structure_.oRepCount; ++i)
  {
    scene.Get(metaData_.oRepId.at(i)) = data.oRep.at(i);
  }
}

//==============================================================================
// Unnamed namespace
//------------------------------------------------------------------------------
namespace
{

//==============================================================================
// Local functions
//------------------------------------------------------------------------------
RequiredElements Analyze(Scene const& scene)
{
  RequiredElements required;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint64 const oRepRange = scene.ORepS().Range();

  for (uint64 i = 0; i < oRepRange; ++i)
  {
    ORepId const oRepId = ORepId(i);

    if (!scene.IsValid(oRepId)) { continue; }

    ORep const& oRep = scene.Get(oRepId);

    auto fTrkSet = oRep.ReferenceSet<FTrk>::Get();

    //check if the object point is referenced
    if (fTrkSet.empty())              { continue; }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    std::set<TRepId> oRepTReps;

    if (oRep.Reference<TSeq>::IsValid())
    {
      oRepTReps = SA::Enumerate(scene, oRep.Reference<TSeq>::Get());
    }

    bool oRepOptimizationRequired = oRep.Count() != 0;

    if (!oRepOptimizationRequired)
    {
      for (auto it = oRepTReps.begin(); it != oRepTReps.end(); ++it)
      {
        if (scene.Get(*it).Count())
        {
          oRepOptimizationRequired = true;

          break;
        }
      }
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    for (auto it = fTrkSet.begin(); it != fTrkSet.end(); ++it)
    {
      FTrk const& fTrk = scene.Get(*it);

      //check if the feature point track is referenced
      if (fTrk.empty()) { continue;}

      // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      std::set<TRepId> fTrkTReps;

      if (fTrk.Reference<TSeq>::IsValid())
      {
        fTrkTReps = SA::Enumerate(scene, fTrk.Reference<TSeq>::Get());
      }

      bool fTrkOptimizationRequired = false;

      for (auto tIt = fTrkTReps.begin(); tIt != fTrkTReps.end(); ++tIt)
      {
        if (scene.Get(*tIt).Count())
        {
          fTrkOptimizationRequired = true;

          break;
        }
      }

      // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      for (auto fTrkIt = fTrk.begin(); fTrkIt != fTrk.end(); ++fTrkIt)
      {
        FSet const& fSet = scene.Get(fTrkIt->first);

        //check if the feature point set is referenced
        if (!fSet.Reference<IRep>::IsValid()) { continue; }

        IRep const& iRep = scene.Get(fSet.Reference<IRep>::Get());

        DLM_ASSERT(!iRep.Reference<PRep>::IsValid(),DLM_RUNTIME_ERROR);

        //check if the image data has a camera node
        if (!iRep.Reference<CRep>::IsValid()) { continue; }

        CRepId const cRepId = iRep.Reference<CRep>::Get();

        // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //if this place is reached, the object point can be projected
        std::set<TRepId> iRepTReps;

        if (iRep.Reference<TSeq>::IsValid())
        {
          iRepTReps = SA::Enumerate(scene, iRep.Reference<TSeq>::Get());
        }

        bool iRepOptimizationRequired = scene.Get(cRepId).Count() != 0;

        if (!iRepOptimizationRequired)
        {
          for (auto tIt = iRepTReps.begin(); tIt != iRepTReps.end(); ++tIt)
          {
            if (scene.Get(*tIt).Count())
            {
              iRepOptimizationRequired = true;

              break;
            }
          }
        }

        // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        if (oRepOptimizationRequired ||
            fTrkOptimizationRequired ||
            iRepOptimizationRequired)
        {
          required.fRep.insert(*fTrkIt);

          FRep const& fRep = scene.Get(fTrkIt->first, fTrkIt->second);

          if (fRep.Reference<WMat>::IsValid())
          {
            required.wMat.insert(fRep.Reference<WMat>::Get());
          }

          required.cRep.insert(cRepId);

          if (iRep.Reference<DRep>::IsValid())
          {
            required.dRep.insert(iRep.Reference<DRep>::Get());
          }

          required.tRep.insert(oRepTReps.begin(), oRepTReps.end());
          required.tRep.insert(fTrkTReps.begin(), fTrkTReps.end());
          required.tRep.insert(iRepTReps.begin(), iRepTReps.end());
          
          required.oRep.insert(oRepId);
        }
      }
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return required;
}

//------------------------------------------------------------------------------
void Acquire(
  Scene       const& scene,
  RequiredElements const& elements,
  MetricOptimizerOptimizationData& data,
  MetricOptimizer::MetaData      & metaData)
{
  std::map<CRepId, uint64> cRepIdMap;
  std::map<DRepId, uint64> dRepIdMap;
  std::map<TRepId, uint64> tRepIdMap;
  std::map<ORepId, uint64> oRepIdMap;
  std::map<WMatId, uint64> wMatIdMap;

  data = MetricOptimizerOptimizationData();
  metaData = MetricOptimizer::MetaData();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // acquire camera data
  for (auto it = elements.cRep.begin(); it != elements.cRep.end(); ++it)
  {
    cRepIdMap[*it] = data.cRep.size();

    CRep const& cRep = scene.Get(*it);

    data.cRep.push_back(cRep);
    
    metaData.cRepFlags.push_back(cRep);
    metaData.cRepId.push_back(*it);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // acquire distortion data
  for (auto it = elements.dRep.begin(); it != elements.dRep.end(); ++it)
  {
    dRepIdMap[*it] = data.dRep.size();

    DRep const& dRep = scene.Get(*it);

    data.dRep.push_back(dRep);
    
    metaData.dRepFlags.push_back(dRep);
    metaData.dRepId.push_back(*it);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // acquire transformation data 
  for (auto it = elements.tRep.begin(); it != elements.tRep.end(); ++it)
  {
    tRepIdMap[*it] = data.tRep.size();

    TRep const& tRep = scene.Get(*it);

    data.tRep.push_back(tRep);

    metaData.tRepFlags.push_back(tRep);
    metaData.tRepId.push_back(*it);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // acquire object point data
  for (auto it = elements.oRep.begin(); it != elements.oRep.end(); ++it)
  {
    oRepIdMap[*it] = data.oRep.size();

    ORep const& oRep = scene.Get(*it);

    data.oRep.push_back(oRep);
    
    metaData.oRepFlags.push_back(oRep);
    metaData.oRepId.push_back(*it);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // acquire weighting matrix data
  for (auto it = elements.wMat.begin(); it != elements.wMat.end(); ++it)
  {
    wMatIdMap[*it] = metaData.wMat.size();

    metaData.wMat.push_back(scene.Get(*it));
    metaData.wMatId.push_back(*it);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // assemble feature point data
  for (auto it = elements.fRep.begin(); it != elements.fRep.end(); ++it)
  {    
    FSet const& fSet = scene.Get(it->first);
    FRep const& fRep = scene.Get(it->first, it->second);

    IRep const& iRep = scene.Get(fSet.Reference<IRep>::Get());
    FTrk const& fTrk = scene.Get(fRep.Reference<FTrk>::Get());
    
    auto const sequence =
      SA::AssembleTRepSequence(scene, it->first, it->second);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    MetricOptimizer::FRepData fRepData;

    for (auto sIt = sequence.begin(); sIt != sequence.end(); ++sIt)
    {
      fRepData.tRepIds.push_back(
        std::make_pair(tRepIdMap[sIt->first], sIt->second));
    }

    fRepData.position = fRep;

    if (fRep.Reference<WMat>::IsValid())
    {
      fRepData.wMatId = wMatIdMap[fRep.Reference<WMat>::Get()];
    }

    fRepData.cRepId   = cRepIdMap[iRep.Reference<CRep>::Get()];
    
    if (iRep.Reference<DRep>::IsValid())
    {
      fRepData.dRepId = dRepIdMap[iRep.Reference<DRep>::Get()];
    }

    fRepData.oRepId   = oRepIdMap[fTrk.Reference<ORep>::Get()];

    fRepData.hasDRep = iRep.Reference<DRep>::IsValid();
    fRepData.hasWMat = fRep.Reference<WMat>::IsValid();  

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    metaData.fRep.push_back(fRepData);
  }
}

//------------------------------------------------------------------------------
MetricOptimizer::Structure Process(MetricOptimizer::MetaData& metaData)
{
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint64 const cRepCount = metaData.cRepId.size();
  uint64 const dRepCount = metaData.dRepId.size();
  uint64 const tRepCount = metaData.tRepId.size();
  uint64 const oRepCount = metaData.oRepId.size();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  MetricOptimizer::Structure structure;
  
  structure.cRepCount = cRepCount;
  structure.dRepCount = dRepCount;
  structure.tRepCount = tRepCount;
  structure.oRepCount = oRepCount;

  {
    uint64 sSize      = 0;
    uint64 tOptimized = 0;
    uint64 cOptimized = 0;
    uint64 dOptimized = 0;

    structure.s.tRepIdx = 
      Implementation::ObtainLayout(metaData.tRepFlags, sSize, tOptimized);
    structure.s.cRepIdx = 
      Implementation::ObtainLayout(metaData.cRepFlags, sSize, cOptimized);
    structure.s.dRepIdx = 
      Implementation::ObtainLayout(metaData.dRepFlags, sSize, dOptimized);

    structure.s.size = sSize;
    
    structure.tRepOptimizedCount = tOptimized;
    structure.cRepOptimizedCount = cOptimized;
    structure.dRepOptimizedCount = dOptimized;
  }

  // Determine number of optimized object points.
  {
    uint64 optimized = 0;
    
    for (auto it = metaData.oRepFlags.begin(); it != metaData.oRepFlags.end();
      ++it)
    {
      optimized += it->Count() > 0;
    }
    
    structure.oRepOptimizedCount = optimized;
  }

  // determine w matrix structure
  DynamicMatrix<bool> wTOStructure(tRepCount, oRepCount);
  DynamicMatrix<bool> wCOStructure(cRepCount, oRepCount);
  DynamicMatrix<bool> wDOStructure(dRepCount, oRepCount);

  uint64 const fCount = metaData.fRep.size();

  for (uint64 i = 0; i < fCount; ++i)
  {
    MetricOptimizer::FRepData const& fRepData = metaData.fRep.at(i);

    wCOStructure.At(fRepData.cRepId, fRepData.oRepId)          = true;

    if (fRepData.hasDRep)
    {
      wDOStructure.At(fRepData.dRepId, fRepData.oRepId)        = true;
    }

    for (uint64 j = 0; j < fRepData.tRepIds.size(); ++j)
    {
      wTOStructure.At(fRepData.tRepIds.at(j).first, fRepData.oRepId) = true;
    }
  }

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // determine w matrix indices
  DynamicMatrix<uint64> wTOIndices(tRepCount, oRepCount);
  DynamicMatrix<uint64> wCOIndices(cRepCount, oRepCount);
  DynamicMatrix<uint64> wDOIndices(dRepCount, oRepCount);

  uint64 wTOSize = 0;
  uint64 wCOSize = 0;
  uint64 wDOSize = 0;

  for (uint64 j = 0; j < oRepCount; ++j)
  {
    for (uint64 i = 0; i < tRepCount; ++i)
    {
      if (!wTOStructure.At(i, j)) { continue; }

      wTOIndices.At(i, j) = wTOSize;

      structure.wTO.colIdx.push_back(i);

      ++wTOSize;
    }
    
    structure.wTO.rowEnd.push_back(wTOSize);
  }

  for (uint64 j = 0; j < oRepCount; ++j)
  {
    for (uint64 i = 0; i < cRepCount; ++i)
    {
      if (!wCOStructure.At(i, j)) { continue; }

      wCOIndices.At(i, j) = wCOSize;

      structure.wCO.colIdx.push_back(i);

      ++wCOSize;
    }
    
    structure.wCO.rowEnd.push_back(wCOSize);
  }

  for (uint64 j = 0; j < oRepCount; ++j)
  {
    for (uint64 i = 0; i < dRepCount; ++i)
    {
      if (!wDOStructure.At(i, j)) { continue; }

      wDOIndices.At(i, j) = wDOSize;

      structure.wDO.colIdx.push_back(i);

      ++wDOSize;
    }
    
    structure.wDO.rowEnd.push_back(wDOSize);
  }

  structure.wTO.size = wTOSize;
  structure.wCO.size = wCOSize;
  structure.wDO.size = wDOSize;

  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // update feature point data with the indices
  for (uint64 i = 0; i < fCount; ++i)
  {
    MetricOptimizer::FRepData& fRepData = metaData.fRep.at(i);

    fRepData.wCOIndex = wCOIndices.At(fRepData.cRepId, fRepData.oRepId);

    if (fRepData.hasDRep)
    {
      fRepData.wDOIndex = wDOIndices.At(fRepData.dRepId, fRepData.oRepId);
    }

    for (uint64 j = 0; j < fRepData.tRepIds.size(); ++j)
    {
      uint64 const tRepId = fRepData.tRepIds.at(j).first;

      fRepData.wTOIndices.push_back(wTOIndices.At(tRepId, fRepData.oRepId));
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return structure;  
}

//------------------------------------------------------------------------------
std::vector<Matrix3f> NormalizeFeaturePoints(
  MetricOptimizerOptimizationData& data,
  MetricOptimizer::MetaData& metaData)
{
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  std::vector<std::vector<Vector2f>> featurePoints;

  featurePoints.resize(data.cRep.size());

  for (auto const& featurePoint : metaData.fRep)
  {
    featurePoints.at(featurePoint.cRepId).push_back(featurePoint.position);
  }

  std::vector<Matrix3f> normalizations;

  for (auto const& points : featurePoints)
  {
    normalizations.push_back(Normalizer::Calculate(points));
  }

  for (std::vector<CRep>::size_type i = 0; i < data.cRep.size(); ++i)
  {
    Camera& cRep = data.cRep.at(i);

    Matrix3f const& normalization = normalizations.at(i);

    FloatType const scaling = normalization(0, 0);

    Vector2f const offset(normalization(0, 2), normalization(1, 2));

    cRep.PrincipalPoint() *= scaling;
    cRep.PrincipalPoint() += offset;

    cRep.FocalLength() *= scaling;
    cRep.Skew       () *= scaling;
  }
     

  for (auto& featurePoint : metaData.fRep)
  {
    Matrix3f const& normalization = normalizations.at(featurePoint.cRepId);

    featurePoint.position = VF::Inhomogenize(normalization * VF::Homogenize(featurePoint.position));
  }

  std::vector<Matrix3f> denormalizations;

  denormalizations.reserve(normalizations.size());

  for (auto const& normalization : normalizations)
  {
    denormalizations.push_back(MF::Inverse(normalization));
  }
  
  return denormalizations;
}

//------------------------------------------------------------------------------
template <typename T>
inline MetricOptimizer::Vectors SolveImplementation(
  MetricOptimizer::SystemMatrix       const& jtj,
  MetricOptimizer::Vectors   const& epsilon,
  MetricOptimizer::Structure const& structure)
{
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicMatrix<T> s(structure.s.size, structure.s.size);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::Insert(s, jtj.uTT, structure.s.tRepIdx, structure.s.tRepIdx, true);
  Implementation::Insert(s, jtj.uTC, structure.s.tRepIdx, structure.s.cRepIdx);
  Implementation::Insert(s, jtj.uTD, structure.s.tRepIdx, structure.s.dRepIdx);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::Insert(s, jtj.uCC, structure.s.cRepIdx, structure.s.cRepIdx, true);
  Implementation::Insert(s, jtj.uCD, structure.s.cRepIdx, structure.s.dRepIdx);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::Insert(s, jtj.uDD, structure.s.dRepIdx, structure.s.dRepIdx, true);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicVector<T> e(structure.s.size);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::Insert(e, epsilon.t, structure.s.tRepIdx);
  Implementation::Insert(e, epsilon.c, structure.s.cRepIdx);
  Implementation::Insert(e, epsilon.d, structure.s.dRepIdx);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DVectord solution = Cholesky::Solve(s, e);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  MetricOptimizer::Vectors delta;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  delta.t = Implementation::Extract<6>(solution, structure.s.tRepIdx);
  delta.c = Implementation::Extract<5>(solution, structure.s.cRepIdx);
  delta.d = Implementation::Extract<8>(solution, structure.s.dRepIdx);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  delta.o = DynamicVector<Vector<3, T> >(structure.oRepCount);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Implementation::SubtractWTDl(delta.o, jtj.wTO, delta.t, structure.wTO);
  Implementation::SubtractWTDl(delta.o, jtj.wCO, delta.c, structure.wCO);
  Implementation::SubtractWTDl(delta.o, jtj.wDO, delta.d, structure.wDO);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < structure.oRepCount; ++i)
  {
    delta.o.At(i) = jtj.vOO.At(i) * (epsilon.o.At(i) + delta.o.At(i));
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return delta;
}

//------------------------------------------------------------------------------
template <typename T>
inline void UpdateImplementation(
  MetricOptimizer::OptimizationData& scene,
  MetricOptimizer::Vectors const& delta)
{
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < delta.t.Size(); ++i)
  {
    Vector6d const& d = delta.t.At(i);

    Vector3d const deltaR(d.At(0), d.At(1), d.At(2));
    Vector3d const deltaT(d.At(3), d.At(4), d.At(5));
      
    Transformation& tRep = scene.tRep.at(i);
      
    tRep.Rotation() =
      QF::FromEulerAnglesYXZr(QF::EulerAnglesYXZr(tRep.Rotation()) + deltaR);
    
    tRep.Translation() += deltaT;
  }
        
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // apply camera delta
  for (uint64 i = 0; i < delta.c.Size(); ++i)
  {
    Vector5d const& d = delta.c.At(i);
      
    Camera& cRep = scene.cRep.at(i);
      
    cRep.FocalLength       () += d.At(0);
    cRep.PrincipalPoint().X() += d.At(1);
    cRep.PrincipalPoint().Y() += d.At(2);
    cRep.PixelAspectRatio  () += d.At(3);
    cRep.Skew              () += d.At(4);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // apply distortion delta
  for (uint64 i = 0; i < delta.d.Size(); ++i)
  {
    Vector8d const& d = delta.d.At(i);
      
    Distortion& dRep = scene.dRep.at(i);

    dRep.P().X()   += d.At(0);
    dRep.P().Y()   += d.At(1);

    dRep.K().At(0) += d.At(2);
    dRep.K().At(1) += d.At(3);
    dRep.K().At(2) += d.At(4);
    dRep.K().At(3) += d.At(5);
    dRep.K().At(4) += d.At(6);
    dRep.K().At(5) += d.At(7);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // apply object point delta
  for (uint64 i = 0; i < delta.o.Size(); ++i)
  {
    scene.oRep.at(i) += delta.o.At(i);
  }
}

//==============================================================================
// Unnamed namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
