//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2014  TrackMen Ltd.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_optimizer_Optimizer_inline_h_
#define dlm_optimizer_Optimizer_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/optimizer/Optimizer.h"

//------------------------------------------------------------------------------
#include "dlm/optimizer/CostFunctions.h"
#include "dlm/optimizer/CostFunctions.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Nested
//------------------------------------------------------------------------------
// Constructors
//------------------------------------------------------------------------------
template <typename OptimizationData_,
          typename SystemMatrix_,
          typename Vectors_>
inline Optimizer<OptimizationData_,
                 SystemMatrix_,
                 Vectors_>::OptimizationParameters::OptimizationParameters(
  AugmentationStrategy const augmentationStrategy_,
  FloatType const defaultLambda_,
  FloatType const lambdaMultiplierOnSuccess_,
  FloatType const lambdaMultiplierOnFailure_,
  FloatType const minimumResidual_,
  FloatType const epsilonTolerance_,
  uint64 const maxIterations_,
  uint64 const maxFailures_,
  uint64 const maxEpsilonImprovements_)
  : defaultLambda            (defaultLambda_            )
  , lambdaMultiplierOnSuccess(lambdaMultiplierOnSuccess_)
  , lambdaMultiplierOnFailure(lambdaMultiplierOnFailure_)
  , minimumResidual          (minimumResidual_          )
  , epsilonTolerance         (epsilonTolerance_         )
  , maxIterations            (maxIterations_            )
  , maxFailures              (maxFailures_              )
  , maxEpsilonImprovements   (maxEpsilonImprovements_   )
  , augmentationStrategy     (augmentationStrategy_     )
{}

//------------------------------------------------------------------------------
template <typename OptimizationData_,
          typename SystemMatrix_,
          typename Vectors_>
inline Optimizer<OptimizationData_,
                 SystemMatrix_,
                 Vectors_>::IterationSummary::IterationSummary(
  uint64 const iteration_,
  FloatType const initialRMSE_,
  FloatType const initialCost_,
  FloatType const initialLambda_)
  : initialRMSE  (initialRMSE_               )
  , currentRMSE  (static_cast<FloatType>(0.0))
  , initialCost  (initialCost_               )
  , currentCost  (static_cast<FloatType>(0.0))
  , initialLambda(initialLambda_             )
  , currentLambda(static_cast<FloatType>(0.0))
  , iteration                 (iteration_)
  , currentFailures           (0         )
  , overallFailures           (0         )
  , currentEpsilonImprovements(0         )
  , overallEpsilonImprovements(0         )
  , result(IterationResult::Failure)
  , epsilonImprovement  (false)
  , optimizationComplete(false)
  , optimizationSuccess (false)
  , externalBreak       (false)
{}


//==============================================================================
// Constructors
//------------------------------------------------------------------------------
template <typename OptimizationData_,
          typename SystemMatrix_,
          typename Vectors_>
inline Optimizer<OptimizationData_,
                 SystemMatrix_,
                 Vectors_>::Optimizer(OptimizationParameters const& parameters)
  : parameters_                (parameters)
  , costFunction_              (Squared() )
  , currentLambda_             (0.0       )
  , currentFailures_           (0         )
  , overallFailures_           (0         )
  , currentEpsilonImprovements_(0         )
  , overallEpsilonImprovements_(0         )
  , currentIteration_          (0         )
  , mayResume_                 (false     )
{}

//==============================================================================
// Destructor
//------------------------------------------------------------------------------
template <typename OptimizationData_,
          typename SystemMatrix_,
          typename Vectors_>
inline Optimizer<OptimizationData_,
                 SystemMatrix_,
                 Vectors_>::~Optimizer()
{}

//==============================================================================
// Accessors
//------------------------------------------------------------------------------
template <typename OptimizationData_,
          typename SystemMatrix_,
          typename Vectors_>
inline typename Optimizer<OptimizationData_,
                          SystemMatrix_,
                          Vectors_>::OptimizationParameters const&
  Optimizer<OptimizationData_,
            SystemMatrix_,
            Vectors_>::Parameters() const
{
  return parameters_;
}

//==============================================================================
// Mutators
//------------------------------------------------------------------------------
template <typename OptimizationData_,
          typename SystemMatrix_,
          typename Vectors_>
inline typename Optimizer<OptimizationData_,
                          SystemMatrix_,
                          Vectors_>::OptimizationParameters&
  Optimizer<OptimizationData_,
            SystemMatrix_,
            Vectors_>::Parameters()
{
  return parameters_;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
template <typename OptimizationData_,
          typename SystemMatrix_,
          typename Vectors_>
inline std::vector<typename Optimizer<OptimizationData_,
                                      SystemMatrix_,
                                      Vectors_>::IterationSummary>
  Optimizer<OptimizationData_,
            SystemMatrix_,
            Vectors_>::Optimize(Scene& scene, bool const resume)
{
  if (resume)
  {
    DLM_ASSERT(mayResume_, DLM_RUNTIME_ERROR);

    FloatType const lambda = currentLambda_;

    Reset();

    currentLambda_ = lambda;
  }
  else
  {
    Reset();

    data_ = Pack(scene);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  mayResume_ = true;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  std::vector<IterationSummary> results;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (;;)
  {
    results.push_back(Iterate());

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    bool requestBreak = false;

    if (results.back().result == IterationResult::NumericsProblem)
    {
      requestBreak = true;
    }

    if (results.back().result == IterationResult::InitialResidualTooLow)
    {
      results.back().optimizationComplete = true;
      results.back().optimizationSuccess  = true;

      requestBreak = true;
    }

    if (currentFailures_ > parameters_.maxFailures)
    {
      results.back().optimizationComplete = true;

      requestBreak = true;
    }

    if (currentEpsilonImprovements_ > parameters_.maxEpsilonImprovements)
    {
      results.back().optimizationComplete = true;
      results.back().optimizationSuccess  = true;

      requestBreak = true;
    }

    if (currentIteration_ > parameters_.maxIterations)
    {
      results.back().optimizationComplete = true;

      requestBreak = true;
    }

    if (callback_)
    {
      bool const result = callback_(results.back());

      if (!result)
      {
        results.back().externalBreak = true;

        requestBreak = true;
      }
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    if (requestBreak)
    {
      break;
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (results.back().optimizationSuccess)
  {
    Unpack(data_, scene);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return results;
}

//------------------------------------------------------------------------------
template <typename OptimizationData_,
          typename SystemMatrix_,
          typename Vectors_>
inline void Optimizer<OptimizationData_,
                      SystemMatrix_,
                      Vectors_>::Extract(Scene& scene)
{
  Unpack(data_, scene);
}

//------------------------------------------------------------------------------
template <typename OptimizationData_,
          typename SystemMatrix_,
          typename Vectors_>
inline void Optimizer<OptimizationData_,
                      SystemMatrix_,
                      Vectors_>::SetCallback(CallbackFunction const& callback)
{
  callback_ = callback;
}

//------------------------------------------------------------------------------
template <typename OptimizationData_,
          typename SystemMatrix_,
          typename Vectors_>
inline void Optimizer<OptimizationData_,
                      SystemMatrix_,
                      Vectors_>::SetCostFunction(
  CostFunction const& costFunction)
{
  DLM_ASSERT(costFunction, DLM_RUNTIME_ERROR);

  costFunction_ = costFunction;
}

//------------------------------------------------------------------------------
template <typename OptimizationData_,
          typename SystemMatrix_,
          typename Vectors_>
inline typename Optimizer<OptimizationData_,
                 SystemMatrix_,
                 Vectors_>::IterationSummary
  Optimizer<OptimizationData_,
            SystemMatrix_,
            Vectors_>::Iterate()
{
  using namespace dlm;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  ++currentIteration_;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  SystemMatrix A;
  Vectors      b;

  auto const initialCost = CreateSystem(data_, A, b, costFunction_);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  IterationSummary summary(
    currentIteration_, initialCost.first, initialCost.second, currentLambda_);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (initialCost.second < parameters_.minimumResidual)
  {
    summary.result = IterationResult::InitialResidualTooLow;

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    return summary;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (parameters_.augmentationStrategy == AugmentationStrategy::Multiplicative)
  {
    AugmentMultiplicative(A, static_cast<FloatType>(1.0) + currentLambda_);
  }
  else
  {
    if (currentIteration_ == 1)
    {
      FloatType const scale = LambdaScaleHint(A);

      DLM_ASSERT(scale > Epsilon, DLM_RUNTIME_ERROR);

      currentLambda_ *= scale;
    }

    AugmentAdditive(A, currentLambda_);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Vectors delta;

  try
  {
    delta = Solve(A, b);
  }
  catch (DomainError const&)
  {
    summary.result = IterationResult::NumericsProblem;

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    return summary;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  OptimizationData copy = data_;

  Update(data_, delta);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  auto const currentCost = Evaluate(data_, costFunction_);

  summary.currentRMSE = currentCost.first;
  summary.currentCost = currentCost.second;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (currentCost.second < initialCost.second)
  {
    currentLambda_ *= parameters_.lambdaMultiplierOnSuccess;

    summary.result = IterationResult::Success;
  }
  else
  {
    data_ = copy;

    currentLambda_ *= parameters_.lambdaMultiplierOnFailure;

    summary.result = IterationResult::Failure;
  }

  summary.currentLambda = currentLambda_;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FloatType const reduction =
    static_cast<FloatType>(1.0) - currentCost.second / initialCost.second;

  if (reduction > static_cast<FloatType>(0.0))
  {
    currentFailures_ = 0;

    if (reduction < parameters_.epsilonTolerance)
    {
      ++currentEpsilonImprovements_;
      ++overallEpsilonImprovements_;

      summary.epsilonImprovement = true;
    }
    else
    {
      currentEpsilonImprovements_ = 0;
    }
  }
  else
  {
    ++currentFailures_;
    ++overallFailures_;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  summary.currentFailures = currentFailures_;
  summary.overallFailures = overallFailures_;

  summary.currentEpsilonImprovements = currentEpsilonImprovements_;
  summary.overallEpsilonImprovements = overallEpsilonImprovements_;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return summary;
}

//------------------------------------------------------------------------------
template <typename OptimizationData_,
          typename SystemMatrix_,
          typename Vectors_>
inline void Optimizer<OptimizationData_,
                      SystemMatrix_,
                      Vectors_>::Reset()
{
  currentLambda_ = parameters_.defaultLambda;

  currentIteration_           = 0;

  currentFailures_            = 0;
  overallFailures_            = 0;
  currentEpsilonImprovements_ = 0;
  overallEpsilonImprovements_ = 0;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
