//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
//------------------------------------------------------------------------------
// Copyright (C)  2014  TrackMen Ltd.
//
//------------------------------------------------------------------------------
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz


//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_optimizer_CommonFunctions_h_
#define dlm_optimizer_CommonFunctions_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/flags/Flags.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Matrix.h"
#include "dlm/math/DynamicVector.h"
#include "dlm/math/DynamicMatrix.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Types.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <vector>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Implementation namespace
//------------------------------------------------------------------------------
namespace Implementation
{

//==============================================================================
// Functions
//------------------------------------------------------------------------------
template <uint32 SIZE>
std::vector<uint64> ObtainLayout(
  std::vector<Flags<SIZE> > const& flags,
  uint64                         & currentSize,
  uint64                         & optimized);

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
void Cull(Matrix<2, SIZE, T>& matrix, Flags<SIZE> const& flags);

//------------------------------------------------------------------------------
template <uint32 DIM, typename T>
void MultiplicativeUpdate(Matrix<DIM, DIM, T>& matrix, T const value);

//------------------------------------------------------------------------------
template <uint32 DIM, typename T>
void MultiplicativeUpdate(
  DynamicMatrix<Matrix<DIM, DIM, T> >& matrix, T const value);

//------------------------------------------------------------------------------
template <uint32 DIM, typename T>
void AdditiveUpdate(Matrix<DIM, DIM, T>& matrix, T const value);

//------------------------------------------------------------------------------
template <uint32 DIM, typename T>
void AdditiveUpdate(
  DynamicMatrix<Matrix<DIM, DIM, T> >& matrix, T const value);

//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, uint32 DIM3, typename T, typename MatrixData>
void SubtractYWT(
  DynamicMatrix<Matrix<ROWS, COLS, T> >      & u,
  DynamicVector<Matrix<DIM3, DIM3, T> > const& vVector,
  DynamicVector<Matrix<ROWS, DIM3, T> > const& w0,
  DynamicVector<Matrix<COLS, DIM3, T> > const& w1,
  MatrixData                     const& w0Data,
  MatrixData                     const& w1Data,
  bool                                            const  reduce = false);

//------------------------------------------------------------------------------
template <uint32 SIZE, uint32 DIM2, typename T, typename MatrixData>
void SubtractYEpO(
  DynamicVector<Vector<SIZE,       T> >      & e,
  DynamicVector<Matrix<SIZE, DIM2, T> > const& w,
  DynamicVector<Matrix<DIM2, DIM2, T> > const& v,
  DynamicVector<Vector<DIM2,       T> > const& epO,
  MatrixData                     const& wData);

//----------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
void Insert(
  DynamicMatrix<T>                           & s,
  DynamicMatrix<Matrix<ROWS, COLS, T> > const& jtj,
  std::vector<uint64>                   const& writeIdx0,
  std::vector<uint64>                   const& writeIdx1,
  bool                                  const  reduce = false);

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
void Insert(
  DynamicVector<T>                          & e,
  DynamicVector<Vector<SIZE, T> > const& epsilon,
  std::vector<uint64>                  const& writeIdx);

//------------------------------------------------------------------------------
template <unsigned SIZE, typename T>
DynamicVector<Vector<SIZE, T> > Extract(
  DynamicVector<T> const& e, std::vector<uint64> const& indices);

//------------------------------------------------------------------------------
template <uint32 SIZE, uint32 DIM2, typename T, typename MatrixData>
void SubtractWTDl(
  DynamicVector<Vector<DIM2,       T> >      & dlO,
  DynamicVector<Matrix<SIZE, DIM2, T> > const& w,
  DynamicVector<Vector<SIZE,       T> > const& d,
  MatrixData                                      const& wData);

//==============================================================================
// Implementation namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
