//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
//------------------------------------------------------------------------------
// Copyright (C)  2014  TrackMen Ltd.
//
//------------------------------------------------------------------------------
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_optimizer_Optimizer_h_
#define dlm_optimizer_Optimizer_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/core/Types.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <vector>
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <functional>

//==============================================================================
// Forward declarations
//------------------------------------------------------------------------------
namespace dlm
{
class Scene;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Optimizer class
//------------------------------------------------------------------------------
template <typename OptimizationData_,
          typename SystemMatrix_,
          typename Vectors_>
class Optimizer
{
public:
  //============================================================================
  // Nested
  //----------------------------------------------------------------------------
  // Type definitions
  //----------------------------------------------------------------------------
  typedef OptimizationData_ OptimizationData;
  typedef SystemMatrix_     SystemMatrix;
  typedef Vectors_          Vectors;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  typedef Optimizer<OptimizationData_,
                    SystemMatrix_,
                    Vectors_> BaseClass;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  typedef std::function<FloatType(FloatType const)> CostFunction;

  //----------------------------------------------------------------------------
  // AugmentationStrategy enum
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Specifies whether the main diagonal should be augmented as x = x + lambda
  // for Additive update, or x = x * (1 + lambda) for Multiplicative update.
  // For the additive update, the initial lambda is calculated based on the
  // average of the diagonal elements of the system matrix (see Hartley and
  // Zisserman, Multiple View Geometry, Second Edition, A6.2, page 600).
  //----------------------------------------------------------------------------
  enum AugmentationStrategy
  {
    Additive,
    Multiplicative
  };

  //----------------------------------------------------------------------------
  // IterationResult enum
  //----------------------------------------------------------------------------
  enum IterationResult
  {
    Success,
    Failure,
    InitialResidualTooLow,
    NumericsProblem
  };

  //----------------------------------------------------------------------------
  // OptimizationParameters struct
  //----------------------------------------------------------------------------
  struct OptimizationParameters
  {
    //--------------------------------------------------------------------------
    // Constructors
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    OptimizationParameters(
      AugmentationStrategy augmentationStrategy_ = AugmentationStrategy::Multiplicative,
      FloatType const defaultLambda_             = static_cast<FloatType>(1e-3),
      FloatType const lambdaMultiplierOnSuccess_ = static_cast<FloatType>(0.5),
      FloatType const lambdaMultiplierOnFailure_ = static_cast<FloatType>(2.0),
      FloatType const minimumResidual_  = static_cast<FloatType>(1e-6),
      FloatType const epsilonTolerance_ = static_cast<FloatType>(1e-3),
      uint64 const maxIterations_          = 500,
      uint64 const maxFailures_            = 10,
      uint64 const maxEpsilonImprovements_ = 4);

    //--------------------------------------------------------------------------
    // Member variables
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    FloatType defaultLambda;
    FloatType lambdaMultiplierOnSuccess;
    FloatType lambdaMultiplierOnFailure;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    FloatType minimumResidual;
    FloatType epsilonTolerance;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    uint64 maxIterations;
    uint64 maxFailures;
    uint64 maxEpsilonImprovements;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    AugmentationStrategy augmentationStrategy;
  };

  //----------------------------------------------------------------------------
  // IterationSummary struct
  //----------------------------------------------------------------------------
  struct IterationSummary
  {
    //--------------------------------------------------------------------------
    // Constructors
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    IterationSummary(
      uint64 const iteration_ = 0,
      FloatType const initialRMSE_   = static_cast<FloatType>(0.0),
      FloatType const initialCost_   = static_cast<FloatType>(0.0),
      FloatType const initialLambda_ = static_cast<FloatType>(0.0));

    //--------------------------------------------------------------------------
    // Member variables
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    FloatType initialRMSE;
    FloatType currentRMSE;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    FloatType initialCost;
    FloatType currentCost;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    FloatType initialLambda;
    FloatType currentLambda;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    uint64 iteration;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    uint64 currentFailures;
    uint64 overallFailures;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    uint64 currentEpsilonImprovements;
    uint64 overallEpsilonImprovements;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    IterationResult result;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    bool epsilonImprovement;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    bool optimizationComplete;
    bool optimizationSuccess;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    bool externalBreak;
  };

  //----------------------------------------------------------------------------
  // Type definitions
  //----------------------------------------------------------------------------
  typedef std::function<bool(IterationSummary const&)> CallbackFunction;

  //============================================================================
  // Constructors
  //----------------------------------------------------------------------------
  Optimizer(OptimizationParameters const& parameters =
    OptimizationParameters());

  //============================================================================
  // Destructor
  //----------------------------------------------------------------------------
  virtual ~Optimizer();

  //============================================================================
  // Accessors
  //----------------------------------------------------------------------------
  OptimizationParameters const& Parameters() const;

  //============================================================================
  // Mutators
  //----------------------------------------------------------------------------
  OptimizationParameters& Parameters();

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  virtual std::vector<IterationSummary> Optimize(
    Scene& scene, bool const resume = false) final;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  virtual void Extract(Scene& scene) final;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void SetCallback(CallbackFunction const& callback);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void SetCostFunction(CostFunction const& costFunction);

protected:
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  virtual OptimizationData Pack(Scene const& scene) = 0;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  virtual std::pair<FloatType, FloatType> CreateSystem(
    OptimizationData const& data,
    SystemMatrix& A, Vectors& b,
    CostFunction const& costFunction) = 0;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  virtual FloatType LambdaScaleHint(SystemMatrix const& A) = 0;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  virtual void AugmentAdditive(SystemMatrix& A, FloatType const value) = 0;
  virtual void AugmentMultiplicative(SystemMatrix& A, FloatType const value) = 0;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  virtual Vectors Solve(SystemMatrix& A, Vectors& b) = 0;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  virtual void Update(OptimizationData& data, Vectors const& delta) = 0;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  virtual std::pair<FloatType, FloatType> Evaluate(
    OptimizationData const& data,
    CostFunction const& costFunction) = 0;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  virtual void Unpack(OptimizationData const& data, Scene& scene) = 0;

private:
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  IterationSummary Iterate();
  void Reset();

  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  OptimizationParameters parameters_;

  //----------------------------------------------------------------------------
  OptimizationData data_;

  //----------------------------------------------------------------------------
  CallbackFunction callback_;

  //----------------------------------------------------------------------------
  CostFunction costFunction_;

  //----------------------------------------------------------------------------
  FloatType currentLambda_;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint64 currentFailures_;
  uint64 overallFailures_;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint64 currentEpsilonImprovements_;
  uint64 overallEpsilonImprovements_;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint64 currentIteration_;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool mayResume_;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
