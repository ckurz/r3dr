//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
//------------------------------------------------------------------------------
// Copyright (C)  2014  TrackMen Ltd.
//
//------------------------------------------------------------------------------
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_optimizer_ProjectiveOptimizer_h_
#define dlm_optimizer_ProjectiveOptimizer_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/optimizer/Optimizer.h"
#include "dlm/optimizer/Optimizer.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/Identifiers.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/flags/Flags.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/entity/FeaturePoint.h"
#include "dlm/entity/ProjectiveCamera.h"
#include "dlm/entity/ObjectPoint.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/DynamicVector.h"
#include "dlm/math/DynamicMatrix.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Types.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <vector>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
//
//------------------------------------------------------------------------------
struct ProjectiveOptimizerOptimizationData
{
  std::vector<ProjectiveCamera> pRep;
  std::vector<ObjectPoint     > oRep;
};

struct ProjectiveOptimizerSystemMatrix
{
  //----------------------------------------------------------------------------
  // Member variables
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // U matrix
  DynamicMatrix<Matrix12d  > uPP;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // V matrix
  DynamicVector<Matrix3d   > vOO;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // W matrix
  DynamicVector<Matrix12x3d> wPO;
};

struct ProjectiveOptimizerVectors
{
  //----------------------------------------------------------------------------
  // Member variables
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicVector<Vector12d> p;
  DynamicVector<Vector3d > o;
};

//==============================================================================
// ProjectiveOptimizer class
//------------------------------------------------------------------------------
class ProjectiveOptimizer : public Optimizer<ProjectiveOptimizerOptimizationData,
                                             ProjectiveOptimizerSystemMatrix,
                                             ProjectiveOptimizerVectors>
{
public:
  //============================================================================
  // Nested
  //----------------------------------------------------------------------------
  // MatrixLayout struct
  //----------------------------------------------------------------------------
  struct MatrixLayout
  {
    //--------------------------------------------------------------------------
    // Constructors
    //--------------------------------------------------------------------------
    MatrixLayout()
      : size(0)
    {}

    //--------------------------------------------------------------------------
    // Member variables
    //--------------------------------------------------------------------------
    std::vector<uint64> pRepIdx;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    uint64 size;
  };

  //----------------------------------------------------------------------------
  // MatrixData struct
  //----------------------------------------------------------------------------
  struct MatrixData
  {
    //--------------------------------------------------------------------------
    // Constructors
    //--------------------------------------------------------------------------
    MatrixData()
      : size(0)
    {}

    //--------------------------------------------------------------------------
    // Member variables
    //--------------------------------------------------------------------------
    std::vector<uint64> rowEnd;
    std::vector<uint64> colIdx;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    uint64 size;
  };
  
  //----------------------------------------------------------------------------
  // Structure struct
  //----------------------------------------------------------------------------
  struct Structure
  {
    //--------------------------------------------------------------------------
    // Constructors
    //--------------------------------------------------------------------------
    Structure()
      : pRepCount         (0)
      , oRepCount         (0)
      , pRepOptimizedCount(0)
      , oRepOptimizedCount(0)
    {}

    //--------------------------------------------------------------------------
    // Member variables
    //--------------------------------------------------------------------------
    MatrixLayout s;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    MatrixData wPO;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    uint64 pRepCount;
    uint64 oRepCount;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    uint64 pRepOptimizedCount;
    uint64 oRepOptimizedCount;
  };

  //----------------------------------------------------------------------------
  // FRepData struct
  //----------------------------------------------------------------------------
  struct FRepData
  {
    //--------------------------------------------------------------------------
    // Constructors
    //--------------------------------------------------------------------------
    FRepData(Vector2f const& position_ = Vector2f())
      : position(position_)
      , pRepId  (Invalid)
      , oRepId  (Invalid)
      , wPOIndex(Invalid)
      , hasWMat (false  )
    {}


    //--------------------------------------------------------------------------
    // Member variables
    //--------------------------------------------------------------------------
    Vector2f position;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    uint64 pRepId;
    uint64 oRepId;
    uint64 wMatId;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    uint64 wPOIndex;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    bool hasWMat;
  };

  //----------------------------------------------------------------------------
  // MetaData struct
  //----------------------------------------------------------------------------
  struct MetaData
  {
    //--------------------------------------------------------------------------
    // Member variables
    //--------------------------------------------------------------------------
    std::vector<FRepData> fRep;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    std::vector<Matrix2f> wMat;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    std::vector<Flags<12> > pRepFlags;
    std::vector<Flags< 3> > oRepFlags;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    std::vector<PRepId> pRepId;
    std::vector<ORepId> oRepId;
    std::vector<WMatId> wMatId;
  };

  //----------------------------------------------------------------------------
  // Denormalization struct
  //----------------------------------------------------------------------------
  struct Normalization
  {
    Matrix4f objectPoint;
    std::vector<Matrix3f> featurePoint;
  };

  //============================================================================
  // Constructors
  //----------------------------------------------------------------------------
  ProjectiveOptimizer(
    bool const normalizeFeaturePoints = false,
    bool const normalizeObjectPoints  = false,
    BaseClass::OptimizationParameters const& parameters =
      BaseClass::OptimizationParameters());

  //============================================================================
  // Destructor
  //----------------------------------------------------------------------------
  virtual ~ProjectiveOptimizer();

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  virtual OptimizationData Pack(Scene const& scene);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  virtual std::pair<FloatType, FloatType> CreateSystem(
    OptimizationData const& data,
    SystemMatrix& A, Vectors& b,
    CostFunction const& costFunction); 

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  virtual FloatType LambdaScaleHint(SystemMatrix const& A);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  virtual void AugmentAdditive      (SystemMatrix& A, FloatType const value);
  virtual void AugmentMultiplicative(SystemMatrix& A, FloatType const value);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  virtual Vectors Solve(SystemMatrix& A, Vectors& b);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  virtual void Update(OptimizationData& data, Vectors const& delta);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  virtual std::pair<FloatType, FloatType> Evaluate(
    OptimizationData const& data,
    CostFunction const& costFunction);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  virtual void Unpack(OptimizationData const& data, Scene& scene);

private:
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  Structure structure_;
  MetaData metaData_;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Normalization   normalization_;
  Normalization denormalization_;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool normalizeFeaturePoints_;
  bool normalizeObjectPoints_;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
