//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/svd/SVD.h"

//------------------------------------------------------------------------------
#include "dlm/math/DynamicVector.inline.h"
#include "dlm/math/DynamicMatrix.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/adapter/EigenAdapter.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
SVD::Result SVD::Decompose(
  DMatrixd const& matrix, Mode const uMode, Mode const vMode)
{
  typedef Eigen::Matrix<float64, Eigen::Dynamic, Eigen::Dynamic> MatrixType;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  MatrixType mCopy = EA::Convert<float64>(matrix);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  unsigned int computationOptions = 0; // No custom type because of Eigen3.
  
  if (uMode == Thin) { computationOptions |= Eigen::ComputeThinU; }
  if (uMode == Full) { computationOptions |= Eigen::ComputeFullU; }

  if (vMode == Thin) { computationOptions |= Eigen::ComputeThinV; }
  if (vMode == Full) { computationOptions |= Eigen::ComputeFullV; }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Eigen::JacobiSVD<MatrixType> svd(mCopy, computationOptions);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Result result;

  result.Sigma = EA::Convert<float64>(svd.singularValues());

  if (uMode == Thin || uMode == Full)
  {
    result.U = EA::Convert<float64>(svd.matrixU());
  }

  if (vMode == Thin || vMode == Full)
  {
    result.V = EA::Convert<float64>(svd.matrixV());
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
DVectord SVD::Solve(DMatrixd const& matrix, DVectord const& vector)
{
  typedef Eigen::Matrix<float64, Eigen::Dynamic, Eigen::Dynamic> MatrixType;
  typedef Eigen::Matrix<float64, Eigen::Dynamic, 1             > VectorType;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  MatrixType mCopy = EA::Convert<float64>(matrix);
  VectorType vCopy = EA::Convert<float64>(vector);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Eigen::JacobiSVD<MatrixType> svd(mCopy,
    Eigen::ComputeThinU | Eigen::ComputeThinV);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  VectorType result = svd.solve(vCopy);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return EA::Convert<float64>(result);
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm
