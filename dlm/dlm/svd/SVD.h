//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_svd_SVD_h_
#define dlm_svd_SVD_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/math/DynamicVector.h"
#include "dlm/math/DynamicMatrix.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// SVD class
//------------------------------------------------------------------------------
class SVD
{
public:
  //============================================================================
  // Nested
  //----------------------------------------------------------------------------
  enum Mode
  {
    Omit,
    Thin,
    Full
  };

  //----------------------------------------------------------------------------
  struct Result
  {
    DMatrixd U;
    DVectord Sigma;
    DMatrixd V;
  };

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  static Result Decompose(
    DMatrixd const& matrix, Mode const uMode = Omit, Mode const vMode = Omit);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  static DVectord Solve(DMatrixd const& matrix, DVectord const& vector);
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_svd_SVD_h_
