//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// Copyright (C)  2018  TrackMen Ltd.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_math_DynamicVector_inline_h_
#define dlm_math_DynamicVector_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/math/DynamicVector.h"

//------------------------------------------------------------------------------
#include "dlm/core/AutoArrayPtr.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//#include <type_traits>
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//#include <cmath>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
template <typename T>
inline DynamicVector<T>::DynamicVector()
  : elements_(0)
  , size_    (0)
{}

//------------------------------------------------------------------------------
template <typename T>
inline DynamicVector<T>::DynamicVector(uint64 const size)
  : elements_(new T[size]()) //value-intialization
  , size_    (size         )
{}

//------------------------------------------------------------------------------
template <typename T>
inline DynamicVector<T>::DynamicVector(DynamicVector const& other)
  : elements_(new T[other.size_])
  , size_    (other.size_       )
{
  for (uint64 i = 0; i < other.size_; ++i)
  {
    elements_[i] = other.elements_[i];
  }
}

//==============================================================================
// Operators
//------------------------------------------------------------------------------
template <typename T>
inline DynamicVector<T>& DynamicVector<T>::operator=(DynamicVector const& other)
{
  //gracefully handle self-assignment
  AutoArrayPtr<T> newElements(new T[other.size_]);

  for (unsigned i = 0; i < other.size_; ++i)
  {
    newElements[i] = other.elements_[i];
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  size_     = other.size_;

  elements_ = newElements;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
template <typename T>
inline
DynamicVector<T>
DynamicVector<T>::operator+(
  DynamicVector const& other) const
{
  return DynamicVector(*this) += other;
}

//------------------------------------------------------------------------------
template <typename T>
inline
DynamicVector<T>
DynamicVector<T>::operator-(
  DynamicVector const& other) const
{
  return DynamicVector(*this) -= other;
}

//------------------------------------------------------------------------------
template <typename T>
inline
DynamicVector<T>&
DynamicVector<T>::operator+=(
  DynamicVector const& other)
{
  for (auto i = static_cast<uint64>(0); i < size_; ++i)
  {
    elements_[i] += other.elements_[i];
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
template <typename T>
inline
DynamicVector<T>&
DynamicVector<T>::operator-=(
  DynamicVector const& other)
{
  for (auto i = static_cast<uint64>(0); i < size_; ++i)
  {
    elements_[i] -= other.elements_[i];
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
template <typename T>
inline
DynamicVector<T>
DynamicVector<T>::operator*(
  T const scalar) const
{
  return DynamicVector(*this) *= scalar;
}

//------------------------------------------------------------------------------
template <typename T>
inline
DynamicVector<T>
DynamicVector<T>::operator/(
  T const scalar) const
{
  return DynamicVector(*this) /= scalar;
}

//------------------------------------------------------------------------------
template <typename T>
inline
DynamicVector<T>&
DynamicVector<T>::operator*=(
  T const scalar)
{
  for (auto i = static_cast<uint64>(0); i < size_; ++i)
  {
    elements_[i] *= scalar;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
template <typename T>
inline
DynamicVector<T>&
DynamicVector<T>::operator/=(
  T const scalar)
{
  static_assert(std::is_floating_point<T>::value,
                "Only defined for floating point types.");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DLM_ASSERT(scalar != T(), DLM_DOMAIN_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (auto i = static_cast<uint64>(0); i < size_; ++i)
  {
    elements_[i] /= scalar;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
template <typename T>
inline
DynamicVector<T>
DynamicVector<T>::operator-() const
{
  DynamicVector result(*this);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (auto i = static_cast<uint64>(0); i < size_; ++i)
  {
    result.elements_[i] = -result.elements_[i];
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//==============================================================================
// Operators - Accessors
//------------------------------------------------------------------------------
template <typename T>
inline T const& DynamicVector<T>::operator()(uint64 const i) const
{
  DLM_ASSERT(i < size_, DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[i];
}

//==============================================================================
// Operators - Mutators
//------------------------------------------------------------------------------
template <typename T>
inline T& DynamicVector<T>::operator()(uint64 const i)
{
  DLM_ASSERT(i < size_, DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[i];
}

//==============================================================================
// Accessors
//------------------------------------------------------------------------------
template <typename T>
inline uint64 DynamicVector<T>::Size() const
{
  return size_;
}

//------------------------------------------------------------------------------
template <typename T>
inline T const& DynamicVector<T>::At(uint64 const i) const
{
  DLM_ASSERT(i < size_, DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[i];
}

//------------------------------------------------------------------------------
template <typename T>
inline
T const&
DynamicVector<T>::X() const
{
  DLM_ASSERT(Size() > 0, DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[0];
}

//------------------------------------------------------------------------------
template <typename T>
inline
T const&
DynamicVector<T>::Y() const
{
  DLM_ASSERT(Size() > 1, DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[1];
}

//------------------------------------------------------------------------------
template <typename T>
inline
T const&
DynamicVector<T>::Z() const
{
  DLM_ASSERT(Size() > 2, DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[2];
}

//------------------------------------------------------------------------------
template <typename T>
inline
T const&
DynamicVector<T>::W() const
{
  DLM_ASSERT(Size() > 3, DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[3];
}

//==============================================================================
// Mutators
//------------------------------------------------------------------------------
template <typename T>
inline T& DynamicVector<T>::At(uint64 const i)
{
  DLM_ASSERT(i < size_, DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[i];
}

//------------------------------------------------------------------------------
template <typename T>
inline
T&
DynamicVector<T>::X()
{
  DLM_ASSERT(Size() > 0, DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[0];
}

//------------------------------------------------------------------------------
template <typename T>
inline
T&
DynamicVector<T>::Y()
{
  DLM_ASSERT(Size() > 1, DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[1];
}

//------------------------------------------------------------------------------
template <typename T>
inline
T&
DynamicVector<T>::Z()
{
  DLM_ASSERT(Size() > 2, DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[2];
}

//------------------------------------------------------------------------------
template <typename T>
inline
T&
DynamicVector<T>::W()
{
  DLM_ASSERT(Size() > 3, DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[3];
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
template <typename T>
inline
T
DynamicVector<T>::Dot(
  DynamicVector const& other) const
{
  DLM_ASSERT(Size() == other.Size(), DLM_RUNTIME_ERROR);

  T result = 0;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < Size(); ++i)
  {
    result += elements_[i] * other.elements_[i];
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <typename T>
inline
DynamicVector<T>
DynamicVector<T>::Cross(
  DynamicVector const& other) const
{
  DLM_ASSERT(Size()       == 3, DLM_RUNTIME_ERROR);
  DLM_ASSERT(other.Size() == 3, DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return DynamicVector(elements_[1] * other.elements_[2] -
                       elements_[2] * other.elements_[1],
                       elements_[2] * other.elements_[0] -
                       elements_[0] * other.elements_[2],
                       elements_[0] * other.elements_[1] -
                       elements_[1] * other.elements_[0]);
}

//------------------------------------------------------------------------------
template <typename T>
inline
T
DynamicVector<T>::Magnitude() const
{
  static_assert(std::is_floating_point<T>::value,
                "Only defined for floating point types.");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return sqrt(Dot(*this));
}

//------------------------------------------------------------------------------
template <typename T>
inline
DynamicVector<T>&
DynamicVector<T>::Normalize()
{
  static_assert(std::is_floating_point<T>::value,
                "Only defined for floating point types.");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this /= Magnitude();
}

//------------------------------------------------------------------------------
template <typename T>
inline
DynamicVector<T>
DynamicVector<T>::Normalized() const
{
  static_assert(std::is_floating_point<T>::value,
                "Only defined for floating point types.");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return DynamicVector(*this).Normalize();
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_math_DynamicVector_inline_h_
