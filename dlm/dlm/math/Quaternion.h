//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_math_Quaternion_h_
#define dlm_math_Quaternion_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include <type_traits>
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Types.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Quaternion template class
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// This implementation assumes the coordinate systems to be right-handed and all
// the rotations to occur counter-clockwise when the axis of rotation points
// towards the observer (i.e., when looking from above, the x-axis points right,
// the y-axis points up, and the z-axis points towards the observer; rotation
// around the z-axis rotates a point lying on the x-axis towards the y-axis).
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Furthermore, the standard convention of rotation order is followed by
// requiring rotations occuring after each other to be ordered from
// right to left in multiplication, with the first rotation that is
// applied to a vector located on the right, and the last on the left.
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Example: starting point (0, 1, 0)
//
// rotation q0 of (0, 1, 0) by pi/2 around x-axis -> (0, 0, 1)
// rotation q1 of (0, 1, 0) by pi/2 around y-axis -> (1, 0, 0)
// rotation q2 of (0, 1, 0) by pi/2 around z-axis -> (0, 1, 0)
//
// combined rotation q3 = q2 * q1 * q0
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// See ftp://ftp.cis.upenn.edu/pub/graphics/shoemake/quatut.ps.Z for a
// good tutorial on quaternions, or look on Wikipedia for Quaternion and
// Rotation matrix.
//------------------------------------------------------------------------------
template <typename T>
class Quaternion
{
  //============================================================================
  // Type restriction
  //----------------------------------------------------------------------------
  static_assert(std::is_floating_point<T>::value,
                "Only for floating point types.");

public:
  //============================================================================
  // Constructors
  //----------------------------------------------------------------------------
  Quaternion(); //sets w to 1 and the vector component to 0
  Quaternion(Vector<3, T> const& xyz, T const w); //just assignment

  //============================================================================
  // Operators
  //----------------------------------------------------------------------------
  // Elementwise addition and subtraction
  Quaternion  operator+ (Quaternion const& other) const;
  Quaternion  operator- (Quaternion const& other) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Quaternion& operator+=(Quaternion const& other);
  Quaternion& operator-=(Quaternion const& other);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Quaternion multiplication
  // Chain multiplications from right to left, i.e., if you first want to rotate
  // around the x-axis with q0 and then around the y-axis with q1, then this
  // rotation can be combined into q2 = q1 * q0.
  Quaternion  operator* (Quaternion const& other) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Quaternion& operator*=(Quaternion const& other);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Vector rotation using the angle/axis-representation of the quaternion;
  // the quaternion is assumed to have been converted to angle/axis
  Vector<3, T>  operator* (Vector<3, T> const& vector) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Quaternion  operator* (T const value) const;
  Quaternion  operator/ (T const value) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Quaternion& operator*=(T const value);
  Quaternion& operator/=(T const value);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Quaternion  operator-() const;

  //============================================================================
  // Accessors
  //----------------------------------------------------------------------------
  Vector<3, T>const& Xyz() const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T W() const;

  //============================================================================
  // Mutators
  //----------------------------------------------------------------------------
  Vector<3, T>& Xyz();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T& W();

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  T Dot(Quaternion const& other) const; //norm if argument is *this
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T Magnitude() const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Normalize the quaternion to unit length
  Quaternion  Normalized() const;
  Quaternion& Normalize ();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Conjugate the quaternion, i.e., reverse the sign of the vector part
  Quaternion  Conjugated() const;
  Quaternion& Conjugate ();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Invert the quaternion, i.e., assign the conjugate and normalize it
  // For unit quaternions, use the conjugate instead.
  Quaternion  Inverted() const;
  Quaternion& Invert  ();

  //============================================================================
  // Friends
  //----------------------------------------------------------------------------
  friend class QuaternionFunctions; //TODO maybe get rid of that
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  friend class BinarySerializer;
  friend class BinaryDeserializer;

private:
  //====================================================================
  // Member variables
  //--------------------------------------------------------------------
  Vector<3, T> xyz_;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T w_;
};

//==============================================================================
// Type definitions
//------------------------------------------------------------------------------
// Quaternions of floating point values
//------------------------------------------------------------------------------
typedef Quaternion<float64> Quatd;

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_math_Quaternion_h_
