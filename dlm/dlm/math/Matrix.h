//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_math_Matrix_h_
#define dlm_math_Matrix_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/core/Types.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Forward declarations
//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
class Vector;

//==============================================================================
//! \brief Fixed-size matrix template class.
// Matrix template class
//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
class Matrix
{
  //============================================================================
  // Size restriction
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Memory for this class is typically allocated on the stack. It is therefore
  // not advisable to let the internal storage grow too large, as this might
  // lead to a stack overflow.
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // If you choose to substantially increase the possible storage capacity of
  // this class (however unlikely that may be), keep in mind that the interface
  // for this class is 32bit and consequently cannot handle more than 2^32
  // elements.
  //----------------------------------------------------------------------------
  static_assert(ROWS * COLS * sizeof(T) <= 32768, "Matrix is too large.");

public:
  //============================================================================
  // Constructors
  //----------------------------------------------------------------------------
  Matrix();

  //============================================================================
  // Operators
  //----------------------------------------------------------------------------
  Matrix  operator+ (Matrix const& other) const;
  Matrix  operator- (Matrix const& other) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Matrix& operator+=(Matrix const& other);
  Matrix& operator-=(Matrix const& other);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template<uint32 DIM>
  Matrix<ROWS, DIM, T> operator*(Matrix<COLS, DIM, T> const& other) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Matrix& operator*=(Matrix<COLS, COLS, T> const& other);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Vector<ROWS, T> operator*(Vector<COLS, T> const& vector) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Matrix  operator* (T const scalar) const;
  Matrix  operator/ (T const scalar) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Matrix& operator*=(T const scalar);
  Matrix& operator/=(T const scalar);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Matrix  operator- () const;

  //============================================================================
  // Operators - Accessors
  //----------------------------------------------------------------------------
  T const& operator()(uint32 const row, uint32 const col) const;

  //============================================================================
  // Operators - Mutators
  //----------------------------------------------------------------------------
  T& operator()(uint32 const row, uint32 const col);

  //============================================================================
  // Accessors
  //----------------------------------------------------------------------------
  T const& At(uint32 const row, uint32 const col) const;

  //============================================================================
  // Mutators
  //----------------------------------------------------------------------------
  T& At(uint32 const row, uint32 const col);

  //============================================================================
  // Friends
  //----------------------------------------------------------------------------
  template <uint32 DIM0, uint32 DIM1, typename U>
  friend class Matrix;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  friend class BinarySerializer;
  friend class BinaryDeserializer;

private:
  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  T elements_[ROWS * COLS];
};

//==============================================================================
// Type definitions
//------------------------------------------------------------------------------
// Matrices of floating point values
//------------------------------------------------------------------------------
typedef Matrix< 2,  2, FloatType> Matrix2f;
typedef Matrix< 3,  3, FloatType> Matrix3f;
typedef Matrix< 4,  4, FloatType> Matrix4f;
typedef Matrix< 6,  6, FloatType> Matrix6f;
typedef Matrix< 8,  8, FloatType> Matrix8f;
typedef Matrix<12, 12, FloatType> Matrix12f;
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
typedef Matrix< 2,  3, FloatType> Matrix2x3f;
typedef Matrix< 2,  4, FloatType> Matrix2x4f;
typedef Matrix< 2,  6, FloatType> Matrix2x6f;
typedef Matrix< 2,  8, FloatType> Matrix2x8f;
typedef Matrix< 2, 12, FloatType> Matrix2x12f;
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
typedef Matrix< 3,  2, FloatType> Matrix3x2f;
typedef Matrix< 3,  4, FloatType> Matrix3x4f;
typedef Matrix< 3,  6, FloatType> Matrix3x6f;
typedef Matrix< 3,  8, FloatType> Matrix3x8f;
typedef Matrix< 3, 12, FloatType> Matrix3x12f;
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
typedef Matrix< 4,  2, FloatType> Matrix4x2f;
typedef Matrix< 4,  3, FloatType> Matrix4x3f;
typedef Matrix< 4,  6, FloatType> Matrix4x6f;
typedef Matrix< 4,  8, FloatType> Matrix4x8f;
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
typedef Matrix< 6,  2, FloatType> Matrix6x2f;
typedef Matrix< 6,  3, FloatType> Matrix6x3f;
typedef Matrix< 6,  4, FloatType> Matrix6x4f;
typedef Matrix< 6,  8, FloatType> Matrix6x8f;
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
typedef Matrix< 8,  2, FloatType> Matrix8x2f;
typedef Matrix< 8,  3, FloatType> Matrix8x3f;
typedef Matrix< 8,  4, FloatType> Matrix8x4f;
typedef Matrix< 8,  6, FloatType> Matrix8x6f;
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
typedef Matrix<12,  2, FloatType> Matrix12x2f;
typedef Matrix<12,  3, FloatType> Matrix12x3f;

typedef Matrix< 2,  2, FloatType> Matrix2d;    // Using this typedef is deprecated.
typedef Matrix< 3,  3, FloatType> Matrix3d;    // Using this typedef is deprecated.
typedef Matrix< 4,  4, FloatType> Matrix4d;    // Using this typedef is deprecated.
typedef Matrix< 5,  5, FloatType> Matrix5d;    // Using this typedef is deprecated.
typedef Matrix< 6,  6, FloatType> Matrix6d;    // Using this typedef is deprecated.
typedef Matrix< 8,  8, FloatType> Matrix8d;    // Using this typedef is deprecated.
typedef Matrix<12, 12, FloatType> Matrix12d;   // Using this typedef is deprecated.
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
typedef Matrix< 2,  3, FloatType> Matrix2x3d;  // Using this typedef is deprecated.
typedef Matrix< 2,  4, FloatType> Matrix2x4d;  // Using this typedef is deprecated.
typedef Matrix< 2,  5, FloatType> Matrix2x5d;  // Using this typedef is deprecated.
typedef Matrix< 2,  6, FloatType> Matrix2x6d;  // Using this typedef is deprecated.
typedef Matrix< 2,  8, FloatType> Matrix2x8d;  // Using this typedef is deprecated.
typedef Matrix< 2, 12, FloatType> Matrix2x12d; // Using this typedef is deprecated.
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
typedef Matrix< 3,  2, FloatType> Matrix3x2d;  // Using this typedef is deprecated.
typedef Matrix< 3,  4, FloatType> Matrix3x4d;  // Using this typedef is deprecated.
typedef Matrix< 3,  5, FloatType> Matrix3x5d;  // Using this typedef is deprecated.
typedef Matrix< 3,  6, FloatType> Matrix3x6d;  // Using this typedef is deprecated.
typedef Matrix< 3,  8, FloatType> Matrix3x8d;  // Using this typedef is deprecated.
typedef Matrix< 3, 12, FloatType> Matrix3x12d; // Using this typedef is deprecated.
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
typedef Matrix< 4,  2, FloatType> Matrix4x2d;  // Using this typedef is deprecated.
typedef Matrix< 4,  3, FloatType> Matrix4x3d;  // Using this typedef is deprecated.
typedef Matrix< 4,  5, FloatType> Matrix4x5d;  // Using this typedef is deprecated.
typedef Matrix< 4,  6, FloatType> Matrix4x6d;  // Using this typedef is deprecated.
typedef Matrix< 4,  8, FloatType> Matrix4x8d;  // Using this typedef is deprecated.
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
typedef Matrix< 5,  2, FloatType> Matrix5x2d;  // Using this typedef is deprecated.
typedef Matrix< 5,  3, FloatType> Matrix5x3d;  // Using this typedef is deprecated.
typedef Matrix< 5,  4, FloatType> Matrix5x4d;  // Using this typedef is deprecated.
typedef Matrix< 5,  6, FloatType> Matrix5x6d;  // Using this typedef is deprecated.
typedef Matrix< 5,  8, FloatType> Matrix5x8d;  // Using this typedef is deprecated.
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
typedef Matrix< 6,  2, FloatType> Matrix6x2d;  // Using this typedef is deprecated.
typedef Matrix< 6,  3, FloatType> Matrix6x3d;  // Using this typedef is deprecated.
typedef Matrix< 6,  4, FloatType> Matrix6x4d;  // Using this typedef is deprecated.
typedef Matrix< 6,  5, FloatType> Matrix6x5d;  // Using this typedef is deprecated.
typedef Matrix< 6,  8, FloatType> Matrix6x8d;  // Using this typedef is deprecated.
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
typedef Matrix< 8,  2, FloatType> Matrix8x2d;  // Using this typedef is deprecated.
typedef Matrix< 8,  3, FloatType> Matrix8x3d;  // Using this typedef is deprecated.
typedef Matrix< 8,  4, FloatType> Matrix8x4d;  // Using this typedef is deprecated.
typedef Matrix< 8,  5, FloatType> Matrix8x5d;  // Using this typedef is deprecated.
typedef Matrix< 8,  6, FloatType> Matrix8x6d;  // Using this typedef is deprecated.
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
typedef Matrix<12,  2, FloatType> Matrix12x2d; // Using this typedef is deprecated.
typedef Matrix<12,  3, FloatType> Matrix12x3d; // Using this typedef is deprecated.

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_math_Matrix_h_
