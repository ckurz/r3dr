//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_math_MatrixFunctions_h_
#define dlm_math_MatrixFunctions_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/core/Types.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Forward declarations
//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
class Vector;

template <uint32 ROWS, uint32 COLS, typename T>
class Matrix;

template <typename T>
class DynamicMatrix;

//==============================================================================
//! \brief MatrixFunction class
// MatrixFunctions class
//------------------------------------------------------------------------------
class MatrixFunctions
{
public:
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  // Row and column access
  //----------------------------------------------------------------------------
  template <uint32 ROWS, uint32 COLS, typename T>
  static Vector<COLS, T> GetRow(Matrix<ROWS, COLS, T> const& matrix,
                                uint32                const  row);
  
  template <uint32 ROWS, uint32 COLS, typename T>
  static Vector<ROWS, T> GetCol(Matrix<ROWS, COLS, T> const& matrix,
                                uint32                const  col);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <uint32 ROWS, uint32 COLS, typename T>
  static void SetRow(Matrix<ROWS, COLS, T>& matrix,
                     uint32          const  row,
                     Vector<COLS, T> const& vector);
  
  template <uint32 ROWS, uint32 COLS, typename T>
  static void SetCol(Matrix<ROWS, COLS, T>& matrix,
                     uint32          const  col,
                     Vector<ROWS, T> const& vector);

  //----------------------------------------------------------------------------
  // Transposition
  //----------------------------------------------------------------------------
  template <uint32 ROWS, uint32 COLS, typename T>
  static Matrix<COLS, ROWS, T> Transpose(Matrix<ROWS, COLS, T> const& matrix);

  //----------------------------------------------------------------------------
  // Submatrices
  //----------------------------------------------------------------------------
  template <uint32 DIM0, uint32 DIM1, uint32 ROWS, uint32 COLS, typename T>
  static Matrix<DIM0, DIM1, T> GetSubmatrix(Matrix<ROWS, COLS, T> const& matrix,
                                            uint32                const  row,
                                            uint32                const  col);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <uint32 DIM0, uint32 DIM1, uint32 ROWS, uint32 COLS, typename T>
  static void SetSubmatrix(Matrix<ROWS, COLS, T>&       matrix,
                           uint32                const  row,
                           uint32                const  col,
                           Matrix<DIM0, DIM1, T> const& submatrix);

  //----------------------------------------------------------------------------
  // Assignment
  //----------------------------------------------------------------------------
  template <uint32 ROWS, uint32 COLS, typename T>
  static void MakeIdentity(Matrix<ROWS, COLS, T>& matrix);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <uint32 ROWS, uint32 COLS, typename T>
  static Matrix<ROWS, COLS, T> Identity();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  static Matrix2f Identity2f();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  static Matrix3x4f Identity3x4f();

  //----------------------------------------------------------------------------
  // Inversion
  //----------------------------------------------------------------------------
  template <uint32 SIZE, typename T>
  static void Invert(Matrix<SIZE, SIZE, T>& matrix);

  template <uint32 SIZE, typename T>
  static Matrix<SIZE, SIZE, T> Inverse(Matrix<SIZE, SIZE, T> const& matrix);

  //----------------------------------------------------------------------------
  // Conversion
  //----------------------------------------------------------------------------
  template <uint32 ROWS, uint32 COLS, typename T>
  static DynamicMatrix<T> Convert(Matrix<ROWS, COLS, T> const& matrix);
};

//==============================================================================
// Shorthand
//------------------------------------------------------------------------------
typedef MatrixFunctions MF;

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// Inline code
//------------------------------------------------------------------------------
// This file is likely to be included from source files only, and for
// convenience includes the corresponding inline file automatically.
//------------------------------------------------------------------------------
#include "dlm/math/MatrixFunctions.inline.h"

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
