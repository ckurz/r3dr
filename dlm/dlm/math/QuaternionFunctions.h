//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_math_QuaternionFunctions_h_
#define dlm_math_QuaternionFunctions_h_

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Forward declarations
//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
class Vector;
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
template <uint32 ROWS, uint32 COLS, typename T>
class Matrix;
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
template <typename T>
class Quaternion;

//==============================================================================
// QuaternionFunctions class
//------------------------------------------------------------------------------
class QuaternionFunctions
{
public:
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  // Axis/angle conversion
  //----------------------------------------------------------------------------
  template <typename T>
  static Quaternion<T> FromAxisAngle(Vector<3, T> axis, T angle);

  //----------------------------------------------------------------------------
  // Euler angles conversion
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // EulerAnglesYXZr are assumed to be in EulOrdYXZr format, i.e., the vector
  // contains [pan, tilt, roll]: pan rotates around the y-axis, tilt around the
  // x-axis, and roll around the z-axis.
  //----------------------------------------------------------------------------
  template <typename T>
  static Quaternion<T> FromEulerAnglesYXZr(Vector<3, T> const& angles);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <typename T>
  static Vector<3, T> EulerAnglesYXZr(Quaternion<T> const& quaternion);

  //----------------------------------------------------------------------------
  // Rotation matrix conversion
  //----------------------------------------------------------------------------
  template <typename T>
  static Quaternion<T> FromRotation(Matrix<3, 3, T> const& rotation);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <typename T>
  static Matrix<3, 3, T> Rotation(Quaternion<T> const& quaternion);

  //----------------------------------------------------------------------------
  // Spherical linear interpolation
  //----------------------------------------------------------------------------
  template <typename T>
  static Quaternion<T> Slerp(
    Quaternion<T> const& a, Quaternion<T> const& b, T t);
};

//==============================================================================
// Shorthand
//------------------------------------------------------------------------------
typedef QuaternionFunctions QF;

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// Inline code
//------------------------------------------------------------------------------
// This file is likely to be included from source files only, and for
// convenience includes the corresponding inline file automatically.
//------------------------------------------------------------------------------
#include "dlm/math/QuaternionFunctions.inline.h"

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_math_QuaternionFunctions_h_
