//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_math_DynamicMatrix_h_
#define dlm_math_DynamicMatrix_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/core/Types.h"
#include "dlm/core/AutoArrayPtr.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Forward declarations
//------------------------------------------------------------------------------
template <typename T>
class DynamicVector;

//==============================================================================
// DynamicMatrix template class
//------------------------------------------------------------------------------
template <typename T>
class DynamicMatrix
{
public:
  //============================================================================
  // Constructors
  //----------------------------------------------------------------------------
  DynamicMatrix();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicMatrix(uint64 const rows, uint64 const cols);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicMatrix(DynamicMatrix const& other);

  //============================================================================
  // Operators
  //----------------------------------------------------------------------------
  DynamicMatrix& operator= (DynamicMatrix const& other);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicMatrix  operator+ (DynamicMatrix const& other) const;
  DynamicMatrix  operator- (DynamicMatrix const& other) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicMatrix& operator+=(DynamicMatrix const& other);
  DynamicMatrix& operator-=(DynamicMatrix const& other);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicMatrix  operator* (DynamicMatrix const& other) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicMatrix& operator*=(DynamicMatrix const& other);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicVector<T> operator*(DynamicVector<T> const& vector) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicMatrix  operator* (T const scalar) const;
  DynamicMatrix  operator/ (T const scalar) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicMatrix& operator*=(T const scalar);
  DynamicMatrix& operator/=(T const scalar);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicMatrix  operator- () const;

  //============================================================================
  // Operators - Accessors
  //----------------------------------------------------------------------------
  T const& operator()(uint64 const row, uint64 const col) const;
  
  //============================================================================
  // Operators - Mutators
  //----------------------------------------------------------------------------
  T& operator()(uint64 const row, uint64 const col);

  //============================================================================
  // Accessors
  //----------------------------------------------------------------------------
  uint64 Rows() const;
  uint64 Cols() const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T const& At(uint64 const row, uint64 const col) const;

  //============================================================================
  // Mutators
  //----------------------------------------------------------------------------
  T& At(uint64 const row, uint64 const col);

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  uint64 ArraySize() const;

private:
  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  AutoArrayPtr<T> elements_;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint64 rows_;
  uint64 cols_;
};

//==============================================================================
// Type definitions
//------------------------------------------------------------------------------
typedef DynamicMatrix<float64> DMatrixd;
typedef DynamicMatrix<FloatType> DMatrixf;

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_math_DynamicMatrix_h_
