//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_math_MatrixFunctions_Eigen_inline_h_
#define dlm_math_MatrixFunctions_Eigen_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/math/Matrix.h"
#include "dlm/math/Matrix.inline.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace Eigen_Reimplementation
{

//------------------------------------------------------------------------------
// This function (except forward and back substitution) is based on
// Eigen/PartialPivLU. Eigen can be obtained at
// [http://eigen.tuxfamily.org/index.php?title=Main_Page].
// Here is the respective copyright and license statement:

// This file is part of Eigen, a lightweight C++ template library
// for linear algebra.
//
// Copyright (C) 2006-2009 Benoit Jacob <jacob.benoit.1@gmail.com>
// Copyright (C) 2009 Gael Guennebaud <gael.guennebaud@inria.fr>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
//------------------------------------------------------------------------------
template <dlm::uint32 SIZE, typename T>
inline void Invert_PartialPivLU(dlm::Matrix<SIZE, SIZE, T>& matrix)
{
  dlm::uint32 permutations[SIZE] = {};

  dlm::uint32 permutationCounter = 0;

  for (dlm::uint32 k = 0; k < SIZE; ++k)
  {
    // Determine the highest value in column k and store its row index.
    T maxValue = static_cast<dlm::FloatType>(0.0);

    dlm::uint32 rowIndex = k; // In case there is only a zero-value left.

    for (dlm::uint32 row = k; row < SIZE; ++row)
    {
      T const current = std::fabs(matrix.At(row, k));

      if (current >= maxValue)
      {
        maxValue = current;

        rowIndex = row;
      }
    }

    permutations[k] = rowIndex;

    if (maxValue != static_cast<dlm::FloatType>(0.0))
    {
      if (rowIndex != k)
      {
        for (dlm::uint32 i = 0; i < SIZE; ++i)
        {
          std::swap(matrix.At(k, i), matrix.At(rowIndex, i));
        }

        ++permutationCounter;
      }

      T const value = matrix.At(k, k);

      for (dlm::uint32 row = k + 1; row < SIZE; ++row)
      {
        matrix.At(row, k) /= value;
      }
    }

    if (k < SIZE - 1)
    {
      for (dlm::uint32 i = k + 1; i < SIZE; ++i)
      {
        for (dlm::uint32 j = k + 1; j < SIZE; ++j)
        {
          matrix.At(i, j) -= matrix.At(i, k) * matrix.At(k, j);
        }
      }
    }
  }

  dlm::uint32 indices[SIZE];

  for (dlm::uint32 i = 0; i < SIZE; ++i)
  {
    indices[i] = i;
  }

  for (dlm::uint32 i = SIZE; i --> 0;)
  {
    std::swap(indices[i], indices[permutations[i]]);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Generate permutated identity matrix.
  dlm::Matrix<SIZE, SIZE, T> dst;

  for (dlm::uint32 i = 0; i < SIZE; ++i)
  {
    dst(indices[i], i) = static_cast<dlm::FloatType>(1.0);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Solve.
  for (dlm::uint32 col = 0; col < SIZE; ++col)
  {
    // Forward substitution in-place.
    for (dlm::uint32 i = 0; i < SIZE; ++i)
    {
      // The first step, assignment of x_0 = b_0 / l_00 is a NOP because the
      // diagonal of the lower triangular matrix is composed of ones only.
    
      T sum = static_cast<dlm::FloatType>(0.0);

      for (dlm::uint32 j = 0; j < i; ++j)
      {
        sum += matrix(i, j) * dst(j, col);
      }

      dst(i, col) -= sum;
    }

    //Back substitution in-place.
    for (dlm::uint32 i = SIZE; i --> 0;)
    {
      T sum = static_cast<dlm::FloatType>(0.0);
      
      for (dlm::uint32 j = SIZE; j --> i + 1;)
      {
        sum += matrix(i, j) * dst(j, col);
      }

      dst(i, col) -= sum;
      dst(i, col) /= matrix(i, i);
    }
  }

  matrix = dst;
}

//------------------------------------------------------------------------------
// These functions are based on Eigen/src/LU/Inverse.h. Eigen can be obtained at
// [http://eigen.tuxfamily.org/index.php?title=Main_Page].
// Here is the respective copyright and license statement:

// This file is part of Eigen, a lightweight C++ template library
// for linear algebra.
//
// Copyright (C) 2008-2010 Benoit Jacob <jacob.benoit.1@gmail.com>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
//------------------------------------------------------------------------------
template <int i, int j, typename T>
inline T Cofactor_3x3(dlm::Matrix<3, 3, T> const& matrix)
{
  enum
  {
    i1 = (i + 1) % 3,
    i2 = (i + 2) % 3,
    j1 = (j + 1) % 3,
    j2 = (j + 2) % 3
  };

  return matrix(i1, j1) * matrix(i2, j2) - matrix(i1, j2) * matrix(i2, j1);
}

//------------------------------------------------------------------------------
template <typename T>
inline void Invert_3x3(dlm::Matrix<3, 3, T>& result)
{
  dlm::Matrix<3, 3, T> const matrix = result;

  T const cf0 = Cofactor_3x3<0, 0>(matrix);
  T const cf1 = Cofactor_3x3<1, 0>(matrix);
  T const cf2 = Cofactor_3x3<2, 0>(matrix);

  T const determinant = cf0 * matrix(0, 0)
                      + cf1 * matrix(1, 0)
                      + cf2 * matrix(2, 0);
                      
  T const inverseDeterminant = static_cast<T>(1) / determinant;

  result(0, 0) = cf0 * inverseDeterminant;  
  result(0, 1) = cf1 * inverseDeterminant;  
  result(0, 2) = cf2 * inverseDeterminant;  

  result(1, 0) = Cofactor_3x3<0, 1>(matrix) * inverseDeterminant;  
  result(1, 1) = Cofactor_3x3<1, 1>(matrix) * inverseDeterminant;  
  result(1, 2) = Cofactor_3x3<2, 1>(matrix) * inverseDeterminant;  

  result(2, 0) = Cofactor_3x3<0, 2>(matrix) * inverseDeterminant;  
  result(2, 1) = Cofactor_3x3<1, 2>(matrix) * inverseDeterminant;  
  result(2, 2) = Cofactor_3x3<2, 2>(matrix) * inverseDeterminant;
}

//------------------------------------------------------------------------------
template <dlm::uint32 SIZE, typename T>
inline void Invert(dlm::Matrix<SIZE, SIZE, T>& matrix)
{
  Invert_PartialPivLU(matrix);
}

//------------------------------------------------------------------------------
template <typename T>
inline void Invert(dlm::Matrix<3, 3, T>& matrix)
{
  Invert_3x3(matrix);
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
