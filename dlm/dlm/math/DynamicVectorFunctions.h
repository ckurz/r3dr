//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// Copyright (C)  2018  TrackMen Ltd.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_math_DynamicVectorFunctions_h_
#define dlm_math_DynamicVectorFunctions_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/core/Types.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Forward declarations
//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
class Matrix;

template <typename T>
class DynamicVector;

template <typename T>
class DynamicMatrix;

//==============================================================================
// DynamicVectorFunctions class
//------------------------------------------------------------------------------
class DynamicVectorFunctions
{
public:
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  template <typename T>
  static
  DynamicVector<T>
  Homogenize(
    DynamicVector<T> const& vector);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <typename T>
  static
  DynamicVector<T>
  Inhomogenize(
    DynamicVector<T> const& vector);

  //----------------------------------------------------------------------------
  // Tensor product
  //----------------------------------------------------------------------------
  template <typename T>
  static
  DynamicMatrix<T>
  TensorProduct(
    DynamicVector<T> const& a,
    DynamicVector<T> const& b);

  //----------------------------------------------------------------------------
  // Coefficient-wise operations
  //----------------------------------------------------------------------------
  template <typename T>
  static
  DynamicVector<T>
  CWiseMultiply(
    DynamicVector<T> const& a,
    DynamicVector<T> const& b);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <typename T>
  static
  DynamicVector<T>
  CWiseDivide(
    DynamicVector<T> const& a,
    DynamicVector<T> const& b);

  //----------------------------------------------------------------------------
  // Type conversion
  //----------------------------------------------------------------------------
  template<uint32 SIZE, typename T>
  static Vector<SIZE, T> Convert(DynamicVector<T> const& vector);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <typename T>
  static
  DynamicMatrix<T>
  AsRows(
    DynamicVector<T> const& vector,
    dlm::uint64      const  rowSize);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <typename T>
  static
  DynamicMatrix<T>
  AsCols(
    DynamicVector<T> const& vector,
    dlm::uint64      const  colSize);

  //----------------------------------------------------------------------------
  // Misc
  //----------------------------------------------------------------------------
  template <typename T>
  static T Sum(DynamicVector<T> const& vector);
};

//==============================================================================
// Shorthand
//------------------------------------------------------------------------------
typedef DynamicVectorFunctions DVF;

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// Inline code
//------------------------------------------------------------------------------
// This file is likely to be included from source files only, and for
// convenience includes the corresponding inline file automatically.
//------------------------------------------------------------------------------
#include "dlm/math/DynamicVectorFunctions.inline.h"

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_math_DynamicVectorFunctions_h_
