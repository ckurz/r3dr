//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_math_DynamicMatrixFunctions_Eigen_inline_h_
#define dlm_math_DynamicMatrixFunctions_Eigen_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/math/DynamicMatrix.h"
#include "dlm/math/DynamicMatrix.inline.h"

#include "dlm/math/DynamicVector.h"
#include "dlm/math/DynamicVector.inline.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace Eigen_Reimplementation
{

//------------------------------------------------------------------------------
// This function (except forward and back substitution) is based on
// Eigen/PartialPivLU. Eigen can be obtained at
// [http://eigen.tuxfamily.org/index.php?title=Main_Page].
// Here is the respective copyright and license statement:

// This file is part of Eigen, a lightweight C++ template library
// for linear algebra.
//
// Copyright (C) 2006-2009 Benoit Jacob <jacob.benoit.1@gmail.com>
// Copyright (C) 2009 Gael Guennebaud <gael.guennebaud@inria.fr>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
//------------------------------------------------------------------------------
template <typename T>
inline
void
Invert_PartialPivLU(
  dlm::DynamicMatrix<T>& matrix)
{
  DLM_ASSERT(matrix.Cols() > 0, DLM_RUNTIME_ERROR);
  DLM_ASSERT(matrix.Rows() > 0, DLM_RUNTIME_ERROR);
  DLM_ASSERT(matrix.Cols() == matrix.Rows(), DLM_RUNTIME_ERROR);

  auto const size = matrix.Cols();

  auto permutations = dlm::DynamicVector<dlm::uint64>(size);

  auto permutationCounter = static_cast<dlm::uint64>(0);

  for (auto k = static_cast<dlm::uint64>(0); k < size; ++k)
  {
    // Determine the highest value in column k and store its row index.
    auto maxValue = static_cast<dlm::FloatType>(0.0);

    auto rowIndex = k; // In case there is only a zero-value left.

    for (auto row = k; row < size; ++row)
    {
      auto const current = std::fabs(matrix(row, k));

      if (current >= maxValue)
      {
        maxValue = current;

        rowIndex = row;
      }
    }

    permutations(k) = rowIndex;

    if (maxValue != static_cast<dlm::FloatType>(0.0))
    {
      if (rowIndex != k)
      {
        for (auto i = static_cast<dlm::uint64>(0); i < size; ++i)
        {
          std::swap(matrix(k, i), matrix(rowIndex, i));
        }

        ++permutationCounter;
      }

      auto const value = matrix(k, k);

      for (auto row = k + 1; row < size; ++row)
      {
        matrix(row, k) /= value;
      }
    }

    if (k < size - 1)
    {
      for (auto i = k + 1; i < size; ++i)
      {
        for (auto j = k + 1; j < size; ++j)
        {
          matrix(i, j) -= matrix(i, k) * matrix(k, j);
        }
      }
    }
  }

  auto indices = dlm::DynamicVector<dlm::uint64>(size);

  for (auto i = static_cast<dlm::uint64>(0); i < size; ++i)
  {
    indices(i) = i;
  }

  for (auto i = size; i --> 0;)
  {
    using std::swap;

    swap(indices(i), indices(permutations(i)));
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Generate permutated identity matrix.
  auto dst = dlm::DynamicMatrix<T>(size, size);

  for (auto i = static_cast<dlm::uint64>(0); i < size; ++i)
  {
    dst(indices(i), i) = static_cast<dlm::FloatType>(1.0);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Solve.
  for (auto col = static_cast<dlm::uint64>(0); col < size; ++col)
  {
    // Forward substitution in-place.
    for (auto i = static_cast<dlm::uint64>(0); i < size; ++i)
    {
      // The first step, assignment of x_0 = b_0 / l_00 is a NOP because the
      // diagonal of the lower triangular matrix is composed of ones only.

      auto sum = static_cast<dlm::FloatType>(0.0);

      for (auto j = static_cast<dlm::uint64>(0); j < i; ++j)
      {
        sum += matrix(i, j) * dst(j, col);
      }

      dst(i, col) -= sum;
    }

    //Back substitution in-place.
    for (auto i = size; i --> 0;)
    {
      auto sum = static_cast<dlm::FloatType>(0.0);

      for (auto j = size; j --> i + 1;)
      {
        sum += matrix(i, j) * dst(j, col);
      }

      dst(i, col) -= sum;
      dst(i, col) /= matrix(i, i);
    }
  }

  matrix = dst;
}

//------------------------------------------------------------------------------
// These functions are based on Eigen/src/LU/Inverse.h. Eigen can be obtained at
// [http://eigen.tuxfamily.org/index.php?title=Main_Page].
// Here is the respective copyright and license statement:

// This file is part of Eigen, a lightweight C++ template library
// for linear algebra.
//
// Copyright (C) 2008-2010 Benoit Jacob <jacob.benoit.1@gmail.com>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
//------------------------------------------------------------------------------
template <int i, int j, typename T>
inline
T
Cofactor_3x3(
  dlm::DynamicMatrix<T> const& matrix)
{
  DLM_ASSERT(matrix.Rows() == 3, DLM_RUNTIME_ERROR);
  DLM_ASSERT(matrix.Cols() == 3, DLM_RUNTIME_ERROR);

  enum
  {
    i1 = (i + 1) % 3,
    i2 = (i + 2) % 3,
    j1 = (j + 1) % 3,
    j2 = (j + 2) % 3
  };

  return matrix(i1, j1) * matrix(i2, j2) - matrix(i1, j2) * matrix(i2, j1);
}

//------------------------------------------------------------------------------
template <typename T>
inline
void
Invert_3x3(
  dlm::DynamicMatrix<T>& result)
{
  DLM_ASSERT(result.Rows() == 3, DLM_RUNTIME_ERROR);
  DLM_ASSERT(result.Cols() == 3, DLM_RUNTIME_ERROR);

  auto const matrix = result;

  T const cf0 = Cofactor_3x3<0, 0>(matrix);
  T const cf1 = Cofactor_3x3<1, 0>(matrix);
  T const cf2 = Cofactor_3x3<2, 0>(matrix);

  T const determinant = cf0 * matrix(0, 0)
                      + cf1 * matrix(1, 0)
                      + cf2 * matrix(2, 0);

  T const inverseDeterminant = static_cast<T>(1) / determinant;

  result(0, 0) = cf0 * inverseDeterminant;
  result(0, 1) = cf1 * inverseDeterminant;
  result(0, 2) = cf2 * inverseDeterminant;

  result(1, 0) = Cofactor_3x3<0, 1>(matrix) * inverseDeterminant;
  result(1, 1) = Cofactor_3x3<1, 1>(matrix) * inverseDeterminant;
  result(1, 2) = Cofactor_3x3<2, 1>(matrix) * inverseDeterminant;

  result(2, 0) = Cofactor_3x3<0, 2>(matrix) * inverseDeterminant;
  result(2, 1) = Cofactor_3x3<1, 2>(matrix) * inverseDeterminant;
  result(2, 2) = Cofactor_3x3<2, 2>(matrix) * inverseDeterminant;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
