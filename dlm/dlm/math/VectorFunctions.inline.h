//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_math_VectorFunctions_inline_h_
#define dlm_math_VectorFunctions_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/math/VectorFunctions.h"

//------------------------------------------------------------------------------
#include "dlm/math/Vector.h"
#include "dlm/math/Vector.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Matrix.h"
#include "dlm/math/Matrix.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/DynamicVector.h"
#include "dlm/math/DynamicVector.inline.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline Vector<SIZE + 1, T> VectorFunctions::Homogenize(
  Vector<SIZE, T> const& vector)
{
  Vector<SIZE + 1, T> result;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < SIZE; ++i)
  {
    result.At(i) = vector.At(i);
  }

  result.At(SIZE) = 1;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline Vector<SIZE - 1, T> VectorFunctions::Inhomogenize(
  Vector<SIZE, T> const& vector)
{
  Vector<SIZE - 1, T> result;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < SIZE - 1; ++i)
  {
    result.At(i) = vector.At(i);
  }

  result /= vector.At(SIZE - 1);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
inline Matrix<ROWS, COLS, T> VectorFunctions::TensorProduct(
  Vector<ROWS, T> const& a, Vector<COLS, T> const& b)
{
  Matrix<ROWS, COLS, T> result;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 row = 0; row < ROWS; ++row)
  {
    for (uint32 col = 0; col < COLS; ++col)
    {
      result(row, col) = a(row) * b(col);
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline Vector<SIZE, T> VectorFunctions::CWiseMultiply(
  Vector<SIZE, T> const& a, Vector<SIZE, T> const& b)
{
  Vector<SIZE, T> result;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < SIZE; ++i)
  {
    result.At(i) = a.At(i) * b.At(i);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline Vector<SIZE, T> VectorFunctions::CWiseDivide(
  Vector<SIZE, T> const& a, Vector<SIZE, T> const& b)
{
  static_assert(std::is_floating_point<T>::value,
                "Only defined for floating point types.");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Vector<SIZE, T> result;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < SIZE; ++i)
  {
    T const divisor = b.At(i);
  
    DLM_ASSERT(divisor != T(), DLM_DOMAIN_ERROR);
    
    result.At(i) = a.At(i) / divisor;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <typename T, uint32 SIZE, typename U>
inline Vector<SIZE, T> VectorFunctions::Convert(Vector<SIZE, U> const& vector)
{
  Vector<SIZE, T> result;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < SIZE; ++i)
  {
    result.At(i) = static_cast<T>(vector.At(i));
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline DynamicVector<T> VectorFunctions::Convert(Vector<SIZE, T> const& vector)
{
  DynamicVector<T> result(SIZE);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < SIZE; ++i)
  {
    result.At(i) = vector.At(i);
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
inline Matrix<ROWS, COLS, T> VectorFunctions::AsRows(
  Vector<ROWS * COLS, T> const& vector)
{
  Matrix<ROWS, COLS, T> result;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < ROWS; ++i)
  {
    for (uint32 j = 0; j < COLS; ++j)
    {
      result.At(i, j) = vector.At(i * COLS + j);
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
inline Matrix<ROWS, COLS, T> VectorFunctions::AsCols(
  Vector<ROWS * COLS, T> const& vector)
{
  Matrix<ROWS, COLS, T> result;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < ROWS; ++i)
  {
    for (uint32 j = 0; j < COLS; ++j)
    {
      result.At(i, j) = vector.At(j * ROWS + i);
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline T VectorFunctions::Sum(Vector<SIZE, T> const& vector)
{
  T sum = static_cast<T>(0);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < SIZE; ++i)
  {
    sum += vector(i);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return sum;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_math_VectorFunctions_inline_h_
