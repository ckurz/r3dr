//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include <iostream>
#include <iomanip>
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Types.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_math_OStream_h_
#define dlm_math_OStream_h_

//==============================================================================
// OStream configuration
//------------------------------------------------------------------------------
static uint32 const FPPrecision =  6; //floating point output precision
static uint32 const FPWidth     = 14; //floating point output width
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static bool const UseFPPrecision = true;
static bool const UseFPWidth     = true;

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifdef  dlm_math_Matrix_h_
#ifndef dlm_math_Matrix_OStream_h_
#define dlm_math_Matrix_OStream_h_

//==============================================================================
// std::ostream operator
//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
inline std::ostream& operator<<(std::ostream               & stream,
                                Matrix<ROWS, COLS, T> const& matrix)
{
  //obtain stream format flags to later reset original stream state
  std::ios_base::fmtflags const flags = stream.flags();

  if (UseFPPrecision) { stream.setf(std::ios::fixed); }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < ROWS; ++i)
  {
    for (uint32 j = 0; j < COLS; ++j)
    {
      if (j)
      {
        stream << " "; // no more trailing spaces
      }

      // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      if (UseFPWidth    ) { stream << std::setw(FPWidth);             }
      if (UseFPPrecision) { stream << std::setprecision(FPPrecision); }

      // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      stream << matrix.At(i, j);
    }

    stream << std::endl;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  stream.flags(flags);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_math_Matrix_OStream_h_
#endif //dlm_math_Matrix_h_

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifdef  dlm_math_Vector_h_
#ifndef dlm_math_Vector_OStream_h_
#define dlm_math_Vector_OStream_h_

//==============================================================================
// std::ostream operator
//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline std::ostream& operator<<(std::ostream         & stream,
                                Vector<SIZE, T> const& vector)
{
  //obtain stream format flags to later reset original stream state
  std::ios_base::fmtflags const flags = stream.flags();

  if (UseFPPrecision) { stream.setf(std::ios::fixed); }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < SIZE; ++i)
  {
    if (i)
    {
      stream << " "; // no more trailing spaces
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    if (UseFPWidth    ) { stream << std::setw(FPWidth);             }
    if (UseFPPrecision) { stream << std::setprecision(FPPrecision); }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    stream << vector.At(i);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  stream.flags(flags);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//==============================================================================
// std::istream operator
//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline std::istream& operator>>(std::istream &stream, Vector<SIZE, T>& vector)
{
  for (uint32 i = 0; i < SIZE; ++i)
  {
    stream >> vector.At(i);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_math_Vector_OStream_h_
#endif //dlm_math_Vector_h_

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifdef  dlm_math_Quaternion_h_
#ifndef dlm_math_Quaternion_OStream_h_
#define dlm_math_Quaternion_OStream_h_

//==============================================================================
// std::ostream operator
//------------------------------------------------------------------------------
template <typename T>
inline std::ostream& operator<<(std::ostream       & stream,
                                Quaternion<T> const& quaternion)
{
  stream << quaternion.Xyz();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //obtain stream format flags to later reset original stream state
  std::ios_base::fmtflags const flags = stream.flags();

  if (UseFPPrecision) { stream.setf(std::ios::fixed); }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  stream << " ";

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (UseFPWidth    ) { stream << std::setw(FPWidth);             }
  if (UseFPPrecision) { stream << std::setprecision(FPPrecision); }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  stream << quaternion.W();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  stream.flags(flags);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_math_Quaternion_OStream_h_
#endif //dlm_math_Quaternion_h_

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifdef  dlm_math_DynamicMatrix_h_
#ifndef dlm_math_DynamicMatrix_OStream_h_
#define dlm_math_DynamicMatrix_OStream_h_

//==============================================================================
// std::ostream operator
//------------------------------------------------------------------------------
template <typename T>
inline std::ostream& operator<<(std::ostream          & stream,
                                DynamicMatrix<T> const& matrix)
{
  //obtain stream format flags to later reset original stream state
  std::ios_base::fmtflags const flags = stream.flags();

  if (UseFPPrecision) { stream.setf(std::ios::fixed); }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < matrix.Rows(); ++i)
  {
    for (uint32 j = 0; j < matrix.Cols(); ++j)
    {
      if (j)
      {
        stream << " "; // no more trailing spaces
      }

      // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      if (UseFPWidth    ) { stream << std::setw(FPWidth);             }
      if (UseFPPrecision) { stream << std::setprecision(FPPrecision); }

      // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      stream << matrix.At(i, j);
    }

    stream << std::endl;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  stream.flags(flags);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_math_DynamicMatrix_OStream_h_
#endif //dlm_math_DynamicMatrix_h_

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifdef  dlm_math_DynamicVector_h_
#ifndef dlm_math_DynamicVector_OStream_h_
#define dlm_math_DynamicVector_OStream_h_

//==============================================================================
// std::ostream operator
//------------------------------------------------------------------------------
template <typename T>
inline std::ostream& operator<<(std::ostream          & stream,
                                DynamicVector<T> const& vector)
{
  //obtain stream format flags to later reset original stream state
  std::ios_base::fmtflags const flags = stream.flags();

  if (UseFPPrecision) { stream.setf(std::ios::fixed); }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < vector.Size(); ++i)
  {
    if (i)
    {
      stream << " "; // no more trailing spaces
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    if (UseFPWidth    ) { stream << std::setw(FPWidth);             }
    if (UseFPPrecision) { stream << std::setprecision(FPPrecision); }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    stream << vector.At(i);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  stream.flags(flags);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return stream;
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_math_DynamicVector_OStream_h_
#endif //dlm_math_DynamicVector_h_

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm
