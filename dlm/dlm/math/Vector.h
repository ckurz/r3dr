//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_math_Vector_h_
#define dlm_math_Vector_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/core/Types.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
//! \brief Fixed-size vector template class.
// Vector template class
//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
class Vector
{
  //============================================================================
  // Size restriction
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Memory for this class is typically allocated on the stack. It is therefore
  // not advisable to let the internal storage grow too large, as this might
  // lead to a stack overflow.
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // If you choose to substantially increase the possible storage capacity of
  // this class (however unlikely that may be), keep in mind that the interface
  // for this class is 32bit and consequently cannot handle more than 2^32
  // elements.
  //----------------------------------------------------------------------------
  static_assert(SIZE * sizeof(T) <= 4096, "Vector is too large.");

public:
  //============================================================================
  // Constructors
  //----------------------------------------------------------------------------
  Vector();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // convenience constructors
  Vector(T const x);
  Vector(T const x, T const y);
  Vector(T const x, T const y, T const z);
  Vector(T const x, T const y, T const z, T const w);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //Vector(T const& defaultValue); //bad idea if not explicit;
  //remember the dot-product incident of '11?

  //============================================================================
  // Operators
  //----------------------------------------------------------------------------
  Vector  operator+ (Vector const& other) const;
  Vector  operator- (Vector const& other) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Vector& operator+=(Vector const& other);
  Vector& operator-=(Vector const& other);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Vector  operator* (T const scalar) const;
  Vector  operator/ (T const scalar) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Vector& operator*=(T const scalar);
  Vector& operator/=(T const scalar);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Vector  operator- () const;

  //============================================================================
  // Operators - Accessors
  //----------------------------------------------------------------------------
  T const& operator()(uint32 const index) const;

  //============================================================================
  // Operators - Mutators
  //----------------------------------------------------------------------------
  T& operator()(uint32 const index);

  //============================================================================
  // Accessors
  //----------------------------------------------------------------------------
  T const& At(uint32 const index) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // convenience accesssors
  T const& X() const;
  T const& Y() const;
  T const& Z() const;
  T const& W() const;

  //============================================================================
  // Mutators
  //----------------------------------------------------------------------------
  T& At(uint32 const index);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // convenience mutators
  T& X();
  T& Y();
  T& Z();
  T& W();

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  T Dot(Vector const& other) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Vector Cross(Vector const& other) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T Magnitude() const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Vector& Normalize ();
  Vector  Normalized() const;

  //============================================================================
  // Friends
  //----------------------------------------------------------------------------
  friend class BinarySerializer;
  friend class BinaryDeserializer;

protected:
  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  T elements_[SIZE];
};

//==============================================================================
// Type definitions
//------------------------------------------------------------------------------
// Vectors of signed integer values
//------------------------------------------------------------------------------
typedef Vector<2, int32> Vector2i;
typedef Vector<3, int32> Vector3i;
typedef Vector<4, int32> Vector4i;

//------------------------------------------------------------------------------
// Vectors of unsigned integer values
//------------------------------------------------------------------------------
typedef Vector<2, uint32> Vector2u;
typedef Vector<3, uint32> Vector3u;
typedef Vector<4, uint32> Vector4u;

//------------------------------------------------------------------------------
// Vectors of floating point values
//------------------------------------------------------------------------------
typedef Vector< 2, FloatType> Vector2f;
typedef Vector< 3, FloatType> Vector3f;
typedef Vector< 4, FloatType> Vector4f;
typedef Vector< 6, FloatType> Vector6f;
typedef Vector< 8, FloatType> Vector8f;
typedef Vector<12, FloatType> Vector12f;

typedef Vector< 2, FloatType> Vector2d;  // Using this typedef is deprecated.
typedef Vector< 3, FloatType> Vector3d;  // Using this typedef is deprecated.
typedef Vector< 4, FloatType> Vector4d;  // Using this typedef is deprecated.
typedef Vector< 5, FloatType> Vector5d;  // Using this typedef is deprecated.
typedef Vector< 6, FloatType> Vector6d;  // Using this typedef is deprecated.
typedef Vector< 8, FloatType> Vector8d;  // Using this typedef is deprecated.
typedef Vector<12, FloatType> Vector12d; // Using this typedef is deprecated.

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_math_Vector_h_
