//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_math_Vector_inline_h_
#define dlm_math_Vector_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/math/Vector.h"

//------------------------------------------------------------------------------
#include <cmath>
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <type_traits>
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline Vector<SIZE, T>::Vector()
  : elements_() //value-intialization
{}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline Vector<SIZE, T>::Vector(T const x)
  //  : elements_({x, y}) //initializer list
  //does not compile on VS 11; may be possible in the future
{
  static_assert(SIZE == 1, "Only defined for vectors of size 1.");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  elements_[0] = x;
}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline Vector<SIZE, T>::Vector(T const x, T const y)
//  : elements_({x, y}) //initializer list
//does not compile on VS 11; may be possible in the future
{
  static_assert(SIZE == 2, "Only defined for vectors of size 2.");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  elements_[0] = x;
  elements_[1] = y;
}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline Vector<SIZE, T>::Vector(T const x, T const y, T const z)
//  : elements_({x, y, z}) //initializer list
//does not compile on VS 11; may be possible in the future
{
  static_assert(SIZE == 3, "Only defined for vectors of size 3.");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  elements_[0] = x;
  elements_[1] = y;
  elements_[2] = z;
}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline Vector<SIZE, T>::Vector(T const x, T const y, T const z, T const w)
//  : elements_({x, y, z, w}) //initializer list
//does not compile on VS 11; may be possible in the future
{
  static_assert(SIZE == 4, "Only defined for vectors of size 4.");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  elements_[0] = x;
  elements_[1] = y;
  elements_[2] = z;
  elements_[3] = w;
}

//==============================================================================
// Operators
//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline Vector<SIZE, T> Vector<SIZE, T>::operator+(Vector const& other) const
{
  return Vector(*this) += other;
}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline Vector<SIZE, T> Vector<SIZE, T>::operator-(Vector const& other) const
{
  return Vector(*this) -= other;
}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline Vector<SIZE, T>& Vector<SIZE, T>::operator+=(Vector const& other)
{
  for (uint32 i = 0; i < SIZE; ++i)
  {
    elements_[i] += other.elements_[i];
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline Vector<SIZE, T>& Vector<SIZE, T>::operator-=(Vector const& other)
{
  for (uint32 i = 0; i < SIZE; ++i)
  {
    elements_[i] -= other.elements_[i];
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline Vector<SIZE, T> Vector<SIZE, T>::operator*(T const scalar) const
{
  return Vector(*this) *= scalar;
}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline Vector<SIZE, T> Vector<SIZE, T>::operator/(T const scalar) const
{
  return Vector(*this) /= scalar;
}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline Vector<SIZE, T>& Vector<SIZE, T>::operator*=(T const scalar)
{
  for (uint32 i = 0; i < SIZE; ++i)
  {
    elements_[i] *= scalar;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline Vector<SIZE, T>& Vector<SIZE, T>::operator/=(T const scalar)
{
  static_assert(std::is_floating_point<T>::value,
                "Only defined for floating point types.");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DLM_ASSERT(scalar != T(), DLM_DOMAIN_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < SIZE; ++i)
  {
    elements_[i] /= scalar;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline Vector<SIZE, T> Vector<SIZE, T>::operator-() const
{
  Vector result(*this);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < SIZE; ++i)
  {
    result.elements_[i] = -result.elements_[i];
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//==============================================================================
// Operators - Accessors
//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline T const& Vector<SIZE, T>::operator()(uint32 const i) const
{
  DLM_ASSERT(i < SIZE, DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[i];
}

//==============================================================================
// Operators - Mutators
//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline T& Vector<SIZE, T>::operator()(uint32 const i)
{
  DLM_ASSERT(i < SIZE, DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[i];
}

//==============================================================================
// Accessors
//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline T const& Vector<SIZE, T>::At(uint32 const i) const
{
  DLM_ASSERT(i < SIZE, DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[i];
}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline T const& Vector<SIZE, T>::X() const
{
  static_assert(SIZE > 0, "Only for vectors of size > 0.");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[0];
}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline T const& Vector<SIZE, T>::Y() const
{
  static_assert(SIZE > 1, "Only for vectors of size > 1.");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[1];
}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline T const& Vector<SIZE, T>::Z() const
{
  static_assert(SIZE > 2, "Only for vectors of size > 2.");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[2];
}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline T const& Vector<SIZE, T>::W() const
{
  static_assert(SIZE > 3, "Only for vectors of size > 3.");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[3];
}

//==============================================================================
// Mutators
//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline T& Vector<SIZE, T>::At(uint32 const i)
{
  DLM_ASSERT(i < SIZE, DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[i];
}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline T& Vector<SIZE, T>::X()
{
  static_assert(SIZE > 0, "Only for vectors of size > 0.");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[0];
}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline T& Vector<SIZE, T>::Y()
{
  static_assert(SIZE > 1, "Only for vectors of size > 1.");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[1];
}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline T& Vector<SIZE, T>::Z()
{
  static_assert(SIZE > 2, "Only for vectors of size > 2.");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[2];
}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline T& Vector<SIZE, T>::W()
{
  static_assert(SIZE > 3, "Only for vectors of size > 3.");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[3];
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline T Vector<SIZE, T>::Dot(Vector const& other) const
{
  T result = 0;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < SIZE; ++i)
  {
    result += elements_[i] * other.elements_[i];
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline Vector<SIZE, T> Vector<SIZE, T>::Cross(Vector const& other) const
{
  static_assert(SIZE == 3, "Only defined for vectors of size 3.");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return Vector(elements_[1] * other.elements_[2] -
                elements_[2] * other.elements_[1],
                elements_[2] * other.elements_[0] -
                elements_[0] * other.elements_[2],
                elements_[0] * other.elements_[1] -
                elements_[1] * other.elements_[0]);
}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline T Vector<SIZE, T>::Magnitude() const
{
  static_assert(std::is_floating_point<T>::value,
                "Only defined for floating point types.");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return sqrt(Dot(*this));
}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline Vector<SIZE, T>& Vector<SIZE, T>::Normalize()
{
  static_assert(std::is_floating_point<T>::value,
                "Only defined for floating point types.");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this /= Magnitude();
}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline Vector<SIZE, T> Vector<SIZE, T>::Normalized() const
{
  static_assert(std::is_floating_point<T>::value,
                "Only defined for floating point types.");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return Vector(*this).Normalize();
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_math_Vector_inline_h_
