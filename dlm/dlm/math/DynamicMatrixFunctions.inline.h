//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// Copyright (C)  2018  TrackMen Ltd.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_math_DynamicMatrixFunctions_inline_h_
#define dlm_math_DynamicMatrixFunctions_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/math/DynamicMatrixFunctions.h"

//------------------------------------------------------------------------------
#include "dlm/math/DynamicMatrixFunctions.Eigen.inline.h"

//------------------------------------------------------------------------------
#include "dlm/math/Matrix.h"
#include "dlm/math/Matrix.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/DynamicMatrix.h"
#include "dlm/math/DynamicMatrix.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/DynamicVector.h"
#include "dlm/math/DynamicVector.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <type_traits>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
template <typename T>
inline DynamicVector<T> DynamicMatrixFunctions::GetRow(
  DynamicMatrix<T> const& matrix, uint64 const row)
{
  DLM_ASSERT(row < matrix.Rows(), DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicVector<T> result(matrix.Cols());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < matrix.Cols(); ++i)
  {
    result.At(i) = matrix.At(row, i);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <typename T>
inline DynamicVector<T> DynamicMatrixFunctions::GetCol(
  DynamicMatrix<T> const& matrix, uint64 const col)
{
  DLM_ASSERT(col < matrix.Cols(), DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicVector<T> result(matrix.Rows());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < matrix.Rows(); ++i)
  {
    result.At(i) = matrix.At(i, col);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <typename T>
inline void DynamicMatrixFunctions::SetRow(
  DynamicMatrix<T>      & matrix,
  uint64           const  row,
  DynamicVector<T> const& vector)
{
  DLM_ASSERT(vector.Size() == matrix.Cols(), DLM_SIZE_MISMATCH);
  
  DLM_ASSERT(row < matrix.Rows(), DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < matrix.Cols(); ++i)
  {
    matrix.At(row, i) = vector.At(i);
  }
}

//------------------------------------------------------------------------------
template <typename T>
inline void DynamicMatrixFunctions::SetCol(
  DynamicMatrix<T>      & matrix,
  uint64           const  col,
  DynamicVector<T> const& vector)
{
  DLM_ASSERT(vector.Size() == matrix.Rows(), DLM_SIZE_MISMATCH);

  DLM_ASSERT(col < matrix.Cols(), DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < matrix.Rows(); ++i)
  {
    matrix.At(i, col) = vector.At(i);
  }
}

//------------------------------------------------------------------------------
template <typename T>
inline DynamicMatrix<T> DynamicMatrixFunctions::Transpose(
  DynamicMatrix<T> const& matrix)
{
  static_assert(std::is_arithmetic<T>::value,
                "Only defined for arithmetic types.");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint64 const rows = matrix.Rows();
  uint64 const cols = matrix.Cols();
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicMatrix<T> result(cols, rows);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 row = 0; row < rows; ++row)
  {
    for (uint64 col = 0; col < cols; ++col)
    {
      result.At(col, row) = matrix.At(row, col);
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <typename T>
inline
DynamicMatrix<T>
DynamicMatrixFunctions::GetSubmatrix(
  DynamicMatrix<T> const& matrix,
  uint64 const row,
  uint64 const col,
  uint64 const rows,
  uint64 const cols)
{
  DLM_ASSERT(row + rows <= matrix.Rows(), DLM_OUT_OF_RANGE);
  DLM_ASSERT(col + cols <= matrix.Cols(), DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  auto result = DynamicMatrix<T>(rows, cols);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (auto i = static_cast<uint64>(0); i < rows; ++i)
  {
    for (auto j = static_cast<uint64>(0); j < cols; ++j)
    {
      result(i, j) = matrix(row + i, col + j);
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <typename T>
inline
void
DynamicMatrixFunctions::SetSubmatrix(
  DynamicMatrix<T>      & matrix,
  uint64           const  row,
  uint64           const  col,
  DynamicMatrix<T> const& submatrix)
{
  DLM_ASSERT(row + submatrix.Rows() <= matrix.Rows(), DLM_OUT_OF_RANGE);
  DLM_ASSERT(col + submatrix.Cols() <= matrix.Cols(), DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (auto i = static_cast<uint64>(0); i < submatrix.Rows(); ++i)
  {
    for (auto j = static_cast<uint64>(0); j < submatrix.Cols(); ++j)
    {
      matrix(row + i, col + j) = submatrix(i, j);
    }
  }
}

//------------------------------------------------------------------------------
template <typename T>
inline
void
DynamicMatrixFunctions::MakeIdentity(
  DynamicMatrix<T>& matrix)
{
  matrix = DynamicMatrix<T>(matrix.Rows(), matrix.Cols());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  auto const range =
    (matrix.Rows() < matrix.Cols()) ? matrix.Rows() : matrix.Cols();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (auto i = static_cast<uint64>(0); i < range; ++i)
  {
    matrix(i, i) = static_cast<T>(1);
  }
}

//------------------------------------------------------------------------------
//! \brief Invert the given matrix in-place.
//! @param matrix The matrix to be inverted.
template <typename T>
inline
void
DynamicMatrixFunctions::Invert(
  DynamicMatrix<T>& matrix)
{
  DLM_ASSERT(matrix.Rows() == matrix.Cols(), DLM_RUNTIME_ERROR);

  if (matrix.Rows() == 3)
  {
    Eigen_Reimplementation::Invert_3x3(matrix);
  }
  else
  {
    Eigen_Reimplementation::Invert_PartialPivLU(matrix);
  }
}

//------------------------------------------------------------------------------
template <typename T>
inline
DynamicMatrix<T>
DynamicMatrixFunctions::Inverse(
  DynamicMatrix<T> const& matrix)
{
  auto result = matrix;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Invert(result);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template<uint32 ROWS, uint32 COLS, typename T>
inline Matrix<ROWS, COLS, T> DynamicMatrixFunctions::Convert(
  DynamicMatrix<T> const& matrix)
{
  DLM_ASSERT(matrix.Rows() < UInt32Max, DLM_SIZE_MISMATCH);
  DLM_ASSERT(matrix.Cols() < UInt32Max, DLM_SIZE_MISMATCH);

  DLM_ASSERT(uint32(matrix.Rows()) == ROWS, DLM_SIZE_MISMATCH);
  DLM_ASSERT(uint32(matrix.Cols()) == COLS, DLM_SIZE_MISMATCH);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Matrix<ROWS, COLS, T> result;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 row = 0; row < ROWS; ++row)
  {
    for (uint32 col = 0; col < COLS; ++col)
    {
      result.At(row, col) = matrix.At(row, col);
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_math_DynamicMatrixFunctions_inline_h_
