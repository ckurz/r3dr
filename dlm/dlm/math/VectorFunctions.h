//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_math_VectorFunctions_h_
#define dlm_math_VectorFunctions_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/core/Types.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Forward declarations
//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
class Vector;

template <uint32 ROWS, uint32 COLS, typename T>
class Matrix;

template <typename T>
class DynamicVector;

//==============================================================================
// VectorFunctions class
//------------------------------------------------------------------------------
class VectorFunctions
{
public:
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  template <uint32 SIZE, typename T>
  static Vector<SIZE + 1, T> Homogenize  (Vector<SIZE, T> const& vector);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <uint32 SIZE, typename T>
  static Vector<SIZE - 1, T> Inhomogenize(Vector<SIZE, T> const& vector);

  //----------------------------------------------------------------------------
  // Tensor product
  //----------------------------------------------------------------------------
  template <uint32 ROWS, uint32 COLS, typename T>
  static Matrix<ROWS, COLS, T> TensorProduct(
    Vector<ROWS, T> const& a, Vector<COLS, T> const& b);

  //----------------------------------------------------------------------------
  // Coefficient-wise operations
  //----------------------------------------------------------------------------
  template <uint32 SIZE, typename T>
  static Vector<SIZE, T> CWiseMultiply(
    Vector<SIZE, T> const& a, Vector<SIZE, T> const& b);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <uint32 SIZE, typename T>
  static Vector<SIZE, T> CWiseDivide(
    Vector<SIZE, T> const& a, Vector<SIZE, T> const& b);

  //----------------------------------------------------------------------------
  // Type conversion
  //----------------------------------------------------------------------------
  template <typename T, uint32 SIZE, typename U>
  static Vector<SIZE, T> Convert(Vector<SIZE, U> const& vector);

  //----------------------------------------------------------------------------
  // Conversion
  //----------------------------------------------------------------------------
  template <uint32 SIZE, typename T>
  static DynamicVector<T> Convert(Vector<SIZE, T> const& vector);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <uint32 ROWS, uint32 COLS, typename T>
  static Matrix<ROWS, COLS, T> AsRows(Vector<ROWS * COLS, T> const& vector);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <uint32 ROWS, uint32 COLS, typename T>
  static Matrix<ROWS, COLS, T> AsCols(Vector<ROWS * COLS, T> const& vector);

  //----------------------------------------------------------------------------
  // Misc
  //----------------------------------------------------------------------------
  template <uint32 SIZE, typename T>
  static T Sum(Vector<SIZE, T> const& vector);
};

//==============================================================================
// Shorthand
//------------------------------------------------------------------------------
typedef VectorFunctions VF;

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// Inline code
//------------------------------------------------------------------------------
// This file is likely to be included from source files only, and for
// convenience includes the corresponding inline file automatically.
//------------------------------------------------------------------------------
#include "dlm/math/VectorFunctions.inline.h"

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_math_VectorFunctions_h_
