//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// Copyright (C)  2018  TrackMen Ltd.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_math_DynamicMatrixFunctions_h_
#define dlm_math_DynamicMatrixFunctions_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/core/Types.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Forward declarations
//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
class Matrix;

template <typename T>
class DynamicVector;

template <typename T>
class DynamicMatrix;

//==============================================================================
// DynamicMatrixFunctions class
//------------------------------------------------------------------------------
class DynamicMatrixFunctions
{
public:
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  // Row and column access
  //----------------------------------------------------------------------------
  template <typename T>
  static DynamicVector<T> GetRow(DynamicMatrix<T> const& matrix,
                                 uint64           const  row);

  template <typename T>
  static DynamicVector<T> GetCol(DynamicMatrix<T> const& matrix,
                                 uint64           const  col);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <typename T>
  static void SetRow(DynamicMatrix<T>      & matrix,
                     uint64           const  row,
                     DynamicVector<T> const& vector);

  template <typename T>
  static void SetCol(DynamicMatrix<T>      & matrix,
                     uint64           const  col,
                     DynamicVector<T> const& vector);

  //----------------------------------------------------------------------------
  // Transposition
  //----------------------------------------------------------------------------
  template <typename T>
  static DynamicMatrix<T> Transpose(DynamicMatrix<T> const& matrix);

  //----------------------------------------------------------------------------
  // Submatrices
  //----------------------------------------------------------------------------
  template <typename T>
  static
  DynamicMatrix<T>
  GetSubmatrix(
    DynamicMatrix<T> const& matrix,
    uint64           const  row,
    uint64           const  col,
    uint64           const  rows,
    uint64           const  cols);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  template <typename T>
  static
  void
  SetSubmatrix(
    DynamicMatrix<T>&       matrix,
    uint64           const  row,
    uint64           const  col,
    DynamicMatrix<T> const& submatrix);

  //----------------------------------------------------------------------------
  // Assignment
  //----------------------------------------------------------------------------
  template <typename T>
  static
  void
  MakeIdentity(
    DynamicMatrix<T>& matrix);

  //----------------------------------------------------------------------------
  // Inversion
  //----------------------------------------------------------------------------
  template <typename T>
  static
  void
  Invert(
    DynamicMatrix<T>& matrix);

  template <typename T>
  static
  DynamicMatrix<T>
  Inverse(
    DynamicMatrix<T> const& matrix);

  //----------------------------------------------------------------------------
  // Conversion
  //----------------------------------------------------------------------------
  template<uint32 ROWS, uint32 COLS, typename T>
  static Matrix<ROWS, COLS, T> Convert(DynamicMatrix<T> const& matrix);
};

//==============================================================================
// Shorthand
//------------------------------------------------------------------------------
typedef DynamicMatrixFunctions DMF;

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// Inline code
//------------------------------------------------------------------------------
// This file is likely to be included from source files only, and for
// convenience includes the corresponding inline file automatically.
//------------------------------------------------------------------------------
#include "dlm/math/DynamicMatrixFunctions.inline.h"

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_math_DynamicMatrixFunctions_h_
