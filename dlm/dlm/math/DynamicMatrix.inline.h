//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_math_DynamicMatrix_inline_h_
#define dlm_math_DynamicMatrix_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/math/DynamicMatrix.h"

//------------------------------------------------------------------------------
#include "dlm/math/DynamicVector.h"
#include "dlm/math/DynamicVector.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/AutoArrayPtr.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
template <typename T>
inline DynamicMatrix<T>::DynamicMatrix()
  : elements_(0)
  , rows_    (0)
  , cols_    (0)
{}

//------------------------------------------------------------------------------
template <typename T>
inline DynamicMatrix<T>::DynamicMatrix(uint64 const rows, uint64 const cols)
  : elements_(new T[rows * cols]()) //value-initialization
  , rows_    (rows                )
  , cols_    (cols                )
{}

//------------------------------------------------------------------------------
template <typename T>
inline DynamicMatrix<T>::DynamicMatrix(DynamicMatrix const& other)
  : elements_(new T[other.ArraySize()])
  , rows_    (other.rows_             )
  , cols_    (other.cols_             )
{
  for (uint64 i = 0; i < ArraySize(); ++i)
  {
    elements_[i] = other.elements_[i];
  }
}

//==============================================================================
// Operators
//------------------------------------------------------------------------------
template <typename T>
inline DynamicMatrix<T>& DynamicMatrix<T>::operator=(DynamicMatrix const& other)
{
  uint64 const size = other.ArraySize();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //gracefully handle self-assignment
  AutoArrayPtr<T> newElements(new T[size]);

  for (uint64 i = 0; i < size; ++i)
  {
    newElements[i] = other.elements_[i];
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  rows_ = other.rows_;
  cols_ = other.cols_;

  elements_ = newElements;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
template <typename T>
inline DynamicMatrix<T> DynamicMatrix<T>::operator+(
  DynamicMatrix const& other) const
{
  return DynamicMatrix(*this) += other;
}

//------------------------------------------------------------------------------
template <typename T>
inline DynamicMatrix<T> DynamicMatrix<T>::operator-(
  DynamicMatrix const& other) const
{
  return DynamicMatrix(*this) -= other;
}

//------------------------------------------------------------------------------
template <typename T>
inline DynamicMatrix<T>& DynamicMatrix<T>::operator+=(
  DynamicMatrix const& other)
{
  DLM_ASSERT(Rows() == other.Rows(), DLM_SIZE_MISMATCH);  
  DLM_ASSERT(Cols() == other.Cols(), DLM_SIZE_MISMATCH);  
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint64 const range = ArraySize();

  for (uint64 i = 0; i < range; ++i)
  {
    elements_[i] += other.elements_[i];
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
template <typename T>
inline DynamicMatrix<T>& DynamicMatrix<T>::operator-=(
  DynamicMatrix const& other)
{
  DLM_ASSERT(Rows() == other.Rows(), DLM_SIZE_MISMATCH);  
  DLM_ASSERT(Cols() == other.Cols(), DLM_SIZE_MISMATCH);  
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint64 const range = ArraySize();

  for (uint64 i = 0; i < range; ++i)
  {
    elements_[i] -= other.elements_[i];
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
template <typename T>
inline DynamicMatrix<T> DynamicMatrix<T>::operator*(
  DynamicMatrix const& other) const
{
  uint64 const rows = Rows();
  uint64 const cols = Cols();
  
  uint64 const dim  = other.Cols();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DLM_ASSERT(cols == other.Rows(), DLM_SIZE_MISMATCH);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicMatrix result(rows, dim);

  uint64 i0 = 0; //surprisingly, the version without multiplications for index
  uint64 i1 = 0; //calculation is much faster

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 row = 0; row < rows; ++row)
  {
    for (uint64 col = 0; col < dim; ++col)
    {
      uint64 i2 = col; //that's the important one; you could get away with a
                       //minor performance penalty when getting rid of i0 and
      T sum = T();     //i1, but don't try it for i2

      // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      for (uint64 j = 0; j < cols; ++j)
      {
        sum += elements_[i0 + j] * other.elements_[i2];

        i2 += dim;
      }

      // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      result.elements_[i1 + col] = sum;
    }

    i0 += cols;
    i1 += dim;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <typename T>
inline DynamicMatrix<T>& DynamicMatrix<T>::operator*=(
  DynamicMatrix const& other)
{
  DLM_ASSERT(other.Rows() == other.Cols(), DLM_SIZE_MISMATCH);  

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  *this = *this * other;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
template <typename T>
inline DynamicVector<T> DynamicMatrix<T>::operator*(
  DynamicVector<T> const& vector) const
{
  uint64 const rows = Rows();
  uint64 const cols = Cols();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DLM_ASSERT(vector.Size() == cols, DLM_SIZE_MISMATCH);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicVector<T> result(rows);

  uint64 i0 = 0; //see Matrix-Matrix-multiplication

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 row = 0; row < rows; ++row)
  {
    T sum = T();

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    for (uint64 col = 0; col < cols; ++col)
    {
      sum += elements_[i0 + col] * vector.At(col);
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    result.At(row) = sum;

    i0 += cols;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <typename T>
inline DynamicMatrix<T> DynamicMatrix<T>::operator*(T const scalar) const
{
  return DynamicMatrix(*this) *= scalar;
}

//------------------------------------------------------------------------------
template <typename T>
inline DynamicMatrix<T> DynamicMatrix<T>::operator/(T const scalar) const
{
  return DynamicMatrix(*this) /= scalar;
}

//------------------------------------------------------------------------------
template <typename T>
inline DynamicMatrix<T>& DynamicMatrix<T>::operator*=(T const scalar)
{
  uint64 const range = ArraySize();

  for (uint64 i = 0; i < range; ++i)
  {
    elements_[i] *= scalar;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
template <typename T>
inline DynamicMatrix<T>& DynamicMatrix<T>::operator/=(T const scalar)
{
  static_assert(std::is_floating_point<T>::value,
                "Only defined for floating point types.");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DLM_ASSERT(scalar != T(), DLM_DOMAIN_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint64 const range = ArraySize();

  for (uint64 i = 0; i < range; ++i)
  {
    elements_[i] /= scalar;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
template <typename T>
inline DynamicMatrix<T> DynamicMatrix<T>::operator-() const
{
  DynamicMatrix result(*this);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint64 const range = ArraySize();

  for (uint64 i = 0; i < range; ++i)
  {
    result.elements_[i] = -result.elements_[i];
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//==============================================================================
// Operators - Accessors
//------------------------------------------------------------------------------
template <typename T>
inline T const& DynamicMatrix<T>::operator()(
  uint64 const row, uint64 const col) const
{
  DLM_ASSERT(row < rows_, DLM_OUT_OF_RANGE);
  DLM_ASSERT(col < cols_, DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[row * cols_ + col];
}

//==============================================================================
// Operators - Mutators
//------------------------------------------------------------------------------
template <typename T>
inline T& DynamicMatrix<T>::operator()(uint64 const row, uint64 const col)
{
  DLM_ASSERT(row < rows_, DLM_OUT_OF_RANGE);
  DLM_ASSERT(col < cols_, DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[row * cols_ + col];
}

//==============================================================================
// Accessors
//------------------------------------------------------------------------------
template <typename T>
inline uint64 DynamicMatrix<T>::Rows() const
{
  return rows_;
}

//------------------------------------------------------------------------------
template <typename T>
inline uint64 DynamicMatrix<T>::Cols() const
{
  return cols_;
}

//------------------------------------------------------------------------------
template <typename T>
inline T const& DynamicMatrix<T>::At(uint64 const row, uint64 const col) const
{
  DLM_ASSERT(row < rows_, DLM_OUT_OF_RANGE);
  DLM_ASSERT(col < cols_, DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[row * cols_ + col];
}

//==============================================================================
// Mutators
//------------------------------------------------------------------------------
template <typename T>
inline T& DynamicMatrix<T>::At(uint64 const row, uint64 const col)
{
  DLM_ASSERT(row < rows_, DLM_OUT_OF_RANGE);
  DLM_ASSERT(col < cols_, DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[row * cols_ + col];
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
template <typename T>
inline uint64 DynamicMatrix<T>::ArraySize() const
{
  return rows_ * cols_;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_math_DynamicMatrix_inline_h_
