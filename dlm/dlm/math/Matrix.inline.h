//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_math_Matrix_inline_h_
#define dlm_math_Matrix_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/math/Matrix.h"

//------------------------------------------------------------------------------
#include "dlm/core/Exception.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.h"
#include "dlm/math/Vector.inline.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
inline Matrix<ROWS, COLS, T>::Matrix()
  : elements_() //value-intialization
{}

//==============================================================================
// Operators
//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
inline Matrix<ROWS, COLS, T> Matrix<ROWS, COLS, T>::operator+(
  Matrix const& other) const
{
  return Matrix(*this) += other;
}

//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
inline Matrix<ROWS, COLS, T> Matrix<ROWS, COLS, T>::operator-(
  Matrix const& other) const
{
  return Matrix(*this) -= other;
}

//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
inline Matrix<ROWS, COLS, T>& Matrix<ROWS, COLS, T>::operator+=(
  Matrix const& other)
{
  for (uint32 i = 0; i < ROWS * COLS; ++i)
  {
    elements_[i] += other.elements_[i];
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
inline Matrix<ROWS, COLS, T>& Matrix<ROWS, COLS, T>::operator-=(
  Matrix<ROWS, COLS, T> const& other)
{
  for (uint32 i = 0; i < ROWS * COLS; ++i)
  {
    elements_[i] -= other.elements_[i];
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
template <uint32 DIM>
inline Matrix<ROWS, DIM, T> Matrix<ROWS, COLS, T>::operator*(
  Matrix<COLS, DIM, T> const& other) const
{
  Matrix<ROWS, DIM, T> result;

  uint32 i0 = 0; //surprisingly, the version without multiplications for index
  uint32 i1 = 0; //calculation is much faster

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 row = 0; row < ROWS; ++row)
  {
    for (uint32 col = 0; col < DIM; ++col)
    {
      uint32 i2 = col; //that's the important one; you could get away with a
                       //minor performance penalty when getting rid of i0 and
      T sum = T();     //i1, but don't try it for i2

      // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      for (uint32 j = 0; j < COLS; ++j)
      {
        sum += elements_[i0 + j] * other.elements_[i2];

        i2 += DIM;
      }

      // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      result.elements_[i1 + col] = sum;
    }

    i0 += COLS;
    i1 += DIM;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
inline Matrix<ROWS, COLS, T>& Matrix<ROWS, COLS, T>::operator*=(
  Matrix<COLS, COLS, T> const& other)
{
  *this = *this * other;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
inline Vector<ROWS, T> Matrix<ROWS, COLS, T>::operator*(
  Vector<COLS, T> const& vector) const
{
  Vector<ROWS, T> result;

  uint32 i0 = 0; //see Matrix-Matrix-multiplication

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 row = 0; row < ROWS; ++row)
  {
    T sum = T();

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    for (uint32 col = 0; col < COLS; ++col)
    {
      sum += elements_[i0 + col] * vector.At(col);
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    result.At(row) = sum;

    i0 += COLS;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
inline Matrix<ROWS, COLS, T> Matrix<ROWS, COLS, T>::operator*(
  T const scalar) const
{
  return Matrix(*this) *= scalar;
}

//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
inline Matrix<ROWS, COLS, T> Matrix<ROWS, COLS, T>::operator/(
  T const scalar) const
{
  return Matrix(*this) /= scalar;
}

//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
inline Matrix<ROWS, COLS, T>& Matrix<ROWS, COLS, T>::operator*=(T const scalar)
{
  for (uint32 i = 0; i < ROWS * COLS; ++i)
  {
    elements_[i] *= scalar;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
inline Matrix<ROWS, COLS, T>& Matrix<ROWS, COLS, T>::operator/=(T const scalar)
{
  static_assert(std::is_floating_point<T>::value,
                "Only defined for floating point types.");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DLM_ASSERT(scalar != T(),DLM_DOMAIN_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < ROWS * COLS; ++i)
  {
    elements_[i] /= scalar;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
inline Matrix<ROWS, COLS, T> Matrix<ROWS, COLS, T>::operator-() const
{
  Matrix result(*this);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < ROWS * COLS; ++i)
  {
    result.elements_[i] = -result.elements_[i];
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//==============================================================================
// Operators - Accessors
//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
inline T const& Matrix<ROWS, COLS, T>::operator()(
  uint32 const row, uint32 const col) const
{
  DLM_ASSERT(row < ROWS, DLM_OUT_OF_RANGE);
  DLM_ASSERT(col < COLS, DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[row * COLS + col];
}

//==============================================================================
// Operators - Mutators
//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
inline T& Matrix<ROWS, COLS, T>::operator()(uint32 const row, uint32 const col)
{
  DLM_ASSERT(row < ROWS, DLM_OUT_OF_RANGE);
  DLM_ASSERT(col < COLS, DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[row * COLS + col];
}

//==============================================================================
// Accessors
//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
inline T const& Matrix<ROWS, COLS, T>::At(uint32 const row, uint32 const col) const
{
  DLM_ASSERT(row < ROWS, DLM_OUT_OF_RANGE);
  DLM_ASSERT(col < COLS, DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[row * COLS + col];
}

//==============================================================================
// Mutators
//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
inline T& Matrix<ROWS, COLS, T>::At(uint32 const row, uint32 const col)
{
  DLM_ASSERT(row < ROWS, DLM_OUT_OF_RANGE);
  DLM_ASSERT(col < COLS, DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return elements_[row * COLS + col];
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_math_Matrix_inline_h_
