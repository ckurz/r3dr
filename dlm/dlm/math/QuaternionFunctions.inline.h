//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_math_QuaternionFunctions_inline_h_
#define dlm_math_QuaternionFunctions_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/math/QuaternionFunctions.h"

//------------------------------------------------------------------------------
#include <cmath>
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <limits>
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.h"
#include "dlm/math/Vector.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Matrix.h"
#include "dlm/math/Matrix.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Quaternion.h"
#include "dlm/math/Quaternion.inline.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
template <typename T>
inline Quaternion<T> QuaternionFunctions::FromAxisAngle(
  Vector<3, T> axis, T angle)
{
  angle /= 2;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  axis.Normalize();
  axis *= sin(angle);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return Quaternion<T>(axis, cos(angle));
}

//------------------------------------------------------------------------------
template <typename T>
inline Quaternion<T> QuaternionFunctions::FromEulerAnglesYXZr(
  Vector<3, T> const& angles)
//conversion from Euler angles to quaternion, specifically designed for
//EulOrdYXZr (as specified in EulerAngles.h by Ken Shoemake)
//see http://www.euclideanspace.com/maths/geometry/rotations/conversions/eulerToQuaternion/index.htm
//for details
{
  T const a0 = angles.At(2) * 0.5;
  T const a1 = angles.At(1) * 0.5;
  T const a2 = angles.At(0) * 0.5;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T const ca0 = cos(a0);
  T const ca1 = cos(a1);
  T const ca2 = cos(a2);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T const sa0 = sin(a0);
  T const sa1 = sin(a1);
  T const sa2 = sin(a2);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Vector<3, T> const xyz(sa0 * ca1 * sa2 + ca0 * sa1 * ca2,
                         ca0 * ca1 * sa2 - sa0 * sa1 * ca2,
                         sa0 * ca1 * ca2 - ca0 * sa1 * sa2);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T            const w = ca0 * ca1 * ca2 + sa0 * sa1 * sa2;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return Quaternion<T>(xyz, w);
}

//------------------------------------------------------------------------------
template <typename T>
inline Vector<3, T> QuaternionFunctions::EulerAnglesYXZr(
  Quaternion<T> const& quaternion)
//conversion from quaternion to Euler angles, specifically designed for
//EulOrdYXZr (as specified in EulerAngles.h by Ken Shoemake)
//see http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToEuler/index.htm
//for details
{
  T const x     = quaternion.Xyz().X();
  T const y     = quaternion.Xyz().Y();
  T const z     = quaternion.Xyz().Z();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T const w     = quaternion.W();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T const x2    = x * x;
  T const y2    = y * y;
  T const z2    = z * z;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T const w2    = w * w;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T const norm  = x2 + y2 + z2 + w2;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T const value = x * w - y * z;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if      (value >  (0.5 - std::numeric_limits<T>::epsilon()) * norm)
  {
    return Vector<3, T>(2 * atan2(y, w),  M_PI_2, 0);
  }
  //----------------------------------------------------------------------------
  else if (value < -(0.5 - std::numeric_limits<T>::epsilon()) * norm)
  {
    return Vector<3, T>(2 * atan2(y, w), -M_PI_2, 0);
  }
  //----------------------------------------------------------------------------
  else
  {
    return Vector<3, T>(atan2(2 * (x * z + y * w), z2 - x2 - y2 + w2),
                        asin (2 * (value / norm)),
                        atan2(2 * (x * y + z * w), y2 - x2 - z2 + w2));
  }
}

//------------------------------------------------------------------------------
template <typename T>
inline Quaternion<T> QuaternionFunctions::FromRotation(
  Matrix<3, 3, T> const& rotation)
{
  //see the quaternion tutorial by Ken Shoemake for details

  Vector<3, T> xyz;

  T w = 0;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T const trace = rotation.At(0, 0) + rotation.At(1, 1) + rotation.At(2, 2);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (trace >= 0)
  {
    T scale = sqrt(trace + 1);
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    w       = 0.5 * scale;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    scale   = 0.5 / scale;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    xyz.X() = (rotation.At(2, 1) - rotation.At(1, 2)) * scale;
    xyz.Y() = (rotation.At(0, 2) - rotation.At(2, 0)) * scale;
    xyz.Z() = (rotation.At(1, 0) - rotation.At(0, 1)) * scale;
  }
  //----------------------------------------------------------------------------
  else if ((rotation.At(0, 0) > rotation.At(1, 1)) &&
           (rotation.At(0, 0) > rotation.At(2, 2)))
  {
    T scale =
      sqrt(rotation.At(0, 0) - rotation.At(1, 1) - rotation.At(2, 2) + 1);
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    xyz.X() = 0.5 * scale;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    scale   = 0.5 / scale;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    xyz.Y() = (rotation.At(1, 0) + rotation.At(0, 1)) * scale;
    xyz.Z() = (rotation.At(0, 2) + rotation.At(2, 0)) * scale;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    w       = (rotation.At(2, 1) - rotation.At(1, 2)) * scale;
  }
  //----------------------------------------------------------------------------
  else if (rotation.At(1, 1) > rotation.At(2, 2))
  {
    T scale =
      sqrt(rotation.At(1, 1) - rotation.At(0, 0) - rotation.At(2, 2) + 1);
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    xyz.Y() = 0.5 * scale;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    scale   = 0.5 / scale;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    xyz.X() = (rotation.At(1, 0) + rotation.At(0, 1)) * scale;
    xyz.Z() = (rotation.At(2, 1) + rotation.At(1, 2)) * scale;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    w       = (rotation.At(0, 2) - rotation.At(2, 0)) * scale;
  }
  //----------------------------------------------------------------------------
  else
  {
    T scale =
      sqrt(rotation.At(2, 2) - rotation.At(0, 0) - rotation.At(1, 1) + 1);
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    xyz.Z() = 0.5 * scale;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    scale   = 0.5 / scale;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    xyz.X() = (rotation.At(0, 2) + rotation.At(2, 0)) * scale;
    xyz.Y() = (rotation.At(2, 1) + rotation.At(1, 2)) * scale;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    w       = (rotation.At(1, 0) - rotation.At(0, 1)) * scale;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return Quaternion<T>(xyz, w);
}

//------------------------------------------------------------------------------
template <typename T>
inline Matrix<3, 3, T> QuaternionFunctions::Rotation(
  Quaternion<T> const& quaternion)
{
  //see the quaternion tutorial by Ken Shoemake for details

  T const norm = quaternion.Magnitude();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T const s    = (norm > 0) ? (2 / norm) : 0;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T const xs   = quaternion.Xyz().X() * s;
  T const ys   = quaternion.Xyz().Y() * s;
  T const zs   = quaternion.Xyz().Z() * s;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T const wx   = quaternion.W()       * xs;
  T const wy   = quaternion.W()       * ys;
  T const wz   = quaternion.W()       * zs;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T const xx   = quaternion.Xyz().X() * xs;
  T const xy   = quaternion.Xyz().X() * ys;
  T const xz   = quaternion.Xyz().X() * zs;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T const yy   = quaternion.Xyz().Y() * ys;
  T const yz   = quaternion.Xyz().Y() * zs;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T const zz   = quaternion.Xyz().Z() * zs;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Matrix<3, 3, T> rotation;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  rotation.At(0, 0) = 1 - (yy + zz);
  rotation.At(0, 1) =     (xy - wz);
  rotation.At(0, 2) =     (xz + wy);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  rotation.At(1, 0) =     (xy + wz);
  rotation.At(1, 1) = 1 - (xx + zz);
  rotation.At(1, 2) =     (yz - wx);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  rotation.At(2, 0) =     (xz - wy);
  rotation.At(2, 1) =     (yz + wx);
  rotation.At(2, 2) = 1 - (xx + yy);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return rotation;
}

//------------------------------------------------------------------------------
template <typename T>
inline Quaternion<T> QuaternionFunctions::Slerp(
  Quaternion<T> const& a, Quaternion<T> const& b, T t)
{
  if (t < 0) { t = 0; }
  if (t > 1) { t = 1; }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T cosOmega = a.Dot(b);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Quaternion<T> tmp = b;

  if (cosOmega < 0)
  {
    tmp      = -b;
    cosOmega = -cosOmega;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T const    omega = acos(cosOmega);
  T const sinOmega = sin (   omega);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Quaternion<T> result = a   * (sin((1 - t) * omega) / sinOmega);
  result              += tmp * (sin(     t  * omega) / sinOmega);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_math_QuaternionFunctions_inline_h_
