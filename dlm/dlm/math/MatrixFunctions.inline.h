//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_math_MatrixFunctions_inline_h_
#define dlm_math_MatrixFunctions_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/math/MatrixFunctions.h"

//------------------------------------------------------------------------------
#include "dlm/math/MatrixFunctions.Eigen.inline.h"

//------------------------------------------------------------------------------
#include "dlm/math/Matrix.h"
#include "dlm/math/Matrix.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.h"
#include "dlm/math/Vector.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/DynamicMatrix.h"
#include "dlm/math/DynamicMatrix.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
inline Vector<COLS, T> MatrixFunctions::GetRow(
  Matrix<ROWS, COLS, T> const& matrix, uint32 const row)
{
  DLM_ASSERT(row < ROWS, DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Vector<COLS, T> result;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < COLS; ++i)
  {
    result.At(i) = matrix.At(row, i);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
inline Vector<ROWS, T> MatrixFunctions::GetCol(
  Matrix<ROWS, COLS, T> const& matrix, uint32 const col)
{
  DLM_ASSERT(col < COLS, DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Vector<ROWS, T> result;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < ROWS; ++i)
  {
    result.At(i) = matrix.At(i, col);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
inline void MatrixFunctions::SetRow(Matrix<ROWS, COLS, T>& matrix,
                                    uint32          const  row,
                                    Vector<COLS, T> const& vector)
{
  DLM_ASSERT(row < ROWS, DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < COLS; ++i)
  {
    matrix.At(row, i) = vector.At(i);
  }
}

//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
inline void MatrixFunctions::SetCol(Matrix<ROWS, COLS, T>& matrix,
                                    uint32          const  col,
                                    Vector<ROWS, T> const& vector)
{
  DLM_ASSERT(col < COLS, DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < ROWS; ++i)
  {
    matrix.At(i, col) = vector.At(i);
  }
}

//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
inline Matrix<COLS, ROWS, T> MatrixFunctions::Transpose(
  Matrix<ROWS, COLS, T> const& matrix)
{
  Matrix<COLS, ROWS, T> result;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 row = 0; row < ROWS; ++row)
  {
    for (uint32 col = 0; col < COLS; ++col)
    {
      result.At(col, row) = matrix.At(row, col);
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <uint32 DIM0, uint32 DIM1, uint32 ROWS, uint32 COLS, typename T>
inline Matrix<DIM0, DIM1, T> MatrixFunctions::GetSubmatrix(
  Matrix<ROWS, COLS, T> const& matrix, uint32 const row, uint32 const col)
{
  static_assert(DIM0 <= ROWS && DIM1 <= COLS, "Submatrix too large.");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DLM_ASSERT(row + DIM0 <= ROWS, DLM_OUT_OF_RANGE);
  DLM_ASSERT(col + DIM1 <= COLS, DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Matrix<DIM0, DIM1, T> result;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < DIM0; ++i)
  {
    for (uint32 j = 0; j < DIM1; ++j)
    {
      result.At(i, j) = matrix.At(row + i, col + j);
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <uint32 DIM0, uint32 DIM1, uint32 ROWS, uint32 COLS, typename T>
inline void MatrixFunctions::SetSubmatrix(
  Matrix<ROWS, COLS, T>      & matrix,
  uint32                const  row,
  uint32                const  col,
  Matrix<DIM0, DIM1, T> const& submatrix)
{
  static_assert(DIM0 <= ROWS && DIM1 <= COLS, "Submatrix too large.");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DLM_ASSERT(row + DIM0 <= ROWS, DLM_OUT_OF_RANGE);
  DLM_ASSERT(col + DIM1 <= COLS, DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < DIM0; ++i)
  {
    for (uint32 j = 0; j < DIM1; ++j)
    {
      matrix.At(row + i, col + j) = submatrix.At(i, j);
    }
  }
}

//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
inline void MatrixFunctions::MakeIdentity(Matrix<ROWS, COLS, T>& matrix)
{
  matrix = Matrix<ROWS, COLS, T>();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint32 const range = (ROWS < COLS) ? ROWS : COLS;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < range; ++i)
  {
    matrix.At(i, i) = 1;
  }
}

//------------------------------------------------------------------------------
template <uint32 ROWS, uint32 COLS, typename T>
inline Matrix<ROWS, COLS, T> Identity()
{
  Matrix<ROWS, COLS, T> identity;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  MakeIdentity(identity);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return identity;
}

//------------------------------------------------------------------------------
//! \brief Create a 2-by-2 FloatType identity matrix.
//! \deprecated This function is introduced deprecated to avoid a compiler bug
//! in Visual Studio 2012 for WeightingMatrix::WeightingMatrix() and may be
//! removed if another solution is found.
inline Matrix2f MatrixFunctions::Identity2f()
{
  Matrix2f identity;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  MakeIdentity(identity);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return identity;
}

//------------------------------------------------------------------------------
//! \brief Create a 3-by-4 FloatType identity matrix.
//! \deprecated This function is introduced deprecated to avoid a compiler bug
//! in Visual Studio 2012 for ProjectiveCamera::ProjectiveCamera() and may be
//! removed if another solution is found.
inline Matrix3x4f MatrixFunctions::Identity3x4f()
{
  Matrix3x4f identity;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  MakeIdentity(identity);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return identity;
}

//------------------------------------------------------------------------------
//! \brief Invert the given matrix in-place.
//! @param matrix The matrix to be inverted.
template <uint32 SIZE, typename T>
inline void MatrixFunctions::Invert(Matrix<SIZE, SIZE, T>& matrix)
{
  Eigen_Reimplementation::Invert(matrix);
}

//------------------------------------------------------------------------------
template <uint32 SIZE, typename T>
inline Matrix<SIZE, SIZE, T> MatrixFunctions::Inverse(
  Matrix<SIZE, SIZE, T> const& matrix)
{
  Matrix<SIZE, SIZE, T> result = matrix;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Invert(result);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template<uint32 ROWS, uint32 COLS, typename T>
inline DynamicMatrix<T> MatrixFunctions::Convert(
  Matrix<ROWS, COLS, T> const& matrix)
{
  DynamicMatrix<T> result(ROWS, COLS);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 row = 0; row < ROWS; ++row)
  {
    for (uint32 col = 0; col < COLS; ++col)
    {
      result.At(row, col) = matrix.At(row, col);
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
