//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// Copyright (C)  2018  TrackMen Ltd.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_math_DynamicVector_h_
#define dlm_math_DynamicVector_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/core/Types.h"
#include "dlm/core/AutoArrayPtr.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// DynamicVector template class
//------------------------------------------------------------------------------
template <typename T>
class DynamicVector
{
public:
  //============================================================================
  // Constructors
  //----------------------------------------------------------------------------
  DynamicVector();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicVector(uint64 const size);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicVector(DynamicVector const& other);

  //============================================================================
  // Operators
  //----------------------------------------------------------------------------
  DynamicVector& operator= (DynamicVector const& other);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicVector  operator+ (DynamicVector const& other) const;
  DynamicVector  operator- (DynamicVector const& other) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicVector& operator+=(DynamicVector const& other);
  DynamicVector& operator-=(DynamicVector const& other);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicVector  operator* (T const scalar) const;
  DynamicVector  operator/ (T const scalar) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicVector& operator*=(T const scalar);
  DynamicVector& operator/=(T const scalar);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicVector  operator- () const;

  //============================================================================
  // Operators - Accessors
  //----------------------------------------------------------------------------
  T const& operator()(uint64 const i) const;

  //============================================================================
  // Operators - Mutators
  //----------------------------------------------------------------------------
  T& operator()(uint64 const i);

  //============================================================================
  // Accessors
  //----------------------------------------------------------------------------
  uint64 Size() const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T const& At(uint64 const i) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // convenience accesssors
  T const& X() const;
  T const& Y() const;
  T const& Z() const;
  T const& W() const;

  //============================================================================
  // Mutators
  //----------------------------------------------------------------------------
  T& At(uint64 const i);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // convenience mutators
  T& X();
  T& Y();
  T& Z();
  T& W();

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  T Dot(DynamicVector const& other) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicVector Cross(DynamicVector const& other) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T Magnitude() const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DynamicVector& Normalize ();
  DynamicVector  Normalized() const;

private:
  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  AutoArrayPtr<T> elements_;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint64 size_;
};

//==============================================================================
// Type definitions
//------------------------------------------------------------------------------
typedef DynamicVector<float64> DVectord;

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_math_DynamicVector_h_
