//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_math_Quaternion_inline_h_
#define dlm_math_Quaternion_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/math/Quaternion.h"

//------------------------------------------------------------------------------
#include "dlm/core/Exception.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.inline.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
template<typename T>
inline Quaternion<T>::Quaternion()
  : w_(1) //initialize to identity quaternion
{}

//------------------------------------------------------------------------------
template<typename T>
inline Quaternion<T>::Quaternion(Vector<3, T> const& xyz, T const w)
  : xyz_(xyz)
  , w_  (w  )
{}

//==============================================================================
// Operators
//------------------------------------------------------------------------------
template <typename T>
inline Quaternion<T> Quaternion<T>::operator+(Quaternion<T> const& other) const
{
  return Quaternion(*this) += other;
}

//------------------------------------------------------------------------------
template <typename T>
inline Quaternion<T> Quaternion<T>::operator-(Quaternion<T> const& other) const
{
  return Quaternion(*this) -= other;
}

//------------------------------------------------------------------------------
template <typename T>
inline Quaternion<T>& Quaternion<T>::operator+=(Quaternion<T> const& other)
{
  xyz_ += other.xyz_;
  w_   += other.w_;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
template <typename T>
inline Quaternion<T>& Quaternion<T>::operator-=(Quaternion<T> const& other)
{
  xyz_ -= other.xyz_;
  w_   -= other.w_;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
template <typename T>
inline Quaternion<T> Quaternion<T>::operator*(Quaternion<T> const& other) const
{
  return Quaternion(*this) *= other;
}

//------------------------------------------------------------------------------
template <typename T>
inline Quaternion<T>& Quaternion<T>::operator*=(Quaternion<T> const& other)
{
  Vector<3, T> tmp = xyz_.Cross(other.xyz_);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  tmp += other.xyz_ * w_;
  tmp += xyz_ * other.w_;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  w_   = w_ * other.w_ - xyz_.Dot(other.xyz_);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  xyz_ = tmp;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
template <typename T>
inline Vector<3, T> Quaternion<T>::operator*(Vector<3, T> const& vector) const
{
  //NVidia SDK implementation
  Vector<3, T> tmp0(xyz_.Cross(vector));
  Vector<3, T> tmp1(xyz_.Cross(tmp0  ));

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  tmp0 *= 2 * w_;
  tmp1 *= 2;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return vector + tmp0 + tmp1;
}

//------------------------------------------------------------------------------
template <typename T>
inline Quaternion<T> Quaternion<T>::operator*(T const value) const
{
  return Quaternion(*this) *= value;
}

//------------------------------------------------------------------------------
template <typename T>
inline Quaternion<T> Quaternion<T>::operator/(T const value) const
{
  return Quaternion(*this) /= value;
}

//------------------------------------------------------------------------------
template <typename T>
inline Quaternion<T>& Quaternion<T>::operator*=(T const value)
{
  xyz_  *= value;
  w_    *= value;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
template <typename T>
inline Quaternion<T>& Quaternion<T>::operator/=(T const value)
{
  DLM_ASSERT(value != T(), DLM_DOMAIN_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  xyz_ /= value;
  w_   /= value;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
template <typename T>
inline Quaternion<T> Quaternion<T>::operator-() const
{
  return Quaternion(-xyz_, -w_);
}

//==============================================================================
// Accessors
//------------------------------------------------------------------------------
template <typename T>
inline Vector<3, T> const& Quaternion<T>::Xyz() const
{
  return xyz_;
}

//------------------------------------------------------------------------------
template <typename T>
inline T Quaternion<T>::W() const
{
  return w_;
}

//==============================================================================
// Mutators
//------------------------------------------------------------------------------
template <typename T>
inline Vector<3, T>& Quaternion<T>::Xyz()
{
  return xyz_;
}

//------------------------------------------------------------------------------
template <typename T>
inline T& Quaternion<T>::W()
{
  return w_;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
template <typename T>
inline T Quaternion<T>::Dot(Quaternion const& other) const
{
  return xyz_.Dot(other.xyz_) + w_ * other.w_;
}

//------------------------------------------------------------------------------
template <typename T>
inline T Quaternion<T>::Magnitude() const
{
  return sqrt(Dot(*this));
}

//------------------------------------------------------------------------------
template <typename T>
inline Quaternion<T> Quaternion<T>::Normalized() const
{
  return Quaternion(*this).Normalize();
}

//------------------------------------------------------------------------------
template <typename T>
inline Quaternion<T>& Quaternion<T>::Normalize()
{
  return *this /= Magnitude();
}

//------------------------------------------------------------------------------
template <typename T>
inline Quaternion<T> Quaternion<T>::Conjugated() const
{
  return Quaternion(*this).Conjugate();
}

//------------------------------------------------------------------------------
template <typename T>
inline Quaternion<T>& Quaternion<T>::Conjugate()
{
  xyz_ = -xyz_;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
template <typename T>
inline Quaternion<T> Quaternion<T>::Inverted() const
{
  return Quaternion(*this).Invert();
}

//------------------------------------------------------------------------------
template <typename T>
inline Quaternion<T>& Quaternion<T>::Invert()
{
  return Conjugate() /= Dot(*this);
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_math_Quaternion_inline_h_
