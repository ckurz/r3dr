//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// Copyright (C)  2018  TrackMen Ltd.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_math_DynamicVectorFunctions_inline_h_
#define dlm_math_DynamicVectorFunctions_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/math/DynamicVectorFunctions.h"

//------------------------------------------------------------------------------
#include "dlm/math/Vector.h"
#include "dlm/math/Vector.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/DynamicVector.h"
#include "dlm/math/DynamicVector.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
template <typename T>
inline
DynamicVector<T>
DynamicVectorFunctions::Homogenize(
  DynamicVector<T> const& vector)
{
  auto result = DynamicVector<T>(vector.Size() + 1);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (auto i = static_cast<uint64>(0); i < vector.Size(); ++i)
  {
    result(i) = vector(i);
  }

  result(vector.Size()) = static_cast<T>(1);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <typename T>
inline
DynamicVector<T>
DynamicVectorFunctions::Inhomogenize(
  DynamicVector<T> const& vector)
{
  DLM_ASSERT(vector.Size() > 1, DLM_RUNTIME_ERROR);

  auto result = DynamicVector<T>(vector.Size() - 1);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (auto i = static_cast<uint64>(0); i < vector.Size() - 1; ++i)
  {
    result(i) = vector(i);
  }

  result /= vector(vector.Size() - 1);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <typename T>
inline
DynamicMatrix<T>
DynamicVectorFunctions::TensorProduct(
  DynamicVector<T> const& a,
  DynamicVector<T> const& b)
{
  auto result = DynamicMatrix<T>(a.Size(), b.Size());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (auto row = static_cast<uint64>(0); row < a.Size(); ++row)
  {
    for (auto col = static_cast<uint64>(0); col < b.Size(); ++col)
    {
      result(row, col) = a(row) * b(col);
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <typename T>
inline
DynamicVector<T>
DynamicVectorFunctions::CWiseMultiply(
  DynamicVector<T> const& a,
  DynamicVector<T> const& b)
{
  DLM_ASSERT(a.Size() == b.Size(), DLM_RUNTIME_ERROR);

  auto result = a;

  for (auto i = static_cast<dlm::uint64>(0); i < result.Size(); ++i)
  {
    result(i) *= b(i);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <typename T>
inline
DynamicVector<T>
DynamicVectorFunctions::CWiseDivide(
  DynamicVector<T> const& a,
  DynamicVector<T> const& b)
{
  static_assert(std::is_floating_point<T>::value,
                "Only defined for floating point types.");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  auto result = a;

  for (auto i = static_cast<dlm::uint64>(0); i < result.Size(); ++i)
  {
    auto const divisor = b(i);

    DLM_ASSERT(divisor != T(), DLM_DOMAIN_ERROR);

    result(i) /= divisor;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template<uint32 SIZE, typename T>
inline Vector<SIZE, T> DynamicVectorFunctions::Convert(
  DynamicVector<T> const& vector)
{
  DLM_ASSERT(       vector.Size()  <  UInt32Max, DLM_SIZE_MISMATCH);
  DLM_ASSERT(uint32(vector.Size()) == SIZE,      DLM_SIZE_MISMATCH);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Vector<SIZE, T> result;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < SIZE; ++i)
  {
    result.At(i) = vector.At(i);
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <typename T>
inline
DynamicMatrix<T>
DynamicVectorFunctions::AsRows(
  DynamicVector<T> const& vector,
  dlm::uint64      const  rowSize)
{
  auto const rows      = vector.size() / rowSize;
  auto const remainder = vector.size() % rowSize;

  DLM_ASSERT(remainder == 0, DLM_RUNTIME_ERROR);

  auto result = DynamicMatrix<T>(rows, rowSize);

  for (auto i = static_cast<uint64>(0); i < rows; ++i)
  {
    for (auto j = static_cast<uint64>(0); j < rowSize; ++j)
    {
      result(i, j) = vector(i * rowSize + j);
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <typename T>
inline
DynamicMatrix<T>
DynamicVectorFunctions::AsCols(
  DynamicVector<T> const& vector,
  dlm::uint64      const  colSize)
{
  auto const cols      = vector.size() / colSize;
  auto const remainder = vector.size() % colSize;

  DLM_ASSERT(remainder == 0, DLM_RUNTIME_ERROR);

  auto result = DynamicMatrix<T>(cols, colSize);

  for (auto i = static_cast<uint64>(0); i < colSize; ++i)
  {
    for (auto j = static_cast<uint64>(0); j < cols; ++j)
    {
      result(i, j) = vector(j * colSize + i);
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <typename T>
inline T DynamicVectorFunctions::Sum(DynamicVector<T> const& vector)
{
  T sum = static_cast<T>(0);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint32 i = 0; i < vector.Size(); ++i)
  {
    sum += vector(i);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return sum;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_math_DynamicVectorFunctions_inline_h_
