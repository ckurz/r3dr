//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/scene/Scene.h"

//------------------------------------------------------------------------------
#include "dlm/core/Exception.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
//! \brief Unreference the indicated ImageRepresentation object.
//! @param iRepId The Identifier for the IRep that shall be unreferenced.
//!
//! All connections of the specified IRep within the Scene will be removed.
//! Additional objects may then be deleted, depending on the auto-deletion
//! settings.
//!
//! If the Identifier is not in the valid range of the \link iRepS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
void Scene::Unreference(IRepId const iRepId)
{
  IRep& iRep = Get(iRepId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (iRep.Reference<PRep>::IsValid())
  {
    PRepId const pRepId = iRep.Reference<PRep>::Get();

    Get(pRepId).Reference<IRep>::Remove();

    iRep.Reference<PRep>::Remove();

    DeleteUnreferenced(pRepId);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (iRep.Reference<CRep>::IsValid())
  {
    CRepId const cRepId = iRep.Reference<CRep>::Get();

    Get(cRepId).ReferenceSet<IRep>::Remove(iRepId);

    iRep.Reference<CRep>::Remove();

    DeleteUnreferenced(cRepId);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (iRep.Reference<DRep>::IsValid())
  {
    DRepId const dRepId = iRep.Reference<DRep>::Get();

    Get(dRepId).ReferenceSet<IRep>::Remove(iRepId);

    iRep.Reference<DRep>::Remove();

    DeleteUnreferenced(dRepId);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (iRep.Reference<FSet>::IsValid())
  {
    FSetId const fSetId = iRep.Reference<FSet>::Get();

    Get(fSetId).Reference<IRep>::Remove();

    iRep.Reference<FSet>::Remove();

    DeleteUnreferenced(fSetId);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (iRep.Reference<TSeq>::IsValid())
  {
    TSeqId const tSeqId = iRep.Reference<TSeq>::Get();

    Get(tSeqId).ReferenceSet<IRep>::Remove(iRepId);

    iRep.Reference<TSeq>::Remove();

    DeleteUnreferenced(tSeqId);
  }
}

//------------------------------------------------------------------------------
//! \brief Unreference the indicated FeaturePointSet object.
//! @param fSetId The Identifier for the FSet that shall be unreferenced.
//!
//! All connections of the specified FSet within the Scene will be removed; all
//! FeaturePointRepresentation objects contained in the FSet will be
//! \link Unreference(FSetId const fSetId, FRepId const fRepId)
//! unreferenced\endlink as well. This may cause additional objects to be
//! deleted, depending on the auto-deletion settings.
//!
//! If the Identifier is not in the valid range of the \link iRepS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
void Scene::Unreference(FSetId const fSetId)
{
  FSet& fSet = Get(fSetId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (fSet.Reference<IRep>::IsValid())
  {
    Get(fSet.Reference<IRep>::Get()).Reference<FSet>::Remove();

    fSet.Reference<IRep>::Remove();
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < fSet.Range(); ++i)
  {
    if (!fSet.Active(i)) { continue; }

    Unreference(fSetId, FRepId(i));
  }
}

//------------------------------------------------------------------------------
//! \brief Unreference the indicated WeightingMatrixRepresentation object.
//! @param wMatId The Identifier for the WMat that shall be unreferenced.
//!
//! All connections of the specified WMat within the Scene will be removed.
//!
//! If the Identifier is not in the valid range of the \link wMatS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
void Scene::Unreference(WMatId const wMatId)
{
  WMat& wMat = Get(wMatId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  auto fRepS = wMat.ReferencePairSet<FSet, FRep>::Get();

  for (auto it = fRepS.cbegin(); it != fRepS.cend(); ++it)
  {
    Get(it->first, it->second).Reference<WMat>::Remove();
  }

  wMat.ReferencePairSet<FSet, FRep>::Clear();
}

//------------------------------------------------------------------------------
//! \brief Unreference the indicated FeaturePointTrack object.
//! @param fTrkId The Identifier for the FTrk that shall be unreferenced.
//!
//! All connections of the specified FTrk within the Scene will be removed.
//! Additional objects may then be deleted, depending on the auto-deletion
//! settings.
//!
//! If the Identifier is not in the valid range of the \link fTrkS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
void Scene::Unreference(FTrkId const fTrkId)
{
  FTrk& fTrk = Get(fTrkId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (auto it = fTrk.cbegin(); it != fTrk.cend(); ++it)
  {
    Get(it->first, it->second).Reference<FTrk>::Remove();
  }

  fTrk.clear();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (fTrk.Reference<TSeq>::IsValid())
  {
    TSeqId const tSeqId = fTrk.Reference<TSeq>::Get();

    Get(tSeqId).ReferenceSet<FTrk>::Remove(fTrkId);

    fTrk.Reference<TSeq>::Remove();

    DeleteUnreferenced(tSeqId);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (fTrk.Reference<ORep>::IsValid())
  {
    ORepId const oRepId = fTrk.Reference<ORep>::Get();

    Get(oRepId).ReferenceSet<FTrk>::Remove(fTrkId);

    fTrk.Reference<ORep>::Remove();

    DeleteUnreferenced(oRepId);
  }
}

//------------------------------------------------------------------------------
//! \brief Unreference the indicated ProjectiveCameraRepresentation object.
//! @param pRepId The Identifier for the PRep that shall be unreferenced.
//!
//! All connections of the specified PRep within the Scene will be removed.
//!
//! If the Identifier is not in the valid range of the \link pRepS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
void Scene::Unreference(PRepId const pRepId)
{
  PRep& pRep = Get(pRepId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (pRep.Reference<IRep>::IsValid())
  {
    IRepId const iRepId = pRep.Reference<IRep>::Get();

    Get(iRepId).Reference<PRep>::Remove();

    pRep.Reference<IRep>::Remove();
  }
}

//------------------------------------------------------------------------------
//! \brief Unreference the indicated CameraRepresentation object.
//! @param cRepId The Identifier for the CRep that shall be unreferenced.
//!
//! All connections of the specified CRep within the Scene will be removed.
//!
//! If the Identifier is not in the valid range of the \link cRepS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
void Scene::Unreference(CRepId const cRepId)
{
  CRep& cRep = Get(cRepId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  auto iRepS = cRep.ReferenceSet<IRep>::Get();

  for (auto it = iRepS.cbegin(); it != iRepS.cend(); ++it)
  {
    Get(*it).Reference<CRep>::Remove();
  }

  cRep.ReferenceSet<IRep>::Clear();
}

//------------------------------------------------------------------------------
//! \brief Unreference the indicated DistortionRepresentation object.
//! @param dRepId The Identifier for the DRep that shall be unreferenced.
//!
//! All connections of the specified DRep within the Scene will be removed.
//!
//! If the Identifier is not in the valid range of the \link dRepS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
void Scene::Unreference(DRepId const dRepId)
{
  DRep& dRep = Get(dRepId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  std::set<IRepId> const& iRepS = dRep.ReferenceSet<IRep>::Get();

  for (auto it = iRepS.cbegin(); it != iRepS.cend(); ++it)
  {
    Get(*it).Reference<DRep>::Remove();
  }

  dRep.ReferenceSet<IRep>::Clear();
}

//------------------------------------------------------------------------------
//! \brief Unreference the indicated ObjectPointRepresentation object.
//! @param oRepId The Identifier for the ORep that shall be unreferenced.
//!
//! All connections of the specified ORep within the Scene will be removed.
//! Additional objects may then be deleted, depending on the auto-deletion
//! settings.
//!
//! If the Identifier is not in the valid range of the \link oRepS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
void Scene::Unreference(ORepId const oRepId)
{
  ORep& oRep = Get(oRepId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  std::set<FTrkId> const& fTrkS = oRep.ReferenceSet<FTrk>::Get();

  for (auto it = fTrkS.cbegin(); it != fTrkS.cend(); ++it)
  {
    Get(*it).Reference<ORep>::Remove();
  }

  oRep.ReferenceSet<FTrk>::Clear();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (oRep.Reference<TSeq>::IsValid())
  {
    TSeqId const tSeqId = oRep.Reference<TSeq>::Get();

    Get(tSeqId).ReferenceSet<ORep>::Remove(oRepId);

    oRep.Reference<TSeq>::Remove();

    DeleteUnreferenced(tSeqId);
  }
}

//------------------------------------------------------------------------------
//! \brief Unreference the indicated TransformationSequence object.
//! @param tSeqId The Identifier for the TSeq that shall be unreferenced.
//!
//! All connections of the specified TSeq within the Scene will be removed.
//! Additional objects may then be deleted, depending on the auto-deletion
//! settings.
//!
//! If the Identifier is not in the valid range of the \link tSeqS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
void Scene::Unreference(TSeqId const tSeqId)
{
  TSeq& tSeq = Get(tSeqId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  std::set<IRepId> const& iRepS = tSeq.ReferenceSet<IRep>::Get();

  for (auto it = iRepS.cbegin(); it != iRepS.cend(); ++it)
  {
    Get(*it).Reference<TSeq>::Remove();
  }

  tSeq.ReferenceSet<IRep>::Clear();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  std::set<FTrkId> const& fTrkS = tSeq.ReferenceSet<FTrk>::Get();

  for (auto it = fTrkS.cbegin(); it != fTrkS.cend(); ++it)
  {
    Get(*it).Reference<TSeq>::Remove();
  }

  tSeq.ReferenceSet<FTrk>::Clear();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  std::set<ORepId> const& oRepS = tSeq.ReferenceSet<ORep>::Get();

  for (auto it = oRepS.cbegin(); it != oRepS.cend(); ++it)
  {
    Get(*it).Reference<TSeq>::Remove();
  }

  tSeq.ReferenceSet<ORep>::Clear();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  std::set<TSeqId> const& tSeqS = tSeq.ReferenceSet<TSeq>::Get();

  for (auto it = tSeqS.cbegin(); it != tSeqS.cend(); ++it)
  {
    Get(*it).Reference<TSeq>::Remove();
  }

  tSeq.ReferenceSet<TSeq>::Clear();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (tSeq.Reference<TSeq>::IsValid())
  {
    TSeqId const tsRefId = tSeq.Reference<TSeq>::Get();

    Get(tsRefId).ReferenceSet<TSeq>::Remove(tSeqId);

    tSeq.Reference<TSeq>::Remove();

    DeleteUnreferenced(tsRefId); // the only one we care about
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (auto it = tSeq.cbegin(); it != tSeq.cend(); ++it)
  {
    Get(it->first).Remove(tSeqId);

    DeleteUnreferenced(it->first);
  }

  tSeq.clear();
}

//------------------------------------------------------------------------------
//! \brief Unreference the indicated TransformationRepresentation object.
//! @param tRepId The Identifier for the TRep that shall be unreferenced.
//!
//! All connections of the specified TRep within the Scene will be removed.
//!
//! If the Identifier is not in the valid range of the \link tRepS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
void Scene::Unreference(TRepId const tRepId)
{
  TRep& tRep = Get(tRepId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  TRep::TSeqMap const& tSeqM = tRep.TSeqM();

  for (auto it = tSeqM.cbegin(); it != tSeqM.cend(); ++it)
  {
    TSeq& tSeq = Get(it->first);

    std::vector<uint64> const positions = tSeq.GetTRepPositions(tRepId);

    DLM_ASSERT(positions.size() == it->second, DLM_RUNTIME_ERROR);

    for (auto pIt = positions.cbegin(); pIt != positions.cend(); ++pIt)
    {
      // The positions are in reverse order, so there's no need to worry about
      // corrupting the data structure.
      tSeq.Remove(tRepId, *pIt);
    }
  }

  tRep.Clear();
}

//------------------------------------------------------------------------------
//! \brief Unreference the indicated FeaturePointRepresentation object in the
//! specified FeaturePointSet.
//! @param fSetId The Identifier for the FSet the FRep resides in.
//! @param fRepId The Identifier for the FRep that shall be unreferenced.
//!
//! All connections of the specified FRep within the Scene will be removed.
//! Additional objects may then be deleted, depending on the auto-deletion
//! settings.
//!
//! If either Identifier is not in the valid range of the container, an
//! OutOfRange exception will be thrown.
//!
//! If either Identifier is not valid, a RuntimeError exception will be thrown.
void Scene::Unreference(FSetId const fSetId, FRepId const fRepId)
{
  FRep& fRep = Get(fSetId, fRepId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (fRep.Reference<FTrk>::IsValid())
  {
    FTrkId const fTrkId = fRep.Reference<FTrk>::Get();

    Get(fTrkId).Remove(fSetId);

    fRep.Reference<FTrk>::Remove();

    DeleteUnreferenced(fTrkId);
  }
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
