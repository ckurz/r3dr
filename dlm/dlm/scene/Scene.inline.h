//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_scene_Scene_inline_h_
#define dlm_scene_Scene_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/scene/Scene.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
//! \brief The default constructor.
//! @param autoDelete Switches auto-delete functionality \b on (`true`) or
//! \b off(`false`).
inline Scene::Scene(bool const autoDelete)
  : autoDeleteFSet_(autoDelete)
  , autoDeleteWMat_(autoDelete)
  , autoDeleteFTrk_(autoDelete)
  , autoDeletePRep_(autoDelete)
  , autoDeleteCRep_(autoDelete)
  , autoDeleteDRep_(autoDelete)
  , autoDeleteORep_(autoDelete)
  , autoDeleteTSeq_(autoDelete)
  , autoDeleteTRep_(autoDelete)
{}

//==============================================================================
// Accessors/Mutators
//------------------------------------------------------------------------------
//! \brief Access the internal ImageRepresentation storage.
//! @return A \b const \b reference to the ImageRepresentation storage.
//!
//! \note Access to the internal storage is provided for convenience; direct
//! manipulation is not supported.
//!
//! \sa CreateIRep(), Delete(IRepId const iRepId), 
//! IsValid(IRepId const iRepId) const, Get(IRepId const iRepId),
//! ReserveIRepS(uint64 const size), ExpandIRepS(uint64 const newSize).
inline Scene::IRepStorage const& Scene::IRepS() const
{
  return iRepS_;
}

//------------------------------------------------------------------------------
//! \brief Access the internal FeaturePointSet storage.
//! @return A \b const \b reference to the FeaturePointSet storage.
//!
//! \note Access to the internal storage is provided for convenience; direct
//! manipulation is not supported.
//!
//! \sa CreateFSet(), Delete(FSetId const fSetId), 
//! IsValid(FSetId const fSetId) const, Get(FSetId const fSetId),
//! ReserveFSetS(uint64 const size), ExpandFSetS(uint64 const newSize).
inline Scene::FSetStorage const& Scene::FSetS() const
{
  return fSetS_;
}

//------------------------------------------------------------------------------
//! \brief Access the internal WeightingMatrix storage.
//! @return A \b const \b reference to the WeightingMatrix storage.
//!
//! \note Access to the internal storage is provided for convenience; direct
//! manipulation is not supported.
//!
//! \sa CreateWMat(), Delete(WMatId const wMatId), 
//! IsValid(WMatId const wMatId) const, Get(WMatId const wMatId),
//! ReserveWMatS(uint64 const size), ExpandWMatS(uint64 const newSize).
inline Scene::WMatStorage const& Scene::WMatS() const
{
  return wMatS_;
}

//------------------------------------------------------------------------------
//! \brief Access the internal FeaturePointTrack storage.
//! @return A \b const \b reference to the FeaturePointTrack storage.
//!
//! \note Access to the internal storage is provided for convenience; direct
//! manipulation is not supported.
//!
//! \sa CreateFTrk(), Delete(FTrkId const fTrkId), 
//! IsValid(FTrkId const fTrkId) const, Get(FTrkId const fTrkId),
//! ReserveFTrkS(uint64 const size), ExpandFTrkS(uint64 const newSize).
inline Scene::FTrkStorage const& Scene::FTrkS() const
{
  return fTrkS_;
}

//------------------------------------------------------------------------------
//! \brief Access the internal ProjectiveCameraRepresentation storage.
//! @return A \b const \b reference to the ProjectiveCameraRepresentation
//! storage.
//!
//! \note Access to the internal storage is provided for convenience; direct
//! manipulation is not supported.
//!
//! \sa CreatePRep(), Delete(PRepId const pRepId), 
//! IsValid(PRepId const pRepId) const, Get(PRepId const pRepId),
//! ReservePRepS(uint64 const size), ExpandPRepS(uint64 const newSize).
inline Scene::PRepStorage const& Scene::PRepS() const
{
  return pRepS_;
}

//------------------------------------------------------------------------------
//! \brief Access the internal CameraRepresentation storage.
//! @return A \b const \b reference to the CameraRepresentation storage.
//!
//! \note Access to the internal storage is provided for convenience; direct
//! manipulation is not supported.
//!
//! \sa CreateCRep(), Delete(CRepId const cRepId), 
//! IsValid(CRepId const cRepId) const, Get(CRepId const cRepId),
//! ReserveCRepS(uint64 const size), ExpandCRepS(uint64 const newSize).
inline Scene::CRepStorage const& Scene::CRepS() const
{
  return cRepS_;
}

//------------------------------------------------------------------------------
//! \brief Access the internal DistortionRepresentation storage.
//! @return A \b const \b reference to the DistortionRepresentation storage.
//!
//! \note Access to the internal storage is provided for convenience; direct
//! manipulation is not supported.
//!
//! \sa CreateDRep(), Delete(DRepId const dRepId), 
//! IsValid(DRepId const dRepId) const, Get(DRepId const dRepId),
//! ReserveDRepS(uint64 const size), ExpandDRepS(uint64 const newSize).
inline Scene::DRepStorage const& Scene::DRepS() const
{
  return dRepS_;
}

//------------------------------------------------------------------------------
//! \brief Access the internal ObjectPointRepresentation storage.
//! @return A \b const \b reference to the ObjectPointRepresentation storage.
//!
//! \note Access to the internal storage is provided for convenience; direct
//! manipulation is not supported.
//!
//! \sa CreateORep(), Delete(ORepId const oRepId), 
//! IsValid(ORepId const oRepId) const, Get(ORepId const oRepId),
//! ReserveORepS(uint64 const size), ExpandORepS(uint64 const newSize).
inline Scene::ORepStorage const& Scene::ORepS() const
{
  return oRepS_;
}

//------------------------------------------------------------------------------
//! \brief Access the internal TransformationSequence storage.
//! @return A \b const \b reference to the TransformationSequence storage.
//!
//! \note Access to the internal storage is provided for convenience; direct
//! manipulation is not supported.
//!
//! \sa CreateTSeq(), Delete(TSeqId const tSeqId), 
//! IsValid(TSeqId const tSeqId) const, Get(TSeqId const tSeqId),
//! ReserveTSeqS(uint64 const size), ExpandTSeqS(uint64 const newSize).
inline Scene::TSeqStorage const& Scene::TSeqS() const
{
  return tSeqS_;
}

//------------------------------------------------------------------------------
//! \brief Access the internal TransformationRepresenation storage.
//! @return A \b const \b reference to the TransformationRepresenation storage.
//!
//! \note Access to the internal storage is provided for convenience; direct
//! manipulation is not supported.
//!
//! \sa CreateTRep(), Delete(TRepId const tRepId), 
//! IsValid(TRepId const tRepId) const, Get(TRepId const tRepId),
//! ReserveTRepS(uint64 const size), ExpandTRepS(uint64 const newSize).
inline Scene::TRepStorage const& Scene::TRepS() const
{
  return tRepS_;
}

//------------------------------------------------------------------------------
//! \brief Access the FeaturePointSet \link autoDeleteFSet_ auto-deletion
//! flag\endlink.
//! @return The \b value of the FSet \link autoDeleteFSet_ auto-deletion
//! flag\endlink.
//!
//! This is the `const` version of `#AutoDeleteFSet()`.
inline bool Scene::AutoDeleteFSet() const
{
  return autoDeleteFSet_;
}

//------------------------------------------------------------------------------
//! \brief Access the FeaturePointSet \link autoDeleteFSet_ auto-deletion
//! flag\endlink.
//! @return A \b reference to the FSet \link autoDeleteFSet_ auto-deletion
//! flag\endlink.
//!
//! This is the non-`const` version of `#AutoDeleteFSet() const`.
inline bool& Scene::AutoDeleteFSet()
{
  return autoDeleteFSet_;
}

//------------------------------------------------------------------------------
//! \brief Access the WeightingMatrixRepresentation \link autoDeleteWMat_
//! auto-deletion flag\endlink.
//! @return The \b value of the WMat \link autoDeleteWMat_ auto-deletion
//! flag\endlink.
//!
//! This is the `const` version of `#AutoDeleteWMat()`.
inline bool Scene::AutoDeleteWMat() const
{
  return autoDeleteWMat_;
}

//------------------------------------------------------------------------------
//! \brief Access the WeightingMatrixRepresentation \link autoDeleteWMat_
//! auto-deletion flag\endlink.
//! @return A \b reference to the WMat \link autoDeleteWMat_ auto-deletion
//! flag\endlink.
//!
//! This is the non-`const` version of `#AutoDeleteWMat() const`.
inline bool& Scene::AutoDeleteWMat()
{
  return autoDeleteWMat_;
}

//------------------------------------------------------------------------------
//! \brief Access the FeaturePointTrack \link autoDeleteFTrk_ auto-deletion
//! flag\endlink.
//! @return The \b value of the FTrk \link autoDeleteFTrk_ auto-deletion
//! flag\endlink.
//!
//! This is the `const` version of `#AutoDeleteFTrk()`.
inline bool Scene::AutoDeleteFTrk() const
{
  return autoDeleteFTrk_;
}

//------------------------------------------------------------------------------
//! \brief Access the FeaturePointTrack \link autoDeleteFTrk_ auto-deletion
//! flag\endlink.
//! @return A \b reference to the FTrk \link autoDeleteFTrk_ auto-deletion
//! flag\endlink.
//!
//! This is the non-`const` version of `#AutoDeleteFTrk() const`.
inline bool& Scene::AutoDeleteFTrk()
{
  return autoDeleteFTrk_;
}

//------------------------------------------------------------------------------
//! \brief Access the ProjectiveCameraRepresentation \link autoDeletePRep_
//! auto-deletion flag\endlink.
//! @return The \b value of the PRep \link autoDeletePRep_ auto-deletion
//! flag\endlink.
//!
//! This is the `const` version of `#AutoDeletePRep()`.
inline bool Scene::AutoDeletePRep() const
{
  return autoDeletePRep_;
}

//------------------------------------------------------------------------------
//! \brief Access the ProjectiveCameraRepresentation \link autoDeletePRep_
//! auto-deletion flag\endlink.
//! @return A \b reference to the PRep \link autoDeletePRep_ auto-deletion
//! flag\endlink.
//!
//! This is the non-`const` version of `#AutoDeletePRep() const`.
inline bool& Scene::AutoDeletePRep()
{
  return autoDeletePRep_;
}

//------------------------------------------------------------------------------
//! \brief Access the CameraRepresentation \link autoDeleteCRep_ auto-deletion
//! flag\endlink.
//! @return The \b value of the CRep \link autoDeleteCRep_ auto-deletion
//! flag\endlink.
//!
//! This is the `const` version of `#AutoDeleteCRep()`.
inline bool Scene::AutoDeleteCRep() const
{
  return autoDeleteCRep_;
}

//------------------------------------------------------------------------------
//! \brief Access the CameraRepresentation \link autoDeleteCRep_ auto-deletion
//! flag\endlink.
//! @return A \b reference to the CRep \link autoDeleteCRep_ auto-deletion
//! flag\endlink.
//!
//! This is the non-`const` version of `#AutoDeleteCRep() const`.
inline bool& Scene::AutoDeleteCRep()
{
  return autoDeleteCRep_;
}

//------------------------------------------------------------------------------
//! \brief Access the DistortionRepresentation \link autoDeleteDRep_
//! auto-deletion flag\endlink.
//! @return The \b value of the DRep \link autoDeleteDRep_ auto-deletion
//! flag\endlink.
//!
//! This is the `const` version of `#AutoDeleteDRep()`.
inline bool Scene::AutoDeleteDRep() const
{
  return autoDeleteDRep_;
}

//------------------------------------------------------------------------------
//! \brief Access the DistortionRepresentation \link autoDeleteDRep_
//! auto-deletion flag\endlink.
//! @return A \b reference to the DRep \link autoDeleteDRep_ auto-deletion
//! flag\endlink.
//!
//! This is the non-`const` version of `#AutoDeleteDRep() const`.
inline bool& Scene::AutoDeleteDRep()
{
  return autoDeleteDRep_;
}

//------------------------------------------------------------------------------
//! \brief Access the ObjectPointRepresentation \link autoDeleteORep_
//! auto-deletion flag\endlink.
//! @return The \b value of the ORep \link autoDeleteORep_ auto-deletion
//! flag\endlink.
//!
//! This is the `const` version of `#AutoDeleteORep()`.
inline bool Scene::AutoDeleteORep() const
{
  return autoDeleteORep_;
}

//------------------------------------------------------------------------------
//! \brief Access the ObjectPointRepresentation \link autoDeleteORep_
//! auto-deletion flag\endlink.
//! @return A \b reference to the ORep \link autoDeleteORep_ auto-deletion
//! flag\endlink.
//!
//! This is the non-`const` version of `#AutoDeleteORep() const`.
inline bool& Scene::AutoDeleteORep()
{
  return autoDeleteORep_;
}

//------------------------------------------------------------------------------
//! \brief Access the TransformationSequence \link autoDeleteTSeq_ auto-deletion
//! flag\endlink.
//! @return The \b value of the TSeq \link autoDeleteTSeq_ auto-deletion
//! flag\endlink.
//!
//! This is the `const` version of `#AutoDeleteTSeq()`.
inline bool Scene::AutoDeleteTSeq() const
{
  return autoDeleteTSeq_;
}

//------------------------------------------------------------------------------
//! \brief Access the TransformationSequence \link autoDeleteTSeq_ auto-deletion
//! flag\endlink.
//! @return A \b reference to the TSeq \link autoDeleteTSeq_ auto-deletion
//! flag\endlink.
//!
//! This is the non-`const` version of `#AutoDeleteTSeq() const`.
inline bool& Scene::AutoDeleteTSeq()
{
  return autoDeleteTSeq_;
}

//------------------------------------------------------------------------------
//! \brief Access the TransformationRepresentation \link autoDeleteTRep_
//! auto-deletion flag\endlink.
//! @return The \b value of the TRep \link autoDeleteTRep_ auto-deletion
//! flag\endlink.
//!
//! This is the `const` version of `#AutoDeleteTRep()`.
inline bool Scene::AutoDeleteTRep() const
{
  return autoDeleteTRep_;
}

//------------------------------------------------------------------------------
//! \brief Access the TransformationRepresentation \link autoDeleteTRep_
//! auto-deletion flag\endlink.
//! @return A \b reference to the TRep \link autoDeleteTRep_ auto-deletion
//! flag\endlink.
//!
//! This is the non-`const` version of `#AutoDeleteTRep() const`.
inline bool& Scene::AutoDeleteTRep()
{
  return autoDeleteTRep_;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
//! \brief Returns the ImageRepresentation associated with the given Identifier.
//! @param iRepId The Identifier for the desired IRep.
//! @return A \b const \b reference to the desired IRep.
//!
//! If the Identifier is not in the valid range of the \link iRepS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! This is the `const` version of `#Get(IRepId const iRepId)`.
//!
//! \sa CreateIRep(), ExpandIRepS(uint64 newSize),
//! IsValid(IRepId const iRepId) const.
inline IRep const& Scene::Get(IRepId const iRepId) const
{
  return iRepS_.At(iRepId.Get());
}

//------------------------------------------------------------------------------
//! \brief Returns the ImageRepresentation associated with the given Identifier.
//! @param iRepId The Identifier for the desired IRep.
//! @return A \b reference to the desired IRep.
//!
//! If the Identifier is not in the valid range of the \link iRepS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! This is the non-`const` version of `#Get(IRepId const iRepId) const`.
//!
//! \sa CreateIRep(), ExpandIRepS(uint64 newSize),
//! IsValid(IRepId const iRepId) const.
inline IRep& Scene::Get(IRepId const iRepId)
{
  return iRepS_.At(iRepId.Get());
}

//------------------------------------------------------------------------------
//! \brief Returns the FeaturePointSet associated with the given Identifier.
//! @param fSetId The Identifier for the desired FSet.
//! @return A \b const \b reference to the desired FSet.
//!
//! If the Identifier is not in the valid range of the \link fSetS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! This is the `const` version of `#Get(FSetId const fSetId)`.
//!
//! \sa CreateFSet(), ExpandFSetS(uint64 newSize),
//! IsValid(FSetId const fSetId) const.
inline FSet const& Scene::Get(FSetId const fSetId) const
{
  return fSetS_.At(fSetId.Get());
}

//------------------------------------------------------------------------------
//! \brief Returns the FeaturePointSet associated with the given Identifier.
//! @param fSetId The Identifier for the desired FSet.
//! @return A \b reference to the desired FSet.
//!
//! If the Identifier is not in the valid range of the \link fSetS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! This is the non-`const` version of `#Get(FSetId const fSetId) const`.
//!
//! \sa CreateFSet(), ExpandFSetS(uint64 newSize),
//! IsValid(FSetId const fSetId) const.
inline FSet& Scene::Get(FSetId const fSetId)
{
  return fSetS_.At(fSetId.Get());
}

//------------------------------------------------------------------------------
//! \brief Returns the WeightingMatrixRepresentation associated with the given
//! Identifier.
//! @param wMatId The Identifier for the desired WMat.
//! @return A \b const \b reference to the desired WMat.
//!
//! If the Identifier is not in the valid range of the \link wMatS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! This is the `const` version of `#Get(WMatId const wMatId)`.
//!
//! \sa CreateWMat(), ExpandWMatS(uint64 newSize),
//! IsValid(WMatId const wMatId) const.
inline WMat const& Scene::Get(WMatId const wMatId) const
{
  return wMatS_.At(wMatId.Get());
}

//------------------------------------------------------------------------------
//! \brief Returns the WeightingMatrixRepresentation associated with the given
//! Identifier.
//! @param wMatId The Identifier for the desired WMat.
//! @return A \b reference to the desired WMat.
//!
//! If the Identifier is not in the valid range of the \link wMatS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! This is the non-`const` version of `#Get(WMatId const wMatId) const`.
//!
//! \sa CreateWMat(), ExpandWMatS(uint64 newSize),
//! IsValid(WMatId const wMatId) const.
inline WMat& Scene::Get(WMatId const wMatId)
{
  return wMatS_.At(wMatId.Get());
}

//------------------------------------------------------------------------------
//! \brief Returns the FeaturePointTrack associated with the given Identifier.
//! @param fTrkId The Identifier for the desired FTrk.
//! @return A \b const \b reference to the desired FTrk.
//!
//! If the Identifier is not in the valid range of the \link fTrkS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! This is the `const` version of `#Get(FTrkId const fTrkId)`.
//!
//! \sa CreateFTrk(), ExpandFTrkS(uint64 newSize),
//! IsValid(FTrkId const fTrkId) const.
inline FTrk const& Scene::Get(FTrkId const fTrkId) const
{
  return fTrkS_.At(fTrkId.Get());
}

//------------------------------------------------------------------------------
//! \brief Returns the FeaturePointTrack associated with the given Identifier.
//! @param fTrkId The Identifier for the desired FTrk.
//! @return A \b reference to the desired FTrk.
//!
//! If the Identifier is not in the valid range of the \link fTrkS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! This is the non-`const` version of `#Get(FTrkId const fTrkId) const`.
//!
//! \sa CreateFTrk(), ExpandFTrkS(uint64 newSize),
//! IsValid(FTrkId const fTrkId) const.
inline FTrk& Scene::Get(FTrkId const fTrkId)
{
  return fTrkS_.At(fTrkId.Get());
}

//------------------------------------------------------------------------------
//! \brief Returns the ProjectiveCameraRepresentation associated with the given
//! Identifier.
//! @param pRepId The Identifier for the desired PRep.
//! @return A \b const \b reference to the desired PRep.
//!
//! If the Identifier is not in the valid range of the \link pRepS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! This is the `const` version of `#Get(PRepId const pRepId)`.
//!
//! \sa CreatePRep(), ExpandPRepS(uint64 newSize),
//! IsValid(PRepId const pRepId) const.
inline PRep const& Scene::Get(PRepId const pRepId) const
{
  return pRepS_.At(pRepId.Get());
}

//------------------------------------------------------------------------------
//! \brief Returns the ProjectiveCameraRepresentation associated with the given
//! Identifier.
//! @param pRepId The Identifier for the desired PRep.
//! @return A \b reference to the desired PRep.
//!
//! If the Identifier is not in the valid range of the \link pRepS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! This is the non-`const` version of `#Get(PRepId const pRepId) const`.
//!
//! \sa CreatePRep(), ExpandPRepS(uint64 newSize),
//! IsValid(PRepId const pRepId) const.
inline PRep& Scene::Get(PRepId const pRepId)
{
  return pRepS_.At(pRepId.Get());
}

//------------------------------------------------------------------------------
//! \brief Returns the CameraRepresentation associated with the given
//! Identifier.
//! @param cRepId The Identifier for the desired CRep.
//! @return A \b const \b reference to the desired CRep.
//!
//! If the Identifier is not in the valid range of the \link cRepS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! This is the `const` version of `#Get(CRepId const cRepId)`.
//!
//! \sa CreateCRep(), ExpandCRepS(uint64 newSize),
//! IsValid(CRepId const cRepId) const.
inline CRep const& Scene::Get(CRepId const cRepId) const
{
  return cRepS_.At(cRepId.Get());
}

//------------------------------------------------------------------------------
//! \brief Returns the CameraRepresentation associated with the given
//! Identifier.
//! @param cRepId The Identifier for the desired CRep.
//! @return A \b reference to the desired CRep.
//!
//! If the Identifier is not in the valid range of the \link cRepS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! This is the non-`const` version of `#Get(CRepId const cRepId) const`.
//!
//! \sa CreateCRep(), ExpandCRepS(uint64 newSize),
//! IsValid(CRepId const cRepId) const.
inline CRep& Scene::Get(CRepId const cRepId)
{
  return cRepS_.At(cRepId.Get());
}

//------------------------------------------------------------------------------
//! \brief Returns the DistortionRepresentation associated with the given
//! Identifier.
//! @param dRepId The Identifier for the desired DRep.
//! @return A \b const \b reference to the desired DRep.
//!
//! If the Identifier is not in the valid range of the \link dRepS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! This is the `const` version of `#Get(DRepId const dRepId)`.
//!
//! \sa CreateDRep(), ExpandDRepS(uint64 newSize),
//! IsValid(DRepId const dRepId) const.
inline DRep const& Scene::Get(DRepId const dRepId) const
{
  return dRepS_.At(dRepId.Get());
}

//------------------------------------------------------------------------------
//! \brief Returns the DistortionRepresentation associated with the given
//! Identifier.
//! @param dRepId The Identifier for the desired DRep.
//! @return A \b reference to the desired DRep.
//!
//! If the Identifier is not in the valid range of the \link dRepS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! This is the non-`const` version of `#Get(DRepId const dRepId) const`.
//!
//! \sa CreateDRep(), ExpandDRepS(uint64 newSize),
//! IsValid(DRepId const dRepId) const.
inline DRep& Scene::Get(DRepId const dRepId)
{
  return dRepS_.At(dRepId.Get());
}

//------------------------------------------------------------------------------
//! \brief Returns the ObjectPointRepresentation associated with the given
//! Identifier.
//! @param oRepId The Identifier for the desired ORep.
//! @return A \b const \b reference to the desired ORep.
//!
//! If the Identifier is not in the valid range of the \link oRepS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! This is the `const` version of `#Get(ORepId const oRepId)`.
//!
//! \sa CreateORep(), ExpandORepS(uint64 newSize),
//! IsValid(ORepId const oRepId) const.
inline ORep const& Scene::Get(ORepId const oRepId) const
{
  return oRepS_.At(oRepId.Get());
}

//------------------------------------------------------------------------------
//! \brief Returns the ObjectPointRepresentation associated with the given
//! Identifier.
//! @param oRepId The Identifier for the desired ORep.
//! @return A \b reference to the desired ORep.
//!
//! If the Identifier is not in the valid range of the \link oRepS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! This is the non-`const` version of `#Get(ORepId const oRepId) const`.
//!
//! \sa CreateORep(), ExpandORepS(uint64 newSize),
//! IsValid(ORepId const oRepId) const.
inline ORep& Scene::Get(ORepId const oRepId)
{
  return oRepS_.At(oRepId.Get());
}

//------------------------------------------------------------------------------
//! \brief Returns the TransformationSequence associated with the given
//! Identifier.
//! @param tSeqId The Identifier for the desired TSeq.
//! @return A \b const \b reference to the desired TSeq.
//!
//! If the Identifier is not in the valid range of the \link tSeqS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! This is the `const` version of `#Get(TSeqId const tSeqId)`.
//!
//! \sa CreateTSeq(), ExpandTSeqS(uint64 newSize),
//! IsValid(TSeqId const tSeqId) const.
inline TSeq const& Scene::Get(TSeqId const tSeqId) const
{
  return tSeqS_.At(tSeqId.Get());
}

//------------------------------------------------------------------------------
//! \brief Returns the TransformationSequence associated with the given
//! Identifier.
//! @param tSeqId The Identifier for the desired TSeq.
//! @return A \b reference to the desired TSeq.
//!
//! If the Identifier is not in the valid range of the \link tSeqS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! This is the non-`const` version of `#Get(TSeqId const tSeqId) const`.
//!
//! \sa CreateTSeq(), ExpandTSeqS(uint64 newSize),
//! IsValid(TSeqId const tSeqId) const.
inline TSeq& Scene::Get(TSeqId const tSeqId)
{
  return tSeqS_.At(tSeqId.Get());
}

//------------------------------------------------------------------------------
//! \brief Returns the TransformationRepresentation associated with the given
//! Identifier.
//! @param tRepId The Identifier for the desired TRep.
//! @return A \b const \b reference to the desired TRep.
//!
//! If the Identifier is not in the valid range of the \link tRepS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! This is the `const` version of `#Get(TRepId const tRepId)`.
//!
//! \sa CreateTRep(), ExpandTRepS(uint64 newSize),
//! IsValid(TRepId const tRepId) const.
inline TRep const& Scene::Get(TRepId const tRepId) const
{
  return tRepS_.At(tRepId.Get());
}

//------------------------------------------------------------------------------
//! \brief Returns the TransformationRepresentation associated with the given
//! Identifier.
//! @param tRepId The Identifier for the desired TRep.
//! @return A \b reference to the desired TRep.
//!
//! If the Identifier is not in the valid range of the \link tRepS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! This is the non-`const` version of `#Get(TRepId const tRepId) const`.
//!
//! \sa CreateTRep(), ExpandTRepS(uint64 newSize),
//! IsValid(TRepId const tRepId) const.
inline TRep& Scene::Get(TRepId const tRepId)
{
  return tRepS_.At(tRepId.Get());
}

//------------------------------------------------------------------------------
//! \brief Returns the FeaturePointRepresentation associated with the given
//! Identifier for the indicated FeaturePointSet.
//! @param fSetId The Identifier for the desired FSet.
//! @param fRepId The Identifier for the desired FRep.
//! @return A \b const \b reference to the desired FRep.
//!
//! If either Identifier is not in the valid range of the respective container,
//! an OutOfRange exception will be thrown.
//!
//! If either Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! This is the `const` version of
//! `#Get(FSetId const fSetId, FRepId const fRepId)`.
//!
//! \sa CreateFRep(FSetId const fSetId),
//! ExpandFSet(FSetId const fSetId, uint64 newSize),
//! IsValid(FSetId const fSetId, FRepId const fRepId) const.
inline FRep const& Scene::Get(
  FSetId const fSetId,
  FRepId const fRepId) const
{
  return Get(fSetId).At(fRepId.Get());
}

//------------------------------------------------------------------------------
//! \brief Returns the FeaturePointRepresentation associated with the given
//! Identifier for the indicated FeaturePointSet.
//! @param fSetId The Identifier for the desired FSet.
//! @param fRepId The Identifier for the desired FRep.
//! @return A \b reference to the desired FRep.
//!
//! If either Identifier is not in the valid range of the respective container,
//! an OutOfRange exception will be thrown.
//!
//! If either Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! This is the non-`const` version of
//! `#Get(FSetId const fSetId, FRepId const fRepId) const`.
//!
//! \sa CreateFRep(FSetId const fSetId),
//! ExpandFSet(FSetId const fSetId, uint64 newSize),
//! IsValid(FSetId const fSetId, FRepId const fRepId) const.
inline FRep& Scene::Get(FSetId const fSetId, FRepId const fRepId)
{
  return Get(fSetId).At(fRepId.Get());
}

//------------------------------------------------------------------------------
//! \brief Validate the ImageRepresentation Identifier.
//! @param iRepId The IRepId to be validated.
//! @return `true` if the IRepId is valid, `false` otherwise.
//!
//! \note An Identifier is valid only if it is in the current range of the
//! container and the corresponding element is active.
//!
//! \sa CreateIRep(), ArrayObjectTable<T>::Range() const,
//! ArrayObjectTable<T>::Active(uint64 const i) const.
inline bool Scene::IsValid(IRepId const iRepId) const
{
  return (iRepId.Get() < iRepS_.Range()) && iRepS_.Active(iRepId.Get());
}

//------------------------------------------------------------------------------
//! \brief Validate the FeaturePointSet Identifier.
//! @param fSetId The FSetId to be validated.
//! @return `true` if the FSetId is valid, `false` otherwise.
//!
//! \note An Identifier is valid only if it is in the current range of the
//! container and the corresponding element is active.
//!
//! \sa CreateTRep(), ArrayObjectTable<T>::Range() const,
//! ArrayObjectTable<T>::Active(uint64 const i) const.
inline bool Scene::IsValid(FSetId const fSetId) const
{
  return (fSetId.Get() < fSetS_.Range()) && fSetS_.Active(fSetId.Get());
}

//------------------------------------------------------------------------------
//! \brief Validate the WeightingMatrix Identifier.
//! @param wMatId The WMatId to be validated.
//! @return `true` if the WMatId is valid, `false` otherwise.
//!
//! \note An Identifier is valid only if it is in the current range of the
//! container and the corresponding element is active.
//!
//! \sa CreateWMat(), ArrayObjectTable<T>::Range() const,
//! ArrayObjectTable<T>::Active(uint64 const i) const.
inline bool Scene::IsValid(WMatId const wMatId) const
{
  return (wMatId.Get() < wMatS_.Range()) && wMatS_.Active(wMatId.Get());
}

//------------------------------------------------------------------------------
//! \brief Validate the FeaturePointTrack Identifier.
//! @param fTrkId The FTrkId to be validated.
//! @return `true` if the FTrkId is valid, `false` otherwise.
//!
//! \note An Identifier is valid only if it is in the current range of the
//! container and the corresponding element is active.
//!
//! \sa CreateTRep(), ArrayObjectTable<T>::Range() const,
//! ArrayObjectTable<T>::Active(uint64 const i) const.
inline bool Scene::IsValid(FTrkId const fTrkId) const
{
  return (fTrkId.Get() < fTrkS_.Range()) && fTrkS_.Active(fTrkId.Get());
}

//------------------------------------------------------------------------------
//! \brief Validate the ProjectiveCameraRepresentation Identifier.
//! @param pRepId The PRepId to be validated.
//! @return `true` if the PRepId is valid, `false` otherwise.
//!
//! \note An Identifier is valid only if it is in the current range of the
//! container and the corresponding element is active.
//!
//! \sa CreatePRep(), ArrayObjectTable<T>::Range() const,
//! ArrayObjectTable<T>::Active(uint64 const i) const.
inline bool Scene::IsValid(PRepId const pRepId) const
{
  return (pRepId.Get() < pRepS_.Range()) && pRepS_.Active(pRepId.Get());
}

//------------------------------------------------------------------------------
//! \brief Validate the CameraRepresentation Identifier.
//! @param cRepId The CRepId to be validated.
//! @return `true` if the CRepId is valid, `false` otherwise.
//!
//! \note An Identifier is valid only if it is in the current range of the
//! container and the corresponding element is active.
//!
//! \sa CreateCRep(), ArrayObjectTable<T>::Range() const,
//! ArrayObjectTable<T>::Active(uint64 const i) const.
inline bool Scene::IsValid(CRepId const cRepId) const
{
  return (cRepId.Get() < cRepS_.Range()) && cRepS_.Active(cRepId.Get());
}

//------------------------------------------------------------------------------
//! \brief Validate the DistortionRepresentation Identifier.
//! @param dRepId The DRepId to be validated.
//! @return `true` if the DRepId is valid, `false` otherwise.
//!
//! \note An Identifier is valid only if it is in the current range of the
//! container and the corresponding element is active.
//!
//! \sa CreateDRep(), ArrayObjectTable<T>::Range() const,
//! ArrayObjectTable<T>::Active(uint64 const i) const.
inline bool Scene::IsValid(DRepId const dRepId) const
{
  return (dRepId.Get() < dRepS_.Range()) && dRepS_.Active(dRepId.Get());
}

//------------------------------------------------------------------------------
//! \brief Validate the ObjectPointRepresentation Identifier.
//! @param oRepId The ORepId to be validated.
//! @return `true` if the ORepId is valid, `false` otherwise.
//!
//! \note An Identifier is valid only if it is in the current range of the
//! container and the corresponding element is active.
//!
//! \sa CreateORep(), ArrayObjectTable<T>::Range() const,
//! ArrayObjectTable<T>::Active(uint64 const i) const.
inline bool Scene::IsValid(ORepId const oRepId) const
{
  return (oRepId.Get() < oRepS_.Range()) && oRepS_.Active(oRepId.Get());
}

//------------------------------------------------------------------------------
//! \brief Validate the TransformationSequence Identifier.
//! @param tSeqId The TSeqId to be validated.
//! @return `true` if the TSeqId is valid,
//! `false` otherwise.
//!
//! \note An Identifier is valid only if it is in the current range of the
//! container and the corresponding element is active.
//!
//! \sa CreateTSeq(), ArrayObjectTable<T>::Range() const,
//! ArrayObjectTable<T>::Active(uint64 const i) const.
inline bool Scene::IsValid(TSeqId const tSeqId) const
{
  return (tSeqId.Get() < tSeqS_.Range()) && tSeqS_.Active(tSeqId.Get());
}

//------------------------------------------------------------------------------
//! \brief Validate the TransformationRepresentation Identifier.
//! @param tRepId The TRepId to be validated.
//! @return `true` if the TRepId is valid, `false` otherwise.
//!
//! \note An Identifier is valid only if it is in the current range of the
//! container and the corresponding element is active.
//!
//! \sa CreateTRep(), ArrayObjectTable<T>::Range() const,
//! ArrayObjectTable<T>::Active(uint64 const i) const.
inline bool Scene::IsValid(TRepId const tRepId) const
{
  return (tRepId.Get() < tRepS_.Range()) && tRepS_.Active(tRepId.Get());
}

//------------------------------------------------------------------------------
//! \brief Validate the FeaturePointSet Identifier and the 
//! FeaturePointRepresentation Identifier.
//! @param fSetId The FSetId to be validated.
//! @param fRepId The FRepId to be validated.
//! @return `true` if both the FSetId and the FRepId are valid, `false`
//! otherwise.
//!
//! \note An Identifier is valid only if it is in the current range of the
//! container and the corresponding element is active.
//!
//! \sa CreateFSet(), CreateFRep(FSetId const fSetId),
//! ArrayObjectTable<T>::Range() const,
//! ArrayObjectTable<T>::Active(uint64 const i) const.
inline bool Scene::IsValid(FSetId const fSetId, FRepId const fRepId) const
{
  if ((fSetId.Get() < fSetS_.Range()) && fSetS_.Active(fSetId.Get()))
  {
    FSet const& fSet = Get(fSetId);
    
    return (fRepId.Get() < fSet.Range()) && fSet.Active(fRepId.Get());
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return false;
}

//------------------------------------------------------------------------------
//! \brief Reserves space in the \link iRepS_ container\endlink.
//! @param size The desired storage capacity of the \link iRepS_
//! container\endlink.
//!
//! If the requested capacity is less than or equal to the current capacity of
//! the \link iRepS_ container\endlink, this function has no effect.
//!
//! \sa IRepS(), ArrayObjectTable<T>::Reserve().
inline void Scene::ReserveIRepS(uint64 const size)
{
  iRepS_.Reserve(size);
}

//------------------------------------------------------------------------------
//! \brief Reserves space in the \link fSetS_ container\endlink.
//! @param size The desired storage capacity of the \link fSetS_
//! container\endlink.
//!
//! If the requested capacity is less than or equal to the current capacity of
//! the \link fSetS_ container\endlink, this function has no effect.
//!
//! \sa FSetS(), ArrayObjectTable<T>::Reserve().
inline void Scene::ReserveFSetS(uint64 const size)
{
  fSetS_.Reserve(size);
}

//------------------------------------------------------------------------------
//! \brief Reserves space in the \link wMatS_ container\endlink.
//! @param size The desired storage capacity of the \link wMatS_
//! container\endlink.
//!
//! If the requested capacity is less than or equal to the current capacity of
//! the \link wMatS_ container\endlink, this function has no effect.
//!
//! \sa WMatS(), ArrayObjectTable<T>::Reserve().
inline void Scene::ReserveWMatS(uint64 const size)
{
  wMatS_.Reserve(size);
}

//------------------------------------------------------------------------------
//! \brief Reserves space in the \link fTrkS_ container\endlink.
//! @param size The desired storage capacity of the \link fTrkS_
//! container\endlink.
//!
//! If the requested capacity is less than or equal to the current capacity of
//! the \link fTrkS_ container\endlink, this function has no effect.
//!
//! \sa FTrkS(), ArrayObjectTable<T>::Reserve().
inline void Scene::ReserveFTrkS(uint64 const size)
{
  fTrkS_.Reserve(size);
}

//------------------------------------------------------------------------------
//! \brief Reserves space in the \link pRepS_ container\endlink.
//! @param size The desired storage capacity of the \link pRepS_
//! container\endlink.
//!
//! If the requested capacity is less than or equal to the current capacity of
//! the \link pRepS_ container\endlink, this function has no effect.
//!
//! \sa PRepS(), ArrayObjectTable<T>::Reserve().
inline void Scene::ReservePRepS(uint64 const size)
{
  pRepS_.Reserve(size);
}

//------------------------------------------------------------------------------
//! \brief Reserves space in the \link cRepS_ container\endlink.
//! @param size The desired storage capacity of the \link cRepS_
//! container\endlink.
//!
//! If the requested capacity is less than or equal to the current capacity of
//! the \link cRepS_ container\endlink, this function has no effect.
//!
//! \sa CRepS(), ArrayObjectTable<T>::Reserve().
inline void Scene::ReserveCRepS(uint64 const size)
{
  cRepS_.Reserve(size);
}

//------------------------------------------------------------------------------
//! \brief Reserves space in the \link dRepS_ container\endlink.
//! @param size The desired storage capacity of the \link dRepS_
//! container\endlink.
//!
//! If the requested capacity is less than or equal to the current capacity of
//! the \link dRepS_ container\endlink, this function has no effect.
//!
//! \sa DRepS(), ArrayObjectTable<T>::Reserve().
inline void Scene::ReserveDRepS(uint64 const size)
{
  dRepS_.Reserve(size);
}

//------------------------------------------------------------------------------
//! \brief Reserves space in the \link oRepS_ container\endlink.
//! @param size The desired storage capacity of the \link oRepS_
//! container\endlink.
//!
//! If the requested capacity is less than or equal to the current capacity of
//! the \link oRepS_ container\endlink, this function has no effect.
//!
//! \sa ORepS(), ArrayObjectTable<T>::Reserve().
inline void Scene::ReserveORepS(uint64 const size)
{
  oRepS_.Reserve(size);
}

//------------------------------------------------------------------------------
//! \brief Reserves space in the \link tSeqS_ container\endlink.
//! @param size The desired storage capacity of the \link tSeqS_
//! container\endlink.
//!
//! If the requested capacity is less than or equal to the current capacity of
//! the \link tSeqS_ container\endlink, this function has no effect.
//!
//! \sa TSeqS(), ArrayObjectTable<T>::Reserve().
inline void Scene::ReserveTSeqS(uint64 const size)
{
  tSeqS_.Reserve(size);
}

//------------------------------------------------------------------------------
//! \brief Reserves space in the \link tRepS_ container\endlink.
//! @param size The desired storage capacity of the \link tRepS_
//! container\endlink.
//!
//! If the requested capacity is less than or equal to the current capacity of
//! the \link tRepS_ container\endlink, this function has no effect.
//!
//! \sa TRepS(), ArrayObjectTable<T>::Reserve().
inline void Scene::ReserveTRepS(uint64 const size)
{
  tRepS_.Reserve(size);
}

//------------------------------------------------------------------------------
//! \brief Reserves space in the FeaturePointSet.
//! @param fSetId The Identifier of the FeaturePointSet for which storage
//! storage capacity shall be reserved.
//! @param size The desired storage capacity of the FeaturePointSet.
//!
//! If the requested capacity is less than or equal to the current capacity of
//! the FeaturePointSet, this function has no effect.
//!
//! \sa Get(FSetId const fSetId), ArrayObjectTable<T>::Reserve().
inline void Scene::ReserveFSet(FSetId const fSetId, uint64 const size)
{
  Get(fSetId).Reserve(size);
}

//------------------------------------------------------------------------------
//! \brief Resizes the \link iRepS_ container\endlink to accomodate the
//! requested size.
//! @param newSize The desired size of the \link iRepS_ container\endlink.
//!
//! If the \link iRepS_ container\endlink is not compact, a RuntimeError
//! exception is thrown.
//!
//! If the requested size is less than or equal to the size of the \link iRepS_
//! container\endlink, this function has no effect.
//!
//! After this function has succeeded, the valid Identifier range for the 
//! \link iRepS_ container\endlink is \f$[0,newSize)\f$.
//!
//! \sa IRepS(), CompactIRepS(), ArrayObjectTable<T>::IsCompact().
inline void Scene::ExpandIRepS(uint64 const newSize)
{
  iRepS_.Expand(newSize);
}

//------------------------------------------------------------------------------
//! \brief Resizes the \link fSetS_ container\endlink to accomodate the
//! requested size.
//! @param newSize The desired size of the \link fSetS_ container\endlink.
//!
//! If the \link fSetS_ container\endlink is not compact, a RuntimeError
//! exception is thrown.
//!
//! If the requested size is less than or equal to the size of the \link fSetS_
//! container\endlink, this function has no effect.
//!
//! After this function has succeeded, the valid Identifier range for the 
//! \link fSetS_ container\endlink is \f$[0,newSize)\f$.
//!
//! \sa FSetS(), CompactFSetS(), ArrayObjectTable<T>::IsCompact().
inline void Scene::ExpandFSetS(uint64 const newSize)
{
  fSetS_.Expand(newSize);
}

//------------------------------------------------------------------------------
//! \brief Resizes the \link wMatS_ container\endlink to accomodate the
//! requested size.
//! @param newSize The desired size of the \link wMatS_ container\endlink.
//!
//! If the \link wMatS_ container\endlink is not compact, a RuntimeError
//! exception is thrown.
//!
//! If the requested size is less than or equal to the size of the \link wMatS_
//! container\endlink, this function has no effect.
//!
//! After this function has succeeded, the valid Identifier range for the 
//! \link wMatS_ container\endlink is \f$[0,newSize)\f$.
//!
//! \sa WMatS(), CompactWMatS(), ArrayObjectTable<T>::IsCompact().
inline void Scene::ExpandWMatS(uint64 const newSize)
{
  wMatS_.Expand(newSize);
}

//------------------------------------------------------------------------------
//! \brief Resizes the \link fTrkS_ container\endlink to accomodate the
//! requested size.
//! @param newSize The desired size of the \link fTrkS_ container\endlink.
//!
//! If the \link fTrkS_ container\endlink is not compact, a RuntimeError
//! exception is thrown.
//!
//! If the requested size is less than or equal to the size of the \link fTrkS_
//! container\endlink, this function has no effect.
//!
//! After this function has succeeded, the valid Identifier range for the 
//! \link fTrkS_ container\endlink is \f$[0,newSize)\f$.
//!
//! \sa FTrkS(), CompactFTrkS(), ArrayObjectTable<T>::IsCompact().
inline void Scene::ExpandFTrkS(uint64 const newSize)
{
  fTrkS_.Expand(newSize);
}

//------------------------------------------------------------------------------
//! \brief Resizes the \link pRepS_ container\endlink to accomodate the
//! requested size.
//! @param newSize The desired size of the \link pRepS_ container\endlink.
//!
//! If the \link pRepS_ container\endlink is not compact, a RuntimeError
//! exception is thrown.
//!
//! If the requested size is less than or equal to the size of the \link pRepS_
//! container\endlink, this function has no effect.
//!
//! After this function has succeeded, the valid Identifier range for the 
//! \link pRepS_ container\endlink is \f$[0,newSize)\f$.
//!
//! \sa PRepS(), CompactPRepS(), ArrayObjectTable<T>::IsCompact(),
//! ArrayObjectTable<T>::Expand(uint64 const n).
inline void Scene::ExpandPRepS(uint64 const newSize)
{
  pRepS_.Expand(newSize);
}

//------------------------------------------------------------------------------
//! \brief Resizes the \link cRepS_ container\endlink to accomodate the
//! requested size.
//! @param newSize The desired size of the \link cRepS_ container\endlink.
//!
//! If the \link cRepS_ container\endlink is not compact, a RuntimeError
//! exception is thrown.
//!
//! If the requested size is less than or equal to the size of the \link cRepS_
//! container\endlink, this function has no effect.
//!
//! After this function has succeeded, the valid Identifier range for the 
//! \link cRepS_ container\endlink is \f$[0,newSize)\f$.
//!
//! \sa CRepS(), CompactCRepS(), ArrayObjectTable<T>::IsCompact(),
//! ArrayObjectTable<T>::Expand(uint64 const n).
inline void Scene::ExpandCRepS(uint64 const newSize)
{
  cRepS_.Expand(newSize);
}

//------------------------------------------------------------------------------
//! \brief Resizes the \link dRepS_ container\endlink to accomodate the
//! requested size.
//! @param newSize The desired size of the \link dRepS_ container\endlink.
//!
//! If the \link dRepS_ container\endlink is not compact, a RuntimeError
//! exception is thrown.
//!
//! If the requested size is less than or equal to the size of the \link dRepS_
//! container\endlink, this function has no effect.
//!
//! After this function has succeeded, the valid Identifier range for the 
//! \link dRepS_ container\endlink is \f$[0,newSize)\f$.
//!
//! \sa DRepS(), CompactDRepS(), ArrayObjectTable<T>::IsCompact(),
//! ArrayObjectTable<T>::Expand(uint64 const n).
inline void Scene::ExpandDRepS(uint64 const newSize)
{
  dRepS_.Expand(newSize);
}

//------------------------------------------------------------------------------
//! \brief Resizes the \link oRepS_ container\endlink to accomodate the
//! requested size.
//! @param newSize The desired size of the \link oRepS_ container\endlink.
//!
//! If the \link oRepS_ container\endlink is not compact, a RuntimeError
//! exception is thrown.
//!
//! If the requested size is less than or equal to the size of the \link oRepS_
//! container\endlink, this function has no effect.
//!
//! After this function has succeeded, the valid Identifier range for the 
//! \link oRepS_ container\endlink is \f$[0,newSize)\f$.
//!
//! \sa ORepS(), CompactORepS(), ArrayObjectTable<T>::IsCompact(),
//! ArrayObjectTable<T>::Expand(uint64 const n).
inline void Scene::ExpandORepS(uint64 const newSize)
{
  oRepS_.Expand(newSize);
}

//------------------------------------------------------------------------------
//! \brief Resizes the \link tSeqS_ container\endlink to accomodate the
//! requested size.
//! @param newSize The desired size of the \link tSeqS_ container\endlink.
//!
//! If the \link tSeqS_ container\endlink is not compact, a RuntimeError
//! exception is thrown.
//!
//! If the requested size is less than or equal to the size of the \link tSeqS_
//! container\endlink, this function has no effect.
//!
//! After this function has succeeded, the valid Identifier range for the 
//! \link tSeqS_ container\endlink is \f$[0,newSize)\f$.
//!
//! \sa TSeqS(), CompactTSeqS(), ArrayObjectTable<T>::IsCompact(),
//! ArrayObjectTable<T>::Expand(uint64 const n).
inline void Scene::ExpandTSeqS(uint64 const newSize)
{
  tSeqS_.Expand(newSize);
}

//------------------------------------------------------------------------------
//! \brief Resizes the \link tRepS_ container\endlink to accomodate the
//! requested size.
//! @param newSize The desired size of the \link tRepS_ container\endlink.
//!
//! If the \link tRepS_ container\endlink is not compact, a RuntimeError
//! exception is thrown.
//!
//! If the requested size is less than or equal to the size of the \link tRepS_
//! container\endlink, this function has no effect.
//!
//! After this function has succeeded, the valid Identifier range for the 
//! \link tRepS_ container\endlink is \f$[0,newSize)\f$.
//!
//! \sa TRepS(), CompactTRepS(), ArrayObjectTable<T>::IsCompact(),
//! ArrayObjectTable<T>::Expand(uint64 const n).
inline void Scene::ExpandTRepS(uint64 const newSize)
{
  tRepS_.Expand(newSize);
}

//------------------------------------------------------------------------------
//! \brief Resizes the \link iRepS_ container\endlink to accomodate the
//! requested size.
//! @param fSetId The Identifier of the FeaturePointSet to be resized.
//! @param newSize The desired size of the FeaturePointSet.
//!
//! If the FeaturePointSet is not compact, a RuntimeError exception is thrown.
//!
//! If the requested size is less than or equal to the size of the
//! FeaturePointSet, this function has no effect.
//!
//! After this function has succeeded, the valid Identifier range for the 
//! FeaturePointSet is \f$[0,newSize)\f$.
//!
//! \sa Get(FSetId const fSetId), CompactFSet(FSetId const fSetId),
//! ArrayObjectTable<T>::IsCompact(),
//! ArrayObjectTable<T>::Expand(uint64 const n).
inline void Scene::ExpandFSet(FSetId const fSetId, uint64 const newSize)
{
  Get(fSetId).Expand(newSize);
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
