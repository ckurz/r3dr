//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2014  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_scene_WeightingMatrixRepresentation_h_
#define dlm_scene_WeightingMatrixRepresentation_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/scene/ReferencePairSet.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/Identifiers.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/entity/WeightingMatrix.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/ArrayObjectTable.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// CovarianceMatrixRepresentation class
//------------------------------------------------------------------------------
class WeightingMatrixRepresentation : public WeightingMatrix
                                    , public ReferencePairSet<FSet, FRep>
{
public:
  //----------------------------------------------------------------------------
  // Friends
  //----------------------------------------------------------------------------
  friend class BinarySerializer;   //!< \brief For serialization.
  friend class BinaryDeserializer; //!< \brief For deserialization.
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  friend class Scene; //!< \brief The Scene is allowed to change the state.
  friend class ArrayObjectTable<WeightingMatrixRepresentation>; //!< For construction.

  //----------------------------------------------------------------------------
  // Nested
  //----------------------------------------------------------------------------
  typedef WeightingMatrix BaseClass;

//  using BaseClass::operator();

  typedef std::set<std::pair<FSetId, FRepId>> FSetFRepPairSet;

  //----------------------------------------------------------------------------
  // Operators
  //----------------------------------------------------------------------------
  WeightingMatrixRepresentation& operator=(BaseClass const& matrix);

  //----------------------------------------------------------------------------
  // Convenience functions
  //----------------------------------------------------------------------------
  //! @name Convenience functions
  //!
  //! @{
  FSetFRepPairSet const& FSetFRepPairS() const;
  //! @}

private:
  //----------------------------------------------------------------------------
  // Constructors
  //----------------------------------------------------------------------------
  WeightingMatrixRepresentation(BaseClass const& matrix = BaseClass());
  WeightingMatrixRepresentation(WeightingMatrixRepresentation const& other);

  //----------------------------------------------------------------------------
  // Operators
  //----------------------------------------------------------------------------
  WeightingMatrixRepresentation& operator=(
    WeightingMatrixRepresentation const& other);
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
