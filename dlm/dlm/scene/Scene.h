//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_scene_Scene_h_
#define dlm_scene_Scene_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/scene/ImageRepresentation.h"
#include "dlm/scene/FeaturePointSet.h"
#include "dlm/scene/FeaturePointRepresentation.h"
#include "dlm/scene/WeightingMatrixRepresentation.h"
#include "dlm/scene/FeaturePointTrack.h"
#include "dlm/scene/ProjectiveCameraRepresentation.h"
#include "dlm/scene/CameraRepresentation.h"
#include "dlm/scene/DistortionRepresentation.h"
#include "dlm/scene/ObjectPointRepresentation.h"
#include "dlm/scene/TransformationSequence.h"
#include "dlm/scene/TransformationRepresentation.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/Identifiers.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/ArrayObjectTable.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Types.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// In addition to the files listed here, this file includes one or more inline
// header files for convenience. These includes can be found below in the
// section 'Inline code', after the main body of the document.
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
//! \brief The main class that manages all entities.
//!
//! 
//!
// Scene class
//------------------------------------------------------------------------------
class Scene
{
public:
  //----------------------------------------------------------------------------
  // Friends
  //----------------------------------------------------------------------------
  friend class BinarySerializer;   //!< For serialization.
  friend class BinaryDeserializer; //!< For deserialization.

  //----------------------------------------------------------------------------
  // Nested
  //----------------------------------------------------------------------------
  //! \brief ImageRepresentation storage type.
  typedef ArrayObjectTable<IRep> IRepStorage;
  //! \brief FeaturePointSet storage type.
  typedef ArrayObjectTable<FSet> FSetStorage;
  //! \brief WeightingMatrix storage type.
  typedef ArrayObjectTable<WMat> WMatStorage; 
  //! \brief FeatureTrack storage type.
  typedef ArrayObjectTable<FTrk> FTrkStorage;
  //! \brief ProjectiveCameraRepresentation storage type.
  typedef ArrayObjectTable<PRep> PRepStorage;
  //! \brief CameraRepresentation storage type.
  typedef ArrayObjectTable<CRep> CRepStorage;
  //! \brief DistortionRepresentation storage type.
  typedef ArrayObjectTable<DRep> DRepStorage;
  //! \brief ObjectPointRepresentation storage type.
  typedef ArrayObjectTable<ORep> ORepStorage;
  //! \brief TransformationSequence storage type.
  typedef ArrayObjectTable<TSeq> TSeqStorage;
  //! \brief TransformationRepresentation storage type.
  typedef ArrayObjectTable<TRep> TRepStorage;

  //----------------------------------------------------------------------------
  // Constructors
  //----------------------------------------------------------------------------
  Scene(bool autoDelete = true);

  //----------------------------------------------------------------------------
  // Accessors/Mutators
  //----------------------------------------------------------------------------
  //! @name Storage access
  //!
  //! @{
  IRepStorage const& IRepS() const;
  FSetStorage const& FSetS() const;
  WMatStorage const& WMatS() const;
  FTrkStorage const& FTrkS() const;
  PRepStorage const& PRepS() const;
  CRepStorage const& CRepS() const;
  DRepStorage const& DRepS() const;
  ORepStorage const& ORepS() const;
  TSeqStorage const& TSeqS() const;
  TRepStorage const& TRepS() const;
  //! @}
 
  //----------------------------------------------------------------------------
  //! @name AutoDelete settings
  //!
  //! @{
  bool  AutoDeleteFSet() const;
  bool& AutoDeleteFSet();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool  AutoDeleteWMat() const;
  bool& AutoDeleteWMat();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool  AutoDeleteFTrk() const;
  bool& AutoDeleteFTrk();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool  AutoDeletePRep() const;
  bool& AutoDeletePRep();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool  AutoDeleteCRep() const;
  bool& AutoDeleteCRep();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool  AutoDeleteDRep() const;
  bool& AutoDeleteDRep();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool  AutoDeleteORep() const;
  bool& AutoDeleteORep();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool  AutoDeleteTSeq() const;
  bool& AutoDeleteTSeq();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool  AutoDeleteTRep() const;
  bool& AutoDeleteTRep();
  //! @}

  //----------------------------------------------------------------------------
  // Member functions 
  //----------------------------------------------------------------------------
  //! @name Entity access
  //!
  //! @{
  IRep const& Get(IRepId const iRepId) const;
  IRep&       Get(IRepId const iRepId);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FSet const& Get(FSetId const fSetId) const;
  FSet&       Get(FSetId const fSetId);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  WMat const& Get(WMatId const wMatId) const;
  WMat&       Get(WMatId const wMatId);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FTrk const& Get(FTrkId const fTrkId) const;
  FTrk&       Get(FTrkId const fTrkId);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  PRep const& Get(PRepId const pRepId) const;
  PRep&       Get(PRepId const pRepId);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  CRep const& Get(CRepId const cRepId) const;
  CRep&       Get(CRepId const cRepId);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DRep const& Get(DRepId const dRepId) const;
  DRep&       Get(DRepId const dRepId);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  ORep const& Get(ORepId const oRepId) const;
  ORep&       Get(ORepId const oRepId);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  TSeq const& Get(TSeqId const tSeqId) const;
  TSeq&       Get(TSeqId const tSeqId);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  TRep const& Get(TRepId const tRepId) const;
  TRep&       Get(TRepId const tRepId);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FRep const& Get(FSetId const fSetId, FRepId const fRepId) const;
  FRep&       Get(FSetId const fSetId, FRepId const fRepId);
  //! @}
  
  //----------------------------------------------------------------------------
  //! @name Identifier validation
  //!
  //! @{
  bool IsValid(IRepId const iRepId) const;
  bool IsValid(FSetId const fSetId) const;
  bool IsValid(WMatId const wMatId) const;
  bool IsValid(FTrkId const fTrkId) const;
  bool IsValid(PRepId const pRepId) const;
  bool IsValid(CRepId const cRepId) const;
  bool IsValid(DRepId const dRepId) const;
  bool IsValid(ORepId const oRepId) const;
  bool IsValid(TSeqId const tSeqId) const;
  bool IsValid(TRepId const tRepId) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool IsValid(FSetId const fSetId, FRepId const fRepId) const;
  //! @}

  //----------------------------------------------------------------------------
  //! @name Storage reservation
  //!
  //! @{
  void ReserveIRepS(uint64 size);
  void ReserveFSetS(uint64 size);
  void ReserveWMatS(uint64 size);
  void ReserveFTrkS(uint64 size);
  void ReservePRepS(uint64 size);
  void ReserveCRepS(uint64 size);
  void ReserveDRepS(uint64 size);
  void ReserveORepS(uint64 size);
  void ReserveTSeqS(uint64 size);
  void ReserveTRepS(uint64 size);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void ReserveFSet(FSetId const fSetId, uint64 const size);
  //! @}

  //----------------------------------------------------------------------------
  //! @name Storage expansion
  //!
  //! @{
  void ExpandIRepS(uint64 newSize);
  void ExpandFSetS(uint64 newSize);
  void ExpandWMatS(uint64 newSize);
  void ExpandFTrkS(uint64 newSize);
  void ExpandPRepS(uint64 newSize);
  void ExpandCRepS(uint64 newSize);
  void ExpandDRepS(uint64 newSize);
  void ExpandORepS(uint64 newSize);
  void ExpandTSeqS(uint64 newSize);
  void ExpandTRepS(uint64 newSize);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void ExpandFSet(FSetId const fSetId, uint64 const newSize);
  //! @}

  //----------------------------------------------------------------------------
  //! @name Data creation
  //!
  //! @{
  IRepId CreateIRep();
  FSetId CreateFSet();
  WMatId CreateWMat();
  FTrkId CreateFTrk();
  PRepId CreatePRep();
  CRepId CreateCRep();
  DRepId CreateDRep();
  ORepId CreateORep();
  TSeqId CreateTSeq();
  TRepId CreateTRep();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FRepId CreateFRep(FSetId const fSetId);
  //! @}
  
  //----------------------------------------------------------------------------
  //! @name Data deletion
  //!
  //! @{
  void Delete(IRepId const iRepId);
  void Delete(FSetId const fSetId);
  void Delete(WMatId const wMatId);
  void Delete(FTrkId const fTrkId);
  void Delete(PRepId const pRepId);
  void Delete(CRepId const cRepId);
  void Delete(DRepId const dRepId);
  void Delete(ORepId const oRepId);
  void Delete(TSeqId const tSeqId);
  void Delete(TRepId const tRepId);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Delete(FSetId const fSetId, FRepId const fRepId);
  //! @}

  //----------------------------------------------------------------------------
  //! @name Assignment
  //!
  //! @{
  void Assign(IRepId const iRepId, FSetId const fSetId);
  void Assign(IRepId const iRepId, PRepId const pRepId);
  void Assign(IRepId const iRepId, CRepId const cRepId);
  void Assign(IRepId const iRepId, DRepId const dRepId);
  void Assign(IRepId const iRepId, TSeqId const tSeqId);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Assign(FTrkId const fTrkId, ORepId const oRepId);
  void Assign(FTrkId const fTrkId, TSeqId const tSeqId);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Assign(ORepId const oRepId, TSeqId const tSeqId);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Assign(FSetId const fSetId, FRepId const fRepId, WMatId const wMatId);
  void Assign(FSetId const fSetId, FRepId const fRepId, FTrkId const fTrkId);
  //! @}

  //----------------------------------------------------------------------------
  //! @name Removal
  //!
  //! @{
  void RemoveFSet(IRepId const iRepId);
  void RemovePRep(IRepId const iRepId);
  void RemoveCRep(IRepId const iRepId);
  void RemoveDRep(IRepId const iRepId);
  void RemoveTSeq(IRepId const iRepId);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void RemoveORep(FTrkId const fTrkId);
  void RemoveTSeq(FTrkId const fTrkId);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void RemoveTSeq(ORepId const oRepId);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void RemoveWMat(FSetId const fSetId, FRepId const fRepId);
  void RemoveFTrk(FSetId const fSetId, FRepId const fRepId);
  //! @}

  //----------------------------------------------------------------------------
  //! @name TransformationSequence management
  //!
  //! @{
  void Append(TSeqId const tSeqId, TRepId const tRepId);
  void Insert(TSeqId const tSeqId, TRepId const tRepId, uint64 const position);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Remove(TSeqId const tSeqId, TRepId const tRepId, uint64 const position);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void AssignTSRef(TSeqId const tSeqId, TSeqId const tSRefId);
  void RemoveTSRef(TSeqId const tSeqId);
  //! @}

  //----------------------------------------------------------------------------
  //! @name Validation
  //!
  //! @{
  bool Validate(IRepId const iRepId) const;
  bool Validate(FSetId const fSetId) const;
  bool Validate(WMatId const wMatId) const;
  bool Validate(FTrkId const fTrkId) const;
  bool Validate(PRepId const pRepId) const;
  bool Validate(CRepId const cRepId) const;
  bool Validate(DRepId const dRepId) const;
  bool Validate(ORepId const oRepId) const;
  bool Validate(TSeqId const tSeqId) const;
  bool Validate(TRepId const tRepId) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool Validate(FSetId const fSetId, FRepId const fRepId) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool ValidateIRepS() const;
  bool ValidateFSetS() const;
  bool ValidateWMatS() const;
  bool ValidateFTrkS() const;
  bool ValidatePRepS() const;
  bool ValidateCRepS() const;
  bool ValidateDRepS() const;
  bool ValidateORepS() const;
  bool ValidateTSeqS() const;
  bool ValidateTRepS() const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool Validate() const;
  //! @}

  //----------------------------------------------------------------------------
  //! @name Clean-up
  //!
  //! @{
  void CleanUpFSetS();
  void CleanUpWMatS();
  void CleanUpFTrkS();
  void CleanUpPRepS();
  void CleanUpCRepS();
  void CleanUpDRepS();
  void CleanUpORepS();
  void CleanUpTSeqS();
  void CleanUpTRepS();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void CleanUp();
  //! @}

  //----------------------------------------------------------------------------
  //! @name Compaction
  //!
  //! @{
  void CompactIRepS();
  void CompactFSetS();
  void CompactWMatS();
  void CompactFTrkS();
  void CompactPRepS();
  void CompactCRepS();
  void CompactDRepS();
  void CompactORepS();
  void CompactTSeqS();
  void CompactTRepS();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void CompactFSet(FSetId const fSetId);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Compact();
  //! @}

private:
  //----------------------------------------------------------------------------
  // Member functions
  //----------------------------------------------------------------------------
  //! @name Unreferencing
  //!
  //! @{
  void Unreference(IRepId const iRepId);
  void Unreference(FSetId const fSetId);
  void Unreference(FSetId const fSetId, FRepId const fRepId);
  void Unreference(WMatId const wMatId);
  void Unreference(FTrkId const fTrkId);
  void Unreference(PRepId const pRepId);
  void Unreference(CRepId const cRepId);
  void Unreference(DRepId const dRepId);
  void Unreference(ORepId const oRepId);
  void Unreference(TSeqId const tSeqId);
  void Unreference(TRepId const tRepId);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //! @}

  //----------------------------------------------------------------------------
  //! @name Conditional delete
  //!
  //! @{
  void DeleteUnreferenced(FSetId const fSetId);
  void DeleteUnreferenced(WMatId const wMatId);
  void DeleteUnreferenced(FTrkId const fTrkId);
  void DeleteUnreferenced(PRepId const pRepId);
  void DeleteUnreferenced(CRepId const cRepId);
  void DeleteUnreferenced(DRepId const dRepId);
  void DeleteUnreferenced(ORepId const oRepId);
  void DeleteUnreferenced(TSeqId const tSeqId);
  void DeleteUnreferenced(TRepId const tRepId);
  //! @}

  //----------------------------------------------------------------------------
  // Member variables
  //----------------------------------------------------------------------------
  IRepStorage iRepS_; //!< Storage for ImageRepresentation objects.
  FSetStorage fSetS_; //!< Storage for FeaturePointSet objects.
  WMatStorage wMatS_; //!< Storage for WeightingMatrixRepresentation objects.
  FTrkStorage fTrkS_; //!< Storage for FeaturePointTrack objects.
  PRepStorage pRepS_; //!< Storage for ProjectiveCameraRepresentation objects.
  CRepStorage cRepS_; //!< Storage for CameraRepresentation objects.
  DRepStorage dRepS_; //!< Storage for DistortionRepresentation objects.
  ORepStorage oRepS_; //!< Storage for ObjectPointRepresentation objects.
  TSeqStorage tSeqS_; //!< Storage for TransformationSequence objects.
  TRepStorage tRepS_; //!< Storage for TransformationRepresentation objects.
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool autoDeleteFSet_; //!< FeaturePointSet auto-deletion flag.
  bool autoDeleteWMat_; //!< WeightingMatrixRepresentation auto-deletion flag.
  bool autoDeleteFTrk_; //!< FeaturePointTrack auto-deletion flag.
  bool autoDeletePRep_; //!< ProjectiveCameraRepresentation auto-deletion flag.
  bool autoDeleteCRep_; //!< CameraRepresentation auto-deletion flag.
  bool autoDeleteDRep_; //!< DistortionRepresentation auto-deletion flag.
  bool autoDeleteORep_; //!< ObjectPointRepresentation auto-deletion flag.
  bool autoDeleteTSeq_; //!< TransformationSequence auto-deletion flag.
  bool autoDeleteTRep_; //!< TransformationRepresentation auto-deletion flag.
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// Inline code
//------------------------------------------------------------------------------
#include "dlm/scene/ImageRepresentation.inline.h"
#include "dlm/scene/FeaturePointSet.inline.h"
#include "dlm/scene/FeaturePointRepresentation.inline.h"
#include "dlm/scene/WeightingMatrixRepresentation.inline.h"
#include "dlm/scene/FeaturePointTrack.inline.h"
#include "dlm/scene/ProjectiveCameraRepresentation.inline.h"
#include "dlm/scene/CameraRepresentation.inline.h"
#include "dlm/scene/DistortionRepresentation.inline.h"
#include "dlm/scene/ObjectPointRepresentation.inline.h"
#include "dlm/scene/TransformationSequence.inline.h"
#include "dlm/scene/TransformationRepresentation.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/Scene.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/ArrayObjectTable.inline.h"

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
