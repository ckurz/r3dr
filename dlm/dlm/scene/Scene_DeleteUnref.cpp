//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/scene/Scene.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
//! \brief Conditionally deletes the specified FeaturePointSet.
//! @param fSetId The Identifier for the FSet that may be deleted.
//!
//! If the FSet \link autoDeleteFSet_ auto-deletion flag\endlink is set to
//! `true` and the specified FSet is not referenced by any ImageRepresentation
//! objects, all FeaturePointRepresentation objects contained in the FSet will
//! be unreferenced, and then the associated storage object will be released.
//! Additional objects may be deleted, depending on the auto-deletion settings.
//!
//! If the FSet \link autoDeleteFSet_ auto-deletion flag\endlink is set to
//! `false`, this function has no effect.
//!
//! \note If deletion occurs, the Identifier is no longer valid.
void Scene::DeleteUnreferenced(FSetId const fSetId)
{
  if (autoDeleteFSet_ && !Get(fSetId).Reference<IRep>::IsValid())
  {
    Unreference(fSetId); // Unreference feature points.

    fSetS_.Release(fSetId.Get());
  }
}

//------------------------------------------------------------------------------
//! \brief Conditionally deletes the specified WeightingMatrixRepresentation.
//! @param wMatId The Identifier for the WMat that may be deleted.
//!
//! If the WMat \link autoDeleteWMat_ auto-deletion flag\endlink is set to
//! `true` and the specified WMat is not referenced by any
//! FeaturePointRepresentation objects, the associated storage object will be
//! released.
//!
//! If the FSet \link autoDeleteWMat_ auto-deletion flag\endlink is set to
//! `false`, this function has no effect.
//!
//! \note If deletion occurs, the Identifier is no longer valid.
void Scene::DeleteUnreferenced(WMatId const wMatId)
{
  if (autoDeleteWMat_ && Get(wMatId).ReferencePairSet<FSet, FRep>::Get().empty())
  {
    wMatS_.Release(wMatId.Get());
  }
}

//------------------------------------------------------------------------------
//! \brief Conditionally deletes the specified FeaturePointTrack.
//! @param fTrkId The Identifier for the FTrk that may be deleted.
//!
//! If the FTrk \link autoDeleteFTrk_ auto-deletion flag\endlink is set to
//! `true` and the specified FTrk is not referenced by any
//! FeaturePointRepresentation objects, all connections to
//! TransformationSequence and ObjectPointRepresentation objects will be removed
//! and the associated storage object will be released.
//! Additional objects may be deleted, depending on the auto-deletion settings.
//!
//! If the FTrk \link autoDeleteFTrk_ auto-deletion flag\endlink is set to
//! `false`, this function has no effect.
//!
//! \note If deletion occurs, the Identifier is no longer valid.
void Scene::DeleteUnreferenced(FTrkId const fTrkId)
{
  if (autoDeleteFTrk_ && Get(fTrkId).empty())
  {
    Unreference(fTrkId); // Unreference transformations and object points.

    fTrkS_.Release(fTrkId.Get());
  }
}

//------------------------------------------------------------------------------
//! \brief Conditionally deletes the specified ProjectiveCameraRepresentation.
//! @param pRepId The Identifier for the PRep that may be deleted.
//!
//! If the PRep \link autoDeletePRep_ auto-deletion flag\endlink is set to
//! `true` and the specified PRep is not referenced by any ImageRepresentation
//! objects, the associated storage object will be released.
//!
//! If the PRep \link autoDeletePRep_ auto-deletion flag\endlink is set to
//! `false`, this function has no effect.
//!
//! \note If deletion occurs, the Identifier is no longer valid.
void Scene::DeleteUnreferenced(PRepId const pRepId)
{
  if (autoDeletePRep_ && !Get(pRepId).Reference<IRep>::IsValid())
  {
    pRepS_.Release(pRepId.Get());
  }
}

//------------------------------------------------------------------------------
//! \brief Conditionally deletes the specified CameraRepresentation.
//! @param cRepId The Identifier for the CRep that may be deleted.
//!
//! If the CRep \link autoDeleteCRep_ auto-deletion flag\endlink is set to
//! `true` and the specified CRep is not referenced by any ImageRepresentation
//! objects, the associated storage object will be released.
//!
//! If the CRep \link autoDeleteCRep_ auto-deletion flag\endlink is set to
//! `false`, this function has no effect.
//!
//! \note If deletion occurs, the Identifier is no longer valid.
void Scene::DeleteUnreferenced(CRepId const cRepId)
{
  if (autoDeleteCRep_ && Get(cRepId).ReferenceSet<IRep>::Get().empty())
  {
    cRepS_.Release(cRepId.Get());
  }
}

//------------------------------------------------------------------------------
//! \brief Conditionally deletes the specified DistortionRepresentation.
//! @param dRepId The Identifier for the DRep that may be deleted.
//!
//! If the DRep \link autoDeleteDRep_ auto-deletion flag\endlink is set to
//! `true` and the specified DRep is not referenced by any ImageRepresentation
//! objects, the associated storage object will be released.
//!
//! If the DRep \link autoDeleteDRep_ auto-deletion flag\endlink is set to
//! `false`, this function has no effect.
//!
//! \note If deletion occurs, the Identifier is no longer valid.
void Scene::DeleteUnreferenced(DRepId const dRepId)
{
  if (autoDeleteDRep_ && Get(dRepId).ReferenceSet<IRep>::Get().empty())
  {
    dRepS_.Release(dRepId.Get());
  }
}

//------------------------------------------------------------------------------
//! \brief Conditionally deletes the specified ObjectPointRepresentation.
//! @param oRepId The Identifier for the ORep that may be deleted.
//!
//! If the ORep \link autoDeleteORep_ auto-deletion flag\endlink is set to
//! `true` and the specified ORep is not referenced by any
//! FeaturePointTrack objects, all connections to TransformationSequence
//! objects will be removed and the associated storage object will be released.
//! Additional objects may be deleted, depending on the auto-deletion settings.
//!
//! If the ORep \link autoDeleteORep_ auto-deletion flag\endlink is set to
//! `false`, this function has no effect.
//!
//! \note If deletion occurs, the Identifier is no longer valid.
void Scene::DeleteUnreferenced(ORepId const oRepId)
{
  if (autoDeleteORep_ && Get(oRepId).ReferenceSet<FTrk>::Get().empty())
  {
    Unreference(oRepId); // Unreference transformation.

    oRepS_.Release(oRepId.Get());
  }
}

//------------------------------------------------------------------------------
//! \brief Conditionally deletes the specified TransformationSequence.
//! @param tSeqId The Identifier for the TSeq that may be deleted.
//!
//! If the TSeq \link autoDeleteTSeq_ auto-deletion flag\endlink is set to
//! `true` and the specified TSeq is not referenced by any ImageRepresentation,
//! FeaturePointTrack, ObjectPointRepresentation, or other
//! TransformationSequence objects, all connections to
//! TransformationRepresentation and TransformationSequence objects will be
//! removed, and then the associated storage object will be released.
//! Additional objects may be deleted, depending on the auto-deletion settings.
//!
//! If the TSeq \link autoDeleteTSeq_ auto-deletion flag\endlink is set to
//! `false`, this function has no effect.
//!
//! \note If deletion occurs, the Identifier is no longer valid.
void Scene::DeleteUnreferenced(TSeqId const tSeqId)
{
  if (autoDeleteTSeq_)
  {
    TSeq const& tSeq = Get(tSeqId);
  
    if (tSeq.ReferenceSet<IRep>::Get().empty() &&
        tSeq.ReferenceSet<FTrk>::Get().empty() &&
        tSeq.ReferenceSet<ORep>::Get().empty() &&
        tSeq.ReferenceSet<TSeq>::Get().empty())
    {
      Unreference(tSeqId); // Unreference transformations.

      tSeqS_.Release(tSeqId.Get());
    }
  }
}

//------------------------------------------------------------------------------
//! \brief Conditionally deletes the specified TransformationRepresentation.
//! @param tRepId The Identifier for the TRep that may be deleted.
//!
//! If the TRep \link autoDeleteTRep_ auto-deletion flag\endlink is set to
//! `true` and the specified TRep is not referenced by any
//! TransformationSequence objects, the associated storage object will be
//! released.
//!
//! If the TRep \link autoDeleteTRep_ auto-deletion flag\endlink is set to
//! `false`, this function has no effect.
//!
//! \note If deletion occurs, the Identifier is no longer valid.
void Scene::DeleteUnreferenced(TRepId const tRepId)
{
  if (autoDeleteTRep_ && Get(tRepId).TSeqM().empty())
  {
    tRepS_.Release(tRepId.Get());
  }
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
