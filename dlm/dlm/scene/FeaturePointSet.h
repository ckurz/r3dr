//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_scene_FeaturePointSet_h_
#define dlm_scene_FeaturePointSet_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/scene/Reference.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/Identifiers.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/FeaturePointRepresentation.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/ArrayObjectTable.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
//! \brief Specify.
// FeaturePointSet class
//------------------------------------------------------------------------------
class FeaturePointSet : private ArrayObjectTable<FRep>
                      , public Reference<dlm::IRep>
{
public:
  //----------------------------------------------------------------------------
  // Friends
  //----------------------------------------------------------------------------
  friend class BinarySerializer;   //!< \brief For serialization.
  friend class BinaryDeserializer; //!< \brief For deserialization.
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  friend class Scene; //!< \brief The Scene is allowed to change the state.
  friend class ArrayObjectTable<FeaturePointSet>; //!< For construction.

  //----------------------------------------------------------------------------
  // Nested
  //----------------------------------------------------------------------------
  typedef ArrayObjectTable<FRep> BaseClass;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  typedef Reference<dlm::IRep> IRepReference;

  //----------------------------------------------------------------------------
  // Public interface
  //----------------------------------------------------------------------------
  using BaseClass::Active;
  using BaseClass::At;
  using BaseClass::Range;
  using BaseClass::Size;
  using BaseClass::IsEmpty;

  using BaseClass::iterator;
  using BaseClass::const_iterator;

  using BaseClass::begin;
  using BaseClass::cbegin;

  using BaseClass::end;
  using BaseClass::cend;

  //----------------------------------------------------------------------------
  // Convenience functions
  //----------------------------------------------------------------------------
  //! @name Convenience functions
  //!
  //! @{
  bool HasIRep() const;
  IRepId  IRep() const;
  //! @}

private:
  //----------------------------------------------------------------------------
  // Constructors
  //----------------------------------------------------------------------------
  FeaturePointSet(BaseClass       const& arrayObjectTable = BaseClass());
  FeaturePointSet(FeaturePointSet const& other);

  //----------------------------------------------------------------------------
  // Operators
  //----------------------------------------------------------------------------
  FeaturePointSet& operator=(BaseClass       const& arrayObjectTable);
  FeaturePointSet& operator=(FeaturePointSet const& other);

  //----------------------------------------------------------------------------
  // Member functions
  //----------------------------------------------------------------------------
  FRepId CreateFRep();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Delete(FRepId const id);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FRep const& Get(FRepId const id) const;
  FRep      & Get(FRepId const id);
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
