//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_scene_ReferencePairSet_h_
#define dlm_scene_ReferencePairSet_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/core/Identifier.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <set>
#include <utility>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// ReferencePairSet class
//------------------------------------------------------------------------------
template <typename T, typename U>
class ReferencePairSet
{
public:
  //============================================================================
  // Nested
  //----------------------------------------------------------------------------
  typedef std::pair<Identifier<T>, Identifier<U> > ElementType;

  //============================================================================
  // Accessors
  //----------------------------------------------------------------------------
  std::set<ElementType> const& Get() const;

  //============================================================================
  // Friends
  //----------------------------------------------------------------------------
  friend class Scene;
  friend class WeightingMatrixRepresentation;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  friend class BinarySerializer;
  friend class BinaryDeserializer;

private:
  //============================================================================
  // Constructors
  //----------------------------------------------------------------------------
  ReferencePairSet(std::set<ElementType> const& set = std::set<ElementType>());
  ReferencePairSet(ReferencePairSet const& other);

  //============================================================================
  // Operators
  //----------------------------------------------------------------------------
  ReferencePairSet& operator=(ReferencePairSet const& other);

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  void Add   (ElementType const id);
  void Remove(ElementType const id);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Clear();

  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  std::set<ElementType> set_;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
