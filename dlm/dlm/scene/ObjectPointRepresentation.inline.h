//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_scene_ObjectPointRepresentation_inline_h_
#define dlm_scene_ObjectPointRepresentation_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/scene/ObjectPointRepresentation.h"

//------------------------------------------------------------------------------
#include "dlm/scene/Reference.inline.h"
#include "dlm/scene/ReferenceSet.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/flags/ObjectPointFlags.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/entity/ObjectPoint.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
inline ObjectPointRepresentation::ObjectPointRepresentation(
  BaseClass const& objectPoint, FlagClass const& flags)
  : BaseClass(objectPoint)
  , FlagClass(flags      )
{}

//------------------------------------------------------------------------------
inline ObjectPointRepresentation::ObjectPointRepresentation(
  ObjectPointRepresentation const& other)
  : BaseClass         (other)
  , FlagClass         (other)
  , TSeqReference     (other)
  , ReferenceSet<FTrk>(other)
{}

//==============================================================================
// Accessors
//------------------------------------------------------------------------------
inline ObjectPointRepresentation& ObjectPointRepresentation::operator=(
  BaseClass const& objectPoint)
{
  BaseClass::operator=(objectPoint);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
inline ObjectPointRepresentation& ObjectPointRepresentation::operator=(
  FlagClass const& flags)
{
  FlagClass::operator=(flags);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
inline ObjectPointRepresentation& ObjectPointRepresentation::operator=(
  ObjectPointRepresentation const& other)
{
  BaseClass::operator=(other);
  FlagClass::operator=(other);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  TSeqReference     ::operator=(other);
  ReferenceSet<FTrk>::operator=(other);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//==============================================================================
// Convenience functions
//------------------------------------------------------------------------------
//! \brief Specify.
//! @return Specify.
//!
inline bool ObjectPointRepresentation::HasTSeq() const
{
  return this->TSeqReference::IsValid();
}

//------------------------------------------------------------------------------
//! \brief Specify.
//! @return Specify.
//!
inline TSeqId ObjectPointRepresentation::TSeq() const
{
  return this->TSeqReference::Get();
}

//------------------------------------------------------------------------------
inline ObjectPointRepresentation::FTrkSet const&
  ObjectPointRepresentation::FTrkS() const
{
  return this->ReferenceSet<FTrk>::Get();
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
