//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_scene_FeaturePointSet_inline_h_
#define dlm_scene_FeaturePointSet_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/scene/FeaturePointSet.h"

//------------------------------------------------------------------------------
#include "dlm/scene/Reference.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/FeaturePointRepresentation.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
inline FeaturePointSet::FeaturePointSet(BaseClass const& arrayObjectTable)
  : BaseClass(arrayObjectTable)
{}

//------------------------------------------------------------------------------
inline FeaturePointSet::FeaturePointSet(FeaturePointSet const& other)
  : BaseClass    (other)
  , IRepReference(other)
{}

//==============================================================================
// Operators
//------------------------------------------------------------------------------
inline FeaturePointSet& FeaturePointSet::operator=(
  BaseClass const& arrayObjectTable)
{
  BaseClass::operator=(arrayObjectTable);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
inline FeaturePointSet& FeaturePointSet::operator=(
  FeaturePointSet const& other)
{
  BaseClass::operator=(other);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  IRepReference::operator=(other);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
inline FRepId FeaturePointSet::CreateFRep()
{
  return FRepId(PushBack(FRep()));
}

//------------------------------------------------------------------------------
inline void FeaturePointSet::Delete(FRepId const id)
{
  BaseClass::Release(id.Get()); //will complain if id is not valid
}

//------------------------------------------------------------------------------
inline FRep const& FeaturePointSet::Get(FRepId const id) const
{
  return BaseClass::At(id.Get()); //will complain if id is not valid
}

//------------------------------------------------------------------------------
inline FRep& FeaturePointSet::Get(FRepId const id)
{
  return BaseClass::At(id.Get()); //will complain if id is not valid
}

//==============================================================================
// Convenience functions
//------------------------------------------------------------------------------
//! \brief Specify.
//! @return Specify.
//!
inline bool FeaturePointSet::HasIRep() const
{
  return this->IRepReference::IsValid();
}

//------------------------------------------------------------------------------
//! \brief Specify.
//! @return Specify.
//!
inline IRepId FeaturePointSet::IRep() const
{
  return this->IRepReference::Get();
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
