//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_scene_FeaturePointTrack_h_
#define dlm_scene_FeaturePointTrack_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/scene/Reference.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/Identifiers.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/ArrayObjectTable.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <map>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// FeaturePointTrack class
//------------------------------------------------------------------------------
class FeaturePointTrack : private std::map<FSetId, FRepId>
                        , public Reference<ORep>
                        , public Reference<TSeq>
{
public:
  //----------------------------------------------------------------------------
  // Friends
  //----------------------------------------------------------------------------
  friend class BinarySerializer;   //!< \brief For serialization.
  friend class BinaryDeserializer; //!< \brief For deserialization.
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  friend class Scene; //!< \brief The Scene is allowed to change the state.
  friend class ArrayObjectTable<FeaturePointTrack>; //!< For construction.

  //----------------------------------------------------------------------------
  // Nested
  //----------------------------------------------------------------------------
  typedef std::map<FSetId, FRepId> BaseClass;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  typedef Reference<ObjectPointRepresentation> ORepReference;
  typedef Reference<TransformationSequence>    TSeqReference;

  //----------------------------------------------------------------------------
  // Public interface
  //----------------------------------------------------------------------------
  using BaseClass::const_iterator;
  using BaseClass::const_reverse_iterator;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  using BaseClass::begin;
  using BaseClass::cbegin;
  using BaseClass::end;
  using BaseClass::cend;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  using BaseClass::rbegin;
  using BaseClass::crbegin;
  using BaseClass::rend;
  using BaseClass::crend;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  using BaseClass::empty;
  using BaseClass::size;

  //----------------------------------------------------------------------------
  // Member functions
  //----------------------------------------------------------------------------
  bool Get(FSetId const fSet, FRepId& id) const;

  //----------------------------------------------------------------------------
  // Convenience functions
  //----------------------------------------------------------------------------
  //! @name Convenience functions
  //!
  //! @{
  bool HasORep() const;
  ORepId  ORep() const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool HasTSeq() const;
  TSeqId  TSeq() const;
  //! @}

private:
  //----------------------------------------------------------------------------
  // Constructors
  //----------------------------------------------------------------------------
  FeaturePointTrack(BaseClass         const& tracks = BaseClass());
  FeaturePointTrack(FeaturePointTrack const& other);

  //----------------------------------------------------------------------------
  // Operators
  //----------------------------------------------------------------------------
  FeaturePointTrack& operator=(BaseClass         const& tracks);
  FeaturePointTrack& operator=(FeaturePointTrack const& other );

  //----------------------------------------------------------------------------
  // Member functions
  //----------------------------------------------------------------------------
  void Assign(FSetId const fSetId, FRepId const id);
  void Remove(FSetId const fSetId);
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
