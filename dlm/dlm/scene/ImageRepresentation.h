//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_scene_ImageRepresentation_h_
#define dlm_scene_ImageRepresentation_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/scene/Reference.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/Identifiers.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/entity/Image.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/ArrayObjectTable.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <string>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
//! \brief The representation for Image entities in the Scene.
//!
//! The ImageRepresentation (IRep) links Image entities with a FeatureSet (FSet)
//! and either a ProjectiveCamera entity or a combination of a Camera entity, a
//! Distortion entity, and a TransformationSequence (TSeq).
//! To this end, it may store a Reference to a FeatureSet, a
//! ProjectiveCameraRepresentation (PRep), a CameraRepresentation (CRep), a
//! DistortionRepresentation (DRep), and a TransformationSequence.
//!
//! \warning While the ImageRepresentation is capable of storing both a
//! PRepId and a set of CRepId, DRepId, and TSeqId, the Scene the object is
//! contained in enforces that at any point in time there will only be either
//! a PRepId or a set of CRepId, DRepId, and TSeqId present, with the last one
//! to be assigned breaking the assignments of the other group.
//!
//! \note Starting with dlm 1.6.0, support for the interface of version 1.3.0
//! has been re-enabled. The following equivalences exist:
//! \code
//!   dlm::ImageRepresentation iRep;
//!
//!   // In the following, each pair of function calls (variantes A and B)
//!   // produces equivalent results.
//!  
//!   // Feature point set referencing
//!   {
//!     bool validA = iRep.dlm::Reference<dlm::FSet>::IsValid();
//!     bool validB = iRep.HasFSet();
//!     
//!     dlm::FSetId fSetIdA = iRep.dlm::Reference<dlm::FSet>::Get();
//!     dlm::FSetId fSetIdB = iRep.FSet();
//!   }
//!
//!   // Projective camera referencing
//!   {
//!     bool validA = iRep.dlm::Reference<dlm::PRep>::IsValid();
//!     bool validB = iRep.HasPRep();
//!     
//!     dlm::PRepId pRepIdA = iRep.dlm::Reference<dlm::PRep>::Get();
//!     dlm::PRepId pRepIdB = iRep.PRep();
//!   }
//!
//!   // Camera referencing
//!   {
//!     bool validA = iRep.dlm::Reference<dlm::CRep>::IsValid();
//!     bool validB = iRep.HasCRep();
//!     
//!     dlm::CRepId cRepIdA = iRep.dlm::Reference<dlm::CRep>::Get();
//!     dlm::CRepId cRepIdB = iRep.CRep();
//!   }
//!
//!   // Distortion referencing
//!   {
//!     bool validA = iRep.dlm::Reference<dlm::DRep>::IsValid();
//!     bool validB = iRep.HasDRep();
//!     
//!     dlm::DRepId dRepIdA = iRep.dlm::Reference<dlm::PRep>::Get();
//!     dlm::DRepId dRepIdB = iRep.DRep();
//!   }
//!
//!   // Projective camera referencing
//!   {
//!     bool validA = iRep.dlm::Reference<dlm::TSeq>::IsValid();
//!     bool validB = iRep.HasTSeq();
//!     
//!     dlm::TSeqId tSeqIdA = iRep.dlm::Reference<dlm::TSeq>::Get();
//!     dlm::TSeqId tSeqIdB = iRep.TSeq();
//!   }
//! \endcode
//! The interface of version 1.3.0 (variant B) is less verbose, as it is a
//! shorthand for the native function calls.
//!
// ImageRepresentation class
//------------------------------------------------------------------------------
class ImageRepresentation : public Image
                          , public Reference<FeaturePointSet>
                          , public Reference<ProjectiveCameraRepresentation>
                          , public Reference<CameraRepresentation>
                          , public Reference<DistortionRepresentation>
                          , public Reference<TransformationSequence>
{
public:
  //----------------------------------------------------------------------------
  // Friends
  //----------------------------------------------------------------------------
  friend class BinarySerializer;   //!< \brief For serialization.
  friend class BinaryDeserializer; //!< \brief For deserialization.
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  friend class Scene; //!< \brief The Scene is allowed to change the state.
  friend class ArrayObjectTable<ImageRepresentation>; //!< For construction.
  
  //----------------------------------------------------------------------------
  // Nested
  //----------------------------------------------------------------------------
  typedef Image BaseClass; //!< \brief For convenience.

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  typedef Reference<FeaturePointSet>                FSetReference;
  typedef Reference<ProjectiveCameraRepresentation> PRepReference;
  typedef Reference<CameraRepresentation>           CRepReference;
  typedef Reference<DistortionRepresentation>       DRepReference;
  typedef Reference<TransformationSequence>         TSeqReference;

  //----------------------------------------------------------------------------
  // Operators
  //----------------------------------------------------------------------------
  ImageRepresentation& operator=(BaseClass const& image);
  
  //----------------------------------------------------------------------------
  // Convenience functions
  //----------------------------------------------------------------------------
  //! @name Convenience functions
  //!
  //! @{
  bool HasPRep() const;
  PRepId  PRep() const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool HasCRep() const;
  CRepId  CRep() const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool HasDRep() const;
  DRepId  DRep() const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool HasFSet() const;
  FSetId  FSet() const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool HasTSeq() const;
  TSeqId  TSeq() const;
  //! @}

  //----------------------------------------------------------------------------
  // Mutators
  //----------------------------------------------------------------------------
  //! @name Sequence number
  //!
  //! @{
  uint64  SequenceNumber() const;
  uint64& SequenceNumber();
  //! @}

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //! @name Filename
  //!
  //! @{
  std::string const& Filename() const;
  std::string&       Filename();
  //! @}

private:
  //----------------------------------------------------------------------------
  // Constructors
  //----------------------------------------------------------------------------
  ImageRepresentation(BaseClass           const& image = BaseClass());
  ImageRepresentation(ImageRepresentation const& other);

  //----------------------------------------------------------------------------
  // Operators
  //----------------------------------------------------------------------------
  ImageRepresentation& operator=(ImageRepresentation const& other);

  //----------------------------------------------------------------------------
  // Member variables
  //----------------------------------------------------------------------------
  std::string filename_; //!< \brief The image filename.
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint64 sequenceNumber_; //!< \brief The number of the image in the sequence.
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
