//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/scene/Scene.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
//! \brief Connect the specified FeaturePointSet with the indicated
//! ImageRepresentation.
//! @param iRepId The Identifier of the IRep that shall be connected.
//! @param fSetId The Identifier of the FSet that shall be connected.
//!
//! A connection between the indicated objects is created; a previously existing
//! connection is removed.
//! Subsequently unreferenced objects may be deleted, depending on the
//! auto-deletion settings.
//!
//! If either Identifier is not in the valid range of the container, an
//! OutOfRange exception will be thrown.
//!
//! If either Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! \sa RemoveFSet(IRepId const iRepId).
void Scene::Assign(IRepId const iRepId, FSetId const fSetId)
{
  DLM_ASSERT(IsValid(fSetId), DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  IRep& iRep = Get(iRepId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (iRep.Reference<FSet>::IsValid())
  {
    FSetId const old = iRep.Reference<FSet>::Get();

    if (old == fSetId) { return; } // Nothing to do.

    Get(old).Reference<IRep>::Remove();

    DeleteUnreferenced(old);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Get(fSetId).Reference<IRep>::Assign(iRepId);

  iRep.Reference<FSet>::Assign(fSetId);
}

//------------------------------------------------------------------------------
//! \brief Connect the specified ProjectiveCameraRepresentation with the
//! indicated ImageRepresentation.
//! @param iRepId The Identifier of the IRep that shall be connected.
//! @param pRepId The Identifier of the PRep that shall be connected.
//!
//! A connection between the indicated objects is created; a previously existing
//! connection is removed. This includes previously assigned
//! CameraRepresentation, DistortionRepresentation, and TransformationSequence
//! objects.
//! Subsequently unreferenced objects may be deleted, depending on the
//! auto-deletion settings.
//!
//! If either Identifier is not in the valid range of the container, an
//! OutOfRange exception will be thrown.
//!
//! If either Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! \sa RemovePRep(IRepId const iRepId).
void Scene::Assign(IRepId const iRepId, PRepId const pRepId)
{
  DLM_ASSERT(IsValid(pRepId), DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  IRep& iRep = Get(iRepId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Remove metric camera stuff.
  RemoveCRep(iRepId);
  RemoveDRep(iRepId);
  RemoveTSeq(iRepId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (iRep.Reference<PRep>::IsValid())
  {
    PRepId const old = iRep.Reference<PRep>::Get();

    if (old == pRepId) { return; } // Nothing to do.

    Get(old).Reference<IRep>::Remove();

    DeleteUnreferenced(old);
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Get(pRepId).Reference<IRep>::Assign(iRepId);

  iRep.Reference<PRep>::Assign(pRepId);
}

//------------------------------------------------------------------------------
//! \brief Connect the specified CameraRepresentation with the
//! indicated ImageRepresentation.
//! @param iRepId The Identifier of the IRep that shall be connected.
//! @param cRepId The Identifier of the CRep that shall be connected.
//!
//! A connection between the indicated objects is created; a previously existing
//! connection is removed. This includes previously assigned
//! ProjectiveCameraRepresentation objects.
//! Subsequently unreferenced objects may be deleted, depending on the
//! auto-deletion settings.
//!
//! If either Identifier is not in the valid range of the container, an
//! OutOfRange exception will be thrown.
//!
//! If either Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! \sa RemoveCRep(IRepId const iRepId).
void Scene::Assign(IRepId const iRepId, CRepId const cRepId)
{
  DLM_ASSERT(IsValid(cRepId), DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  IRep& iRep = Get(iRepId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Remove projective camera stuff.
  RemovePRep(iRepId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (iRep.Reference<CRep>::IsValid())
  {
    CRepId const old = iRep.Reference<CRep>::Get();

    if (old == cRepId) { return; } // Nothing to do.

    Get(old).ReferenceSet<IRep>::Remove(iRepId);

    DeleteUnreferenced(old);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Get(cRepId).ReferenceSet<IRep>::Add(iRepId);

  iRep.Reference<CRep>::Assign(cRepId);
}

//------------------------------------------------------------------------------
//! \brief Connect the specified DistortionRepresentation with the
//! indicated ImageRepresentation.
//! @param iRepId The Identifier of the IRep that shall be connected.
//! @param dRepId The Identifier of the DRep that shall be connected.
//!
//! A connection between the indicated objects is created; a previously existing
//! connection is removed.
//! Subsequently unreferenced objects may be deleted, depending on the
//! auto-deletion settings.
//!
//! \note If the indicated IRep is connected to a
//! ProjectiveCameraRepresentation, a RuntimeError exception will be thrown.
//!
//! If either Identifier is not in the valid range of the container, an
//! OutOfRange exception will be thrown.
//!
//! If either Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! \sa RemoveDRep(IRepId const iRepId).
void Scene::Assign(IRepId const iRepId, DRepId const dRepId)
{
  DLM_ASSERT(IsValid(dRepId), DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  IRep& iRep = Get(iRepId);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DLM_ASSERT(!iRep.Reference<PRep>::IsValid(), DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (iRep.Reference<DRep>::IsValid())
  {
    DRepId const old = iRep.Reference<DRep>::Get();

    if (old == dRepId) { return; } // Nothing to do.

    Get(old).ReferenceSet<IRep>::Remove(iRepId);

    DeleteUnreferenced(old);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Get(dRepId).ReferenceSet<IRep>::Add(iRepId);

  iRep.Reference<DRep>::Assign(dRepId);
}

//------------------------------------------------------------------------------
//! \brief Connect the specified TransformationSequence with the
//! indicated ImageRepresentation.
//! @param iRepId The Identifier of the IRep that shall be connected.
//! @param tSeqId The Identifier of the TSeq that shall be connected.
//!
//! A connection between the indicated objects is created; a previously existing
//! connection is removed.
//! Subsequently unreferenced objects may be deleted, depending on the
//! auto-deletion settings.
//!
//! \note If the indicated IRep is connected to a
//! ProjectiveCameraRepresentation, a RuntimeError exception will be thrown.
//!
//! If either Identifier is not in the valid range of the container, an
//! OutOfRange exception will be thrown.
//!
//! If either Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! \sa RemoveTSeq(IRepId const iRepId).
void Scene::Assign(IRepId const iRepId, TSeqId const tSeqId)
{
  DLM_ASSERT(IsValid(tSeqId), DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  IRep& iRep = Get(iRepId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DLM_ASSERT(!iRep.Reference<PRep>::IsValid(), DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (iRep.Reference<TSeq>::IsValid())
  {
    TSeqId const old = iRep.Reference<TSeq>::Get();

    if (old == tSeqId) { return; } // Nothing to do.

    Get(old).ReferenceSet<IRep>::Remove(iRepId);

    DeleteUnreferenced(old);
  }

  Get(tSeqId).ReferenceSet<IRep>::Add(iRepId);

  iRep.Reference<TSeq>::Assign(tSeqId);
}

//------------------------------------------------------------------------------
//! \brief Connect the specified ObjectPointRepresentation with the
//! indicated FeaturePointTrack.
//! @param fTrkId The Identifier of the FTrk that shall be connected.
//! @param oRepId The Identifier of the ORep that shall be connected.
//!
//! A connection between the indicated objects is created; a previously existing
//! connection is removed.
//! Subsequently unreferenced objects may be deleted, depending on the
//! auto-deletion settings.
//!
//! If either Identifier is not in the valid range of the container, an
//! OutOfRange exception will be thrown.
//!
//! If either Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! \sa RemoveORep(FTrkId const fTrkId).
void Scene::Assign(FTrkId const fTrkId, ORepId const oRepId)
{
  DLM_ASSERT(IsValid(oRepId), DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FTrk& fTrk = Get(fTrkId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (fTrk.Reference<ORep>::IsValid())
  {
    ORepId const old = fTrk.Reference<ORep>::Get();

    if (old == oRepId) { return; } // Nothing to do.

    Get(old).ReferenceSet<FTrk>::Remove(fTrkId);

    DeleteUnreferenced(old);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Get(oRepId).ReferenceSet<FTrk>::Add(fTrkId);

  fTrk.Reference<ORep>::Assign(oRepId);
}

//------------------------------------------------------------------------------
//! \brief Connect the specified TransformationSequence with the
//! indicated FeaturePointTrack.
//! @param fTrkId The Identifier of the FTrk that shall be connected.
//! @param tSeqId The Identifier of the TSeq that shall be connected.
//!
//! A connection between the indicated objects is created; a previously existing
//! connection is removed.
//! Subsequently unreferenced objects may be deleted, depending on the
//! auto-deletion settings.
//!
//! If either Identifier is not in the valid range of the container, an
//! OutOfRange exception will be thrown.
//!
//! If either Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! \sa RemoveTSeq(FTrkId const fTrkId).
void Scene::Assign(FTrkId const fTrkId, TSeqId const tSeqId)
{
  DLM_ASSERT(IsValid(tSeqId), DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FTrk& fTrk = Get(fTrkId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (fTrk.Reference<TSeq>::IsValid())
  {
    TSeqId const old = fTrk.Reference<TSeq>::Get();

    if (old == tSeqId) { return; } // Nothing to do.

    Get(old).ReferenceSet<FTrk>::Remove(fTrkId);

    DeleteUnreferenced(old);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Get(tSeqId).ReferenceSet<FTrk>::Add(fTrkId);

  fTrk.Reference<TSeq>::Assign(tSeqId);
}

//------------------------------------------------------------------------------
//! \brief Connect the specified TransformationSequence with the
//! indicated ObjectPointRepresentation.
//! @param oRepId The Identifier of the ORep that shall be connected.
//! @param tSeqId The Identifier of the TSeq that shall be connected.
//!
//! A connection between the indicated objects is created; a previously existing
//! connection is removed.
//! Subsequently unreferenced objects may be deleted, depending on the
//! auto-deletion settings.
//!
//! If either Identifier is not in the valid range of the container, an
//! OutOfRange exception will be thrown.
//!
//! If either Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! \sa RemoveTSeq(ORepId const oRepId).
void Scene::Assign(ORepId const oRepId, TSeqId const tSeqId)
{
  DLM_ASSERT(IsValid(tSeqId), DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  ORep& oRep = Get(oRepId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (oRep.Reference<TSeq>::IsValid())
  {
    TSeqId const old = oRep.Reference<TSeq>::Get();

    if (old == tSeqId) { return; } // Nothing to do.

    Get(old).ReferenceSet<ORep>::Remove(oRepId);

    DeleteUnreferenced(old);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Get(tSeqId).ReferenceSet<ORep>::Add(oRepId);

  oRep.Reference<TSeq>::Assign(tSeqId);
}

//------------------------------------------------------------------------------
//! \brief Connect the specified WeightingMatrixRepresentation with the
//! indicated FeaturePointRepresentation.
//! @param fSetId The Identifier of the FSet the FRep resides in.
//! @param fRepId The Identifier of the FRep that shall be connected.
//! @param wMatId The Identifier of the WMat that shall be connected.
//!
//! A connection between the indicated objects is created; a previously existing
//! connection is removed.
//! Subsequently unreferenced objects may be deleted, depending on the
//! auto-deletion settings.
//!
//! If either Identifier is not in the valid range of the container, an
//! OutOfRange exception will be thrown.
//!
//! If either Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! \sa RemoveWMat(FSetId const fSetId, FRepId const fRepId).
void Scene::Assign(
  FSetId const fSetId, FRepId const fRepId, WMatId const wMatId)
{
  FRep& fRep = fSetS_.At(fSetId.Get()).At(fRepId.Get());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (fRep.Reference<WMat>::IsValid())
  {
    WMatId const old = fRep.Reference<WMat>::Get();

    if (old == wMatId) { return; } // Nothing to do.

    wMatS_.At(old.Get()).Remove(std::make_pair(fSetId, fRepId));

    DeleteUnreferenced(old);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  wMatS_.At(wMatId.Get()).Add(std::make_pair(fSetId, fRepId));

  fRep.Reference<WMat>::Assign(wMatId);
}

//------------------------------------------------------------------------------
//! \brief Connect the specified FeaturePointTrack with the indicated
//! FeaturePointRepresentation.
//! @param fSetId The Identifier of the FSet the FRep resides in.
//! @param fRepId The Identifier of the FRep that shall be connected.
//! @param fTrkId The Identifier of the FTrk that shall be connected.
//!
//! A connection between the indicated objects is created; a previously existing
//! connection is removed.
//! Subsequently unreferenced objects may be deleted, depending on the
//! auto-deletion settings.
//!
//! If either Identifier is not in the valid range of the container, an
//! OutOfRange exception will be thrown.
//!
//! If either Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! \sa RemoveFTrk(FSetId const fSetId, FRepId const fRepId).
void Scene::Assign(
  FSetId const fSetId, FRepId const fRepId, FTrkId const fTrkId)
{
  FRep& fRep = fSetS_.At(fSetId.Get()).At(fRepId.Get());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (fRep.Reference<FTrk>::IsValid())
  {
    FTrkId const old = fRep.Reference<FTrk>::Get();

    if (old == fTrkId) { return; } // Nothing to do.

    fTrkS_.At(old.Get()).Remove(fSetId);

    DeleteUnreferenced(old);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  fTrkS_.At(fTrkId.Get()).Assign(fSetId, fRepId);

  fRep.Reference<FTrk>::Assign(fTrkId);
}

//------------------------------------------------------------------------------
//! \brief Remove the referenced FeaturePointSet object from the
//! indicated ImageRepresentation object.
//! @param iRepId The Identifier of the IRep the connection shall be removed
//! from.
//!
//! The connection between the indicated objects is removed.
//! Subsequently unreferenced objects may be deleted, depending on the
//! auto-deletion settings.
//!
//! If the Identifier is not in the valid range of the \link iRepS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! If the IRep does not reference an FSet, this function has no effect.
//!
//! \sa Assign(IRepId const iRepId, FSetId const fSetId).
void Scene::RemoveFSet(IRepId const iRepId)
{
  IRep& iRep = Get(iRepId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (!iRep.Reference<FSet>::IsValid()) { return; } // Nothing to do.

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FSetId const fSetId = iRep.Reference<FSet>::Get();

  Get(fSetId).Reference<IRep>::Remove();

  iRep.Reference<FSet>::Remove();

  DeleteUnreferenced(fSetId);
}

//------------------------------------------------------------------------------
//! \brief Remove the referenced ProjectiveCameraRepresentation object from the
//! indicated ImageRepresentation object.
//! @param iRepId The Identifier of the IRep the connection shall be removed
//! from.
//!
//! The connection between the indicated objects is removed.
//! Subsequently unreferenced objects may be deleted, depending on the
//! auto-deletion settings.
//!
//! If the Identifier is not in the valid range of the \link iRepS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! If the IRep does not reference a PRep, this function has no effect.
//!
//! \sa Assign(IRepId const iRepId, PRepId const pRepId).
void Scene::RemovePRep(IRepId const iRepId)
{
  IRep& iRep = Get(iRepId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (!iRep.Reference<PRep>::IsValid()) { return; } // Nothing to do.

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  PRepId const pRepId = iRep.Reference<PRep>::Get();

  Get(pRepId).Reference<IRep>::Remove();

  iRep.Reference<PRep>::Remove();

  DeleteUnreferenced(pRepId);
}

//------------------------------------------------------------------------------
//! \brief Remove the referenced CameraRepresentation object from the
//! indicated ImageRepresentation object.
//! @param iRepId The Identifier of the IRep the connection shall be removed
//! from.
//!
//! The connection between the indicated objects is removed.
//! Subsequently unreferenced objects may be deleted, depending on the
//! auto-deletion settings.
//!
//! If the Identifier is not in the valid range of the \link iRepS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! If the IRep does not reference a CRep, this function has no effect.
//!
//! \sa Assign(IRepId const iRepId, CRepId const cRepId).
void Scene::RemoveCRep(IRepId const iRepId)
{
  IRep& iRep = Get(iRepId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (!iRep.Reference<CRep>::IsValid()) { return; } // Nothing to do.

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  CRepId const cRepId = iRep.Reference<CRep>::Get();

  Get(cRepId).ReferenceSet<IRep>::Remove(iRepId);

  iRep.Reference<CRep>::Remove();

  DeleteUnreferenced(cRepId);
}

//------------------------------------------------------------------------------
//! \brief Remove the referenced DistortionRepresentation object from the
//! indicated ImageRepresentation object.
//! @param iRepId The Identifier of the IRep the connection shall be removed
//! from.
//!
//! The connection between the indicated objects is removed.
//! Subsequently unreferenced objects may be deleted, depending on the
//! auto-deletion settings.
//!
//! If the Identifier is not in the valid range of the \link iRepS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! If the IRep does not reference a DRep, this function has no effect.
//!
//! \sa Assign(IRepId const iRepId, DRepId const dRepId).
void Scene::RemoveDRep(IRepId const iRepId)
{
  IRep& iRep = Get(iRepId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (!iRep.Reference<DRep>::IsValid()) { return; } // Nothing to do.

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DRepId const dRepId = iRep.Reference<DRep>::Get();

  Get(dRepId).ReferenceSet<IRep>::Remove(iRepId);

  iRep.Reference<DRep>::Remove();

  DeleteUnreferenced(dRepId);
}

//------------------------------------------------------------------------------
//! \brief Remove the referenced TransformationSequence object from the
//! indicated ImageRepresentation object.
//! @param iRepId The Identifier of the IRep the connection shall be removed
//! from.
//!
//! The connection between the indicated objects is removed.
//! Subsequently unreferenced objects may be deleted, depending on the
//! auto-deletion settings.
//!
//! If the Identifier is not in the valid range of the \link iRepS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! If the IRep does not reference a TSeq, this function has no effect.
//!
//! \sa Assign(IRepId const iRepId, TSeqId const tSeqId).
void Scene::RemoveTSeq(IRepId const iRepId)
{
  IRep& iRep = Get(iRepId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (!iRep.Reference<TSeq>::IsValid()) { return; } // Nothing to do.

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  TSeqId const tSeqId = iRep.Reference<TSeq>::Get();

  Get(tSeqId).ReferenceSet<IRep>::Remove(iRepId);

  iRep.Reference<TSeq>::Remove();

  DeleteUnreferenced(tSeqId);
}

//------------------------------------------------------------------------------
//! \brief Remove the referenced ObjectPointRepresentation object from the
//! indicated FeaturePointTrack object.
//! @param fTrkId The Identifier of the FTrk the connection shall be removed
//! from.
//!
//! The connection between the indicated objects is removed.
//! Subsequently unreferenced objects may be deleted, depending on the
//! auto-deletion settings.
//!
//! If the Identifier is not in the valid range of the \link fTrkS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! If the FTrk does not reference a ORep, this function has no effect.
//!
//! \sa Assign(FTrkId const fTrkId, ORepId const oRepId).
void Scene::RemoveORep(FTrkId const fTrkId)
{
  FTrk& fTrk = Get(fTrkId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (!fTrk.Reference<ORep>::IsValid()) { return; } // Nothing to do.

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  ORepId const oRepId = fTrk.Reference<ORep>::Get();

  Get(oRepId).ReferenceSet<FTrk>::Remove(fTrkId);

  fTrk.Reference<ORep>::Remove();

  DeleteUnreferenced(oRepId);
}

//------------------------------------------------------------------------------
//! \brief Remove the referenced TransformationSequence object from the
//! indicated FeaturePointTrack object.
//! @param fTrkId The Identifier of the FTrk the connection shall be removed
//! from.
//!
//! The connection between the indicated objects is removed.
//! Subsequently unreferenced objects may be deleted, depending on the
//! auto-deletion settings.
//!
//! If the Identifier is not in the valid range of the \link fTrkS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! If the FTrk does not reference a TSeq, this function has no effect.
//!
//! \sa Assign(FTrkId const fTrkId, TSeqId const tSeqId).
void Scene::RemoveTSeq(FTrkId const fTrkId)
{
  FTrk& fTrk = Get(fTrkId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (!fTrk.Reference<TSeq>::IsValid()) { return; } // Nothing to do.

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  TSeqId const tSeqId = fTrk.Reference<TSeq>::Get();

  Get(tSeqId).ReferenceSet<FTrk>::Remove(fTrkId);

  fTrk.Reference<TSeq>::Remove();

  DeleteUnreferenced(tSeqId);
}

//------------------------------------------------------------------------------
//! \brief Remove the referenced TransformationSequence object from the
//! indicated ObjectPointRepresentation object.
//! @param oRepId The Identifier of the IRep the connection shall be removed
//! from.
//!
//! The connection between the indicated objects is removed.
//! Subsequently unreferenced objects may be deleted, depending on the
//! auto-deletion settings.
//!
//! If the Identifier is not in the valid range of the \link oRepS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! If the ORep does not reference a TSeq, this function has no effect.
//!
//! \sa Assign(ORepId const oRepId, TSeqId const tSeqId).
void Scene::RemoveTSeq(ORepId const oRepId)
{
  ORep& oRep = Get(oRepId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (!oRep.Reference<TSeq>::IsValid()) { return; } // Nothing to do.

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  TSeqId const tSeqId = oRep.Reference<TSeq>::Get();

  Get(tSeqId).ReferenceSet<ORep>::Remove(oRepId);

  oRep.Reference<TSeq>::Remove();

  DeleteUnreferenced(tSeqId);
}

//------------------------------------------------------------------------------
//! \brief Remove the referenced WeightingMatrixRepresentation from the
//! indicated FeaturePointRepresentation.
//! @param fSetId The Identifier of the FSet the FRep resides in.
//! @param fRepId The Identifier of the FRep the WMat shall be removed from.
//!
//! The connection between the indicated objects is removed.
//! Subsequently unreferenced objects may be deleted, depending on the
//! auto-deletion settings.
//!
//! If either Identifier is not in the valid range of the container, an
//! OutOfRange exception will be thrown.
//!
//! If either Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! \sa
//! AssignWMat(FSetId const fSetId, FRepId const fRepId, WMatId const wMatId).
void Scene::RemoveWMat(FSetId const fSetId, FRepId const fRepId)
{
  FRep& fRep = Get(fSetId, fRepId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (!fRep.Reference<WMat>::IsValid()) { return; } // Nothing to do.

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  WMatId const wMatId = fRep.Reference<WMat>::Get();

  Get(wMatId).Remove(std::make_pair(fSetId, fRepId));

  fRep.Reference<WMat>::Remove();

  DeleteUnreferenced(wMatId);
}

//------------------------------------------------------------------------------
//! \brief Remove the referenced FeaturePointTrack from the
//! indicated FeaturePointRepresentation.
//! @param fSetId The Identifier of the FSet the FRep resides in.
//! @param fRepId The Identifier of the FRep the FTrk shall be removed from.
//!
//! The connection between the indicated objects is removed.
//! Subsequently unreferenced objects may be deleted, depending on the
//! auto-deletion settings.
//!
//! If either Identifier is not in the valid range of the container, an
//! OutOfRange exception will be thrown.
//!
//! If either Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! \sa
//! AssignFTrk(FSetId const fSetId, FRepId const fRepId, FTrkId const fTrkId).
void Scene::RemoveFTrk(FSetId const fSetId, FRepId const fRepId)
{
  FRep& fRep = Get(fSetId, fRepId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (!fRep.Reference<FTrk>::IsValid()) { return; } // Nothing to do.

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FTrkId const fTrkId = fRep.Reference<FTrk>::Get();

  Get(fTrkId).Remove(fSetId);

  fRep.Reference<FTrk>::Remove();

  DeleteUnreferenced(fTrkId);
}

//------------------------------------------------------------------------------
//! \brief Append the specified TransformationRepresentation to the indicated
//! TransformationSequence.
//! @param tSeqId The Identifier of the TSeq.
//! @param tRepId The Identifier of the TRep.
//!
//! If either Identifier is not in the valid range of the container, an
//! OutOfRange exception will be thrown.
//!
//! If either Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! \sa Insert(TSeqId const tSeqId, TRepId const tRepId, uint64 const position),
//! Remove(TSeqId const tSeqId, TRepId const tRepId, uint64 const position).
void Scene::Append(TSeqId const tSeqId, TRepId const tRepId)
{
  DLM_ASSERT(IsValid(tRepId), DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Get(tSeqId).Append(tRepId);
  Get(tRepId).Add   (tSeqId);
}

//------------------------------------------------------------------------------
//! \brief Insert the specified TransformationRepresentation into the indicated
//! TransformationSequence.
//! @param tSeqId The Identifier of the TSeq.
//! @param tRepId The Identifier of the TRep.
//! @param position The position in the TSeq the insertion shall occur at.
//!
//! If either Identifier is not in the valid range of the container, an
//! OutOfRange exception will be thrown.
//!
//! If either Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! If the position is not valid, an OutOfRange exception will be thrown.
//!
//! \sa Append(TSeqId const tSeqId, TRepId const tRepId),
//! Remove(TSeqId const tSeqId, TRepId const tRepId, uint64 const position).
void Scene::Insert(
  TSeqId const tSeqId, TRepId const tRepId, uint64 const position)
{
  DLM_ASSERT(IsValid(tRepId), DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Get(tSeqId).Insert(tRepId, position);
  Get(tRepId).Add   (tSeqId);
}

//------------------------------------------------------------------------------
//! \brief Remove the specified TransformationRepresentation from the indicated
//! TransformationSequence.
//! @param tSeqId The Identifier of the TSeq.
//! @param tRepId The Identifier of the TRep.
//! @param position The position in the TSeq the removal shall occur at.
//!
//! If either Identifier is not in the valid range of the container, an
//! OutOfRange exception will be thrown.
//!
//! If either Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! If the position is not valid, an OutOfRange exception will be thrown.
//!
//! \sa Append(TSeqId const tSeqId, TRepId const tRepId),
//! Append(TSeqId const tSeqId, TRepId const tRepId).
void Scene::Remove(
  TSeqId const tSeqId, TRepId const tRepId, uint64 const position)
{
  Get(tSeqId).Remove(tRepId, position);
  Get(tRepId).Remove(tSeqId);

  DeleteUnreferenced(tRepId);
}

//------------------------------------------------------------------------------
//! \brief Create a reference between the specified TransformationSequence
//! objects.
//! @param tSeqId The Identifier of the TSeq that shall reference the other
//! TSeq.
//! @param tsRefId The Identifier of the TSeq that shall be referenced by the
//! other object.
//!
//! A connection between the indicated objects is created; a previously existing
//! connection is removed.
//! Subsequently unreferenced objects may be deleted, depending on the
//! auto-deletion settings.
//!
//! If either Identifier is not in the valid range of the container, an
//! OutOfRange exception will be thrown.
//!
//! If either Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! If a reference cycle would be created, either by a TSeq referencing itself
//! or by an actual cycle, a RuntimeError exception is thrown.
//!
//! \sa RemoveTSRef(TSeqId const tSeqId).
void Scene::AssignTSRef(TSeqId const tSeqId, TSeqId const tsRefId)
{
  DLM_ANNOTATED_ASSERT(tsRefId != tSeqId, DLM_RUNTIME_ERROR,
    "Scene::AssignTDRef: would create reference cycle");

  //check for circular references
  for (TSeqId current = tsRefId; Get(current).Reference<TSeq>::IsValid();
       current = Get(current).Reference<TSeq>::Get())
  {
    DLM_ANNOTATED_ASSERT(current != tSeqId, DLM_RUNTIME_ERROR,
      "Scene::AssignTDRef: would create reference cycle");
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  TSeq& tSeq  = Get( tSeqId);
  TSeq& tDRef = Get(tsRefId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (tSeq.Reference<TSeq>::IsValid())
  {
    TSeqId const old = tSeq.Reference<TSeq>::Get();

    if (old == tsRefId) { return; } // Nothing to do.

    Get(old).ReferenceSet<TSeq>::Remove(tSeqId);

    DeleteUnreferenced(old);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  tSeq.Reference<TSeq>::Assign(tsRefId);

  tDRef.ReferenceSet<TSeq>::Add(tSeqId);
}

//------------------------------------------------------------------------------
//! \brief Remove the referenced TransformationSequence object from the
//! indicated TransformationSequence object.
//! @param tSeqId The Identifier of the TSeq the reference shall be removed
//! from.
//!
//! The connection between the indicated TSeq object and the TSeq object it
//! references is removed.
//! Subsequently unreferenced objects may be deleted, depending on the
//! auto-deletion settings.
//!
//! If the Identifier is not in the valid range of the \link tSeqS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! If the TSeq does not have a reference, this function has no effect.
//!
//! \sa AssignTSRef(TSeqId const tSeqId, TSeqId const tsRefId).
void Scene::RemoveTSRef(TSeqId const tSeqId)
{
  TSeq& tSeq = Get(tSeqId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (!tSeq.Reference<TSeq>::IsValid()) { return; } // Nothing to do.

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  TSeqId tsRefId = tSeq.Reference<TSeq>::Get();

  Get(tsRefId).ReferenceSet<TSeq>::Remove(tSeqId);

  tSeq.Reference<TSeq>::Remove();

  DeleteUnreferenced(tsRefId);
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
