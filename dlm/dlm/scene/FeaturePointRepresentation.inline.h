//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_scene_FeaturePointRepresentation_inline_h_
#define dlm_scene_FeaturePointRepresentation_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/scene/FeaturePointRepresentation.h"

//------------------------------------------------------------------------------
#include "dlm/scene/Reference.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/entity/FeaturePoint.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
inline FeaturePointRepresentation::FeaturePointRepresentation(
  BaseClass const& featurePoint)
  : BaseClass(featurePoint)
{}

//------------------------------------------------------------------------------
inline FeaturePointRepresentation::FeaturePointRepresentation(
  FeaturePointRepresentation const& other)
  : BaseClass    (other)
  , FTrkReference(other)
  , WMatReference(other)
{}

//==============================================================================
// Operators
//------------------------------------------------------------------------------
inline FeaturePointRepresentation& FeaturePointRepresentation::operator=(
  BaseClass const& featurePoint)
{
  BaseClass::operator=(featurePoint);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
inline FeaturePointRepresentation& FeaturePointRepresentation::operator=(
  FeaturePointRepresentation const& other)
{
  BaseClass::operator=(other);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FTrkReference::operator=(other);
  WMatReference::operator=(other);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//==============================================================================
// Convenience functions
//------------------------------------------------------------------------------
//! \brief Specify.
//! @return Specify.
//!
inline bool FeaturePointRepresentation::HasFTrk() const
{
  return this->FTrkReference::IsValid();
}

//------------------------------------------------------------------------------
//! \brief Specify.
//! @return Specify.
//!
//! This function is a shorthand alternative to calling
//! `this->dlm::Reference<dlm::FTrk>::Get`.
inline FTrkId FeaturePointRepresentation::FTrk() const
{
  return this->FTrkReference::Get();
}

//------------------------------------------------------------------------------
//! \brief Specify.
//! @return Specify.
//!
inline bool FeaturePointRepresentation::HasWMat() const
{
  return this->WMatReference::IsValid();
}

//------------------------------------------------------------------------------
//! \brief Specify.
//! @return Specify.
//!
inline WMatId FeaturePointRepresentation::WMat() const
{
  return this->WMatReference::Get();
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
