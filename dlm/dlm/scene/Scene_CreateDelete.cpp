//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/scene/Scene.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
//! \brief Create a new ImageRepresentation object.
//! @return The Identifier of the newly created IRep.
//!
//! \note If an IRepId has been \link Delete(IRepId const iRepId)
//! deleted\endlink, it will be reused.
//!
//! \sa Get(IRepId const iRepId), Delete(IRepId const iRepId), IRepS().
IRepId Scene::CreateIRep()
{
  return IRepId(iRepS_.PushBack(IRep()));
}

//------------------------------------------------------------------------------
//! \brief Create a new FeaturePointSet object.
//! @return The Identifier of the newly created FSet.
//!
//! \note If an FSetId has been \link Delete(FSetId const fSetId)
//! deleted\endlink, it will be reused.
//!
//! \sa Get(FSetId const fSetId), Delete(FSetId const fSetId), FSetS().
FSetId Scene::CreateFSet()
{
  return FSetId(fSetS_.PushBack(FSet()));
}

//------------------------------------------------------------------------------
//! \brief Create a new WeightingMatrixRepresentation object.
//! @return The Identifier of the newly created WMat.
//!
//! \note If an WMatId has been \link Delete(WMatId const wMatId)
//! deleted\endlink, it will be reused.
//!
//! \sa Get(WMatId const wMatId), Delete(wMatId const wMatId), WMatS().
WMatId Scene::CreateWMat()
{
  return WMatId(wMatS_.PushBack(WMat()));
}

//------------------------------------------------------------------------------
//! \brief Create a new FeaturePointTrack object.
//! @return The Identifier of the newly created FTrk.
//!
//! \note If an FTrkId has been \link Delete(FTrkId const fTrkId)
//! deleted\endlink, it will be reused.
//!
//! \sa Get(FTrkId const fTrkId), Delete(FTrkId const fTrkId), FTrkS().
FTrkId Scene::CreateFTrk()
{
  return FTrkId(fTrkS_.PushBack(FTrk()));
}

//------------------------------------------------------------------------------
//! \brief Create a new ProjectiveCameraRepresentation object.
//! @return The Identifier of the newly created PRep.
//!
//! \note If an PRepId has been \link Delete(PRepId const pRepId)
//! deleted\endlink, it will be reused.
//!
//! \sa Get(PRepId const pRepId), Delete(PRepId const pRepId), PRepS().
PRepId Scene::CreatePRep()
{
  return PRepId(pRepS_.PushBack(PRep()));
}

//------------------------------------------------------------------------------
//! \brief Create a new CameraRepresentation object.
//! @return The Identifier of the newly created CRep.
//!
//! \note If an CRepId has been \link Delete(CRepId const cRepId)
//! deleted\endlink, it will be reused.
//!
//! \sa Get(CRepId const cRepId), Delete(CRepId const cRepId), IRepS().
CRepId Scene::CreateCRep()
{
  return CRepId(cRepS_.PushBack(CRep()));
}

//------------------------------------------------------------------------------
//! \brief Create a new DistortionRepresentation object.
//! @return The Identifier of the newly created DRep.
//!
//! \note If an DRepId has been \link Delete(DRepId const dRepId)
//! deleted\endlink, it will be reused.
//!
//! \sa Get(DRepId const dRepId), Delete(DRepId const dRepId), DRepS().
DRepId Scene::CreateDRep()
{
  return DRepId(dRepS_.PushBack(DRep()));
}

//------------------------------------------------------------------------------
//! \brief Create a new ObjectPointRepresentation object.
//! @return The Identifier of the newly created ORep.
//!
//! \note If an ORepId has been \link Delete(ORepId const oRepId)
//! deleted\endlink, it will be reused.
//!
//! \sa Get(ORepId const oRepId), Delete(ORepId const oRepId), ORepS().
ORepId Scene::CreateORep()
{
  return ORepId(oRepS_.PushBack(ORep()));
}

//------------------------------------------------------------------------------
//! \brief Create a new TransformationSequence object.
//! @return The Identifier of the newly created TSeq.
//!
//! \note If an TSeqId has been \link Delete(TSeqId const tSeqId)
//! deleted\endlink, it will be reused.
//!
//! \sa Get(TSeqId const tSeqId), Delete(TSeqId const tSeqId), IRepS().
TSeqId Scene::CreateTSeq()
{
  return TSeqId(tSeqS_.PushBack(TSeq()));
}

//------------------------------------------------------------------------------
//! \brief Create a new TransformationRepresentation object.
//! @return The Identifier of the newly created TRep.
//!
//! \note If an TRepId has been \link Delete(TRepId const tRepId)
//! deleted\endlink, it will be reused.
//!
//! \sa Get(TRepId const tRepId), Delete(TRepId const tRepId), TRepS().
TRepId Scene::CreateTRep()
{
  return TRepId(tRepS_.PushBack(TRep()));
}

//------------------------------------------------------------------------------
//! \brief Create a new FeaturePointRepresentation object in the specified
//! FeaturePointSet.
//! @param fSetId The Identifier for the FSet the FRep shall be created in.
//! @return The Identifier of the newly created FRep.
//!
//! \note If an FRepId has been
//! \link Delete(FSetId const fSetId, FRepId const fRepId) deleted\endlink in
//! the specified FSet, it will be reused.
//!
//! If the FSetId is not in the valid range of the container, an OutOfRange
//! exception will be thrown.
//!
//! If the FSetId is not valid, a RuntimeError exception will be thrown.
//!
//! \sa Get(FSetId const fSetId, FRepId const fRepId),
//! Delete(FSetId const fSetId, FRepId const fRepId).
FRepId Scene::CreateFRep(FSetId const fSetId)
{
  return fSetS_.At(fSetId.Get()).CreateFRep();
}

//------------------------------------------------------------------------------
//! \brief Delete the indicated ImageRepresentation object.
//! @param iRepId The Identifier for the IRep that shall be deleted.
//!
//! The specified IRep will be \link Unreference(IRepId const iRepId)
//! unreferenced\endlink, effectively removing all connections within the Scene,
//! and then the associated storage object will be released.
//! Additional objects may be deleted, depending on the auto-deletion settings.
//!
//! \note The Identifier will be reused on subsequent calls to `CreateIRep()`.
//!
//! If the Identifier is not in the valid range of the \link iRepS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! \sa CreateIRep().
void Scene::Delete(IRepId const iRepId)
{
  Unreference(iRepId);

  iRepS_.Release(iRepId.Get());
}

//------------------------------------------------------------------------------
//! \brief Delete the indicated FeaturePointSet object.
//! @param fSetId The Identifier for the FSet that shall be deleted.
//!
//! The specified FSet will be \link Unreference(FSetId const fSetId)
//! unreferenced\endlink, as will be all FeaturePointRepresentation objects
//! contained in it, effectively removing all connections within the Scene,
//! and then the associated storage object will be released.
//! Additional objects may be deleted, depending on the auto-deletion settings.
//!
//! \note The Identifier will be reused on subsequent calls to `CreateFSet()`.
//!
//! If the Identifier is not in the valid range of the \link fSetS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! \sa CreateIRep(), Delete(FSetId const fSetId, FRepId const fRepId).
void Scene::Delete(FSetId const fSetId)
{
  Unreference(fSetId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  {
    FSet& fSet = Get(fSetId);

    for (uint64 i = 0; i < fSet.Range(); ++i)
    {
      if (!fSet.Active(i)) { continue; }

      Unreference(fSetId, FRepId(i));
    }

    fSet.Clear();
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  fSetS_.Release(fSetId.Get());
}

//------------------------------------------------------------------------------
//! \brief Delete the indicated WeightingMatrixRepresentation object.
//! @param wMatId The Identifier for the WMat that shall be deleted.
//!
//! The specified WMat will be \link Unreference(WMatId const wMatId)
//! unreferenced\endlink, effectively removing all connections within the Scene,
//! and then the associated storage object will be released.
//!
//! \note The Identifier will be reused on subsequent calls to `CreateWMat()`.
//!
//! If the Identifier is not in the valid range of the \link wMatS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! \sa CreateWMat().
void Scene::Delete(WMatId const wMatId)
{
  Unreference(wMatId);

  wMatS_.Release(wMatId.Get());
}

//------------------------------------------------------------------------------
//! \brief Delete the indicated FeaturePointTrack object.
//! @param fTrkId The Identifier for the FTrk that shall be deleted.
//!
//! The specified FTrk will be \link Unreference(FTrkId const fTrkId)
//! unreferenced\endlink, effectively removing all connections within the Scene,
//! and then the associated storage object will be released.
//! Additional objects may be deleted, depending on the auto-deletion settings.
//!
//! \note The Identifier will be reused on subsequent calls to `CreateFTrk()`.
//!
//! If the Identifier is not in the valid range of the \link fTrkS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! \sa CreateFTrk().
void Scene::Delete(FTrkId const fTrkId)
{
  Unreference(fTrkId);

  fTrkS_.Release(fTrkId.Get());
}

//------------------------------------------------------------------------------
//! \brief Delete the indicated ProjectiveCameraRepresentation object.
//! @param pRepId The Identifier for the PRep that shall be deleted.
//!
//! The specified PRep will be \link Unreference(PRepId const pRepId)
//! unreferenced\endlink, effectively removing all connections within the Scene,
//! and then the associated storage object will be released.
//!
//! \note The Identifier will be reused on subsequent calls to `CreatePRep()`.
//!
//! If the Identifier is not in the valid range of the \link pRepS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! \sa CreatePRep().
void Scene::Delete(PRepId const pRepId)
{
  Unreference(pRepId);

  pRepS_.Release(pRepId.Get());
}

//------------------------------------------------------------------------------
//! \brief Delete the indicated CameraRepresentation object.
//! @param cRepId The Identifier for the CRep that shall be deleted.
//!
//! The specified CRep will be \link Unreference(CRepId const cRepId)
//! unreferenced\endlink, effectively removing all connections within the Scene,
//! and then the associated storage object will be released.
//!
//! \note The Identifier will be reused on subsequent calls to `CreateCRep()`.
//!
//! If the Identifier is not in the valid range of the \link cRepS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! \sa CreateCRep().
void Scene::Delete(CRepId const cRepId)
{
  Unreference(cRepId);

  cRepS_.Release(cRepId.Get());
}

//------------------------------------------------------------------------------
//! \brief Delete the indicated DistortionRepresentation object.
//! @param dRepId The Identifier for the DRep that shall be deleted.
//!
//! The specified DRep will be \link Unreference(DRepId const dRepId)
//! unreferenced\endlink, effectively removing all connections within the Scene,
//! and then the associated storage object will be released.
//!
//! \note The Identifier will be reused on subsequent calls to `CreateDRep()`.
//!
//! If the Identifier is not in the valid range of the \link dRepS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! \sa CreateDRep().
void Scene::Delete(DRepId const dRepId)
{
  Unreference(dRepId);

  dRepS_.Release(dRepId.Get());
}

//------------------------------------------------------------------------------
//! \brief Delete the indicated ObjectPointRepresentation object.
//! @param oRepId The Identifier for the ORep that shall be deleted.
//!
//! The specified ORep will be \link Unreference(ORepId const oRepId)
//! unreferenced\endlink, effectively removing all connections within the Scene,
//! and then the associated storage object will be released.
//! Additional objects may be deleted, depending on the auto-deletion settings.
//!
//! \note The Identifier will be reused on subsequent calls to `CreateORep()`.
//!
//! If the Identifier is not in the valid range of the \link oRepS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! \sa CreateORep().
void Scene::Delete(ORepId const oRepId)
{
  Unreference(oRepId);

  oRepS_.Release(oRepId.Get());
}

//------------------------------------------------------------------------------
//! \brief Delete the indicated TransformationSequence object.
//! @param tSeqId The Identifier for the TSeq that shall be deleted.
//!
//! The specified TSeq will be \link Unreference(TSeqId const tSeqId)
//! unreferenced\endlink, effectively removing all connections within the Scene,
//! and then the associated storage object will be released.
//! Additional objects may be deleted, depending on the auto-deletion settings.
//!
//! \note The Identifier will be reused on subsequent calls to `CreateTSeq()`.
//!
//! If the Identifier is not in the valid range of the \link tSeqS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! \sa CreateTSeq().
void Scene::Delete(TSeqId const tSeqId)
{
  Unreference(tSeqId);

  tSeqS_.Release(tSeqId.Get());
}

//------------------------------------------------------------------------------
//! \brief Delete the indicated TransformationRepresentation object.
//! @param tRepId The Identifier for the TRep that shall be deleted.
//!
//! The specified TRep will be \link Unreference(TRepId const tRepId)
//! unreferenced\endlink, effectively removing all connections within the Scene,
//! and then the associated storage object will be released.
//!
//! \note The Identifier will be reused on subsequent calls to `CreateTRep()`.
//!
//! If the Identifier is not in the valid range of the \link tRepS_
//! container\endlink, an OutOfRange exception will be thrown.
//!
//! If the Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! \sa CreateTRep().
void Scene::Delete(TRepId const tRepId)
{
  Unreference(tRepId);

  tRepS_.Release(tRepId.Get());
}

//------------------------------------------------------------------------------
//! \brief Delete the indicated FeaturePointRepresentation object in the
//! specified FeaturePointSet.
//! @param fSetId The Identifier for the FSet the FRep shall be deleted from.
//! @param fRepId The Identifier for the FRep that shall be deleted.
//!
//! The specified FRep will be
//! \link Unreference(FSetId const fSetId, FRepId const fRepId)
//! unreferenced\endlink, effectively removing all connections within the Scene,
//! and then the associated storage object will be released.
//! Additional objects may be deleted, depending on the auto-deletion settings.
//!
//! \note The Identifier will be reused on subsequent calls to
//! `CreateFRep(FSetId const fSetId)`.
//!
//! If either Identifier is not in the valid range of the container, an
//! OutOfRange exception will be thrown.
//!
//! If either Identifier is not valid, a RuntimeError exception will be thrown.
//!
//! \sa CreateFRep(FSetId const fSetId).
void Scene::Delete(FSetId const fSetId, FRepId const fRepId)
{
  Unreference(fSetId, fRepId);

  Get(fSetId).Delete(fRepId);
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
