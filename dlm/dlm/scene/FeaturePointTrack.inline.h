//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_scene_FeaturePointTrack_inline_h_
#define dlm_scene_FeaturePointTrack_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/scene/FeaturePointTrack.h"

//------------------------------------------------------------------------------
#include "dlm/scene/Reference.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
inline FeaturePointTrack::FeaturePointTrack(BaseClass const& tracks)
  : BaseClass(tracks)
{}

//------------------------------------------------------------------------------
inline FeaturePointTrack::FeaturePointTrack(FeaturePointTrack const& other)
  : BaseClass    (other)
  , ORepReference(other)
  , TSeqReference(other)
{}

//==============================================================================
// Operators
//------------------------------------------------------------------------------
inline FeaturePointTrack& FeaturePointTrack::operator=(BaseClass const& tracks)
{
  BaseClass::operator=(tracks);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
inline FeaturePointTrack& FeaturePointTrack::operator=(
  FeaturePointTrack const& other)
{
  BaseClass::operator=(other);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  ORepReference::operator=(other);
  TSeqReference::operator=(other);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
inline bool FeaturePointTrack::Get(FSetId const fSetId, FRepId& id) const
{
  BaseClass::const_iterator it = BaseClass::find(fSetId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (it == BaseClass::end())
  {
    return false;
  }

  id = it->second;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return true;
}

//------------------------------------------------------------------------------
inline void FeaturePointTrack::Assign(FSetId const fSetId, FRepId const id)
{
  BaseClass::operator[](fSetId) = id;
}

//------------------------------------------------------------------------------
inline void FeaturePointTrack::Remove(FSetId const fSetId)
{
  auto it = BaseClass::find(fSetId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DLM_ASSERT(it != BaseClass::end(), DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  BaseClass::erase(it);
}

//==============================================================================
// Convenience functions
//------------------------------------------------------------------------------
//! \brief Specify.
//! @return Specify.
//!
inline bool FeaturePointTrack::HasORep() const
{
  return this->ORepReference::IsValid();
}

//------------------------------------------------------------------------------
//! \brief Specify.
//! @return Specify.
//!
inline ORepId FeaturePointTrack::ORep() const
{
  return this->ORepReference::Get();
}

//------------------------------------------------------------------------------
//! \brief Specify.
//! @return Specify.
//!
inline bool FeaturePointTrack::HasTSeq() const
{
  return this->TSeqReference::IsValid();
}

//------------------------------------------------------------------------------
//! \brief Specify.
//! @return Specify.
//!
inline TSeqId FeaturePointTrack::TSeq() const
{
  return this->TSeqReference::Get();
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
