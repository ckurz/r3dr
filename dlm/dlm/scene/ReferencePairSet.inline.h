//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_scene_ReferencePairSet_inline_h_
#define dlm_scene_ReferencePairSet_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/scene/ReferencePairSet.h"

//------------------------------------------------------------------------------
#include "dlm/core/Identifier.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
template <typename T, typename U>
inline ReferencePairSet<T, U>::ReferencePairSet(
  std::set<ElementType> const& set)
  : set_(set)
{}

//------------------------------------------------------------------------------
template <typename T, typename U>
inline ReferencePairSet<T, U>::ReferencePairSet(ReferencePairSet const& other)
  : set_(other.set_)
{}

//==============================================================================
// Operators
//------------------------------------------------------------------------------
template <typename T, typename U>
inline ReferencePairSet<T, U>& ReferencePairSet<T, U>::operator=(
  ReferencePairSet const& other)
{
  set_ = other.set_;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//==============================================================================
// Accessors
//------------------------------------------------------------------------------
template <typename T, typename U>
inline std::set<typename ReferencePairSet<T, U>::ElementType> const&
  ReferencePairSet<T, U>::Get() const
{
  return set_;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
template <typename T, typename U>
inline void ReferencePairSet<T, U>::Add(ElementType const id)
{
  auto it = set_.find(id);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DLM_ASSERT(it == set_.end(), DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  set_.insert(id);
}

//------------------------------------------------------------------------------
template <typename T, typename U>
inline void ReferencePairSet<T, U>::Remove(ElementType const id)
{
  auto it = set_.find(id);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DLM_ASSERT(it != set_.end(), DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  set_.erase(it);
}

//------------------------------------------------------------------------------
template <typename T, typename U>
inline void ReferencePairSet<T, U>::Clear()
{
  set_.clear();
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
