//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_scene_Identifiers_h_
#define dlm_scene_Identifiers_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/core/Identifier.h"
#include "dlm/core/Identifier.inline.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Forward declarations
//------------------------------------------------------------------------------
class ImageRepresentation;
class FeaturePointSet;
class FeaturePointRepresentation;
class WeightingMatrixRepresentation;
class FeaturePointTrack;
class ProjectiveCameraRepresentation;
class CameraRepresentation;
class DistortionRepresentation;
class ObjectPointRepresentation;
class TransformationSequence;
class TransformationRepresentation;

//==============================================================================
// Shorthand typedefs
//------------------------------------------------------------------------------
typedef ImageRepresentation            IRep; //!< \brief Shorthand.
typedef FeaturePointSet                FSet; //!< \brief Shorthand.
typedef FeaturePointRepresentation     FRep; //!< \brief Shorthand.
typedef WeightingMatrixRepresentation  WMat; //!< \brief Shorthand.
typedef FeaturePointTrack              FTrk; //!< \brief Shorthand.
typedef ProjectiveCameraRepresentation PRep; //!< \brief Shorthand.
typedef CameraRepresentation           CRep; //!< \brief Shorthand.
typedef DistortionRepresentation       DRep; //!< \brief Shorthand.
typedef ObjectPointRepresentation      ORep; //!< \brief Shorthand.
typedef TransformationSequence         TSeq; //!< \brief Shorthand.
typedef TransformationRepresentation   TRep; //!< \brief Shorthand.
 
//==============================================================================
// Identifier typedefs
//------------------------------------------------------------------------------
typedef Identifier<IRep> IRepId; //!< \brief Shorthand.
typedef Identifier<FSet> FSetId; //!< \brief Shorthand.
typedef Identifier<FRep> FRepId; //!< \brief Shorthand.
typedef Identifier<WMat> WMatId; //!< \brief Shorthand.
typedef Identifier<FTrk> FTrkId; //!< \brief Shorthand.
typedef Identifier<PRep> PRepId; //!< \brief Shorthand.
typedef Identifier<CRep> CRepId; //!< \brief Shorthand.
typedef Identifier<DRep> DRepId; //!< \brief Shorthand.
typedef Identifier<ORep> ORepId; //!< \brief Shorthand.
typedef Identifier<TSeq> TSeqId; //!< \brief Shorthand.
typedef Identifier<TRep> TRepId; //!< \brief Shorthand.

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
