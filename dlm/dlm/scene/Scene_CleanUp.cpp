//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/scene/Scene.h"

//------------------------------------------------------------------------------
#include "dlm/core/Exception.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
//! \brief Delete all currently unused FeaturePointSet objects in the Scene.
//!
//! \note An object is not currently used if it would be deleted by auto-delete.
//!
//! \warning Additional objects may be deleted based on the current auto-delete
//! settings.
void Scene::CleanUpFSetS()
{
  for (uint64 i = 0; i < fSetS_.Range(); ++i)
  {
    FSetId const fSetId = FSetId(i);

    if (IsValid(fSetId) && !Get(fSetId).Reference<IRep>::IsValid())
    {
      Unreference(fSetId);

      fSetS_.Release(i);
    }
  }
}

//------------------------------------------------------------------------------
//! \brief Delete all currently unused WeightingMatrixRepresentation objects in
//! the Scene.
//!
//! \note An object is not currently used if it would be deleted by auto-delete.
void Scene::CleanUpWMatS()
{
  for (uint64 i = 0; i < wMatS_.Range(); ++i)
  {
    WMatId const wMatId = WMatId(i);

    if (IsValid(wMatId) && Get(wMatId).ReferencePairSet<FSet, FRep>::Get().empty())
    {
      wMatS_.Release(i);
    }
  }
}

//------------------------------------------------------------------------------
//! \brief Delete all currently unused FeaturePointTrack objects in the Scene.
//!
//! \note An object is not currently used if it would be deleted by auto-delete.
//!
//! \warning Additional objects may be deleted based on the current auto-delete
//! settings.
void Scene::CleanUpFTrkS()
{
  for (unsigned i = 0; i < fTrkS_.Range(); ++i)
  {
    FTrkId const fTrkId = FTrkId(i);

    if (IsValid(fTrkId) && Get(fTrkId).empty())
    {
      Unreference(fTrkId);

      fTrkS_.Release(i);
    }
  }
}

//------------------------------------------------------------------------------
//! \brief Delete all currently unused ProjectiveCameraRepresentation objects in
//! the Scene.
//!
//! \note An object is not currently used if it would be deleted by auto-delete.
void Scene::CleanUpPRepS()
{
  for (uint64 i = 0; i < pRepS_.Range(); ++i)
  {
    PRepId const pRepId = PRepId(i);

    if (IsValid(pRepId) && !Get(pRepId).Reference<IRep>::IsValid())
    {
      pRepS_.Release(i);
    }
  }
}

//------------------------------------------------------------------------------
//! \brief Delete all currently unused CameraRepresentation objects in the
//! Scene.
//!
//! \note An object is not currently used if it would be deleted by auto-delete.
void Scene::CleanUpCRepS()
{
  for (uint64 i = 0; i < cRepS_.Range(); ++i)
  {
    CRepId const cRepId = CRepId(i);

    if (IsValid(cRepId) && Get(cRepId).ReferenceSet<IRep>::Get().empty())
    {
      cRepS_.Release(i);
    }
  }
}

//------------------------------------------------------------------------------
//! \brief Delete all currently unused DistortionRepresentation objects in
//! the Scene.
//!
//! \note An object is not currently used if it would be deleted by auto-delete.
void Scene::CleanUpDRepS()
{
  for (uint64 i = 0; i < dRepS_.Range(); ++i)
  {
    DRepId const dRepId = DRepId(i);

    if (IsValid(dRepId) && Get(dRepId).ReferenceSet<IRep>::Get().empty())
    {
      dRepS_.Release(i);
    }
  }
}

//------------------------------------------------------------------------------
//! \brief Delete all currently unused ObjectPointRepresentation objects in the
//! Scene.
//!
//! \note An object is not currently used if it would be deleted by auto-delete.
//!
//! \warning Additional objects may be deleted based on the current auto-delete
//! settings.
void Scene::CleanUpORepS()
{
  for (unsigned i = 0; i < oRepS_.Range(); ++i)
  {
    ORepId const oRepId = ORepId(i);

    if (IsValid(oRepId) && Get(oRepId).ReferenceSet<FTrk>::Get().empty())
    {
      Unreference(oRepId);

      oRepS_.Release(i);
    }
  }
}

//------------------------------------------------------------------------------
//! \brief Delete all currently unused TransformationSequence objects in the
//! Scene.
//!
//! \note An object is not currently used if it would be deleted by auto-delete.
//!
//! \warning Additional objects may be deleted based on the current auto-delete
//! settings.
void Scene::CleanUpTSeqS()
{
  for (uint64 i = 0; i < tSeqS_.Range(); ++i)
  {
    TSeqId const tSeqId = TSeqId(i);

    if (IsValid(tSeqId))
    {
      TSeq const& tSeq = Get(tSeqId);

      if (tSeq.ReferenceSet<IRep>::Get().empty() && 
          tSeq.ReferenceSet<FTrk>::Get().empty() && 
          tSeq.ReferenceSet<ORep>::Get().empty() &&
          tSeq.ReferenceSet<TSeq>::Get().empty())
      {
        Unreference(tSeqId);

        tSeqS_.Release(i);
      }
    }
  }
}

//------------------------------------------------------------------------------
//! \brief Delete all currently unused TransformationRepresenation objects in
//! the Scene.
//!
//! \note An object is not currently used if it would be deleted by auto-delete.
void Scene::CleanUpTRepS()
{
  for (uint64 i = 0; i < tRepS_.Range(); ++i)
  {
    TRepId const tRepId = TRepId(i);

    if (IsValid(tRepId) && Get(tRepId).TSeqM().empty())
    {
      tRepS_.Release(i);
    }
  }
}

//------------------------------------------------------------------------------
//! \brief Delete all currently unused objects in the Scene.
//!
//! \note An object is not currently used if it would be deleted by auto-delete.
//!
//! \warning Additional objects may be deleted based on the current auto-delete
//! settings.
void Scene::CleanUp()
{
  CleanUpFSetS();
  CleanUpWMatS();
  CleanUpFTrkS();
  CleanUpPRepS();
  CleanUpCRepS();
  CleanUpDRepS();
  CleanUpORepS();
  CleanUpTSeqS();
  CleanUpTRepS();
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
