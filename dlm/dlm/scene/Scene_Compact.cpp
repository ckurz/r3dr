//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/scene/Scene.h"

//------------------------------------------------------------------------------
#include "dlm/core/Exception.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
//! \brief Compact the ImageRepresentation \link iRepS_ storage\endlink.
//!
//! This function removes all released ImageRepresentation objects from storage.
//!
//! \warning This function potentially invalidates all IRepId items.
void Scene::CompactIRepS()
{
  if (iRepS_.IsCompact()) { return; }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  IRepId const validRange = IRepId(iRepS_.Range());

  std::vector<uint64> lut = iRepS_.Purge();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < pRepS_.Range(); ++i)
  {
    PRepId const pRepId = PRepId(i);

    if (!IsValid(pRepId)) { continue; }

    PRep& pRep = Get(pRepId);

    if (!pRep.Reference<IRep>::IsValid())  { continue; }

    IRepId const old = pRep.Reference<IRep>::Get();

    DLM_ASSERT(old < validRange, DLM_RUNTIME_ERROR);

    IRepId const iRepId = IRepId(lut.at(old.Get()));

    DLM_ASSERT(iRepId != Invalid, DLM_RUNTIME_ERROR);

    pRep.Reference<IRep>::Assign(iRepId);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < cRepS_.Range(); ++i)
  {
    if (!IsValid(CRepId(i))) { continue; }

    CRep& cRep = Get(CRepId(i));

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    auto iRepS = cRep.ReferenceSet<IRep>::Get();

    for (auto it = cRep.ReferenceSet<IRep>::Get().cbegin();
      it != cRep.ReferenceSet<IRep>::Get().cend(); ++it)
    {
      DLM_ASSERT(*it < validRange, DLM_RUNTIME_ERROR);

      IRepId iRepId = IRepId(lut.at(it->Get()));

      DLM_ASSERT(iRepId != Invalid, DLM_RUNTIME_ERROR);

      iRepS.insert(iRepId);
    }

    cRep.ReferenceSet<IRep>::set_.swap(iRepS);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < dRepS_.Range(); ++i)
  {
    if (!IsValid(DRepId(i))) { continue; }

    DRep& dRep = Get(DRepId(i));

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    std::set<IRepId> iRepS;

    for (auto it = dRep.ReferenceSet<IRep>::set_.cbegin(); it != dRep.ReferenceSet<IRep>::set_.cend(); ++it)
    {
      DLM_ASSERT(*it < validRange, DLM_RUNTIME_ERROR);

      IRepId iRepId = IRepId(lut.at(it->Get()));

      DLM_ASSERT(iRepId != Invalid, DLM_RUNTIME_ERROR);

      iRepS.insert(iRepId);
    }

    dRep.ReferenceSet<IRep>::set_.swap(iRepS);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < tSeqS_.Range(); ++i)
  {
    if (!IsValid(TSeqId(i))) { continue; }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    TSeq& tSeq = Get(TSeqId(i));

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    std::set<IRepId> iRepS;

    for (auto it = tSeq.ReferenceSet<IRep>::set_.cbegin(); it != tSeq.ReferenceSet<IRep>::set_.cend(); ++it)
    {
      DLM_ASSERT(*it < validRange, DLM_RUNTIME_ERROR);

      IRepId iRepId = IRepId(lut.at(it->Get()));

      DLM_ASSERT(iRepId != Invalid, DLM_RUNTIME_ERROR);

      iRepS.insert(iRepId);
    }

    tSeq.ReferenceSet<IRep>::set_.swap(iRepS);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < fSetS_.Range(); ++i)
  {
    if (!IsValid(FSetId(i))) { continue; }

    FSet& fSet = Get(FSetId(i));

    if (!fSet.Reference<IRep>::IsValid())  { continue; }

    IRepId const old = fSet.Reference<IRep>::Get();

    DLM_ASSERT(old < validRange, DLM_RUNTIME_ERROR);

    IRepId const iRepId = IRepId(lut.at(old.Get()));

    DLM_ASSERT(iRepId != Invalid, DLM_RUNTIME_ERROR);

    fSet.Reference<IRep>::Assign(iRepId);
  }
}

//------------------------------------------------------------------------------
//! \brief Compact the FeaturePointSet \link fSetS_ storage\endlink.
//!
//! This function removes all released FeaturePointSet objects from storage.
//!
//! \warning This function potentially invalidates all FSetId items.
void Scene::CompactFSetS()
{
  if (fSetS_.IsCompact()) { return; }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FSetId const validRange = FSetId(fSetS_.Range());

  std::vector<uint64> lut = fSetS_.Purge();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < iRepS_.Range(); ++i)
  {
    if (!IsValid(IRepId(i))) { continue; }

    IRep& iRep = Get(IRepId(i));

    if (!iRep.Reference<FSet>::IsValid())  { continue; }

    FSetId const old = iRep.Reference<FSet>::Get();

    DLM_ASSERT(old < validRange, DLM_RUNTIME_ERROR);

    FSetId const fSetId = FSetId(lut.at(old.Get()));

    DLM_ASSERT(fSetId != Invalid, DLM_RUNTIME_ERROR);

    iRep.Reference<FSet>::Assign(fSetId);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < wMatS_.Range(); ++i)
  {
    if (!IsValid(WMatId(i))) { continue; }
    
    WMat& wMat = Get(WMatId(i));
    
    ReferencePairSet<FSet, FRep> fRepS;
    
    {
      auto const& old = wMat.ReferencePairSet<FSet, FRep>::Get();
      
      for (auto it = old.cbegin(); it != old.cend(); ++it)
      {
        DLM_ASSERT(it->first < validRange, DLM_RUNTIME_ERROR);
        
        FSetId const fSetId = FSetId(lut.at(it->first.Get()));
        
        DLM_ASSERT(fSetId != Invalid, DLM_RUNTIME_ERROR);
        
        fRepS.Add(std::make_pair(fSetId, it->second));
      }
    }
    
    wMat.ReferencePairSet<FSet, FRep>::set_.swap(fRepS.set_);
  }
}

//------------------------------------------------------------------------------
//! \brief Compact the WeightingMatrixRepresentation \link wMatS_
//! storage\endlink.
//!
//! This function removes all released WeightingMatrixRepresentation objects
//! from storage.
//!
//! \warning This function potentially invalidates all WMatId items.
void Scene::CompactWMatS()
{
  if (wMatS_.IsCompact()) { return; }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  WMatId const validRange = WMatId(wMatS_.Range());
  
  auto lut = wMatS_.Purge();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < wMatS_.Range(); ++i)
  {
    auto const& fRepS = wMatS_.At(i).ReferencePairSet<FSet, FRep>::Get();
    
    for (auto it = fRepS.cbegin(); it != fRepS.cend(); ++it)
    {
      FRep& fRep = Get(it->first, it->second);
      
      DLM_ASSERT(fRep.Reference<WMat>::Get() < validRange, DLM_RUNTIME_ERROR);
      
      WMatId const wMatId = WMatId(lut.at(fRep.Reference<WMat>::Get().Get()));
      
      DLM_ASSERT(wMatId != Invalid, DLM_RUNTIME_ERROR);
      
      fRep.Reference<WMat>::Assign(wMatId);
    }
  }
}

//------------------------------------------------------------------------------
//! \brief Compact the FeaturePointTrack \link fTrkS_ storage\endlink.
//!
//! This function removes all released FeaturePointTrack objects from storage.
//!
//! \warning This function potentially invalidates all FTrkId items.
void Scene::CompactFTrkS()
{
  if (fTrkS_.IsCompact()) { return; }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FTrkId const validRange = FTrkId(fTrkS_.Range());

  std::vector<uint64> lut = fTrkS_.Purge();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < tSeqS_.Range(); ++i)
  {
    if (!IsValid(TSeqId(i))) { continue; }

    TSeq& tSeq = Get(TSeqId(i));

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    std::set<FTrkId> fTrkS;

    for (auto it = tSeq.ReferenceSet<FTrk>::set_.cbegin(); it != tSeq.ReferenceSet<FTrk>::set_.cend(); ++it)
    {
      DLM_ASSERT(*it < validRange, DLM_RUNTIME_ERROR);

      FTrkId const fTrkId = FTrkId(lut.at(it->Get()));

      DLM_ASSERT(fTrkId != Invalid, DLM_RUNTIME_ERROR);

      fTrkS.insert(fTrkId);
    }

    tSeq.ReferenceSet<FTrk>::set_.swap(fTrkS);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < oRepS_.Range(); ++i)
  {
    if (!IsValid(ORepId(i))) { continue; }

    ORep& oRep = Get(ORepId(i));

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    std::set<FTrkId> fTrkS;

    for (auto it = oRep.ReferenceSet<FTrk>::set_.cbegin(); it != oRep.ReferenceSet<FTrk>::set_.cend(); ++it)
    {
      DLM_ASSERT(*it < validRange, DLM_RUNTIME_ERROR);

      FTrkId fTrkId = FTrkId(lut.at(it->Get()));

      DLM_ASSERT(fTrkId != Invalid, DLM_RUNTIME_ERROR);

      fTrkS.insert(fTrkId);
    }

    oRep.ReferenceSet<FTrk>::set_.swap(fTrkS);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < fSetS_.Range(); ++i)
  {
    if (!IsValid(FSetId(i))) { continue; }

    FSet& fSet = Get(FSetId(i));

    for (uint64 j = 0; j < fSet.Range(); ++j)
    {
      if (!fSet.Active(j)) { continue; }

      FRep& fRep = fSet.At(j);

      // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      if (!fRep.Reference<FTrk>::IsValid()) { continue; }

      FTrkId const old = fRep.Reference<FTrk>::Get();

      DLM_ASSERT(old < validRange, DLM_RUNTIME_ERROR);

      FTrkId const fTrkId = FTrkId(lut.at(old.Get()));

      DLM_ASSERT(fTrkId != Invalid, DLM_RUNTIME_ERROR);

      fRep.Reference<FTrk>::Assign(fTrkId);
    }
  }
}

//------------------------------------------------------------------------------
//! \brief Compact the ProjectiveCameraRepresentation \link pRepS_
//! storage\endlink.
//!
//! This function removes all released ProjectiveCameraRepresentation objects
//! from storage.
//!
//! \warning This function potentially invalidates all PRepId items.
void Scene::CompactPRepS()
{
  if (pRepS_.IsCompact()) { return; }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  PRepId const validRange = PRepId(pRepS_.Range());

  std::vector<uint64> lut = pRepS_.Purge();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < iRepS_.Range(); ++i)
  {
    if (!IsValid(IRepId(i))) { continue; }

    IRep& iRep = Get(IRepId(i));

    if (!iRep.Reference<PRep>::IsValid())  { continue; }

    PRepId const old = iRep.Reference<PRep>::Get();

    DLM_ASSERT(old < validRange, DLM_RUNTIME_ERROR);

    PRepId const pRepId = PRepId(lut.at(old.Get()));

    DLM_ASSERT(pRepId != Invalid, DLM_RUNTIME_ERROR);

    iRep.Reference<PRep>::Assign(pRepId);
  }
}

//------------------------------------------------------------------------------
//! \brief Compact the CameraRepresentation \link cRepS_ storage\endlink.
//!
//! This function removes all released CameraRepresentation objects from
//! storage.
//!
//! \warning This function potentially invalidates all CRepId items.
void Scene::CompactCRepS()
{
  if (cRepS_.IsCompact()) { return; }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  CRepId const validRange = CRepId(cRepS_.Range());

  std::vector<uint64> lut = cRepS_.Purge();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < iRepS_.Range(); ++i)
  {
    if (!IsValid(IRepId(i))) { continue; }

    IRep& iRep = Get(IRepId(i));

    if (!iRep.Reference<CRep>::IsValid())  { continue; }

    CRepId const old = iRep.Reference<CRep>::Get();

    DLM_ASSERT(old < validRange, DLM_RUNTIME_ERROR);

    CRepId const cRepId = CRepId(lut.at(old.Get()));

    DLM_ASSERT(cRepId != Invalid, DLM_RUNTIME_ERROR);

    iRep.Reference<CRep>::Assign(cRepId);
  }
}

//------------------------------------------------------------------------------
//! \brief Compact the DistortionRepresentation \link dRepS_ storage\endlink.
//!
//! This function removes all released DistortionRepresentation objects from
//! storage.
//!
//! \warning This function potentially invalidates all DRepId items.
void Scene::CompactDRepS()
{
  if (dRepS_.IsCompact()) { return; }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DRepId const validRange = DRepId(dRepS_.Range());

  std::vector<uint64> lut = dRepS_.Purge();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < iRepS_.Range(); ++i)
  {
    if (!IsValid(IRepId(i))) { continue; }

    IRep& iRep = Get(IRepId(i));

    if (!iRep.Reference<DRep>::IsValid())  { continue; }

    DRepId const old = iRep.Reference<DRep>::Get();

    DLM_ASSERT(old < validRange, DLM_RUNTIME_ERROR);

    DRepId const dRepId = DRepId(lut.at(old.Get()));

    DLM_ASSERT(dRepId != Invalid, DLM_RUNTIME_ERROR);

    iRep.Reference<DRep>::Assign(dRepId);
  }
}

//------------------------------------------------------------------------------
//! \brief Compact the ObjectPointRepresentation \link oRepS_ storage\endlink.
//!
//! This function removes all released ObjectPointRepresentation objects from
//! storage.
//!
//! \warning This function potentially invalidates all ORepId items.
void Scene::CompactORepS()
{
  if (oRepS_.IsCompact()) { return; }

  ORepId const validRange = ORepId(oRepS_.Range());

  std::vector<uint64> lut = oRepS_.Purge();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < tSeqS_.Range(); ++i)
  {
    if (!IsValid(TSeqId(i))) { continue; }

    TSeq& tSeq = Get(TSeqId(i));

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    std::set<ORepId> oRepS;

    for (auto it = tSeq.ReferenceSet<ORep>::set_.cbegin(); it != tSeq.ReferenceSet<ORep>::set_.cend(); ++it)
    {
      DLM_ASSERT(*it < validRange, DLM_RUNTIME_ERROR);

      ORepId oRepId = ORepId(lut.at(it->Get()));

      DLM_ASSERT(oRepId != Invalid, DLM_RUNTIME_ERROR);

      oRepS.insert(oRepId);
    }

    tSeq.ReferenceSet<ORep>::set_.swap(oRepS);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < fTrkS_.Range(); ++i)
  {
    if (!IsValid(FTrkId(i))) { continue; }

    FTrk& fTrk = Get(FTrkId(i));

    if (!fTrk.Reference<ORep>::IsValid())  { continue; }

    ORepId const old = fTrk.Reference<ORep>::Get();

    DLM_ASSERT(old < validRange, DLM_RUNTIME_ERROR);

    ORepId const oRepId = ORepId(lut.at(old.Get()));

    DLM_ASSERT(oRepId != Invalid, DLM_RUNTIME_ERROR);

    fTrk.Reference<ORep>::Assign(oRepId);
  }
}

//------------------------------------------------------------------------------
//! \brief Compact the TransformationSequence \link tSeqS_ storage\endlink.
//!
//! This function removes all released TransformationSequence objects from
//! storage.
//!
//! \warning This function potentially invalidates all TSeqId items.
void Scene::CompactTSeqS()
{
  if (tSeqS_.IsCompact()) { return; }

  TSeqId const validRange = TSeqId(tSeqS_.Range());

  std::vector<uint64> lut = tSeqS_.Purge();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < iRepS_.Range(); ++i)
  {
    if (!IsValid(IRepId(i))) { continue; }

    IRep& iRep = Get(IRepId(i));

    if (!iRep.Reference<TSeq>::IsValid())  { continue; }

    TSeqId const old = iRep.Reference<TSeq>::Get();

    DLM_ASSERT(old < validRange, DLM_RUNTIME_ERROR);

    TSeqId const tSeqId = TSeqId(lut.at(old.Get()));

    DLM_ASSERT(tSeqId != Invalid, DLM_RUNTIME_ERROR);

    iRep.Reference<TSeq>::Assign(tSeqId);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < fTrkS_.Range(); ++i)
  {
    if (!IsValid(FTrkId(i))) { continue; }

    FTrk& fTrk = Get(FTrkId(i));

    if (!fTrk.Reference<TSeq>::IsValid())  { continue; }

    TSeqId const old = fTrk.Reference<TSeq>::Get();

    DLM_ASSERT(old < validRange, DLM_RUNTIME_ERROR);

    TSeqId const tSeqId = TSeqId(lut.at(old.Get()));

    DLM_ASSERT(tSeqId != Invalid, DLM_RUNTIME_ERROR);

    fTrk.Reference<TSeq>::Assign(tSeqId);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < oRepS_.Range(); ++i)
  {
    if (!IsValid(ORepId(i))) { continue; }

    ORep& oRep = Get(ORepId(i));

    if (!oRep.Reference<TSeq>::IsValid())  { continue; }

    TSeqId const old = oRep.Reference<TSeq>::Get();

    DLM_ASSERT(old < validRange, DLM_RUNTIME_ERROR);

    TSeqId const tSeqId = TSeqId(lut.at(old.Get()));

    DLM_ASSERT(tSeqId != Invalid, DLM_RUNTIME_ERROR);

    oRep.Reference<TSeq>::Assign(tSeqId);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < tSeqS_.Range(); ++i)
  {
    if (!IsValid(TSeqId(i))) { continue; }

    TSeq& tSeq = Get(TSeqId(i));

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    std::set<TSeqId> tSeqS;

    for (auto it = tSeq.ReferenceSet<TSeq>::set_.cbegin(); it != tSeq.ReferenceSet<TSeq>::set_.cend(); ++it)
    {
      DLM_ASSERT(*it < validRange, DLM_RUNTIME_ERROR);

      TSeqId tSeqId = TSeqId(lut.at(it->Get()));

      DLM_ASSERT(tSeqId != Invalid, DLM_RUNTIME_ERROR);

      tSeqS.insert(tSeqId);
    }

    tSeq.ReferenceSet<TSeq>::set_.swap(tSeqS);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    if (!tSeq.Reference<TSeq>::IsValid())  { continue; }

    TSeqId const old = tSeq.Reference<TSeq>::Get();

    DLM_ASSERT(old < validRange, DLM_RUNTIME_ERROR);

    TSeqId const tSeqId = TSeqId(lut.at(old.Get()));

    DLM_ASSERT(tSeqId != Invalid, DLM_RUNTIME_ERROR);

    tSeq.Reference<TSeq>::Assign(tSeqId);
  }
}

//------------------------------------------------------------------------------
//! \brief Compact the TRansformationRepresentation \link tRepS_
//! storage\endlink.
//!
//! This function removes all released TRansformationRepresentation objects from
//! storage.
//!
//! \warning This function potentially invalidates all TRepId items.
void Scene::CompactTRepS()
{
  if (tRepS_.IsCompact()) { return; }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  TRepId const validRange = TRepId(tRepS_.Range());

  std::vector<uint64> lut = tRepS_.Purge();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < tSeqS_.Range(); ++i)
  {
    if (!IsValid(TSeqId(i))) { continue; }

    TSeq& tSeq = Get(TSeqId(i));

    for (auto it = tSeq.begin(); it != tSeq.end(); ++it)
    {
      DLM_ASSERT(it->first < validRange, DLM_RUNTIME_ERROR);

      TRepId const tRepId = TRepId(lut.at(it->first.Get()));

      DLM_ASSERT(tRepId != Invalid, DLM_RUNTIME_ERROR);

      it->first = tRepId;
    }
  }
}

//------------------------------------------------------------------------------
//! \brief Compact the FeaturePointSet \link fSetS_ storage\endlink.
//!
//! This function removes all released FeaturePointSet objects from storage.
//!
//! \warning This function potentially invalidates all FSetId items.
void Scene::CompactFSet(FSetId const fSetId)
{
  FSet& fSet = Get(fSetId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (fSet.IsCompact()) { return; }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FRepId const validRange = FRepId(fSet.Range());

  std::vector<uint64> lut = fSet.Purge();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < wMatS_.Range(); ++i)
  {
    if (!IsValid(WMatId(i))) { continue; }
    
    WMat& wMat = Get(WMatId(i));
    
    ReferencePairSet<FSet, FRep> fRepS;
    
    {
      auto const& old = wMat.ReferencePairSet<FSet, FRep>::Get();
      
      for (auto it = old.cbegin(); it != old.cend(); ++it)
      {
        if (it->first != fSetId) { continue; }
      
        DLM_ASSERT(it->second < validRange, DLM_RUNTIME_ERROR);
        
        FRepId const fRepId = FRepId(lut.at(it->second.Get()));
        
        DLM_ASSERT(fRepId != Invalid, DLM_RUNTIME_ERROR);
        
        fRepS.Add(std::make_pair(it->first, fRepId));
      }
    }
    
    wMat.ReferencePairSet<FSet, FRep>::set_.swap(fRepS.set_);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < fTrkS_.Range(); ++i)
  {
    if (!IsValid(FTrkId(i))) { continue; }

    FTrk& fTrk = Get(FTrkId(i));

    FRepId old;

    if (!fTrk.Get(fSetId, old)) { continue; }

    DLM_ASSERT(old < validRange, DLM_RUNTIME_ERROR);

    FRepId const fRepId = FRepId(lut.at(old.Get()));

    DLM_ASSERT(fRepId != Invalid, DLM_RUNTIME_ERROR);

    fTrk.Assign(fSetId, fRepId);
  }
}

//------------------------------------------------------------------------------
//! \brief Compact the whole scene.
//!
//! This function removes all released objects from storage.
//!
//! \warning This function potentially invalidates all Identifier items.
void Scene::Compact()
{
  CompactIRepS();
  CompactFSetS();
  CompactWMatS();
  CompactFTrkS();
  CompactPRepS();
  CompactCRepS();
  CompactDRepS();
  CompactORepS();
  CompactTSeqS();
  CompactTRepS();

  for (uint64 i = 0; i < fSetS_.Range(); ++i)
  {
    CompactFSet(FSetId(i));
  }
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
