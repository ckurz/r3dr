//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_scene_DistortionRepresentation_inline_h_
#define dlm_scene_DistortionRepresentation_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/scene/DistortionRepresentation.h"

//------------------------------------------------------------------------------
#include "dlm/scene/ReferenceSet.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/flags/DistortionFlags.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/entity/Distortion.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
inline DistortionRepresentation::DistortionRepresentation(
  BaseClass const& distortion, FlagClass const& flags)
  : BaseClass (distortion)
  , FlagClass (flags     )
{}

//------------------------------------------------------------------------------
inline DistortionRepresentation::DistortionRepresentation(
  DistortionRepresentation const& other)
  : BaseClass         (other)
  , FlagClass         (other)
  , ReferenceSet<IRep>(other)
{}

//==============================================================================
// Operators
//------------------------------------------------------------------------------
inline DistortionRepresentation& DistortionRepresentation::operator=(
  BaseClass const& distortion)
{
  BaseClass::operator=(distortion);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
inline DistortionRepresentation& DistortionRepresentation::operator=(
  FlagClass const& flags)
{
  FlagClass::operator=(flags);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
inline DistortionRepresentation& DistortionRepresentation::operator=(
  DistortionRepresentation const& other)
{
  BaseClass::operator=(other);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  ReferenceSet<IRep>::operator=(other);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//==============================================================================
// Convenience functions
//------------------------------------------------------------------------------
inline DistortionRepresentation::IRepSet const&
  DistortionRepresentation::IRepS() const
{
  return this->ReferenceSet<IRep>::Get();
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
