//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_scene_ObjectPointRepresentation_h_
#define dlm_scene_ObjectPointRepresentation_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/scene/Reference.h"
#include "dlm/scene/ReferenceSet.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/Identifiers.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/flags/ObjectPointFlags.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/entity/ObjectPoint.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/ArrayObjectTable.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// ObjectPointRepresentation class
//------------------------------------------------------------------------------
class ObjectPointRepresentation : public ObjectPoint
                                , public ObjectPointFlags
                                , public Reference   <dlm::TSeq>
                                , public ReferenceSet<FTrk>
{
public:
  //----------------------------------------------------------------------------
  // Friends
  //----------------------------------------------------------------------------
  friend class BinarySerializer;   //!< \brief For serialization.
  friend class BinaryDeserializer; //!< \brief For deserialization.
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  friend class Scene; //!< \brief The Scene is allowed to change the state.
  friend class ArrayObjectTable<ObjectPointRepresentation>; //!< For construction.

  //----------------------------------------------------------------------------
  // Nested
  //----------------------------------------------------------------------------
  typedef ObjectPoint      BaseClass;
  typedef ObjectPointFlags FlagClass;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  typedef Reference<dlm::TSeq> TSeqReference;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  typedef std::set<FTrkId> FTrkSet;

  //----------------------------------------------------------------------------
  // using-declarations to avoid ambiguity
  // ObjectPoint      is a Vector<3, double>
  // ObjectPointFlags is a Vector<3, bool  >
  //----------------------------------------------------------------------------
  using BaseClass::operator+;
  using BaseClass::operator-;
  using BaseClass::operator+=;
  using BaseClass::operator-=;
  using BaseClass::operator*;
  using BaseClass::operator/;
  using BaseClass::operator*=;
  using BaseClass::operator/=;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  using BaseClass::At;
  using BaseClass::X;
  using BaseClass::Y;
  using BaseClass::Z;
  using BaseClass::W;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  using BaseClass::Dot;
  using BaseClass::Cross;
  using BaseClass::Magnitude;
  using BaseClass::Normalize;
  using BaseClass::Normalized;

  //----------------------------------------------------------------------------
  // Operators
  //----------------------------------------------------------------------------
  ObjectPointRepresentation& operator=(BaseClass const& objectPoint);
  ObjectPointRepresentation& operator=(FlagClass const& flags      );

  //----------------------------------------------------------------------------
  // Convenience functions
  //----------------------------------------------------------------------------
  //! @name Convenience functions
  //!
  //! @{
  bool HasTSeq() const;
  TSeqId  TSeq() const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FTrkSet const& FTrkS() const;
  //! @}

private:
  //----------------------------------------------------------------------------
  // Constructors
  //----------------------------------------------------------------------------
  ObjectPointRepresentation(BaseClass const& objectPoint = BaseClass(),
                            FlagClass const& flags       = FlagClass());
  ObjectPointRepresentation(ObjectPointRepresentation const& other);

  //----------------------------------------------------------------------------
  // Operators
  //----------------------------------------------------------------------------
  ObjectPointRepresentation& operator=(ObjectPointRepresentation const& other);
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
