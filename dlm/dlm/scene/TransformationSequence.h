//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_scene_TransformationSequence_h_
#define dlm_scene_TransformationSequence_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/scene/Reference.h"
#include "dlm/scene/ReferenceSet.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/Identifiers.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/ArrayObjectTable.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <vector>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// TransformationSequence class
//------------------------------------------------------------------------------
class TransformationSequence : private std::vector<std::pair<TRepId, bool> >
                             , public ReferenceSet<IRep>
                             , public ReferenceSet<FTrk>
                             , public ReferenceSet<ORep>
                             , public ReferenceSet<TSeq>
                             , public Reference<TSeq>
{
public:
  //----------------------------------------------------------------------------
  // Friends
  //----------------------------------------------------------------------------
  friend class BinarySerializer;   //!< \brief For serialization.
  friend class BinaryDeserializer; //!< \brief For deserialization.
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  friend class Scene; //!< \brief The Scene is allowed to change the state.
  friend class ArrayObjectTable<TransformationSequence>; //!< For construction.

  //----------------------------------------------------------------------------
  // Nested
  //----------------------------------------------------------------------------
  typedef std::vector<std::pair<TRepId, bool> > BaseClass;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  typedef std::set<IRepId> IRepSet;
  typedef std::set<FTrkId> FTrkSet;
  typedef std::set<ORepId> ORepSet;
  typedef std::set<TSeqId> TSeqSet;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  typedef Reference<TSeq> TSReference;

  //----------------------------------------------------------------------------
  // Public interface
  //----------------------------------------------------------------------------
  using BaseClass::const_iterator;
  using BaseClass::const_reverse_iterator;
  using BaseClass::empty;
  using BaseClass::size;
  using BaseClass::begin;
  using BaseClass::end;
  using BaseClass::rbegin;
  using BaseClass::rend;
  using BaseClass::cbegin;
  using BaseClass::cend;
  using BaseClass::crbegin;
  using BaseClass::crend;
  using BaseClass::at;

  //----------------------------------------------------------------------------
  // Member functions
  //----------------------------------------------------------------------------
  // The positions of the transformation representations are returned in reverse
  // order, as they would be applied to an object point. That is, for a given
  // transformation representation, the result would be 7 - 5 - 2 - ...
  // This facilitates deletion.
  std::vector<uint64> GetTRepPositions(TRepId const id) const;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void SetSpecial(uint64 const position, bool const value);

  //----------------------------------------------------------------------------
  // Convenience functions
  //----------------------------------------------------------------------------
  //! @name Convenience functions
  //!
  //! @{
  IRepSet const& IRepS() const;
  TSeqSet const& TSeqS() const;
  FTrkSet const& FTrkS() const;
  ORepSet const& ORepS() const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool HasTSRef() const;
  TSeqId  TSRef() const;
  //! @}

private:
  //----------------------------------------------------------------------------
  // Constructors
  //----------------------------------------------------------------------------
  TransformationSequence(BaseClass              const& baseClass = BaseClass());
  TransformationSequence(TransformationSequence const& other                  );

  //----------------------------------------------------------------------------
  // Operators
  //----------------------------------------------------------------------------
  TransformationSequence& operator=(BaseClass              const& baseClass);
  TransformationSequence& operator=(TransformationSequence const& other    );

  //----------------------------------------------------------------------------
  // Member functions
  //----------------------------------------------------------------------------
  void Append(TRepId const id,                        bool const special = false);
  void Insert(TRepId const id, uint64 const position, bool const special = false);
  void Remove(TRepId const id, uint64 const position);
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
