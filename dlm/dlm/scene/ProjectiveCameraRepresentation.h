//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_scene_ProjectiveCameraRepresentation_h_
#define dlm_scene_ProjectiveCameraRepresentation_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/scene/Reference.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/Identifiers.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/flags/ProjectiveCameraFlags.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/entity/ProjectiveCamera.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/ArrayObjectTable.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <set>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// ProjectiveCameraRepresentation class
//------------------------------------------------------------------------------
class ProjectiveCameraRepresentation : public ProjectiveCamera
                                     , public ProjectiveCameraFlags
                                     , public Reference<IRep>
{
public:
  //----------------------------------------------------------------------------
  // Friends
  //----------------------------------------------------------------------------
  friend class BinarySerializer;   //!< \brief For serialization.
  friend class BinaryDeserializer; //!< \brief For deserialization.
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  friend class Scene; //!< \brief The Scene is allowed to change the state.
  friend class ArrayObjectTable<ProjectiveCameraRepresentation>; //!< For construction.

  //----------------------------------------------------------------------------
  // Nested
  //----------------------------------------------------------------------------
  typedef ProjectiveCamera      BaseClass;
  typedef ProjectiveCameraFlags FlagClass;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  typedef Reference<ImageRepresentation> IRepReference;

  using BaseClass::operator*=;
  using BaseClass::operator();

  //----------------------------------------------------------------------------
  // Operators
  //----------------------------------------------------------------------------
  ProjectiveCameraRepresentation& operator=(BaseClass const& camera);
  ProjectiveCameraRepresentation& operator=(FlagClass const& flags );

  //----------------------------------------------------------------------------
  // Convenience functions
  //----------------------------------------------------------------------------
  //! @name Convenience functions
  //!
  //! @{
  bool HasIRep() const;
  IRepId  IRep() const;
  //! @}

private:
  //----------------------------------------------------------------------------
  // Constructors
  //----------------------------------------------------------------------------
  ProjectiveCameraRepresentation(
    BaseClass const& camera = BaseClass(),
    FlagClass const& flags  = FlagClass());
  ProjectiveCameraRepresentation(ProjectiveCameraRepresentation const& other);

  //----------------------------------------------------------------------------
  // Operators
  //----------------------------------------------------------------------------
  ProjectiveCameraRepresentation& operator=(
    ProjectiveCameraRepresentation const& other);
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
