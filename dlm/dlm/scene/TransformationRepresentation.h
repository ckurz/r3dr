//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_scene_TransformationRepresentation_h_
#define dlm_scene_TransformationRepresentation_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/core/ArrayObjectTable.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/entity/Transformation.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/flags/TransformationFlags.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/Identifiers.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <map>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// TransformationRepresentation class
//------------------------------------------------------------------------------
class TransformationRepresentation : public Transformation
                                   , public TransformationFlags
{
public:
  //----------------------------------------------------------------------------
  // Friends
  //----------------------------------------------------------------------------
  friend class BinarySerializer;   //!< \brief For serialization.
  friend class BinaryDeserializer; //!< \brief For deserialization.
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  friend class Scene; //!< \brief The Scene is allowed to change the state.
  friend class ArrayObjectTable<TransformationRepresentation>; //!< For construction.

  //----------------------------------------------------------------------------
  // Nested
  //----------------------------------------------------------------------------
  typedef Transformation      BaseClass;
  typedef TransformationFlags FlagClass;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  typedef std::map<TSeqId, uint64> TSeqMap;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // to avoid getting "request for member 'operator*' is ambiguous" on g++ 4.4.5
  using BaseClass::operator*;

  //----------------------------------------------------------------------------
  // Operators
  //----------------------------------------------------------------------------
  TransformationRepresentation& operator=(BaseClass const& transformation);
  TransformationRepresentation& operator=(FlagClass const& flags         );

  //----------------------------------------------------------------------------
  // Accessors
  //----------------------------------------------------------------------------
  TSeqMap const& TSeqM() const;

private:
  //----------------------------------------------------------------------------
  // Constructors
  //----------------------------------------------------------------------------
  TransformationRepresentation(BaseClass const& transformation = BaseClass(),
                               FlagClass const& flags          = FlagClass());
  TransformationRepresentation(TransformationRepresentation const& other);

  //----------------------------------------------------------------------------
  // Operators
  //----------------------------------------------------------------------------
  TransformationRepresentation& operator=(
    TransformationRepresentation const& other);

  //----------------------------------------------------------------------------
  // Member functions
  //----------------------------------------------------------------------------
  void    Add(TSeqId const id, uint64 const count = 1);
  void Remove(TSeqId const id, uint64 const count = 1);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void  Clear();

  //----------------------------------------------------------------------------
  // Member variables
  //----------------------------------------------------------------------------
  TSeqMap tSeqM_;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
