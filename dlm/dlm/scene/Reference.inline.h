//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_scene_Reference_inline_h_
#define dlm_scene_Reference_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/scene/Reference.h"

//------------------------------------------------------------------------------
#include "dlm/core/Identifier.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
template <typename T>
inline Reference<T>::Reference(Identifier<T> const id)
  : id_(id)
{}

//------------------------------------------------------------------------------
template <typename T>
inline Reference<T>::Reference(Reference const& other)
  : id_(other.id_)
{}

//==============================================================================
// Operators
//------------------------------------------------------------------------------
template <typename T>
inline Reference<T>& Reference<T>::operator=(Reference const& other)
{
  id_ = other.id_;

  return *this;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
//! \brief Specify.
template <typename T>
inline bool Reference<T>::IsValid() const
{
  return id_ != Identifier<T>();
}

//------------------------------------------------------------------------------
//! \brief Specify.
template <typename T>
inline Identifier<T> Reference<T>::Get() const
{
  DLM_ASSERT(id_ != Identifier<T>(), DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return id_;
}

//------------------------------------------------------------------------------
template <typename T>
inline void Reference<T>::Assign(Identifier<T> const id)
{
  id_ = id;
}

//------------------------------------------------------------------------------
template <typename T>
inline void Reference<T>::Remove()
{
  id_ = Identifier<T>();
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
