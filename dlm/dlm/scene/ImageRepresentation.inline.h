//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_scene_ImageRepresentation_inline_h_
#define dlm_scene_ImageRepresentation_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/scene/ImageRepresentation.h"

//------------------------------------------------------------------------------
#include "dlm/scene/Reference.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/entity/Image.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
inline ImageRepresentation::ImageRepresentation(BaseClass const& image)
  : BaseClass      (image  )
  , sequenceNumber_(Invalid)
{}

//------------------------------------------------------------------------------
inline ImageRepresentation::ImageRepresentation(
  ImageRepresentation const& other)
  : BaseClass    (other)
  , FSetReference(other)
  , PRepReference(other)
  , CRepReference(other)
  , DRepReference(other)
  , TSeqReference(other)
  , filename_      (other.filename_)
  , sequenceNumber_(other.sequenceNumber_)
{}

//==============================================================================
// Operators
//------------------------------------------------------------------------------
inline ImageRepresentation& ImageRepresentation::operator=(
  BaseClass const& image)
{
  BaseClass::operator=(image);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
inline ImageRepresentation& ImageRepresentation::operator=(
  ImageRepresentation const& other)
{
  BaseClass::operator=(other);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FSetReference::operator=(other);
  PRepReference::operator=(other);
  CRepReference::operator=(other);
  DRepReference::operator=(other);
  TSeqReference::operator=(other);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  filename_       = other.filename_;

  sequenceNumber_ = other.sequenceNumber_;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//==============================================================================
// Convenience functions
//------------------------------------------------------------------------------
//! \brief Determine whether a Reference to a FeaturePointSet is available.
//! @return A bool value indicating whether a Reference to a FeaturePointSet is
//! available.
//!
//! This function is a shorthand alternative to calling
//! `dlm::Reference<dlm::FSet>::IsValid() const` on the given object.
//! `dlm::Reference<dlm::FeaturePointSet>::IsValid() const` on the given object.
inline bool ImageRepresentation::HasFSet() const
{
  return FSetReference::IsValid();
}

//------------------------------------------------------------------------------
//! \brief Get the Reference to the FeaturePointSet.
//! @return The FSetId of the referenced FeaturePointSet.
//!
//! This function is a shorthand alternative to calling
//! `dlm::Reference<dlm::FSet>::Get` on the given object.
//! This function is a shorthand alternative to calling
//! `dlm::Reference<dlm::FeaturePointSet>::Get` on the given object.
inline FSetId ImageRepresentation::FSet() const
{
  return FSetReference::Get();
}

//------------------------------------------------------------------------------
//! \brief Determine whether a Reference to a ProjectiveCamera is available.
//! @return A bool value indicating whether a Reference to a ProjectiveCamera is
//! available.
//!
//! This function is a shorthand alternative to calling
//! `dlm::Reference<dlm::PRep>::IsValid()` on the given object.
inline bool ImageRepresentation::HasPRep() const
{
  return PRepReference::IsValid();
}

//------------------------------------------------------------------------------
//! \brief Get the Reference to the ProjectiveCamera.
//! @return The PRepId of the referenced ProjectiveCamera.
//!
//! This function is a shorthand alternative to calling
//! `dlm::Reference<dlm::PRep>::Get()` on the given object.
inline PRepId ImageRepresentation::PRep() const
{
  return PRepReference::Get();
}

//------------------------------------------------------------------------------
//! \brief Determine whether a Reference to a Camera is available.
//! @return A bool value indicating whether a Reference to a Camera is
//! available.
//!
//! This function is a shorthand alternative to calling
//! `dlm::Reference<dlm::CRep>::IsValid()` on the given object.
inline bool ImageRepresentation::HasCRep() const
{
  return CRepReference::IsValid();
}

//------------------------------------------------------------------------------
//! \brief Get the Reference to the Camera.
//! @return The CRepId of the referenced Camera.
//!
//! This function is a shorthand alternative to calling
//! `dlm::Reference<dlm::CRep>::Get()` on the given object.
inline CRepId ImageRepresentation::CRep() const
{
  return CRepReference::Get();
}

//------------------------------------------------------------------------------
//! \brief Determine whether a Reference to a Distortion is available.
//! @return A bool value indicating whether a Reference to a Distortion is
//! available.
//!
//! This function is a shorthand alternative to calling
//! `dlm::Reference<dlm::DRep>::IsValid()` on the given object.
inline bool ImageRepresentation::HasDRep() const
{
  return DRepReference::IsValid();
}

//------------------------------------------------------------------------------
//! \brief Get the Reference to the Distortion.
//! @return The DRepId of the referenced Distortion.
//!
//! This function is a shorthand alternative to calling
//! `dlm::Reference<dlm::DRep>::Get()` on the given object.
inline DRepId ImageRepresentation::DRep() const
{
  return DRepReference::Get();
}

//------------------------------------------------------------------------------
//! \brief Determine whether a Reference to a TransformationSequence is
//! available.
//! @return A bool value indicating whether a Reference to a
//! TransformationSequence is available.
//!
//! This function is a shorthand alternative to calling
//! `dlm::Reference<dlm::TSeq>::IsValid()` on the given object.
inline bool ImageRepresentation::HasTSeq() const
{
  return TSeqReference::IsValid();
}

//------------------------------------------------------------------------------
//! \brief Get the Reference to the TransformationSequence.
//! @return The TSeqId of the referenced TransformationSequence.
//!
//! This function is a shorthand alternative to calling
//! `dlm::Reference<dlm::FSet>::Get()` on the given object.
inline TSeqId ImageRepresentation::TSeq() const
{
  return TSeqReference::Get();
}

//==============================================================================
// Accessors/Mutators
//------------------------------------------------------------------------------
//! \brief The accessor method for the stored \link filename_ filename \endlink.
//! @return A \b const \b reference to the stored \link filename_ filename
//! \endlink.
//! \deprecated This function will be removed in an upcoming release.
inline std::string const& ImageRepresentation::Filename() const
{
  return filename_;
}

//------------------------------------------------------------------------------
//! \brief The mutator method for the stored \link filename_ filename \endlink.
//! @return A \b reference to the stored \link filename_ filename \endlink.
//! \deprecated This function will be removed in an upcoming release.
inline std::string& ImageRepresentation::Filename()
{
  return filename_;
}

//------------------------------------------------------------------------------
//! \brief The accessor method for the stored \link sequenceNumber_ sequence
//! number \endlink.
//! @return The stored \link sequenceNumber_ sequence number \endlink.
//! \deprecated This function will be removed in an upcoming release.
inline uint64 ImageRepresentation::SequenceNumber() const
{
  return sequenceNumber_;
}

//------------------------------------------------------------------------------
//! \brief The mutator method for the stored \link sequenceNumber_ sequence
//! number \endlink.
//! @return A \b reference to the stored \link sequenceNumber_ sequence number
//! \endlink.
//! \deprecated This function will be removed in an upcoming release.
inline uint64& ImageRepresentation::SequenceNumber()
{
  return sequenceNumber_;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
