//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_scene_DistortionRepresentation_h_
#define dlm_scene_DistortionRepresentation_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/scene/ReferenceSet.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/scene/Identifiers.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/flags/DistortionFlags.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/entity/Distortion.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/ArrayObjectTable.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
//! \brief Specify.
// DistortionRepresentation class
//------------------------------------------------------------------------------
class DistortionRepresentation : public Distortion
                               , public DistortionFlags
                               , public ReferenceSet<IRep>
{
public:
  //----------------------------------------------------------------------------
  // Friends
  //----------------------------------------------------------------------------
  friend class BinarySerializer;   //!< \brief For serialization.
  friend class BinaryDeserializer; //!< \brief For deserialization.
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  friend class Scene; //!< \brief The Scene is allowed to change the state.
  friend class ArrayObjectTable<DistortionRepresentation>; //!< For construction.

  //----------------------------------------------------------------------------
  // Nested
  //----------------------------------------------------------------------------
  typedef Distortion      BaseClass;
  typedef DistortionFlags FlagClass;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  typedef std::set<IRepId> IRepSet;

  using BaseClass::operator();

  //----------------------------------------------------------------------------
  // Operators
  //----------------------------------------------------------------------------
  DistortionRepresentation& operator=(BaseClass const& distortion);
  DistortionRepresentation& operator=(FlagClass const& flags     );

  //----------------------------------------------------------------------------
  // Convenience functions
  //----------------------------------------------------------------------------
  //! @name Convenience functions
  //!
  //! @{
  IRepSet const& IRepS() const;
  //! @}

private:
  //----------------------------------------------------------------------------
  // Constructors
  //----------------------------------------------------------------------------
  DistortionRepresentation(BaseClass const& distortion = BaseClass(),
                           FlagClass const& flags      = FlagClass());
  DistortionRepresentation(DistortionRepresentation const& other);

  //----------------------------------------------------------------------------
  // Operators
  //----------------------------------------------------------------------------
  DistortionRepresentation& operator=(DistortionRepresentation const& other);
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
