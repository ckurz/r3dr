//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_sceneSceneAssistant_inline_h_
#define dlm_sceneSceneAssistant_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/scene/SceneAssistant.h"

//------------------------------------------------------------------------------
#include "dlm/math/VectorFunctions.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Static member functions
//------------------------------------------------------------------------------
inline Vector3d SceneAssistant::Transform(
  Scene const& scene, FTrkId const fTrkId)
{
  FTrk const& fTrk = scene.Get(fTrkId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Vector3d point = Transform(scene, fTrk.Reference<ORep>::Get());

  if (fTrk.Reference<TSeq>::IsValid())
  {
    point = Transform(point, scene, fTrk.Reference<TSeq>::Get());
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return point;
}

//------------------------------------------------------------------------------
inline Vector3d SceneAssistant::Transform(
  Scene const& scene, ORepId const oRepId)
{
  ORep const& oRep = scene.Get(oRepId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Vector3d point = oRep;

  if (oRep.Reference<TSeq>::IsValid())
  {
    point = Transform(point, scene, oRep.Reference<TSeq>::Get());
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return point;
}

//------------------------------------------------------------------------------
inline Vector3d SceneAssistant::Transform(
  Vector3d const& point, Scene const& scene, TSeqId const tSeqId)
{
  Vector3d transformed = point;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  TSeqId current = tSeqId;

  while (true)
  {
    TSeq const& tSeq = scene.Get(current);

    for (auto it = tSeq.crbegin(); it != tSeq.crend(); ++it)
    {
      transformed = scene.Get(it->first) * transformed;
    }

    if (!tSeq.Reference<TSeq>::IsValid()) { break; }

    current = tSeq.Reference<TSeq>::Get();
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return transformed;
}

//------------------------------------------------------------------------------
inline Vector2d SceneAssistant::Project(
  Scene const& scene, FSetId const fSetId, FRepId const fRepId)
{
  FRep const& fRep = scene.Get(fSetId, fRepId);

  FSet const& fSet = scene.Get(fSetId);
  IRep const& iRep = scene.Get(fSet.Reference<IRep>::Get());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (iRep.Reference<PRep>::IsValid())
  {
    PRep const& pRep = scene.Get(iRep.Reference<PRep>::Get());

    FTrk const& fTrk = scene.Get(fRep.Reference<FTrk>::Get());
    ORep const& oRep = scene.Get(fTrk.Reference<ORep>::Get());

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    DLM_ASSERT(!fTrk.Reference<TSeq>::IsValid(), DLM_RUNTIME_ERROR);
    DLM_ASSERT(!oRep.Reference<TSeq>::IsValid(), DLM_RUNTIME_ERROR);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    return pRep(oRep);
  }
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  else
  {
    Vector3d point = Transform(scene, fRep.Reference<FTrk>::Get());

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // apply transformations
    if (iRep.Reference<TSeq>::IsValid())
    {
      point = Transform(point, scene, iRep.Reference<TSeq>::Get());
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // perspective division
    Vector2d point2D = VF::Inhomogenize(point);
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // apply distortion
    if (iRep.Reference<DRep>::IsValid())
    {
      point2D = scene.Get(iRep.Reference<DRep>::Get())(point2D);
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // apply camera
    if (iRep.Reference<CRep>::IsValid())
    {
      point2D = scene.Get(iRep.Reference<CRep>::Get())(point2D);
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    return point2D;
  }
}

//------------------------------------------------------------------------------
inline Transformation SceneAssistant::Collapse(
  Scene const& scene, TSeqId const tSeqId)
{
  TSeq const& tSeq = scene.Get(tSeqId);

  Transformation result;

  auto it = tSeq.cbegin();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (it != tSeq.cend())
  {
    // Single out the first one; otherwise the collapsed transformation will
    // never be inverted.
    result = scene.Get(it->first);

    ++it;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (; it != tSeq.cend(); ++it)
  {
    result *= scene.Get(it->first);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (tSeq.Reference<TSeq>::IsValid())
  {
    result = Collapse(scene, tSeq.Reference<TSeq>::Get()) * result;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
inline std::set<TRepId> SceneAssistant::Enumerate(
  Scene const& scene, TSeqId const tSeqId)
{
  std::set<TRepId> tRepS;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  TSeq const& tSeq = scene.Get(tSeqId);

  for (auto it = tSeq.cbegin(); it != tSeq.cend(); ++it)
  {
    tRepS.insert(it->first);
  }

  //tRepS.insert(tSeq.cbegin(), tSeq.cend());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //process references
  if (tSeq.Reference<TSeq>::IsValid())
  {
    TSeqId current = tSeq.Reference<TSeq>::Get();

    while (true)
    {
      TSeq const& tsRef = scene.Get(current);

      for (auto it = tsRef.cbegin(); it != tsRef.cend(); ++it)
      {
        tRepS.insert(it->first);
      }

      //tRepS.insert(tsRef.cbegin(), tsRef.cend());

      if (!tsRef.Reference<TSeq>::IsValid()) { break; }

      current = tsRef.Reference<TSeq>::Get();
    }
  }
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  return tRepS;
}

//------------------------------------------------------------------------------
inline std::vector<std::pair<TRepId, bool> >
  SceneAssistant::AssembleTRepSequence(
    Scene const& scene, TSeqId const tSeqId)
{
  TSeq const& tSeq = scene.Get(tSeqId);

  std::vector<std::pair<TRepId, bool> > result;
  
  result.insert(result.begin(), tSeq.cbegin(), tSeq.cend());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //process references
  if (tSeq.Reference<TSeq>::IsValid())
  {
    TSeqId current = tSeq.Reference<TSeq>::Get();

    while (true)
    {
      TSeq const& tsRef = scene.Get(current);

      result.insert(result.begin(), tsRef.begin(), tsRef.end());

      if (!tsRef.Reference<TSeq>::IsValid()) { break; }

      current = tsRef.Reference<TSeq>::Get();
    }
  }
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  return result;
}

//------------------------------------------------------------------------------
inline std::vector<std::pair<TRepId, bool> >
  SceneAssistant::AssembleTRepSequence(
    Scene const& scene, FSetId const fSetId, FRepId const fRepId)
{
  std::vector<std::pair<TRepId, bool> > result;

  FSet const& fSet = scene.Get(fSetId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // get image data TRep sequence
  if (fSet.Reference<IRep>::IsValid())
  {
    IRep const& iRep = scene.Get(fSet.Reference<IRep>::Get());

    if (iRep.Reference<TSeq>::IsValid())
    {
      result = AssembleTRepSequence(scene, iRep.Reference<TSeq>::Get());
    }
  }
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  FRep const& fRep = scene.Get(fSetId, fRepId);

  if (!fRep.Reference<FTrk>::IsValid()) { return result; }

  FTrk const& fTrk = scene.Get(fRep.Reference<FTrk>::Get());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // get feature point track TRep sequence
  if (fTrk.Reference<TSeq>::IsValid())
  {
    auto const sequence = AssembleTRepSequence(scene, fTrk.Reference<TSeq>::Get());

    result.insert(result.end(), sequence.begin(), sequence.end());
  }
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  if (!fTrk.Reference<ORep>::IsValid()) { return result; }

  ORep const& oRep = scene.Get(fTrk.Reference<ORep>::Get());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // get object point TRep sequence
  if (oRep.Reference<TSeq>::IsValid())
  {
    auto const sequence = AssembleTRepSequence(scene, oRep.Reference<TSeq>::Get());

    result.insert(result.end(), sequence.begin(), sequence.end());
  }
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  return result;
}

//------------------------------------------------------------------------------
inline void SceneAssistant::Set(
  Scene& scene, ProjectiveCameraFlags const& flags)
{
  uint64 const range = scene.PRepS().Range();
  
  for (uint64 i = 0; i < range; ++i)
  {
    PRepId const pRepId = PRepId(i);

    if (scene.IsValid(pRepId))
    {
      scene.Get(pRepId) = flags;
    }
  }
}

//------------------------------------------------------------------------------
inline void SceneAssistant::Set(
  Scene& scene, CameraFlags const& flags)
{
  uint64 const range = scene.CRepS().Range();
  
  for (uint64 i = 0; i < range; ++i)
  {
    CRepId const cRepId = CRepId(i);

    if (scene.IsValid(cRepId))
    {
      scene.Get(cRepId) = flags;
    }
  }
}

//------------------------------------------------------------------------------
inline void SceneAssistant::Set(
  Scene& scene, DistortionFlags const& flags)
{
  uint64 const range = scene.DRepS().Range();
  
  for (uint64 i = 0; i < range; ++i)
  {
    DRepId const dRepId = DRepId(i);

    if (scene.IsValid(dRepId))
    {
      scene.Get(dRepId) = flags;
    }
  }
}

//------------------------------------------------------------------------------
inline void SceneAssistant::Set(
  Scene& scene, ObjectPointFlags const& flags)
{
  uint64 const range = scene.ORepS().Range();
  
  for (uint64 i = 0; i < range; ++i)
  {
    ORepId const oRepId = ORepId(i);

    if (scene.IsValid(oRepId))
    {
      scene.Get(oRepId) = flags;
    }
  }
}

//------------------------------------------------------------------------------
inline void SceneAssistant::Set(
  Scene& scene, TransformationFlags const& flags)
{
  uint64 const range = scene.TRepS().Range();

  for (uint64 i = 0; i < range; ++i)
  {
    TRepId const tRepId = TRepId(i);

    if (scene.IsValid(tRepId))
    {
      scene.Get(tRepId) = flags;
    }
  }
}

//------------------------------------------------------------------------------
inline std::vector<std::pair<IRepId, DRepId> > SceneAssistant::AddDistortion(
  Scene& scene, DistortionFlags const& flags)
{
  std::vector<std::pair<IRepId, DRepId> > result;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint64 const range = scene.IRepS().Range();
  
  for (uint64 i = 0; i < range; ++i)
  {
    IRepId const iRepId = IRepId(i);
  
    if (scene.IsValid(iRepId))
    {
      if (!scene.Get(iRepId).Reference<DRep>::IsValid())
      {
        DRepId const dRepId = scene.CreateDRep();
        
        scene.Get(dRepId) = flags;
      
        scene.Assign(iRepId, dRepId);
        
        result.push_back(std::pair<IRepId, DRepId>(iRepId, dRepId));
      }
    }
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_sceneSceneAssistant_inline_h_
