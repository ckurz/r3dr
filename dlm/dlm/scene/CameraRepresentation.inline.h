//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_scene_CameraRepresentation_inline_h_
#define dlm_scene_CameraRepresentation_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/scene/CameraRepresentation.h"

//------------------------------------------------------------------------------
#include "dlm/scene/ReferenceSet.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/flags/CameraFlags.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/entity/Camera.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
inline CameraRepresentation::CameraRepresentation(
  BaseClass const& camera, FlagClass const& flags)
  : BaseClass   (camera)
  , FlagClass   (flags )
{}

//------------------------------------------------------------------------------
inline CameraRepresentation::CameraRepresentation(
  CameraRepresentation const& other)
  : BaseClass         (other)
  , FlagClass         (other)
  , ReferenceSet<IRep>(other)
{}

//==============================================================================
// Operators
//------------------------------------------------------------------------------
inline CameraRepresentation& CameraRepresentation::operator=(
  BaseClass const& camera)
{
  BaseClass::operator=(camera);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
inline CameraRepresentation& CameraRepresentation::operator=(
  FlagClass const& flags)
{
  FlagClass::operator=(flags);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
inline CameraRepresentation& CameraRepresentation::operator=(
  CameraRepresentation const& other)
{
  BaseClass::operator=(other);
  FlagClass::operator=(other);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  ReferenceSet<IRep>::operator=(other);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//==============================================================================
// Convenience functions
//------------------------------------------------------------------------------
inline CameraRepresentation::IRepSet const& CameraRepresentation::IRepS() const
{
  return this->ReferenceSet<IRep>::Get();
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
