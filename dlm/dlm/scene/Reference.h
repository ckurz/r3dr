//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_scene_Reference_h_
#define dlm_scene_Reference_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/core/Identifier.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
//! \brief Reference class.
//!
// Reference class
//------------------------------------------------------------------------------
template <typename T>
class Reference
{
public:
  //============================================================================
  // Friends
  //----------------------------------------------------------------------------
  bool IsValid() const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Identifier<T> Get() const;

  //============================================================================
  // Friends
  //----------------------------------------------------------------------------
  friend class Scene;
  friend class ImageRepresentation;
  friend class FeaturePointSet;
  friend class FeaturePointRepresentation;
  friend class FeaturePointTrack;
  friend class ProjectiveCameraRepresentation;
  friend class ObjectPointRepresentation;
  friend class TransformationSequence;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  friend class BinarySerializer;
  friend class BinaryDeserializer;

private:
  //============================================================================
  // Constructors
  //----------------------------------------------------------------------------
  Reference(Identifier<T> const id = Identifier<T>());
  Reference(Reference const& other);

  //============================================================================
  // Operators
  //----------------------------------------------------------------------------
  Reference& operator=(Reference const& other);

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  void Assign(Identifier<T> const id);
  void Remove();

  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  Identifier<T> id_;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
