//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_scene_TransformationRepresentation_inline_h_
#define dlm_scene_TransformationRepresentation_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/scene/TransformationRepresentation.h"

//------------------------------------------------------------------------------
#include "dlm/flags/TransformationFlags.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/entity/Transformation.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
inline TransformationRepresentation::TransformationRepresentation(
  BaseClass const& transformation, FlagClass const& flags)
  : BaseClass(transformation)
  , FlagClass(flags         )
{}

//------------------------------------------------------------------------------
inline TransformationRepresentation::TransformationRepresentation(
  TransformationRepresentation const& other)
  : BaseClass(other       )
  , FlagClass(other       )
  , tSeqM_   (other.tSeqM_)
{}

//==============================================================================
// Operators
//------------------------------------------------------------------------------
inline TransformationRepresentation& TransformationRepresentation::operator=(
  BaseClass const& transformation)
{
  BaseClass::operator=(transformation);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
inline TransformationRepresentation& TransformationRepresentation::operator=(
  FlagClass const& flags)
{
  FlagClass::operator=(flags);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
inline TransformationRepresentation& TransformationRepresentation::operator=(
  TransformationRepresentation const& other)
{
  BaseClass::operator=(other);
  FlagClass::operator=(other);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  tSeqM_ = other.tSeqM_;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//==============================================================================
// Accessors
//------------------------------------------------------------------------------
inline TransformationRepresentation::TSeqMap const&
  TransformationRepresentation::TSeqM() const
{
  return tSeqM_;
}

//------------------------------------------------------------------------------
inline void TransformationRepresentation::Add(
  TSeqId const id, uint64 const count)
{
  DLM_ASSERT(count, DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  TSeqMap::iterator it = tSeqM_.find(id);

  if (it == tSeqM_.end())
  {
    tSeqM_[id] = count;
  }
  else
  {
    it->second += count;
  }
}

//------------------------------------------------------------------------------
inline void TransformationRepresentation::Remove(
  TSeqId const id, uint64 const count)
{
  DLM_ASSERT(count, DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  TSeqMap::iterator it = tSeqM_.find(id);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DLM_ANNOTATED_ASSERT(it != tSeqM_.end(), DLM_RUNTIME_ERROR,
    "TransformationRepresentation::RemoveTSeq: TSeqId not present");

  DLM_ANNOTATED_ASSERT(count <= it->second, DLM_RUNTIME_ERROR,
    "TransformationRepresentation::RemoveTSeq: not enough instances left");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  it->second -= count;

  if (!it->second)
  {
    tSeqM_.erase(it);
  }
}

//------------------------------------------------------------------------------
inline void TransformationRepresentation::Clear()
{
  tSeqM_.clear();
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_scene_TransformationRepresentation_inline_h_
