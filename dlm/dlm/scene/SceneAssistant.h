//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_scene_SceneAssistant_h_
#define dlm_scene_SceneAssistant_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/scene/Scene.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <vector>
#include <set>
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// In addition to the files listed here, this file includes one or more inline
// header files for convenience. These includes can be found below in the
// section 'Inline code', after the main body of the document.
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// SceneAssistant class
//------------------------------------------------------------------------------
class SceneAssistant
{
public:
  //============================================================================
  // Static member functions
  //----------------------------------------------------------------------------
  // Point transformation
  //----------------------------------------------------------------------------
  static Vector3d Transform(Scene const& scene, FTrkId const fTrkId);
  static Vector3d Transform(Scene const& scene, ORepId const oRepId);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  static Vector3d Transform(
    Vector3d const& point, Scene const& scene, TSeqId const tSeqId);
  
  //----------------------------------------------------------------------------
  // Point projection
  //----------------------------------------------------------------------------
  static Vector2d Project(
    Scene const& scene, FSetId const fSetId, FRepId const fRepId);

  //----------------------------------------------------------------------------
  // Transformation collapsing
  //----------------------------------------------------------------------------
  static Transformation Collapse(Scene const& scene, TSeqId const tSeqId);

  //----------------------------------------------------------------------------
  // TRep enumeration
  //----------------------------------------------------------------------------
  static std::set<TRepId> Enumerate(
    Scene const& scene, TSeqId const tSeqId);

  //----------------------------------------------------------------------------
  // TRep sequence assembling
  //----------------------------------------------------------------------------
  static std::vector<std::pair<TRepId, bool> > AssembleTRepSequence(
    Scene const& scene, TSeqId const tSeqId);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  static std::vector<std::pair<TRepId, bool> > AssembleTRepSequence(
    Scene const& scene, FSetId const fSetId, FRepId const fRepId);

  //----------------------------------------------------------------------------
  // Flag manipulation
  //----------------------------------------------------------------------------
  // TODO: Set could return a vector of <xRepId, xFlags> pairs
  // TODO: Implement Set that accepts a vector of <xRepId, xFlags> pairs 
  static void Set(Scene& scene, ProjectiveCameraFlags const& flags);
  static void Set(Scene& scene,           CameraFlags const& flags);
  static void Set(Scene& scene,       DistortionFlags const& flags);
  static void Set(Scene& scene,      ObjectPointFlags const& flags);
  static void Set(Scene& scene,   TransformationFlags const& flags);

  //----------------------------------------------------------------------------
  // Data manipulation
  //----------------------------------------------------------------------------
  static std::vector<std::pair<IRepId, DRepId> > AddDistortion(
    Scene& scene, DistortionFlags const& flags = DistortionFlags::Enabled());
};

//==============================================================================
// Shorthand
//------------------------------------------------------------------------------
typedef SceneAssistant SA;

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// Inline code
//------------------------------------------------------------------------------
#include "dlm/scene/SceneAssistant.inline.h"

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_scene_SceneAssistant_h_
