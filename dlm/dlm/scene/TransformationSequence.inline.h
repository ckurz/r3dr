//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_scene_TransformationSequence_inline_h_
#define dlm_scene_TransformationSequence_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/scene/TransformationSequence.h"

//------------------------------------------------------------------------------
#include "dlm/scene/Reference.h"
#include "dlm/scene/ReferenceSet.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
inline TransformationSequence::TransformationSequence(
  BaseClass const& baseClass)
  : BaseClass(baseClass)
{}

//------------------------------------------------------------------------------
inline TransformationSequence::TransformationSequence(
  TransformationSequence const& other)
  : BaseClass         (other)
  , ReferenceSet<IRep>(other)
  , ReferenceSet<FTrk>(other)
  , ReferenceSet<ORep>(other)
  , ReferenceSet<TSeq>(other)
  , Reference   <TSeq>(other)
{}

//==============================================================================
// Operators
//------------------------------------------------------------------------------
inline TransformationSequence& TransformationSequence::operator=(
  BaseClass const& baseClass)
{
  BaseClass::operator=(baseClass);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
inline TransformationSequence& TransformationSequence::operator=(
  TransformationSequence const& other)
{
  BaseClass::operator=(other);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  ReferenceSet<IRep>::operator=(other);
  ReferenceSet<FTrk>::operator=(other);
  ReferenceSet<ORep>::operator=(other);
  ReferenceSet<TSeq>::operator=(other);
  Reference   <TSeq>::operator=(other);
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
inline std::vector<uint64> TransformationSequence::GetTRepPositions(
  TRepId const id) const
{
  uint64 const count = BaseClass::size();

  std::vector<uint64> result;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < count; ++i)
  {
    uint64 const index = count - i - 1; //reverse ordering

    if (BaseClass::at(index).first != id) { continue; }

    result.push_back(index);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
inline void TransformationSequence::SetSpecial(
  uint64 const position, bool const value)
{
  DLM_ASSERT(position <= BaseClass::size(), DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  BaseClass::at(position).second = value;
}

//------------------------------------------------------------------------------
inline void TransformationSequence::Append(TRepId const id, bool const special)
{
  BaseClass::push_back(std::pair<TRepId, bool>(id, special));
}

//------------------------------------------------------------------------------
inline void TransformationSequence::Insert(
  TRepId const id, uint64 const position, bool const special)
{
  DLM_ASSERT(position <= BaseClass::size(), DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  auto const offset =
    static_cast<BaseClass::iterator::difference_type>(position);
  
  BaseClass::iterator it = BaseClass::begin() + offset;

  BaseClass::insert(it, std::pair<TRepId, bool>(id, special));
}

//------------------------------------------------------------------------------
inline void TransformationSequence::Remove(
  TRepId const id, uint64 const position)
{
  DLM_ASSERT(position <= BaseClass::size(), DLM_OUT_OF_RANGE);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  auto const offset =
    static_cast<BaseClass::iterator::difference_type>(position);
  
  BaseClass::iterator it = BaseClass::begin() + offset;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DLM_ASSERT(it->first == id, DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  BaseClass::erase(it);
}

//==============================================================================
// Convenience functions
//------------------------------------------------------------------------------
inline TransformationSequence::IRepSet const&
  TransformationSequence::IRepS() const
{
  return this->ReferenceSet<IRep>::Get();
}

//------------------------------------------------------------------------------
inline TransformationSequence::TSeqSet const&
  TransformationSequence::TSeqS() const
{
  return this->ReferenceSet<TSeq>::Get();
}

//------------------------------------------------------------------------------
inline TransformationSequence::FTrkSet const&
  TransformationSequence::FTrkS() const
{
  return this->ReferenceSet<FTrk>::Get();
}

//------------------------------------------------------------------------------
inline TransformationSequence::ORepSet const&
  TransformationSequence::ORepS() const
{
  return this->ReferenceSet<ORep>::Get();
}

//------------------------------------------------------------------------------
//! \brief Specify.
//! @return Specify.
//!
inline bool TransformationSequence::HasTSRef() const
{
  return this->TSReference::IsValid();
}

//------------------------------------------------------------------------------
//! \brief Specify.
//! @return Specify.
//!
inline TSeqId TransformationSequence::TSRef() const
{
  return this->TSReference::Get();
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
