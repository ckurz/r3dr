//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2014  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Doxygen file documentation
//------------------------------------------------------------------------------
//! \file

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_scene_WeightingMatrixRepresentation_inline_h_
#define dlm_scene_WeightingMatrixRepresentation_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/scene/WeightingMatrixRepresentation.h"

//------------------------------------------------------------------------------
#include "dlm/scene/ReferencePairSet.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/entity/WeightingMatrix.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
inline WeightingMatrixRepresentation::WeightingMatrixRepresentation(
  BaseClass const& matrix)
  : BaseClass(matrix)
{}

//------------------------------------------------------------------------------
inline WeightingMatrixRepresentation::WeightingMatrixRepresentation(
  WeightingMatrixRepresentation const& other)
  : BaseClass                   (other)
  , ReferencePairSet<FSet, FRep>(other)
{}

//==============================================================================
// Operators
//------------------------------------------------------------------------------
inline WeightingMatrixRepresentation&
  WeightingMatrixRepresentation::operator=(
    BaseClass const& matrix)
{
  BaseClass::operator=(matrix);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
inline WeightingMatrixRepresentation&
  WeightingMatrixRepresentation::operator=(
    WeightingMatrixRepresentation const& other)
{
  BaseClass::operator=(other);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  ReferencePairSet<FSet, FRep>::operator=(other);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//==============================================================================
// Convenience functions
//------------------------------------------------------------------------------
inline WeightingMatrixRepresentation::FSetFRepPairSet const&
  WeightingMatrixRepresentation::FSetFRepPairS() const
{
  return this->ReferencePairSet<FSet, FRep>::Get();
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
