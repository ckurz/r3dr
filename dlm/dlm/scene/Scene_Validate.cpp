//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//------------------------------------------------------------------------------
// Copyright (C)  2015  Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/scene/Scene.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Member function
//------------------------------------------------------------------------------
//! \brief Validate the connections of the specified ImageRepresentation object.
//! @param iRepId The Identifier of the IRep to be validated.
//! @return `true` if the validation succeeded, `false` otherwise.
//!
//! \note This function must not fail. If it does, please contact the developer.
bool Scene::Validate(IRepId const iRepId) const
{
  if (!IsValid(iRepId))                           { return false; }

  IRep const& iRep = Get(iRepId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (iRep.Reference<PRep>::IsValid())
  {
    if (iRep.Reference<CRep>::IsValid())       { return false; }
    if (iRep.Reference<DRep>::IsValid())       { return false; }
    if (iRep.Reference<TSeq>::IsValid())       { return false; }
  
    PRepId const pRepId = iRep.Reference<PRep>::Get();

    if (!IsValid(pRepId))                      { return false; }

    PRep const& pRep = Get(pRepId);

    if (!pRep.Reference<IRep>::IsValid() || pRep.Reference<IRep>::Get() != iRepId) { return false; }
  }


  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (iRep.Reference<CRep>::IsValid())
  {
    CRepId const cRepId = iRep.Reference<CRep>::Get();

    if (!IsValid(cRepId))                         { return false; }

    auto iRepS = Get(cRepId).ReferenceSet<IRep>::Get();

    if (iRepS.find(iRepId) == iRepS.end())        { return false; }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (iRep.Reference<DRep>::IsValid())
  {
    DRepId const dRepId = iRep.Reference<DRep>::Get();

    if (!IsValid(dRepId))                         { return false; }

    std::set<IRepId> const& iRepS = Get(dRepId).ReferenceSet<IRep>::Get();

    if (iRepS.find(iRepId) == iRepS.end())        { return false; }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (iRep.Reference<FSet>::IsValid())
  {
    FSetId const fSetId = iRep.Reference<FSet>::Get();

    if (!IsValid(fSetId))                         { return false; }

    FSet const& fSet = Get(fSetId);

    if (!fSet.Reference<IRep>::IsValid() || fSet.Reference<IRep>::Get() != iRepId) { return false; }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (iRep.Reference<TSeq>::IsValid())
  {
    TSeqId const tSeqId = iRep.Reference<TSeq>::Get();

    if (!IsValid(tSeqId))                         { return false; }

    std::set<IRepId> const& iRepS = Get(tSeqId).ReferenceSet<IRep>::Get();

    if (iRepS.find(iRepId) == iRepS.end())        { return false; }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return true;
}

//------------------------------------------------------------------------------
//! \brief Validate the connections of the specified FeaturePointSet object.
//! @param fSetId The Identifier of the FSet to be validated.
//! @return `true` if the validation succeeded, `false` otherwise.
//!
//! \note This function must not fail. If it does, please contact the developer.
bool Scene::Validate(FSetId const fSetId) const
{
  if (!IsValid(fSetId))                                 { return false; }

  FSet const& fSet = Get(fSetId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (fSet.Reference<IRep>::IsValid())
  {
    IRepId const iRepId = fSet.Reference<IRep>::Get();

    if (!IsValid(iRepId))                               { return false; }

    IRep const& iRep = Get(iRepId);

    if (!iRep.Reference<FSet>::IsValid() || iRep.Reference<FSet>::Get() != fSetId)       { return false; }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for (uint64 i = 0; i < fSet.Range(); ++i)
  {
    if (fSet.Active(i) && !Validate(fSetId, FRepId(i))) { return false; }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return true;
}

//------------------------------------------------------------------------------
//! \brief Validate the connections of the specified
//! WeightingMatrixRepresentation object.
//! @param wMatId The Identifier of the WMat to be validated.
//! @return `true` if the validation succeeded, `false` otherwise.
//!
//! \note This function must not fail. If it does, please contact the developer.
bool Scene::Validate(WMatId const wMatId) const
{
  if (!IsValid(wMatId)) { return false; }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  auto const& fRepS = Get(wMatId).ReferencePairSet<FSet, FRep>::Get();

  for (auto it = fRepS.cbegin(); it != fRepS.cend(); ++it)
  {
    if (!IsValid(it->first)            ) { return false; }
    if (!IsValid(it->first, it->second)) { return false; }
    
    FRep const& fRep = Get(it->first, it->second);
    
    if (!fRep.Reference<WMat>::IsValid() || fRep.Reference<WMat>::Get() != wMatId) { return false; }
  }
  
  return true;
}

//------------------------------------------------------------------------------
//! \brief Validate the connections of the specified FeaturePointTrack
//! object.
//! @param fTrkId The Identifier of the FTrk to be validated.
//! @return `true` if the validation succeeded, `false` otherwise.
//!
//! \note This function must not fail. If it does, please contact the developer.
bool Scene::Validate(FTrkId const fTrkId) const
{
  if (!IsValid(fTrkId))                           { return false; }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FTrk const& fTrk = Get(fTrkId);

  for (auto it = fTrk.begin(); it != fTrk.end(); ++it)
  {
    if (!IsValid(it->first)            )          { return false; }
    if (!IsValid(it->first, it->second))          { return false; }

    FRep const& fRep = Get(it->first, it->second);

    if (!fRep.Reference<FTrk>::IsValid() || fRep.Reference<FTrk>::Get() != fTrkId) { return false; }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (fTrk.Reference<TSeq>::IsValid())
  {
    TSeqId const tSeqId = fTrk.Reference<TSeq>::Get();

    if (!IsValid(tSeqId))                         { return false; }

    std::set<FTrkId> const& fTrkS = Get(tSeqId).ReferenceSet<FTrk>::Get();

    if (fTrkS.find(fTrkId) == fTrkS.end())        { return false; }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (fTrk.Reference<ORep>::IsValid())
  {
    ORepId const oRepId = fTrk.Reference<ORep>::Get();

    if (!IsValid(oRepId))                         { return false; }

    std::set<FTrkId> const& fTrkS = Get(oRepId).ReferenceSet<FTrk>::Get();

    if (fTrkS.find(fTrkId) == fTrkS.end())        { return false; }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return true;
}

//------------------------------------------------------------------------------
//! \brief Validate the connections of the specified
//! ProjectiveCameraRepresentation object.
//! @param pRepId The Identifier of the PRep to be validated.
//! @return `true` if the validation succeeded, `false` otherwise.
//!
//! \note This function must not fail. If it does, please contact the developer.
bool Scene::Validate(PRepId const pRepId) const
{
  if (!IsValid(pRepId))                           { return false; }

  PRep const& pRep = Get(pRepId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (pRep.Reference<IRep>::IsValid())
  {
    IRepId const iRepId = pRep.Reference<IRep>::Get();

    if (!IsValid(iRepId))                               { return false; }

    IRep const& iRep = Get(iRepId);

    if (!iRep.Reference<PRep>::IsValid() ||
         iRep.Reference<PRep>::Get() != pRepId)       { return false; }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return true;
}

//------------------------------------------------------------------------------
//! \brief Validate the connections of the specified CameraRepresentation
//! object.
//! @param cRepId The Identifier of the CRep to be validated.
//! @return `true` if the validation succeeded, `false` otherwise.
//!
//! \note This function must not fail. If it does, please contact the developer.
bool Scene::Validate(CRepId const cRepId) const
{
  if (!IsValid(cRepId))                           { return false; }

  CRep const& cRep = Get(cRepId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  auto iRepS = cRep.ReferenceSet<IRep>::Get();

  for (auto it = iRepS.cbegin(); it != iRepS.cend(); ++it)
  {
    if (!IsValid(*it))                            { return false; }

    IRep const& iRep = Get(*it);

    if (!iRep.Reference<CRep>::IsValid() ||
         iRep.Reference<CRep>::Get() != cRepId) { return false; }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return true;
}

//------------------------------------------------------------------------------
//! \brief Validate the connections of the specified DistortionRepresentation
//! object.
//! @param dRepId The Identifier of the DRep to be validated.
//! @return `true` if the validation succeeded, `false` otherwise.
//!
//! \note This function must not fail. If it does, please contact the developer.
bool Scene::Validate(DRepId const dRepId) const
{
  if (!IsValid(dRepId))                           { return false; }

  DRep const& dRep = Get(dRepId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  std::set<IRepId> const& iRepS = dRep.ReferenceSet<IRep>::Get();

  for (auto it = iRepS.cbegin(); it != iRepS.cend(); ++it)
  {
    if (!IsValid(*it))                            { return false; }

    IRep const& iRep = Get(*it);

    if (!iRep.Reference<DRep>::IsValid() || iRep.Reference<DRep>::Get() != dRepId) { return false; }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return true;
}

//------------------------------------------------------------------------------
//! \brief Validate the connections of the specified ObjectPointRepresentation
//! object.
//! @param oRepId The Identifier of the ORep to be validated.
//! @return `true` if the validation succeeded, `false` otherwise.
//!
//! \note This function must not fail. If it does, please contact the developer.
bool Scene::Validate(ORepId const oRepId) const
{
  if (!IsValid(oRepId))                           { return false; }

  ORep const& oRep = Get(oRepId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  std::set<FTrkId> const& fTrkS = oRep.ReferenceSet<FTrk>::Get();

  for (auto it = fTrkS.begin(); it != fTrkS.end(); ++it)
  {
    if (!IsValid(*it))                            { return false; }

    FTrk const& fTrk = Get(*it);

    if (!fTrk.Reference<ORep>::IsValid() || fTrk.Reference<ORep>::Get() != oRepId) { return false; }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (oRep.Reference<TSeq>::IsValid())
  {
    TSeqId const tSeqId = oRep.Reference<TSeq>::Get();

    if (!IsValid(tSeqId))                         { return false; }

    std::set<ORepId> const& oRepS = Get(tSeqId).ReferenceSet<ORep>::Get();

    if (oRepS.find(oRepId) == oRepS.end())        { return false; }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return true;
}

//------------------------------------------------------------------------------
//! \brief Validate the connections of the specified TransformationSequence
//! object.
//! @param tSeqId The Identifier of the TSeq to be validated.
//! @return `true` if the validation succeeded, `false` otherwise.
//!
//! \note This function must not fail. If it does, please contact the developer.
bool Scene::Validate(TSeqId const tSeqId) const
{
  if (!IsValid(tSeqId))                                   { return false; }

  TSeq const& tSeq = Get(tSeqId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  std::set<IRepId> const& iRepS = tSeq.ReferenceSet<IRep>::Get();

  for (auto it = iRepS.cbegin(); it != iRepS.cend(); ++it)
  {
    if (!IsValid(*it))                                    { return false; }

    IRep const& iRep = Get(*it);

    if (!iRep.Reference<TSeq>::IsValid() || iRep.Reference<TSeq>::Get() != tSeqId)         { return false; }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  std::set<TSeqId> const& tSeqS = tSeq.ReferenceSet<TSeq>::Get();

  for (auto it = tSeqS.cbegin(); it != tSeqS.cend(); ++it)
  {
    if (!IsValid(*it))                                    { return false; }

    TSeq const& current = Get(*it);

    if (!current.Reference<TSeq>::IsValid() || current.Reference<TSeq>::Get() != tSeqId) { return false; }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  std::set<FTrkId> const& fTrkS = tSeq.ReferenceSet<FTrk>::Get();

  for (auto it = fTrkS.cbegin(); it != fTrkS.cend(); ++it)
  {
    if (!IsValid(*it))                                    { return false; }

    FTrk const& fTrk = Get(*it);

    if (!fTrk.Reference<TSeq>::IsValid() || fTrk.Reference<TSeq>::Get() != tSeqId)         { return false; }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  std::set<ORepId> const& oRepS = tSeq.ReferenceSet<ORep>::Get();

  for (auto it = oRepS.cbegin(); it != oRepS.cend(); ++it)
  {
    if (!IsValid(*it))                                    { return false; }

    ORep const& oRep = Get(*it);

    if (!oRep.Reference<TSeq>::IsValid() || oRep.Reference<TSeq>::Get() != tSeqId)         { return false; }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (tSeq.Reference<TSeq>::IsValid())
  {
    TSeqId const tsRefId = tSeq.Reference<TSeq>::Get();

    if (!IsValid(tsRefId))                                { return false; }

    TSeq const& tsRef = Get(tsRefId);

    std::set<TSeqId> const& tsRefS = tsRef.ReferenceSet<TSeq>::Get();

    if (tsRefS.find(tSeqId) == tsRefS.end())              { return false; }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // check for self reference
    if (tsRefId == tSeqId)                                { return false; }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // check for circular references
    for (TSeqId current = tsRefId; Get(current).Reference<TSeq>::IsValid();
         current = Get(current).Reference<TSeq>::Get())
    {
      if (current == tSeqId)                              { return false; }
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return true;
}

//------------------------------------------------------------------------------
//! \brief Validate the connections of the specified
//! TransformationRepresentation object.
//! @param tRepId The Identifier of the TRep to be validated.
//! @return `true` if the validation succeeded, `false` otherwise.
//!
//! \note This function must not fail. If it does, please contact the developer.
bool Scene::Validate(TRepId const tRepId) const
{
  if (!IsValid(tRepId))                                     { return false; }

  TRep const& tRep = Get(tRepId);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  TRep::TSeqMap const& tSeqM = tRep.TSeqM();

  for (auto it = tSeqM.cbegin(); it != tSeqM.cend(); ++it)
  {
    if (!IsValid(it->first))                                { return false; }

    TSeq const& tSeq = Get(it->first);

    if (tSeq.GetTRepPositions(tRepId).size() != it->second) { return false; }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return true;
}

//------------------------------------------------------------------------------
//! \brief Validate the connections of the specified 
//! FeaturePointRepresentation object.
//! @param fSetId The FSet the FRep resides in.
//! @param fRepId The Identifier of the FRep to be validated.
//! @return `true` if the validation succeeded, `false` otherwise.
//!
//! \note This function must not fail. If it does, please contact the developer.
bool Scene::Validate(FSetId const fSetId, FRepId const fRepId) const
{
  if (!IsValid(fSetId)        )           { return false; }
  if (!IsValid(fSetId, fRepId))           { return false; }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FRep const& fRep = Get(fSetId, fRepId);

  if (fRep.Reference<FTrk>::IsValid())
  {
    FTrkId const fTrkId = fRep.Reference<FTrk>::Get();

    if (!IsValid(fTrkId))                 { return false; }

    FTrk const& fTrk = Get(fTrkId);

    FRepId                trkFRepId;
    if (!fTrk.Get(fSetId, trkFRepId))     { return false; }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (fRep.Reference<WMat>::IsValid())
  {
    WMatId const wMatId = fRep.Reference<WMat>::Get();
    
    if (!IsValid(wMatId))                 { return false; }
    
    auto const& fRepS = Get(wMatId).ReferencePairSet<FSet, FRep>::Get();

    if (fRepS.find(std::make_pair(fSetId, fRepId)) == fRepS.end())
    {
      return false;
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return true;
}

//------------------------------------------------------------------------------
//! \brief Validate the connections of all ImageRepresentation objects.
//! @return `true` if the validation succeeded, `false` otherwise.
//!
//! \note This function must not fail. If it does, please contact the developer.
bool Scene::ValidateIRepS() const
{
  for (uint64 i = 0; i < iRepS_.Range(); ++i)
  {
    IRepId const iRepId = IRepId(i);

    if (IsValid(iRepId) && !Validate(iRepId)) { return false; }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return true;
}

//------------------------------------------------------------------------------
//! \brief Validate the connections of all FeaturePointSet objects.
//! @return `true` if the validation succeeded, `false` otherwise.
//!
//! \note This function must not fail. If it does, please contact the developer.
bool Scene::ValidateFSetS() const
{
  for (uint64 i = 0; i < fSetS_.Range(); ++i)
  {
    FSetId const fSetId = FSetId(i);

    if (IsValid(fSetId) && !Validate(fSetId)) { return false; }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return true;
}

//------------------------------------------------------------------------------
//! \brief Validate the connections of all WeightingMatrixRepresentation
//! objects.
//! @return `true` if the validation succeeded, `false` otherwise.
//!
//! \note This function must not fail. If it does, please contact the developer.
bool Scene::ValidateWMatS() const
{
  for (uint64 i = 0; i < wMatS_.Range(); ++i)
  {
    WMatId const wMatId = WMatId(i);

    if (IsValid(wMatId) && !Validate(wMatId)) { return false; }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return true;
}

//------------------------------------------------------------------------------
//! \brief Validate the connections of all FeaturePointTrack objects.
//! @return `true` if the validation succeeded, `false` otherwise.
//!
//! \note This function must not fail. If it does, please contact the developer.
bool Scene::ValidateFTrkS() const
{
  for (uint64 i = 0; i < fTrkS_.Range(); ++i)
  {
    FTrkId const fTrkId = FTrkId(i);

    if (IsValid(fTrkId) && !Validate(fTrkId)) { return false; }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return true;
}

//------------------------------------------------------------------------------
//! \brief Validate the connections of all ProjectiveCameraRepresentation
//! objects.
//! @return `true` if the validation succeeded, `false` otherwise.
//!
//! \note This function must not fail. If it does, please contact the developer.
bool Scene::ValidatePRepS() const
{
  for (uint64 i = 0; i < pRepS_.Range(); ++i)
  {
    PRepId const pRepId = PRepId(i);

    if (IsValid(pRepId) && !Validate(pRepId)) { return false; }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return true;
}

//------------------------------------------------------------------------------
//! \brief Validate the connections of all CameraRepresention objects.
//! @return `true` if the validation succeeded, `false` otherwise.
//!
//! \note This function must not fail. If it does, please contact the developer.
bool Scene::ValidateCRepS() const
{
  for (uint64 i = 0; i < cRepS_.Range(); ++i)
  {
    CRepId const cRepId = CRepId(i);

    if (IsValid(cRepId) && !Validate(cRepId)) { return false; }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return true;
}

//------------------------------------------------------------------------------
//! \brief Validate the connections of all DistortionRepresentation objects.
//! @return `true` if the validation succeeded, `false` otherwise.
//!
//! \note This function must not fail. If it does, please contact the developer.
bool Scene::ValidateDRepS() const
{
  for (uint64 i = 0; i < dRepS_.Range(); ++i)
  {
    DRepId const dRepId = DRepId(i);

    if (IsValid(dRepId) && !Validate(dRepId)) { return false; }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return true;
}

//------------------------------------------------------------------------------
//! \brief Validate the connections of all ObjectPointRepresentation objects.
//! @return `true` if the validation succeeded, `false` otherwise.
//!
//! \note This function must not fail. If it does, please contact the developer.
bool Scene::ValidateORepS() const
{
  for (uint64 i = 0; i < oRepS_.Range(); ++i)
  {
    ORepId const oRepId = ORepId(i);

    if (IsValid(oRepId) && !Validate(oRepId)) { return false; }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return true;
}

//------------------------------------------------------------------------------
//! \brief Validate the connections of all TransformationSequence objects.
//! @return `true` if the validation succeeded, `false` otherwise.
//!
//! \note This function must not fail. If it does, please contact the developer.
bool Scene::ValidateTSeqS() const
{
  for (uint64 i = 0; i < tSeqS_.Range(); ++i)
  {
    TSeqId const tSeqId = TSeqId(i);

    if (IsValid(tSeqId) && !Validate(tSeqId)) { return false; }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return true;
}

//------------------------------------------------------------------------------
//! \brief Validate the connections of all TransformationRepresentation objects.
//! @return `true` if the validation succeeded, `false` otherwise.
//!
//! \note This function must not fail. If it does, please contact the developer.
bool Scene::ValidateTRepS() const
{
  for (uint64 i = 0; i < tRepS_.Range(); ++i)
  {
    TRepId const tRepId = TRepId(i);

    if (IsValid(tRepId) && !Validate(tRepId)) { return false; }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return true;
}

//------------------------------------------------------------------------------
//! \brief Validate the connections between all objects in the scene.
//! @return `true` if the validation succeeded, `false` otherwise.
//!
//! \note This function must not fail. If it does, please contact the developer.
bool Scene::Validate() const
{
  if (!ValidateIRepS()) { return false; }
  if (!ValidateFSetS()) { return false; }
  if (!ValidateWMatS()) { return false; }
  if (!ValidateFTrkS()) { return false; }
  if (!ValidatePRepS()) { return false; }
  if (!ValidateCRepS()) { return false; }
  if (!ValidateDRepS()) { return false; }
  if (!ValidateORepS()) { return false; }
  if (!ValidateTSeqS()) { return false; }
  if (!ValidateTRepS()) { return false; }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return true;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
