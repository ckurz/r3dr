//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_flags_ObjectPointFlags_inline_h_
#define dlm_flags_ObjectPointFlags_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/flags/ObjectPointFlags.h"

//------------------------------------------------------------------------------
#include "dlm/flags/Flags.inline.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
inline ObjectPointFlags::ObjectPointFlags(bool const value)
{
  SetOptimize(value);
}

//==============================================================================
// Member functions: Accessors
//------------------------------------------------------------------------------
inline bool ObjectPointFlags::OptimizeX() const
{
  return At(0);
}

//------------------------------------------------------------------------------
inline bool ObjectPointFlags::OptimizeY() const
{
  return At(1);
}

//------------------------------------------------------------------------------
inline bool ObjectPointFlags::OptimizeZ() const
{
  return At(2);
}

//==============================================================================
// Member functions: Mutators
//------------------------------------------------------------------------------
inline bool& ObjectPointFlags::OptimizeX()
{
  return At(0);
}

//------------------------------------------------------------------------------
inline bool& ObjectPointFlags::OptimizeY()
{
  return At(1);
}

//------------------------------------------------------------------------------
inline bool& ObjectPointFlags::OptimizeZ()
{
  return At(2);
}

//==============================================================================
// Static member functions
//------------------------------------------------------------------------------
inline ObjectPointFlags ObjectPointFlags::Enabled()
{
  return ObjectPointFlags(true);
}

//------------------------------------------------------------------------------
inline ObjectPointFlags ObjectPointFlags::Disabled()
{
  return ObjectPointFlags(false);
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_flags_ObjectPointFlags_inline_h_
