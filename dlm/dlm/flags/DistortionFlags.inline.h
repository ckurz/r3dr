//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_flags_DistortionFlags_inline_h_
#define dlm_flags_DistortionFlags_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/flags/DistortionFlags.h"

//------------------------------------------------------------------------------
#include "dlm/flags/Flags.inline.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{
//==============================================================================
// Constructors
//------------------------------------------------------------------------------
inline DistortionFlags::DistortionFlags(bool const value)
{
  SetOptimize(value);
}

//==============================================================================
// Member functions: Accessors
//------------------------------------------------------------------------------
inline bool DistortionFlags::OptimizeP() const
{
  return At(0) || At(1); //just to be sure ;)
}

//----------------------------------------------------------------------
inline bool DistortionFlags::OptimizeK(uint32 const i) const
{
  return At(2 + i); //Vector will implicitely do the range check
}

//==============================================================================
// Member functions: Mutators
//------------------------------------------------------------------------------
inline bool& DistortionFlags::OptimizeK(uint32 const i)
{
  return At(2 + i); //Vector will implicitely do the range check
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
inline void DistortionFlags::SetOptimizeP(bool const value)
{
  At(0) = value;
  At(1) = value;
}

//------------------------------------------------------------------------------
inline void DistortionFlags::SetOptimizeK(bool const value)
{
  OptimizeK(0) = value;
  OptimizeK(1) = value;
  OptimizeK(2) = value;
  OptimizeK(3) = value;
  OptimizeK(4) = value;
  OptimizeK(5) = value;
}

//==============================================================================
// Static member functions
//------------------------------------------------------------------------------
inline DistortionFlags DistortionFlags::Enabled()
{
  return DistortionFlags(true);
}

//------------------------------------------------------------------------------
inline DistortionFlags DistortionFlags::Disabled()
{
  return DistortionFlags(false);
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_flags_DistortionFlags_inline_h_
