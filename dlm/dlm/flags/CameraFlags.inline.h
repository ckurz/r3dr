//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_flags_CameraFlags_inline_h_
#define dlm_flags_CameraFlags_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/flags/CameraFlags.h"

//------------------------------------------------------------------------------
#include "dlm/flags/Flags.inline.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
inline CameraFlags::CameraFlags(bool const value)
{
  SetOptimize(value);
}

//==============================================================================
// Member functions: Accessors
//------------------------------------------------------------------------------
inline bool CameraFlags::OptimizeFocalLength() const
{
  return operator()(0);
}

//------------------------------------------------------------------------------
inline bool CameraFlags::OptimizePrincipalPointX() const
{
  return operator()(1);
}

//------------------------------------------------------------------------------
inline bool CameraFlags::OptimizePrincipalPointY() const
{
  return operator()(2);
}

//------------------------------------------------------------------------------
inline bool CameraFlags::OptimizePixelAspectRatio() const
{
  return operator()(3);
}

//------------------------------------------------------------------------------
inline bool CameraFlags::OptimizeSkew() const
{
  return operator()(4);
}

//------------------------------------------------------------------------------
inline bool CameraFlags::OptimizePrincipalPoint() const
{
  return OptimizePrincipalPointX() || OptimizePrincipalPointY();
}

//==============================================================================
// Mutators
//------------------------------------------------------------------------------
inline bool& CameraFlags::OptimizeFocalLength()
{
  return operator()(0);
}

//------------------------------------------------------------------------------
inline bool& CameraFlags::OptimizePrincipalPointX()
{
  return operator()(1);
}

//------------------------------------------------------------------------------
inline bool& CameraFlags::OptimizePrincipalPointY()
{
  return operator()(2);
}

//------------------------------------------------------------------------------
inline bool& CameraFlags::OptimizePixelAspectRatio()
{
  return operator()(3);
}

//------------------------------------------------------------------------------
inline bool& CameraFlags::OptimizeSkew()
{
  return operator()(4);
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
inline void CameraFlags::SetOptimizePrincipalPoint(bool const value)
{
  OptimizePrincipalPointX() = value;
  OptimizePrincipalPointY() = value;
}

//==============================================================================
// Static member functions
//------------------------------------------------------------------------------
inline CameraFlags CameraFlags::Enabled()
{
  return CameraFlags(true);
}

//------------------------------------------------------------------------------
inline CameraFlags CameraFlags::Disabled()
{
  return CameraFlags(false);
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
