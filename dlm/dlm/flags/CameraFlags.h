//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_flags_CameraFlags_h_
#define dlm_flags_CameraFlags_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/flags/Flags.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// CameraFlags class
// Stores information about parameters to be optimized for the camera class
//------------------------------------------------------------------------------
class CameraFlags : public Flags<5>
{
public:
  //============================================================================
  // Constructors
  //----------------------------------------------------------------------------
  CameraFlags(bool const value = true);

  //============================================================================
  // Member functions: Accessors
  //----------------------------------------------------------------------------
  bool OptimizeFocalLength     () const;
  bool OptimizePrincipalPointX () const;
  bool OptimizePrincipalPointY () const;
  bool OptimizePixelAspectRatio() const;
  bool OptimizeSkew            () const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool OptimizePrincipalPoint  () const;

  //============================================================================
  // Member functions: Mutators
  //----------------------------------------------------------------------------
  bool& OptimizeFocalLength     ();
  bool& OptimizePrincipalPointX ();
  bool& OptimizePrincipalPointY ();
  bool& OptimizePixelAspectRatio();
  bool& OptimizeSkew            ();

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  void SetOptimizePrincipalPoint(bool const value);

  //============================================================================
  // Static member functions
  //----------------------------------------------------------------------------
  static CameraFlags  Enabled();
  static CameraFlags Disabled();
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
