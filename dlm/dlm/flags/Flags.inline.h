//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_flags_Flags_inline_h_
#define dlm_flags_Flags_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/flags/Flags.h"

//------------------------------------------------------------------------------
#include "dlm/math/Vector.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <iostream>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Operators
//------------------------------------------------------------------------------
template <uint32 SIZE>
inline Flags<SIZE>& Flags<SIZE>::operator=(BaseClass const& vector)
{
  BaseClass::operator=(vector);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
template <uint32 SIZE>
inline Flags<SIZE>& Flags<SIZE>::SetOptimize(bool const value)
{
  for (uint32 i = 0; i < SIZE; ++i)
  {
    BaseClass::At(i) = value;
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return *this;
}

//------------------------------------------------------------------------------
template <uint32 SIZE>
inline bool Flags<SIZE>::GetOptimize() const
{
  for (uint32 i = 0; i < SIZE; ++i)
  {
    if (BaseClass::At(i))
    {
      return true;
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return false;
}

//------------------------------------------------------------------------------
template <uint32 SIZE>
inline uint32 Flags<SIZE>::Count() const
{
  uint32 count = 0;

  for (uint32 i = 0; i < SIZE; ++i)
  {
    count += BaseClass::At(i);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return count;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_flags_Flags_inline_h_
