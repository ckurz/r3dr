//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_flags_ProjectiveCameraFlags_inline_h_
#define dlm_flags_ProjectiveCameraFlags_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/flags/ProjectiveCameraFlags.h"

//------------------------------------------------------------------------------
#include "dlm/flags/Flags.inline.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Constructor
//------------------------------------------------------------------------------
inline ProjectiveCameraFlags::ProjectiveCameraFlags(bool const value)
{
  SetOptimize(value);
}

//==============================================================================
// Static member functions
//------------------------------------------------------------------------------
inline ProjectiveCameraFlags ProjectiveCameraFlags::Enabled()
{
  return ProjectiveCameraFlags(true);
}

//------------------------------------------------------------------------------
inline ProjectiveCameraFlags ProjectiveCameraFlags::Disabled()
{
  return ProjectiveCameraFlags(false);
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_flags_ProjectiveCameraFlags_inline_h_
