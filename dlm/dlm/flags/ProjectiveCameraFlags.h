//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_flags_ProjectiveCameraFlags_h_
#define dlm_flags_ProjectiveCameraFlags_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/flags/Flags.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// ProjectiveCameraFlags class
// Stores information about parameters to be optimized for the camera class
//------------------------------------------------------------------------------
class ProjectiveCameraFlags : public Flags<12>
{
public:
  //============================================================================
  // Constructor
  //----------------------------------------------------------------------------
  ProjectiveCameraFlags(bool const value = true);

  //============================================================================
  // Static member functions
  //----------------------------------------------------------------------------
  static ProjectiveCameraFlags  Enabled();
  static ProjectiveCameraFlags Disabled();
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_flags_ProjectiveCameraFlags_h_
