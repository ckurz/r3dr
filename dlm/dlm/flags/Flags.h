//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_flags_Flags_h_
#define dlm_flags_Flags_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/math/Vector.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <iosfwd>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Flags template class
// Stores information about parameters to be optimized for the various elements
// used in the optimization process (e.g., points, intrinsics, etc.).
// Derive from this class to provide appropriate constructors and facilitate
// handling.
//------------------------------------------------------------------------------
template <uint32 SIZE>
class Flags : public Vector<SIZE, bool>
{
public:
  //============================================================================
  // Nested
  //----------------------------------------------------------------------------
  typedef Vector<SIZE, bool> BaseClass;

  //============================================================================
  // Operators
  //----------------------------------------------------------------------------
  Flags& operator=(BaseClass const& vector);

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  Flags& SetOptimize(bool const value);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool GetOptimize() const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint32 Count() const;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_flags_Flags_h_
