//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_flags_DistortionFlags_h_
#define dlm_flags_DistortionFlags_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/flags/Flags.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// CameraFlags class
// Stores information about parameters to be optimized for the camera class
//------------------------------------------------------------------------------
class DistortionFlags : public Flags<8>
{
public:
  //============================================================================
  // Constructors
  //----------------------------------------------------------------------------
  DistortionFlags(bool const value = true);

  //============================================================================
  // Member functions: Accessors
  //----------------------------------------------------------------------------
  bool OptimizeP() const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool OptimizeK(uint32 const i) const;

  //============================================================================
  // Member functions: Mutators
  //----------------------------------------------------------------------------
  bool& OptimizeK(uint32 const i);

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  void SetOptimizeP(bool const value);
  void SetOptimizeK(bool const value);

  //============================================================================
  // Static member functions
  //----------------------------------------------------------------------------
  static DistortionFlags  Enabled();
  static DistortionFlags Disabled();
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_flags_DistortionFlags_h_
