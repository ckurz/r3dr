//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_flags_TransformationFlags_h_
#define dlm_flags_TransformationFlags_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/flags/Flags.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// TransformationFlags class
// Stores information about parameters to be optimized for the transformation
// class
//------------------------------------------------------------------------------
class TransformationFlags : public Flags<6>
{
public:
  //============================================================================
  // Member functions: Accessors
  //----------------------------------------------------------------------------
  TransformationFlags(bool const value = true);

  //============================================================================
  // Member functions: Accessors
  //----------------------------------------------------------------------------
  bool OptimizePan () const;
  bool OptimizeTilt() const;
  bool OptimizeRoll() const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool OptimizeX() const;
  bool OptimizeY() const;
  bool OptimizeZ() const;

  //============================================================================
  // Member functions: Mutators
  //----------------------------------------------------------------------------
  bool& OptimizePan ();
  bool& OptimizeTilt();
  bool& OptimizeRoll();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool& OptimizeX();
  bool& OptimizeY();
  bool& OptimizeZ();
  
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  void SetOptimizeRotation   (bool const value);
  void SetOptimizeTranslation(bool const value);

  //============================================================================
  // Static member functions
  //----------------------------------------------------------------------------
  static TransformationFlags  Enabled();
  static TransformationFlags Disabled();
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_flags_TransformationFlags_h_
