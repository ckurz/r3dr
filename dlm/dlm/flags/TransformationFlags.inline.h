//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_flags_TransformationFlags_inline_h_
#define dlm_flags_TransformationFlags_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/flags/TransformationFlags.h"

//------------------------------------------------------------------------------
#include "dlm/flags/Flags.inline.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
inline TransformationFlags::TransformationFlags(bool const value)
{
  SetOptimize(value);
}

//==============================================================================
// Member functions: Accessors
//------------------------------------------------------------------------------
inline bool TransformationFlags::OptimizePan() const
{
  return At(0);
}

//------------------------------------------------------------------------------
inline bool TransformationFlags::OptimizeTilt() const
{
  return At(1);
}

//------------------------------------------------------------------------------
inline bool TransformationFlags::OptimizeRoll() const
{
  return At(2);
}

//------------------------------------------------------------------------------
inline bool TransformationFlags::OptimizeX() const
{
  return At(3);
}

//------------------------------------------------------------------------------
inline bool TransformationFlags::OptimizeY() const
{
  return At(4);
}

//------------------------------------------------------------------------------
inline bool TransformationFlags::OptimizeZ() const
{
  return At(5);
}

//==============================================================================
// Member functions: Mutators
//------------------------------------------------------------------------------
inline bool& TransformationFlags::OptimizePan()
{
  return At(0);
}

//------------------------------------------------------------------------------
inline bool& TransformationFlags::OptimizeTilt()
{
  return At(1);
}

//------------------------------------------------------------------------------
inline bool& TransformationFlags::OptimizeRoll()
{
  return At(2);
}

//------------------------------------------------------------------------------
inline bool& TransformationFlags::OptimizeX()
{
  return At(3);
}

//------------------------------------------------------------------------------
inline bool& TransformationFlags::OptimizeY()
{
  return At(4);
}

//------------------------------------------------------------------------------
inline bool& TransformationFlags::OptimizeZ()
{
  return At(5);
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
inline void TransformationFlags::SetOptimizeRotation(bool const value)
{
  OptimizePan () = value;
  OptimizeTilt() = value;
  OptimizeRoll() = value;
}

//------------------------------------------------------------------------------
inline void TransformationFlags::SetOptimizeTranslation(bool const value)
{
  OptimizeX() = value;
  OptimizeY() = value;
  OptimizeZ() = value;
}

//==============================================================================
// Static member functions
//------------------------------------------------------------------------------
inline TransformationFlags TransformationFlags::Enabled()
{
  return TransformationFlags(true);
}

//------------------------------------------------------------------------------
inline TransformationFlags TransformationFlags::Disabled()
{
  return TransformationFlags(false);
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_flags_TransformationFlags_inline_h_
