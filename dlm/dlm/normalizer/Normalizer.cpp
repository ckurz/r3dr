//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/normalizer/Normalizer.h"

//------------------------------------------------------------------------------
#include "dlm/math/MatrixFunctions.h"
#include "dlm/math/VectorFunctions.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/math/Vector.inline.h"
#include "dlm/math/Matrix.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Types.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <cmath>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
Matrix3f Normalizer::Calculate(std::vector<Vector2f> const& points)
{
  DLM_ASSERT(points.size() > 1,DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Vector2f centroid;

  for (auto it = points.cbegin(); it != points.cend(); ++it)
  {
    centroid += *it;
  }

  centroid /= static_cast<FloatType>(points.size());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FloatType distance = static_cast<FloatType>(0.0);

  for (auto it = points.cbegin(); it != points.cend(); ++it)
  {
    distance += (*it - centroid).Magnitude();
  }

  distance /= static_cast<FloatType>(points.size());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DLM_ASSERT(distance > Epsilon,DLM_DOMAIN_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FloatType const scale = static_cast<FloatType>(M_SQRT2) / distance;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Calculate normalization matrix
  Matrix3f normalization;

  normalization(0, 0) = scale;
  normalization(1, 1) = scale;

  MF::SetCol(normalization, 2, VF::Homogenize(-centroid * scale));

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return normalization;
}

//------------------------------------------------------------------------------
Matrix4f Normalizer::Calculate(std::vector<Vector3f> const& points)
{
  static FloatType const sqrt3 = static_cast<FloatType>(std::sqrt(3.0));

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DLM_ASSERT(points.size() > 1,DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Vector3f centroid;

  for (auto it = points.cbegin(); it != points.cend(); ++it)
  {
    centroid += *it;
  }

  centroid /= static_cast<FloatType>(points.size());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FloatType distance = static_cast<FloatType>(0.0);

  for (auto it = points.cbegin(); it != points.cend(); ++it)
  {
    distance += (*it - centroid).Magnitude();
  }

  distance /= static_cast<FloatType>(points.size());

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DLM_ASSERT(distance > Epsilon,DLM_DOMAIN_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  FloatType const scale = sqrt3 / distance;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Calculate normalization matrix
  Matrix4d normalization;

  normalization(0, 0) = scale;
  normalization(1, 1) = scale;
  normalization(2, 2) = scale;

  MF::SetCol(normalization, 3, VF::Homogenize(-centroid * scale));

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return normalization;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
