//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Authors: Aref Ariyapour
//          Christian Kurz
//          Valentin Savenko

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_ransac_RANSAC_inline_h_
#define dlm_ransac_RANSAC_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/ransac/RANSAC.h"

//------------------------------------------------------------------------------
#include "dlm/ransac/PseudorandomNumberGenerator.inline.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Exception.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <algorithm>
#include <limits>
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <cmath>

#include <iostream>
#include <iomanip>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Destructor
//------------------------------------------------------------------------------
template <typename Model>
inline RANSAC<Model>::~RANSAC()
{}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
template <typename Model>
inline std::set<uint32> RANSAC<Model>::Select()
{
  // Try not to create an infinite loop.
  DLM_ASSERT(NumberOfMSSSamples() < NumberOfSamples(), DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  std::set<uint32> result;

  while (result.size() < NumberOfMSSSamples())
  {
    result.insert(prng_.Rand(uint32(NumberOfSamples())));
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return result;
}

//------------------------------------------------------------------------------
template <typename Model>
inline typename RANSAC<Model>::Result RANSAC<Model>::Run(
  FloatType const threshold,
  uint64    const minIterations,
  uint64    const maxIterations,
  FloatType const probability,
  FloatType const sdFactor)
{
  DLM_ASSERT(minIterations <= maxIterations, DLM_RUNTIME_ERROR);
  DLM_ASSERT(maxIterations >  0,             DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Result best;

  bool present = false;

  {
    uint64 currentMaxIterations = maxIterations;
    
    uint64 iteration  = 0;
    uint64 degenerate = 0;
    
    while ( iteration < minIterations ||
           (iteration < maxIterations && iteration < currentMaxIterations))
    {
      ++iteration;

      // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      Result current;

      try
      {
        current = ChooseBestModel(Create(Select()), threshold);
      }
      catch (DegenerateSet const&)
      {
        ++degenerate;    
              
        if (degenerate < maxIterations) { continue; }
        
        break;
      }

      // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      if (present)
      {
        // There's a number of possibilities to decide whether the now set
        // should supersede the old one or not. PCL just evaluates the summed up
        // value for MSAC, but the MATLAB RANSAC Toolbox also checks whether the
        // new number of inliers is higher or equal to the old one. This method
        // is also used here.
        if (current.second.first.size() >= best.second.first.size() &&
            current.second.second       <  best.second.second)
        {
          best = current;
        }
      }
      else
      {
        best = current;
        
        present = true;
      }
    
      // Update adaptive maximum number of iterations.
      currentMaxIterations =
        CalculateMaxIterations(best.second.first.size(), probability, sdFactor);

      // TODO: Remove debug output
      {
        //std::cout << std::setw(6) << iteration << " " << std::setw(8) << currentMaxIterations << " | ";
        //std::cout << std::setw(6) << best.second.first.size() << " " << best.second.second << std::endl;
      }
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DLM_ASSERT(present, DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return best;
}

//------------------------------------------------------------------------------
template <typename Model>
inline typename RANSAC<Model>::Result RANSAC<Model>::ChooseBestModel(
  std::vector<Model> const& models, FloatType const& threshold) const
{
  DLM_ASSERT(models.size() > 0, DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint64 index = 0;
  
  Evaluation best = Evaluate(models.at(index), threshold);

  for (uint64 i = 1; i < models.size(); ++i)
  {
    Evaluation current = Evaluate(models.at(i), threshold);
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // See above for the rationale of this choice of evaluation.
    if (current.first.size() >= best.first.size() &&
        current.second       <  best.second)
    {
      best = current;
      
      index = i;
    }
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return Result(models.at(index), best);
}

//------------------------------------------------------------------------------
template <typename Model>
inline uint64 RANSAC<Model>::CalculateMaxIterations(
  uint64    const inliers,
  FloatType const probability,
  FloatType const sdFactor) const
{
  uint64 const    samples = NumberOfSamples();
  uint64 const mssSamples = NumberOfMSSSamples();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  DLM_ASSERT(mssSamples >  0,       DLM_RUNTIME_ERROR);
//  DLM_ASSERT(mssSamples <= inliers, RUNTIME_ERROR);
  DLM_ASSERT(inliers    <= samples, DLM_RUNTIME_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Specify the desired probability p for a good result, i.e., the probability
  // that during at least one iteration of the whole RANSAC procedure a set with
  // no outliers should be selected. 
  FloatType const p = probability;

  // Calculate an estimate of the probability w of selecting an inlier from the
  // whole set using the number of samples currently supporting the result.
  FloatType const w = FloatType(inliers) / FloatType(samples);

  // Estimate of the probability w^n to select a set consisting of inliers only.
  FloatType wn = pow(w, FloatType(mssSamples));
  
  // Avoid division by -Inf and by 0. Thanks to the PCL guys for that.
  wn = std::max(                 std::numeric_limits<FloatType>::epsilon(), wn);
  wn = std::min(FloatType(1.0) - std::numeric_limits<FloatType>::epsilon(), wn);

  // Estimate the new maximum number of trials k assuming that with a
  // probability p at least one set free of outliers shall be selected.
  // The equation boils down to: 1 - p = (1 - w^n)^k
  // Taking the logarithm on both sides yields: k = log(1 - p) / log(1 - w^n)
  FloatType const k = log(FloatType(1.0) - p) / log(FloatType(1.0) - wn);
  // This assumes that the samples are selected independently, which means that
  // samples can be selected repeatedly during the same trial. This is not the
  // case for this implementation, which makes k an upper bound.

  // Wikipedia suggests adding multiples of the standard deviation of k to k in
  // order to gain additional confidence. The standard deviation amounts to an
  // approximate five percent (~4.6 for high outlier percentages, more for less
  // outliers, depending on n). Can't hurt...
  FloatType const sd = sqrt(FloatType(1.0) - wn) / wn;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return uint64(k + sdFactor * sd);
}
  
//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_ransac_RANSAC_inline_h_
