//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Authors: Aref Ariyapour
//          Christian Kurz
//          Valentin Savenko

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_ransac_RANSAC_h_
#define dlm_ransac_RANSAC_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/ransac/PseudorandomNumberGenerator.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include "dlm/core/Types.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <type_traits>
#include <vector>
#include <set>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// RANSAC template class
//------------------------------------------------------------------------------
template <typename Model>
class RANSAC
{
public:
  //============================================================================
  // Nested
  //----------------------------------------------------------------------------
  typedef std::pair<std::set<uint32>, FloatType> Evaluation;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  typedef std::pair<Model, Evaluation> Result;

  //============================================================================
  // Destructor
  //----------------------------------------------------------------------------
  virtual ~RANSAC();

  //============================================================================
  // Abstract member functions
  //----------------------------------------------------------------------------
  virtual uint64 NumberOfSamples   () const = 0;
  virtual uint64 NumberOfMSSSamples() const = 0;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  virtual std::vector<Model> Create(
    std::set<uint32> const& selection) const = 0;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  virtual Evaluation Evaluate(
    Model const& model, FloatType const threshold) const = 0;

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  Result Run(
    FloatType const threshold,
    uint64    const minIterations = 32,
    uint64    const maxIterations = 2048,
    FloatType const probability = FloatType(0.99),
    FloatType const sdFactor    = FloatType(1.00));
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  std::set<uint32> Select();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Result ChooseBestModel(
    std::vector<Model> const& models, FloatType const& threshold) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  uint64 CalculateMaxIterations(
    uint64    const inliers,
    FloatType const probability,
    FloatType const sdFactor) const;

  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  PRNG prng_;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_ransac_RANSAC_h_
