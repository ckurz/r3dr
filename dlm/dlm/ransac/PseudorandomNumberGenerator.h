//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_ransac_PseudorandomNumberGenerator_h_
#define dlm_ransac_PseudorandomNumberGenerator_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/core/Types.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <type_traits>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// PseudorandomNumberGenerator template class
//
// This class generates uniformly distributed pseudorandom numbers.
// It is based on xorshift by George Marsaglia
// (see http://de.wikipedia.org/wiki/Xorshift).
//------------------------------------------------------------------------------
template <typename T>
class PseudorandomNumberGenerator
{
  //============================================================================
  // Type restriction
  //----------------------------------------------------------------------------
  static_assert(std::is_floating_point<T>::value,
                "PseudorandomNumberGenerator requires a floating point type "
                "as template argument.");

public:
  //============================================================================
  // Nested
  //----------------------------------------------------------------------------
  static uint32 const RandMax = uint32(-1);

  //============================================================================
  // Constructors
  //----------------------------------------------------------------------------
  PseudorandomNumberGenerator(uint32 const x = 123456789,
                              uint32 const y = 362436069,
                              uint32 const z = 521288629,
                              uint32 const w =  88675123);

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  uint32 Rand();

  //----------------------------------------------------------------------------
  // Convenience functions
  // These functions calculate the pseudorandom numbers in the following
  // intervals: [0,   range)
  //            [low, high )
  //----------------------------------------------------------------------------
  uint32 Rand(uint32 const range);
  uint32 Rand(uint32 const low, uint32 const high);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  T RandF(T const range = T(1.0));
  T RandF(T const low, T const high);

private:
  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  uint32 x_;
  uint32 y_;
  uint32 z_;
  uint32 w_;
};

//==============================================================================
// Shorthand
//------------------------------------------------------------------------------
typedef PseudorandomNumberGenerator<FloatType> PRNG;

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_ransac_PseudorandomNumberGenerator_h_
