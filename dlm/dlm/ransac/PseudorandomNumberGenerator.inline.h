//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/dlm library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef dlm_ransac_PseudorandomNumberGenerator_inline_h_
#define dlm_ransac_PseudorandomNumberGenerator_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "dlm/ransac/PseudorandomNumberGenerator.h"

//------------------------------------------------------------------------------
#include "dlm/core/Exception.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace dlm
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
template <typename T>
inline PseudorandomNumberGenerator<T>::PseudorandomNumberGenerator(
  uint32 const x, uint32 const y, uint32 const z, uint32 const w)
  : x_(x)
  , y_(y)
  , z_(z)
  , w_(w)
{}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
template <typename T>
inline uint32 PseudorandomNumberGenerator<T>::Rand()
{
  uint32 const t = x_ ^ (x_ << 11);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  x_ = y_;
  y_ = z_;
  z_ = w_;

  w_ ^= (w_ >> 19) ^ (t ^ (t >> 8));

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return w_;
}

//------------------------------------------------------------------------------
template <typename T>
inline uint32 PseudorandomNumberGenerator<T>::Rand(uint32 const range)
{
  return uint32(RandF(range));
}

//------------------------------------------------------------------------------
template <typename T>
inline uint32 PseudorandomNumberGenerator<T>::Rand(
  uint32 const low, uint32 const high)
{
  return uint32(RandF(low, high));
}

//------------------------------------------------------------------------------
template <typename T>
inline T PseudorandomNumberGenerator<T>::RandF(T const range)
{
  return RandF(T(0.0), range);
}

//------------------------------------------------------------------------------
template <typename T>
inline T PseudorandomNumberGenerator<T>::RandF(T const low, T const high)
{
  DLM_ASSERT(low < high, DLM_DOMAIN_ERROR);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return low + Rand() / (RandMax + T(1.0)) * (high - low);
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
} //namespace dlm

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif //dlm_ransac_PseudorandomNumberGenerator_inline_h_
