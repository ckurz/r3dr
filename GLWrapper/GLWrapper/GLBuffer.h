//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/GLWrapper library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef GLWrapper_GLBuffer_h_
#define GLWrapper_GLBuffer_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "GLWrapper/GLWrapper.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace GLWrapper
{

//==============================================================================
// GLBuffer class
//------------------------------------------------------------------------------
class GLBuffer
{
public:
  //============================================================================
  // Constructors
  //----------------------------------------------------------------------------
  GLBuffer();

  //============================================================================
  // Accessors
  //----------------------------------------------------------------------------
  GLuint Id    () const;
  GLenum Target() const;

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  void Create(GLenum const target = GL_ARRAY_BUFFER);
  void Delete();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void   Bind() const;
  void Unbind() const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void    Data(
    GLsizeiptr const  size,
    GLvoid     const* data, 
    GLenum     const  usage = GL_STATIC_DRAW);
  void SubData(
    GLsizeiptr const  size,
    GLvoid     const* data,
    GLintptr   const  offset = 0);

private:
  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  GLuint buffer_;
  GLenum target_;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  GLsizeiptr size_;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
