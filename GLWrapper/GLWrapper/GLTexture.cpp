//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/GLWrapper library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "GLWrapper/GLTexture.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace GLWrapper
{

//==============================================================================
// GLTexture class
//------------------------------------------------------------------------------
GLTexture::GLTexture()
  : id_    (0)
  , width_ (0)
  , height_(0)
{}

//==============================================================================
// Accessors
//------------------------------------------------------------------------------
GLuint GLTexture::Id() const
{
  return id_;
}

//------------------------------------------------------------------------------
GLsizei GLTexture::Width() const
{
  return width_;
}

//------------------------------------------------------------------------------
GLsizei GLTexture::Height() const
{
  return height_;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
void GLTexture::Create()
{
  Delete();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  glGenTextures(1, &id_);
}

//------------------------------------------------------------------------------
void GLTexture::Delete()
{
  // Much like delete, glDeleteBuffers silently ignores 0's.
  glDeleteTextures(1, &id_);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  id_     = 0;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  width_  = 0;
  height_ = 0;
}

//------------------------------------------------------------------------------
void GLTexture::Bind() const
{
//  DLM_ASSERT(id_, RUNTIME_ERROR);

  glBindTexture(GL_TEXTURE_2D, id_);
}

//------------------------------------------------------------------------------
void GLTexture::Unbind() const
{
  glBindTexture(GL_TEXTURE_2D, 0);
}

//------------------------------------------------------------------------------
void GLTexture::Create(
  GLsizei        const width,
  GLsizei        const height,
  GLubyte const* const data,
  bool           const mipmap)
{
  Create();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  width_  = width;
  height_ = height;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Bind();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  if (mipmap)
  {
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  }
  else
  {
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  }

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

  glTexImage2D(
    GL_TEXTURE_2D, 0, GL_RGB8, width_, height_, 0, GL_RGB, GL_UNSIGNED_BYTE, data);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (mipmap)
  {
    gl::GenerateMipmap(GL_TEXTURE_2D);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Unbind();
}

//------------------------------------------------------------------------------
void GLTexture::GetImage(GLubyte* const data)
{
  Bind();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  glGetTexImage(GL_TEXTURE_2D, 0, GL_RGB, GL_UNSIGNED_BYTE, data);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Unbind();
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
