//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/GLWrapper library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "GLWrapper/GLShader.h"

//------------------------------------------------------------------------------
#include <iostream>
#include <fstream>
#include <sstream>
#include <memory>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace GLWrapper
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
GLShader::GLShader()
  : type_(0)
  , id_  (0)
{}

//==============================================================================
// Accessors
//------------------------------------------------------------------------------
GLuint GLShader::Id() const
{
  return id_;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
void GLShader::Create(GLuint type, const std::string& filename)
{
  Delete();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  type_ = type;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Load(filename);
  
  Compile();
}

//------------------------------------------------------------------------------
void GLShader::Delete()
{
  gl::DeleteShader(id_);

  id_   = 0;
  type_ = 0;
}


//------------------------------------------------------------------------------
void GLShader::Load(const std::string& filename)
{
  std::stringstream fileData;

  {
    std::ifstream file(filename.c_str());

    if (!file.is_open())
    {
      throw(std::string("Shader load file open failed"));
    }

    fileData << file.rdbuf();

    file.close();
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  std::string shaderSource = fileData.str();

  if (shaderSource.empty())
  {
    throw(std::string("Shader load empty string"));
  }

  id_ = gl::CreateShader(type_);

  const char* shaderString = shaderSource.c_str();
  GLint length = static_cast<GLint>(shaderSource.size());

  gl::ShaderSource(id_, 1, (const GLchar **)&shaderString, &length);
}

//------------------------------------------------------------------------------
void GLShader::Compile()
{
  GLint compiled = 0;

  gl::CompileShader(id_);

  gl::GetShaderiv(id_, GL_COMPILE_STATUS, &compiled);

  if (compiled == GL_FALSE)
  {
    PrintInfoLog();
    throw("Shader compile failed");
  }
}


//------------------------------------------------------------------------------
void GLShader::PrintInfoLog()
{
  int logLength = 0;

  gl::GetShaderiv(id_, GL_INFO_LOG_LENGTH, &logLength);

  if (logLength)
  {
    GLchar* log = new GLchar[logLength];
    int written = 0;

    gl::GetShaderInfoLog(id_, logLength, &written, log);

    std::cout << "InfoLog: " << std::endl;
    std::cout << log << std::endl;

    delete[] log;

    //std::unique_ptr<GLchar[]> log(new GLchar[logLength]);
    //int written = 0;

    //gl::GetShaderInfoLog(id_, logLength, &written, log.get());

    //std::cout << "InfoLog: " << std::endl;
    //std::cout << log << std::endl;
  }
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
