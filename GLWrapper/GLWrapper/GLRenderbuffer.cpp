//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/GLWrapper library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "GLWrapper/GLRenderbuffer.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace GLWrapper
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
GLRenderbuffer::GLRenderbuffer()
  : id_(0)
{}

//==============================================================================
// Accessors
//------------------------------------------------------------------------------
GLuint GLRenderbuffer::Id() const
{
  return id_;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
void GLRenderbuffer::Create()
{
  Delete();

  gl::GenRenderbuffers(1, &id_);
}

//------------------------------------------------------------------------------
void GLRenderbuffer::Delete()
{
  gl::DeleteRenderbuffers(1, &id_);

  id_ = 0;
}

//------------------------------------------------------------------------------
void GLRenderbuffer::Bind() const
{
//  DLM_ASSERT(id_, RUNTIME_ERROR);

  gl::BindRenderbuffer(GL_RENDERBUFFER, id_);
}

//------------------------------------------------------------------------------
void GLRenderbuffer::Unbind() const
{
  gl::BindRenderbuffer(GL_RENDERBUFFER, 0);
}

//------------------------------------------------------------------------------
GLenum GLRenderbuffer::Format() const
{
  Bind();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  GLint format = 0;

  gl::GetRenderbufferParameteriv(
    GL_RENDERBUFFER, GL_RENDERBUFFER_INTERNAL_FORMAT, &format);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Unbind();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return format;
}

//------------------------------------------------------------------------------
void GLRenderbuffer::Storage(
  GLsizei const width,
  GLsizei const height,
  GLenum  const format,
  GLsizei const samples) const
{
  Bind();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  if (samples == 0)
  {
    gl::RenderbufferStorage(GL_RENDERBUFFER, format, width, height);
  }
  else
  {
    gl::RenderbufferStorageMultisample(
      GL_RENDERBUFFER, samples, format, width, height);
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Unbind();
}

//------------------------------------------------------------------------------
void GLRenderbuffer::Create(
  GLsizei const width,
  GLsizei const height,
  GLenum  const format,
  GLsizei const samples)
{
  Create();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Storage(width, height, format, samples);
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
