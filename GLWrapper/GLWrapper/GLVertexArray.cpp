//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/GLWrapper library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "GLWrapper/GLVertexArray.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace GLWrapper
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
GLVertexArray::GLVertexArray()
  : vertexArray_(0)
{}

//==============================================================================
// Accessors
//------------------------------------------------------------------------------
GLuint GLVertexArray::Id() const
{
  return vertexArray_;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
void GLVertexArray::Create()
{
  Delete();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  GLWrapper::GenVertexArrays(1, &vertexArray_);
}

//------------------------------------------------------------------------------
void GLVertexArray::Delete()
{
  // Much like delete, glDeleteBuffers silently ignores 0's.
  gl::DeleteVertexArrays(1, &vertexArray_);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  vertexArray_ = 0;
}

//------------------------------------------------------------------------------
void GLVertexArray::Bind() const
{
  gl::BindVertexArray(vertexArray_);
}

//------------------------------------------------------------------------------
void GLVertexArray::Unbind() const
{
  gl::BindVertexArray(0);
}

//------------------------------------------------------------------------------
void GLVertexArray::Attach(
  GLBuffer  const& buffer,
  GLuint    const  index,
  GLint     const  size,
  GLenum    const  type,
  GLboolean const  normalized,
  GLsizei   const  stride,
  GLvoid    const* pointer)
{
  if (buffer.Target() != GL_ARRAY_BUFFER)
  {
    throw "Wrong buffer target.";
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Bind();
  buffer.Bind();

  gl::VertexAttribPointer(index, size, type, normalized, stride, pointer);
  gl::EnableVertexAttribArray(index);

  buffer.Unbind();
  Unbind();
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
