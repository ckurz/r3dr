//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/GLWrapper library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "GLWrapper/GLFramebuffer.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace GLWrapper
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
GLFramebuffer::GLFramebuffer()
  : id_    (0)
  , target_(0)
{}

//==============================================================================
// Accessors
//------------------------------------------------------------------------------
GLuint GLFramebuffer::Id() const
{
  return id_;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
void GLFramebuffer::Create()
{
  Delete();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  gl::GenFramebuffers(1, &id_);
}

//------------------------------------------------------------------------------
void GLFramebuffer::Delete()
{
  gl::DeleteFramebuffers(1, &id_);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  id_     = 0;
  target_ = 0;
}

//------------------------------------------------------------------------------
void GLFramebuffer::Bind(GLenum const target)
{
//  DLM_ASSERT(id_, RUNTIME_ERROR);

  gl::BindFramebuffer(target, id_);

  target_ = target;
}

//------------------------------------------------------------------------------
void GLFramebuffer::Unbind()
{
  if (target_ == 0) { return; }

  gl::BindFramebuffer(target_, 0);

  target_ = 0;
}

//------------------------------------------------------------------------------
void GLFramebuffer::Attach(GLRenderbuffer const& renderbuffer)
{
  GLenum attachment = 0;

  switch(renderbuffer.Format())
  {
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  case GL_DEPTH_COMPONENT:
    {
      attachment = GL_DEPTH_ATTACHMENT;
      break;
    }
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  case GL_DEPTH_STENCIL:
    {
      attachment = GL_DEPTH_STENCIL_ATTACHMENT;
      break;
    }
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  case GL_RGB8:
  case GL_RGBA8:
    {
      attachment = GL_COLOR_ATTACHMENT0;
      break;
    }
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  }

//  DLM_ASSERT(attachment, RUNTIME_ERROR);

  Attach(renderbuffer, attachment);
}

//------------------------------------------------------------------------------
void GLFramebuffer::Attach(
  GLRenderbuffer const& renderbuffer, GLenum const attachment)
{
//  DLM_ASSERT(target_,           RUNTIME_ERROR);
//  DLM_ASSERT(renderbuffer.Id(), RUNTIME_ERROR);

  gl::FramebufferRenderbuffer(
    target_, attachment, GL_RENDERBUFFER, renderbuffer.Id());
}

//------------------------------------------------------------------------------
void GLFramebuffer::Attach(GLTexture const& texture, GLenum  const attachment)
{
//  DLM_ASSERT(target_,      RUNTIME_ERROR);
//  DLM_ASSERT(texture.Id(), RUNTIME_ERROR);

  gl::FramebufferTexture2D(target_, attachment, GL_TEXTURE_2D, texture.Id(), 0);
}

//------------------------------------------------------------------------------
void GLFramebuffer::Detach(GLenum const attachment)
{
  if (target_ == 0) { return; }

  gl::FramebufferRenderbuffer(target_, attachment, GL_RENDERBUFFER, 0);
}

//------------------------------------------------------------------------------
bool GLFramebuffer::CheckStatus()
{
  GLenum const status = gl::CheckFramebufferStatus(target_);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return status == GL_FRAMEBUFFER_COMPLETE;
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
