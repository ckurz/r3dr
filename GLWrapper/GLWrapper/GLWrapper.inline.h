//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/GLWrapper library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef GLWrapper_GLWrapper_inline_h_
#define GLWrapper_GLWrapper_inline_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "GLWrapper/GLWrapper.h"

//------------------------------------------------------------------------------
#include <iostream>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace GLWrapper
{

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
#if defined WIN32
//{
template <typename T>
inline bool GLWrapper::RegisterFunctionPointer(
  T& variable, char const* const functionName)
{
  variable = reinterpret_cast<T>(wglGetProcAddress(functionName));

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool const success = variable != nullptr;

  if (!success)
  {
    std::cout << "Failed for " << functionName << std::endl;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return success;
}
//}
#else
//{
template <typename T>
inline bool GLWrapper::RegisterFunctionPointer(
  T& variable, char const* const functionName)
{
  variable = reinterpret_cast<T>(
    glXGetProcAddress(reinterpret_cast<GLubyte const* const>(functionName)));

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  bool const success = variable != nullptr;

  if (!success)
  {
    std::cout << "Failed for " << functionName << std::endl;
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return success;
}
//}
#endif

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
