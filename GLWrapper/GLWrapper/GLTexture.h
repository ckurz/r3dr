//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/GLWrapper library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef GLWrapper_GLTexture_h_
#define GLWrapper_GLTexture_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "GLWrapper/GLWrapper.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace GLWrapper
{

//==============================================================================
// GLTexture class
//------------------------------------------------------------------------------
class GLTexture
{
public:
  //============================================================================
  // Constructors
  //----------------------------------------------------------------------------
  GLTexture();

  //============================================================================
  // Accessors
  //----------------------------------------------------------------------------
  GLuint Id() const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  GLsizei Width () const;
  GLsizei Height() const;

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  void Create();
  void Delete();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Bind  () const;
  void Unbind() const;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Create(
    GLsizei        const width,
    GLsizei        const height,
    GLubyte const* const data = 0,
    bool           const mipmap = false);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void GetImage(GLubyte* const data);

private:
  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  GLuint id_;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  GLsizei width_;
  GLsizei height_;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
