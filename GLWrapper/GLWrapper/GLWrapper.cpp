//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.nullptr. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.nullptr/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2nullptr13  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/GLWrapper library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "GLWrapper/GLWrapper.h"
#include "GLWrapper/GLWrapper.inline.h"

//------------------------------------------------------------------------------
#include <iostream>
#include <sstream>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace GLWrapper
{

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
bool GLWrapper::Initialize()
{
  bool success = true;
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(IsRenderbuffer,
                                            "glIsRenderbuffer");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(BindRenderbuffer,
                                            "glBindRenderbuffer");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(DeleteRenderbuffers,
                                            "glDeleteRenderbuffers");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(GenRenderbuffers,
                                            "glGenRenderbuffers");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(RenderbufferStorage,
                                            "glRenderbufferStorage");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(GetRenderbufferParameteriv,
                                            "glGetRenderbufferParameteriv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(IsFramebuffer,
                                            "glIsFramebuffer");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(BindFramebuffer,
                                            "glBindFramebuffer");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(DeleteFramebuffers,
                                            "glDeleteFramebuffers");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(GenFramebuffers,
                                            "glGenFramebuffers");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(CheckFramebufferStatus,
                                            "glCheckFramebufferStatus");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(FramebufferTexture1D,
                                            "glFramebufferTexture1D");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(FramebufferTexture2D,
                                            "glFramebufferTexture2D");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(FramebufferTexture3D,
                                            "glFramebufferTexture3D");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(FramebufferRenderbuffer,
                                            "glFramebufferRenderbuffer");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(GetFramebufferAttachmentParameteriv,
                                            "glGetFramebufferAttachmentParameteriv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(GenerateMipmap,
                                            "glGenerateMipmap");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(BlitFramebuffer,
                                            "glBlitFramebuffer");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(RenderbufferStorageMultisample,
                                            "glRenderbufferStorageMultisample");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(FramebufferTextureLayer,
                                            "glFramebufferTextureLayer");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(BindVertexArray,
                                            "glBindVertexArray");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(DeleteVertexArrays,
                                            "glDeleteVertexArrays");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(GenVertexArrays,
                                            "glGenVertexArrays");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(GenQueries,
                                            "glGenQueries");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(DeleteQueries,
                                            "glDeleteQueries");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(IsQuery,
                                            "glIsQuery");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(BeginQuery,
                                            "glBeginQuery");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(EndQuery,
                                            "glEndQuery");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(GetQueryiv,
                                            "glGetQueryiv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(GetQueryObjectiv,
                                            "glGetQueryObjectiv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(GetQueryObjectuiv,
                                            "glGetQueryObjectuiv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(BindBuffer,
                                            "glBindBuffer");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(DeleteBuffers,
                                            "glDeleteBuffers");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(GenBuffers,
                                            "glGenBuffers");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(IsBuffer,
                                            "glIsBuffer");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(BufferData,
                                            "glBufferData");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(BufferSubData,
                                            "glBufferSubData");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(GetBufferSubData,
                                            "glGetBufferSubData");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(MapBuffer,
                                            "glMapBuffer");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(UnmapBuffer,
                                            "glUnmapBuffer");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(GetBufferParameteriv,
                                            "glGetBufferParameteriv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(GetBufferPointerv,
                                            "glGetBufferPointerv");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(BlendEquationSeparate,
                                            "glBlendEquationSeparate");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(DrawBuffers,
                                            "glDrawBuffers");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(StencilOpSeparate,
                                            "glStencilOpSeparate");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(StencilFuncSeparate,
                                            "glStencilFuncSeparate");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(StencilMaskSeparate,
                                            "glStencilMaskSeparate");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(AttachShader,
                                            "glAttachShader");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(BindAttribLocation,
                                            "glBindAttribLocation");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(CompileShader,
                                            "glCompileShader");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(CreateProgram,
                                            "glCreateProgram");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(CreateShader,
                                            "glCreateShader");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(DeleteProgram,
                                            "glDeleteProgram");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(DeleteShader,
                                            "glDeleteShader");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(DetachShader,
                                            "glDetachShader");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(DisableVertexAttribArray,
                                            "glDisableVertexAttribArray");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(EnableVertexAttribArray,
                                            "glEnableVertexAttribArray");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(GetActiveAttrib,
                                            "glGetActiveAttrib");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(GetActiveUniform,
                                            "glGetActiveUniform");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(GetAttachedShaders,
                                            "glGetAttachedShaders");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(GetAttribLocation,
                                            "glGetAttribLocation");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(GetProgramiv,
                                            "glGetProgramiv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(GetProgramInfoLog,
                                            "glGetProgramInfoLog");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(GetShaderiv,
                                            "glGetShaderiv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(GetShaderInfoLog,
                                            "glGetShaderInfoLog");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(GetShaderSource,
                                            "glGetShaderSource");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(GetUniformLocation,
                                            "glGetUniformLocation");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(GetUniformfv,
                                            "glGetUniformfv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(GetUniformiv,
                                            "glGetUniformiv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(GetVertexAttribdv,
                                            "glGetVertexAttribdv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(GetVertexAttribfv,
                                            "glGetVertexAttribfv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(GetVertexAttribiv,
                                            "glGetVertexAttribiv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(GetVertexAttribPointerv,
                                            "glGetVertexAttribPointerv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(IsProgram,
                                            "glIsProgram");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(IsShader,
                                            "glIsShader");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(LinkProgram,
                                            "glLinkProgram");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(ShaderSource,
                                            "glShaderSource");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(UseProgram,
                                            "glUseProgram");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(Uniform1f,
                                            "glUniform1f");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(Uniform2f,
                                            "glUniform2f");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(Uniform3f,
                                            "glUniform3f");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(Uniform4f,
                                            "glUniform4f");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(Uniform1i,
                                            "glUniform1i");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(Uniform2i,
                                            "glUniform2i");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(Uniform3i,
                                            "glUniform3i");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(Uniform4i,
                                            "glUniform4i");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(Uniform1fv,
                                            "glUniform1fv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(Uniform2fv,
                                            "glUniform2fv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(Uniform3fv,
                                            "glUniform3fv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(Uniform4fv,
                                            "glUniform4fv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(Uniform1iv,
                                            "glUniform1iv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(Uniform2iv,
                                            "glUniform2iv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(Uniform3iv,
                                            "glUniform3iv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(Uniform4iv,
                                            "glUniform4iv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(UniformMatrix2fv,
                                            "glUniformMatrix2fv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(UniformMatrix3fv,
                                            "glUniformMatrix3fv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(UniformMatrix4fv,
                                            "glUniformMatrix4fv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(ValidateProgram,
                                            "glValidateProgram");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib1d,
                                            "glVertexAttrib1d");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib1dv,
                                            "glVertexAttrib1dv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib1f,
                                            "glVertexAttrib1f");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib1fv,
                                            "glVertexAttrib1fv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib1s,
                                            "glVertexAttrib1s");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib1sv,
                                            "glVertexAttrib1sv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib2d,
                                            "glVertexAttrib2d");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib2dv,
                                            "glVertexAttrib2dv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib2f,
                                            "glVertexAttrib2f");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib2fv,
                                            "glVertexAttrib2fv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib2s,
                                            "glVertexAttrib2s");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib2sv,
                                            "glVertexAttrib2sv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib3d,
                                            "glVertexAttrib3d");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib3dv,
                                            "glVertexAttrib3dv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib3f,
                                            "glVertexAttrib3f");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib3fv,
                                            "glVertexAttrib3fv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib3s,
                                            "glVertexAttrib3s");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib3sv,
                                            "glVertexAttrib3sv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib4Nbv,
                                            "glVertexAttrib4Nbv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib4Niv,
                                            "glVertexAttrib4Niv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib4Nsv,
                                            "glVertexAttrib4Nsv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib4Nub,
                                            "glVertexAttrib4Nub");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib4Nubv,
                                            "glVertexAttrib4Nubv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib4Nuiv,
                                            "glVertexAttrib4Nuiv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib4Nusv,
                                            "glVertexAttrib4Nusv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib4bv,
                                            "glVertexAttrib4bv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib4d,
                                            "glVertexAttrib4d");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib4dv,
                                            "glVertexAttrib4dv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib4f,
                                            "glVertexAttrib4f");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib4fv,
                                            "glVertexAttrib4fv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib4iv,
                                            "glVertexAttrib4iv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib4s,
                                            "glVertexAttrib4s");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib4sv,
                                            "glVertexAttrib4sv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib4ubv,
                                            "glVertexAttrib4ubv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib4uiv,
                                            "glVertexAttrib4uiv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttrib4usv,
                                            "glVertexAttrib4usv");
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(VertexAttribPointer,
                                            "glVertexAttribPointer");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  success = success && RegisterFunctionPointer(DrawElementsBaseVertex,
                                            "glDrawElementsBaseVertex");

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return success;
}

//------------------------------------------------------------------------------
GLint GLWrapper::CheckForErrors()
{
  GLint counter = 0;

  for (GLenum currentError = glGetError(); currentError != GL_NO_ERROR;
    currentError = glGetError())
  {
    std::stringstream message;

    switch (currentError)
    {
    case GL_INVALID_ENUM:
      {
        message << "GL_INVALID_ENUM";
        break;
      }
    case GL_INVALID_VALUE:
      {
        message << "GL_INVALID_VALUE";
        break;
      }
    case GL_INVALID_OPERATION:
      {
        message << "GL_INVALID_OPERATION";
        break;
      }
    case GL_STACK_OVERFLOW:
      {
        message << "GL_STACK_OVERFLOW";
        break;
      }
    case GL_STACK_UNDERFLOW:
      {
        message << "GL_STACK_UNDERFLOW";
        break;
      }
    case GL_OUT_OF_MEMORY:
      {
        message << "GL_OUT_OF_MEMORY";
        break;
      }
    case GL_INVALID_FRAMEBUFFER_OPERATION:
      {
        message << "GL_INVALID_FRAMEBUFFER_OPERATION";
        break;
      }
    case GL_TABLE_TOO_LARGE:
      {
        message << "GL_TABLE_TOO_LARGE";
        break;
      }
    case GL_INVALID_INDEX:
      {
        message << "GL_INVALID_INDEX";
        break;
      }
    default:
      {
        message << "UNKNOWN";
        break;
      }
    }

    ++counter;

    std::cout << message.str() << std::endl;
  }
  
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  return counter;
}

//==============================================================================
// Static member variable initialization
//------------------------------------------------------------------------------
PFNGLISRENDERBUFFERPROC                      GLWrapper::IsRenderbuffer                      = nullptr;
PFNGLBINDRENDERBUFFERPROC                    GLWrapper::BindRenderbuffer                    = nullptr;
PFNGLDELETERENDERBUFFERSPROC                 GLWrapper::DeleteRenderbuffers                 = nullptr;
PFNGLGENRENDERBUFFERSPROC                    GLWrapper::GenRenderbuffers                    = nullptr;
PFNGLRENDERBUFFERSTORAGEPROC                 GLWrapper::RenderbufferStorage                 = nullptr;
PFNGLGETRENDERBUFFERPARAMETERIVPROC          GLWrapper::GetRenderbufferParameteriv          = nullptr;
PFNGLISFRAMEBUFFERPROC                       GLWrapper::IsFramebuffer                       = nullptr;
PFNGLBINDFRAMEBUFFERPROC                     GLWrapper::BindFramebuffer                     = nullptr;
PFNGLDELETEFRAMEBUFFERSPROC                  GLWrapper::DeleteFramebuffers                  = nullptr;
PFNGLGENFRAMEBUFFERSPROC                     GLWrapper::GenFramebuffers                     = nullptr;
PFNGLCHECKFRAMEBUFFERSTATUSPROC              GLWrapper::CheckFramebufferStatus              = nullptr;
PFNGLFRAMEBUFFERTEXTURE1DPROC                GLWrapper::FramebufferTexture1D                = nullptr;
PFNGLFRAMEBUFFERTEXTURE2DPROC                GLWrapper::FramebufferTexture2D                = nullptr;
PFNGLFRAMEBUFFERTEXTURE3DPROC                GLWrapper::FramebufferTexture3D                = nullptr;
PFNGLFRAMEBUFFERRENDERBUFFERPROC             GLWrapper::FramebufferRenderbuffer             = nullptr;
PFNGLGETFRAMEBUFFERATTACHMENTPARAMETERIVPROC GLWrapper::GetFramebufferAttachmentParameteriv = nullptr;
PFNGLGENERATEMIPMAPPROC                      GLWrapper::GenerateMipmap                      = nullptr;
PFNGLBLITFRAMEBUFFERPROC                     GLWrapper::BlitFramebuffer                     = nullptr;
PFNGLRENDERBUFFERSTORAGEMULTISAMPLEPROC      GLWrapper::RenderbufferStorageMultisample      = nullptr;
PFNGLFRAMEBUFFERTEXTURELAYERPROC             GLWrapper::FramebufferTextureLayer             = nullptr;
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
PFNGLBINDVERTEXARRAYPROC                     GLWrapper::BindVertexArray                     = nullptr;
PFNGLDELETEVERTEXARRAYSPROC                  GLWrapper::DeleteVertexArrays                  = nullptr;
PFNGLGENVERTEXARRAYSPROC                     GLWrapper::GenVertexArrays                     = nullptr;
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
PFNGLGENQUERIESPROC                          GLWrapper::GenQueries                          = nullptr;
PFNGLDELETEQUERIESPROC                       GLWrapper::DeleteQueries                       = nullptr;
PFNGLISQUERYPROC                             GLWrapper::IsQuery                             = nullptr;
PFNGLBEGINQUERYPROC                          GLWrapper::BeginQuery                          = nullptr;
PFNGLENDQUERYPROC                            GLWrapper::EndQuery                            = nullptr;
PFNGLGETQUERYIVPROC                          GLWrapper::GetQueryiv                          = nullptr;
PFNGLGETQUERYOBJECTIVPROC                    GLWrapper::GetQueryObjectiv                    = nullptr;
PFNGLGETQUERYOBJECTUIVPROC                   GLWrapper::GetQueryObjectuiv                   = nullptr;
PFNGLBINDBUFFERPROC                          GLWrapper::BindBuffer                          = nullptr;
PFNGLDELETEBUFFERSPROC                       GLWrapper::DeleteBuffers                       = nullptr;
PFNGLGENBUFFERSPROC                          GLWrapper::GenBuffers                          = nullptr;
PFNGLISBUFFERPROC                            GLWrapper::IsBuffer                            = nullptr;
PFNGLBUFFERDATAPROC                          GLWrapper::BufferData                          = nullptr;
PFNGLBUFFERSUBDATAPROC                       GLWrapper::BufferSubData                       = nullptr;
PFNGLGETBUFFERSUBDATAPROC                    GLWrapper::GetBufferSubData                    = nullptr;
PFNGLMAPBUFFERPROC                           GLWrapper::MapBuffer                           = nullptr;
PFNGLUNMAPBUFFERPROC                         GLWrapper::UnmapBuffer                         = nullptr;
PFNGLGETBUFFERPARAMETERIVPROC                GLWrapper::GetBufferParameteriv                = nullptr;
PFNGLGETBUFFERPOINTERVPROC                   GLWrapper::GetBufferPointerv                   = nullptr;
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
PFNGLBLENDEQUATIONSEPARATEPROC               GLWrapper::BlendEquationSeparate               = nullptr;
PFNGLDRAWBUFFERSPROC                         GLWrapper::DrawBuffers                         = nullptr;
PFNGLSTENCILOPSEPARATEPROC                   GLWrapper::StencilOpSeparate                   = nullptr;
PFNGLSTENCILFUNCSEPARATEPROC                 GLWrapper::StencilFuncSeparate                 = nullptr;
PFNGLSTENCILMASKSEPARATEPROC                 GLWrapper::StencilMaskSeparate                 = nullptr;
PFNGLATTACHSHADERPROC                        GLWrapper::AttachShader                        = nullptr;
PFNGLBINDATTRIBLOCATIONPROC                  GLWrapper::BindAttribLocation                  = nullptr;
PFNGLCOMPILESHADERPROC                       GLWrapper::CompileShader                       = nullptr;
PFNGLCREATEPROGRAMPROC                       GLWrapper::CreateProgram                       = nullptr;
PFNGLCREATESHADERPROC                        GLWrapper::CreateShader                        = nullptr;
PFNGLDELETEPROGRAMPROC                       GLWrapper::DeleteProgram                       = nullptr;
PFNGLDELETESHADERPROC                        GLWrapper::DeleteShader                        = nullptr;
PFNGLDETACHSHADERPROC                        GLWrapper::DetachShader                        = nullptr;
PFNGLDISABLEVERTEXATTRIBARRAYPROC            GLWrapper::DisableVertexAttribArray            = nullptr;
PFNGLENABLEVERTEXATTRIBARRAYPROC             GLWrapper::EnableVertexAttribArray             = nullptr;
PFNGLGETACTIVEATTRIBPROC                     GLWrapper::GetActiveAttrib                     = nullptr;
PFNGLGETACTIVEUNIFORMPROC                    GLWrapper::GetActiveUniform                    = nullptr;
PFNGLGETATTACHEDSHADERSPROC                  GLWrapper::GetAttachedShaders                  = nullptr;
PFNGLGETATTRIBLOCATIONPROC                   GLWrapper::GetAttribLocation                   = nullptr;
PFNGLGETPROGRAMIVPROC                        GLWrapper::GetProgramiv                        = nullptr;
PFNGLGETPROGRAMINFOLOGPROC                   GLWrapper::GetProgramInfoLog                   = nullptr;
PFNGLGETSHADERIVPROC                         GLWrapper::GetShaderiv                         = nullptr;
PFNGLGETSHADERINFOLOGPROC                    GLWrapper::GetShaderInfoLog                    = nullptr;
PFNGLGETSHADERSOURCEPROC                     GLWrapper::GetShaderSource                     = nullptr;
PFNGLGETUNIFORMLOCATIONPROC                  GLWrapper::GetUniformLocation                  = nullptr;
PFNGLGETUNIFORMFVPROC                        GLWrapper::GetUniformfv                        = nullptr;
PFNGLGETUNIFORMIVPROC                        GLWrapper::GetUniformiv                        = nullptr;
PFNGLGETVERTEXATTRIBDVPROC                   GLWrapper::GetVertexAttribdv                   = nullptr;
PFNGLGETVERTEXATTRIBFVPROC                   GLWrapper::GetVertexAttribfv                   = nullptr;
PFNGLGETVERTEXATTRIBIVPROC                   GLWrapper::GetVertexAttribiv                   = nullptr;
PFNGLGETVERTEXATTRIBPOINTERVPROC             GLWrapper::GetVertexAttribPointerv             = nullptr;
PFNGLISPROGRAMPROC                           GLWrapper::IsProgram                           = nullptr;
PFNGLISSHADERPROC                            GLWrapper::IsShader                            = nullptr;
PFNGLLINKPROGRAMPROC                         GLWrapper::LinkProgram                         = nullptr;
PFNGLSHADERSOURCEPROC                        GLWrapper::ShaderSource                        = nullptr;
PFNGLUSEPROGRAMPROC                          GLWrapper::UseProgram                          = nullptr;
PFNGLUNIFORM1FPROC                           GLWrapper::Uniform1f                           = nullptr;
PFNGLUNIFORM2FPROC                           GLWrapper::Uniform2f                           = nullptr;
PFNGLUNIFORM3FPROC                           GLWrapper::Uniform3f                           = nullptr;
PFNGLUNIFORM4FPROC                           GLWrapper::Uniform4f                           = nullptr;
PFNGLUNIFORM1IPROC                           GLWrapper::Uniform1i                           = nullptr;
PFNGLUNIFORM2IPROC                           GLWrapper::Uniform2i                           = nullptr;
PFNGLUNIFORM3IPROC                           GLWrapper::Uniform3i                           = nullptr;
PFNGLUNIFORM4IPROC                           GLWrapper::Uniform4i                           = nullptr;
PFNGLUNIFORM1FVPROC                          GLWrapper::Uniform1fv                          = nullptr;
PFNGLUNIFORM2FVPROC                          GLWrapper::Uniform2fv                          = nullptr;
PFNGLUNIFORM3FVPROC                          GLWrapper::Uniform3fv                          = nullptr;
PFNGLUNIFORM4FVPROC                          GLWrapper::Uniform4fv                          = nullptr;
PFNGLUNIFORM1IVPROC                          GLWrapper::Uniform1iv                          = nullptr;
PFNGLUNIFORM2IVPROC                          GLWrapper::Uniform2iv                          = nullptr;
PFNGLUNIFORM3IVPROC                          GLWrapper::Uniform3iv                          = nullptr;
PFNGLUNIFORM4IVPROC                          GLWrapper::Uniform4iv                          = nullptr;
PFNGLUNIFORMMATRIX2FVPROC                    GLWrapper::UniformMatrix2fv                    = nullptr;
PFNGLUNIFORMMATRIX3FVPROC                    GLWrapper::UniformMatrix3fv                    = nullptr;
PFNGLUNIFORMMATRIX4FVPROC                    GLWrapper::UniformMatrix4fv                    = nullptr;
PFNGLVALIDATEPROGRAMPROC                     GLWrapper::ValidateProgram                     = nullptr;
PFNGLVERTEXATTRIB1DPROC                      GLWrapper::VertexAttrib1d                      = nullptr;
PFNGLVERTEXATTRIB1DVPROC                     GLWrapper::VertexAttrib1dv                     = nullptr;
PFNGLVERTEXATTRIB1FPROC                      GLWrapper::VertexAttrib1f                      = nullptr;
PFNGLVERTEXATTRIB1FVPROC                     GLWrapper::VertexAttrib1fv                     = nullptr;
PFNGLVERTEXATTRIB1SPROC                      GLWrapper::VertexAttrib1s                      = nullptr;
PFNGLVERTEXATTRIB1SVPROC                     GLWrapper::VertexAttrib1sv                     = nullptr;
PFNGLVERTEXATTRIB2DPROC                      GLWrapper::VertexAttrib2d                      = nullptr;
PFNGLVERTEXATTRIB2DVPROC                     GLWrapper::VertexAttrib2dv                     = nullptr;
PFNGLVERTEXATTRIB2FPROC                      GLWrapper::VertexAttrib2f                      = nullptr;
PFNGLVERTEXATTRIB2FVPROC                     GLWrapper::VertexAttrib2fv                     = nullptr;
PFNGLVERTEXATTRIB2SPROC                      GLWrapper::VertexAttrib2s                      = nullptr;
PFNGLVERTEXATTRIB2SVPROC                     GLWrapper::VertexAttrib2sv                     = nullptr;
PFNGLVERTEXATTRIB3DPROC                      GLWrapper::VertexAttrib3d                      = nullptr;
PFNGLVERTEXATTRIB3DVPROC                     GLWrapper::VertexAttrib3dv                     = nullptr;
PFNGLVERTEXATTRIB3FPROC                      GLWrapper::VertexAttrib3f                      = nullptr;
PFNGLVERTEXATTRIB3FVPROC                     GLWrapper::VertexAttrib3fv                     = nullptr;
PFNGLVERTEXATTRIB3SPROC                      GLWrapper::VertexAttrib3s                      = nullptr;
PFNGLVERTEXATTRIB3SVPROC                     GLWrapper::VertexAttrib3sv                     = nullptr;
PFNGLVERTEXATTRIB4NBVPROC                    GLWrapper::VertexAttrib4Nbv                    = nullptr;
PFNGLVERTEXATTRIB4NIVPROC                    GLWrapper::VertexAttrib4Niv                    = nullptr;
PFNGLVERTEXATTRIB4NSVPROC                    GLWrapper::VertexAttrib4Nsv                    = nullptr;
PFNGLVERTEXATTRIB4NUBPROC                    GLWrapper::VertexAttrib4Nub                    = nullptr;
PFNGLVERTEXATTRIB4NUBVPROC                   GLWrapper::VertexAttrib4Nubv                   = nullptr;
PFNGLVERTEXATTRIB4NUIVPROC                   GLWrapper::VertexAttrib4Nuiv                   = nullptr;
PFNGLVERTEXATTRIB4NUSVPROC                   GLWrapper::VertexAttrib4Nusv                   = nullptr;
PFNGLVERTEXATTRIB4BVPROC                     GLWrapper::VertexAttrib4bv                     = nullptr;
PFNGLVERTEXATTRIB4DPROC                      GLWrapper::VertexAttrib4d                      = nullptr;
PFNGLVERTEXATTRIB4DVPROC                     GLWrapper::VertexAttrib4dv                     = nullptr;
PFNGLVERTEXATTRIB4FPROC                      GLWrapper::VertexAttrib4f                      = nullptr;
PFNGLVERTEXATTRIB4FVPROC                     GLWrapper::VertexAttrib4fv                     = nullptr;
PFNGLVERTEXATTRIB4IVPROC                     GLWrapper::VertexAttrib4iv                     = nullptr;
PFNGLVERTEXATTRIB4SPROC                      GLWrapper::VertexAttrib4s                      = nullptr;
PFNGLVERTEXATTRIB4SVPROC                     GLWrapper::VertexAttrib4sv                     = nullptr;
PFNGLVERTEXATTRIB4UBVPROC                    GLWrapper::VertexAttrib4ubv                    = nullptr;
PFNGLVERTEXATTRIB4UIVPROC                    GLWrapper::VertexAttrib4uiv                    = nullptr;
PFNGLVERTEXATTRIB4USVPROC                    GLWrapper::VertexAttrib4usv                    = nullptr;
PFNGLVERTEXATTRIBPOINTERPROC                 GLWrapper::VertexAttribPointer                 = nullptr;
PFNGLDRAWELEMENTSBASEVERTEXPROC              GLWrapper::DrawElementsBaseVertex              =
nullptr;

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
