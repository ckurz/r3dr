//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/GLWrapper library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef GLWrapper_GLRenderbuffer_h_
#define GLWrapper_GLRenderbuffer_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "GLWrapper/GLWrapper.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace GLWrapper
{

//==============================================================================
// GLRenderbuffer class
//------------------------------------------------------------------------------
class GLRenderbuffer
{
public:
  //============================================================================
  // Constructors
  //----------------------------------------------------------------------------
  GLRenderbuffer();

  //============================================================================
  // Accessors
  //----------------------------------------------------------------------------
  GLuint Id() const;

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  void Create();
  void Delete();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void   Bind() const;
  void Unbind() const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  GLenum Format() const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Storage(
    GLsizei const width,
    GLsizei const height,
    GLenum  const format  = GL_RGBA8,
    GLsizei const samples = 0) const;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Create (
    GLsizei const width,
    GLsizei const height,
    GLenum  const format  = GL_RGBA8,
    GLsizei const samples = 0);

private:
  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  GLuint id_;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
