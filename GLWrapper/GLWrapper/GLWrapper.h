//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/GLWrapper library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef GLWrapper_GLWrapper_h_
#define GLWrapper_GLWrapper_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include <GL/glx.h>
#include <GL/glext.h>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace GLWrapper
{

//==============================================================================
// GLWrapper struct
//------------------------------------------------------------------------------
struct GLWrapper
{
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  static bool Initialize();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  static GLint CheckForErrors();

  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  static PFNGLISRENDERBUFFERPROC                      IsRenderbuffer;
  static PFNGLBINDRENDERBUFFERPROC                    BindRenderbuffer;
  static PFNGLDELETERENDERBUFFERSPROC                 DeleteRenderbuffers;
  static PFNGLGENRENDERBUFFERSPROC                    GenRenderbuffers;
  static PFNGLRENDERBUFFERSTORAGEPROC                 RenderbufferStorage;
  static PFNGLGETRENDERBUFFERPARAMETERIVPROC          GetRenderbufferParameteriv;
  static PFNGLISFRAMEBUFFERPROC                       IsFramebuffer;
  static PFNGLBINDFRAMEBUFFERPROC                     BindFramebuffer;
  static PFNGLDELETEFRAMEBUFFERSPROC                  DeleteFramebuffers;
  static PFNGLGENFRAMEBUFFERSPROC                     GenFramebuffers;
  static PFNGLCHECKFRAMEBUFFERSTATUSPROC              CheckFramebufferStatus;
  static PFNGLFRAMEBUFFERTEXTURE1DPROC                FramebufferTexture1D;
  static PFNGLFRAMEBUFFERTEXTURE2DPROC                FramebufferTexture2D;
  static PFNGLFRAMEBUFFERTEXTURE3DPROC                FramebufferTexture3D;
  static PFNGLFRAMEBUFFERRENDERBUFFERPROC             FramebufferRenderbuffer;
  static PFNGLGETFRAMEBUFFERATTACHMENTPARAMETERIVPROC GetFramebufferAttachmentParameteriv;
  static PFNGLGENERATEMIPMAPPROC                      GenerateMipmap;
  static PFNGLBLITFRAMEBUFFERPROC                     BlitFramebuffer;
  static PFNGLRENDERBUFFERSTORAGEMULTISAMPLEPROC      RenderbufferStorageMultisample;
  static PFNGLFRAMEBUFFERTEXTURELAYERPROC             FramebufferTextureLayer;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  static PFNGLBINDVERTEXARRAYPROC                     BindVertexArray;
  static PFNGLDELETEVERTEXARRAYSPROC                  DeleteVertexArrays;
  static PFNGLGENVERTEXARRAYSPROC                     GenVertexArrays;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  static PFNGLGENQUERIESPROC                          GenQueries;
  static PFNGLDELETEQUERIESPROC                       DeleteQueries;
  static PFNGLISQUERYPROC                             IsQuery;
  static PFNGLBEGINQUERYPROC                          BeginQuery;
  static PFNGLENDQUERYPROC                            EndQuery;
  static PFNGLGETQUERYIVPROC                          GetQueryiv;
  static PFNGLGETQUERYOBJECTIVPROC                    GetQueryObjectiv;
  static PFNGLGETQUERYOBJECTUIVPROC                   GetQueryObjectuiv;
  static PFNGLBINDBUFFERPROC                          BindBuffer;
  static PFNGLDELETEBUFFERSPROC                       DeleteBuffers;
  static PFNGLGENBUFFERSPROC                          GenBuffers;
  static PFNGLISBUFFERPROC                            IsBuffer;
  static PFNGLBUFFERDATAPROC                          BufferData;
  static PFNGLBUFFERSUBDATAPROC                       BufferSubData;
  static PFNGLGETBUFFERSUBDATAPROC                    GetBufferSubData;
  static PFNGLMAPBUFFERPROC                           MapBuffer;
  static PFNGLUNMAPBUFFERPROC                         UnmapBuffer;
  static PFNGLGETBUFFERPARAMETERIVPROC                GetBufferParameteriv;
  static PFNGLGETBUFFERPOINTERVPROC                   GetBufferPointerv;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  static PFNGLBLENDEQUATIONSEPARATEPROC               BlendEquationSeparate;
  static PFNGLDRAWBUFFERSPROC                         DrawBuffers;
  static PFNGLSTENCILOPSEPARATEPROC                   StencilOpSeparate;
  static PFNGLSTENCILFUNCSEPARATEPROC                 StencilFuncSeparate;
  static PFNGLSTENCILMASKSEPARATEPROC                 StencilMaskSeparate;
  static PFNGLATTACHSHADERPROC                        AttachShader;
  static PFNGLBINDATTRIBLOCATIONPROC                  BindAttribLocation;
  static PFNGLCOMPILESHADERPROC                       CompileShader;
  static PFNGLCREATEPROGRAMPROC                       CreateProgram;
  static PFNGLCREATESHADERPROC                        CreateShader;
  static PFNGLDELETEPROGRAMPROC                       DeleteProgram;
  static PFNGLDELETESHADERPROC                        DeleteShader;
  static PFNGLDETACHSHADERPROC                        DetachShader;
  static PFNGLDISABLEVERTEXATTRIBARRAYPROC            DisableVertexAttribArray;
  static PFNGLENABLEVERTEXATTRIBARRAYPROC             EnableVertexAttribArray;
  static PFNGLGETACTIVEATTRIBPROC                     GetActiveAttrib;
  static PFNGLGETACTIVEUNIFORMPROC                    GetActiveUniform;
  static PFNGLGETATTACHEDSHADERSPROC                  GetAttachedShaders;
  static PFNGLGETATTRIBLOCATIONPROC                   GetAttribLocation;
  static PFNGLGETPROGRAMIVPROC                        GetProgramiv;
  static PFNGLGETPROGRAMINFOLOGPROC                   GetProgramInfoLog;
  static PFNGLGETSHADERIVPROC                         GetShaderiv;
  static PFNGLGETSHADERINFOLOGPROC                    GetShaderInfoLog;
  static PFNGLGETSHADERSOURCEPROC                     GetShaderSource;
  static PFNGLGETUNIFORMLOCATIONPROC                  GetUniformLocation;
  static PFNGLGETUNIFORMFVPROC                        GetUniformfv;
  static PFNGLGETUNIFORMIVPROC                        GetUniformiv;
  static PFNGLGETVERTEXATTRIBDVPROC                   GetVertexAttribdv;
  static PFNGLGETVERTEXATTRIBFVPROC                   GetVertexAttribfv;
  static PFNGLGETVERTEXATTRIBIVPROC                   GetVertexAttribiv;
  static PFNGLGETVERTEXATTRIBPOINTERVPROC             GetVertexAttribPointerv;
  static PFNGLISPROGRAMPROC                           IsProgram;
  static PFNGLISSHADERPROC                            IsShader;
  static PFNGLLINKPROGRAMPROC                         LinkProgram;
  static PFNGLSHADERSOURCEPROC                        ShaderSource;
  static PFNGLUSEPROGRAMPROC                          UseProgram;
  static PFNGLUNIFORM1FPROC                           Uniform1f;
  static PFNGLUNIFORM2FPROC                           Uniform2f;
  static PFNGLUNIFORM3FPROC                           Uniform3f;
  static PFNGLUNIFORM4FPROC                           Uniform4f;
  static PFNGLUNIFORM1IPROC                           Uniform1i;
  static PFNGLUNIFORM2IPROC                           Uniform2i;
  static PFNGLUNIFORM3IPROC                           Uniform3i;
  static PFNGLUNIFORM4IPROC                           Uniform4i;
  static PFNGLUNIFORM1FVPROC                          Uniform1fv;
  static PFNGLUNIFORM2FVPROC                          Uniform2fv;
  static PFNGLUNIFORM3FVPROC                          Uniform3fv;
  static PFNGLUNIFORM4FVPROC                          Uniform4fv;
  static PFNGLUNIFORM1IVPROC                          Uniform1iv;
  static PFNGLUNIFORM2IVPROC                          Uniform2iv;
  static PFNGLUNIFORM3IVPROC                          Uniform3iv;
  static PFNGLUNIFORM4IVPROC                          Uniform4iv;
  static PFNGLUNIFORMMATRIX2FVPROC                    UniformMatrix2fv;
  static PFNGLUNIFORMMATRIX3FVPROC                    UniformMatrix3fv;
  static PFNGLUNIFORMMATRIX4FVPROC                    UniformMatrix4fv;
  static PFNGLVALIDATEPROGRAMPROC                     ValidateProgram;
  static PFNGLVERTEXATTRIB1DPROC                      VertexAttrib1d;
  static PFNGLVERTEXATTRIB1DVPROC                     VertexAttrib1dv;
  static PFNGLVERTEXATTRIB1FPROC                      VertexAttrib1f;
  static PFNGLVERTEXATTRIB1FVPROC                     VertexAttrib1fv;
  static PFNGLVERTEXATTRIB1SPROC                      VertexAttrib1s;
  static PFNGLVERTEXATTRIB1SVPROC                     VertexAttrib1sv;
  static PFNGLVERTEXATTRIB2DPROC                      VertexAttrib2d;
  static PFNGLVERTEXATTRIB2DVPROC                     VertexAttrib2dv;
  static PFNGLVERTEXATTRIB2FPROC                      VertexAttrib2f;
  static PFNGLVERTEXATTRIB2FVPROC                     VertexAttrib2fv;
  static PFNGLVERTEXATTRIB2SPROC                      VertexAttrib2s;
  static PFNGLVERTEXATTRIB2SVPROC                     VertexAttrib2sv;
  static PFNGLVERTEXATTRIB3DPROC                      VertexAttrib3d;
  static PFNGLVERTEXATTRIB3DVPROC                     VertexAttrib3dv;
  static PFNGLVERTEXATTRIB3FPROC                      VertexAttrib3f;
  static PFNGLVERTEXATTRIB3FVPROC                     VertexAttrib3fv;
  static PFNGLVERTEXATTRIB3SPROC                      VertexAttrib3s;
  static PFNGLVERTEXATTRIB3SVPROC                     VertexAttrib3sv;
  static PFNGLVERTEXATTRIB4NBVPROC                    VertexAttrib4Nbv;
  static PFNGLVERTEXATTRIB4NIVPROC                    VertexAttrib4Niv;
  static PFNGLVERTEXATTRIB4NSVPROC                    VertexAttrib4Nsv;
  static PFNGLVERTEXATTRIB4NUBPROC                    VertexAttrib4Nub;
  static PFNGLVERTEXATTRIB4NUBVPROC                   VertexAttrib4Nubv;
  static PFNGLVERTEXATTRIB4NUIVPROC                   VertexAttrib4Nuiv;
  static PFNGLVERTEXATTRIB4NUSVPROC                   VertexAttrib4Nusv;
  static PFNGLVERTEXATTRIB4BVPROC                     VertexAttrib4bv;
  static PFNGLVERTEXATTRIB4DPROC                      VertexAttrib4d;
  static PFNGLVERTEXATTRIB4DVPROC                     VertexAttrib4dv;
  static PFNGLVERTEXATTRIB4FPROC                      VertexAttrib4f;
  static PFNGLVERTEXATTRIB4FVPROC                     VertexAttrib4fv;
  static PFNGLVERTEXATTRIB4IVPROC                     VertexAttrib4iv;
  static PFNGLVERTEXATTRIB4SPROC                      VertexAttrib4s;
  static PFNGLVERTEXATTRIB4SVPROC                     VertexAttrib4sv;
  static PFNGLVERTEXATTRIB4UBVPROC                    VertexAttrib4ubv;
  static PFNGLVERTEXATTRIB4UIVPROC                    VertexAttrib4uiv;
  static PFNGLVERTEXATTRIB4USVPROC                    VertexAttrib4usv;
  static PFNGLVERTEXATTRIBPOINTERPROC                 VertexAttribPointer;

  static PFNGLDRAWELEMENTSBASEVERTEXPROC              DrawElementsBaseVertex;

private:
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  template <typename T>
  static bool RegisterFunctionPointer(
    T& variable, char const* const functionName);
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// Shorthand
//------------------------------------------------------------------------------
typedef GLWrapper::GLWrapper gl;

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
