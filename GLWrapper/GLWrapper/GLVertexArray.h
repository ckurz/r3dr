//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/GLWrapper library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef GLWrapper_GLVertexArray_h_
#define GLWrapper_GLVertexArray_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "GLWrapper/GLWrapper.h"
#include "GLWrapper/GLBuffer.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace GLWrapper
{

//==============================================================================
// GLVertexArray class
//------------------------------------------------------------------------------
class GLVertexArray
{
public:
  //============================================================================
  // Constructors
  //----------------------------------------------------------------------------
  GLVertexArray();

  //============================================================================
  // Accessors
  //----------------------------------------------------------------------------
  GLuint Id() const;

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  void Create();
  void Delete();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void   Bind() const;
  void Unbind() const;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Attach(
    GLBuffer  const& buffer,
    GLuint    const  index,
    GLint     const  size,
    GLenum    const  type,
    GLboolean const  normalized = GL_FALSE,
    GLsizei   const  stride     = 0,
    GLvoid    const* pointer    = NULL);

private:
  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  GLuint vertexArray_;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
