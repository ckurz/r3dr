//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/GLWrapper library.
//
// Author: Christian Kurz

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#ifndef GLWrapper_GLProgram_h_
#define GLWrapper_GLProgram_h_

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "GLWrapper/GLWrapper.h"
#include "GLWrapper/GLShader.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <set>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace GLWrapper
{

//==============================================================================
// GLProgram class
// The code in this class is largely based on stuff found on
// http://www.opengl.org/wiki/
//------------------------------------------------------------------------------
class GLProgram
{
public:
  //============================================================================
  // Constructors
  //----------------------------------------------------------------------------
  GLProgram();

  //============================================================================
  // Accessors
  //----------------------------------------------------------------------------
  GLuint Id() const;

  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  void Create();
  void Delete();
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Attach(GLShader const& shader);
  void Detach(GLShader const& shader);
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void BindAttribLocation(GLuint index, std::string const& name) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  GLint GetUniformLocation(std::string const& name) const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Link() const;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  void Use() const;

private:
  //============================================================================
  // Member functions
  //----------------------------------------------------------------------------
  void PrintInfoLog() const;

  //============================================================================
  // Member variables
  //----------------------------------------------------------------------------
  GLuint id_;
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  std::set<GLuint> shaders_;
};

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}

//==============================================================================
// #include guard
//------------------------------------------------------------------------------
#endif
