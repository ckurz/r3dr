//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/GLWrapper library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "GLWrapper/GLBuffer.h"

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace GLWrapper
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
GLBuffer::GLBuffer()
  : buffer_(0)
  , target_(0)
{}

//==============================================================================
// Accessors
//------------------------------------------------------------------------------
GLuint GLBuffer::Id() const
{
  return buffer_;
}

//------------------------------------------------------------------------------
GLenum GLBuffer::Target() const
{
  return target_;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
void GLBuffer::Create(GLenum const target)
{
  Delete();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  target_ = target;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  GLWrapper::GenBuffers(1, &buffer_);
}

//------------------------------------------------------------------------------
void GLBuffer::Delete()
{
  // Much like delete, glDeleteBuffers silently ignores 0's.
  // All bindings to this buffer object automatically revert to 0.
  gl::DeleteBuffers(1, &buffer_);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  buffer_ = 0;
  target_ = 0;
}

//------------------------------------------------------------------------------
void GLBuffer::Bind() const
{
  gl::BindBuffer(target_, buffer_);
}

//------------------------------------------------------------------------------
void GLBuffer::Unbind() const
{
  gl::BindBuffer(target_, 0);
}

//------------------------------------------------------------------------------
void GLBuffer::Data(
  GLsizeiptr const  size,
  GLvoid     const* data,
  GLenum     const  usage)
{
  Bind();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  gl::BufferData(target_, size, data, usage);

  size_ = size;

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Unbind();
}

//------------------------------------------------------------------------------
void GLBuffer::SubData(
  GLsizeiptr const  size,
  GLvoid     const* data,
  GLintptr   const  offset)
{
  if ((size + offset) > size_)
  {
    throw "Buffer not big enough.";
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Bind();

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  gl::BufferSubData(target_, offset, size, data);

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Unbind();
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
