//==============================================================================
// License
//------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//==============================================================================
// Copyright information
//------------------------------------------------------------------------------
// Copyright (C)  2013  Max-Planck-Gesellschaft
//                      zur Förderung der Wissenschaften e.V., Germany
//
// The source code contained in this file has been developed by staff members
// of the Max-Planck-Institut Informatik and is owned by and copyrighted
// proprietary material of the Max-Planck-Gesellschaft zur Förderung der
// Wissenschaften e.V., Germany.
//
// This file is part of the R3DR/GLWrapper library.
//
// Author: Christian Kurz

//==============================================================================
// Includes
//------------------------------------------------------------------------------
#include "GLWrapper/GLProgram.h"
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#include <iostream>
#include <fstream>
#include <sstream>

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
namespace GLWrapper
{

//==============================================================================
// Constructors
//------------------------------------------------------------------------------
GLProgram::GLProgram()
  : id_(0)
{}

//==============================================================================
// Accessors
//------------------------------------------------------------------------------
GLuint GLProgram::Id() const
{
  return id_;
}

//==============================================================================
// Member functions
//------------------------------------------------------------------------------
void GLProgram::Create()
{
  Delete();

  id_ = gl::CreateProgram();
}

//------------------------------------------------------------------------------
void GLProgram::Delete()
{
  for (auto it = shaders_.begin(); it != shaders_.end(); ++it)
  {
    gl::DetachShader(id_, *it);
  }

  shaders_.clear();

  gl::DeleteProgram(id_);
  
  id_ = 0;
}

//------------------------------------------------------------------------------
void GLProgram::Attach(GLShader const& shader)
{
  shaders_.insert(shader.Id());

  gl::AttachShader(id_, shader.Id());
}

//------------------------------------------------------------------------------
void GLProgram::Detach(GLShader const& shader)
{
  std::set<GLuint>::iterator it = shaders_.find(shader.Id());

  if (it != shaders_.end())
  {
    gl::DetachShader(id_, shader.Id());
    shaders_.erase(it);
  }
  else
  {
    throw("DetachShader: shader not found");
  }
}

//------------------------------------------------------------------------------
void GLProgram::BindAttribLocation(GLuint index, const std::string& name) const
{
  gl::BindAttribLocation(id_, index, name.c_str());
}

//------------------------------------------------------------------------------
GLint GLProgram::GetUniformLocation(const std::string& name) const
{
  return gl::GetUniformLocation(id_, name.c_str());
}

//------------------------------------------------------------------------------
void GLProgram::Link() const
{
  gl::LinkProgram(id_);

  GLint linked = 0;

  gl::GetProgramiv(id_, GL_LINK_STATUS, &linked);

  if (linked == GL_FALSE)
  {
    PrintInfoLog();
    throw("Program link failed.");
  }
}

//------------------------------------------------------------------------------
void GLProgram::Use() const
{
  gl::UseProgram(id_);
}

//------------------------------------------------------------------------------
void GLProgram::PrintInfoLog() const
{
  int logLength = 0;

  gl::GetProgramiv(id_, GL_INFO_LOG_LENGTH, &logLength);

  std::cout << "The log length is " << logLength << std::endl;

  if (logLength)
  {
    GLchar* log = new GLchar[logLength];
    int written = 0;

    gl::GetProgramInfoLog(id_, logLength, &written, log);

    std::cout << "InfoLog: " << std::endl;
    std::cout << log << std::endl;

    delete[] log;
  }
}

//==============================================================================
// Top-level namespace
//------------------------------------------------------------------------------
}
